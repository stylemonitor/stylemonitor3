#!/bin/bash
##########################
##                      ##
## Backup of files      ##
## ver 1.0 DAL 20200917 ##
## ver 1.1 DAL 20211012 ##
##                      ##
##########################

#VARIABLES
HOME=/root
SMDEV=/var/www/vhosts/becarri.com/httpdocs
SMBACKUPDEL=$HOME/backups
KEEP=14
TODAY=$(date +%Y%m%d)

#Check if backup directory exists
if [ ! -e $HOME/backups ]
   then mkdir $HOME/backups
fi

#Check if backup date directory exists
if [ ! -e $HOME/backups/$TODAY ]
   then mkdir $HOME/backups/$TODAY
fi

#Additional variable
SM_BACKUP=$HOME/backups/$TODAY

# Copy files from smBecarri dir to backup
cp -R $SMDEV $SM_BACKUP/DEV

# remove backup directorys older than 14 days
cd $SMBACKUPDEL
find -type d -mtime +$KEEP -exec rm -rf {} \;
