#!/bin/bash
##########################
##                      ##
## Upgrade of DEV       ##
## ver 1.0 DAL 20210917 ##
##                      ##
##########################

home='/var/www/vhosts/becarri.com/httpdocs/include'
httpdoc='/var/www/vhosts/becarri.com/httpdocs'

# Move upgrade files to include dir
cd $home

# DATABASE CONNECTION
mv dbconnect.inc.php dbconnect.inc.php.TEMP
cp $home/admin/dbconnect.inc.php.LOCK $home/dbconnect.inc.php

# WEBSITE CONNECTION
mv index.inc.php index.inc.php.TEMP
cp $home/admin/index.inc.php.LOCK $home/index.inc.php

# WEBPAGE CONNECTION
mv $httpdoc/header.php $httpdoc/header.php.TEMP
cp $home/admin/header.php.LOCK $httpdoc/header.php
