index.inc.php#!/bin/bash
##########################
##                      ##
## Upgrade of DEV       ##
## ver 1.0 DAL 20210917 ##
##                      ##
##########################

home='/var/www/vhosts/becarri.com/httpdocs/include'
httpdoc='/var/www/vhosts/becarri.com/httpdocs'

# Remove upgrade files from include dir
cd $home
mv dbconnect.inc.php.TEMP dbconnect.inc.php
mv index.inc.php.TEMP index.inc.php
mv $httpdoc/header.php.TEMP $httpdoc/header.php
