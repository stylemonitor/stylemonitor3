#!/bin/bash

######################
# Menu script        #
# v1.0  DAL 20211117 # 
######################

# VARIABLES ######
home="/root/scripts"
frame_line="========================"
title="#     UPGRADE MENU     #"
column=$(tput cols)
y1=$((($column-${#frame_line})/2))
y2=$((($column-${#title})/2))

# FUNCTIONS ######

key_pressed()
{
echo ""
echo -n "    Press any key to continue..."
read answer 
}

menu()
{
tput clear
tput cup 2 $y1
echo "$frame_line"
tput cup 3 $y2
echo "$title"
tput cup 4 $y1
echo "$frame_line"
echo ""
echo "    1. Lock DEV database and appliction"
echo ""
echo "    2. Unlock DEV database and application"
echo ""
echo "    Q. Quit"
echo ""
echo -n "    SELECT CHOICE TO CONTINUE..."
}

DEV_lock()
{
sh $home/dev_lock.sh
clear
echo ""
echo "    LOCK files now in place - Database and Application locked out"
key_pressed
run_script
}


DEV_unlock()
{
sh $home/dev_unlock.sh
clear
echo ""
echo "    LOCK files have been removed"
key_pressed
run_script
}

LIVE_upgrade()
{
echo live upgrade script
}

wrong_selection()
{
echo "Incorrect selection. Please select again"
}

run_script()
{
menu
while true
do
  read -r choice
  case $choice in
    1) DEV_lock ;;
    2) DEV_unlock ;;
  Q|q) clear ; exit ;;
    *) wrong_selection ;;
  esac
done
}
# MAIN SCRIPT
run_script
