INSERT INTO `forgotten_password_questions` VALUES (1,'Where were you born?')
                                                 ,(2,'What is/was your mothers first name?')
                                                 ,(3,'What was the colour of your first pet?')
                                                 ,(4,'What is the colour of your back door?')
                                                 ,(5,'What is your least favorite food?\r\n')
                                                 ,(6,'What was the make of your first mobile phone?')
                                                 ,(7,'In which country is your favourite film set?')
                                                 ,(8,'How many rooms has your house got?')
                                                 ,(9,'What did your parents get you for your wedding gift?')
                                                 ,(10,'How many children does your brother or sister have?');