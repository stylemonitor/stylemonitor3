/*
StyleMonitor Insert statements for tables with default values
Eleven tables:
company_types table
country table
demo_pwd table
forgotten_password_questions table
licence_type
order_item_movement_reason
order_type
timezones
country_timezone
user_privileges
user_status
*/

-- company_types table
INSERT INTO `company_type` (`ID`, `name`, `type`) VALUES (1, 'Select relationship', ''),
                                                         (2, 'Interested', ''),
                                                         (3, 'Associate', ''),
                                                         (4, 'Client', 'Retailer'),
                                                         (5, 'Supplier', 'Manufacturer'),
                                                         (6, 'Both', ''),
                                                         (7, 'Self', '');

-- country table
INSERT INTO `country` VALUES (1,'Select country','UN2','UN3','Dial',0)
                            ,(2,'Afghanistan','AF','AFG','93',4)
                            ,(3,'Albania','AL','ALB','355',8)
                            ,(4,'Algeria','DZ','DZA','213',12)
                            ,(5,'American Samoa','AS','ASM','1-684',16)
                            ,(6,'Andorra','AD','AND','376',20)
                            ,(7,'Angola','AO','AGO','244',24)
                            ,(8,'Anguilla','AI','AIA','1-264',660)
                            ,(9,'Antarctica','AQ','ATA','672',10)
                            ,(10,'Antigua and Barbuda','AG','ATG','1-268',28)
                            ,(11,'Argentina','AR','ARG','54',32)
                            ,(12,'Armenia','AM','ARM','374',51)
                            ,(13,'Aruba','AW','ABW','297',533)
                            ,(14,'Australia','AU','AUS','61',36)
                            ,(15,'Austria','AT','AUT','43',40)
                            ,(16,'Azerbaijan','AZ','AZE','994',31)
                            ,(17,'Bahamas','BS','BHS','1-242',44)
                            ,(18,'Bahrain','BH','BHR','973',48)
                            ,(19,'Bangladesh','BD','BGD','880',50)
                            ,(20,'Barbados','BB','BRB','1-246',52)
                            ,(21,'Belarus','BY','BLR','375',112)
                            ,(22,'Belgium','BE','BEL','32',56)
                            ,(23,'Belize','BZ','BLZ','501',84)
                            ,(24,'Benin','BJ','BEN','229',204)
                            ,(25,'Bermuda','BM','BMU','1-441',60)
                            ,(26,'Bhutan','BT','BTN','975',64)
                            ,(27,'Bolivia','BO','BOL','591',68)
                            ,(28,'Bonaire','BQ','BES','599',535)
                            ,(29,'Bosnia and Herzegovina','BA','BIH','387',70)
                            ,(30,'Botswana','BW','BWA','267',72)
                            ,(31,'Bouvet Island','BV','BVT','47',74)
                            ,(32,'Brazil','BR','BRA','55',76)
                            ,(33,'British Indian Ocean Territory','IO','IOT','246',86)
                            ,(34,'Brunei Darussalam','BN','BRN','673',96)
                            ,(35,'Bulgaria','BG','BGR','359',100)
                            ,(36,'Burkina Faso','BF','BFA','226',854)
                            ,(37,'Burundi','BI','BDI','257',108)
                            ,(38,'Cambodia','KH','KHM','855',116)
                            ,(39,'Cameroon','CM','CMR','237',120)
                            ,(40,'Canada','CA','CAN','1',124)
                            ,(41,'Cape Verde','CV','CPV','238',132)
                            ,(42,'Cayman Islands','KY','CYM','1-345',136)
                            ,(43,'Central African Republic','CF','CAF','236',140)
                            ,(44,'Chad','TD','TCD','235',148)
                            ,(45,'Chile','CL','CHL','56',152)
                            ,(46,'China','CN','CHN','86',156)
                            ,(47,'Christmas Island','CX','CXR','61',162)
                            ,(48,'Cocos (Keeling) Islands','CC','CCK','61',166)
                            ,(49,'Colombia','CO','COL','57',170)
                            ,(50,'Comoros','KM','COM','269',174)
                            ,(51,'Congo','CG','COG','242',178)
                            ,(52,'Democratic Republic of the Congo','CD','COD','243',180)
                            ,(53,'Cook Islands','CK','COK','682',184)
                            ,(54,'Costa Rica','CR','CRI','506',188)
                            ,(55,'Croatia','HR','HRV','385',191)
                            ,(56,'Cuba','CU','CUB','53',192)
                            ,(57,'Curacao','CW','CUW','599',531)
                            ,(58,'Cyprus','CY','CYP','357',196)
                            ,(59,'Czech Republic','CZ','CZE','420',203)
                            ,(60,'Cote d\'Ivoire','CI','CIV','225',384)
                            ,(61,'Denmark','DK','DNK','45',208)
                            ,(62,'Djibouti','DJ','DJI','253',262)
                            ,(63,'Dominica','DM','DMA','1-767',212)
                            ,(64,'Dominican Republic','DO','DOM','1-809',214)
                            ,(65,'Ecuador','EC','ECU','593',218)
                            ,(66,'Egypt','EG','EGY','20',818)
                            ,(67,'El Salvador','SV','SLV','503',222)
                            ,(68,'Equatorial Guinea','GQ','GNQ','240',226)
                            ,(69,'Eritrea','ER','ERI','291',232)
                            ,(70,'Estonia','EE','EST','372',233)
                            ,(71,'Ethiopia','ET','ETH','251',231)
                            ,(72,'Falkland Islands (Malvinas)','FK','FLK','500',238)
                            ,(73,'Faroe Islands','FO','FRO','298',234)
                            ,(74,'Fiji','FJ','FJI','679',242)
                            ,(75,'Finland','FI','FIN','358',246)
                            ,(76,'France','FR','FRA','33',250)
                            ,(77,'French Guiana','GF','GUF','594',254)
                            ,(78,'French Polynesia','PF','PYF','689',258)
                            ,(79,'French Southern Territories','TF','ATF','262',260)
                            ,(80,'Gabon','GA','GAB','241',266)
                            ,(81,'Gambia','GM','GMB','220',270)
                            ,(82,'Georgia','GE','GEO','995',268)
                            ,(83,'Germany','DE','DEU','49',276)
                            ,(84,'Ghana','GH','GHA','233',288)
                            ,(85,'Gibraltar','GI','GIB','350',292)
                            ,(86,'Greece','GR','GRC','30',300)
                            ,(87,'Greenland','GL','GRL','299',304)
                            ,(88,'Grenada','GD','GRD','1-473',308)
                            ,(89,'Guadeloupe','GP','GLP','590',312)
                            ,(90,'Guam','GU','GUM','1-671',316)
                            ,(91,'Guatemala','GT','GTM','502',320)
                            ,(92,'Guernsey','GG','GGY','44',831)
                            ,(93,'Guinea','GN','GIN','224',324)
                            ,(94,'Guinea-Bissau','GW','GNB','245',624)
                            ,(95,'Guyana','GY','GUY','592',328)
                            ,(96,'Haiti','HT','HTI','509',332)
                            ,(97,'Heard Island and McDonald Islands','HM','HMD','672',334)
                            ,(98,'Holy See (Vatican City State)','VA','VAT','379',336)
                            ,(99,'Honduras','HN','HND','504',340)
                            ,(100,'Hong Kong','HK','HKG','852',344)
                            ,(101,'Hungary','HU','HUN','36',348)
                            ,(102,'Iceland','IS','ISL','354',352)
                            ,(103,'India','IN','IND','91',356)
                            ,(104,'Indonesia','ID','IDN','62',360)
                            ,(105,'Iran, Islamic Republic of','IR','IRN','98',364)
                            ,(106,'Iraq','IQ','IRQ','964',368)
                            ,(107,'Ireland','IE','IRL','353',372)
                            ,(108,'Isle of Man','IM','IMN','44',833)
                            ,(109,'Israel','IL','ISR','972',376)
                            ,(110,'Italy','IT','ITA','39',380)
                            ,(111,'Jamaica','JM','JAM','1-876',388)
                            ,(112,'Japan','JP','JPN','81',392)
                            ,(113,'Jersey','JE','JEY','44',832)
                            ,(114,'Jordan','JO','JOR','962',400)
                            ,(115,'Kazakhstan','KZ','KAZ','7',398)
                            ,(116,'Kenya','KE','KEN','254',404)
                            ,(117,'Kiribati','KI','KIR','686',296)
                            ,(118,'Korea, Democratic People\'s Republic of','KP','PRK','850',408)
                            ,(119,'Korea, Republic of','KR','KOR','82',410)
                            ,(120,'Kuwait','KW','KWT','965',414)
                            ,(121,'Kyrgyzstan','KG','KGZ','996',417)
                            ,(122,'Lao People\'s Democratic Republic','LA','LAO','856',418)
                            ,(123,'Latvia','LV','LVA','371',428)
                            ,(124,'Lebanon','LB','LBN','961',422)
                            ,(125,'Lesotho','LS','LSO','266',426)
                            ,(126,'Liberia','LR','LBR','231',430)
                            ,(127,'Libya','LY','LBY','218',434)
                            ,(128,'Liechtenstein','LI','LIE','423',438)
                            ,(129,'Lithuania','LT','LTU','370',440)
                            ,(130,'Luxembourg','LU','LUX','352',442)
                            ,(131,'Macao','MO','MAC','853',446)
                            ,(132,'Macedonia, the Former Yugoslav Republic of','MK','MKD','389',807)
                            ,(133,'Madagascar','MG','MDG','261',450)
                            ,(134,'Malawi','MW','MWI','265',454)
                            ,(135,'Malaysia','MY','MYS','60',458)
                            ,(136,'Maldives','MV','MDV','960',462)
                            ,(137,'Mali','ML','MLI','223',466)
                            ,(138,'Malta','MT','MLT','356',470)
                            ,(139,'Marshall Islands','MH','MHL','692',584)
                            ,(140,'Martinique','MQ','MTQ','596',474)
                            ,(141,'Mauritania','MR','MRT','222',478)
                            ,(142,'Mauritius','MU','MUS','230',480)
                            ,(143,'Mayotte','YT','MYT','262',175)
                            ,(144,'Mexico','MX','MEX','52',484)
                            ,(145,'Micronesia, Federated States of','FM','FSM','691',583)
                            ,(146,'Moldova, Republic of','MD','MDA','373',498)
                            ,(147,'Monaco','MC','MCO','377',492)
                            ,(148,'Mongolia','MN','MNG','976',496)
                            ,(149,'Montenegro','ME','MNE','382',499)
                            ,(150,'Montserrat','MS','MSR','1-664',500)
                            ,(151,'Morocco','MA','MAR','212',504)
                            ,(152,'Mozambique','MZ','MOZ','258',508)
                            ,(153,'Myanmar','MM','MMR','95',104)
                            ,(154,'Namibia','NA','NAM','264',516)
                            ,(155,'Nauru','NR','NRU','674',520)
                            ,(156,'Nepal','NP','NPL','977',524)
                            ,(157,'Netherlands','NL','NLD','31',528)
                            ,(158,'New Caledonia','NC','NCL','687',540)
                            ,(159,'New Zealand','NZ','NZL','64',554)
                            ,(160,'Nicaragua','NI','NIC','505',558)
                            ,(161,'Niger','NE','NER','227',562)
                            ,(162,'Nigeria','NG','NGA','234',566)
                            ,(163,'Niue','NU','NIU','683',570)
                            ,(164,'Norfolk Island','NF','NFK','672',574)
                            ,(165,'Northern Mariana Islands','MP','MNP','1-670',580)
                            ,(166,'Norway','NO','NOR','47',578)
                            ,(167,'Oman','OM','OMN','968',512)
                            ,(168,'Pakistan','PK','PAK','92',586)
                            ,(169,'Palau','PW','PLW','680',585)
                            ,(170,'Palestine, State of','PS','PSE','970',275)
                            ,(171,'Panama','PA','PAN','507',591)
                            ,(172,'Papua New Guinea','PG','PNG','675',598)
                            ,(173,'Paraguay','PY','PRY','595',600)
                            ,(174,'Peru','PE','PER','51',604)
                            ,(175,'Philippines','PH','PHL','63',608)
                            ,(176,'Pitcairn','PN','PCN','870',612)
                            ,(177,'Poland','PL','POL','48',616)
                            ,(178,'Portugal','PT','PRT','351',620)
                            ,(179,'Puerto Rico','PR','PRI','1',630)
                            ,(180,'Qatar','QA','QAT','974',634)
                            ,(181,'Romania','RO','ROU','40',642)
                            ,(182,'Russian Federation','RU','RUS','7',643)
                            ,(183,'Rwanda','RW','RWA','250',646)
                            ,(184,'Reunion','RE','REU','262',638)
                            ,(185,'Saint Barthelemy','BL','BLM','590',652)
                            ,(186,'Saint Helena','SH','SHN','290',654)
                            ,(187,'Saint Kitts and Nevis','KN','KNA','1-869',659)
                            ,(188,'Saint Lucia','LC','LCA','1-758',662)
                            ,(189,'Saint Martin (French part)','MF','MAF','590',663)
                            ,(190,'Saint Pierre and Miquelon','PM','SPM','508',666)
                            ,(191,'Saint Vincent and the Grenadines','VC','VCT','1-784',670)
                            ,(192,'Samoa','WS','WSM','685',882)
                            ,(193,'San Marino','SM','SMR','378',674)
                            ,(194,'Sao Tome and Principe','ST','STP','239',678)
                            ,(195,'Saudi Arabia','SA','SAU','966',682)
                            ,(196,'Senegal','SN','SEN','221',686)
                            ,(197,'Serbia','RS','SRB','381',688)
                            ,(198,'Seychelles','SC','SYC','248',690)
                            ,(199,'Sierra Leone','SL','SLE','232',694)
                            ,(200,'Singapore','SG','SGP','65',702)
                            ,(201,'Sint Maarten (Dutch part)','SX','SXM','1-721',534)
                            ,(202,'Slovakia','SK','SVK','421',703)
                            ,(203,'Slovenia','SI','SVN','386',705)
                            ,(204,'Solomon Islands','SB','SLB','677',90)
                            ,(205,'Somalia','SO','SOM','252',706)
                            ,(206,'South Africa','ZA','ZAF','27',710)
                            ,(207,'South Georgia and the South Sandwich Islands','GS','SGS','500',239)
                            ,(208,'South Sudan','SS','SSD','211',728)
                            ,(209,'Spain','ES','ESP','34',724)
                            ,(210,'Sri Lanka','LK','LKA','94',144)
                            ,(211,'Sudan','SD','SDN','249',729)
                            ,(212,'Suriname','SR','SUR','597',740)
                            ,(213,'Svalbard and Jan Mayen','SJ','SJM','47',744)
                            ,(214,'Swaziland','SZ','SWZ','268',748)
                            ,(215,'Sweden','SE','SWE','46',752)
                            ,(216,'Switzerland','CH','CHE','41',756)
                            ,(217,'Syrian Arab Republic','SY','SYR','963',760)
                            ,(218,'Taiwan','TW','TWN','886',158)
                            ,(219,'Tajikistan','TJ','TJK','992',762)
                            ,(220,'United Republic of Tanzania','TZ','TZA','255',834)
                            ,(221,'Thailand','TH','THA','66',764)
                            ,(222,'Timor-Leste','TL','TLS','670',626)
                            ,(223,'Togo','TG','TGO','228',768)
                            ,(224,'Tokelau','TK','TKL','690',772)
                            ,(225,'Tonga','TO','TON','676',776)
                            ,(226,'Trinidad and Tobago','TT','TTO','1-868',780)
                            ,(227,'Tunisia','TN','TUN','216',788)
                            ,(228,'Turkey','TR','TUR','90',792)
                            ,(229,'Turkmenistan','TM','TKM','993',795)
                            ,(230,'Turks and Caicos Islands','TC','TCA','1-649',796)
                            ,(231,'Tuvalu','TV','TUV','688',798)
                            ,(232,'Uganda','UG','UGA','256',800)
                            ,(233,'Ukraine','UA','UKR','380',804)
                            ,(234,'United Arab Emirates','AE','ARE','971',784)
                            ,(235,'United Kingdom','GB','GBR','44',826)
                            ,(236,'United States','US','USA','1',840)
                            ,(237,'United States Minor Outlying Islands','UM','UMI','1',581)
                            ,(238,'Uruguay','UY','URY','598',858)
                            ,(239,'Uzbekistan','UZ','UZB','998',860)
                            ,(240,'Vanuatu','VU','VUT','678',548)
                            ,(241,'Venezuela','VE','VEN','58',862)
                            ,(242,'Viet Nam','VN','VNM','84',704)
                            ,(243,'British Virgin Islands','VG','VGB','1-284',92)
                            ,(244,'US Virgin Islands','VI','VIR','1-340',850)
                            ,(245,'Wallis and Futuna','WF','WLF','681',876)
                            ,(246,'Western Sahara','EH','ESH','212',732)
                            ,(247,'Yemen','YE','YEM','967',887)
                            ,(248,'Zambia','ZM','ZMB','260',894)
                            ,(249,'Zimbabwe','ZW','ZWE','263',716)
                            ,(253,'Not known','NO','NOT','00000',0);

-- demo_pwd table
INSERT INTO `demo_pwd` VALUES (1,'P3ache5')
                             ,(2,'Mel0n5')
                             ,(3,'4ppl3S')
                             ,(4,'L3mon5')
                             ,(5,'Gr4p3s')
                             ,(6,'B3rr1e5')
                             ,(7,'M4ng0e5')
                             ,(8,'B4n4n45')
                             ,(9,'Ch3rr1e5')
                             ,(10,'Or4ng35');

-- forgotten_password_questions table
INSERT INTO `forgotten_password_questions` VALUES (1,'Where were you born?')
                                                 ,(2,'What is/was your mothers first name?')
                                                 ,(3,'What was the colour of your first pet?')
                                                 ,(4,'What is the colour of your back door?')
                                                 ,(5,'What is your least favorite food?\r\n')
                                                 ,(6,'What was the make of your first mobile phone?')
                                                 ,(7,'In which country is your favourite film set?')
                                                 ,(8,'How many rooms has your house got?')
                                                 ,(9,'What did your parents get you for your wedding gift?')
                                                 ,(10,'How many children does your brother or sister have?');

-- licence_type table
INSERT INTO `licence_type` VALUES (1,'Trial',1209600)
                                 ,(2,'Yearly',31536000)
                                 ,(3,'Monthly',2425846);

-- order_item_movement_reason table
INSERT INTO `order_item_movement_reason` VALUES (1,'Original Order Quantity','1')
                                               ,(2,'Order Placement','1')
                                               ,(3,'Awaiting (Awt) to 1','1')
                                               ,(4,'1 to 2','1')
                                               ,(5,'2 to 3','1')
                                               ,(6,'3 to 4','1')
                                               ,(7,'4 to 5','1')
                                               ,(8,'5 to W/house (Whs)','1')
                                               ,(9,'Order quantity increase','1')
                                               ,(10,'Order quantity reduction','-1')
                                               ,(11,'Order placement increase','1')
                                               ,(12,'Order placement reduction','-1')
                                               ,(13,'Rejection 1 to Balance','-1')
                                               ,(14,'Rejection 2 to 1','-1')
                                               ,(15,'Rejection 3 to 2','-1')
                                               ,(16,'Rejection 4 to 3','-1')
                                               ,(17,'Rejection 5 to 4','-1')
                                               ,(18,'Reject whs to 5','-1')
                                               ,(19,'Despatched to client','-1')
                                               ,(20,'Returned from client','1')
                                               ,(21,'Returned to supplier','-1')
                                               ,(22,'Received from supplier','1')
                                               ,(23,'Correction balance  to 1','-1')
                                               ,(24,'Correction 1 to 2','-1')
                                               ,(25,'Correction 2 to 3','-1')
                                               ,(26,'Correction 3 to 4','-1')
                                               ,(27,'Correction 4 to 5','-1')
                                               ,(28,'Correction 5 to Warehouse','-1')
                                               ,(29,'Correction 1 to Balance','-1')
                                               ,(30,'Correction 2 to 1','-1')
                                               ,(31,'Correction 3 to 2','-1')
                                               ,(32,'Correction 4 to 3','-1')
                                               ,(33,'Correction 5 to 4','-1')
                                               ,(34,'Correction Warehouse to 5','-1')
                                               ,(35,'Reduced qty sent - CLI','1')
                                               ,(36,'increased qty sent - CLI','-1')
                                               ,(37,'Reduced qty received - SUP','-1')
                                               ,(38,'increased qty received - SUP','1')
                                               ,(39,'NOT IN USE','0')
                                               ,(40,'NOT IN USE','0');

-- order_type table
INSERT INTO `order_type` VALUES (1,'Sales','S')
                                ,(2,'Purchases','P')
                                ,(3,'Partner Sales','PS')
                                ,(4,'Partner Purchases','PP');

INSERT INTO `timezones` VALUES  (1, 'Australian Central Daylight Time', '10:30:00', 37800),
                                (2, 'Australian Central Standard Time', '09:30:00', 34200),
                                (3, 'Australian Eastern Daylight Time', '11:00:00', 39600),
                                (4, 'Australian Eastern Standard Time', '10:00:00', 36000),
                                (5, 'Australian Western Daylight Time', '09:00:00', 32400),
                                (6, 'Australian Western Standard Time', '08:00:00', 28800),
                                (7, 'Australian Central Western Standard Time', '08:45:00', 31500),
                                (8, 'Acre Time ', '-05:00:00', -18000),
                                (9, 'Arabia Daylight Time', '04:00:00', 14400),
                                (10, 'Arabia Standard Time', '03:00:00', 10800),
                                (11, 'Atlantic Daylight Time', '-03:00:00', -10800),
                                (12, 'Atlantic Standard Time', '-04:00:00', -14400),
                                (13, 'Afghanistan Time ', '04:30:00', 16200),
                                (14, 'Alaska Daylight Time', '-08:00:00', -28800),
                                (15, 'Alaska Standard Time', '-09:00:00', -32400),
                                (16, 'Alma-Ata Time ', '06:00:00', 21600),
                                (17, 'Amazon Summer Time ', '-03:00:00', -10800),
                                (18, 'Armenia Summer Time', '05:00:00', 18000),
                                (19, 'Amazon Time ', '-04:00:00', -14400),
                                (20, 'Armenia Time ', '04:00:00', 14400),
                                (21, 'Aqtobe Time ', '05:00:00', 18000),
                                (22, 'Argentina Time', '-03:00:00', -10800),
                                (23, 'Azores Summer Time', '00:00:00', 0),
                                (24, 'Azores Time', '-01:00:00', -3600),
                                (25, 'Azerbaijan Summer Time ', '05:00:00', 18000),
                                (26, 'Azerbaijan Time ', '04:00:00', 14400),
                                (27, 'Brunei Darussalam Time', '08:00:00', 28800),
                                (28, 'Bolivia Time ', '-04:00:00', -14400),
                                (29, 'Brasília Summer Time', '-02:00:00', -7200),
                                (30, 'Brasília Time', '-03:00:00', -10800),
                                (31, 'Bangladesh Standard Time ', '06:00:00', 21600),
                                (32, 'Bougainville Standard Time ', '11:00:00', 39600),
                                (33, 'British Summer Time', '01:00:00', 3600),
                                (34, 'Greenwich Mean Time', '00:00:00', 0),
                                (35, 'Bhutan Time ', '06:00:00', 21600),
                                (36, 'Central Africa Time ', '02:00:00', 7200),
                                (37, 'Cocos Islands Time ', '06:30:00', 23400),
                                (38, 'Central Daylight Time', '-05:00:00', -18000),
                                (39, 'Central Standard Time', '-06:00:00', -21600),
                                (40, 'Cuba Daylight Time ', '-04:00:00', -14400),
                                (41, 'Central European Summer Time', '02:00:00', 7200),
                                (42, 'Central European Time', '01:00:00', 3600),
                                (43, 'Chatham Island Daylight Time', '13:45:00', 49500),
                                (44, 'Chatham Island Standard Time ', '12:45:00', 45900),
                                (45, 'Choibalsan Summer Time', '09:00:00', 32400),
                                (46, 'Choibalsan Time ', '08:00:00', 28800),
                                (47, 'Chuuk Time ', '10:00:00', 36000),
                                (48, 'Cayman Islands Daylight Saving Time ', '-04:00:00', -14400),
                                (49, 'Cayman Islands Standard Time', '-05:00:00', -18000),
                                (50, 'Cook Island Time ', '-10:00:00', -36000),
                                (51, 'Chile Summer Time', '-03:00:00', -10800),
                                (52, 'Chile Standard Time', '-04:00:00', -14400),
                                (53, 'Colombia Time ', '-05:00:00', -18000),
                                (54, 'China Standard Time ', '08:00:00', 28800),
                                (55, 'Cuba Standard Time ', '-05:00:00', -18000),
                                (56, 'Cape Verde Time ', '-01:00:00', -3600),
                                (57, 'Christmas Island Time ', '07:00:00', 25200),
                                (58, 'Chamorro Standard Time', '10:00:00', 36000),
                                (59, 'Easter Island Summer Time', '-05:00:00', -18000),
                                (60, 'Easter Island Standard Time ', '-06:00:00', -21600),
                                (61, 'Eastern Africa Time', '03:00:00', 10800),
                                (62, 'Ecuador Time ', '-05:00:00', -18000),
                                (63, 'Eastern Standard Time', '-05:00:00', -18000),
                                (64, 'Eastern Daylight Time', '-04:00:00', -14400),
                                (65, 'Eastern European Summer Time', '03:00:00', 10800),
                                (66, 'Eastern European Time', '02:00:00', 7200),
                                (67, 'Eastern Greenland Summer Time', '00:00:00', 0),
                                (68, 'East Greenland Time', '-01:00:00', -3600),
                                (69, 'Further-Eastern European Time ', '03:00:00', 10800),
                                (70, 'Fiji Summer Time', '13:00:00', 46800),
                                (71, 'Fiji Time ', '12:00:00', 43200),
                                (72, 'Falkland Islands Summer Time', '-03:00:00', -10800),
                                (73, 'Falkland Island Time', '-04:00:00', -14400),
                                (74, 'Fernando de Noronha Time ', '-02:00:00', -7200),
                                (75, 'Galapagos Time ', '-06:00:00', -21600),
                                (76, 'Gambier Time', '-09:00:00', -32400),
                                (77, 'Georgia Standard Time ', '04:00:00', 14400),
                                (78, 'French Guiana Time ', '-03:00:00', -10800),
                                (79, 'Gilbert Island Time ', '12:00:00', 43200),
                                (80, 'Gulf Standard Time ', '04:00:00', 14400),
                                (81, 'South Georgia Time ', '-02:00:00', -7200),
                                (82, 'Guyana Time ', '-04:00:00', -14400),
                                (83, 'Hawaii-Aleutian Daylight Time', '-09:00:00', -32400),
                                (84, 'Hong Kong Time ', '08:00:00', 28800),
                                (85, 'Hovd Summer Time', '08:00:00', 28800),
                                (86, 'Hovd Time ', '07:00:00', 25200),
                                (87, 'Hawaii Standard Time', '-10:00:00', -36000),
                                (88, 'Indochina Time ', '07:00:00', 25200),
                                (89, 'Israel Daylight Time ', '03:00:00', 10800),
                                (90, 'Indian Chagos Time ', '06:00:00', 21600),
                                (91, 'Iran Daylight Time', '04:30:00', 16200),
                                (92, 'Iran Standard Time', '03:30:00', 12600),
                                (93, 'Irkutsk Summer Time ', '09:00:00', 32400),
                                (94, 'Irkutsk Time ', '08:00:00', 28800),
                                (95, 'India Standard Time', '05:30:00', 19800),
                                (96, 'Irish Standard Time', '01:00:00', 3600),
                                (97, 'Israel Standard Time ', '02:00:00', 7200),
                                (98, 'Japan Standard Time ', '09:00:00', 32400),
                                (99, 'Kyrgyzstan Time ', '06:00:00', 21600),
                                (100, 'Kosrae Time ', '11:00:00', 39600),
                                (101, 'Krasnoyarsk Summer Time ', '08:00:00', 28800),
                                (102, 'Krasnoyarsk Time ', '07:00:00', 25200),
                                (103, 'Korea Standard Time', '09:00:00', 32400),
                                (104, 'Kuybyshev Time', '04:00:00', 14400),
                                (105, 'Lord Howe Daylight Time ', '11:00:00', 39600),
                                (106, 'Lord Howe Standard Time ', '10:30:00', 37800),
                                (107, 'Line Islands Time ', '14:00:00', 50400),
                                (108, 'Magadan Summer Time', '12:00:00', 43200),
                                (109, 'Magadan Time', '11:00:00', 39600),
                                (110, 'Marquesas Time ', '-09:30:00', -30600),
                                (111, 'Mountain Daylight Time', '-06:00:00', -21600),
                                (112, 'Mountain Standard Time', '-07:00:00', -25200),
                                (113, 'Marshall Islands Time ', '12:00:00', 43200),
                                (114, 'Myanmar Time ', '06:30:00', 23400),
                                (115, 'Moscow Daylight Time', '04:00:00', 14400),
                                (116, 'Moscow Standard Time', '03:00:00', 10800),
                                (117, 'Mauritius Time ', '04:00:00', 14400),
                                (118, 'Maldives Time ', '05:00:00', 18000),
                                (119, 'Malaysia Time', '08:00:00', 28800),
                                (120, 'New Caledonia Time ', '11:00:00', 39600),
                                (121, 'Newfoundland Daylight Time', '02:30:00', 9000),
                                (122, 'Newfoundland Standard Time', '-03:30:00', -9000),
                                (123, 'Norfolk Island Time', '11:00:00', 39600),
                                (124, 'Novosibirsk Summer Time', '07:00:00', 25200),
                                (125, 'Novosibirsk Time', '07:00:00', 25200),
                                (126, 'Nepal Time ', '05:45:00', 20700),
                                (127, 'Nauru Time ', '12:00:00', 43200),
                                (128, 'Niue Time ', '-11:00:00', -39600),
                                (129, 'New Zealand Daylight Time ', '13:00:00', 46800),
                                (130, 'New Zealand Standard Time ', '12:00:00', 43200),
                                (131, 'Omsk Summer Time', '07:00:00', 25200),
                                (132, 'Omsk Standard Time', '06:00:00', 21600),
                                (133, 'Oral Time ', '05:00:00', 18000),
                                (134, 'Pacific Daylight Time', '-07:00:00', -25200),
                                (135, 'Pacific Standard Time', '-08:00:00', -28800),
                                (136, 'Peru Time ', '-05:00:00', -18000),
                                (137, 'Kamchatka Summer Time ', '12:00:00', 43200),
                                (138, 'Kamchatka Time', '12:00:00', 43200),
                                (139, 'Papua New Guinea Time ', '10:00:00', 36000),
                                (140, 'Phoenix Island Time ', '13:00:00', 46800),
                                (141, 'Philippine Time', '08:00:00', 28800),
                                (142, 'Pakistan Standard Time', '05:00:00', 18000),
                                (143, 'Pierre & Miquelon Daylight Time ', '-02:00:00', -7200),
                                (144, 'Pierre & Miquelon Standard Time ', '-03:00:00', -10800),
                                (145, 'Pohnpei Standard Time ', '11:00:00', 39600),
                                (146, 'Pitcairn Standard Time ', '-08:00:00', -28800),
                                (147, 'Palau Time ', '09:00:00', 32400),
                                (148, 'Paraguay Summer Time ', '-03:00:00', -10800),
                                (149, 'Paraguay Time ', '-04:00:00', -14400),
                                (150, 'Pyongyang Time', '08:30:00', 30600),
                                (151, 'Qyzylorda Time ', '06:00:00', 21600),
                                (152, 'Reunion Time ', '04:00:00', 14400),
                                (153, 'Sakhalin Time ', '11:00:00', 39600),
                                (154, 'Samara Time', '04:00:00', 14400),
                                (155, 'South Africa Standard Time', '02:00:00', 7200),
                                (156, 'Solomon Islands Time', '11:00:00', 39600),
                                (157, 'Seychelles Time ', '04:00:00', 14400),
                                (158, 'Singapore Time', '08:00:00', 28800),
                                (159, 'Srednekolymsk Time ', '11:00:00', 39600),
                                (160, 'Suriname Time ', '-03:00:00', -10800),
                                (161, 'Samoa Standard Time ', '-11:00:00', -39600),
                                (162, 'Tahiti Time ', '-10:00:00', -36000),
                                (163, 'French Southern and Antarctic Time ', '05:00:00', 18000),
                                (164, 'Tajikistan Time ', '05:00:00', 18000),
                                (165, 'Tokelau Time ', '13:00:00', 46800),
                                (166, 'East Timor Time ', '09:00:00', 32400),
                                (167, 'Turkmenistan Time ', '05:00:00', 18000),
                                (168, 'Tonga Summer Time ', '14:00:00', 50400),
                                (169, 'Tonga Time ', '13:00:00', 46800),
                                (170, 'Turkey Time ', '03:00:00', 10800),
                                (171, 'Tuvalu Time ', '12:00:00', 43200),
                                (172, 'Ulaanbaatar Summer Time', '09:00:00', 32400),
                                (173, 'Ulaanbaatar Time', '08:00:00', 28800),
                                (174, 'Uruguay Summer Time ', '-02:00:00', -7200),
                                (175, 'Uruguay Time ', '-03:00:00', -10800),
                                (176, 'Uzbekistan Time ', '05:00:00', 18000),
                                (177, 'Venezuelan Standard Time', '-04:00:00', -14400),
                                (178, 'Vladivostok Summer Time ', '11:00:00', 39600),
                                (179, 'Vladivostok Time ', '10:00:00', 36000),
                                (180, 'Vanuatu Time', '11:00:00', 39600),
                                (181, 'Wake Time ', '12:00:00', 43200),
                                (182, 'Western Argentine Summer Time ', '-03:00:00', -10800),
                                (183, 'West Africa Summer Time ', '02:00:00', 7200),
                                (184, 'West Africa Time ', '01:00:00', 3600),
                                (185, 'Western European Summer Time', '01:00:00', 3600),
                                (186, 'Western European Time', '00:00:00', 0),
                                (187, 'Wallis and Futuna Time ', '12:00:00', 43200),
                                (188, 'Western Greenland Summer Time', '-02:00:00', -7200),
                                (189, 'West Greenland Time', '-03:00:00', -10800),
                                (190, 'Western Indonesian Time', '07:00:00', 25200),
                                (191, 'Eastern Indonesian Time', '09:00:00', 32400),
                                (192, 'Central Indonesian Time', '08:00:00', 28800),
                                (193, 'West Samoa Time', '13:00:00', 46800),
                                (194, 'Western Sahara Summer Time ', '01:00:00', 3600),
                                (195, 'Western Sahara Standard Time', '00:00:00', 0),
                                (196, 'Yakutsk Summer Time ', '10:00:00', 36000),
                                (197, 'Yakutsk Time ', '09:00:00', 32400),
                                (198, 'Yap Time ', '10:00:00', 36000),
                                (199, 'Yekaterinburg Summer Time ', '06:00:00', 21600),
                                (200, 'Yekaterinburg Time ', '05:00:00', 18000),
                                (201, 'Anadyr Time', '12:00:00', 43200),
                                (202, 'Mawson Time', '05:00:00', 18000),
                                (203, 'Vostok Time', '06:00:00', 21600),
                                (204, 'Davis Time', '07:00:00', 25200),
                                (205, 'Casey Time', '08:00:00', 28800),
                                (206, 'Dumont-dUrville', '10:00:00', 36000);

-- country_timezone table
Insert into country_timezone values (ID,2,13)
                                    ,(ID,3,41)
                                    ,(ID,3,42)
                                    ,(ID,4,42)
                                    ,(ID,5,161)
                                    ,(ID,6,41)
                                    ,(ID,6,42)
                                    ,(ID,7,184)
                                    ,(ID,8,12)
                                    ,(ID,9,22)
                                    ,(ID,9,51)
                                    ,(ID,9,41)
                                    ,(ID,9,202)
                                    ,(ID,9,203)
                                    ,(ID,9,204)
                                    ,(ID,9,205)
                                    ,(ID,9,206)
                                    ,(ID,9,129)
                                    ,(ID,10,12)
                                    ,(ID,11,22)
                                    ,(ID,12,20)
                                    ,(ID,13,12)
                                    ,(ID,14,6)
                                    ,(ID,14,7)
                                    ,(ID,14,2)
                                    ,(ID,14,4)
                                    ,(ID,14,106)
                                    ,(ID,14,1)
                                    ,(ID,14,3)
                                    ,(ID,14,105)
                                    ,(ID,15,41)
                                    ,(ID,15,42)
                                    ,(ID,16,26)
                                    ,(ID,17,64)
                                    ,(ID,17,63)
                                    ,(ID,18,10)
                                    ,(ID,19,31)
                                    ,(ID,20,12)
                                    ,(ID,21,115)
                                    ,(ID,22,41)
                                    ,(ID,22,42)
                                    ,(ID,23,39)
                                    ,(ID,24,184)
                                    ,(ID,25,11)
                                    ,(ID,25,12)
                                    ,(ID,26,35)
                                    ,(ID,27,28)
                                    ,(ID,28,12)
                                    ,(ID,29,41)
                                    ,(ID,29,42)
                                    ,(ID,30,36)
                                    ,(ID,31,44)
                                    ,(ID,31,42)
                                    ,(ID,32,8)
                                    ,(ID,32,19)
                                    ,(ID,32,30)
                                    ,(ID,32,74)
                                    ,(ID,33,90)
                                    ,(ID,34,27)
                                    ,(ID,35,65)
                                    ,(ID,35,66)
                                    ,(ID,36,34)
                                    ,(ID,37,36)
                                    ,(ID,38,88)
                                    ,(ID,39,184)
                                    ,(ID,40,134)
                                    ,(ID,40,112)
                                    ,(ID,40,111)
                                    ,(ID,40,39)
                                    ,(ID,40,28)
                                    ,(ID,40,63)
                                    ,(ID,40,64)
                                    ,(ID,40,12)
                                    ,(ID,40,11)
                                    ,(ID,40,121)
                                    ,(ID,40,135)
                                    ,(ID,40,122)
                                    ,(ID,41,56)
                                    ,(ID,42,63)
                                    ,(ID,43,63)
                                    ,(ID,44,184)
                                    ,(ID,45,59)
                                    ,(ID,45,52)
                                    ,(ID,45,51)
                                    ,(ID,46,54)
                                    ,(ID,46,84)
                                    ,(ID,47,57)
                                    ,(ID,48,37)
                                    ,(ID,49,53)
                                    ,(ID,50,61)
                                    ,(ID,51,184)
                                    ,(ID,52,184)
                                    ,(ID,52,36)
                                    ,(ID,53,50)
                                    ,(ID,54,39)
                                    ,(ID,55,41)
                                    ,(ID,55,42)
                                    ,(ID,56,40)
                                    ,(ID,56,55)
                                    ,(ID,57,12)
                                    ,(ID,58,65)
                                    ,(ID,58,66)
                                    ,(ID,59,41)
                                    ,(ID,59,42)
                                    ,(ID,60,34)
                                    ,(ID,61,41)
                                    ,(ID,61,42)
                                    ,(ID,62,61)
                                    ,(ID,63,12)
                                    ,(ID,64,12)
                                    ,(ID,65,75)
                                    ,(ID,65,62)
                                    ,(ID,66,66)
                                    ,(ID,67,39)
                                    ,(ID,68,184)
                                    ,(ID,69,61)
                                    ,(ID,70,65)
                                    ,(ID,70,66)
                                    ,(ID,71,61)
                                    ,(ID,72,72)
                                    ,(ID,73,185)
                                    ,(ID,73,186)
                                    ,(ID,74,70)
                                    ,(ID,74,71)
                                    ,(ID,75,65)
                                    ,(ID,75,66)
                                    ,(ID,76,41)
                                    ,(ID,76,42)
                                    ,(ID,77,78)
                                    ,(ID,78,162)
                                    ,(ID,78,110)
                                    ,(ID,78,76)
                                    ,(ID,79,163)
                                    ,(ID,80,184)
                                    ,(ID,81,34)
                                    ,(ID,82,115)
                                    ,(ID,82,77)
                                    ,(ID,83,41)
                                    ,(ID,83,42)
                                    ,(ID,84,34)
                                    ,(ID,85,41)
                                    ,(ID,85,42)
                                    ,(ID,86,65)
                                    ,(ID,86,66)
                                    ,(ID,87,11)
                                    ,(ID,87,188)
                                    ,(ID,87,67)
                                    ,(ID,87,34)
                                    ,(ID,87,12)
                                    ,(ID,87,189)
                                    ,(ID,87,68)
                                    ,(ID,88,12)
                                    ,(ID,89,12)
                                    ,(ID,90,58)
                                    ,(ID,91,39)
                                    ,(ID,92,33)
                                    ,(ID,92,39)
                                    ,(ID,93,39)
                                    ,(ID,94,39)
                                    ,(ID,95,82)
                                    ,(ID,96,64)
                                    ,(ID,96,63)
                                    ,(ID,97,163)
                                    ,(ID,98,41)
                                    ,(ID,98,42)
                                    ,(ID,99,39)
                                    ,(ID,100,84)
                                    ,(ID,101,41)
                                    ,(ID,101,42)
                                    ,(ID,102,34)
                                    ,(ID,103,95)
                                    ,(ID,104,190)
                                    ,(ID,104,192)
                                    ,(ID,104,191)
                                    ,(ID,105,91)
                                    ,(ID,105,92)
                                    ,(ID,106,10)
                                    ,(ID,107,96)
                                    ,(ID,107,34)
                                    ,(ID,108,33)
                                    ,(ID,108,34)
                                    ,(ID,109,89)
                                    ,(ID,109,97)
                                    ,(ID,110,41)
                                    ,(ID,110,42)
                                    ,(ID,111,63)
                                    ,(ID,112,98)
                                    ,(ID,113,33)
                                    ,(ID,113,34)
                                    ,(ID,114,65)
                                    ,(ID,114,66)
                                    ,(ID,115,21)
                                    ,(ID,115,133)
                                    ,(ID,115,16)
                                    ,(ID,116,61)
                                    ,(ID,117,79)
                                    ,(ID,117,140)
                                    ,(ID,117,107)
                                    ,(ID,118,103)
                                    ,(ID,119,103)
                                    ,(ID,120,10)
                                    ,(ID,121,99)
                                    ,(ID,122,88)
                                    ,(ID,123,65)
                                    ,(ID,123,66)
                                    ,(ID,124,65)
                                    ,(ID,124,66)
                                    ,(ID,125,155)
                                    ,(ID,126,34)
                                    ,(ID,127,66)
                                    ,(ID,128,41)
                                    ,(ID,128,42)
                                    ,(ID,129,65)
                                    ,(ID,129,66)
                                    ,(ID,130,41)
                                    ,(ID,130,42)
                                    ,(ID,131,54)
                                    ,(ID,132,41)
                                    ,(ID,132,42)
                                    ,(ID,133,61)
                                    ,(ID,134,36)
                                    ,(ID,135,119)
                                    ,(ID,136,118)
                                    ,(ID,137,34)
                                    ,(ID,138,41)
                                    ,(ID,138,42)
                                    ,(ID,139,113)
                                    ,(ID,140,12)
                                    ,(ID,141,34)
                                    ,(ID,142,117)
                                    ,(ID,143,61)
                                    ,(ID,144,134)
                                    ,(ID,144,112)
                                    ,(ID,144,111)
                                    ,(ID,144,38)
                                    ,(ID,144,63)
                                    ,(ID,145,47)
                                    ,(ID,145,45)
                                    ,(ID,145,100)
                                    ,(ID,146,65)
                                    ,(ID,146,66)
                                    ,(ID,147,41)
                                    ,(ID,147,42)
                                    ,(ID,148,85)
                                    ,(ID,148,86)
                                    ,(ID,148,172)
                                    ,(ID,148,173)
                                    ,(ID,148,45)
                                    ,(ID,148,46)
                                    ,(ID,149,41)
                                    ,(ID,149,42)
                                    ,(ID,150,12)
                                    ,(ID,151,185)
                                    ,(ID,151,186)
                                    ,(ID,152,36)
                                    ,(ID,153,114)
                                    ,(ID,154,36)
                                    ,(ID,155,127)
                                    ,(ID,156,126)
                                    ,(ID,157,41)
                                    ,(ID,157,42)
                                    ,(ID,158,120)
                                    ,(ID,159,130)
                                    ,(ID,159,44)
                                    ,(ID,159,129)
                                    ,(ID,159,43)
                                    ,(ID,160,39)
                                    ,(ID,161,184)
                                    ,(ID,162,184)
                                    ,(ID,163,128)
                                    ,(ID,164,123)
                                    ,(ID,165,58)
                                    ,(ID,166,41)
                                    ,(ID,166,42)
                                    ,(ID,167,80)
                                    ,(ID,168,142)
                                    ,(ID,169,147)
                                    ,(ID,170,65)
                                    ,(ID,170,66)
                                    ,(ID,171,63)
                                    ,(ID,172,139)
                                    ,(ID,172,32)
                                    ,(ID,173,149)
                                    ,(ID,173,148)
                                    ,(ID,174,136)
                                    ,(ID,175,141)
                                    ,(ID,176,146)
                                    ,(ID,177,41)
                                    ,(ID,177,42)
                                    ,(ID,178,23)
                                    ,(ID,178,185)
                                    ,(ID,178,24)
                                    ,(ID,178,186)
                                    ,(ID,179,12)
                                    ,(ID,180,10)
                                    ,(ID,181,65)
                                    ,(ID,181,66)
                                    ,(ID,182,66)
                                    ,(ID,182,116)
                                    ,(ID,182,154)
                                    ,(ID,182,200)
                                    ,(ID,182,132)
                                    ,(ID,182,102)
                                    ,(ID,182,125)
                                    ,(ID,182,94)
                                    ,(ID,182,197)
                                    ,(ID,182,179)
                                    ,(ID,182,109)
                                    ,(ID,182,153)
                                    ,(ID,182,159)
                                    ,(ID,182,201)
                                    ,(ID,182,138)
                                    ,(ID,183,36)
                                    ,(ID,184,152)
                                    ,(ID,185,12)
                                    ,(ID,186,34)
                                    ,(ID,187,12)
                                    ,(ID,188,12)
                                    ,(ID,189,12)
                                    ,(ID,190,143)
                                    ,(ID,190,144)
                                    ,(ID,191,12)
                                    ,(ID,192,193)
                                    ,(ID,193,41)
                                    ,(ID,193,42)
                                    ,(ID,194,34)
                                    ,(ID,195,10)
                                    ,(ID,196,34)
                                    ,(ID,197,41)
                                    ,(ID,197,42)
                                    ,(ID,198,157)
                                    ,(ID,199,34)
                                    ,(ID,200,158)
                                    ,(ID,201,12)
                                    ,(ID,202,41)
                                    ,(ID,202,42)
                                    ,(ID,203,41)
                                    ,(ID,203,42)
                                    ,(ID,204,156)
                                    ,(ID,205,61)
                                    ,(ID,206,155)
                                    ,(ID,206,61)
                                    ,(ID,207,81)
                                    ,(ID,208,36)
                                    ,(ID,209,185)
                                    ,(ID,209,41)
                                    ,(ID,209,186)
                                    ,(ID,209,42)
                                    ,(ID,210,95)
                                    ,(ID,211,36)
                                    ,(ID,212,160)
                                    ,(ID,213,41)
                                    ,(ID,213,42)
                                    ,(ID,214,155)
                                    ,(ID,215,41)
                                    ,(ID,215,42)
                                    ,(ID,216,41)
                                    ,(ID,216,42)
                                    ,(ID,217,65)
                                    ,(ID,217,66)
                                    ,(ID,218,54)
                                    ,(ID,219,164)
                                    ,(ID,220,61)
                                    ,(ID,221,88)
                                    ,(ID,222,166)
                                    ,(ID,223,34)
                                    ,(ID,224,165)
                                    ,(ID,225,169)
                                    ,(ID,226,12)
                                    ,(ID,227,42)
                                    ,(ID,228,170)
                                    ,(ID,229,167)
                                    ,(ID,230,64)
                                    ,(ID,230,63)
                                    ,(ID,231,171)
                                    ,(ID,232,61)
                                    ,(ID,233,116)
                                    ,(ID,233,65)
                                    ,(ID,233,66)
                                    ,(ID,234,80)
                                    ,(ID,235,33)
                                    ,(ID,235,34)
                                    ,(ID,236,87)
                                    ,(ID,236,83)
                                    ,(ID,236,14)
                                    ,(ID,236,134)
                                    ,(ID,236,112)
                                    ,(ID,236,111)
                                    ,(ID,236,38)
                                    ,(ID,236,64)
                                    ,(ID,236,15)
                                    ,(ID,236,135)
                                    ,(ID,236,39)
                                    ,(ID,236,63)
                                    ,(ID,237,161)
                                    ,(ID,237,181)
                                    ,(ID,238,175)
                                    ,(ID,239,176)
                                    ,(ID,240,180)
                                    ,(ID,241,177)
                                    ,(ID,242,88)
                                    ,(ID,243,12)
                                    ,(ID,244,12)
                                    ,(ID,245,187)
                                    ,(ID,246,185)
                                    ,(ID,246,186)
                                    ,(ID,247,10)
                                    ,(ID,248,36)
                                    ,(ID,249,36);

-- user_privileges table
INSERT INTO `user_privileges` VALUES (1,'Admin')
                                    ,(2,'Manager')
                                    ,(3,'User');

-- user_status table
INSERT INTO `user_status` VALUES (1,'Pending')
                                 ,(2,'Active')
                                 ,(3,'Suspended')
                                 ,(4,'Left');

