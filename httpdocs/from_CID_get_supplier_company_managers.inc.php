<?php
include 'dbconnect.inc.php';
// echo "include/from_CID_get_supplier_company_managers.inc.php";
// Is the company name the one the user is registered to
// Check that the company licence is still valid
$CID = $sup_ACID;

$sql = "SELECT u.ID as UID
          , c.name as CIDn
          , u.firstname as UIDf
          , u.email as UIDe
          , cl.st_date as CLstd
          , lt.timelimit as LTlimit
        FROM company c
        	, company_licence cl
            , licence_type lt
            , associate_companies ac
            , division d
            , company_division_user cdu
            , users u
        WHERE c.ID = ?
        AND cl.CID = c.ID
        AND cl.LTID = lt.ID
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND cdu.DID = d.ID
        AND cdu.UID = u.ID
        -- for admin user privilege is 1
        AND u.privileges IN (1,2)
        -- for admin user active is 2
        AND u.active = 2
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgca</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $CIDn = $row['CIDn'];
  $caUID = $row['UID'];
  $caUIDf = $row['UIDf'];
  $caUIDe = $row['UIDe'];
  $CLstd = $row['CLstd'];
  $LTlimit = $row['LTlimit'];
}
// echo "Company Admin Users firstname is $caUIDf";
