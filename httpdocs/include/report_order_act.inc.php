<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_order_act.inc.php";

include 'set_urlPage.inc.php';

if (isset($_POST['oi'])) { $oi = $_POST['oi'];}
if (isset($_POST['ot'])) { $ot = $_POST['ot'];}

if (!isset($_POST['update']) && !isset($_POST['movLog']) && !isset($_POST['show_make_units']) && !isset($_POST['summary'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['update'])) {
  header('Location:../'.$urlPage.'&orf&oi='.$oi.'');
  exit();
}elseif (isset($_POST['movLog'])) {
  // echo "<br>Show the detailed movement log";
  header('Location:../'.$urlPage.'&org&oi='.$oi.'');
  exit();
}elseif (isset($_POST['show_make_units'])) {
  header('Location:../'.$urlPage.'&ork&oi='.$oi.'');
  exit();
}elseif (isset($_POST['summary'])) {
  header('Location:../'.$urlPage.'&orb&oi='.$oi.'');
  exit();
}else {
  echo "<br>Go to the index page";
  echo "<br>The Order Item / order type is $oi : $ot";
}
