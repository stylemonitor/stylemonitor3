<?php
// echo "<br><b>include/update_style.inc.php</b>";

$sql = "INSERT INTO log_prod_ref
          (oPRID, oPRIDr, oPRIDd, oPRIDe, oPTID, oDID, oSCID, oSNID, oCOLID, UID, inputtime)
        VALUES
          (?,?,?,?,?,?,?,?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-ensa2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssssssssss", $PRID, $PRIDr, $PRIDd, $PRIDe, $PTID, $DID, $SCID, $SNID, $nCLID, $UID, $td);
  mysqli_stmt_execute($stmt);
}
// echo "<br>PTID ref used $PTID";
// echo "<br>nPTID ref used $nPTID";
$sql = "UPDATE prod_ref
        SET PTID = ?
          , prod_ref = ?
          , prod_desc = ?
        WHERE ID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-ensa3</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $nPTID, $nPRIDr, $nPRIDd, $PRID);
  mysqli_stmt_execute($stmt);
}

header("Location:../styles.php?S&snn");
exit("Update successful");
