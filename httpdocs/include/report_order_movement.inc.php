<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "<br><b>report_order_movement.inc.php</b>";
// echo "OTID::$OTID";

include 'set_urlPage.inc.php';
include 'from_UID_get_last_logout.inc.php'; // $LRIDout
include 'ot_sp_selection_movement.inc.php';

if (isset($_GET['OI'])) {
  $OIID = $_GET['OI'];
}elseif (isset($_GET['oi'])) {
  $OIID = $_GET['oi'];
}
// echo " :: OIID = $OIID";
// $OIID = $_GET['oi'];
include 'from_OIID_get_order_details.inc.php';
$OID = $OID;
include 'from_OID_count_order_item_open.inc.php';
include 'from_OID_count_order_item_closed.inc.php';
include 'from_OIID_get_make_unit.inc.php';

$CID = $_SESSION['CID'];
$UID = $_SESSION['UID'];
// echo "<b>include/form_new_company_user_act.inc.php</b>";
// echo "form_new_order_movement";
// echo "Order Item ID is $OIID<br>";

$urlAwt = "$urlPage&tab=1";
$urlm1p = "$urlPage&tab=2";
$urlm2p = "$urlPage&tab=3";
$urlm3p = "$urlPage&tab=4";
$urlm4p = "$urlPage&tab=5";
$urlm5p = "$urlPage&tab=6";
$urlWhs = "warehouse.php?W&tab=0";
$bc = '#ffffd1';

if ($OTID == 1) {
  $pddhbc = $salAssCol;
}elseif ($OTID == 2) {
  $pddhbc = $purAssCol;
}elseif ($OTID == 3) {
  $pddhbc = $salPrtCol;
}elseif ($OTID == 4) {
  $pddhbc = $purPrtCol;
}

// echo "OTID rom= $OTID";

if ($samProd == 1) {
  $prodType = "Sample/s Garment/s";
}elseif ($samProd == 2) {
  $prodType = "Bulk Production Run";
}
?>

<table class="trs" style="position:absolute; top:32.5%; z-index:1;">
  <?php
  if (isset($_GET['OI'])) {
    ?>
    <caption style="font-weight: bold; text-align:left; text-indent:2%; background-color:<?php echo $pddhbc ?>;">
      ITEM <?php echo $OIIDnos ?> : <?php echo $itemRef ?> : <?php echo $PTIDn ?> - <?php echo "$PRIDr : $PRIDn" ?></caption>
    <?php
  }else {
    ?>
    <caption style="font-weight: bold; text-align:left; text-indent:2%; background-color:<?php echo $pddhbc ?>;">
      ITEMS ON ORDER </caption>
    <?php
  }
  ?>
  <div style="position:absolute; top:37%; left:29%; width:25%; font-size;150%; text-align:left; z-index:2;">
    <?php echo $prodType ?>
  </div>
  <tr>
    <th colspan="3" style="background-color:<?php echo $bbCol ?>;"></th>
    <th colspan="5" style="background-color:<?php echo $mpCol ?>; border-radius: 10px 10px 0px 0px;">In-line Monitoring Points</th>
  </tr>
  <tr >
    <th style=" width:54%; background-color:#f1f1f1;"></th>
    <th style="width:4%; border-radius: 10px 0px 0px 0px;">Qty</th>
    <th style="width:4%;">Awt</th>
    <th style="width:4%; background-color:<?php echo $mpCol ?>;">MP1</th>
    <th style="width:4%; background-color:<?php echo $mpCol ?>;">MP2</th>
    <th style="width:4%; background-color:<?php echo $mpCol ?>;">MP3</th>
    <th style="width:4%; background-color:<?php echo $mpCol ?>;">MP4</th>
    <th style="width:4%; background-color:<?php echo $mpCol ?>;">MP5</th>
    <th style="width:4%; background-color:<?php echo $whsCol ?>;">Whs</th>
    <th style="width:4%; background-color:<?php echo $sentCol ?>;">Sent</th>
    <th style="width:10%; border-radius: 0px 10px 0px 0px;">Due Date</th>
  </tr>

  <?php
  include 'order_QUERY_OIID.inc.php';
  // echo "<br>string";

  if ($cliCID == $CID) {
    if (!isset($_GET['mvt'])) {
      include 'report_movement_log_CLIENT.inc.php';
    }else {
      include 'report_movement_log.inc.php';
    }
  }elseif ($supCID == $CID) {
    if (!isset($_GET['mvt'])) {
      include 'report_movement_log_CLIENT.inc.php';
    }else {
      include 'report_movement_log.inc.php';
    }
  }
