<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "include/form_users.inc.php";

$UID = $_SESSION['UID'];
$CID = $_SESSION['CID'];
// $ACID = $_SESSION['ACID'];
include 'from_CID_count_users.inc.php';
include 'from_UID_get_company_details.inc.php';
include 'from_UID_count_user_login_records.inc.php'; // $cLLID
include 'from_UID_sum_user_login_records.inc.php'; // $sLLIDt
include 'from_CID_check_user_status.inc.php'; // $sLLIDt
include 'from_CID_get_user_count.inc.php'; // $LTIDu
include 'set_urlPage.inc.php'; // $sLLIDt

$lexp = ($clstd + $lttl);
$lexp = date('d-M_Y', $lexp);

if (isset($_GET['usp'])) {
  $active = 1;
  $pddhbc = $uspCol;
}elseif (isset($_GET['usa'])) {
  $active = 2;
  $pddhbc = $usaCol;
}elseif (isset($_GET['usu'])) {
  $active = 3;
  $pddhbc = $youCol;
}

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'User Register';
include 'page_description_date_header.inc.php';

if(isset($_GET['ord'])){ $ord = $_GET['ord'];}
else{ $ord = 'u.privileges';}

if(isset($_GET['sort'])){ $sort = $_GET['sort'];}
else{ $sort = 'ASC';}

// Number of Users per page
// this could be taken from the number of users they can have
// upto a limit of 25 per page
$rpp = 10;

// Check the URL to see what page we are on
if (isset($_GET['pa'])) { $pa = $_GET['pa'];}
else{ $pa = 0;}

// Set the start value record as per the page we are on
if ($pa > 1) { $start = ($pa * $rpp) - $rpp;}
else { $start = 0;}

// number of users listed to use the system by the company
$cUID = $cUSERS;
// echo "<br>Users (cUID) $cUID :: cUSERS : $cUSERS :: Limit of users (cCIDu) : $cCIDu";

// Number of pages
$PTV = ($cUID / $rpp) - 1;

?>

<table class="trs" style="position:absolute; top:9%; left:0%; width:100%;">
  <tr>
    <th>First name</th>
    <th>Surname</th>
    <!-- create a sort to be able to view according to Admin/Manager/User -->
    <th>Level</th>
    <th>Login Count</th>
    <th>Login Time</th>
    <th></th>
    <th></th>
    <th></th>
  </tr>
  <?php

// get the user details for the company
// get the user details for the company
$sql = "SELECT DISTINCT u.ID as sUID
          , d.name as DIDn
          , u.firstname as UIDf
          , u.surname as UIDs
          , u.userInitial as UIDi
          , u.uid as uid
          , u.email as UIDe
          , up.description as UIDv
          , u.inputtime as UIDj
          , u.active as UIDa
          -- need password for testing of Active/Pending/Closed
          , u.pwd as UIDp
          , u.fail as UIDfail
          -- , cdu.position as CDUIDp
        FROM company c
          , associate_companies ac
          , users u
          , division d
          , company_division_user cdu
          , user_privileges up
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND u.active = ?
        AND d.ACID = ac.ID
        AND cdu.DID = d.ID
        AND cdu.UID = u.ID
        AND cdu.current = 1
        AND u.privileges = up.ID
        ORDER BY $ord $sort
        LIMIT $start, $rpp;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
     echo '<b>FAIL</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $active);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $sort == 'DESC' ? $sort = 'ASC' : $sort = 'DESC';

  while($row = mysqli_fetch_assoc($result)){
    $sUID   = $row['sUID'];
    $DIDn   = $row['DIDn'];
    $UIDf   = $row['UIDf'];
    $UIDs   = $row['UIDs'];
    $UIDi   = $row['UIDi'];
    $uid    = $row['uid'];
    $UIDe   = $row['UIDe'];
    $UIDv   = $row['UIDv'];
    $UIDj   = $row['UIDj'];
    $UIDa   = $row['UIDa'];
    $UIDp   = $row['UIDp'];
    $UIDfail   = $row['UIDfail'];
    // $CDUIDp = $row['CDUIDp'];

    // $UIDj = strtotime($UIDj);
    $UIDj = date('d-M-Y', $UIDj);

    ?>

      <tr>
        <td style="text-indent:1%;"><?php echo $UIDf ?></td>
        <td style="text-indent:1%;"><?php echo $UIDs ?></td>
        <td style="text-align:center;"><?php echo $UIDv ?></td>
        <td>User login count</td>
        <td>User login time</td>
        <td style="text-align:center;"><a style="color:blue; text-decoration:none;" href="company.php?C&usr&u=<?php echo "$sUID"; ?>">Review</a></td>
        <?php
        if ($UIDa == 1) {
          ?>
          <td style="background-color:grey; text-align:center;"><?php echo 'PENDING' ?></td>
          <?php
        }elseif ($UIDa == 2) {
          if ($UIDfail == 3) {
            // echo "<br><br><br><br><br><br>USER IS LOCKED OUT";
            ?>
            <td style="text-align:center;"><a style="background-color:red; text-decoration:none;" href="<?php echo $urlPage ?>&ulk=<?php echo $sUID ?>"><?php echo 'Locked Out' ?></a></td>
            <?php
          }else {
            if ($cUSERS == 1) {
              // Cannot suspend yourself!
            }else {
              ?>
              <td style="text-align:center;"><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&sus=<?php echo $sUID ?>"><?php echo 'Suspend' ?></a></td>
              <?php
            }
          }
        }elseif ($UIDa == 3) { ?>
          <td style="background-color:yellow; text-align:center;"><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&rev=<?php echo $sUID ?>"><?php echo 'Re-activatellll' ?></a></td>
        <?php } ?>

      </tr>
    <?php }
    } ?>

    </table>
<div class="pcount">
  <?php for ($x = 1 ; $x <= $PTV+1 ; $x++){
  echo "<a href='?Y&pe&pa=$x'>  $x  </a>";
} ?>
</div>

<form action="include/report_company_general_act.inc.php" method="POST">
<!-- <form class="" action="index.html" method="post"> -->
  <?php
  // Check if available users is less than or equal to licence
  include 'include/from_CID_count_users.inc.php';
  if (($LTIDu >= $cUSERSa) && ($cUSERSp < ($LTIDu - $cUSERSa))) {
    ?>
    <button class="entbtn" style="position:absolute; bottom:5%; left:85%; width:10%;font-weight: bold; background-color:#76f481; padding-bottom:0.3%;" type="submit" name="add_user1">Add User</button>
    <?php
  }
  ?>
</form>

<?php
if (isset($_GET['nu'])) { ?>
  <button class="entbtn" style="position:absolute; bottom:10%; right:10%; height:4%; width:10%;" type="submit" name="add_user"><a style="text-decoration:none;" href="company.php?C">Cancel</a></button>
<?php }else {?>
<?php
} ?>
