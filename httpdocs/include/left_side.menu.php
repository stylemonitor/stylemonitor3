<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';

// echo "left_side.menu.php";

if (!isset($_SESSION['UID'])) {
  header("Location:index.php");
  exit();
}else {
  $CID = $_SESSION['CID'];
  $CIDn = $_SESSION['CIDn'];
      // include 'get_user_login_details.php';
  include 'from_CID_count_product_type.inc.php';
    // has a product type has been created
  include 'from_CID_get_min_prod_ref.inc.php';
  include 'from_CID_count_styles.inc.php';         // has a product reference been created
  include 'from_CID_count_order_open.inc.php';     // has an order been created $cORIDop
  include 'from_CID_count_order_overdue.inc.php';
  include 'from_CID_count_order_thisweek.inc.php';
  include 'from_CID_count_order_nextweek.inc.php';
  include 'from_CID_count_order_later.inc.php';
  include 'from_CID_count_order_new.inc.php';
  include 'from_CID_count_order_CLOSED.inc.php';
  include 'from_CID_count_order_WAREHOUSE.inc.php';
  include 'from_CID_count_company_partners_pending.inc.php';    // get the admin user of the company $cpCPID
  include 'from_CID_count_associates.inc.php';
  include 'from_UID_get_user_details.inc.php';
  include 'sql/outstanding_change_request_count.sql.php';

// ___________________________________________________________
// NOT TOO SURE WHAT THIS DOES ANYMORE
// Gives you the tabs along the top ONLY if there is somthing in them
  if ($cOIDod <> 0) { $p = "home.php"; $tab = 0; }
  elseif ($cOIDtw <> 0) { $p = "home.php"; $tab =1; }
  elseif ($cOIDnw <> 0) { $p = "home.php"; $tab = 2; }
  elseif ($cOIDla <> 0) { $p = "home.php"; $tab = 3; }
  elseif ($cOIDnew <> 0) { $p = "home.php"; $tab = 4; }
  // elseif ($cpCPID <> 0) { $p = "partners.php"; $tab = 7; }

  // removed as $cpCPID not defined
  // if ($cpCPID == 0) { $p1 = "partners.php"; $ref1 = "pap"; }
  // else { $p1 = "partners.php"; $ref1 = "pan"; }

  if ($itemPref == 0) {
    $itemPref = 2;
  }else {
    $itemPref = $itemPref;
  }

  if ($clientPref == 0) {
    $clientPref = 1;
  }else {
    $clientPref = $clientPref;
  }

// ___________________________________________________________

  // echo '<a class="lcl" style="background-color: #f6f6f6; font-weight: bold;" href="dropdown.php">DROPDOWN TEST</a>';

// this adds a tab for becarri on the left side
  // if ($CID == 2) {
  //   // Gets information about stylemonitor for ELLIEYE/becarri people
  //   echo '<a class="lcl" style="color:white; background-color:#4d1895;" href="becarri.php?B">becarri</a>';
  //   // echo '<a class="lcl" style="color:white; background-color:#4d1895;" href="menu/company/mm_B.php?B&in">becarri</a>';
  // }

  if (isset($_GET['ot'])) {
    $ot = $_GET['ot'];
  }else {
    $ot = "1";
  }

  if (isset($_GET['sp'])) {
    $sp = $_GET['sp'];
  }else {
    $sp = '0';
  }

  if (isset($_GET['rep'])) {
    $td = $_GET['rep'];
  }else {
    $td = date('Y-m-j', $td);
  }

  // if (!isset($_GET['rep'])) {
  //   $td = date('d M Y', $td);
  // }

  if ($cORIDop == 0) {
    // Do not show Order Status tab OR Factory Loading tab
    // echo "NO ORDERS PLACED";
  }else {
    if ($cOICID <> 0) {
      echo '<a class="lcl" style="background-color:RED; font-weight: bold;" href="home.php?H&prq">Pending Requests</a>';
    }

    if (isset($_GET['H'])) {
      echo '<a class="lcl" style="background-color:'.$homCol.'; font-weight: bold;" href="home.php?H&home">Order Status</a>';
    }else {
      echo '<a class="lcl" style="background-color:#bb83fc; font-weight: bold;" href="home.php?H&home">Order Status</a>';
    }

    // if (isset($_GET['N'])) {
    //   echo '<a class="lcl" style="background-color:'.$homCol.'; font-weight: bold;" href="new.php?N&new">NEW</a>';
    // }else {
    //   echo '<a class="lcl" style="font-weight: bold;" href="new.php?N&new">NEW</a>';
    // }

    if ($cOIDwhs > 0) {
      if (isset($_GET['W'])) {
        echo '<a class="lcl" style="background-color: '.$whsCol.'; font-weight: bold;" href="warehouse.php?W&tab=0">Stock in Warehouse</a>';
      }else {
        echo '<a class="lcl" style="background-color:#bb83fc; font-weight: bold;" href="warehouse.php?W&tab=0">Stock in Warehouse</a>';
      }
    }else {
      // No tab to be shown
    }

    if ($cOIDclosed > 0) {
      if (isset($_GET['E'])) {
        echo '<a class="lcl" style="background-color: '.$comCol.'; font-weight: bold;" href="complete.php?E&tab=1&ot=1&oc=1">Completed Orders</a>';
      }else {
        echo '<a class="lcl" style="background-color:#bb83fc; font-weight: bold;" href="complete.php?E&tab=1&ot=1&oc=1">Completed Orders</a>';
      }
    }else {
      // No tab to be shown
    }
    // echo "ORDERS PLACED";
  }
  if ($cPRID < 1) {
    if (isset($_GET['S'])) {
      echo '<a class="lcl" style="background-color: '.$styCol.'; font-weight: bold;" href="styles.php?S&snf">Styles</a>';
    }else {
      echo '<a class="lcl" style="font-weight: bold;" href="styles.php?S&snf">Styles</a>';
    }
    if (isset($_GET['C'])) {
      echo '<a class="lcl" style="background-color: antiquewhite; font-weight: bold;" href="company.php?C&coi&0">'?><?php echo $CIDn ?></a><?php
    }else {
      echo '<a class="lcl" style="font-weight: bold;" href="company.php?C&coi&0">'?><?php echo $CIDn ?></a><?php
    }
    if (isset($_GET['W'])) {
      echo '<a class="lcl" style="background-color:'.$htoCol.'; font-weight: bold;" href="how_to.php?W&ht1">How to ...</a>';
    }else {
      echo '<a class="lcl" style="font-weight: bold;" href="how_to.php?W&ht1">How to ...</a>';
    }
    if (isset($_GET['U'])) {
      echo '<a class="lcl" style="background-color: '.$conCol.'; font-weight: bold;" href="company.php?U&cou">Contact</a>';
    }else {
      echo '<a class="lcl" style="font-weight: bold;" href="company.php?U&cou">Contact</a>';
    }
  }else {
    if (isset($_GET['N'])) {
      echo '<a class="lcl" style="background-color:'.$homCol.'; font-weight: bold;" href="new.php?N&new">New</a>';
    }else {
      echo '<a class="lcl" style="font-weight: bold;" href="new.php?N&new">New</a>';
    }
    if (isset($_GET['S'])) {
      echo '<a class="lcl" style="background-color: '.$styCol.'; font-weight: bold;" href="styles.php?S&snn">Styles</a>';
    }else {
      echo '<a class="lcl" style="font-weight: bold;" href="styles.php?S&snn">Styles</a>';
    }
    if (isset($_GET['A'])) {
      echo '<a class="lcl" style="background-color: '.$assCol.'; font-weight: bold;" href="associates.php?A&acc">Clients/Suppliers</a>';
    }else {
      echo '<a class="lcl" style="font-weight: bold;"  href="associates.php?A&acc">Clients/Suppliers</a>';
    }
    // NOT FULLY WORKING aT THE MOMENT 210121
    // Omit from LIVE version but leave in DEV
    if (isset($_GET['P'])) {
      echo '<a class="lcl" style="background-color: '.$parCol.'; font-weight: bold;" href="partners.php?P&pan">PARTNERS</a>';
    }else {
      echo '<a class="lcl" style="font-weight: bold;" href="partners.php?P&pan">PARTNERS</a>';
    }
    if (isset($_GET['C'])) {
      // removed echo for CIDn as it is undefine'd as yet replaced with Company name?
      echo '<a class="lcl" style="color:white; background-color: '.$cnyCol.'; font-weight: bold;" href="company.php?C&coi&0">'?><?php echo $_SESSION['CIDn'] ?></a><?php
    }else {
      echo '<a class="lcl" style="font-weight: bold;" href="company.php?C&coi&0">'?><?php echo $_SESSION['CIDn'] ?></a><?php
    }
    if (isset($_GET['HOW'])) {
      echo '<a class="lcl" style="background-color: '.$htoCol.'; font-weight: bold;" href="how.php?HOW&tab=0">How to ...</a>';
    }else {
      echo '<a class="lcl" style="font-weight: bold;" href="how.php?HOW&tab=0">How to ...</a>';
    }
    if (isset($_GET['U'])) {
      echo '<a class="lcl" style="color:white; background-color: '.$conCol.'; font-weight: bold;" href="company.php?U&cou">Contact</a>';
    }else {
      echo '<a class="lcl" style="color:black; font-weight: bold;" href="company.php?U&cou">Contact</a>';
    }
  }
}
// }
