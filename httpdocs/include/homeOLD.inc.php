<?php
session_start();
include 'dbconnect.inc.php';
$CID = $_SESSION['CID'];
// echo "<b>include/home.inc.php</b>";

include 'from_CID_count_order_overdue.inc.php';
include 'from_CID_count_order_thisweek.inc.php';
include 'from_CID_count_order_nextweek.inc.php';
include 'from_CID_count_order_later.inc.php';

// Setthe report date
$td = date('U');
$MD = date('d-M-Y', $td);


if (isset($_GET['mp'])) {
  if (!isset($_GET['tab'])) {
    if (isset($_GET['OI'])) {
      $OIID = $_GET['OI'];
      include 'include/REPORT_ORDER_header.inc.php';
      include 'include/report_order_movement.inc.php';
    }
  }
  if (isset($_GET['tab'])) {
    $tab = $_GET['tab'];
    if (($tab == 0) || ($tab == 1) || ($tab == 2) || ($tab == 3) || ($tab == 4)) {
      if (isset($_GET['ot'])) {
        $ot = $_GET['ot'];
      }
      include 'include/report_ORDER_STATUS_due_date.inc.php';
    }elseif ($tab == 5) {
      include 'include/order_report_recent.inc.php';
    }
  }
  include 'form_item_movement_update.inc.php';
}elseif (isset($_GET['oi'])) {
  $OIID = $_GET['oi'];
  include 'include/REPORT_ORDER_header.inc.php';
  include 'include/report_order_movement.inc.php';
}elseif (isset($_GET['o'])) {
  $OID = $_GET['o'];
  include 'REPORT_ORDER_header.inc.php';
  include 'report_order_by_item.inc.php';
}elseif (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  if (($tab == 0) || ($tab == 1) || ($tab == 2) || ($tab == 3) || ($tab == 4)) {
    include 'include/report_ORDER_STATUS_due_date.inc.php';
    // include 'select_or_company_sc.inc.php';
  }elseif ($tab == 5) {
    include 'include/order_report_recent.inc.php';
  }
}
