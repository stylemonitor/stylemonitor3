<?php
include 'dbconnect.inc.php';
echo "<br><b>check_PRID_PTID_combo_free.inc.php</b>";

// Check if the prod_desc AND PTID combination are already in use
$sql = "SELECT SUM(IF((CASE WHEN pr.prod_ref = ? THEN CASE WHEN pr.PTID = ? THEN IF(pr.prod_desc = ?,0,1) END END) IS NULL
          ,0
          ,CASE WHEN pr.prod_ref = ? THEN CASE WHEN pr.PTID = ? THEN IF(pr.prod_desc = ?,0,1) END END)) AS 'CHECK'
-- check_PRID_PTID_combo_free.sql
FROM prod_ref pr
WHERE pr.ACID = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-cprv</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssssss", $prod_ref, $PTID, $prod_desc, $prod_ref, $PTID, $prod_desc, $ACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $PRIDok = $row['CHECK'];
}

if ($PRIDok <> 0) {
  echo "<br>Product Ref short code in use";
  header("Location:../styles.php?S&snn&pdx");
  exit();
}

// echo "<br>NEW Prod_desc/PTID - register the item";
