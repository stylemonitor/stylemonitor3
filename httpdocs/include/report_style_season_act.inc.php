<?php
session_start();
include 'dbconnect.inc.php';
// echo '<b>include/report_style_division_act.inc.php</b>';
if (!isset($_POST['addseas'])){
  // echo 'Incorrect method used';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['addseas'])){
  // echo 'Add a division to the style';

  $PRID = $_POST['PRID'];
  $SNID = $_POST['SNID'];
  $UID = $_SESSION['UID'];
  $td = date('U');

  // echo "PRID is $PRID";
  // echo "<br>SNID is $SNID";

  if (empty($SNID)) {
    header('Location:../styles.php?S&sis');
    exit();
  }else {
    // Check that the divisioin is not already listed
    $sql = "SELECT prd.id as PRSNID
            FROM prod_ref_to_season prd
              , prod_ref pr
              , season sn
            WHERE pr.ID = ?
            AND sn.ID = ?
            AND prd.PRID = pr.ID
            AND prd.SNID = sn.ID
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-rsd</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $PRID, $SNID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_assoc($result);
      $PRSNID = $row['PRSNID'];
    }
    // ADD if not alreaDy listed
    if ($PRSNID <> 0) {
      // echo "<br>The style/division has been set";
    }else {
      $sql = "INSERT INTO prod_ref_to_season
                (PRID, SNID, UID, inputtime)
              VALUES
                (?,?,?,?)
              ;";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b>FAIL-rsda2<b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ssss", $PRID, $SNID, $UID, $td);
        mysqli_stmt_execute($stmt);
      }

    }
  }
  header("Location:../styles.php?S&sis&s=$PRID");
  exit();
}
