<?php
echo "<br><b>include/check_prod_ref_new_valid.inc.php</b>";
// echo "<br>Product reference that needs checking = $prod_ref";
// echo "values (2) [$ACID, $prod_ref]";
// check if short code in use
$sql = "SELECT SUM(CASE WHEN pr.prod_ref = ? THEN 1 ELSE 0 END) AS PRIDn
-- SQL is check_prod_ref_new_valid.sql
FROM prod_ref pr
WHERE pr.ACID = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-cprv</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $prod_ref, $ACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  // $PRID = $row['PRID'];
  $PRIDn = $row['PRIDn'];
}

if ($PRIDn <> 0) {
  echo "<br>Product Ref short code in use";
  // header("Location:../styles.php?S&snn&apt=$oPRIDsc&d=$prod_desc");
  // exit();
}

// echo "<br>NEW PRID - register the item";
