<?php
// echo "<br><b>include/check_email_valid.inc.php</b>";

$sql = "SELECT ID as UID
        FROM users
        WHERE uid = ? OR email = ?
        AND active = 2
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-cul</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $fUID, $fUID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $UID = $row['UID'];
}
if (empty($UID)) {
  // echo "<br>User NOT permitted on StyleMonitor";
  header("Location:../login.php?A=9");
  exit();
}else {
  // echo "<br>Valid User UID == $UID";
  include 'check_password_matches.inc.php';
}
