<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/report_associate_company.inc.php</b>";
// get the information from the db
$CID = $_SESSION['CID'];
// include 'from_ACID_count_associate_orders_open.inc.php';

if (isset($_GET['c'])) {
  include 'from_CID_count_associates_clients.inc.php';
  $cACIDs = $cACIDc;
  $ACIDct = 4;
  $pddhbc = $salAssCol;
  $bc     = $salAssCol;
  $cusType = 'Client';
  if (isset($_GET['adq'])) {
    $pddh   = 'Quick Add Client';
  }elseif (isset($_GET['adf'])) {
    $pddh   = 'FULL Add Client with Address';
  }
}elseif (isset($_GET['s'])) {
  include 'from_CID_count_associates_supplier.inc.php';
  $cACIDs = $cACIDs;
  $ACIDct = 5;
  $pddhbc = $purAssCol;
  $bc     = $purAssCol;
  $cusType = 'Supplier';
  if (isset($_GET['adq'])) {
    $pddh   = 'Quick Add Supplier';
  }elseif (isset($_GET['adf'])) {
    $pddh   = 'FULL Add Supplier with Address';
  }
}

if (isset($_GET['acc'])) {
  $pddh   = 'Associate Clients';
  $pddhbc = $salAssCol;
  $bc     = $salAssCol;
  $rpp    = 25;
  $tdiv   = 9;
  $ttab   = 12.1;
  $pageTab = '90%';
//  $ACIDct = 4;
  $count  = ($cACIDc);
}elseif (isset($_GET['acs'])) {
  $pddh   = 'Associate Suppliers';
  $pddhbc = $purAssCol;
  $bc     = $purAssCol;
  $rpp    = 25;
  $tdiv   = 9;
  $ttab   = 12.1;
  $pageTab = '90%';
  // $ACIDct = 5;
  $count  = ($cACIDc);
}elseif (isset($_GET['acn'])) {
  $pddh   = 'Associate Clients/Suppliers';
  $pddhbc = $salAssCol;
  $bc     = $assCol;
  $rpp    = 5;
  $tdiv = 9;
  $ttab = 12.1;
  $pageTab = '45%';
  $ACIDct = 4;
  $count = ($cACIDc);
}elseif (isset($_GET['adq'])) {
  $rpp     = 30;
  $tdiv   = 9;
  $ttab   = 12.1;
  $ACIDct = 4;
  $count = ($cACIDc);
}elseif (isset($_GET['adf'])) {
  $rpp      = 25;
  $tdiv   = 9;
  $ttab   = 12.1;
  // $ACIDct = 4;
  $count = ($cACIDc);
}

// want to add a count of each companies divisions, orders open and closed
// need Andrew to do the sql!!!

// $r sets the rows per page
// Check which page we are on
// Check the URL to see what page we are on
if (isset($_GET['start'])) {
  $start = $_GET['start'];
}else{
  $start = 0;
}

if ($start > 1) {
  $start = ($start * $rpp) - $rpp;
}else {
  $start = 0;
}
// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($count / $rpp);
// $PTV = ceil($PTV);

if (isset($_GET['acc'])) {
  include 'from_CID_count_associates_clients.inc.php';
  if ($cACIDc == 0) {?>
    <div class="cpsty" style="position:absolute; top: <?php echo $tdiv ?>%; height:3%; left:05; width:100%; background-color:<?php echo $bc ?>;"><?php echo "You have NO Associate CLIENT Companies" ?></div>
    <?php
  }elseif ($cACIDc == 1) {
    ?>
    <div class="cpsty" style="position:absolute; top: <?php echo $tdiv ?>%; height:3%; left:05; width:100%; background-color:<?php echo $bc ?>; z-index:1;"><?php echo "You have $cACIDc Associate CLIENT Company" ?></div>
    <?php
  }else {
    ?>
    <div class="cpsty" style="position:absolute; top: <?php echo $tdiv ?>%; height:3%; left:05; width:100%; background-color:<?php echo $bc ?>; z-index:1;"><?php echo "You have $cACIDc Associate CLIENT Companies" ?></div>
    <?php
  }
}elseif (isset($_GET['acs'])) {
  include 'from_CID_count_associates_supplier.inc.php';
  if ($cACIDs == 0) {?>
    <div class="cpsty" style="position:absolute; top: <?php echo $tdiv ?>%; height:3%; left:0%; width:100%; background-color:<?php echo $hc ?>;"><?php echo "You have NO Associate SUPPLIER Companies" ?></div>
    <?php
  }elseif ($cACIDs == 1) {
    ?>
    <div class="cpsty" style="position:absolute; top: <?php echo $tdiv ?>%; height:3%; left:0%; width:100%; background-color:<?php echo $hc ?>;"><?php echo "You have $cACIDs Associate SUPPLIER Company" ?></div>
    <?php
  }else {
    ?>
    <div class="cpsty" style="position:absolute; top: <?php echo $tdiv ?>%; height:3%; left:0%; width:100%; background-color:<?php echo $hc ?>;"><?php echo "You have $cACIDs Associate SUPPLIER Companies" ?></div>
    <?php
  }
}elseif (isset($_GET['adq'])||isset($_GET['adf'])) {
  if ($cACIDs == 0) {?>
    <div class="cpsty" style="position:absolute; top: <?php echo $tdiv ?>%; height:3%; left:0%; width:100%; background-color:<?php echo $hc ?>;"><?php echo "You have NO Associate $cusType Companies" ?></div>
    <?php
  }elseif ($cACIDs == 1) {
    ?>
    <div class="cpsty" style="position:absolute; top: <?php echo $tdiv ?>%; height:3%; left:0%; width:100%; background-color:<?php echo $hc ?>;"><?php echo "You have $cACIDs Associate $cusType Company" ?></div>
    <?php
  }else {
    ?>
    <div class="cpsty" style="position:absolute; top: <?php echo $tdiv ?>%; height:3%; left:0%; width:100%; background-color:<?php echo $hc ?>;"><?php echo "You have $cACIDs Associate $cusType Companies" ?></div>
    <?php
  }
}

?>
<table class="trs" style="position:absolute; top:<?php echo $ttab ?>%; left:0%; width:100%;">
  <tr>
    <th style="width:35%; text-align:left; text-indent:3%;">Associate Company</th>
    <th style="width:35%; text-align:left; text-indent:3%;">Country</th>
    <!-- <th style="width:8%; text-align:left; text-indent:3%;">Division</th> -->
    <th style="width:20%;">Orders Open</th>
    <th style="width:10%;">Joined on</th>
  </tr>
<?php

$sql = "SELECT ac.ID as ASACID
          , ac.name as ACIDn
          , ct.ID as CTID
          , ct.name as CTIDn
          , cy.name as CYIDn
          , ac.inputtime as stDate
        FROM company c
          , associate_companies ac
          , company_type ct
          , country cy
        WHERE c.ID = ?
        AND ac.ID NOT IN (?)
        AND ac.CID = c.ID
        AND ac.CTID = ct.ID
        AND ct.ID IN (?)
        AND ac.CYID = cy.ID
        ORDER BY ac.name
        LIMIT ?,?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-ract</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssss", $CID, $CID, $ACIDct, $start, $rpp);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $ASACID = $row['ASACID'];
    $ACIDn = $row['ACIDn'];
    $CTID = $row['CTID'];
    $CTIDn = $row['CTIDn'];
    $CYIDn = $row['CYIDn'];
    $stDate = $row['stDate'];

    $stDate = date('M-Y',$stDate);
?>
  <tr>
    <td hidden><?php echo "$ASACID"; ?></td>
    <td style="text-align:left; text-indent:2%;"><a style="color:blue; text-decoration:none;"href="associates.php?A&acn&fao=<?php echo $ASACID ?>"><?php echo $ACIDn ?></a></td>
    <td style="text-align:left; text-indent:2%;"><?php echo $CYIDn ?></td>
    <td>fesfes</td>
    <!-- <td></td> -->
    <!-- <td></td> -->
    <!-- <td>Count/Add</td>
    <td><?php echo $cOIDo ?></td> -->
    <td><?php echo $stDate ?></td>
  </tr>
  <?php
  } ?>

</table>
<div style="position:absolute; top:<?php echo $pageTab ?>; height:5%; left:85%; width:10%;font-size: 200%; font-weight: bold; z-index:1;">
  <?php
}
if (isset($_GET['acn'])) {
  if ($count > $rpp) {
    // $count = $count - $rpp;
    echo "<a style='color:black; text-decoration:none;' href='?A&acc'>MORE</a>";
  }else {
    // don't show anything!;
    // echo "string";
  }
}elseif (isset($_GET['acc'])) {
  for ($x = 1 ; $x <= $PTV ; $x++){
    echo "<a style='color:black; text-decoration:none;' href='?A&acc&start=$x'>  $x  </a>";
    // echo "<a style='color:black; text-decoration:none;' href='?$page&tab=$sp&od=$x'>  $x  </a>";
  }
}
?>
</div>
