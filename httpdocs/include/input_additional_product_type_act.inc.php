<?php
session_start();
include 'dbconnect.inc.php';

// echo "input_additional_product_type_act.inc.php";

if (!isset($_POST['addPTID']) && !isset($_POST['cancel'])) {
  // echo "<br>WRONG METHOD USED";
}elseif (isset($_POST['cancel'])) {
  $ACID = $_POST['ACID'];
  $CTID = $_POST['ct'];
  header("Location:../associates.php?A&asf&id=$ACID&ct=$CTID");
  exit();
}elseif (isset($_POST['addPTID'])) {
  // echo "<br>Creat a new company name, record and replace the old one";

  $UID = $_SESSION['UID'];
  $td = date('U');

  $ACID = $_POST['ACID'];
  $CTID = $_POST['ct'];
  $AID = $_POST['AID'];
  $PTID = $_POST['PTID'];

  // echo "<br>Associate company ID $ACID";
  // echo "<br>Associate company address ID $AID";
  // echo "<br>Additional product type $PTID";
  // echo "<br>User $UID";
  // echo "<br>Inputtime $td";

  // add the product type
  $sql = "INSERT INTO associate_company_product_types
            (ACID, PTID, UID, inputtime)
          VALUES
            (?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-iapta</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $ACID, $PTID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }
  header("Location:../associates.php?A&asf&id=$ACID&ct=$CTID");
  exit();
}
