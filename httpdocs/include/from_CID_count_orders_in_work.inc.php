<?php
include 'dbconnect.inc.php';
$CID = $_SESSION['CID'];
// echo "include/from_CID_count_orders_in_work.inc.php";

$sql = "SELECT DISTINCT o.ID as cOIDiw
        FROM company c
        	, associate_companies ac
          , division d
          , orders o
        	, order_placed op
          , order_placed_move opm
          , order_item_movement_reason oimr
          , order_item oi
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND o.tDID = d.ID
        AND o.orComp = 0
        AND oi.OID = o.ID
        AND op.OIID = oi.ID
        AND oimr.ID IN (3, 13)
        AND opm.OPID = op.ID
        AND opm.OIMRID = oimr.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccoiw</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $cOIDiw = mysqli_num_rows($result);
  global $cOIDiw;
}
