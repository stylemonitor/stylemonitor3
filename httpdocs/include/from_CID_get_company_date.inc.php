<?php
include 'dbconnect.inc.php';
// echo "<br>include/from_CID_get_company_date.inc.php";
// CHECK the email is registered

$CID = $_SESSION['CID'];

$sql = "SELECT ID as TID
					, TIMEZONE as TIDn
					, UTC_Offset as TIDoff
				FROM timezones
				WHERE ID = (SELECT TID
										FROM company
										WHERE ID = ?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fCgcd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row    = mysqli_fetch_assoc($result);
	$TID = $row['TID'];
	$TIDn = $row['TIDn'];
	$TIDoff = $row['TIDoff'];
}
