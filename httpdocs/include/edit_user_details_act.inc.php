<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/form_edit_user_details_act.inc.php</b>";
$UID = $_SESSION['UID'];
$td = date('U');

if (!isset($_POST['update_user']) && !isset($_POST['update_user_cancel'])) {
  // echo "<br>Wrong entry method";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['update_user_cancel'])) {
  // // echo "<br>Entry canceled";
  $sUID = $_POST['sUID'];
  header("Location:../company.php?C&usr&u=$sUID");
  exit();
}else (isset($_POST['update_user'])); {

  // echo "<br>Get the information from the form<br>";
  // $UID  = $_POST['UID'];
  $ACID = $_POST['ACID'];
  $eUIDf = $_POST['eUIDf'];
  $eUIDs = $_POST['eUIDs'];
  $sDID = $_POST['sDID'];
  $eUIDu = $_POST['eUIDu'];
  $eUIDi = $_POST['eUIDi'];
  $TID = $_POST['TID'];

  $eDID = $_POST['eDID'];
  $eUIDp = $_POST['eUIDp'];
  $eUIDt = $_POST['eUIDt'];
  $itemType = $_POST['itemType'];
  $clientType = $_POST['clientType'];
  $orderType = $_POST['orderType'];

// echo "$itemType : $clientType :: $orderType";

  $sUID = $_POST['eUID'];
  include 'from_sUID_get_user_details.inc.php';

  // e* is the edited information
  // s* is the selected users current information
  // get the information from the form

  if (empty($eUIDf)) { $eUIDf = $sUIDf; }
  if (empty($eUIDs)) { $eUIDs = $sUIDs; }
  if (empty($eUIDp)) { $eUIDp = $sUIDp; }
  if (empty($eUIDu)) { $eUIDu = $sUIDu; }
  if (empty($eUIDi)) { $eUIDi = $sUIDi; }
  if (empty($eDID))  { $eDID = $sDID; }

  if (empty($itemType)) {
    $itemType = $itemPref;
  }else {
    $itemType = $itemType;
  }
  if (empty($clientType)) {
    $clientType = $clientPref;
  }else {
    $clientType = $clientType;
  }
  if (empty($orderType)) {
    $orderType = $salesPref;
  }else {
    $orderType = $orderType;
  }

  // echo "<br>Changed : $itemType : $clientType :: $orderType";

  // this is what we have from the form either NEW or old
  // echo "<br>Input User UID is $UID";
  // echo "<br>Associate acompany ACID is $ACID";
  // echo "<br>User sUID is $sUID";
  // echo "<br>Revised firstname is eUIDf $eUIDf";
  // echo "<br>Revised surname is eUIDs $eUIDs";
  // echo "<br>Revised initials are eUIDi $eUIDi";
  // echo "<br>Revised user uid is eUIDu $eUIDu";
  // echo "<br>Revised division eDID is $eDID";
  // echo "<br>Revised position is eUIDp $eUIDp";

  // Start transaction
  // mysqli_begin_transaction($mysqli);
  // try {
    // to

  // set the COMPANY time zone NOT an individual users
  $sql = "UPDATE users
          SET TID = ?
            , itemPref = ?
            , clientPref = ?
            , salesPref = ?
          WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-euda</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sssss", $TID, $itemType, $clientType, $orderType, $UID);
    mysqli_stmt_execute($stmt);
  }

  // echo "<br>Lets update the db";
  // update the division and position detail
  $sql = "INSERT INTO log_user_details
            (UID, ori_first, ori_sur, ori_init, ori_uid, ori_email, inputtime)
          VALUES
            (?,?,?,?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b><br>FAIL-feuda1</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sssssss", $UID, $sUIDf, $sUIDs, $sUIDi, $sUIDu, $sUIDe, $td);
    mysqli_stmt_execute($stmt);
    // // echo "<br>Update company_division_user appears to have worked";
    // working
  }

  // update the users division and position detail
  $sql = "INSERT INTO log_cdu
            (ori_div, ori_position, inputuserID, inputtime)
          VALUES
            (?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b><br>FAIL-feuda2</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $sDID, $sUIDp, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // update users first
  $sql = "UPDATE users
          SET firstname = ?
            , surname = ?
            , userInitial = ?
            , uid = ?
            , itemPref = ?
            , clientPref = ?
            , salesPref = ?
          WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-feuda3</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssssssss", $eUIDf, $eUIDs, $eUIDi, $eUIDu, $itemType, $clientType, $orderType, $sUID);
    mysqli_stmt_execute($stmt);
    // // echo "<br>Update users appears to have worked";
  }

  // update the division and position detail
  $sql = "UPDATE company_division_user
          SET DID = ?
            , position = ?
          WHERE UID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b><br>FAIL-feuda4</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $eDID, $eUIDp, $sUID);
    mysqli_stmt_execute($stmt);
    // // echo "<br>Update company_division_user appears to have worked";
  }
  // echo "Has it worked";
  // } catch (mysqli_sql_exception $exception) {
  //   mysqli_rollback($mysqli);
  //
  //   throw $exception;
  // }
  header("Location:../company.php?C&usr&u=$UID");
  exit();
}
