<?php
include 'dbconnect.inc.php';
// echo '<b>include/from_pCID_select_TO_product_ref.inc.php</b>';
if (isset($_POST['tPTID'])) {
  $tPTID = $_POST['tPTID'];
}

if (isset($_POST['pPTID'])) {
  $pPTID = $_POST['pPTID'];
}

$CID = $_SESSION['CID'];

// count how many items there are
$sql = "SELECT pr.ID as cPRID
        FROM prod_ref pr
          , product_type pt
        WHERE pr.PTID = pt.ID
        AND pt.ID IN (?)
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br><b>FAIL-fpstrd1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $tPTID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $Nrows = $row['cPRID'];
}

// list the items for selection
$sql = "SELECT pr.ID as PRID
          , pr.prod_ref as PRIDn
          , pt.name as PTIDn
          , pt.id as PTID
          , pr.prod_desc PRIDd
        FROM prod_ref pr
          , product_type pt
        WHERE pr.PTID = pt.ID
        AND pt.ID IN (?)
        ORDER BY pr.prod_ref
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br><b>FAIL-fpstrd2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $tPTID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $tPRID  = $row['PRID'];
    $tPTID  = $row['PTID'];
    $tPRIDn = $row['PRIDn'];
    $tPRIDd = $row['PRIDd'];
    $tPTIDn = $row['PTIDn'];

    if ($tPTIDn == 'Select Product Type') {
      $tPTIDn = '';
    }else {
      $tPTIDn = $tPTIDn;
    }

    if ($Nrows <> 0) {
      ?>
      <option value="<?php echo $tPRID ?>"><?php echo "$tPRIDn : $tPRIDd" ?></option>
      <?php
    }elseif ($Nrows == 0) {
      ?>
      <option value="">No Styles to Select</option>
      <?php
    }
  }
}
?>
