<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';

// echo "<b>include/form_user_details.inc.php</b>";

if (isset($_GET['u'])) { $curUID = $_GET['u'];}
else { $curUID = $_SESSION['UID'];}
// echo "UID = $UID";

include 'include/from_UID_get_user_details.inc.php';
// echo "//UID11 = $UID";

// echo "//UID14 = $UID";
// include 'include/from_UID_count_forgotten_pwd.inc.php';
include 'from_UID_count_forgotten_pwd.inc.php';
// echo "//UID17 = $UID";

$UIDstd = date('d-M-Y', $UIDstd);

if ($UIDa == 1) { $UIDa = "PENDING";}
elseif ($UIDa == 2) { $UIDa = "ACTIVE";}
elseif ($UIDa == 3) { $UIDa = "SUSPENDED";}

if ($UIDv == 1) { $UIDv = "ADMIN";}
elseif ($UIDv == 2) { $UIDv = "Manager";}
elseif ($UIDv == 3) { $UIDv = "Input Clerk";}
// echo "//UID21 = $UID";

if ($itemPref == 1) {
  $itemPref = 'Sample';
  $ipCol = $samCol;
}else {
  $itemPref = 'Production';
  $ipCol = $proCol;
}

if ($clientPref == 1) {
  $clientPref = 'Associate';
  $cpCol = $assCol;
}else {
  $clientPref = 'Partner';
  $cpCol = $parCol;
}

if ($salesPref == 1) {
  $salesPref = 'Sales';
  $spCol = $salAssCol;
}else {
  $salesPref = 'Purchase';
  $spCol = $purAssCol;
}

if ($cFPUAID == 3) {
  // password question already set
  ?>
  <div style="position:absolute; bottom:2%; height:3.5%; left: 40%; width:20%; background-color: #76f481; border-radius: 10px; border:thin solid grey;padding-top:0.5%;" >
    <a style="color:blue; text-decoration:none;" href="company.php?C&usr&rpw&u=<?php echo $UID ?>">
      Review Security Questions</a>
  </div>
  <?php
}else {
  if ($curUID <> $_SESSION['UID']) {
    // code...
  }else {
    ?>
    <a style="color:blue; text-decoration:none;" href="company.php?C&spq&u=<?php echo $UID ?>">
      <div style="position:absolute; bottom:2%; height:3.5%; left: 40%; width:20%; background-color: #76f481; border-radius: 10px; z-index:1; border:thin solid grey;padding-top:0.5%;" >
        Set Security Questions
      </div>
    </a>
    <?php
  }
}

if (isset($_GET['usb'])) {
  $pddh = 'User Details Edit';
}elseif (isset($_GET['rpw'])) {
    $pddh = 'User Details Update Security Information';
}else {
  $pddh = 'User Details';
}

$pddhbc = $youCol;

include 'page_description_date_header.inc.php';
include 'include/from_UID_get_user_date.inc.php';
?>

<table class="trs" style="position:absolute; top:9%; height:10%; left:0%; width:100%;">
  <tr>
    <th colspan="2" style="background-color:#f1f1f1;"></th>
    <th colspan="3" style="border-radius:10px 10px 0px 0px;">Order Status Preferences</th>
  </tr>
  <tr style="height:7%; font-weight:normal;">
    <th style="width:30%; text-align:left; text-indent:3%;">First name</th>
    <th style="width:40%; text-align:left;">Surname</th>
    <th style="width:10%; font-size:100%; text-align:left; text-indent:5%;">Item</th>
    <th style="width:10%; font-size:100%; text-align:left; text-indent:5%;">Client</th>
    <th style="width:10%; font-size:100%; text-align:left; text-indent:5%;">Sales</th>
  </tr>
  <tr style="font-size:200%; font-weight:bold; text-align:left;">
    <?php
    if (strlen($UIDf) > 10) {
        $UIDf = substr($UIDf,0,10)
      ?>
      <td style="text-align:left; text-indent:5%;"><?php echo $UIDf ?> ...</td>
      <?php
    }else {
      ?>
      <td style="text-align:left; text-indent:5%;"><?php echo $UIDf ?></td>
      <?php
    }
    ?><?php
    if (strlen($UIDs) > 10) {
        $UIDs = substr($UIDs,0,10)
      ?>
      <td style="text-align:left; text-indent:5%;"><?php echo $UIDs ?> ...</td>
      <?php
    }else {
      ?>
      <td style="text-align:left; text-indent:5%;"><?php echo $UIDs ?></td>
      <?php
    }
    ?>
    <td style="font-size:60%; text-align:center; font-weight:normal; background-color:<?php echo $ipCol ?>;"><?php echo $itemPref ?></td>
    <td style="font-size:60%; text-align:center; font-weight:normal; background-color:<?php echo $cpCol ?>;"><?php echo $clientPref ?></td>
    <td style="font-size:60%; text-align:center; font-weight:normal; background-color:<?php echo $spCol ?>;"><?php echo $salesPref ?></td>
  </tr>
  <tr>

  </tr>
</table>

<table class="trs" style="position:absolute; top:22%; height:10%; left:0%; width:100%;">
  <tr style="height:7%; font-weight:normal;">
    <th>Username</th>
    <th>User ID</th>
    <th>email</th>
    <th>TimeZone</th>
  </tr>
  <tr style="font-size:130%; font-weight:bold;">
    <td><?php echo $UIDu ?></td>
    <td><?php echo $UIDi ?></td>
    <td><?php echo $UIDe ?></td>
    <td><?php echo "$TIDn ($TIDoff)" ?></td>
  </tr>
</table>

<table class="trs" style="position:absolute; top:28.5%; height:10%; left:0%; width:100%;">
  <tr style="height:7%; font-weight:normal;">
    <th>Privilege Level</th>
    <th>Status</th>
    <th>Date joined</th>
  </tr>
  <tr style="font-size:130%; font-weight:bold;">
    <td><?php echo $UIDv ?></td>
    <td><?php echo $UIDa ?></td>
    <td><?php echo $UIDstd ?></td>
  </tr>
</table>

<?php
// if user is ADMIN then they canedit parts of the users Details
// set new password, unlock password etc
// check user rights
include 'check_login_user_details.inc.php';

if (($curUID <> $_SESSION['UID']) && ($UIDv == 1)) {
  ?>
  <a style="color:blue; text-decoration:none;" href="company.php?C&usb&u=<?php echo $curUID ?>">
    <div style="position:absolute; bottom:2%; height:3.5%; left: 37.5.5%; width:10%; background-color: <?php echo $edtCol ?>; border-radius: 10px; z-index:1; border:thin solid grey;padding-top:0.5%;" >
    EDIT
    </div>
  </a>
  <a style="color:blue; text-decoration:none;" href="company.php?C&usp">
    <div style="position:absolute; bottom:2%; height:3.5%; left: 52.5%; width:10%; background-color: #76f481; border-radius: 10px; border:thin solid grey;padding-top:0.5%;" >
    Back
    </div>
  </a>
  <?php
}else {
  ?>
  <a style="color:blue; text-decoration:none;" href="company.php?C&usr&usb&u=<?php echo $UID ?>">
    <div style="position:absolute; bottom:2%; height:3.5%; left: 20%; width:15%; background-color: <?php echo $edtCol ?>; border-radius: 10px; border:thin solid grey;padding-top:0.5%;" >
    Edit
    </div>
  </a>
  <?php
  if (isset($_GET['log'])) {
    ?>
    <a style="color:blue; text-decoration:none;" href="company.php?C&usr&u=<?php echo $UID ?>">
      <div style="position:absolute; bottom:2%; height:3.5%; left: 65%; width:15%; background-color: <?php echo $edtCol ?>; border-radius: 10px; border:thin solid grey;padding-top:0.5%;" >
        Hide Login Details
      </div>
    </a>
    <?php
  }else {
    ?>
    <a style="color:blue; text-decoration:none;" href="company.php?C&usr&log&u=<?php echo $UID ?>">
      <div style="position:absolute; bottom:2%; height:3.5%; left: 65%; width:15%; background-color: <?php echo $edtCol ?>; border-radius: 10px; border:thin solid grey;padding-top:0.5%;" >
        Show Login Details
      </div>
    </a>
    <?php
  }
  ?>
  <?php
}
?>
