<?php
session_start();
// Kept session_start as it might be needed here
include 'dbconnect.inc.php';
// if (isset($_GET['u'])) {
//   $UID = $_GET['u'];
// }else
if (isset($_GET['10'])) {
  $UIDf = $_GET['f'];
}
// include 'from_UID_get_user_details.inc.php';

?>
<br><br><br>
<p>Thank you, <?php echo $UIDf ?>, for taking the time to show an interest in our system.</p>
<br>
<p>We have sent you another email, this time with your password.</p>
<br>
<p>There is a link in the email that will take you directly to the login page.</p>
<br>
<p>You will need to use your password, either by re-typing it</p>
<br>
<p>or by copying and pasting it into the Password box.</p>
<br>
<p>We trust you will find <b>S</b>tyle<b>M</b>onitor useful and of help to your company.</p>
<br>
<br>
<b>NOTE</b>
IF your name <b>does not appear</b> in the text above click the box below
<form action="include/email_demo_pwd_not_sent.inc.php" method="post">
  <input type="hidden" name="UID" value="<?php echo $UID ?>">
  <button class="entbtn" type="submit" style="position:absolute; top:55%; left:40%; width:20%;" name="button">Re-send Password</button>
</form>
