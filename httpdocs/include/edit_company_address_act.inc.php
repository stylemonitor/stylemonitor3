<?php
session_start();
include 'dbconnect.inc.php';
// // echo "<b>include/edit_company_name_act.inc.php</b>";

if (!isset($_POST['update_company_name']) && !isset($_POST['cancel_company_name'])) {
  // echo "<br>WRONG METHOD USED";
}elseif (isset($_POST['cancel_company_name'])) {
  header("Location:../company.php?C&coi");
  exit();
}elseif (isset($_POST['update_company_name'])) {
  // echo "<br>Creat a new company name, record and replace the old one";

  $UID = $_SESSION['UID'];
  $td = date('U');

  // Get the data from the form
  // $nCIDn = $_POST['nCIDn'];
  // $CIDn = $_POST['CIDn'];
  $CID = $_POST['CID'];
  $ACID = $_POST['ACID'];

  include 'from_CID_get_company_address.inc.php';

  $naddn = $_POST['naddn'];
  $nadd1 = $_POST['nadd1'];
  $nadd2 = $_POST['nadd2'];
  $ncity = $_POST['ncity'];
  $npcode = $_POST['npcode'];
  $ncounty = $_POST['ncounty'];
  $ntel = $_POST['ntel'];

  if (empty($naddn) && empty($nadd1) && empty($nadd2) && empty($ncity) && empty($npcode) && empty($ncounty) && empty($ntel)) {
    header("Location:../company.php?C&coi");
    exit();
  }

  if (empty($naddn)) {$naddn = $addn;}
  if (empty($nadd1)) {$nadd1 = $add1;}
  if (empty($nadd2)) {$nadd2 = ' - ';}
  if (empty($ncity)) {$ncity = $city;}
  if (empty($npcode)) {$npcode = $pcode;}
  if (empty($ncounty)) {$ncounty = $county;}
  if (empty($ntel)) {$ntel = $tel;}

  // echo "<br>New address name $naddn";
  // echo "<br>New company/associate company name is $nCIDn";
  // echo "<br>Old company/associate company name is $CIDn";
  // echo "<br>Company ID is $CID";
  // echo "<br>Associate company ID is $ACID";

  // Start transaction
  // mysqli_begin_transaction($mysqli);
  // try {
    // to

    $sql = "INSERT INTO log_addresses
              (AID, ori_addn, ori_add1, ori_add2, ori_city, ori_pcode, ori_county, ori_tel, UID, inputtime)
            VALUES
              (?,?,?,?,?,?,?,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-ecaa</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssssssssss", $AID, $addn, $add1, $add2, $city, $pcode, $county, $tel, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    $sql = "UPDATE addresses
            SET addn = ?
              , add1 = ?
              , add2 = ?
              , city = ?
              , pcode = ?
              , county = ?
              , tel = ?
            WHERE ID = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-ecaa1</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssssssss", $naddn, $nadd1, $nadd2, $ncity, $npcode, $ncounty, $ntel, $AID);
      mysqli_stmt_execute($stmt);
    }

  // } catch (mysqli_sql_exception $exception) {
  //   mysqli_rollback($mysqli);
  //
  //   throw $exception;
  // }

  header("Location:../company.php?C&coi");
  exit();
}
