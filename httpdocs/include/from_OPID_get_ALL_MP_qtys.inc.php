<?php
include 'dbconnect.inc.php';
echo "<b>include/from_OPID_get_ALL_MP_qtys.inc.php</b>";

if (isset($_GET['oi'])) {
  $OPID = $_GET['oi'];
}

$sql = "SELECT VAR1.Order_Placed
       , VAR1.sQty1
       , VAR2.sQty2
       , VAR3.sQty3
       , VAR4.sQty4
       , VAR5.sQty5
       , VAR6.sQty6
FROM
(SELECT op.ID as Order_Placed
       , SUM(opm1.omQty*oimr1.numVal) as sQty1
FROM order_placed op
      JOIN order_placed_move opm1 ON op.ID = opm1.OPID
      JOIN order_item_movement_reason oimr1 ON oimr1.ID = opm1.OIMRID
WHERE oimr1.ID IN (3, 13)
  AND op.ID = ?) VAR1
,(SELECT op.ID as Order_Placed
       , SUM(opm2.omQty*oimr2.numVal) as sQty2
FROM order_placed op
      JOIN order_placed_move opm2 ON op.ID = opm2.OPID
      JOIN order_item_movement_reason oimr2 ON oimr2.ID = opm2.OIMRID
WHERE oimr2.ID IN (4, 14)
  AND op.ID = ?) VAR2
,(SELECT op.ID as Order_Placed
       , SUM(opm3.omQty*oimr3.numVal) as sQty3
FROM order_placed op
      JOIN order_placed_move opm3 ON op.ID = opm3.OPID
      JOIN order_item_movement_reason oimr3 ON oimr3.ID = opm3.OIMRID
WHERE oimr3.ID IN (5, 15)
  AND op.ID = ?) VAR3
,(SELECT op.ID as Order_Placed
       , SUM(opm4.omQty*oimr4.numVal) as sQty4
FROM order_placed op
      JOIN order_placed_move opm4 ON op.ID = opm4.OPID
      JOIN order_item_movement_reason oimr4 ON oimr4.ID = opm4.OIMRID
WHERE oimr4.ID IN (6, 16)
  AND op.ID = ?) VAR4
,(SELECT op.ID as Order_Placed
       , SUM(opm5.omQty*oimr5.numVal) as sQty5
FROM order_placed op
      JOIN order_placed_move opm5 ON op.ID = opm5.OPID
      JOIN order_item_movement_reason oimr5 ON oimr5.ID = opm5.OIMRID
WHERE oimr5.ID IN (7, 17)
  AND op.ID = ?) VAR5
,(SELECT op.ID as Order_Placed
       , SUM(opm6.omQty*oimr6.numVal) as sQty6
FROM order_placed op
      JOIN order_placed_move opm6 ON op.ID = opm6.OPID
      JOIN order_item_movement_reason oimr6 ON oimr6.ID = opm6.OIMRID
WHERE oimr6.ID IN (8, 18)
  AND op.ID = ?) VAR6
WHERE VAR1.Order_Placed = VAR2.Order_Placed
  AND   VAR1.Order_Placed = VAR3.Order_Placed
  AND   VAR1.Order_Placed = VAR4.Order_Placed
  AND   VAR1.Order_Placed = VAR5.Order_Placed
  AND   VAR1.Order_Placed = VAR6.Order_Placed
  ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  // echo '<b>FAIL-fog</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssssss", $OPID, $OPID, $OPID, $OPID, $OPID, $OPID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $sMP1 = $_row['sQty1'];
  $sMP2 = $_row['sQty2'];
  $sMP3 = $_row['sQty3'];
  $sMP4 = $_row['sQty4'];
  $sMP5 = $_row['sQty5'];
  $sMP6 = $_row['sQty6'];
}
echo "<br>Passed MP1 : $sMP1";
echo "<br>Passed MP2 : $sMP2";
echo "<br>Passed MP3 : $sMP3";
echo "<br>Passed MP4 : $sMP4";
echo "<br>Passed MP5 : $sMP5";
echo "<br>Passed MP6 : $sMP6";
