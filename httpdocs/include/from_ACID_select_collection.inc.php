<?php
include 'dbconnect.inc.php';
$CID = $_SESSION['CID'];
//echo '<b>include/from_ACID_select_season.inc.php</b>';
// $ACID = $_SESSION['ACID'];
$sql = "SELECT cl.ID as CLID
	        , cl.name as CLIDn
        FROM collection cl
        	, associate_companies ac
        WHERE ac.ID = (SELECT MIN(ac.ID) as mACID
								        FROM associate_companies ac
								          , company c
								        WHERE c.ID = ?
								        AND ac.CID = c.ID)
        AND cl.ACID = ac.ID
        AND cl.name NOT IN ('Select Collection')
        ORDER BY cl.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmc</b>';
}else{
	// changed $CID to $fCID in the hope that it then picks up the division for the associate company set
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
	while($row = mysqli_fetch_array($res)){
	  $CLID = $row['CLID'];
	  $CLIDn = $row['CLIDn'];

	  // echo '<option value="'.$PTID.'">'.$PTIDn.' : '.$PTIDsc.'</option>';
	  echo '<option value="'.$CLID.'">'.$CLIDn.'</option>';
	}
};
?>
