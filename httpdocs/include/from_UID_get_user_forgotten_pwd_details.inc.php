<?php
include 'dbconnect.inc.php';
// echo "include/from_UID_get_user_forgotten_pwd_details.inc.php";

$sql = "SELECT fpua.FPQID as FPUAID
          , fpq.question as FPQIDq
          , fpua.ANS as FPUAa
        FROM forgotten_password_user_answers fpua
          , forgotten_password_questions fpq
          , users u
        WHERE u.ID = ?
        AND fpua.UID = u.ID
        AND fpq.ID = fpua.FPQID
        AND fpua.current = 0
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fugufpd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $UID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while ($row = mysqli_fetch_assoc($result)) {
    $FPUAID = $row['FPUAID'];
    $FPQIDq = $row['FPQIDq'];
    $FPUAa = $row['FPUAa'];

    ?>
    <tr>
      <!-- <td><?php echo $FPUAID ?></td> -->
      <td><?php echo $FPQIDq ?></td>
      <td><?php echo $FPUAa ?></td>
      <td><a href="<?php echo $urlPage ?>&rpw&u=<?php echo $UID ?>&fp=<?php echo $FPUAID ?>">Change</a></td>
    </tr>
    <?php
  }
}
// echo "User privilege is $UIDv";
