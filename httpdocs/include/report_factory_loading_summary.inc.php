<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_factory_loading_summary.inc.php";

$CID = $_SESSION['CID'];
$td = date('U');

include 'from_CID_count_order_overdue.inc.php';
include 'from_CID_count_order_thisweek.inc.php';
include 'from_CID_count_order_nextweek.inc.php';
include 'from_CID_count_order_later.inc.php';
include 'from_CID_count_order_new.inc.php'; // $cOIDnew
include 'from_CID_count_orders_in_work.inc.php'; // $cOIDiw
include 'from_UID_get_last_logout.inc.php'; // $LRIDout
include 'from_CID_count_section_qtys.inc.php';

// Check the URL to see what page we are on
if (isset($_GET['p'])) { $p = $_GET['p']; }else{ $p = 0; }

// Set the rows per page
$r = 20;
// Check which page we are on
if ($p > 1) { $start = ($p * $r) - $r; }else { $start = 0; }

// sets the order type
// all open orders overdue
// if ($cOIDod == 0) {
// }
if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  $sp = 'aod';
  $OC = 0;
  $sd = $td - 31536000;
  $ed = $td;
  $dr = 'oidd.item_del_date';
  $cp = "orders that are OVERDUE";
  $hc = 'black';
  $hbc = '#f85b6b';
}



if(isset($_GET['ord'])){ $ord = $_GET['ord']; }else{ $ord = 'oidd.item_del_date'; }
if(isset($_GET['sort'])){ $sort = $_GET['sort']; }else{ $sort = 'ASC'; }

// echo "<br>Company ID : $CID";
// echo "<br>Order status : $OC";
// echo "<br>Today : $td";
// echo "<br>Start day : $sd";
// echo "<br>End day : $ed";
// echo "<br>Count_orders - due date source : $dr";

if ($dr == 'COMPANY.orDate') {$dra = 'o.orDate'; $dr = $dr;}
elseif ($dr <> 'COMPANY.orDate') {$dra = $dr; $dr = $dr;}


$sql = "SELECT COUNT(o.ID) as OID
        FROM orders o
          , company c
          , associate_companies ac
          , division d
          , order_item oi
          , order_item_del_date oidd
        WHERE c.ID = ?
        AND o.orComp = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        -- AND o.DID = d.ID
        AND oi.OID = o.ID
        AND oidd.OIID = oi.ID
        AND $dra BETWEEN $sd AND $ed
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rfls</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $OC);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOID = $row['OID'];
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cOID / $r);
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;
// echo '<br>Start record is : '.$start;
// echo "<br>**** count_orders count : $cOID ****";

if ($cOID < 1) {
  echo "<br>NO orders placed";
  ?>
  <div class="trs" style="position:relative; top:0%;">
    <div class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "There are 0 Orders"?></div>
  </div>
      <p>NO ORDERS TO REVIEW</p>
  <?php
  header("Location:../home.php?H");
  exit();

}else {
  ?>
  <div class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "There are $SECqty orders in work" ?></div>
  <table style="position:relative; top:0%; width:100%;">
    <tr>
      <th style="width:14%;"></th>
      <th style="width:14%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
      <th style="width:4%;"></th>
    </tr>
    <tr>
      <th>Order type</th>
      <th>Count of orders</th>
      <th colspan="18">Section</th>
    </tr>
    <tr>
      <th colspan="2"></th>
      <th colspan="3" style="background-color:#d3cbcb;">Or</th>
      <th colspan="3">1</th>
      <th colspan="3" style="background-color:#d3cbcb;">2</th>
      <th colspan="3">3</th>
      <th colspan="3" style="background-color:#d3cbcb;">4</th>
      <th colspan="3">5</th>
    </tr>
    <tr>
      <th colspan="2"></th>
      <th style="background-color:#d3cbcb;">1</th>
      <th style="background-color:#d3cbcb;">2</th>
      <th style="background-color:#d3cbcb;">3</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th style="background-color:#d3cbcb;">1</th>
      <th style="background-color:#d3cbcb;">2</th>
      <th style="background-color:#d3cbcb;">3</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th style="background-color:#d3cbcb;">1</th>
      <th style="background-color:#d3cbcb;">2</th>
      <th style="background-color:#d3cbcb;">3</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>


    </tr>
  <?php
}

// New query from Andrew to loading the factory
$sql = "SELECT TOTALS.CID
       , TOTALS.ACID
       , sum(TOTALS.Awaiting) AS AWAITING
       , sum(TOTALS.Stage1) AS STAGE1
       , sum(TOTALS.Stage2) AS STAGE2
       , sum(TOTALS.Stage3) AS STAGE3
       , sum(TOTALS.Stage4) AS STAGE4
       , sum(TOTALS.Stage5) AS STAGE5
       , sum(TOTALS.Warehouse) AS WAREHOUSE
       , sum(TOTALS.Awaiting) + sum(TOTALS.Stage1) + sum(TOTALS.Stage2) + sum(TOTALS.Stage3) + sum(TOTALS.Stage4) + sum(TOTALS.Stage5) + sum(TOTALS.Warehouse) AS OIQIDtq
       , count(TOTALS.ORDERID) AS NO_OF_ORDERS
       , sum(TOTALS.Orders_Awaiting) AS ORDERS_AWAITING
       , sum(TOTALS.Orders_Stage1) AS ORDERS_STAGE1
       , sum(TOTALS.Orders_Stage2) AS ORDERS_STAGE2
       , sum(TOTALS.Orders_Stage3) AS ORDERS_STAGE3
       , sum(TOTALS.Orders_Stage4) AS ORDERS_STAGE4
       , sum(TOTALS.Orders_Stage5) AS ORDERS_STAGE5
       , sum(TOTALS.Orders_Warehouse) AS ORDERS_WAREHOUSE
FROM
(SELECT Mvemnt.Company AS CID
       , Mvemnt.ASSOC_CO AS ACID
       , Mvemnt.Division AS DID
       , Mvemnt.ORDER_ID AS ORDERID
       , (ORDER_QUANTITIES.ORDER_QTY - Mvemnt.INTO_STG1 - Mvemnt.REJ_BY_STG1 - Mvemnt.COR_IN_STG1 - Mvemnt.COR_REJ_STG1) AS Awaiting

       , (Mvemnt.INTO_STG1 + Mvemnt.REJ_BY_STG1 + Mvemnt.COR_IN_STG1 + Mvemnt.COR_REJ_STG1)
          - (Mvemnt.INTO_STG2 + Mvemnt.REJ_BY_STG2 + Mvemnt.COR_IN_STG2 + Mvemnt.COR_REJ_STG2) AS Stage1

       , (Mvemnt.INTO_STG2 + Mvemnt.REJ_BY_STG2 + Mvemnt.COR_IN_STG2 + Mvemnt.COR_REJ_STG2)
          + (Mvemnt.INTO_STG3 + Mvemnt.REJ_BY_STG3 + Mvemnt.COR_IN_STG3 + Mvemnt.COR_REJ_STG3) AS Stage2

       , (Mvemnt.INTO_STG3 + Mvemnt.REJ_BY_STG3 + Mvemnt.COR_IN_STG3 + Mvemnt.COR_REJ_STG3)
          + (Mvemnt.INTO_STG4 + Mvemnt.REJ_BY_STG4 + Mvemnt.COR_IN_STG4 + Mvemnt.COR_REJ_STG4) AS Stage3

       , (Mvemnt.INTO_STG4 + Mvemnt.REJ_BY_STG4 + Mvemnt.COR_IN_STG4 + Mvemnt.COR_REJ_STG4)
          + (Mvemnt.INTO_STG5 + Mvemnt.REJ_BY_STG5 + Mvemnt.COR_IN_STG5 + Mvemnt.COR_REJ_STG5) AS Stage4

       , (Mvemnt.INTO_STG5 + Mvemnt.REJ_BY_STG5 + Mvemnt.COR_IN_STG5 + Mvemnt.COR_REJ_STG5)
         + (Mvemnt.INTO_WHOUSE + Mvemnt.REJ_BY_WHOUSE + Mvemnt.COR_STG5_TO_WHOUSE + Mvemnt.COR_REJ_TO_WHOUSE) AS Stage5

       , (Mvemnt.INTO_WHOUSE + Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.FROM_CLIENT + Mvemnt.COR_STG5_TO_WHOUSE)
          + (Mvemnt.TO_CLIENT + Mvemnt.RETURNED_TO_SUPPLIER + Mvemnt.REJ_BY_WHOUSE + Mvemnt.COR_REJ_TO_WHOUSE) AS Warehouse

       , CASE WHEN (ORDER_QUANTITIES.ORDER_QTY - Mvemnt.INTO_STG1 - Mvemnt.REJ_BY_STG1 - Mvemnt.COR_IN_STG1 + Mvemnt.COR_REJ_STG1) > 0
              THEN 1 ELSE 0
         END AS Orders_Awaiting
       , CASE WHEN (Mvemnt.INTO_STG1 - Mvemnt.INTO_STG2 - Mvemnt.REJ_BY_STG2 + Mvemnt.REJ_BY_STG1 + Mvemnt.COR_IN_STG1 - Mvemnt.COR_REJ_STG1) > 0
              THEN 1 ELSE 0
         END AS Orders_Stage1
       , CASE WHEN (Mvemnt.INTO_STG2 - Mvemnt.INTO_STG3 - Mvemnt.REJ_BY_STG3 + Mvemnt.REJ_BY_STG2 + Mvemnt.COR_IN_STG2 - Mvemnt.COR_REJ_STG2) > 0
              THEN 1 ELSE 0
         END AS Orders_Stage2
       , CASE WHEN (Mvemnt.INTO_STG3 - Mvemnt.INTO_STG4 - Mvemnt.REJ_BY_STG4 + Mvemnt.REJ_BY_STG3 + Mvemnt.COR_IN_STG3 - Mvemnt.COR_REJ_STG3) > 0
              THEN 1 ELSE 0
         END AS Orders_Stage3
       , CASE WHEN (Mvemnt.INTO_STG4 - Mvemnt.INTO_STG5 - Mvemnt.REJ_BY_STG5 + Mvemnt.REJ_BY_STG4 + Mvemnt.COR_IN_STG4 - Mvemnt.COR_REJ_STG4) > 0
              THEN 1 ELSE 0
         END AS Orders_Stage4
       , CASE WHEN (Mvemnt.INTO_STG5 - Mvemnt.INTO_WHOUSE - Mvemnt.REJ_BY_WHOUSE + Mvemnt.REJ_BY_STG5 + Mvemnt.COR_IN_STG5 - Mvemnt.COR_REJ_STG5) > 0
              THEN 1 ELSE 0
         END AS Orders_Stage5
       , CASE WHEN (Mvemnt.INTO_WHOUSE + Mvemnt.TO_CLIENT - Mvemnt.FROM_CLIENT + Mvemnt.RETURNED_TO_SUPPLIER + Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.REJ_BY_WHOUSE
                     + Mvemnt.COR_STG5_TO_WHOUSE - Mvemnt.COR_REJ_TO_WHOUSE) > 0
              THEN 1 ELSE 0
         END AS Orders_Warehouse
FROM
(SELECT c.ID As Company
       , ac.ID AS ASSOC_CO
       , o.tDID AS Division
       , o.ID AS ORDER_ID
       , IF(Movement.ORIG_ORDER_QTY IS NULL,0,Movement.ORIG_ORDER_QTY) AS ORIG_ORDER_QTY
       , IF(sum(Movement.ORDER_PLACEMENT) IS NULL,0,sum(Movement.ORDER_PLACEMENT)) AS ORDER_PLACEMENT
       , IF(sum(Movement.INTO_STG1) IS NULL,0,sum(Movement.INTO_STG1)) AS INTO_STG1
       , IF(sum(Movement.INTO_STG2) IS NULL,0,sum(Movement.INTO_STG2)) AS INTO_STG2
       , IF(sum(Movement.REJ_BY_STG1) IS NULL,0,sum(Movement.REJ_BY_STG1)) AS REJ_BY_STG1
       , IF(sum(Movement.REJ_BY_STG2) IS NULL,0,sum(Movement.REJ_BY_STG2)) AS REJ_BY_STG2
       , IF(sum(Movement.COR_IN_STG1) IS NULL,0,sum(Movement.COR_IN_STG1)) AS COR_IN_STG1
       , IF(sum(Movement.COR_REJ_STG1) IS NULL,0,sum(Movement.COR_REJ_STG1)) AS COR_REJ_STG1
       , IF(sum(Movement.INTO_STG3) IS NULL,0,sum(Movement.INTO_STG3)) AS INTO_STG3
       , IF(sum(Movement.REJ_BY_STG3) IS NULL,0,sum(Movement.REJ_BY_STG3)) AS REJ_BY_STG3
       , IF(sum(Movement.COR_IN_STG2) IS NULL,0,sum(Movement.COR_IN_STG2)) AS COR_IN_STG2
       , IF(sum(Movement.COR_REJ_STG2) IS NULL,0,sum(Movement.COR_REJ_STG2)) AS COR_REJ_STG2
       , IF(sum(Movement.INTO_STG4) IS NULL,0,sum(Movement.INTO_STG4)) AS INTO_STG4
       , IF(sum(Movement.REJ_BY_STG4) IS NULL,0,sum(Movement.REJ_BY_STG4)) AS REJ_BY_STG4
       , IF(sum(Movement.COR_IN_STG3) IS NULL,0,sum(Movement.COR_IN_STG3))  AS COR_IN_STG3
       , IF(sum(Movement.COR_REJ_STG3) IS NULL,0,sum(Movement.COR_REJ_STG3)) AS COR_REJ_STG3
       , IF(sum(Movement.INTO_STG5) IS NULL,0,sum(Movement.INTO_STG5)) AS INTO_STG5
       , IF(sum(Movement.REJ_BY_STG5) IS NULL,0,sum(Movement.REJ_BY_STG5)) AS REJ_BY_STG5
       , IF(sum(Movement.COR_IN_STG4) IS NULL,0,sum(Movement.COR_IN_STG4)) AS COR_IN_STG4
       , IF(sum(Movement.COR_REJ_STG4) IS NULL,0,sum(Movement.COR_REJ_STG4)) AS COR_REJ_STG4
       , IF(sum(Movement.INTO_WHOUSE) IS NULL,0,sum(Movement.INTO_WHOUSE)) AS INTO_WHOUSE
       , IF(sum(Movement.REJ_BY_WHOUSE) IS NULL,0,sum(Movement.REJ_BY_WHOUSE)) AS REJ_BY_WHOUSE
       , IF(sum(Movement.COR_IN_STG5) IS NULL,0,sum(Movement.COR_IN_STG5)) AS COR_IN_STG5
       , IF(sum(Movement.COR_REJ_STG5) IS NULL,0,sum(Movement.COR_REJ_STG5)) AS COR_REJ_STG5
       , IF(sum(Movement.TO_CLIENT) IS NULL,0,sum(Movement.TO_CLIENT)) AS TO_CLIENT
       , IF(sum(Movement.FROM_CLIENT) IS NULL,0,sum(Movement.FROM_CLIENT)) AS FROM_CLIENT
       , IF(sum(Movement.RETURNED_TO_SUPPLIER) IS NULL,0,sum(Movement.RETURNED_TO_SUPPLIER)) AS RETURNED_TO_SUPPLIER
       , IF(sum(Movement.RECEIVED_FROM_SUPPLIER) IS NULL,0,sum(Movement.RECEIVED_FROM_SUPPLIER)) AS RECEIVED_FROM_SUPPLIER
       , IF(sum(Movement.COR_STG5_TO_WHOUSE) IS NULL,0,sum(Movement.COR_STG5_TO_WHOUSE)) AS COR_STG5_TO_WHOUSE
       , IF(sum(Movement.COR_REJ_TO_WHOUSE) IS NULL,0,sum(Movement.COR_REJ_TO_WHOUSE)) AS COR_REJ_TO_WHOUSE
FROM (
      SELECT opm.OPID AS Order_Placed_ID
           , CASE WHEN oimr.id =  1 THEN sum(opm.omQty) * oimr.numVal END AS ORIG_ORDER_QTY
           , CASE WHEN oimr.id =  2 THEN sum(opm.omQty) * oimr.numVal END AS ORDER_PLACEMENT
           , CASE WHEN oimr.id =  3 THEN sum(opm.omQty) * oimr.numVal END AS INTO_STG1
           , CASE WHEN oimr.id =  4 THEN sum(opm.omQty) * oimr.numVal END AS INTO_STG2
           , CASE WHEN oimr.id =  5 THEN sum(opm.omQty) * oimr.numVal END AS INTO_STG3
           , CASE WHEN oimr.id =  6 THEN sum(opm.omQty) * oimr.numVal END AS INTO_STG4
           , CASE WHEN oimr.id =  7 THEN sum(opm.omQty) * oimr.numVal END AS INTO_STG5
           , CASE WHEN oimr.id =  8 THEN sum(opm.omQty) * oimr.numVal END AS INTO_WHOUSE
-- oimr9 to oimr12 not required at present
           , CASE WHEN oimr.id = 13 THEN sum(opm.omQty) * oimr.numVal END AS REJ_BY_STG1
           , CASE WHEN oimr.id = 14 THEN sum(opm.omQty) * oimr.numVal END AS REJ_BY_STG2
           , CASE WHEN oimr.id = 15 THEN sum(opm.omQty) * oimr.numVal END AS REJ_BY_STG3
           , CASE WHEN oimr.id = 16 THEN sum(opm.omQty) * oimr.numVal END AS REJ_BY_STG4
           , CASE WHEN oimr.id = 17 THEN sum(opm.omQty) * oimr.numVal END AS REJ_BY_STG5
           , CASE WHEN oimr.id = 18 THEN sum(opm.omQty) * oimr.numVal END AS REJ_BY_WHOUSE
           , CASE WHEN oimr.id = 19 THEN sum(opm.omQty) * oimr.numVal END AS TO_CLIENT
           , CASE WHEN oimr.id = 20 THEN sum(opm.omQty) * oimr.numVal END AS FROM_CLIENT
           , CASE WHEN oimr.id = 21 THEN sum(opm.omQty) * oimr.numVal END AS RETURNED_TO_SUPPLIER
           , CASE WHEN oimr.id = 22 THEN sum(opm.omQty) * oimr.numVal END AS RECEIVED_FROM_SUPPLIER
           , CASE WHEN oimr.id = 23 THEN sum(opm.omQty) * oimr.numVal END AS COR_IN_STG1
           , CASE WHEN oimr.id = 24 THEN sum(opm.omQty) * oimr.numVal END AS COR_IN_STG2
           , CASE WHEN oimr.id = 25 THEN sum(opm.omQty) * oimr.numVal END AS COR_IN_STG3
           , CASE WHEN oimr.id = 26 THEN sum(opm.omQty) * oimr.numVal END AS COR_IN_STG4
           , CASE WHEN oimr.id = 27 THEN sum(opm.omQty) * oimr.numVal END AS COR_IN_STG5
           , CASE WHEN oimr.id = 28 THEN sum(opm.omQty) * oimr.numVal END AS COR_STG5_TO_WHOUSE
           , CASE WHEN oimr.id in (29,35) THEN sum(opm.omQty) * oimr.numVal END AS COR_REJ_STG1
           , CASE WHEN oimr.id in (30,36) THEN sum(opm.omQty) * oimr.numVal END AS COR_REJ_STG2
           , CASE WHEN oimr.id in (31,37) THEN sum(opm.omQty) * oimr.numVal END AS COR_REJ_STG3
           , CASE WHEN oimr.id in (32,38) THEN sum(opm.omQty) * oimr.numVal END AS COR_REJ_STG4
           , CASE WHEN oimr.id in (33,39) THEN sum(opm.omQty) * oimr.numVal END AS COR_REJ_STG5
           , CASE WHEN oimr.id in (34,40) THEN sum(opm.omQty) * oimr.numVal END AS COR_REJ_TO_WHOUSE
      FROM order_placed_move opm
      INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
      GROUP BY  opm.OPID, oimr.id
     ) Movement INNER JOIN order_item oi ON Movement.Order_Placed_ID = oi.ID
                INNER JOIN orders o ON oi.OID = o.ID
                INNER JOIN order_type ot ON o.OTID = ot.ID
                INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
                INNER JOIN order_item_del_date oidd ON oidd.OIID = oi.ID
                INNER JOIN division d ON o.tDID = d.ID
                INNER JOIN associate_companies ac ON ac.ID = d.ACID
                INNER JOIN company c ON ac.CID = c.ID
WHERE c.ID = ?
GROUP BY c.ID, ac.ID, o.tDID, o.ID
) Mvemnt INNER JOIN (SELECT oiq.OIID
                            , oiq.order_qty AS ORDER_QTY
                            , oi.ID AS ORDER_ITEM
                            , o.ID AS ORDER_ID
                     FROM order_item_qty oiq
                          INNER JOIN order_item oi ON oiq.OIID = oi.ID
                          INNER JOIN orders o ON oi.OID = o.ID
                    ) ORDER_QUANTITIES ON ORDER_QUANTITIES.ORDER_ID = Mvemnt.ORDER_ID
WHERE Mvemnt.Company = 1
GROUP BY Mvemnt.Company, Mvemnt.ASSOC_CO, Mvemnt.Division, Mvemnt.ORDER_ID
) TOTALS
;
 ;";

  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-RFLS</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $OC, $CID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $sort == 'DESC' ? $sort == 'ASC' : $sort == 'DESC';
    while($row = mysqli_fetch_assoc($result)){
      $OTID    = $row['OTID'];
      $OIDon   = $row['Order_ID'];
      $OIIDon  = $row['Item_ID'];
      $OIIDtir   = $row['OREF'];
      $OIQIDq = $row['OQTY'];
      $ACID = $row['ACID'];
      $ACIDn     = $row['Assoc_Co'];
      $ACIDp = $row['ACPID'];
      $ACIDpn = $row['PAssoc_Co'];
      $OTID = $row['OTID'];

      $B1 = $row['Cutting_Room_1'];
      $B2 = $row['Stage_1_2'];
      $B3 = $row['Stage_2_2'];
      $B4 = $row['Stage_3_2'];
      $B5 = $row['Stage_4_2'];
      $B6 = $row['Stage_5_2'];
      // $B3 = $row['Cutting_Room_3'];

      ?>
      <tr>
        <th style="height:20%; font-size: 150%; text-align:left; text-indent:3%;">Associate</th>
      </tr>
      <tr>
        <th style="height:5%; background-color:#f7cc6c;">Sales</th>
      </tr>
      <tr style=" background-color:#ffc2c2;">
        <th style="text-align:left; text-indent:8%; border-right:thin solid black;">Samples</th>
        <td style="border-right:thin solid black">count</td>
        <td>1-1</td>
        <td>1-2</td>
        <td style="border-right:thin solid black">1-3</td>
        <td>1-4</td>
        <td>1-5</td>
        <td style="border-right:thin solid black">1-6</td>
        <td>1-7</td>
        <td>1-8</td>
        <td style="border-right:thin solid black">1-9</td>
        <td>1-10</td>
        <td>1-11</td>
        <td style="border-right:thin solid black">1-12</td>
        <td>1-13</td>
        <td>1-14</td>
        <td style="border-right:thin solid black">1-15</td>
        <td>1-16</td>
        <td>1-17</td>
        <td>1-18</td>
      </tr>
      <tr>
        <th style="text-align:left; text-indent:8%; border-right:thin solid black;">Production</th>
        <td style="border-right:thin solid black">count</td>
        <td>2-1</td>
        <td>2-2</td>
        <td style="border-right:thin solid black">2-3</td>
        <td>2-4</td>
        <td>2-5</td>
        <td style="border-right:thin solid black">2-6</td>
        <td>2-7</td>
        <td>2-8</td>
        <td style="border-right:thin solid black">2-9</td>
        <td>2-10</td>
        <td>2-11</td>
        <td style="border-right:thin solid black">2-12</td>
        <td>2-13</td>
        <td>2-14</td>
        <td style="border-right:thin solid black">2-15</td>
        <td>2-16</td>
        <td>2-17</td>
        <td>2-18</td>
      <tr>
        <th style="height:5%; background-color:#bbbbff;">Purchases</th>
      </tr>
      <tr style=" background-color:#ffc2c2;">
        <th style="text-align:left; text-indent:8%; border-right:thin solid black;">Samples</th>
        <td style="border-right:thin solid black">count</td>
        <td>3-1</td>
        <td>3-2</td>
        <td style="border-right:thin solid black">3-3</td>
        <td>3-4</td>
        <td>3-5</td>
        <td style="border-right:thin solid black">3-6</td>
        <td>3-7</td>
        <td>3-8</td>
        <td style="border-right:thin solid black">3-9</td>
        <td>3-10</td>
        <td>3-11</td>
        <td style="border-right:thin solid black">3-12</td>
        <td>3-13</td>
        <td>3-14</td>
        <td style="border-right:thin solid black">3-15</td>
        <td>3-16</td>
        <td>3-17</td>
        <td>3-18</td>
      <tr>
        <th style="text-align:left; text-indent:8%; border-right:thin solid black;">Production</th>
        <td style="border-right:thin solid black">count</td>
        <td>4-1</td>
        <td>4-2</td>
        <td style="border-right:thin solid black">4-3</td>
        <td>4-4</td>
        <td>4-5</td>
        <td style="border-right:thin solid black">4-6</td>
        <td>4-7</td>
        <td>4-8</td>
        <td style="border-right:thin solid black">4-9</td>
        <td>4-10</td>
        <td>4-11</td>
        <td style="border-right:thin solid black">4-12</td>
        <td>4-13</td>
        <td>4-14</td>
        <td style="border-right:thin solid black">4-15</td>
        <td>4-16</td>
        <td>4-17</td>
        <td>4-18</td>
      <tr>
        <th style="height:10%; font-size: 150%; text-align:left; text-indent:3%;">Partner</th>
        <td>count</td>
      </tr>
      <tr>
        <th style="height:5%; background-color:#f7cc6c;" >Sales</th>
      </tr>
      <tr style=" background-color:#ffc2c2;">
        <th style="text-align:left; text-indent:8%; border-right:thin solid black;">Samples</th>
        <td style="border-right:thin solid black">count</td>
        <td>5-1</td>
        <td>5-2</td>
        <td style="border-right:thin solid black">5-3</td>
        <td>5-4</td>
        <td>5-5</td>
        <td style="border-right:thin solid black">5-6</td>
        <td>5-7</td>
        <td>5-8</td>
        <td style="border-right:thin solid black">5-9</td>
        <td>5-10</td>
        <td>5-11</td>
        <td style="border-right:thin solid black">5-12</td>
        <td>5-13</td>
        <td>5-14</td>
        <td style="border-right:thin solid black">5-15</td>
        <td>5-16</td>
        <td>5-17</td>
        <td>5-18</td>
      </tr>
      <tr>
        <th style="text-align:left; text-indent:8%; border-right:thin solid black;">Production</th>
        <td style="border-right:thin solid black">count</td>
        <td>6-1</td>
        <td>6-2</td>
        <td style="border-right:thin solid black">6-3</td>
        <td>6-4</td>
        <td>6-5</td>
        <td style="border-right:thin solid black">6-6</td>
        <td>6-7</td>
        <td>6-8</td>
        <td style="border-right:thin solid black">6-9</td>
        <td>6-10</td>
        <td>6-11</td>
        <td style="border-right:thin solid black">6-12</td>
        <td>6-13</td>
        <td>6-14</td>
        <td style="border-right:thin solid black">6-15</td>
        <td>6-16</td>
        <td>6-17</td>
        <td>6-18</td>
      </tr>
      <tr>
        <th style="height:5%; background-color:#bbbbff;">Purchases</th>
      </tr>
      <tr style=" background-color:#ffc2c2;">
        <th style="text-align:left; text-indent:8%; border-right:thin solid black;">Samples</th>
        <td style="border-right:thin solid black">count</td>
        <td>7-1</td>
        <td>7-2</td>
        <td style="border-right:thin solid black">7-3</td>
        <td>7-4</td>
        <td>7-5</td>
        <td style="border-right:thin solid black">7-6</td>
        <td>7-7</td>
        <td>7-8</td>
        <td style="border-right:thin solid black">7-9</td>
        <td>7-10</td>
        <td>7-11</td>
        <td style="border-right:thin solid black">7-12</td>
        <td>7-13</td>
        <td>7-14</td>
        <td style="border-right:thin solid black">7-15</td>
        <td>7-16</td>
        <td>7-17</td>
        <td>7-18</td>
      </tr>
      <tr>
        <th style="text-align:left; text-indent:8%; border-right:thin solid black;">Production</th>
        <td style="border-right:thin solid black">count</td>
        <td>8-1</td>
        <td>8-2</td>
        <td style="border-right:thin solid black">8-3</td>
        <td>8-4</td>
        <td>8-5</td>
        <td style="border-right:thin solid black">8-6</td>
        <td>8-7</td>
        <td>8-8</td>
        <td style="border-right:thin solid black">8-9</td>
        <td>8-10</td>
        <td>8-11</td>
        <td style="border-right:thin solid black">8-12</td>
        <td>8-13</td>
        <td>8-14</td>
        <td style="border-right:thin solid black">8-15</td>
        <td>8-16</td>
        <td>8-17</td>
        <td>8-18</td>
      </tr>
    <?php
  //   }
  // }
    ?>
  </table>

  <div style="position:absolute; bottom:5%; left:10%;">

    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a href='?H&$sp&p=$x'>  $x  </a>";
}}}

    ?>
  </div>
