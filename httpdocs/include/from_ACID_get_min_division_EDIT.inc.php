<?php
// echo "<b>incluDe/from_ACID_get_min_division_EDIT.inc.php</b>";

$sql = "SELECT MIN(d.ID) as DID
          , d.name as mDIDn
        FROM division d
          , associate_companies ac
          , company c
        WHERE ac.ID = ?
        AND d.ACID = ac.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $ACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $mDID = $row['DID'];
}

// get the min section for that division
$sql = "SELECT MIN(s.ID) as SCID
        FROM section s
          , division d
        WHERE d.ID = ?
        AND s.DID = d.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-ffDgms</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $mDID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $mSID = $row['SCID'];
}
