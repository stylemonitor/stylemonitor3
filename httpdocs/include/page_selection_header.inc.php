<?php
// echo "include/page_selection_header.inc.php";
include 'set_urlPage.inc.php';

include 'from_CID_count_partner_clients.inc.php';    // get the admin user of the company $cpCPID
include 'from_CID_count_partner_suppliers.inc.php';    // get the admin user of the company $cpCPID
include 'from_UID_get_user_details.inc.php';    // get the admin user of the company $cpCPID
include 'SM_colours.inc.php';

$CID = $_SESSION['CID'];

if (isset($_GET['tab'])) {  $tab = $_GET['tab'];}

if (isset($_GET['ot'])) {  $OTID = $_GET['ot'];}

if ($itemPref <> 1) {
    $itemPref = $itemPref;
}

if ($clientPref <> 1) {
    $itemPref = $clientPref;
}

if (isset($_GET['ot'])) {
  $ot = $_GET['ot'];
  if ($ot == 1) {
    $otbc = '#f7cc6c';
  }elseif ($ot == 2) {
    $otbc = '#bbbbff';
  }elseif ($ot == 3) {
    $otbc = '#d68e79';
  }elseif ($ot == 4) {
    $otbc = '#c9d1ce';
  }
}else {
  if (($clientPref == 1) && ($salesPref == 1)) {
    $ot = 1;
    $otbc = '#f7cc6c';
  }elseif (($clientPref == 1) && ($salesPref == 2)) {
    $ot = 2;
    $otbc = '#bbbbff';
  }elseif (($clientPref == 2) && ($salesPref == 1)) {
    $ot = 3;
    $otbc = '#d68e79';
  }elseif (($clientPref == 2) && ($salesPref == 2)) {
    $ot = 4;
    $otbc = '#c9d1ce';
  }
}

if (isset($_GET['sp'])) {
  $sp = $_GET['sp'];
  if ($sp == 1) {
    $spbc = "#fa7b7b";
  }elseif ($sp == 2) {
    $spbc ="#76f8bc";
  }
}else {
  if ($itemPref == 1) {
    $sp = 1;
    $spbc = "#fa7b7b";
  }else {
    $sp = 2;
    $spbc ="#76f8bc";
  }
}

include 'sql/page_selection_header.sql.php';

$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)){
  echo 'FAIL-fdcs';
}else{
  mysqli_stmt_bind_param($stmt, "sssss", $CID, $CID, $CID, $OTID, $tab);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $OTID = $row['OTID'];
    $sp = $row['SAM_PROD'];
    $tab = $row['STATUS'];
    $QTY = $row['QTY'];
    $cOIID = $row['COUNT_OIID'];
    $cOID = $row['COUNT_OID'];

    ?>
    <div style="position:absolute; top:8.5%; height:2.9%; left:7%; width:20%; background-color: <?php echo $salAssCol ?>; border:thin solid black;">Associate Salesddd</div>
    <div style="position:absolute; top:8.5%; height:2.9%; left:29%; width:20%; background-color: <?php echo $purAssCol ?>; border:thin solid black;">Associate Purchases</div>
    <div style="position:absolute; top:8.5%; height:2.9%; left:51%; width:20%; background-color: <?php echo $salPrtCol ?>; border:thin solid black;">Partner Sales</div>
    <div style="position:absolute; top:8.5%; height:2.9%; left:73%; width:20%; background-color: <?php echo $purPrtCol ?>; border:thin solid black;">Partner Purchases</div>
    <?php

    if ($tab == 1) {
      // OVERDUE SAMPLES
      if (($tab == 1) && ($OTID == 1) && ($sp == 1)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height: 2.9%; left:7%;  width:10%; border:thin solid black;border:thin solid black; text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }

      // PRODUCTION
      if (($tab == 1) && ($OTID == 1) && ($sp == 2)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:17.1%; width:9.9%; border:thin solid black;border:thin solid black; text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }

      // SAMPLES
      if (($tab == 1) && ($OTID == 2) && ($sp == 1)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:29%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }

      // PRODUCTION
      if (($tab == 1) && ($OTID == 2) && ($sp == 2)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:39.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }

      // SAMPLES
      if (($tab == 1) && ($OTID == 3) && ($sp == 1)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:51%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }

      // PRODUCTION
      if (($tab == 1) && ($OTID == 3) && ($sp == 2)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:61.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }

      // SAMPLES
      if (($tab == 1) && ($OTID == 4) && ($sp == 1)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:73%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
      // PRODUCTION
      if (($tab == 1) && ($OTID == 4) && ($sp == 2)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:83.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
    }elseif ($tab == 2) {
      if (($tab == 2) && ($OTID == 1) && ($sp == 1)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:7%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }

      if (($tab == 2) && ($OTID == 1) && ($sp == 2)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:17.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }

      if (($tab == 2) && ($OTID == 2) && ($sp == 1)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:29%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }

      if (($tab == 2) && ($OTID == 2) && ($sp == 2)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:39.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }

      if (($tab == 2) && ($OTID == 3) && ($sp == 1)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:51%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }

      if (($tab == 2) && ($OTID == 3) && ($sp == 2)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:61.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }

      if (($tab == 2) && ($OTID == 4) && ($sp == 1)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:73%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }

      if (($tab == 2) && ($OTID == 4) && ($sp == 2)) {
        ?>
        <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:83.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
          <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
    }
  elseif ($tab == 3) {
    if (($tab == 3) && ($OTID == 1) && ($sp == 1)) {
      ?>
      <div class="select" class="select" style="position:absolute; top:11.5%; height:2.9%; left:7%;  width:10%; border:thin solid black;border:thin solid black; text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 3) && ($OTID == 1) && ($sp == 2)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:17.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 3) && ($OTID == 2) && ($sp == 1)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:29%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 3) && ($OTID == 2) && ($sp == 2)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:39.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 3) && ($OTID == 3) && ($sp == 1)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:51%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 3) && ($OTID == 3) && ($sp == 2)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:61.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 3) && ($OTID == 4) && ($sp == 1)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:73%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 3) && ($OTID == 4) && ($sp == 2)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:83.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
  }elseif ($tab == 4) {
    if (($tab == 4) && ($OTID == 1) && ($sp == 1)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:7%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 4) && ($OTID == 1) && ($sp == 2)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:17.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 4) && ($OTID == 2) && ($sp == 1)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:29%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 4) && ($OTID == 2) && ($sp == 2)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:39.1%; width:9.9%; border:thin solid black;text-align:center; z-index: 1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 4) && ($OTID == 3) && ($sp == 1)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:51%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 4) && ($OTID == 3) && ($sp == 2)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:61.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 4) && ($OTID == 4) && ($sp == 1)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:73%;  width:10%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    if (($tab == 4) && ($OTID == 4) && ($sp == 2)) {
      ?>
      <div class="select" style="position:absolute; top:11.5%; height:2.9%; left:83.1%; width:9.9%; border:thin solid black;text-align:center; z-index:1;">
        <a class="hreflink" href="<?php echo "home.php?H&rt1&tab=$tab&ot=$OTID&sp=$sp" ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
    }
  }
}
