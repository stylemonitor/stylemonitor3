<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_count_order_complete.inc.php</b>";
$CID = $_SESSION['CID'];
include 'include/from_CID_get_min_licence_start_date.inc.php';
$td = date('U');
// $OC = 1;
// $sd = $mCIDstd;
// $ed = $td + 31536000;
// $dr = 'oidd.item_del_date';

// echo "count_orders - CID : $CID";
// echo "<br>count_orders - Order type : $OTID";
// echo "<br>count_orders - Order status : $OC";
// echo "<br>count_orders - today : $td";
// echo "<br>count_orders - start day : $sd";
// echo "<br>count_orders - end day : $ed";
// echo "<br>count_orders - due date source : $dr";

$sql = "SELECT COUNT(o.ID) as cOID
        FROM  company_partnerships cp
          , orders o
          , order_item oi
          , order_item_del_date oidd
        WHERE ((cp.req_CID = ?) OR (cp.acc_CID = ?))
        AND o.CPID = cp.ID
        AND oi.OID = o.ID
        AND oidd.OIID = oi.ID
        AND o.orComp = 1
        -- AND $dr BETWEEN $sd AND $ed
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fcgocc1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOIDcom = $row['cOID'];
  global $cOIDcom;
}
