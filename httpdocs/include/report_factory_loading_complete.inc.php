<?php
session_start();
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
include 'set_urlPage.inc.php';
// include 'page_selection_complete.inc.php';
// echo "include/report_factory_loading_sum.inc.php";

$CID = $_SESSION['CID'];
$td = date('U');

include 'from_CID_count_section_totals.inc.php';
include 'from_CID_count_order_overdue.inc.php';
include 'from_CID_count_order_thisweek.inc.php';
include 'from_CID_count_order_nextweek.inc.php';
include 'from_CID_count_order_later.inc.php';
include 'from_CID_count_order_new.inc.php'; // $cOIDnew
include 'from_CID_count_orders_in_work.inc.php'; // $cOIDiw
include 'from_UID_get_last_logout.inc.php'; // $LRIDout

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'Completed Orders Registry';
$pddhbc = '#fffddd';
include 'page_description_date_header.inc.php';
include 'SM_colours.inc.php';

// Monitoring Point Colour
// $mpCol = '#ffffd1';
// $whsCol = '#76ffb5';
// $sentCol = '#aff3ff';

if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  $sp = 0;
  // $OC = 0;
  $sd = $td - 31536000;
  $ed = $td + 31536000;
  $dr = 'oidd.item_del_date';
  $cp = "open order items";
  $hc = 'black';
  $hbc = '#c4e4f0';
}

include 'order_QUERY_count_complete_item.inc.php';

$rpp = 25;

// Check the URL to see what page we are on
if (isset($_GET['pa'])) {
  $pa = $_GET['pa'];
}else{
  $pa = 0;
}

// Set the start point for each page
if ($pa > 1) {
  $start = ($pa * $rpp) - $rpp;
}else {
  $start = 0;
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
// $cTOTAL is the count of the total items able to be viewed - cannot be changed
$PTV = ($cTOTAL / $rpp);

// The number of pages to be viewed
$PTV = ceil($PTV)-1;


if(isset($_GET['ord'])){ $ord = $_GET['ord']; }else{ $ord = 'oidd.item_del_date'; }
if(isset($_GET['sort'])){ $sort = $_GET['sort']; }else{ $sort = 'ASC'; }

// echo "<br>Company ID : $CID";
// echo "<br>Order status : $OC";
// echo "<br>Today : $td";
// echo "<br>Start day : $sd";
// echo "<br>End day : $ed";
// echo "<br>Count_orders - due date source : $dr";

if ($cTOTAL < 1) {
  // echo "<br>NO orders placed";
  header("Location:../home.php?H");
  exit();

}else {
  ?>
  <table class="trs" style="position:relative; top:15%;">
    <tr>
      <th style="width:17%; text-align:left; text-indent:5%;">Company</th>
      <th style="width:5%;">Order</th>
      <th style="width:2%;">Item</th>
      <th style="width:22%; text-align:left; text-indent:5%;">OUR Order Ref</th>
      <th style="width:8%;">Short Code</th>
      <th style="width:4%; text-align: center;">Qty</th>
      <!-- <th style="width:4%; background-color:<?php echo $mpCol ?>; text-align: center;"><a style="text-decoration:none;" href="<?php echo "loading.php?L&tab=1" ?>">Awt</a></th>
      <th style="width:4%; background-color:<?php echo $mpCol ?>; text-align: center;"><a style="text-decoration:none;" href="<?php echo "loading.php?L&tab=2" ?>">MP1</a></th>
      <th style="width:4%; background-color:<?php echo $mpCol ?>; text-align: center;"><a style="text-decoration:none;" href="<?php echo "loading.php?L&tab=3" ?>">MP2</a></th>
      <th style="width:4%; background-color:<?php echo $mpCol ?>; text-align: center;"><a style="text-decoration:none;" href="<?php echo "loading.php?L&tab=4" ?>">MP3</a></th>
      <th style="width:4%; background-color:<?php echo $mpCol ?>; text-align: center;"><a style="text-decoration:none;" href="<?php echo "loading.php?L&tab=5" ?>">MP4</a></th>
      <th style="width:4%; background-color:<?php echo $mpCol ?>; text-align: center;"><a style="text-decoration:none;" href="<?php echo "loading.php?L&tab=6" ?>">MP5</a></th>
      <th style="width:4%; background-color:<?php echo $whsCol ?>; text-align: center;"><a style="text-decoration:none;" href="<?php echo "warehouse.php?W&tab=0" ?>">Whs</a></th> -->
      <th style="width:4%; background-color:<?php echo $sentCol ?>; text-align: center;">Sent</th>
      <th style="width:10%; text-align:center;">Due Date</th>
    </tr>
    <?php
  include 'order_QUERY_complete_item.inc.php';
}

// TO BE ADDED to the bottom of each report and adjusted accordingly
?>
<div style="position:absolute; top:80%; right:5%; font-size:150%;">
  <?php
  for ($x = 1 ; $x <= $PTV ; $x++){
    echo "<a style='color:black; text-decoration:none;' href='$urlPage&tab=$tab&pa=$x'>  $x  </a>";
  }
  ?>
</div>
