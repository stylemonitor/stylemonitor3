<?php
include 'dbconnect.inc.php';
// include 'from_CID_select_prod_type.inc.php';
//echo '<b>include/from_CID_select_prod_type.inc.php</b>';
// pt.CID replaced with pt.DIDchanged

$CID = $_SESSION['CID'];

$sql = "SELECT DISTINCT(pt.ID) as PTID
          , pt.name as PTIDn
          , pt.scode as PTIDsc
        FROM product_type pt
        	, associate_companies ac
          , company c
        WHERE c.ID = ?
        AND pt.CID = c.ID
        AND pt.name NOT IN ('Select Product Type')
        ORDER BY pt.name
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcspt</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($res)){
    $PTID = $row['PTID'];
    $PTIDn = $row['PTIDn'];
    $PTIDsc = $row['PTIDsc'];
    echo '<option value="'.$PTID.'">'.$PTIDsc.' : '.$PTIDn.'</option>';
    // echo '<option value="'.$PTID.'">'.$PTID.'/'.$PTIDn.' : '.$PTIDsc.'</option>';
    ?>
    <!-- <option value="<?php echo $PTID ?>"><?php echo $PTIDn ?></option> -->
    <?php
  }
}
?>
