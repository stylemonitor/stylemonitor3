<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_OIID_count_order_item_ALL.inc.php</b>";
$CID = $_SESSION['CID'];

// echo "The OID is $OID";
$sql = "SELECT COUNT(oi.ID) as cOIID
          , o.ID as OID
        FROM order_item oi
          , orders o
        WHERE o.ID = (SELECT o.ID
                      FROM orders o
                      , order_item oi
                      WHERE oi.ID = ?
                      AND oi.OID = o.ID)
        AND oi.OID = o.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br>FAIL-focoi';
}else {
  mysqli_stmt_bind_param($stmt, "s", $OIID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($res);
  $cOIIDall = $row['cOIID'];
  $OID      = $row['OID'];
}
