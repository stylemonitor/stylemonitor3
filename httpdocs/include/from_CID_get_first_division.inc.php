<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_get_first_division.inc.php</b>";

$sql = "SELECT d.ID as mDID
          , d.name as mDIDn
        FROM division d
          , associate_companies ac
          , company c
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        ORDER BY d.ID DESC
        LIMIT 1
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  // echo '<b>FAIL-fcgd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $mDID = $row['DID'];
  $mDIDn = $row['DIDn'];
}
