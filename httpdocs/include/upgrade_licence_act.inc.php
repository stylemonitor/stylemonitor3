<?php
session_start();
include 'dbconnect.inc.php';
// $UID = $_SESSION['UID'];
// $CID = $_SESSION['CID'];

// echo "<b>include/upgrade_licence_act.inc.php</b>";

if (!isset($_POST['month']) && !isset($_POST['year'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['month'])) {
  $licType = 12;
  $CID = $_POST['CID'];
  $UID = $_POST['UID'];
  $CIDn = $_POST['CIDn'];
  $SMIC = $_POST['SMIC'];
  $tcCode = $_POST['tcCode'];

  // Add the tcCode to the company table
  // $sql

  // echo "<br>Monthly payment: CIDn = $CIDn; SMIC = $SMIC: Licence = $licType";
  include "email_upgrade_licence.inc.php";
  // echo "email sent";
  header("Location:../company.php?C&udt");
  exit();
}elseif (isset($_POST['year'])) {
  $licType = 1;
  $CID = $_POST['CID'];
  $UID = $_POST['UID'];
  $CIDn = $_POST['CIDn'];
  $SMIC = $_POST['SMIC'];
  $tcCode = $_POST['tcCode'];
  // echo "<br>Yearly payment: CIDn = $CIDn; SMIC = $SMIC: Licence = $licType";
  include "email_upgrade_licence.inc.php";
  // echo "email sent";
  header("Location:../company.php?C&coi&udt");
  exit();
}
