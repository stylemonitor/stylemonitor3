<?php
session_start();
include 'dbconnect.inc.php';
// echo "company_SETTINGS_act.inc.php";

$CID = $_SESSION['CID'];
$UID = $_SESSION['UID'];

// if (!isset($_POST['saveLT']) && !isset($_POST['savesLT'])) {
if (!isset($_POST['savesLT'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}else {
  // echo "GO TO WORK";
  // LTime = lead time
  // sLtime = sample lead time

  $TID    = $_POST['TID'];
  $Ltime  = $_POST['Ltime'];
  $sLtime = $_POST['sLtime'];
  $ACID   = $_POST['ACID'];

  // echo "Associate company lead time set to Ltime : $Ltime";
  // echo "<br>TimeZone : $TID";


  /* Tell mysqli to throw an exception if an error occurs */
  mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

  // Start transaction
  // mysqli_begin_transaction($mysqli);
  // try {
    // to

    if(empty($TID)){
    }else {

      // set the COMPANY time zone NOT an individual users
      $sql = "UPDATE company
              SET TID = ?
              WHERE ID = ?
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo '<b>FAIL-csa</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ss", $TID, $CID);
        mysqli_stmt_execute($stmt);
      }

      // Check if the users TID is set
      $sql = "SELECT TID as setTID
              FROM users
              WHERE ID = ?
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo '<b>FAIL-csa1</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "s", $UID);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        $row = mysqli_fetch_array($result);
        $setTID = $row['setTID'];
      }

      if ($setTID == 205) {
        $sql = "UPDATE users
        SET TID = ?
        WHERE ID = ?
        ;";
        $stmt = mysqli_stmt_init($con);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
          echo '<b>FAIL-csa2</b>';
        }else{
          mysqli_stmt_bind_param($stmt, "ss", $TID, $UID);
          mysqli_stmt_execute($stmt);
        }
      }
    }
    if (empty($Ltime)) {
    }else {
      $sql = "UPDATE associate_companies
      SET acLtime = ?
      WHERE ID = ?
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo '<b>FAIL-csa3</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ss", $Ltime, $ACID);
        mysqli_stmt_execute($stmt);
      }
    }
    if (empty($sLtime)) {
    }else {
      $sql = "UPDATE associate_companies
      SET acsLtime = ?
      WHERE ID = ?
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo '<b>FAIL-csa4</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ss", $sLtime, $ACID);
        mysqli_stmt_execute($stmt);
      }
    }
  // } catch (mysqli_sql_exception $exception) {
  //   mysqli_rollback($mysqli);
  //
  //   throw $exception;
  // }
  header("Location:../company.php?C&stt");
  exit();
}
