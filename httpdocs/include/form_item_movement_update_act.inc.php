<?php
session_start();
include 'dbconnect.inc.php';
echo '<br><b>include/form_item_movement_update_act.inc.php</b>';

include 'set_urlPage.inc.php';
$UID = $_SESSION['UID'];
$td = date('U');


if (!isset($_POST['move']) && !isset($_POST['canMove'])) {
  // echo "Incorrect access method";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['canMove'])) {
  // echo "Cancel movement";
  $urlPage = $_POST['url'];
  header("Location:../$urlPage");
  exit();
}elseif (isset($_POST['move'])) {
  // echo "Get the information needed";


  if (isset($_POST['OIMRID'])) {
    $mtv = $_POST['OIMRID'];
    if ($mtv == 3) {
      $mty = 'Awt';
      $sect = 'Awt';
    }elseif ($mtv == 4) {
      $mty = 'm1p';
      $sect = 'MP1';
    }elseif ($mtv == 5) {
      $mty = 'm2p';
      $sect = 'MP2';
    }elseif ($mtv == 6) {
      $mty = 'm3p';
      $sect = 'MP3';
    }elseif ($mtv == 7) {
      $mty = 'm4p';
      $sect = 'MP4';
    }elseif ($mtv == 8) {
      $mty = 'm5p';
      $sect = 'MP5';
    }elseif ($mtv == 9) {
      $mty = 'Whs';
      $sect = 'Whs';
    }
  }

  $rtnPage = $_POST['url'];
  $OIID = $_POST['OIID'];
  $OID = $_POST['OID'];
  $OPID = $_POST['OPID'];
  $OIQIDq = $_POST['OIQIDq'];
  $val = $_POST['val'];
  $rej = $_POST['rej'];
  $mp = $_POST['mp'];
  $OIMRID = $_POST['OIMRID'];
  $movQty = $_POST['movQty'];
  $comts = $_POST['comm'];
  $moveType = $_POST['moveType'];

  // gets the amount of work in the next section
  include 'from_mp_get_reject_total.inc.php';

  if (empty($movQty)) {
    $movQty = $val;
  }else {
    $movQty = $movQty;
  }

  // if (empty($moveType)) {
  //   $moveType = 1;
  // }else {
  //   $moveType = $moveType;
  // }

  // echo "oimrid=$OIMRID<BR>";

  if ($movQty > $val) {
    // echo "CANNOT MOVE INSUFFICIENT QUANTITY IN SECTION";
    // include 'form_item_insufficeint.inc.php';

    header("Location:../$urlPage&OI=$OIID&ot=$ot&sp=$sp&mp=$mp&val=$val&ins");
    exit();
  }

  if (empty($moveType)) {
    $moveType = 1;
  }
  echo "<br>Return page (rtnPage) = $rtnPage";
  echo "<br>Order (OID) = $OID";
  echo "<br>Order Item (OIID) = $OIID";
  echo "<br>Order Item ID quantity (OIQIDq) = $OIQIDq";
  echo "<br>Move qty (movQty) = $movQty";
  echo "<br>Order Place ID (OPID) = $OPID";
  echo "<br>Available qty (val) = $val";
  echo "<br>Move comment (comts) = $comts";
  echo "<br>MoveType value (moveType) = $moveType";
  echo "<br>Movement reason (OIMRID) = $OIMRID";
  echo "<br>Todays date is $td";
}

//
// // Rejection of work right to left
if ($moveType == 1) {
  if (empty($comts)) {
    $comts = 'PASSED WORK from '.$sect.'';
  }else {
    $comts = $comts;
    // $comts = 'iuytyui';
  }
  if ($moveType == 9) {
    $comts = 'DESPATCHED TO CLIENT';
  }
  // if ($rej == 1) {
  if ($OIMRID == 3) {$OIMRID = 3;}
  elseif ($OIMRID == 4) {$OIMRID = 4;}
  elseif ($OIMRID == 5) {$OIMRID = 5;}
  elseif ($OIMRID == 6) {$OIMRID = 6;}
  elseif ($OIMRID == 7) {$OIMRID = 7;}
  elseif ($OIMRID == 8) {$OIMRID = 8;}
  elseif ($OIMRID == 19) {$OIMRID = 19;}
}

// Rejection of work right to left
if ($moveType == 2) {
  if (empty($comts)) {
    $comts = 'REJECTED WORK from '.$sect.'';
  }else {
    $comts = $comts;
  }
  // if ($rej == 1) {
  // if ($OIMRID == 3) {$OIMRID = 13;}
  if ($OIMRID == 4) {$OIMRID = 13;}
  elseif ($OIMRID == 5) {$OIMRID = 14;}
  elseif ($OIMRID == 6) {$OIMRID = 15;}
  elseif ($OIMRID == 7) {$OIMRID = 16;}
  elseif ($OIMRID == 8) {$OIMRID = 17;}
  elseif ($OIMRID == 19) {$OIMRID = 20;}
}

// Correction of rejection of work left to right
if ($moveType == 3) {
  if (empty($comts)) {
    $comts = 'Overcount count of PASSED WORK from '.$sect.'';
  }else {
    $comts = $comts;
  }
  // if (($corr == 1) && ($rej == 1)) {
  if ($OIMRID == 4) {$OIMRID = 29;}
  elseif ($OIMRID == 5) {$OIMRID = 30;}
  elseif ($OIMRID == 6) {$OIMRID = 31;}
  elseif ($OIMRID == 7) {$OIMRID = 32;}
  elseif ($OIMRID == 8) {$OIMRID = 33;}
  elseif ($OIMRID == 19) {$OIMRID = 34;}
  // need warehouse to 5
}

// Correction of rejection of work left to right
if ($moveType == 4) {
  if (empty($comts)) {
    $comts = 'Over count of rejects "returned" from '.$sect.'';
  }else {
    $comts = $comts;
  }
  // if (($corr == 1) && ($rej == 1)) {
  if ($OIMRID == 3) {$OIMRID = 23;}
  elseif ($OIMRID == 4) {$OIMRID = 24;}
  elseif ($OIMRID == 5) {$OIMRID = 25;}
  elseif ($OIMRID == 6) {$OIMRID = 26;}
  elseif ($OIMRID == 7) {$OIMRID = 27;}
  elseif ($OIMRID == 8) {$OIMRID = 28;}
  elseif ($OIMRID == 19) {$OIMRID = 35;}
  // need warehouse to 5
}

// echo "<br>Revised OIMRID ID value $OIMRID";
// echo "<br>UID is $UID";

if (!is_numeric($movQty) || $movQty < 1) {
  // echo "The value is NOT a number";
  header("location: ../orders.php?O&orf&oi=$OIID&v");
  exit();
}

// WANT TO ADD A CHECK THAT IF THE NUMBER BEING MOVED FROM ONE SECTION
// IF the qty is GREATER than the qty in the section
// show a screen asking to confirm the movement
// NEED the section totals first

// echo "$OPID/$OIMRID:$movQty::$rejVal";

if (($OIMRID == 23) || ($OIMRID == 24) || ($OIMRID == 25) || ($OIMRID == 26) || ($OIMRID == 27) || ($OIMRID == 28) && ($movQty > $rejVal)) {
  // echo "Correction of reject count";
  // if ($movQty > $rejVal) {
    echo "CANNOT MOVE THIS AMOUNT";
  // }
}else {

  echo "OPID:$OPID, OIMRID:$OIMRID, UID:$UID, movQty:$movQty, comts:$comts, td:$td";

  // Start transaction;
  // mysqli_begin_transaction($mysqli);
  // try {

    $sql = "INSERT INTO order_placed_move
              (OPID, OIMRID, UID, omQty, type, inputtime)
            VALUES
              (?,?,?,?,?,?)";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-fnoma1</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssssss", $OPID, $OIMRID, $UID, $movQty, $comts, $td);
      mysqli_stmt_execute($stmt);
    }

    // what stock levels at warehouse/client/supplier
    $sql = "SELECT DISTINCT
        -- SQL is form_item_movement_update_act.sql
             Mvemnt.ORDER_ID AS OID
             , Mvemnt.O_ITEM_ID AS OIID
             -- , Mvemnt.ordQty AS OIQIDq
             , Mvemnt.ordQty + SUM(Mvemnt.ORDER_INCR - Mvemnt.ORDER_DECR) AS NEW_OIQIDq
        -- CALCULATE INDIVIDUAL GARMENT MOVEMENT VALUES FOR EACH SECTION
             , SUM(Mvemnt.INTO_WHOUSE + Mvemnt.FROM_CLIENT + Mvemnt.COR_IN_WHOUSE + Mvemnt.CORR_OUT_CLIENT - Mvemnt.REJ_BY_WHOUSE - Mvemnt.TO_CLIENT
                   - Mvemnt.COR_OUT_WHOUSE - Mvemnt.CORR_IN_CLIENT) AS Warehouse
             , SUM(Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT) AS CLIENT
             , SUM(Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.RECEIVED_FROM_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER) AS SUPPLIER
             , SUM(Mvemnt.TO_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT + Mvemnt.CORR_IN_CLIENT + Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.RECEIVED_FROM_SUPPLIER
                   - Mvemnt.CORR_OUT_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER) AS SENT
             , IF(SUM(Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT) + SUM(Mvemnt.INTO_WHOUSE + Mvemnt.FROM_CLIENT
                      + Mvemnt.COR_IN_WHOUSE + Mvemnt.CORR_OUT_CLIENT - Mvemnt.REJ_BY_WHOUSE - Mvemnt.TO_CLIENT - Mvemnt.COR_OUT_WHOUSE - Mvemnt.CORR_IN_CLIENT) + SUM(Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.RECEIVED_FROM_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER) = Mvemnt.ordQty + SUM(Mvemnt.ORDER_INCR - Mvemnt.ORDER_DECR), 7, 0) AS OI_STATUS
        FROM (
            SELECT DISTINCT oimr.ID AS OIMR_ID
                 , o.ID AS ORDER_ID
                 , oi.ID AS O_ITEM_ID
                 , oiq.order_qty AS ordQty
                 , oi.itComp AS O_ITEM_COMP
                 , IF((CASE WHEN oimr.ID =  8 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  8 THEN SUM(opm.omQty) END)) AS INTO_WHOUSE
                 , IF((CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END)) AS ORDER_INCR
                 , IF((CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END)) AS ORDER_DECR
                 , IF((CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END)) AS REJ_BY_WHOUSE
                 , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
                 , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
                 , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
                 , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
                 , IF((CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END)) AS COR_IN_WHOUSE
                 , IF((CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END)) AS COR_OUT_WHOUSE
                 , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
                 , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
                 , IF((CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END)) AS CORR_OUT_SUPPLIER
                 , IF((CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END)) AS CORR_IN_SUPPLIER
            FROM order_placed_move opm
               INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
               INNER JOIN order_placed op ON opm.OPID = op.ID
               INNER JOIN order_item oi ON op.OIID = oi.ID
               INNER JOIN orders o ON oi.OID = o.ID
               INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
            WHERE oi.ID = ?
            GROUP BY oi.ID, oimr.ID
           ) Mvemnt
                INNER JOIN orders o ON Mvemnt.ORDER_ID = o.ID
        GROUP BY Mvemnt.O_ITEM_ID
        ORDER BY o.ID, Mvemnt.O_ITEM_ID ASC
          ;";
          $stmt = mysqli_stmt_init($con);
          if (!mysqli_stmt_prepare($stmt, $sql)) {
            echo '<b>FAIL-fnoma2</b>';
          }else{
            mysqli_stmt_bind_param($stmt, "s", $OIID);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            $row = mysqli_fetch_array($result);
            $OID = $row['OID'];
            $OIQIDq = $row['NEW_OIQIDq'];
            $Warehouse = $row['Warehouse'];
            $CLIENT = $row['CLIENT'];
            $SUPPLIER = $row['SUPPLIER'];
          }

          if ($CLIENT == 0) {
            $despatched = $SUPPLIER;
          }else {
            $despatched = $CLIENT;
          }

          echo "<br><b>AFTER form_item_movement_update_act.sql</b>
                <br>OIQIDq = $OIQIDq :: CLIENT= $CLIENT :: SUPPLIER = $SUPPLIER :: Warehouse = $Warehouse";

          // CHECK IF THE ITEM HAS BEEN COMPLETED
          // removed because orders with single items ALL in the warehouse were being lost!
          // if ($Warehouse > 0) {
          // // if ($OIQIDq == ($Warehouse + $despatched)) {
          //   // echo "order Qty = $OIQIDq :: In warehouse = $Warehouse + At Clients = $CLIENT";
          //   // update item to complete
          //   // 7 == items inthe warehouse
          //   $sql = "UPDATE order_item
          //           SET itComp = 7
          //             , itCompDate = ?
          //           WHERE ID = ?
          //   ;";
          //   $stmt = mysqli_stmt_init($con);
          //   if (!mysqli_stmt_prepare($stmt, $sql)) {
          //     echo '<b>FAIL-fnoma3</b>';
          //   }else{
          //     mysqli_stmt_bind_param($stmt, "ss", $td, $OIID);
          //     mysqli_stmt_execute($stmt);
          //   }
          // }elseif ($Warehouse == 0) {
            $sql = "UPDATE order_item
            SET itComp = 0
            , itCompDate = ?
            WHERE ID = ?
            ;";
            $stmt = mysqli_stmt_init($con);
            if (!mysqli_stmt_prepare($stmt, $sql)) {
              echo '<b>FAIL-fnoma3</b>';
            }else{
              mysqli_stmt_bind_param($stmt, "ss", $td, $OIID);
              mysqli_stmt_execute($stmt);
            }
          // }

          // CHECK IF THE ITEM HAS BEEN COMPLETELY SENT
          if ($OIQIDq == $despatched) {
            // update item to complete
            // 9==all items despatched from wharehouse to the customer
            $sql = "UPDATE order_item
                    SET itComp = 9
                      , itDesDate = ?
                    WHERE ID = ?
            ;";
            $stmt = mysqli_stmt_init($con);
            if (!mysqli_stmt_prepare($stmt, $sql)) {
              echo '<b>FAIL-fnoma4</b>';
            }else{
              mysqli_stmt_bind_param($stmt, "ss", $td, $OIID);
              mysqli_stmt_execute($stmt);
            }
          }else {
            // Nothing needs doing
          }

          // CHECK IF THE ALL ITEMS HAVE BEEN COMPLETED
          // count the number of items on the order
          $sql = "SELECT COUNT(oi.ID) as cOIID
                  FROM order_item oi
                    , orders o
                  WHERE o.ID = ?
                  AND oi.OID = o.ID
          ;";
          $stmt = mysqli_stmt_init($con);
          if (!mysqli_stmt_prepare($stmt, $sql)) {
            echo '<b>FAIL-fnoma5</b>';
          }else{
            mysqli_stmt_bind_param($stmt, "s", $OID);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            $row = mysqli_fetch_array($result);
            $cOIID = $row['cOIID'];
          }
          // echo "<br>Count items on order $cOIID";

          // count the number of items despatched on the order
          $sql = "SELECT COUNT(oi.itComp) as cOIIDdes
                  FROM order_item oi
                    , orders o
                  WHERE o.ID = ?
                  AND oi.itComp = 9
                  AND oi.OID = o.ID
          ;";
          $stmt = mysqli_stmt_init($con);
          if (!mysqli_stmt_prepare($stmt, $sql)) {
            echo '<b>FAIL-fnoma5</b>';
          }else{
            mysqli_stmt_bind_param($stmt, "s", $OID);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            $row = mysqli_fetch_array($result);
            $cOIIDdes = $row['cOIIDdes'];
          }
          // echo "<br>Count items despatched on order $cOIIDdes";

          if ($cOIID <> $cOIIDdes) {
            // echo "NOT ALL ITEMS HAVE BEEN DESPATCHED";
          }else{
            // echo "ALL ITEMS IN THE ORDER HAVE BEEN DESPATCHED";
            $sql = "UPDATE orders
                    SET orComp = 1
                      , orCompDate = ?
                    WHERE ID = ?
            ;";
            $stmt = mysqli_stmt_init($con);
            if (!mysqli_stmt_prepare($stmt, $sql)) {
              echo '<b>FAIL-fnoma6</b>';
            }else{
              mysqli_stmt_bind_param($stmt, "ss", $td, $OID);
              mysqli_stmt_execute($stmt);
            }
          }
        // }
        // catch (mysqli_sql_exception $exception) {
        //   mysqli_rollback($mysqli);
        //
        //   throw $exception;
        // }

        // if (isset($_GET['ot'])) {
        //   header("Location:../$urlPage");
        // }elseif (isset($OIID)) {
        //   header("Location:../$urlPage");
        // }elseif (isset($OID)) {
        //   header("Location:../$urlPage");
        // }else {
        header("Location:../$rtnPage");
        exit();
}
