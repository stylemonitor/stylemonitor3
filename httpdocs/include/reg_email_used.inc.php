<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/form_product_type_select_act.inc.php</b>";
if (isset($_GET['f'])) {
  $UIDf = $_GET['f'];
}
?>
<br>
<p>Hi <?php echo $UIDf ?>, sorry to say but the link from your email has already been used.</p>
<br>
<p>Please check your inbox again</p>
<br>
<p><b>OR</b></p>
<br>
<p>check you <b>SPAM</b> folder.</p>
<br>
<p>It might have gone there.</p>
<br><br>
<p>If you still cannot find it, send us a mail via the <b>Contact</b> tab above.</p>
<br>
<p>Thank you for your patience.</p>

<?php
