<?php
include 'dbconnect.inc.php';
// echo "include/report_partners.inc.php";
include 'from_CID_count_company_partners_pending.inc.php'; // $cpCPID
include 'from_CID_count_company_partners_requested.inc.php'; // $crCPID

// $ACID = $_SESSION['ACID'];
$CID = $_SESSION['CID'];
$pstat = 0;
$td = date('U');

$pddh = 'Partnership Pending Report';
$pddhbc = '#e5ff1c';
include 'page_description_date_header.inc.php';

// SETS the ORDER unless changed
if(isset($_GET['or'])){ $ord = $_GET['or']; }else{ $ord = 'c.name'; }
// SETS the SORT sequence at ASC unless changed to DESC
if(isset($_GET['so'])){ $sort = $_GET['so']; }else{ $sort = 'ASC'; }

// echo "<br>**** count_orders count : $cPTID ****";
// Check the URL to see what page we are on
if (isset($_GET['pa'])) { $pa = $_GET['pa']; }else{ $pa = 0; }

// Check which page we are on
if ($pa > 1) { $start = ($pa * $rpp) - $rpp; }else { $start = 0; }
// Set the rows per page
$rpp = 20;

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
include 'sql/pc_report_partners.sql.php';

$PTV = ($pCount / $rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;

// echo '<br>Start record is : '.$start;
?>
<!-- <div class="cpsty" style="position:absolute; top:10%; height:3%; left:0%; width:100%; background-color:#<?php echo $hc ?>"><?php echo "Pending Sent Requests" ?></div> -->

<!-- Partners YOU have requested to join with you -->
<table class="trs" style="position:absolute; top:11%;">
  <tr>
    <th style="text-align:left; text-indent:3%;"><a class="hreflink" href="?P&pap&or=ct.name&so=<?php echo $sort ?>">Requested by</a></th>
    <th style="text-align:left; text-indent:3%;">CLIENT</th>
    <th style="text-align:left; text-indent:3%;">SUPPLIER</th>
    <th style="text-align:left; text-indent:3%;"><a class="hreflink" href="?P&pap&or=cp.inputtime&so=<?php echo $sort ?>">Date Requested</a></th>
    <th>Response</th>
  </tr>
  <?php
  $sql = "SELECT cp.inputtime AS REQUESTED
  -- SQL is Pending_Partners_list.sql
         , cp.ID AS CPID
         , cp.insCID AS INITIATOR
         , cp.req_CID AS CLIENT_ID
         , cp.rel as CPIDrel
         , rc.name AS CLIENT
         , cp.acc_CID AS SUPPLIER_ID
         , ac.name AS SUPPLIER
         , IF(cp.active=0,'PENDING',IF(cp.active=1,'ACCEPTED','REJECTED')) AS STATUS
         , IF(cp.active=0,'PENDING',IF(cp.active=1,cp.accdate,'REJECTED')) AS ACCEPTED
  FROM company_partnerships cp
       INNER JOIN company rc ON cp.req_CID = rc.ID
       INNER JOIN company ac ON cp.acc_CID = ac.ID
  WHERE cp.active = ?
    AND (cp.req_CID = ? OR cp.acc_CID = ?)
          -- ORDER BY $ord $sort
          -- LIMIT $start, $rpp
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-rp1</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $pstat, $CID, $CID);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    // if ($sort==DESC) { $sort = ASC } else { $sort = DESC }
    // $sort == DESC ? $sort = ASC : $sort = DESC;
    while($row = mysqli_fetch_array($res)){
      $CPID = $row['CPID'];
      $CIDi = $row['INITIATOR'];
      $CIDc = $row['CLIENT_ID'];
      $CIDcN = $row['CLIENT'];
      $CIDs = $row['SUPPLIER_ID'];
      $CIDsN = $row['SUPPLIER'];
      $CPIDrel = $row['CPIDrel'];
      $CPIDt = $row['REQUESTED'];

      if (($CIDi == $CIDc) && ($CIDc == $CID)) {
        $insCIDn = $CIDcN;
      }elseif (($CIDi <> $CIDc) && ($CIDc == $CID)) {
        $insCIDn = $CIDsN;
      }elseif (($CIDi == $CIDc) && ($CIDc <> $CID)) {
        $insCIDn = $CIDcN;
      }elseif (($CIDi <> $CIDc) && ($CIDc <> $CID)) {
        $insCIDn = $CIDsN;
      }

      $CPIDt = date('d-M-Y', $CPIDt);
      ?>
      <tr>
        <td style="text-align:left; text-indent:2%;"><?php echo "$insCIDn" ?></td>
        <td style="text-align:left; text-indent:2%;"><?php echo "$CIDcN"; ?></td>
        <td style="text-align:left; text-indent:2%;"><?php echo "$CIDsN"; ?></td>
        <td style="text-align:left; text-indent:2%;"><?php echo "$CPIDt"; ?></td>
        <?php
        if ($CID == $CIDi) {
          ?>
          <td></td>
          <?php
        }else {
          ?>
          <td ><a style="color:blue; text-decoration:none;" href="partners.php?P&paa=<?php echo $CPID ?>"> ACCEPT/Reject </a></td>
          <?php
        }
        ?>
      </tr>
      <?php
    }
  }
  ?>
</table>
