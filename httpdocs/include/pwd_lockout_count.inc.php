<?php
echo "<br><b>pwd_lockout_count.inc.php</b>";
// Count how many times a user has been locked out
if (isset($_GET['uid'])) {
  $UID = $_GET['uid'];
}
echo "<br>UID = $UID";

$sql = "SELECT fail as fail
          , Ftime as Ftime
          , firstname as UIDf
        FROM users
        WHERE ID = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-plc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $UID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $pwd_lockout_count = $row['fail'];
  $UIDf = $row['UIDf'];
  $UIDfailTime = $row['Ftime'];
}
echo "<br>Lockout count = $pwd_lockout_count";
// $fail_update = 1;
// include 'check_pwd_incorrect_add.inc.php';

if ($pwd_lockout_count == 0) {
  $fail_update = 1;
  echo "<br>You have used an incorrect password";
  include 'check_pwd_incorrect_add.inc.php';

  header("Location:../login.php?A=2&uf=$UIDf&f=1&uid=$UID");
  exit();
}elseif ($pwd_lockout_count == 1) {
  $fail_update = 2;
  echo "<br>You have one more attempt to get it right";
  include 'check_pwd_incorrect_add.inc.php';
  header("Location:../login.php?A=2&uf=$UIDf&f=2&uid=$UID");
  exit();
}elseif ($pwd_lockout_count == 2) {
  $fail_update = 3;
  echo "<br>You are locked out for one hour";

  // Get an unlock time $utd
  $sql = "SELECT Ftime as Ftime
  FROM users
  WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-plc</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_assoc($result);
    $Ftime = $row['Ftime'];
  }

  // echo "<br>Ftime = $Ftime";
  $utd = $Ftime + 3600;
  $Ftime = date('d-M-Y::h:m', $Ftime);
  // echo "<br>LOCKED time = $Ftime";

  $utd = date('d-M-Y::h:m', $utd);
  echo "<br>You are locked out until $utd";
  include 'check_pwd_incorrect_add.inc.php';
  header("Location:../login.php?A=2&uf=$UIDf&f=3&uid=$UID");
  exit();
}elseif ($pwd_lockout_count == 3) {
  echo "<br>YOU ARE LOCKED OUT!!!!";
  header("Location:../login.php?A=2&uf=$UIDf&f=3&uid=$UID");
  exit();
}
