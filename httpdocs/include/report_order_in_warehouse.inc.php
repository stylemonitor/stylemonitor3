<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_order_in_work.inc.php";

include 'set_urlPage.inc.php';

$CID = $_SESSION['CID'];

include 'from_CID_count_orders_in_warehouse.inc.php';
// include 'include/R_on_over_sum_MP1.inc.php';
// include 'from_OIID_get_order_details.inc.php';
include 'R_on_sum_MU_LOGMP6.inc.php';
include 'R_on_over_sum_MP6.inc.php';
// include 'include/from_OPID_get_ALL_MP_qtys.inc.php';
$WIP = XXX;
?>
<div class="cpsty" style="background-color:#ef80bb"><?php echo "There are $cOIDwh Orders"?></div>

<table class="trs" style="width:100%;">
  <tr>
    <th style="width:3%;">S/P</th>
    <th style="width:7%;">O/r</th>
    <th style="width:24%; text-align:left; text-indent:5%;" style="width:7%;">Associate / Partner</th>
    <th style="width:34%; text-align:left; text-indent:5%;">Reference</th>
    <th style="width:9%;">Short Code</th>
    <th style="width:8%;">Qty</th>
    <th style="width:14%;">Due date</th>
    <th style="width:1%;"></th>
  </tr>
  <?php
  $sql = "SELECT DISTINCT o.ID as OID
  		      , o.orNos OIDnos
            , oi.ID as OIID
            , oi.their_item_ref as OIDr
            , oi.ord_item_nos OIIDnos
            , ac.name ACIDn
            , o.our_order_ref OIDour
            , pr.prod_ref PRIDr
            , odd.del_Date ODDIDd
            , ot.ID as OTID
          FROM company c
            , associate_companies ac
            , division d
            , orders o
            , orders_due_dates odd
            , order_placed op
            , order_placed_move opm
            , order_item_movement_reason oimr
            , order_item oi
            , order_type ot
            , prod_ref pr
          WHERE c.ID = ?
          AND ac.CID = c.ID
          AND d.ACID = ac.ID
          AND o.DID = d.ID
          -- AND o.orComp = 1
          AND oi.OID = o.ID
          AND op.OIID = oi.ID
          AND oimr.ID IN (8, 18)
          AND opm.OPID = op.ID
          AND opm.OIMRID = oimr.ID
          AND oi.PRID = pr.ID
          AND odd.OID = o.ID
          AND oi.OTID = ot.ID
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-roiwh</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $CID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      $OID     = $row['OID'];
      $OIDn    = $row['OIDnos'];
      $OIID    = $row['OIID'];
      $OIIDn   = $row['OIIDnos'];
      // $OIDnos  = $row['OIDnos'];
      $ACIDn   = $row['ACIDn'];
      $OIDr    = $row['OIDr'];
      $PRIDr   = $row['PRIDr'];
      $ODDIDd  = $row['ODDIDd'];
      $OTID    = $row['OTID'];

      $ODDIDd = date('d-M-Y', $ODDIDd);
      ?>
      <tr>
        <?php
        if ($OTID == 1) {
          ?>
          <td style="width: 3%; font-weight: bold; background-color:#f7cc6c;"><?php echo 'P' ?></td>
        <?php }elseif ($OTID == 2) { ?>
          <td style="width: 3%; font-weight: bold; background-color:#bbbbff;"><?php echo 'P' ?></td>
        <?php }elseif ($OTID == 3) { ?>
          <td style="width: 3%; font-weight: bold; background-color:#f243e4;"><?php echo 'P' ?></td>
        <?php }elseif ($OTID == 4) { ?>
          <td style="width: 3%; font-weight: bold; background-color:#577268;"><?php echo 'P' ?></td>
        <?php }elseif ($OTID == 5) { ?>
          <td style="width: 3%; font-weight: bold; background-color:#f7cc6c;"><?php echo 'S' ?></td>
        <?php }elseif ($OTID == 6) { ?>
          <td style="width: 3%; font-weight: bold; background-color:#bbbbff;"><?php echo 'S' ?></td>
        <?php }elseif ($OTID == 7) { ?>
          <td style="width: 3%; font-weight: bold; background-color:#f243e4;"><?php echo 'S' ?></td>
        <?php }elseif ($OTID == 8) { ?>
          <td style="width: 3%; font-weight: bold; background-color:#577268;"><?php echo 'S' ?></td>
        <?php } ?>
        <td style="width:6%; color:red; text-align: right; padding-right: 1%; border-right:thin solid grey;"><a style="text-decoration:none;" href="<?php echo $urlPage ?>&orb&oi=<?php echo $OIID;?>"><?php echo $OIDn.':'.$OIIDn;?></a></td>
        <td style="width:24%; text-align:left; text-indent:2%; border-right:thin solid grey;"><?php echo $ACIDn ?></td>
        <td style="width:34%; text-align:left; text-indent: 1%; border-right:thin solid grey;"><?php echo $OIDr ?></td>
        <td style="width:9%; border-right:thin solid grey;"><?php echo $PRIDr ?></td>
        <td style="width:8%; border-right:thin solid grey;">W/house?</td>
        <td style="width:14%; border-right:thin solid grey;"><?php echo $ODDIDd ?></td>
        <td style="width:1%; background-color:green;"></td>
      </tr>
      <?php
    }
  }
  ?>
</table>
<?php
