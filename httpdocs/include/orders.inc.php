<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/orders.inc.php</b>";

if (isset($_GET['ori'])) {
  include 'orders_info.inc.php';
}elseif (isset($_GET['orn'])) {
  // include 'form_new_orders_combined.inc.php';
  include 'form_new_orders_combined.inc.php';
  // include 'form_new_orders.inc.php';
}elseif (isset($_GET['orp'])) {
  // echo "ADD ANOTHER ITEM TO THE ORDER";
  include 'form_new_orders_combined_plus.inc.php';
}elseif (isset($_GET['ore'])) {
  echo "<br>Orders should be able to be edited here";
}elseif (isset($_GET['aoo'])) {
  // echo "<br>Review ALL open orders";
  include 'order_report.inc.php';
}elseif (isset($_GET['odr'])) {
  include 'report_order_by_item.inc.php';
  // echo "<br>Show ALL items on the order";
}elseif (isset($_GET['odo'])) {
  include 'order_report_ORDER_NUMBER.inc.php';
  // echo "<br>Show ALL items on the order";
}elseif (isset($_GET['oed'])) {
  // echo "<br>List ALL orders with some in make";
  include 'edit_order.inc.php';
}elseif (isset($_GET['orm'])) {
  echo "<br>List ALL orders with some in make";
}elseif (isset($_GET['orw'])) {
  echo "<br>List ALL orders with some w/house AND in make";
}elseif (isset($_GET['orc'])) {
  echo "<br>ALL orders that have been made BUT not yet fully despatched";
}elseif (isset($_GET['ord'])) {
  echo "<br>ALL orders that have been despatched";
}elseif (isset($_GET['orb'])) {
  // echo "Here we should see the order report basic information";
  include 'REPORT_ORDER_header.inc.php';
  include 'report_order_movement.inc.php';
  include 'form_new_order_movement.inc.php';
  if (isset($_GET['des'])) {
    // echo "Edit despatch details";
    include 'edit_despatch_details.inc.php';
  }
}elseif (isset($_GET['orf'])) {
  // echo "Here we should see the order report basic information";
  include 'REPORT_ORDER_header.inc.php';
}elseif (isset($_GET['org'])) {
  // echo "Here we should see the order report basic information";
  include 'REPORT_ORDER_header.inc.php';
  include 'record_movement_summary.inc.php';
  include 'report_movement_log.inc.php';
}elseif (isset($_GET['oiw'])) {
  // echo "Here we should see the order report basic information";
  include 'report_order_in_work.inc.php';
}elseif (isset($_GET['owh'])) {
  // echo "Here we should see the order report basic information";
  include 'report_factory_loading.inc.php';
}elseif (isset($_GET['orh'])) {
  echo "Here we should see the Pre-Production information";
  // include 'REPORT_ORDER_header.inc.php';
  // include 'report_order_movement.inc.php';
}elseif (isset($_GET['or1'])) {
  echo "Here we should see the Material component information";
  // include 'REPORT_ORDER_header.inc.php';
  // include 'report_order_movement.inc.php';
}elseif (isset($_GET['orj'])) {
  echo "Here we should see the Add another make unit page";
  // include 'REPORT_ORDER_header.inc.php';
  // include 'report_order_movement.inc.php';
}elseif (isset($_GET['ork'])) {
  // echo "<br>Show a summary of the order movement WITH a button to show detail";
  include 'REPORT_ORDER_header.inc.php';
  // include 'report_make_unit_summary.inc.php';
  // include 'record_movement_summary.inc.php';
  // include 'report_movement_log.inc.php';
}elseif (isset($_GET['com'])) {
  // echo "<br>Show a summary of the order movement WITH a button to show detail";
  include 'order_report.inc.php';
  // include 'report_make_unit_summary.inc.php';
  // include 'record_movement_summary.inc.php';
  // include 'report_movement_log.inc.php';
}elseif (isset($_GET['cmb'])) {
  include 'form_new_orders_combined.inc.php';
}elseif (isset($_GET['pat'])) {
  include 'report_partners_suppliers.inc.php';
}elseif (isset($_GET['mlg'])) {
  include 'report_order_movement_log.inc.php';
  include 'report_movement_log.inc.php';
}elseif (isset($_GET['awt'])) {
  include 'report_factory_loading_awaiting.inc.php';
}elseif (isset($_GET['mp1'])) {
  include 'report_factory_loading_1.inc.php';
}elseif (isset($_GET['mp2'])) {
  include 'report_factory_loading_2.inc.php';
}elseif (isset($_GET['mp3'])) {
  include 'report_factory_loading_3.inc.php';
}elseif (isset($_GET['mp4'])) {
  include 'report_factory_loading_4.inc.php';
}elseif (isset($_GET['mp5'])) {
  include 'report_factory_loading_5.inc.php';
}elseif (isset($_GET['whs'])) {
  include 'report_factory_loading_whs.inc.php';
}
