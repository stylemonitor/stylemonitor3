<?php
include 'dbconnect.inc.php';
//echo '<b>include/from_tCID_get_product_ref.inc.php</b>';
$CID = $tCID;

include 'from_tCID_count_product_ref.inc.php';

$sql = "SELECT pr.ID as PRID
          , pr.prod_ref as PRIDn
        FROM associate_companies ac
          , division d
          , company c
          , prod_ref pr
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND pr.DID = d.ID
        ORDER BY 2
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-ftgpr</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $tCID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $tPRID = $row['PRID'];
  $tPRIDn = $row['PRIDn'];
}
?>
