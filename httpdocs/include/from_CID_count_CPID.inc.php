<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_count_CPID.inc.php</b>";

$sql = "SELECT COUNT(ID) as cCPID
          , req_CID as CPIDr
        FROM company_partnerships
        WHERE req_CID = ?
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cCPID = $row['cCPID'];
}
