<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_count_product_type.inc.php</b>";
// pt.DIDchanged
$CID = $_SESSION['CID'];

$sql = "SELECT COUNT(pt.ID) as cPTID
        FROM company c
          , product_type pt
        WHERE c.ID = ?
        AND pt.CID = c.ID
        AND pt.name NOT IN ('Select Product Type')
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fccpt</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cPTID = $row['cPTID'];
}
