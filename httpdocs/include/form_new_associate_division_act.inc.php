<?php
include 'dbconnect.inc.php';
// echo "include/form_new_associate_division_act.inc.php";
$id = $_POST['id'];
$ct = $_POST['ct'];

if (!isset($_POST['newdiv']) && !isset($_POST['candiv']) ) {
  echo "Wrong entry";
}elseif (isset($_POST['candiv'])) {
  // if (isset($_GET['aad'])) {
    // echo "Cancel  Division Add";
    // echo " :: id = $id";
    // echo " :: ct = $ct";
    header("Location:../associates.php?A&asf&id=$id&ct=$ct");
    exit();
  // }
}elseif (isset($_POST['newdiv'])) {
  // echo "Add new Division";
  // get the information frmo the form
  $div  = $_POST['div'];
  $ACID  = $_POST['id'];
  // $ACID = $_SESSION['ACID'];
  $UID  = $_SESSION['UID'];
  $td   = date('U');

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to

    // Check if division name already in use for that company
    $sql = "SELECT ID as DID
    FROM division
    WHERE ACID = ?
    AND name = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b><br>FAIL-fnda2</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $ACID, $div);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $DID = $row['DID'];
    }

    if (!empty($DID)) {
      header("Location:../company.php?C&sdn&2");
      exit();
    }else {
      $sql = "INSERT INTO division
      (ACID, UID, name, inputtime)
      VALUES
      (?,?,?,?)
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b><br>FAIL-fnda</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ssss", $ACID, $UID, $div, $td);
        mysqli_stmt_execute($stmt);
      }

      // Get the new divisions ID
      $sql = "SELECT ID as DID
      FROM division
      WHERE ACID = ?
      AND name = ?
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b><br>FAIL-fnda2</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ss", $ACID, $div);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        $row = mysqli_fetch_array($result);
        $DID = $row['DID'];
        // echo "<br>Division ID : $DID";
      }

      // ADD a section to the new division
      $sql = "INSERT INTO section
      (DID, UID, inputtime)
      VALUES
      (?,?,?)
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b><br>FAIL-fnda3</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "sss", $DID, $UID, $td);
        mysqli_stmt_execute($stmt);
      }

    } catch (mysqli_sql_exception $exception) {
      mysqli_rollback($mysqli);

      throw $exception;
    }

    header("Location:../associates.php?A&ald&id=$id&ct=$ct");
    exit();
  }
}
