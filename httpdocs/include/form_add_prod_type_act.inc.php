<?php
include 'dbconnect.inc.php';
$UID = $_SESSION['UID'];
// echo "<b>include/form_new_company_user_act.inc.php</b>";

if (!isset($_POST['update']) && !isset($_POST['CANCEL'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['CANCEL'])) {
  // echo "<br>Return to the Users page";
  header("Location:../styles.php?S&snn");
  exit();
}elseif (isset($_POST['update'])) {
  include 'dbconnect.inc.php';

  $PTID = $_POST['PTID'];
  $oPTID = $_POST['oPTID'];
  $PRID = $_POST['PRID'];

  // echo "Original DATA == PRID = $PRID // PTID = $oPTID";
  // echo "<br>REVISED PTID = $PTID";

  // Update the prod_ref woth the new product type
  $sql = "UPDATE prod_ref
          SET PTID = ?
          WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-fapta</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $PTID, $PRID);
    mysqli_stmt_execute($stmt);
  }
  header("Location:../styles.php?S&snn");
  exit();
}
