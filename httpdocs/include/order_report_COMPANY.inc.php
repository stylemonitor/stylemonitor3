<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "include/order_report_COMPANY.inc.php";

$CID = $_SESSION['CID'];

if (isset($_GET['oc'])) {
  $OC = $_GET['oc'];
}else {
  $OC = 0;
}

$td = date('U');

include 'from_ACID_get_associate_company_details.inc.php';
// echo "ACID is $ACID";

$pddh = "Open Orders : $ACIDn";
$pddhbc = '#fffddd';
include 'page_description_date_header.inc.php';

// Check the URL to see what page we are on
if (isset($_GET['lt'])) { $lt = $_GET['lt']; }else{ $lt = 0; }

// Set the rows per page
$r = 5;
// Check which page we are on
if ($lt > 1) { $start = ($lt * $r) - $r; }else { $start = 0; }

// sets the order type
// all open orders overdue
if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  if ($tab == 1) {
    $bc ='f7cc6c';
  }elseif ($tab == 2) {
    $bc ='bbbbff';
  }elseif ($tab == 3) {
    $bc ='d68e79';
  }elseif ($tab == 4) {
    $bc ='c9d1ce';
  }
  $sp = 'asc';
  $divpos = 10;
  $tabpos = 13.2;
  $pagepos = 92;
  // $OC = 0;
  $cp = "orders due out after 2 weeks";
  $hc = 'black';
  $hbc = '#92ef9d';
  $r = 20;
}elseif (isset($_GET['asf'])) {
  $sp = 'asc';
  $divpos = 32;
  $tabpos = 35.2;
  $pagepos = 92;
  // $OC = 0;
  $cp = "orders due out after 2 weeks";
  $hc = 'black';
  $hbc = '#92ef9d';
  $r = 20;
}elseif (isset($_GET['asc'])) {
  $sp = 'asc';
  $divpos = 9;
  $tabpos = 12.2;
  $pagepos = 92;
  // $OC = 0;
  $cp = "orders due out after 2 weeks";
  $hc = 'black';
  $hbc = '#92ef9d';
  $r = 20;
}

$sdate = date('d-M-Y', $sd);

if(isset($_GET['ord'])){ $ord = $_GET['ord']; }else{ $ord = 'oidd.item_del_date'; }
if(isset($_GET['sort'])){ $sort = $_GET['sort']; }else{ $sort = 'ASC'; }

if (isset($_GET['csl'])) {
  $OIDcomp = 1;
}else {
  $OIDcomp = 0;
}

// echo "<br>Company ID : $CID";
// echo "<br>Order status : $OC";
// echo "<br>Today : $td";
// echo "<br>Start day : $sd";
// echo "<br>End day : $ed";
// echo "<br>Count_orders - due date source : $dr";

// include 'order_page_count.inc.php';
$sql = "SELECT COUNT(o.ID) as OID
        FROM orders o
          , company c
          , associate_companies ac
          , division d
          , order_item oi
          , order_item_del_date oidd
        WHERE o.CPID IN(SELECT cp.ID
            						FROM company_partnerships cp
            						WHERE cp.req_CID = ? OR cp.acc_CID = ?)
        AND o.orComp = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND o.tDID = d.ID
        AND oi.OID = o.ID
        AND oidd.OIID = oi.ID
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-or1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $OC);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOID = $row['OID'];
}


// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cOID / $r);
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;
// echo '<br>Start record is : '.$start;
// if ($p > 1) { $start = ($p * $r) - $r; }else { $start = 0; }

if ($cOID < 1) {
  // echo "<br>NO orders placed";
  ?>
  <div class="trs" style="position:absolute; top:<?php echo $divpos ?>%; left:0%; width:100%;">
    <div class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "There are 0 Orders"?></div>
  </div>
      <!-- <p>NO ORDERS TO REVIEW count = <?php echo $cOID ?></p> -->
  <?php
}else {
  ?>
  <div class="cpsty" style="position:absolute; top:<?php echo $divpos ?>%; left:0%; width:100%; background-color:<?php echo $hbc ?>; padding-top:0.2%;"><a style="color:<?php echo $hc ?>; text-decoration:none;" href="home.php?H&aot">Open Order Review</a>
  </div>
  <table class="trs" style="position:absolute; top:<?php echo $tabpos ?>%;">
    <tr>
      <th style="width:5%; background-color:<?php echo $spbc ?>;">Order</th>
      <th style="width:2%; background-color:<?php echo $spbc ?>;"></th>
      <th style="width:39%; text-align:left; text-indent:1%;">Our Item Reference</th>
      <th style="width:39%; text-align:left; text-indent:1%;">Their Order Reference</th>
      <th style="width:5%; text-align: center;">Qty</th>
      <th style="width:10%; background-color:<?php echo $bc ?>; text-align:center;">Date</th>
    </tr>

    <?php

    include 'sql/order_report_COMPANY.sql.php';

    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-oQ</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssssss", $CID ,$CID, $ACID, $ACID, $start, $r);
      // mysqli_stmt_bind_param($stmt, "sssssss", $OIDcomp ,$CID ,$CID, $ACID, $ACID, $start, $r);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      while($row = mysqli_fetch_assoc($result)){
        $cliCID = $row['cliCID'];
        $cliCIDn = $row['cliCIDn'];
        $cli_ACID = $row['cli_ACID'];
        $cli_ACIDn = $row['cli_ACIDn'];
        $supCID = $row['supCID'];
        $supCIDn = $row['supCIDn'];
        $sup_ACID = $row['sup_ACID'];
        $sup_ACIDn = $row['sup_ACIDn'];
        $OID = $row['OID'];
        $cOIID = $row['OIIDc'];
        $OIDnos = $row['ordNos'];
        $OTID = $row['OTID'];
        $ourRef = $row['ourRef'];
        $theirRef = $row['theirRef'];
        $OIDDIDd = $row['earliest_date'];
        $CPID = $row['CPID'];
        $OIQIDq = $row['OIQIDq'];
        $Awaiting = $row['Awaiting'];
        $Stage1 = $row['Stage1'];
        $Stage2 = $row['Stage2'];
        $Stage3 = $row['Stage3'];
        $Stage4 = $row['Stage4'];
        $Stage5 = $row['Stage5'];
        $Warehouse = $row['Warehouse'];
        $CLIENT = $row['CLIENT'];
        $SUPPLIER = $row['SUPPLIER'];

// Taken OUT as DAL sorted in his sql
// LEFT IN
        if ($cliCID <> $CID) {
          $cli_ACIDn = $cli_ACIDn;
          $OTID = $OTID - 1;
        }else {
          $cli_ACIDn = $sup_ACIDn;
          $OTID = $OTID;
        }

        $idate = date('d-M-Y', $OIDDIDd);
        ?>
        <tr style="font-family:monospace;">
          <?php

          // if ($samProd == 0) {
          //   $spc ='#76f8bc';
          //   $spbc = '#76f8bc';
          // }else {
          //   $spc = '#fa7b7b';
          //   $spbc = '#fa7b7b';
          // }
          // Shows you the orders placed within the last 7 days
          // if($od>($td-604740)){
          if ($OTID == 1) {
            $c = 'black';
            $bc = $salAssCol;
          }elseif ($OTID == 2) {
            $c = 'black';
            $bc = $purAssCol;
          }elseif ($OTID == 3) {
            $c = 'white';
            $bc = $salPrtCol;
          }elseif ($OTID == 4) {
            $c = 'white';
            $bc = $purPrtCol;
          }
          ?>

          <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"><a class="hreflink" href="<?php echo $urlPage ?>&rt4&o=<?php echo $OID;?>"><?php echo $OIDnos ?></a></td>
          <td class="select" style="text-align: right; border-right:thin solid black; padding-right:0.5%; background-color:<?php echo $bc ?>;"><a class="hreflink" href="<?php echo $urlPage ?>&rt4&tab=<?php echo $tab ?>&o=<?php echo $OID;?>"><?php echo $cOIID ?></a></td>
          <td style="color:black; text-align:left; text-indent:1%; border-left: thin solid grey; border-right:thin solid grey;"><?php echo $ourRef ?></td>
          <td class="select" style="text-align:left; text-indent:1%; border-right:thin solid grey;"><?php echo $theirRef ?></td>
          <td style="text-align:right; border-right:thin solid grey; padding-right:0.5%;"><?php echo $OIQIDq;?></td>

          <?php

          if($OIDDIDd<$td){;?>
            <td style="text-align:right; padding-right:1%; background-color:#fc4141;"><?php echo $idate ;?></td>
            <?php
          }elseif($OIDDIDd<($td+604740)){;?>
            <td style="text-align:right; padding-right:1%; background-color:#f09595;"><?php echo $idate ;?></td>
            <?php
          }elseif($OIDDIDd>($td+121509600)){;?>
            <td style="text-align:right; padding-right:1%; background-color:#e2e8a8;"><?php echo $idate ;?></td>
            <?php
          }else{;?>
            <td style="text-align:right; padding-right:1%;"><?php echo $idate ;?></td>
          <?php
        }?>
        </tr>
        <?php
        }
      } ?>
      </table>



    <div style="position:absolute; top:<?php echo $pagepos ?>%; right:5%; font-size:200%;">

      <!-- <?php
      if (isset($_GET['H'])) {
        $page = 'H';
      }elseif (isset($_GET['L'])) {
        $page = 'L';
      }
        for ($x = 1 ; $x <= $PTV ; $x++){
        echo "<a style='color:red; text-decoration:none;' href='?$page&$sp&lt=$x'>  $x  </a>";
      }
      ?> -->
    </div>

    <?php
    if (isset($_GET['cls'])) {
      ?>
      <form action="home.php?H&tab=6&id=<?php echo $ACID ?>" method="post">
        <button class="entbtn" style="position:absolute; bottom:5%; left:80%; width:15%;" type="submit" name="button">Open Orders</button>
      </form><?php
    }else {
      ?>
      <form action="home.php?H&tab=6&cls&id=<?php echo $ACID ?>" method="post">
        <button class="entbtn" style="position:absolute; bottom:5%; left:80%; width:15%;" type="submit" name="button">Closed Orders</button>
      </form>
      <?php
    }
    ?>

    <?php
  } ?>
