<?php
include 'dbconnect.inc.php';
// echo "include/from_CID_count_order_by_week.inc.php";
$CID = $_SESSION['CID'];
$or = 0;

$sql = "SELECT  SUM(COUNTS.OVERDUE_1) + INDIV_COUNT.OVERDUE_2 AS OVERDUE
-- SQL is order_count_per_week.sql
        , SUM(COUNTS.THIS_WEEK) - INDIV_COUNT.OVERDUE_2 AS THIS_WEEK
        , SUM(COUNTS.NEXT_WEEK) AS NEXT_WEEK
       , SUM(COUNTS.LATER) AS LATER
FROM (
      SELECT IF(
                (CASE WHEN yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) = yearweek(curdate(),7) THEN COUNT(oi.ID) END) IS NULL,0,
                (CASE WHEN yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) = yearweek(curdate(),7) THEN COUNT(oi.ID) END)
                ) AS THIS_WEEK
             , IF(
                  (CASE WHEN yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) = yearweek(curdate(),7)+1 THEN COUNT(oi.ID) END) IS NULL,0,
                  (CASE WHEN yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) = yearweek(curdate(),7)+1 THEN COUNT(oi.ID) END)
                  ) AS NEXT_WEEK
             , IF(
                  (CASE WHEN yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) > yearweek(curdate(),7)+1 THEN COUNT(oi.ID) END) IS NULL,0,
                  (CASE WHEN yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) > yearweek(curdate(),7)+1 THEN COUNT(oi.ID) END)
                  ) AS LATER
             , IF(
                  (CASE WHEN yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) < yearweek(curdate(),7) THEN COUNT(oi.ID) END) IS NULL,0,
                  (CASE WHEN yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) < yearweek(curdate(),7) THEN COUNT(oi.ID) END)
                  ) AS OVERDUE_1
             , cp.req_CID
      FROM company_partnerships cp
           INNER JOIN orders o ON cp.ID = o.CPID
           INNER JOIN order_item oi ON o.ID = oi.OID
           INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
      WHERE (cp.req_CID = ? OR cp.acc_CID = ?)
      AND oi.itComp = ?
      GROUP BY yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7), yearweek(curdate(),7)
    ) COUNTS LEFT OUTER JOIN (
                            SELECT COUNT(oi.ID) AS OVERDUE_2
                                   , cp.req_CID
                            FROM company_partnerships cp
                                       INNER JOIN orders o ON cp.ID = o.CPID
                                       INNER JOIN order_item oi ON o.ID = oi.OID
                                       INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
                            WHERE (cp.req_CID = ? OR cp.acc_CID = ?)
                              AND oi.itComp = ?
                              AND from_unixtime(oidd.item_del_date,'%Y-%m-%d') < curdate()
                          ) INDIV_COUNT ON COUNTS.req_CID = INDIV_COUNT.req_CID

;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccooa</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssssss", $CID, $CID, $or, $CID, $CID, $or);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $cOD = $row['OVERDUE'];
  $cTW = $row['THIS_WEEK'];
  $cNW = $row['NEXT_WEEK'];
  $cLT = $row['LATER'];
}

if (empty($cOD)) {
  $cOD = 0;
}else {
  $cOD = $cOD;
}
if (empty($cTW)) {
  $cTW = 0;
}else {
  $cTW = $cTW;
}
if (empty($cNW)) {
  $cNW = 0;
}else {
  $cNW = $cNW;
}
if (empty($cLT)) {
  $cLT = 0;
}else {
  $cLT = $cLT;
}
// echo "<br>Overdue count : $COIDod";
