<?php
include 'dbconnect.inc.php';
// echo "<br><b>include/from_PRID_get_prod_details.inc.php</b>";

if (isset($_GET['fso'])) {
	$PRID = $_GET['fso'];
}elseif (isset($_GET['s'])) {
	$PRID = $_GET['s'];
}else {
	$PRID = $PRID;
}
// echo "<br>$PRID";

// echo "PRID = $PRID";

$sql = "SELECT concat(pr.prod_ref,' : ',pr.prod_desc) as fullDesc
-- SQL is from_PRID_get_prod_details.sql
        , pr.prod_ref as PRIDr
        , pr.prod_desc as PRIDd
        , pr.etm as PRIDe
        , d.ID as DID
        , d.name as DIDn
        , IF(s.ID IS NULL,'NO SEASON',s.ID) as SNID
        , IF(s.name IS NULL,'NO SEASON',s.name) as SNIDn
        , IF(cl.ID IS NULL,'NO COLLECTION',cl.ID) as CLID
        , IF(cl.name IS NULL,'NO COLLECTION',cl.name) as CLIDn
        , pt.ID as PTID
        , pt.name as PTIDn
        , pt.scode as PTIDr
FROM prod_ref pr
     INNER JOIN associate_companies ac ON ac.ID = pr.ACID
     INNER JOIN division d ON ac.ID = d.ACID
     LEFT OUTER JOIN prod_ref_to_collection prc ON pr.ID = prc.PRID
     LEFT OUTER JOIN prod_ref_to_season prs ON pr.ID = prs.PRID
     LEFT OUTER JOIN collection cl ON prc.CLID = cl.ID
     LEFT OUTER JOIN season s ON prs.SNID = s.ID
     INNER JOIN product_type pt ON pr.PTID = pt.ID
WHERE pr.ID = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fpgpd</b>';
}else{
	mysqli_stmt_bind_param($stmt, "s", $PRID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
	// $PRID   = $row['PRID'];
  $fullDesc = $row['fullDesc'];
  $PRIDr  = $row['PRIDr'];
  $PRIDd  = $row['PRIDd'];
  $PRIDe  = $row['PRIDe'];
	$PTID   = $row['PTID'];
	$PTIDn  = $row['PTIDn'];
	$DID    = $row['DID'];
	$DIDn   = $row['DIDn'];
	$SNID   = $row['SNID'];
	$SNIDn  = $row['SNIDn'];
	$CLID  = $row['CLID'];
  $CLIDn = $row['CLIDn'];
  $PTIDr  = $row['PTIDr'];
}
