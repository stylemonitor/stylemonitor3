<?php
include 'dbconnect.inc.php';
// echo "include/from_pCID_count_divisions.inc.php";
include 'from_CPID_get_partners_CID.inc.php';
// echo "AC1=$aCID";
// echo "RC1=$rCID";
include 'from_pCID_get_min_division.inc.php';
$CID = $_SESSION['CID'];
if ($CID <> $aCID) { $mpCID = $aCID;}else { $mpCID = $rCID;}

$sql = "SELECT COUNT(d.ID) AS cDID
        FROM associate_companies ac
          , company c
          , division d
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND d.ID NOT IN (?)
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $mpDID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $cpDID = $row['cPRID'];
}
