<?php
// echo "<br><b>form_new_order_item_entry.inc.php</b>";

include 'set_urlPage.inc.php';
include 'from_DID_get_min_section.inc.php';
include 'from_OID_count_order_item_open.inc.php';
include 'from_OID_get_next_OIID.inc.php';
include 'from_ACID_get_associate_company_details.inc.php';
include 'from_CID_count_product_type.inc.php';

$cOIID = $cOIIDo + 1;
$title = "Order Item : $cOIID";
// $delDate = strtotime($delDate,0);
?>
    <!-- Detail information about the item on order -->
    <input type="hidden" name="OIID" value="<?php echo "$xOIID"; ?>">
    <input type="hidden" name="cOIID" value="<?php echo "$cOIID"; ?>">
    <!-- <input type="hidden" name="delDate" value="<?php echo $delDate ?>"> -->
    <input type="hidden" name="urlPage" value="<?php echo $urlPage ?>">

    <!-- The box around the item detail entry bit -->
    <div style="position:absolute; top:30%; height:4.5%; left:0.5%; width:99%; padding-top: 0.25%; font-size: 150%; font-weight: bold; background-color: #ebe1e1; border:thin solid black; Text-align:left; text-indent:2%;"><?php echo $title ?>
    </div>
    <div style="position:absolute; top:35%; left:0.5%; width:99%; border-top: 2px solid black; z-index:1;"></div>
    <div style="position:absolute; top:35%; height:18%; left:0.5%; width:54.25%; padding-top: 0.25%; font-size: 150%; font-weight: bold; border:thin solid black; Text-align:left; text-indent:2%;"></div>

    <!-- SAMPLE or PRODUCTION -->
    <!-- <div style="position:absolute; top:31.5%; left:22%; width:20%; text-align:right;">
      Sample/s (if applicable)
    </div> -->
    <input type="checkbox" id="sam_prod" style="position:absolute; top:31.3%; left:29%;" name="sam_prod" value="1">
    <label for="sam_prod" style="position:absolute; top:31.5%; left:31.5%;">Sample/s (if applicable)</label>


    <!-- Quantity input -->
    <div style="position:absolute; top:31.5%; left:47%; width:10%; text-align:right;" tabindex=1><b>Quantity</b></div>
    <input required class ="input_data" style="position:absolute; top:30%; height:3%; left:57%; width:10%; padding-right:0.5%; background-color:<?php echo $rqdCol ?>" name="orQTY" placeholder="Qty">

    <!-- The item due by date if different from the order due by date -->
    <!-- IF left blank will default to the ORDER DUE DATE -->
    <div style="position:absolute; top:31.5%; height:8%; left:68%; width:12%; font-weight: bold; text-align:right;">Item Due Date
    </div>
    <input class ="sel_dropdown" style="position:absolute; top:30%; height:3.5%; left:80.5%; width:18%; text-align:center;" type="date" name="itemDelDate" placeholder="Item Del Date" >

    <div style="position:absolute; top:36%; left:1%; width:38.5%; text-align:left;">Our Item Description</div>
      <?php
      if (isset($_GET['des'])) {
        // get the clients item description
        $des = $_GET['des'];
        ?>
        <input type="text" class ="input_data" style="position:absolute; top:39%; height:3%; left:0.75%; width:52.7%; font-size: 100%; text-align:left; background-color:<?php echo $optCol ?>;" name="oItemDes" placeholder="Our Item Description"  value="<?php echo "$des" ?>">
        <?php
      }else {
        if (isset($_GET['orp'])) {
          ?>
          <input required type="text" class ="input_data" style="position:absolute; top:39%; height:3%; left:0.75%; width:52.7%; font-size: 100%; text-align:left; background-color:<?php echo $rqdCol ?>;" name="oItemDes" placeholder="Our Item Description">
          <?php
        }else {
          ?>
          <input type="text" class ="input_data" style="position:absolute; top:39%; height:3%; left:0.75%; width:52.7%; font-size: 110%; text-align:left; background-color:<?php echo $optCol ?>;" name="oItemDes" placeholder="Our Item Description">
          <?php
        }
      }

    if (isset($_GET['prd'])) {
      $PRID = $_GET['prd'];
      include 'from_PRID_get_prod_details.inc.php';
      ?>
      <div style="position:absolute; top:44.5%; left:1%; width:40%; text-align:left;z-index:1;">
        <b><?php echo $PTIDn ?></b>
      </div>
      <?php
      if (strlen($fullDesc) > 40) {
        $fullDesc = substr($fullDesc,0,40);
        ?>
        <div style="position:absolute; top:47%; left:1%; width:48%; text-align:left; font-size: 150%; font-weight:bold;"><?php echo $fullDesc ?> ...
        </div>
        <?php
      }
      ?>
      <!-- the PRID of the ordering company fPRID -->
        <input type="hidden" name="fPRID" value="<?php echo $PRID ?>">
      <div style="position:absolute; top:47%; left:1%; width:48%; text-align:left; font-size: 150%; font-weight:bold;"><?php echo $fullDesc ?>
      </div>
      <?php
    }else {
        ?>
        <div style="position:absolute; top:44.5%; left:1%; width:40%; text-align:left;z-index:1;">
          Product Type description <?php echo $cPTID ?>
        </div>

        <!-- Dropdown dependent menu START -->
        <!-- <div style="position:absolute; top:49%; left:1%; width:15%; text-align:left;">Product Type</div> -->
        <select id="tPTID" name="tPTID" onchange="FetchToStyle(this.value)" style="position:absolute; top:44.5%; height:3.5%; left:1%; width:19%; background-color:<?php echo $optCol ?>; z-index:1;">
          <option>Select Product type</option>
          <?php
          include 'include/from_CID_select_prod_type_dd.inc.php';
          ?>
        </select>
        <script type="text/javascript">
        function FetchToStyle(id){
          $('#fPRID').html;
          // alert(id);
          // return false;
          $.ajax({
            type : 'POST',
            url : 'include/from_pCID_select_TO_product_ref_dd.inc.php',
            data : {tPTID:id},
            success : function(data){
              $('#fPRID').html(data);
            }
          })
        }
        </script>
        <!-- <div style="position:absolute; top:47%; height:4%; left:41%; width:15%; text-align:left;">Style</div> -->
        <select required id="fPRID" name="fPRID" style="position:absolute; top:49%; height:3.5%; left:1%; width:41%; background-color:<?php echo $rqdCol ?>; z-index:1;">
          <option value="">Select Style</option>
          <?php
          include 'include/from_pCID_select_TO_product_ref.inc.php';
          ?>
        </select>
        <!-- Dropdown dependent menu END -->
        <?php
    }

    ?>

    <!-- The 'FROM' companies detials about the order if known -->
    <div style="position:absolute; top:35%; height:18%; left:55.25%; width:44.25%; padding-top: 0.25%; font-size: 150%; font-weight: bold; border:thin solid black; Text-align:left; text-indent:2%;">
    </div>
    <div style="position:absolute; top:36%; left:56%; width:38.5%; text-align:left;">Their Item Description</div>
    <input type="text" class ="input_data" style="position:absolute; top:39%; height:3%; left:55.5%; width:42.7%; font-size: 110%; text-align:left; background-color:<?php echo $optCol ?>;" name="tItemDes" placeholder="Their Item Description">
    <div style="position:absolute; top:44.5%; left:56%; width:40%; text-align:left;">Their Product Reference</div>
    <input type="text" style="position:absolute; top:48%; height:3.5%; left:55.75%; width:42.7%;" name="pPRref" value="">
