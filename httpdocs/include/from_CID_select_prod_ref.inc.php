<?php
include 'dbconnect.inc.php';
// include 'from_CID_select_prod_ref.inc.php';
//echo '<b>include/from_CID_select_prod_type.inc.php</b>';
// pt.CID replaced with pt.DIDchanged

$CID = $_SESSION['CID'];

$sql = "SELECT DISTINCT(pr.ID) as PRID
          , pr.prod_ref as PRIDr
          , pr.prod_desc as PRIDd
          , pt.name as PTIDn
        FROM product_type pt
        , prod_ref pr
        	, associate_companies ac
          , company c
        WHERE c.ID = ?
        AND pt.CID = c.ID
        AND pt.name NOT IN ('Select Product Type')
        AND pr.PTID = pt.ID
        ORDER BY pt.name
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcspt</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($res)){
    $PRID = $row['PRID'];
    $PRIDr = $row['PRIDr'];
    $PRIDd = $row['PRIDd'];
    $PTIDn = $row['PTIDn'];
    echo '<option value="'.$PRID.'">'.$PRIDr.' : '.$PRIDd.' ('.$PTIDn.')'.'</option>';
    // echo '<option value="'.$PTID.'">'.$PTID.'/'.$PTIDn.' : '.$PTIDsc.'</option>';
    ?>
    <!-- <option value="<?php echo $PTID ?>"><?php echo $PTIDn ?></option> -->
    <?php
  }
}
?>
