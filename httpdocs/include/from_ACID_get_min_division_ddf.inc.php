<?php
// echo "<b>incluDe/from_CID_get_min_division.inc.php</b>";


if (isset($_POST['fACID'])) {
  $ACID = $_POST['fACID'];
}

// $CID = $_SESSION['CID'];

$sql = "SELECT MIN(d.ID) as DID
          , d.name as mDIDn
        FROM division d
          , associate_companies ac
          -- , company c
        WHERE ac.ID = ?
        -- AND ac.CID = c.ID
        AND d.ACID = ac.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $ACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $mDID = $row['DID'];
  $mDIDn = $row['mDIDn'];
}
