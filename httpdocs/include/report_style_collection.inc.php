<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_division.inc.php";

if (isset($_GET['s'])) {
  $PRID = $_GET['s'];
}
?>

<form class="" style="position:absolute; top:10%; height:4%; left:68%; width:30%;" action="include/report_style_collection_act.inc.php" method="post">
  <input type="hidden" name="PRID" value="<?php echo $PRID ?>">
  <select style="position:absolute; top:6%; height:80%; left:3%; width:50%;" name="CLID">
    <option value="">Select Collection</option>
    <?php
      include 'from_ACID_select_collection.inc.php';
    ?>
  </select>
  <button class="entbtn" style="position:absolute; top:6%; height:80%; left:60%; width:35%;" type="submit" name="addcoll">Add Collection</button>
</form>

<table class="trs" style="position:absolute; top:15%; left:68%; width:30%;">
  <tr>
    <th>Collection</th>
    <th></th>
  </tr>
  <?php
  $sql = "SELECT cl.ID as CLID
            , cl.name as CLIDn
          FROM collection cl
          	, prod_ref pr
	          , prod_ref_to_collection prc
          WHERE pr.id = ?
          AND prc.PRID = pr.ID
          AND prc.CLID = cl.ID
          AND cl.name NOT IN ('Select Collection')
          ORDER BY cl.name
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-rscl</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $PRID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      $CLID = $row['CLID'];
      $CLIDn = $row['CLIDn'];
      ?>
      <tr>
        <td><?php echo $CLIDn ?></td>
      </tr>
      <?php
    }
  }
  ?>
</table>
<?php
