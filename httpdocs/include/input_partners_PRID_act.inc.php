<?php
session_start();
include 'dbconnect.inc.php';
$UID = $_SESSION['UID'];
// echo "<b>include/input_partners_PRID_act.inc.php</b>";

if (!isset($_POST['addpPRID']) && !isset($_POST['canpPRID'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['canpPRID'])) {
  // echo "<br>Return to the Users page";
  $url = $_POST['url'];
  header("Location:../$url");
  exit();
}elseif (isset($_POST['addpPRID'])) {
  include 'dbconnect.inc.php';

  $OIID = $_POST['OIID'];
  $pPRID = $_POST['pPRID'];
  $url = $_POST['url'];

  $td = date('U');

  // echo "<br>OIID = $OIID";
  // echo "<br>pPRID = $pPRID";
  echo "<br>td = $td";
  echo "<br>UID = $UID";

  // Get the previous PRID
  $opPRID = $_POST['opPRID'];
  // Add to the log_order_item
  $sql = "INSERT INTO log_order_item
            (OIID, PRID, UID, inputtime)
          VALUES
            (?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-ippa</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $OIID, $PRID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }



  // Update the prod_ref with the new product type
  $sql = "UPDATE order_item
          SET pPRID = ?
          WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-fapta</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $pPRID, $OIID);
    mysqli_stmt_execute($stmt);
  }
  header("Location:../$url");
  exit();
}
