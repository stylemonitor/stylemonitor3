<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_factory_section_loading.inc.php";

$CID = $_SESSION['CID'];
$td = date('U');

include 'from_CID_count_section_totals.inc.php';
?>

<div class="cpsty" style="color:<?php echo $hc ?>; background-color:#a7e0f5"><?php echo "Summary of work by Section"?></div>
<table class="trs" style="position:relative; top:0.25%;">
  <tr>
    <th style="width:20%;"></th>
    <th style="width:10%;">On Order</th>
    <th style="width:10%; text-align:center;">Awaiting</th>
    <th style="width:10%; text-align:center;"><a style="text-decoration:none" href="loading.php?L&mp1">1</a></th>
    <th style="width:10%; text-align:center;"><a style="text-decoration:none" href="loading.php?L&mp2">2</a></th>
    <th style="width:10%; text-align:center;"><a style="text-decoration:none" href="loading.php?L&mp3">3</a></th>
    <th style="width:10%; text-align:center;"><a style="text-decoration:none" href="loading.php?L&mp4">4</a></th>
    <th style="width:10%; text-align:center;"><a style="text-decoration:none" href="loading.php?L&mp5">5</a></th>
    <th style="width:10%; text-align:center;">W/house</th>
  </tr>

  <tr>
    <td style="width:20%;">Items in Section</td>
    <td style="width:10%;"><?php echo $OIQIDtq ?></td>
    <td style="width:10%; text-align:center; border-right:thin solid grey;"><?php echo $AWAITING ?></td>
    <td style="width:10%; text-align:center; border-right:thin solid grey;"><a style="text-decoration:none" href="loading.php?L&mp1"><?php echo $stotal1 ?></a></td>
    <td style="width:10%; text-align:center; border-right:thin solid grey;"><a style="text-decoration:none" href="loading.php?L&mp2"><?php echo $stotal2 ?></a></td>
    <td style="width:10%; text-align:center; border-right:thin solid grey;"><a style="text-decoration:none" href="loading.php?L&mp3"><?php echo $stotal3 ?></a></td>
    <td style="width:10%; text-align:center; border-right:thin solid grey;"><a style="text-decoration:none" href="loading.php?L&mp4"><?php echo $stotal4 ?></a></td>
    <td style="width:10%; text-align:center; border-right:thin solid grey;"><a style="text-decoration:none" href="loading.php?L&mp5"><?php echo $stotal5 ?></a></td>
    <td style="width:10%; text-align:center;"><?php echo $WAREHOUSE ?></td>
  </tr>

  <tr>
    <td style="width:20%;">Orders in Section</td>
    <td style="width:10%;"><?php echo $cORtot ?></td>
    <td style="width:10%; text-align:center; border-right:thin solid grey;"><?php echo $cORst_awt ?></td>
    <td style="width:10%; text-align:center; border-right:thin solid grey;"><a style="text-decoration:none" href="loading.php?L&mp1"><?php echo $cORst1 ?></a></td>
    <td style="width:10%; text-align:center; border-right:thin solid grey;"><a style="text-decoration:none" href="loading.php?L&mp2"><?php echo $cORst2 ?></a></td>
    <td style="width:10%; text-align:center; border-right:thin solid grey;"><a style="text-decoration:none" href="loading.php?L&mp3"><?php echo $cORst3 ?></a></td>
    <td style="width:10%; text-align:center; border-right:thin solid grey;"><a style="text-decoration:none" href="loading.php?L&mp4"><?php echo $cORst4 ?></a></td>
    <td style="width:10%; text-align:center; border-right:thin solid grey;"><a style="text-decoration:none" href="loading.php?L&mp5"><?php echo $cORst5 ?></a></td>
    <td style="width:10%; text-align:center;"><?php echo $cORwhs ?></td>
  </tr>

</table>
