<?php
// echo "whats_being_made_per_order_item.sql.php";
include 'dbconnect.inc.php';

$sql = "SELECT DISTINCT
-- whats_being_made_per_order_item.sql
     Mvemnt.ORDER_ID AS OID
     , Mvemnt.O_ITEM_ID AS OIID
     , Mvemnt.ordQty AS OIQIDq
     , Mvemnt.ordQty + SUM(Mvemnt.ORDER_INCR - Mvemnt.ORDER_DECR) AS NEW_OIQIDq
     , SUM(Mvemnt.INTO_STG1 + Mvemnt.COR_IN_STG1) AS Stage1
FROM (
    SELECT DISTINCT oimr.ID AS OIMR_ID
         , o.ID AS ORDER_ID
         , oi.ID AS O_ITEM_ID
         , oiq.order_qty AS ordQty
         , IF((CASE WHEN oimr.ID =  3 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  3 THEN SUM(opm.omQty) END)) AS INTO_STG1
         , IF((CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END)) AS ORDER_INCR
         , IF((CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END)) AS ORDER_DECR
         , IF((CASE WHEN oimr.ID = 23 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 23 THEN SUM(opm.omQty) END)) AS COR_IN_STG1
    FROM order_placed_move opm
         INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
         INNER JOIN order_placed op ON opm.OPID = op.ID
         INNER JOIN order_item oi ON op.OIID = oi.ID
         INNER JOIN orders o ON oi.OID = o.ID
         INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
    WHERE oi.ID = ?
    GROUP BY oi.ID, oimr.ID
   ) Mvemnt INNER JOIN order_item oi ON Mvemnt.O_ITEM_ID = oi.ID
            INNER JOIN orders o ON Mvemnt.ORDER_ID = o.ID
GROUP BY Mvemnt.O_ITEM_ID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-wbmpoi</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OIID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $NewOrdQty = $row['NEW_OIQIDq'];
  $MP1 = $row['Stage1'];
}
