<?php
include 'include/dbconnect.inc.php';
// echo '<br><b>form_new_company_user.inc.php</b>';

$CID = $_SESSION['CID'];
include 'from_CID_count_company_division.inc.php';

if (isset($_GET['usp'])) {
  $ret = "can_new_user1";
}else {
  $ret = "can_new_user";
}

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'New Company Users Registration';
$pddhbc = '#fffddd';
// include 'page_description_date_header.inc.php';

if (!isset($_GET['usp'])) {
  ?>
  <!-- <div class="acjSM" style="position:absolute; top:9%; height:80%; left:0%; width:100%; font-size:150%; background-color: #f1f1f1; z-index:1;">
    <br>
    <p>To add another user, fill out the form below</p>
    <br>
    <p>They will then be sent an email to confirm their email address</p>
    <br>
    <p>You need to select the type of user they are.</p>
    <br><br>
    <p>You need a minimum of two ADMIN users</p>
  </div> -->
  <?php
}else {
  // code...
}

// Check how many sections the associate company has
include 'check_count_countries_for_CID_associates.inc.php';
// IF 1 then use the CYID and TZID for the 'parent company'
// if > 1 then you need to select which company and where they are

?>
<div style="position:absolute; top:9%; height:90%; left:0%; width:100%; background-blend-mode: overlay;"></div>

<div style="position:absolute; top:19%; height:38%; left:19%; width:62%; text-align:center; font-size: 150%; font-weight: bold; text-indent:2%; background-color:<?php echo $addRecCol1 ?>; border: 3px solid grey; border-radius: 8px;">
  Add a Company User
</div>

<form action="include/form_new_company_user_act.inc.php" method="POST">

  <input class="INbtn1" type="hidden" name="UID" value="<?php echo $UID ?>">
  <input class="INbtn1" type="hidden" name="ACID" value="<?php echo $ACID ?>">
  <?php
  if ($cCYID == 1) {
    ?>
    <input class="INbtn1" type="hidden" name="CYID" value="<?php echo $CYID ?>">
    <input class="INbtn1" type="hidden" name="TZID" value="<?php echo $TZID ?>">
    <?php
  }


  if (isset($_GET['f'])) { $UIDf = $_GET['f'];
    ?>
    <input autofocus required class="INbtn1" style="position:absolute; top:28%; height:4%; left:30%; width:19.5%; font-size:130%; text-align:center; background-color: #a5d3f5; z-index:1;" type="text" name="first" placeholder="First name" value="<?php echo $UIDf ?>">
  <?php }else {
    ?>
    <input autofocus required class="INbtn1" style="position:absolute; top:28%; height:4%; left:30%; width:19.5%; font-size:130%; text-align:center; background-color: #a5d3f5; z-index:1;" type="text" name="first" placeholder="First name" value="">
    <?php
  }

  if (isset($_GET['s'])) { $UIDs = $_GET['s'];
    ?>
    <input autofocus required class="INbtn1" style="position:absolute; top:28%; height:4%; left:50.5%; width:19.5%; font-size:130%; text-align:center; background-color: #a5d3f5; z-index:1;" type="text" name="sur" placeholder="Surname" value="<?php echo $UIDs ?>">
  <?php }else {
    ?>
    <input autofocus required class="INbtn1" style="position:absolute; top:28%; height:4%; left:50.5%; width:19.5%; font-size:130%; text-align:center; background-color: #a5d3f5; z-index:1;" type="text" name="sur" placeholder="Surname" value="">
    <?php
  }

  if (isset($_GET['o'])) { $UIDo = $_GET['o'];
    ?>
    <input required class="INbtn1" style="position:absolute; top:33%; height:4%; left:30%; width:40%; font-size:130%; text-align:center; background-color: #a5d3f5; z-index:1;" type="text" name="job_title" placeholder="Job title" value="<?php echo $UIDo ?>">
  <?php }else {
    ?>
    <input required class="INbtn1" style="position:absolute; top:34%; height:4%; left:30%; width:40%; font-size:130%; text-align:center; background-color: #a5d3f5; z-index:1;" type="text" name="job_title" placeholder="Job title" value="">
    <?php
  }
  ?>
  <input required class="INbtn1" style="position:absolute; top:40%; height:4%; left:30%; width:40%; font-size:130%; text-align:center; background-color: #a5d3f5; z-index:1;" type="text" name="email" placeholder="User company email address" value="">
  <select class="" style="position:absolute; top:47%; height:4%; left:30.5%; width:40.5%; text-align:center; z-index:1; " name="userType">
    <option value="">Select User Privilege</option>
    <?php
    include 'include/from_CID_select_user_authority_level.inc.php';
    ?>
  </select>

  <?php
  if ($cDID == 0) {
    include 'from_CID_get_min_company_division.inc.php';
    ?>
    <input hidden type="text" name="DID" value="<?php echo "$mDID"; ?>">
    <?php
  }else {
    ?>
    <select class="" style="position:absolute; top:52%; height:4%; left:30.5%; width:40.5%; text-align:center; z-index:1; " name="DID">
      <?php
      include 'include/from_CID_select_division.inc.php';
      ?>
    </select>
    <?php
  }
  ?>
  <button class="entbtn" style="position:absolute; top:52%; height:4%; left:35%; width:10%; background-color: <?php echo $fsvCol ?>; padding-bottom:1%; z-index:1;" type="submit" name="new_user">Enter</button>
  <button formnovalidate class="entbtn" style="position:absolute; top:52%; height:4%; left:55%; width:10%; background-color: <?php echo $fcnCol ?>; padding-bottom:1%; z-index:1;" type="submit" name="<?php echo $ret ?>">Cancel</button>
</form>
