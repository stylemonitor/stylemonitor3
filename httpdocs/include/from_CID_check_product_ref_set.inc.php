<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_check_product_ref_set.inc.php</b>";
$CID = $_SESSION['CID'];

$sql = "SELECT COUNT(pr.ID) as cPRID
        FROM company c
          , associate_companies ac
          , prod_ref pr
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND pr.ACID = ac.ID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-facpr</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cPRID = $row['cPRID'];
}
