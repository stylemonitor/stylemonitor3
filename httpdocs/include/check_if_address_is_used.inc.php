<?php
// echo "<br><b>check_if_address_is_used.inc.php</b>";

$sql = "SELECT ID as AID
        FROM addresses
        WHERE CYID = ?
        AND TID = ?
        AND addn = ?
        AND add1 = ?
        AND add2 = ?
        AND city = ?
        AND pcode = ?
        AND county = ?
        AND tel = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-ciaiu";
}else {
  mysqli_stmt_bind_param($stmt,"sssssssss", $CYID, $TZID, $addn, $add1, $add2, $city, $pcode, $county, $tel);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $AID  = $row['AID'];
}
