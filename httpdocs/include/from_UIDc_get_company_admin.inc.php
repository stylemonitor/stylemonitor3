<?php
include 'dbconnect.inc.php';
// echo "include/from_UIDc_get_company_admin.inc.php";
// Is the company name the one the user is registered to
$sql = "SELECT u.ID as UID
          , u.firstname as UIDf
          , u.email as UIDe
          , u.privileges as UIDp
        FROM company c
            , division d
            , associate_companies ac
            , company_division_user cdu
            , users u
        WHERE c.name = ?
        AND cdu.DID = d.ID
        AND d.ACID = ac.ID
        AND ac.CID = c.ID
        AND cdu.UID = u.ID
        AND u.privileges = 1
        AND u.active = 1
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fugca</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $UID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $caUID = $row['UID'];
  $caUIDf = $row['UIDf'];
  $caUIDe = $row['UIDe'];
  $caUIDp = $row['UIDp'];
}
