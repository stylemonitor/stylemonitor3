<?php
include 'dbconnect.inc.php';
// echo "<br>include/form_user_details_act.inc.php";
if (!isset($_POST['edit_user_det']) && !isset($_POST['back_user_det']) && !isset($_POST['chg_pwd']) && !isset($_POST['can_pwd'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['edit_user_det'])) {
  $UID = $_SESSION['UID'];
  // echo "<br>Return to the Users page";
  header("Location:../company.php?C&usr&usb&u=$UID");
  exit();
}elseif (isset($_POST['back_user_det'])) {
  $UID = $_SESSION['UID'];
  // echo "<br>Return to the Users page";
  header("Location:../company.php?C&usa");
  exit();
}elseif (isset($_POST['can_pwd'])) {
  // echo "<br>Return to the Users page";
  header("Location:../company.php?C&usa");
  exit();
}elseif (isset($_POST['chg_pwd'])) {
  // echo "<br>Return to the Users page";
  header("Location:../company.php?C&rpw");
  exit();
}
