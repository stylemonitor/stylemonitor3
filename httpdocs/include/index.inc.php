<?php
// echo "include/index.inc.php";

if (isset($_GET['a'])) {
  ?>
  <div style="position:absolute; top:0%; height:100%; left:0%; width:100%; opacity:1; z-index:1;">
    <br>
    <p style="color:#840c0c; font-size:600%;"><b>Development site<b></p>
    <br><br><br>
    <p style="color:purple; font-size:225%; font-weight: bold;">The development Site for StyleMonitor.online</p>
    <br><br><br>
    <p style="font-size:175%;">Connecting companies around the world</p>
    <br><br><br>
    <p style="font-size:150%;">Stop guessing - take control</p>
    <br><br>
    <p style="font-size:150%;">Register your interest</p>
    <br><br><br>
    <p style="font-size:250%;"><b>S</b>tyle<b>M</b>onitor - <b>S</b>haring <b>M</b>ore</p>

    <!-- <br><br><br>
    <br><br><br>
    <br><br><br>
    <br><br><br>
    <p style="font-size:200%; font-weight:bold;">We are currently updating the system</p>
    <br><br>
    <p style="font-size:200%; font-weight:bold;">and will be back online soon</p>
    <br><br>
    <p style="font-size:200%; font-weight:bold;">Thank you for your patience</p>
    <br><br>
    <p style="font-size:200%; font-weight:bold;">StyleMonitor Team</p> -->


    <!-- <img src="include/SM-web02.jpg" style="position:absolute; top:5%; height:90%; left:0%; width:100%; opacity:0.2;" alt=""> -->
  </div>
  <?php
}elseif (isset($_GET['c'])) {
  include 'include/form_email_us.inc.php';
}elseif (isset($_GET['f'])) {
  include 'include/form_forgot_pwd.inc.php';
}elseif (isset($_GET['h'])) {
  // echo "Should be sent to include/about.inc.php, but not sending us anywhere";
  include 'include/about.inc.php';
}elseif (isset($_GET['i'])) {
  // echo "Should be sent to include/form_company_used.inc.php, but not sending us anywhere";
  if (isset($_GET['CUS'])) {
    include 'form_company_used.inc.php';
  }elseif (isset($_GET['UEM'])) {
    include 'include/form_email_used.inc.php';
  }
  include 'include/form_reg.inc.php';
}elseif (isset($_GET['l'])) {
  include 'include/form_login.inc.php';
  ?>
  <!-- <br>
  <p style="font-size:200%; font-weight:bold;">We will be updating the system</p>
  <br><br>
  <p style="font-size:200%; font-weight:bold;">on SUNDAY 21st February between 8am and 8pm UK time</p>
  <br><br>
  <p style="font-size:200%; font-weight:bold;">During this period you will be unable to login</p> -->
  <?php
}elseif (isset($_GET['q'])) {
  // echo "Should be sent to include/FAQ.inc.php, but not sending us anywhere";
  include 'include/FAQ.inc.php';
}elseif (isset($_GET['v'])) {
  $vkey = $_GET['v'];
  ?>
  <br><br><br><br><br><br>
  <br><br><br><br><br><br>
  <p style="color:purple; font-size:175%; font-weight: bold;">Thank you for verifing your email</p>
  <br><br><br>
  <p style="color:purple; font-size:175%; font-weight: bold;">A user password has now been sent to you</p>
  <br><br><br>
  <p style="color:purple; font-size:175%; font-weight: bold;">Enjoy having a looking around</p>

  <?php
}elseif (isset($_GET['FPW'])) {
  include 'form_forgot_pwd.inc.php';
}elseif (isset($_GET['U'])) {
  include 'include/reg_email_used.inc.php';
}elseif (isset($_GET['X'])) {
  include 'include/licence_limit.inc.php';
}elseif (isset($_GET['1'])) {
  include 'include/form_email_us_thanks.inc.php';
}elseif (isset($_GET['LXP'])) {
  // trial/full licence has expired
  // echo "You can extend your licence by 2 weeks";
  include 'include/extend_trial.inc.php';
}elseif (isset($_GET['RLC'])) {
  ?>
  <div style="position:absolute; top:0%; height:100%; left:0%; width:100%; opacity:1; z-index:1;">
    <br>
    <p style="color:#840c0c; font-size:800%;"><b>S</b>tyle<b>M</b>onitor</p>
    <br><br><br>
    <p style="color:purple; font-size:175%; font-weight: bold;">Your TRIAL LICENCE has been renewed</p>
    <br><br>
    <p style="font-size:150%;">for a further 2 weeks</p>
    <br><br>
    <p style="font-size:150%;">We look forward to you upgrading your licence soon</p>
    <br><br>
    <p style="font-size:150%;">Thank you for your continued interest</p>
    <br><br><br>
    <p style="font-size:250%;"><b>S</b>tyle<b>M</b>onitor - <b>S</b>haring <b>M</b>ore</p>
  </div>
  <?php
}
else{
  ?>
  <div style="position:absolute; top:0%; height:100%; left:0%; width:100%; opacity:1; z-index:1;">
    <!-- <br>nothing set -->
    <br>
     <p style="color:#840c0c; font-size:800%;"><b>S</b>tyle<b>M</b>onitor</p>
     <br><br><br>
     <p style="color:purple; font-size:225%; font-weight: bold;">The development site for <b>S</>tyle<b>M</b>onitor</p>
     <br><br><br>
     <p style="font-size:175%;">Creating a simpler way of monitoring your order</p>
     <br>
     <br><br>
     <p style="font-size:150%;">Stop guessing - take control</p>
     <br><br>
     <p style="font-size:150%;">Register your interest</p>
     <br><br><br>
     <p style="font-size:250%;"><b>S</b>tyle<b>M</b>onitor - <b>S</b>haring <b>M</b>ore</p>
  </div>
    <!-- <img src="include/SM-web02.jpg" style="position:absolute; top:5%; height:90%; left:0%; width:100%; opacity:0.1;" alt=""> -->
<?php
}
?>
