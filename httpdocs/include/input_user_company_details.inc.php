<?php
include 'dbconnect.inc.php';
include 'create_company_SMIC.inc.php';
// echo  "<br><b>input_user_company_details.inc.php</b>";
// echo  "<br>Recommending company (rCID) : $rec_CID";
// echo  "<br>Company type (CTID) : $CTID";
// echo  "<br>Company country base (CYID) : $CYID";
// echo  "<br>Input user (UID) : $UID";
// echo  "<br>Company name (CIDn) : $CIDn";
// echo  "<br>Company SMIC (SMIC) : $SMIC";
// echo  "<br>Inputtime (td) : $td";

$sql = "INSERT INTO company
          (rCID, CTID, CYID, TID, UID, name, SMIC, inputtime)
        VALUES
          (?,?,?,?,?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iucd";
}else {
  mysqli_stmt_bind_param($stmt,"ssssssss", $rec_CID, $CTID, $CYID, $TZID, $UID, $CIDn, $SMIC, $td);
  mysqli_stmt_execute($stmt);
}

// Add an address to the company
$sql = "INSERT INTO company_associates_address
          (ACID, AID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iucd2";
}else {
  mysqli_stmt_bind_param($stmt,"ssss", $ACID, $AID, $UID, $td);
  mysqli_stmt_execute($stmt);
}
