<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_PRID_count_collections.inc.php</b>";
$sql = "SELECT COUNT(cl.ID) as cCLID
        FROM prod_ref pr
          , prod_ref_to_collection prtc
          , collection cl
        WHERE pr.ID = ?
        AND prtc.PRID = pr.ID
        AND prtc.CLID = cl.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fprc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $PRID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $cCLID   = $row['cCLID'];
}
