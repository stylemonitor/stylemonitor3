<?php
include 'dbconnect.inc.php';
// echo "order_status_summary_warehouse.inc.php";
include 'set_urlPage.inc.php';
$CID = $_SESSION['CID'];

$td = date('U');
$td = date('d M Y',$td);
// $pddh = 'Order Status Warehouse';
// $pddhbc = '#fffddd';
// include 'page_description_date_header.inc.php';

include 'from_CID_count_Associate_Sales_orders_whs.inc.php';
include 'from_CID_count_Associate_Purchase_orders_whs.inc.php';
include 'from_CID_count_Partner_Sales_orders_whs.inc.php';
include 'from_CID_count_Partner_Purchase_orders_whs.inc.php';

$CID = $_SESSION['CID'];

$rtnPage = "home.php?H&tab=$tab&ot=$OTID&sp=$sp";

$sql = "SELECT CASE WHEN ORDER_STATUS.DEL_WEEK = yearweek(curdate(),7)   THEN IF(ORDER_STATUS.DEL_DATE < curdate(),'1','2')
-- SQL is order_status_summary_whs.sql
            WHEN ORDER_STATUS.DEL_WEEK = yearweek(curdate(),7)+1 THEN '3'
            WHEN ORDER_STATUS.DEL_WEEK > yearweek(curdate(),7)+1 THEN '4'
            ELSE '1'
       END AS STATUS
       , ORDER_STATUS.OTID as OTID
       , ORDER_STATUS.SAM_PROD AS SAM_PROD
       , COUNT(DISTINCT ORDER_STATUS.OIID) AS COUNT_OIID
       , COUNT(DISTINCT ORDER_STATUS.OID) AS COUNT_OID
       , CASE WHEN ORDER_STATUS.OTID IN (1,3) THEN(SUM(ORDER_STATUS.ORDER_QTY) - ORDER_STATUS.WHS_CLIENT)
              WHEN ORDER_STATUS.OTID IN (2,4) THEN(SUM(ORDER_STATUS.ORDER_QTY) - ORDER_STATUS.WHS_SUPPLIER)
         END AS QTY
FROM
(
SELECT DISTINCT
   Mvemnt.OID AS OID
   , Mvemnt.O_ITEM_ID AS OIID
   , yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) AS DEL_WEEK
   , from_unixtime(oidd.item_del_date,'%Y-%m-%d') AS DEL_DATE
   , oiq.order_qty AS ORDER_QTY
   , oi.samProd AS SAM_PROD
   , CASE WHEN o.tCID = o.fCID
          THEN o.OTID
          ELSE CASE WHEN o.tCID = ? THEN 3
               ELSE 4
               END
     END AS OTID
   , SUM(Mvemnt.INTO_WHOUSE + Mvemnt.FROM_CLIENT + Mvemnt.COR_IN_WHOUSE + Mvemnt.CORR_OUT_CLIENT - Mvemnt.REJ_BY_WHOUSE - Mvemnt.TO_CLIENT
         - Mvemnt.COR_OUT_WHOUSE - Mvemnt.CORR_IN_CLIENT) AS WHS_CLIENT
   , SUM(Mvemnt.INTO_WHOUSE + Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.COR_IN_WHOUSE + Mvemnt.CORR_OUT_SUPPLIER - Mvemnt.REJ_BY_WHOUSE
         - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.COR_OUT_WHOUSE - Mvemnt.CORR_IN_SUPPLIER) AS WHS_SUPPLIER
FROM (
  SELECT DISTINCT
         oimr.ID AS OIMR_ID
         , oimr.reason AS Reason
         , oi.ID AS O_ITEM_ID
         , opm.ID AS OPMID
         , o.ID AS OID
         , IF((CASE WHEN oimr.ID =  8 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  8 THEN SUM(opm.omQty) END)) AS INTO_WHOUSE
         , IF((CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END)) AS REJ_BY_WHOUSE
         , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
         , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
         , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
         , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
         , IF((CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END)) AS COR_IN_WHOUSE
         , IF((CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END)) AS COR_OUT_WHOUSE
         , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
         , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
         , IF((CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END)) AS CORR_OUT_SUPPLIER
         , IF((CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END)) AS CORR_IN_SUPPLIER

  FROM order_placed_move opm
       INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
       INNER JOIN order_placed op ON opm.OPID = op.ID
       INNER JOIN order_item oi ON op.OIID = oi.ID
       INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
       INNER JOIN orders o ON oi.OID = o.ID
  GROUP BY opm.ID, oi.ID, oimr.ID
 ) Mvemnt INNER JOIN order_item oi ON Mvemnt.O_ITEM_ID = oi.ID
          INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
          INNER JOIN orders o ON oi.OID = o.ID
          INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
          INNER JOIN company c ON o.fCID = c.ID
          INNER JOIN company cS ON o.tCID = cS.ID
          INNER JOIN associate_companies ac ON o.fACID = ac.ID
          INNER JOIN associate_companies acS ON cS.ID = acS.CID
          INNER JOIN division d ON o.fDID = d.ID
          INNER JOIN division dS ON acS.ID = dS.ACID
WHERE (o.fCID = ? OR o.tCID = ?)
  AND oi.itComp = 0  -- Only include open order items
GROUP BY OIID
ORDER BY OIID ASC
) ORDER_STATUS
GROUP BY STATUS, SAM_PROD, OTID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b><br><br><br><br><br><br>FAIL-fdcs</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $OTID = $row['OTID'];
    $sp = $row['SAM_PROD'];
    $tab = $row['STATUS'];
    $QTY = $row['QTY'];
    $cOIID = $row['COUNT_OIID'];
    $cOID = $row['COUNT_OID'];

    if ($CasOIDwhs == 0) {
    }else {
      ?>
      <!-- SALES ORDERS -->
      <div style="position:absolute; top:16%; left:25%; width: 50%; text-align:center; background-color:#f7cc6c; border:thin solid black; z-index:1;"> Associate SALES Orders in the Warehouse</div>
      <div style="position:absolute; top:19%; left:35%; width:10%; text-align:center;">Over Due</div>
      <div style="position:absolute; top:19%; left:45%; width:10%; text-align:center;">This Week</div>
      <div style="position:absolute; top:19%; left:55%; width:10%; text-align:center;">Next Week</div>
      <div style="position:absolute; top:19%; left:65%; width:10%; text-align:center;">Later</div>
      <div style="position:absolute; top:22%; left:25%; width:10%; text-align:left; text-indent:2%; background-color:#ffffdd;">Samples</div>
      <div style="position:absolute; top:25%; left:25%; width:10%; text-align:left; text-indent:2%;">Production</div>

      <?php
      // SAMPLES
      if (($tab == 1) && ($OTID == 1) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:22%; left:35%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
          <div style="position:absolute; top:22%; height:2.5%; left:35%; width:9.9%; text-align:center; border:thin solid black;">
          </div>
        <?php

      if (($tab == 2) && ($OTID == 1) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:22%; left:45%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:22%; height:2.5%; left:45%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 3) && ($OTID == 1) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:22%; left:55%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:22%; height:2.5%; left:55%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 4) && ($OTID == 1) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:22%; left:65%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:22%; height:2.5%; left:65%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php


      // PRODUCTION
      if (($tab == 1) && ($OTID == 1) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:25%; left:35%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:25%; height:2.6%; left:35%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 2) && ($OTID == 1) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:25%; left:45%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:25%; height:2.6%; left:45%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 3) && ($OTID == 1) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:25%; left:55%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:25%; height:2.6%; left:55%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 4) && ($OTID == 1) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:25%; height:2.6%; left:65%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:25%; height:2.6%; left:65%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php
    }

    if ($CapOIDwhs == 0) {
    }else {
      ?>
      <!-- PURCHASE Orders -->
      <div style="position:absolute; top:32%; left:25%; width: 50%; text-align:center; background-color:#bbbbff;">Associate PURCHASE Orders in the Warehouse</div>
      <div style="position:absolute; top:35%; left:35%; width:10%; text-align:center;">Over Due</div>
      <div style="position:absolute; top:35%; left:45%; width:10%; text-align:center;">This Week</div>
      <div style="position:absolute; top:35%; left:55%; width:10%; text-align:center;">Next Week</div>
      <div style="position:absolute; top:35%; left:65%; width:10%; text-align:center;">Later</div>
      <div style="position:absolute; top:38%; left:25%; width:10%; text-align:left; text-indent:2%; background-color:#ffffdd;">Samples</div>
      <div style="position:absolute; top:41%; left:25%; width:10%; text-align:left;">Production</div>
      <?php

      // SAMPLES
      if (($tab == 1) && ($OTID == 2) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:38%; left:35%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:38%; height:2.6%; left:35%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 2) && ($OTID == 2) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:38%; left:45%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:38%; height:2.6%; left:45%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 3) && ($OTID == 2) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:38%; left:55%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:38%; height:2.6%; left:55%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 4) && ($OTID == 2) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:38%; left:65%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:38%; height:2.6%; left:65%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php


      // PRODUCTION
      if (($tab == 1) && ($OTID == 2) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:41%; left:35%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:41%; height:2.6%; left:35%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 2) && ($OTID == 2) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:41%; left:45%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:41%; height:2.6%; left:45%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 3) && ($OTID == 2) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:41%; left:55%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:41%; height:2.6%; left:55%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 4) && ($OTID == 2) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:41%; left:65%; width:10%; text-align:center; z-index: 1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:41%; height:2.6%; left:65%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php
    }

    // PARTNER SALES
    if ($CpsOIDwhs == 0) {
    }else {
      ?>
      <div style="position:absolute; top:48%; left:25%; width: 50%; text-align:center; background-color:#d68e79;">PARTNER SALES Orders in the Warehouse</div>
      <div style="position:absolute; top:51%; left:35%; width:10%; text-align:center;">Over Due</div>
      <div style="position:absolute; top:51%; left:45%; width:10%; text-align:center;">This Week</div>
      <div style="position:absolute; top:51%; left:55%; width:10%; text-align:center;">Next Week</div>
      <div style="position:absolute; top:51%; left:65%; width:10%; text-align:center;">Later</div>
      <div style="position:absolute; top:54%; left:25%; width:10%; text-align:left; text-indent:2%; background-color:#ffffdd;">Samples</div>
      <div style="position:absolute; top:57%; left:25%; width:10%; text-align:left;">Production</div>
      <?php
      // SAMPLES
      if (($tab == 1) && ($OTID == 3) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:54%; left:35%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:54%; height:2.6%; left:35%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 2) && ($OTID == 3) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:54%; left:45%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:54%; height:2.6%; left:45%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 3) && ($OTID == 3) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:54%; left:55%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:54%; height:2.6%; left:55%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 4) && ($OTID == 3) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:54%; left:65%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:54%; height:2.6%; left:65%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php


      // PRODUCTION
      if (($tab == 1) && ($OTID == 3) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:57%; left:35%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:57%; height:2.6%; left:35%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 2) && ($OTID == 3) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:57%; left:45%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:57%; height:2.6%; left:45%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 3) && ($OTID == 3) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:57%; left:55%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:57%; height:2.6%; left:55%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 4) && ($OTID == 3) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:57%; left:65%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:57%; height:2.6%; left:65%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php
    }

    // PARTNER PURCHASE Orders
    if ($CppOIDwhs == 0) {
    }else {
      ?>
      <div style="position:absolute; top:64%; left:25%; width: 50%; text-align:center; background-color:#c9d1ce;">PARTNER PURCHASE Orders in the Warehouse</div>
      <div style="position:absolute; top:67%; left:35%; width:10%; text-align:center;">Over Due</div>
      <div style="position:absolute; top:67%; left:45%; width:10%; text-align:center;">This Week</div>
      <div style="position:absolute; top:67%; left:55%; width:10%; text-align:center;">Next Week</div>
      <div style="position:absolute; top:67%; left:65%; width:10%; text-align:center;">Later</div>
      <div style="position:absolute; top:70%; left:25%; width:10%; text-align:left; text-indent:2%; background-color:#ffffdd;">Samples</div>
      <div style="position:absolute; top:73%; left:25%; width:10%; text-align:left;">Production</div>
      <?php
      // SAMPLES
      if (($tab == 1) && ($OTID == 4) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:70%; left:35%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:70%; height:2.6%; left:35%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 2) && ($OTID == 4) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:70%; left:45%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:70%; height:2.6%; left:45%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 3) && ($OTID == 4) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:70%; left:55%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:70%; height:2.6%; left:55%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 4) && ($OTID == 4) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:70%; left:65%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:70%; height:2.6%; left:65%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php


      // PRODUCTION
      if (($tab == 1) && ($OTID == 4) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:73%; left:35%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:73%; height:2.6%; left:35%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 2) && ($OTID == 4) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:73%; left:45%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:73%; height:2.6%; left:45%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 3) && ($OTID == 4) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:73%; left:55%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:73%; height:2.6%; left:55%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php

      if (($tab == 4) && ($OTID == 4) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:73%; left:65%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:73%; height:2.6%; left:65%; width:9.9%; text-align:center; border:thin solid black;">
        </div>
        <?php
    }
  }
}
