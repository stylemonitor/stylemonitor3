<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>edit_company_name_act.inc.php</b>";

if (isset($_POST['ACID'])) {
  $ACID = $_POST['ACID'];
  $ct = $_POST['ct'];
}

if (!isset($_POST['update_company_name']) && !isset($_POST['cancel_company_name'])) {
  // echo "<br>WRONG METHOD USED";
}elseif (isset($_POST['cancel_company_name'])) {
  header("Location:../associates.php?A&asf&id=$ACID&ct=$ct");
  exit();
}elseif (isset($_POST['update_company_name'])) {
  // echo "<br>Creat a new company name, record and replace the old one";

  $UID = $_SESSION['UID'];
  $td = date('U');

  // Get the data from the form
  $ACID = $_POST['ACID'];
  $ct = $_POST['ct'];
  $AID = $_POST['AID'];

  $nCIDn = $_POST['nCIDn'];
  $CIDn = $_POST['CIDn'];
  // $CID = $_POST['CID'];

  include 'from_AID_get_associate_company_address.inc.php';

  $AID = $_POST['AID'];
  $naddn = $_POST['naddn'];
  $nadd1 = $_POST['nadd1'];
  $nadd2 = $_POST['nadd2'];
  $ncity = $_POST['ncity'];
  $npcode = $_POST['npcode'];
  $ncounty = $_POST['ncounty'];
  $ntel = $_POST['ntel'];

  // echo "<br>Revised Name: $naddn";
  // echo "<br>Revised Address line 1: $nadd1";
  // echo "<br>Revised Line 2: $nadd2";
  // echo "<br>Revised City: $ncity";
  // echo "<br>Revised Post code: $npcode";
  // echo "<br>Revised County: $ncounty";
  // echo "<br>Revised Tel: $ntel";

  if (empty($naddn) && empty($nadd1) && empty($nadd2) && empty($ncity) && empty($npcode) && empty($ncounty) && empty($ntel)) {
    header("Location:../associate.php?A&asf&id=$ACID&ct=$ct");
    exit();
  }

  if (empty($naddn)) {$naddn = $addn;}else {$naddn = $naddn;}
  if (empty($nadd1)) {$nadd1 = $add1;}else {$nadd1 = $nadd1;}
  if (empty($nadd2)) {$nadd2 = ' - ';}else {$nadd2 = $nadd2;}
  if (empty($ncity)) {$ncity = $city;}else {$ncity = $ncity;}
  if (empty($npcode)) {$npcode = $pcode;}else {$npcode = $npcode;}
  if (empty($ncounty)) {$ncounty = $county;}else {$ncounty = $ncounty;}
  if (empty($ntel)) {$ntel = $tel;}else {$ntel = $ntel;}

  // echo "<br>NEW ADDRESS DETAIL";
  // echo "<br>Name: $naddn";
  // echo "<br>Address line 1: $nadd1";
  // echo "<br>Line 2: $nadd2";
  // echo "<br>City: $ncity";
  // echo "<br>Post code: $npcode";
  // echo "<br>County: $ncounty";
  // echo "<br>Tel: $ntel";

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to

    $sql = "INSERT INTO log_addresses
              (AID, ori_addn, ori_add1, ori_add2, ori_city, ori_pcode, ori_county, ori_tel, UID, inputtime)
            VALUES
              (?,?,?,?,?,?,?,?,?,?)
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-ecaa</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssssssssss", $AID, $addn, $add1, $add2, $city, $pcode, $county, $tel, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    $sql = "UPDATE addresses
            SET addn = ?
              , add1 = ?
              , add2 = ?
              , city = ?
              , pcode = ?
              , county = ?
              , tel = ?
            WHERE ID = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-ecaa1</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssssssss", $naddn, $nadd1, $nadd2, $ncity, $npcode, $ncounty, $ntel, $AID);
      mysqli_stmt_execute($stmt);
    }

  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  header("Location:../associates.php?A&asf&id=$ACID&ct=$ct");
  exit();
}
