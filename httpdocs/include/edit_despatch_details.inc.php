<?php
session_start();
include 'dbconnect.inc.php';
// echo "edit_despatch_details.inc.php";

if (isset($_GET['des'])) {
  $OIID = $_GET['oi'];
  $OPMID = $_GET['des'];
  $UID = $_SESSION['UID'];
  $td = date('U');
}

$sql = "SELECT opm.omQty as opmQTY
          , opm.type as OPMnote
        FROM order_placed_move opm
        WHERE opm.ID = ?
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-edd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OPMID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $opmQTY  = $row['opmQTY'];
  $OPMnote = $row['OPMnote'];
}

?>
<div class="cpsty" style="position:absolute;top:55%; height:4%; left:0%; width:18%; padding-top: 0.75%; border-radius: 0px 10px 10px 0px; background-color: #f2f29a; z-index:1;">
  Amount Despatched : <b><?php echo $opmQTY ?>
</div>
<div style="position:absolute; top:49%; height:5.35%; left:16%; width:84%; background-color: #ffffa9;">

</div>
<div style="position:absolute; top:50.5%; left:19%; width: 15%; text-align:right;"></b> Revised Qty</div>
<div style="position:absolute; top:50.5%; left:34%; width: 15%; text-align:right;">Note</div>
<form action="include/edit_despatch_details_act.inc.php" method="post">
  <input type="hidden" name="OIID" value="<?php echo $OIID ?>">
  <input type="hidden" name="OPMID" value="<?php echo $OPMID ?>">
  <input type="hidden" name="oriQTY" value="<?php echo $opmQTY ?>">
  <input type="hidden" name="oirNOTE" value="<?php echo $OPMnote ?>">
  <input autofocus type="text" style="position:absolute; top:50%; height:2%; left: 35%; width:5%;" name="revQTY" placeholder="<?php echo $opmQTY ?>" value="">
  <input type="text" style="position:absolute; top:50%; height:2%; left: 50%; width:30%;" name="revNote" placeholder="<?php echo $OPMnote ?> "value="">
  <button class="entbtn" style="position:absolute; top:50%; left:84%; width:6%;"type="update" name="update">Update</button>
  <button class="entbtn" style="position:absolute; top:50%; left:92%; width:6%;"type="cancel" name="cancel">Cancel</button>
</form>

<?php
