<?php
session_start();
include 'include/dbconnect.inc.php';
// echo "<b>form_new_orders_select_type.inc.php</b>";
?>
<div class="cpsty" style="color:black; background-color:#ffb974"></div>
<br>
<p>Within the STYLES section you can create your own Styles and Product Types.</p>
<br>
<p>You need at least 1 style within <b>S</b>tyle<b>M</b>onitor to go any further.</p>
<br>
<p>Once you have a style other tabs on the left side of the screen will appear.  You will then be able to place orders, update them and follow the work through your factory.</p>
<br>
<p>When you have added Product TYpes, Divisions etc you will have to select them.</p>
<br>
<p>Should you join with another company within <b>S</b>tyle<b>M</b>onitor you will share the information regarding production thus removing the need to send a production update every week.</p>
