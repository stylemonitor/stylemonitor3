<?php
include 'dbconnect.inc.php';
// include '<br><b>include/from_OIID_get_qty.inc.php</b>';

if (isset($_GET['oi'])) {
  $OIID = $_GET['oi'];
}
// get manufacturing unit placed quantity
$sql = "SELECT SUM(opm.omQty*oimr.numVal) as upQTY
        FROM orders o
          , order_placed op
          , order_placed_move opm
          , order_item_movement_reason oimr
          , order_item oi
        WHERE oi.ID = ?
        AND op.OIID = oi.ID
        AND opm.OPID = op.ID
        AND opm.OIMRID = oimr.ID
        -- AND oimr.ID IN (2,11,12) was but had started to give wrong result!!!
        AND oimr.ID IN (1,2,11,12)
        AND oi.OID = o.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fmgq</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OIID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $upQTY = $row['upQTY'];

  if (empty($upQTY)) {$upQTY = 0;}else {$upQTY = $upQTY;}

  global $upQTY;
}
