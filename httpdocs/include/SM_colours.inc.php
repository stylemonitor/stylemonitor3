<?php
include 'dbconnect.inc.php';
// echo "SM_colours.inc.php";

// basic page background colour
$bbCol = '#f1f1f1';

// Left Side Menu Tabs
// HOME - Order Status
  $homCol = '#b7d8ff';

// Associate SALES/CLIENT Order
$salAssCol = "#f7cc6c";
// Associate PURCHSE/SUPPLIER Order
$purAssCol = "#bbbbff";
// PARTNER SALES/CLIENT Order
$salPrtCol = "#d68e79";
// PARTNER PURCHASE/SUPPLIER Order
$purPrtCol = "#c9d1ce";


// HELP
$hlpCol = "#ffc557";

// WAREHOUSE TAB
  // SUMMARY TAB

  // goods outward
  $goCol = '#b7d8ff';
  // goods inward
  $giCol = '#ffe1d1';
  // suppliers warehouse
  $suCol = '#ebd1f5';

  // New
  $newCol = '#3bfe4a';


// Warehouse
$whsCol = 'pink';
// Completed and completed orders
// $comCol = 'red';
$comCol = '#8df8e0';
// Styles
$styCol = '#ffb974';

// Company Preference
// Associates
$assCol = '#5dffa2';
// Partners
$parCol = '#5199fc';

// COMPANY TAB
  // General
  $cnyCol = '#7ffad9';
  // Users Pending
  $uspCol = '#801c1c';
  // Users Active
  $usaCol = '#933636';
  // Users Suspended
  $ussCol = '#be6161';
  // Settings
  $setCol = '#cb8282';
  // YOU
  $youCol = '#ffcccc';


// How to...
$htoCol = '#e08df4';
// Contact
$conCol = '#676767';

// ACCEPT/REJECT
$accCol = "green";
$rejCol = "RED";



// Sample/Production ITEM Preference
// Sample
$samCol ='#ffed8d';
// Production
$proCol ='#dbacc3';


// Monitoring Point
// monitoring points
$mpCol = '#76ffb5';
// warehouse
$whsCol = '#ffffd1';
// sent items
$sentCol = '#aff3ff';
// mopvement comments
$movComCol = '#f3ffc1';
// Returned from Client
$cliRetCol = 'red';
// Received from Supplier
$supRevCol = 'white';

// Order Date Status
// overdue
// $odCol = 'green';
$odCol = '#fc4141';
// this week
$twCol = '#f7a0a0';
// next week
$nwCol = '#a2e293';
// later
$ltCol = '#e2eae3';

// Product Type
$prtCol = '#80b988';

// incomplete order
$incomCol = '#effcb9';

// ADD a Record
$addRecCol1  = '#83ce9d';
$addRecCol2 = '#36c969';



// REPORT
// Rejected Work
$rejCol = '#ff8383';
// Passed Work
$appCol = '#d9fabc';

// Required information on a form
$rqdCol = '#92fdd4';
// Optional infprmation on a form
$optCol = '#a4aec6';
// Dropdown menu colour
$ddmCol = '#ffee91';

// Form SAVE colour
$fsvCol = '#3bfe4a';
// Form CANCEL colour
$fcnCol = '#808080';

// EDIT colour
$edtCol = '#fcffa8';

// UPDATE Record
$upRecCol = '#f8ae64';

// Background Colour for RHS information tab headers
$rhsCol = '#c9d1ce';

// order_placed_move COLOUR
// forward movement of work
$opmColfwd = "#d1ff89";
// rejection of work back to section
$opmColbck = "#ffa286";
// correction of count from section
$opmColcor = "#c3ecf8";
// REQUEST for a change
$opmColreq = "#ff2727";
// APPROVAL of request
$opmColapp = "#59ff86";
// REJECTION of request
$opmColrej = "#e57d7d";

// Item change reason colour
$OIMRqty = '#fabc73';
$OIMRdte = '#76dfff';
$OIMRcan = '#ff76a0';
