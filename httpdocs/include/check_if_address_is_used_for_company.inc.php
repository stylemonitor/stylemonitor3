<?php
// echo "<br><b>check_if_address_is_used_for_company.inc.php</b>";

$sql = "SELECT ID as CAAID
        FROM company_associates_address
        WHERE ACID = ?
        AND AID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-ciaiu";
}else {
  mysqli_stmt_bind_param($stmt,"ss", $ACID, $AID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $CAAID  = $row['CAAID'];
}
