<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_get_associate_company.inc.php</b>";

if (isset($_POST['comp_ID'])) {
  $CID = $_POST['comp_ID'];
}else {
  $CID = $_SESIION['CID'];
}

$sql = "SELECT ac.ID as ASACID
          , ac.name as ACIDn
          , ct.name as CTIDn
          , cy.name as CYIDn
        FROM company c
          , associate_companies ac
          , company_type ct
          , country cy
        WHERE c.ID = ?
        AND ac.ID NOT IN (?)
        AND ac.CID = c.ID
        AND ac.CTID = ct.ID
        AND ac.CYID = cy.ID;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  // echo '<b>FAIL-fcgac</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $ASACID = $row['ASACID'];
  $ACIDn = $row['ACIDn'];
  $CTIDn = $row['CTIDn'];
  $CYIDn = $row['CYIDn'];
}
