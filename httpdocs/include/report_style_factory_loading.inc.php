<?php
session_start();
include 'dbconnect.inc.php';

include 'set_urlPage.inc.php';

// echo "include/report_style_factory_loading.inc.php";

$CID = $_SESSION['CID'];
$td = date('U');

include 'from_CID_count_order_overdue.inc.php';
include 'from_CID_count_order_thisweek.inc.php';
include 'from_CID_count_order_nextweek.inc.php';
include 'from_CID_count_order_later.inc.php';
include 'from_CID_count_order_new.inc.php'; // $cOIDnew
include 'from_CID_count_orders_in_work.inc.php'; // $cOIDiw
include 'from_UID_get_last_logout.inc.php'; // $LRIDout
include 'from_CID_count_section_qtys.inc.php';
include 'from_CID_count_section_totals.inc.php';

// Check the URL to see what page we are on
if (isset($_GET['p'])) { $p = $_GET['p']; }else{ $p = 0; }

// Set the rows per page
$r = 20;
// Check which page we are on
if ($p > 1) { $start = ($p * $r) - $r; }else { $start = 0; }

// sets the order type
// all open orders overdue
// if ($cOIDod == 0) {
// }

if (isset($_GET['mp1'])) {
  $s1 = 3;
  $s2 = 4;
  $s3 = 13;
  $s4 = 14;
  $s5 = 23;
  $s6 = 29;
  $secqty = $stotal1;
  $secord = $cORst1;
  $secbc = '#63a363';
}if (isset($_GET['mp2'])) {
  $s1 = 4;
  $s2 = 5;
  $s3 = 14;
  $s4 = 15;
  $s5 = 24;
  $s6 = 30;
  $secqty = $stotal2;
  $secord = $cORst2s;
  $secbc = '#63a3a3';
}if (isset($_GET['mp3'])) {
  $s1 = 5;
  $s2 = 6;
  $s3 = 15;
  $s4 = 16;
  $s5 = 25;
  $s6 = 31;
  $secqty = $stotal3;
  $secord = $cORst3;
  $secbc = '#6377a3';
}if (isset($_GET['mp4'])) {
  $s1 = 6;
  $s2 = 7;
  $s3 = 16;
  $s4 = 17;
  $s5 = 26;
  $s6 = 32;
  $secqty = $stotal4;
  $secord = $cORst4;
  $secbc = '#8f63a3';
}if (isset($_GET['mp5'])) {
  $s1 = 7;
  $s2 = 8;
  $s3 = 17;
  $s4 = 18;
  $s5 = 27;
  $s6 = 33;
  $secqty = $stotal5;
  $secord = $cORst5;
  $secbc = '#a3637c';
}if (isset($_GET['owh'])) {
  $s1 = 8;
  $s2 = 18;
  $s3 = 19;
  $s4 = 20;
  $s5 = 28;
  $s6 = 34;
  $secqty = $WAREHOUSE;
  $secord = $cORwhs;
  $secbc = '#ef80bb';
}

// Count how many orders in a section
// WHERE the qty of the order in the section <> 0

$sql = "SELECT COUNT(o.ID) as OID
        FROM orders o
        , order_item_movement_reason oimr
        , order_placed_move opm
        WHERE o.fCID = ?
        AND oimr.ID IN (1)
        AND opm.OIMRID = oimr.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rfl1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOID = $row['OID'];
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cOID / $r);
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;
// echo '<br>Start record is : '.$start;
// echo "<br>**** count_orders count : $cOID ****";

  ?>
    <div class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $secbc; ?>"><?php echo "Orders sorted by STYLE"?></div>
  <table class="trs" style="position:relative; top:0%;">

  <tr>
    <th style="width:2%;">Type</th>
    <th style="width:2%;">Name</th>
    <th style="width:7%; text-align:center;">O/r</th>
    <th style="width:25%; text-align:left; text-indent:5%;" style="width:7%;">Client</th>
    <th style="width:35%; text-align:left; text-indent:5%;">Reference</th>
    <th style="width:9%;">Section Qty</th>
    <th style="width:8%; padding-right:1%;">Order Qty</th>
    <th style="width:13%;">Due date</th>
    <th style="width:1%;"></th>
  </tr>
  <?php

$sql = "SELECT Mvemnt.Item_ID AS OIID
       , Mvemnt.order_type AS OTID
       , Mvemnt.orNos AS OIDon
       , Mvemnt.Order_ID AS Order_ID
       , Mvemnt.ord_item_nos AS OIIDon
       , cr.Client_ID AS req_CID
       , cr.Assoc_Client_Co AS ACID
       , cr.Assoc_Client_Name AS ACIDn
       , cr.Assoc_Supplier_Co AS ACIDp
       , cr.Assoc_Supplier_Name AS ACIDpn
       , Mvemnt.Order_Ref AS OREF
       , Mvemnt.order_qty AS OQTY
       , Mvemnt.Order_Ref AS OIIDtir
       , Mvemnt.prod_ref AS PRIDpr
       , Mvemnt.order_qty AS OIQIDq
       , Mvemnt.item_del_date AS OIDDIDd
       , Mvemnt.type_name AS Type
-- Through Stage
       , Mvemnt.INTO_STG - Mvemnt.OUT_OF_STG - Mvemnt.REJ_BY_NXT_STG + Mvemnt.REJ_BY_STG  + COR_IN_STG - COR_OUT_STG AS QTY_IN_STG
       , LastUser.LastInputTime AS LU
FROM
(
 -- Get Figures and convert NULL values to zero's
 SELECT Movement.Order_Placed_ID AS Order_Placed_ID
     , o.orNos
     , oiq.order_qty AS order_qty
     , oi.OID AS Order_ID
     , oi.ID AS Item_ID
     , oi.ord_item_nos
     , oi.their_item_ref
     , o.our_order_ref AS Order_Ref
     , o.tDID AS Division
     , o.fDID AS Partner_Division
     , ot.ID AS Order_type
     , ot.name AS type_name
     , oidd.item_del_date
     , pr.prod_ref
     , IF(sum(Movement.INTO_STG) IS NULL,0,sum(Movement.INTO_STG)) AS INTO_STG
     , IF(sum(Movement.OUT_OF_STG) IS NULL,0,sum(Movement.OUT_OF_STG)) AS OUT_OF_STG
     , IF(sum(Movement.REJ_BY_STG) IS NULL,0,sum(Movement.REJ_BY_STG)) AS REJ_BY_STG
     , IF(sum(Movement.REJ_BY_NXT_STG) IS NULL,0,sum(Movement.REJ_BY_NXT_STG)) AS REJ_BY_NXT_STG
     , IF(sum(Movement.COR_IN_STG) IS NULL,0,sum(Movement.COR_IN_STG)) AS COR_IN_STG
     , IF(sum(Movement.COR_OUT_STG) IS NULL,0,sum(Movement.COR_OUT_STG)) AS COR_OUT_STG
 FROM (
 SELECT opm.OPID AS Order_Placed_ID
     , CASE WHEN oimr.id = ? THEN sum(opm.omQty) * oimr.numVal END AS INTO_STG
     , CASE WHEN oimr.id = ? THEN sum(opm.omQty) * oimr.numVal END AS OUT_OF_STG
     , CASE WHEN oimr.id = ? THEN sum(opm.omQty) * oimr.numVal END AS REJ_BY_STG
     , CASE WHEN oimr.id = ? THEN sum(opm.omQty) * oimr.numVal END AS REJ_BY_NXT_STG
     , CASE WHEN oimr.id = ? THEN sum(opm.omQty) * oimr.numVal END AS COR_IN_STG
     , CASE WHEN oimr.id = ? THEN sum(opm.omQty) * oimr.numVal END AS COR_OUT_STG
FROM order_placed_move opm
INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
GROUP BY  opm.OPID, oimr.ID
     ) Movement INNER JOIN order_placed op ON Movement.Order_Placed_ID = op.ID
                INNER JOIN order_item oi ON op.OIID = oi.ID
                INNER JOIN orders o ON oi.OID = o.ID
                INNER JOIN order_type ot ON oi.OTID = ot.ID
                -- INNER JOIN order_type ot ON o.OTID = ot.ID
                INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
                INNER JOIN prod_ref pr ON oi.PRID = pr.ID
                INNER JOIN order_item_del_date oidd ON oidd.OIID = oi.ID
GROUP BY Movement.Order_Placed_ID
 ) Mvemnt INNER JOIN order_placed op ON Mvemnt.Order_Placed_ID = op.ID
          INNER JOIN order_item oi ON op.OIID = oi.ID
          INNER JOIN orders o ON oi.OID = o.ID
          INNER JOIN company_relationships cr ON cr.CPID = o.CPID
                                             AND cr.Client_ID = o.fCID
                                             AND cr.Assoc_Client_Div_ID = o.fDID
-- sub select for last user that updated database and the date/time of update
          INNER JOIN (SELECT ID
                             , OPID AS OrderPlacedID
                             , OIMRID
                             , UID AS UserToUpdate
                             , inputtime AS LastInputTime
                      FROM (SELECT *
                            FROM order_placed_move opm
                            WHERE OIMRID NOT IN (1,2)
                            ORDER BY OPID, ID DESC
                           ) opm_order
                      GROUP BY OPID
                     ) LastUser ON LastUser.OrderPlacedID = op.ID
WHERE cr.Client_ID = ?
ORDER BY Mvemnt.item_del_date
;";

  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-rfl2</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sssssss", $s1, $s2, $s3, $s4, $s5, $s6, $CID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $sort == 'DESC' ? $sort == 'ASC' : $sort == 'DESC';
    while($row = mysqli_fetch_assoc($result)){
      $req_CID    = $row['req_CID'];
      $OIID    = $row['OIID'];
      $OTID    = $row['OTID'];
      $OIDon   = $row['OIDon'];
      $OIIDon  = $row['OIIDon'];
      $OIIDtir  = $row['OIIDtir'];
      $OIIDti  = $row['OREF'];
      $OIQIDq  = $row['OQTY'];
      $ACID    = $row['ACID'];
      $ACIDn   = $row['ACIDn'];
      $ACIDp   = $row['ACPID'];
      $ACIDpn  = $row['ACIDpn'];
      $OTID    = $row['OTID'];
      $OIDDIDd = $row['OIDDIDd'];

      $QTY_IN_STG = $row['QTY_IN_STG'];
      $OQTY = $row['OQTY'];

      $idate = date('d-M-Y', $OIDDIDd);

      if ($OTID == 1) {
        $c = 'black';
        $bc = 'f7cc6c';
        $SamProd = 'P';
        // $coName = $pACIDn;
      }elseif ($OTID == 2) {
        $c = 'black';
        $bc = 'bbbbff';
        $SamProd = 'P';
        // $coName = $pACIDn;
      }elseif ($OTID == 3) {
        $c = 'white';
        $bc = 'f243e4';
        $SamProd = 'P';
        // $coName = $pACIDn;
      }elseif ($OTID == 4) {
        $c = 'white';
        $bc = '577268';
        $SamProd = 'P';
        // $coName = $ACIDn;
      }elseif ($OTID == 5) {
        $c = 'black';
        $bc = 'f7cc6c';
        $SamProd = 'S';
        // $coName = $ACIDn;
      }elseif ($OTID == 6) {
        $c = 'black';
        $bc = 'bbbbff';
        $SamProd = 'S';
        // $coName = $ACIDn;
      }elseif ($OTID == 7) {
        $c = 'white';
        $bc = 'f243e4';
        $SamProd = 'S';
        // $coName = $ACIDn;
      }elseif ($OTID == 8) {
        $c = 'white';
        $bc = '577268';
        $SamProd = 'S';
        // $coName = $pACIDn;
      } ?>
      <tr>
        <?php
        // Shows you the orders placed within the last 7 days
        // if($od>($td-604740)){
        if ($QTY_IN_STG > 0) {
          ?>
          <td style="width:2%; font-weight: bold;"><?php echo $SamProd ?></td>
          <td style="width:6.5%; padding-right: 0.5%; border-right:thin solid grey;"><a style=" text-decoration:none; text-align:right; " href="<?php echo $urlPage ?>&orb&oi=<?php echo $OIID;?>"><?php echo $OIDon.':'.$OIIDon;?></a></td>

          <?php
          // use the req_CID to set the name of the client/supplier
          if ($ACIDp <> $CID) {$ACID = $ACIDp; $ACIDn = $ACIDpn;}
          ?>

          <!-- <td style="text-align:left; text-indent:4%; border-right:thin solid grey;"><a class="hreflink" href="associates.php?A&dir&ac=<?php echo $ACID;?>"><?php echo $ACIDn?></a></td> -->
          <td style="width:24%; text-align:left; text-indent:4%; border-right:thin solid grey;color: <?php echo $c ?>; background-color: #<?php echo $bc ?>;"><?php echo $ACIDn?></td>
          <td style="width: 34%; text-indent:3%; text-align:left; border-right:thin solid grey;"><?php echo $OIIDtir;?></td>
          <td style="width: 9%; text-align:right; padding-right:1%; border-right:thin solid grey;"><?php echo $QTY_IN_STG;?></td>
          <td style="width: 8%; text-align:right; padding-right:1%; border-right:thin solid grey;"><?php echo $OQTY;?></td>
          <?php
          if($OIDDIDd<$td){;?>
            <td style="width: 14%;text-align:center; background-color:#d8081e;"><?php echo $idate ;?></td>
            <?php
          }elseif($OIDDIDd<($td+604740)){;?>
            <td style="text-align:center; background-color:#f09595;"><?php echo $idate ;?></td>
            <?php
          }elseif($OIDDIDd>($td+121509600)){;?>
            <td style="text-align:center; background-color:#e2e8a8;"><?php echo $idate ;?></td>
            <?php
          }else{;?>
            <td><?php echo $idate ;
          } ?>
          </td>
          <?php
          if ($LU < $td) {
            ?>
            <td style="background-color:RED;"></td>
            <?php
          }else {
            ?>
            <td style="background-color:green;">zzzz</td>
            <?php
          }
        }else {
          // nothing to show as no product in the section
        }
      } ?>
    </tr>
    <?php
  }
    ?>
  </table>

  <div style="position:absolute; bottom:5%; left:10%;">

    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a href='?L&$sp&p=$x'>  $x  </a>";
    }
    ?>
  </div>
