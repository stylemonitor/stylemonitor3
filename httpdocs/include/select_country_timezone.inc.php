<?php
session_start();
include 'dbconnect.inc.php';
echo "<br><b>select_country_timezone.inc.php</b>";

if (isset($_POST['CYID'])) {
  $CYID = $_POST['CYID'];
}

$sql = "SELECT tz.ID  as TZID
          , cy.name as CYIDn
          , tz.TIMEZONE as TZIDn
        FROM country_timezone ct
          , timezones tz
          , country cy
        WHERE ct.CYID = ?
        AND ct.TZID = tz.ID
        AND ct.CYID = cy.ID
        order by tz.timezone
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-facd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CYID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while ($row = mysqli_fetch_assoc($result)){
    $TZID = $row['TZID'];
    $CYIDn = $row['CYIDn'];
    $TZIDn = $row['TZIDn'];
    echo '<option value="'.$TZID.'">'.$TZIDn.'</option>';
  }
}
