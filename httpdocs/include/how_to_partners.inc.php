<?php
include 'dbconnect.inc.php';
// include 'from_CID_get_company_admin.inc.php';
$UID = $_SESSION['UID'];
$CID = $_SESSION['CID'];
$sm = '<b>S</b>tyle<b>M</b>onitor';

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'How to Create a Partnership';
$pddhbc = '#e08df4';
include 'page_description_date_header.inc.php';
include 'from_CID_get_company_admin.inc.php';

if ($UID <> $caUID) {
  ?>
  <div class="" style="top:9%; height:97%; left:0%; width:100%; background-color:#f4f4f4;">
    <br>
    <p>For the magic to happen you need to have Partners.</p>
    <br>
    <p>You need to ask you companys <?php echo $sm ?> Admin user, <?php echo $caUIDf ?>, to request a Partnership with another member.</p>
    <br>
    <p>Give <?php echo $caUIDf ?> the SMIC of the company you wish to become a Partner with.</p>
    <br>
    <p>They will then make the request for you.</p>
    <br>
    <p>When you have some Partners an index tab will appear at the top and you can then place/ receive orders with/from them.</p>
  </div>
  <?php
}else {
?>
<!-- <div class="cpsty" style="top:0%; height:3%; left:0%; width:100%; background-color:#e08df4;">
  <p>How to ... PARTNERS</p>
</div> -->
<div class="" style="position:absolute; top:9%; left:0%; width:100%; font-size: 150%; background-color:#f4f4f4;">
  <br><br>
  <br>
  <p>This is where the magic starts</p>
  <br>
  <p>You'll need to know your future <b>Partners</b> SMIC</p>
  <br>
  <p>Enter this into the space available</p>
  <br>
  <p>Select their relationship to you</p>
  <br>
  <p>Click on the Request Partnership button</p>
  <br>
  <p>Now await their reply</p>
  <br>
  <p>Of course you can always Cancel if you want</p>
  <br>
  <p>and create the partnership later.</p>
  <br>
<div class=""><a style="color:blue; font-weight:bold; text-decoration:none;" href="partners.php?P&pan">Create a Parntnership</a></div>
</div>
<?php
}
