<?php
// echo "include/new.inc.php";
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';

if (isset($_GET['new'])) {
  ?>

  <div style="position:absolute; top:10%; left:20%; width:60%; font-size: 150%; font-weight: bold; text-align:center;">
    Select from the options below what 'new' item you want
  </div>

  <a style="text-decoration:none;" href="styles.php?S&spt&fnpt"><div style="position:absolute; top:30%; height:4.75%; left:10%; width:30%; padding-top:0.25%; border:2px ridge black; border-radius:10px; font-size: 150%; background-color:<?php echo $prtCol ?>;">
    Product Type
  </div></a>

  <a style="text-decoration:none;" href="styles.php?S&snn&fns"><div style="position:absolute; top:30%; height:5%; left:60%; width:30%; padding-top:0.25%; border:2px ridge black; border-radius:10px; font-size: 150%; background-color:<?php echo $styCol ?>;">
    Style
  </div></a>

  <a style="text-decoration:none;" href="styles.php?S&orn&sal=1"><div style="position:absolute; top:40%; height:5%; left:10%; width:30%; padding-top:0.25%; border:2px ridge black; border-radius:10px; font-size: 150%; background-color:<?php echo $salAssCol ?>;">
    Sales Order
  </div></a>

  <a style="text-decoration:none;" href="styles.php?S&orn&pur=1"><div style="position:absolute; top:40%; height:5%; left:60%; width:30%; padding-top:0.25%; border:2px ridge black; border-radius:10px; font-size: 150%; background-color:<?php echo $purAssCol ?>;">
    Purchase Order
  </div></a>

  <a style="text-decoration:none;" href="associates.php?A&acn&adq&c"><div style="position:absolute; top:50%; height:5%; left:10%; width:30%; padding-top:0.25%; border:2px ridge black; border-radius:10px; font-size: 150%; background-color:<?php echo $salAssCol ?>;">
    Associate Client
  </div></a>

  <a style="text-decoration:none;" href="associates.php?A&acn&adq&s"><div style="position:absolute; top:50%; height:5%; left:60%; width:30%; padding-top:0.25%; border:2px ridge black; border-radius:10px; font-size: 150%; background-color:<?php echo $purAssCol ?>;">
    Associate Supplier
  </div></a>

  <a style="text-decoration:none;" href="partners.php?P&pan&c"><div style="position:absolute; top:60%; height:5%; left:10%; width:30%; padding-top:0.25%; border:2px ridge black; border-radius:10px; font-size: 150%; background-color:<?php echo $salPrtCol ?>;">
    Partner Client
  </div></a>

  <a style="text-decoration:none;" href="partners.php?P&pan&s"><div style="position:absolute; top:60%; height:5%; left:60%; width:30%; padding-top:0.25%; border:2px ridge black; border-radius:10px; font-size: 150%; background-color:<?php echo $purPrtCol ?>;">
    Partner Supplier
  </div></a>
<?php
}
