<?php
// add a product type to a company
$sql = "INSERT INTO product_type
          (ACID, UID, name, scode, inputtime)
        VALUES
          (?,?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-insert_prod_type</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssss", $ACID, $UID, $pref, $scode, $td);
  mysqli_stmt_execute($stmt);
}
