<?php
session_start();
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
include 'set_urlPage.inc.php';
// include 'page_selection_complete.inc.php';
// echo "include/report_completed_orders_production.inc.php";

$CID = $_SESSION['CID'];
$td = date('U');

// include 'from_CID_count_section_totals.inc.php';
// include 'from_CID_count_order_overdue.inc.php';
// include 'from_CID_count_order_thisweek.inc.php';
// include 'from_CID_count_order_nextweek.inc.php';
// include 'from_CID_count_order_later.inc.php';
// include 'from_CID_count_order_new.inc.php'; // $cOIDnew
// include 'from_CID_count_orders_in_work.inc.php'; // $cOIDiw
// include 'from_UID_get_last_logout.inc.php'; // $LRIDout

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'Completed Production Orders';
$pddhbc = '#fffddd';
include 'page_description_date_header.inc.php';
include 'SM_colours.inc.php';

// Set the rows per page
$rpp = 10;

// Check the URL to see what page we are on
if (isset($_GET['pa'])) {
  $pa = $_GET['pa'];
}else{
  $pa = 0;
}

// Check which page we are on
if ($pa > 1) {
  $start = ($pa * $rpp) - $rpp;
}else {
  $start = 0;
}

// get a count of the number of orders that are closed
$sql = "SELECT COUNT(Distinct o.ID)
        FROM order_item oi
          , orders o
          , associate_companies ac
          , company c
        WHERE c.ID = 1
        AND (o.tCID = c.ID or o.fCID = c.ID)
        AND oi.OID = o.ID
        AND o.orComp = 1
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-oQ</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
}





// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 1.9 is also 2
$PTV = ($cOIID / $rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV) - 1;
// echo '<br>The number of pages will be : '.$PTV;

// echo '<br>Start record is : '.$start;
?>
<table class="trs" style="position:absolute; top: 9%; width:100%;">
  <tr>
    <th>Company</th>
    <th>Order</th>
    <th>Reference</th>
    <th>Opened</th>
    <th>Closed</th>
  </tr>

  <?php

  // the query for the table
  $sql = "SELECT Distinct o.ID as OID
  		      , ac.ID as ACID
            , ac.name as ACIDn
            , o.orNos as OIDnos
            , o.our_order_ref as OIDref
            , o.orDate as OIDst
            , o.OTID as OIDtype
          FROM order_item oi
            , orders o
            , associate_companies ac
            , company c
          WHERE c.ID = ?
          AND (o.tCID = c.ID or o.fCID = c.ID)
          AND oi.OID = o.ID
          AND o.orComp = 1
  -- LIMIT ?,?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-oQ</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $CID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      $OID = $row['OID'];
      $ACID = $row['ACID'];
      $ACIDn = $row['ACIDn'];
      $OIDnos = $row['OIDnos'];
      $OIDref = $row['OIDref'];
      $OIDst = $row['OIDst'];
      $OIDtype = $row['OIDtype'];

      $OIDst = date('d-M-Y',$OIDst);

  ?>
    <tr>
      <td><?php echo $ACIDn ?></td>
      <td><?php echo $OIDnos ?></td>
      <td><?php echo $OIDref ?></td>
      <td><?php echo $OIDst ?></td>
      <td>to be found</td>
    </tr>
    <?php
    }
  } ?>
</table>

<div style="position:absolute; bottom:5%; right:5%;font-size:200%">
  <?php for ($x = 1 ; $x <= $PTV ; $x++){
    if (isset($_GET['oi'])) {
      ?>
      <a style="color:black; text-decoration:none;" href='<?php echo $urlPage ?>&tab=<?php echo $tab ?>&oi=<?php echo $OIID ?>&pa=<?php echo $x ?>'><?php echo $x ?></a>
      <?php
    }elseif (isset($_GET['OI'])) {
      ?>
      <a style="color:black; text-decoration:none;" href='<?php echo $urlPage ?>&tab=<?php echo $tab ?>&OI=<?php echo $OIID ?>&pa=<?php echo $x ?>'><?php echo $x ?></a>
      <?php
    }
  } ?>
</div>

<?php
