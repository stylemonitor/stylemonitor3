<?php
include 'dbconnect.inc.php';
// echo '<b>File name : </b>include/pwd_reset_data_update_act.inc.php';
// echo '<br>Accessed from : <b>pwd_reset_data.php</b>';
// echo '<b>RESET USER PASSWORD</b>';

//CONNECTION: to the db in the usual manner
include ('dbconnect.inc.php');

$UID = $_POST['UID'];
//CHECK: that the user has come from the email sent and not by typing in the page address
if (!isset($_POST['reset_pwd_submit']) && !isset($_POST['cancel_pwd_submit'])) {
  header("Location: ../index.php");
  exit();
}elseif (isset($_POST['cancel_pwd_submit'])) {
  header("Location: ../company.php?C&usr&u=$UID");
  exit();
}elseif (isset($_POST['reset_pwd_submit'])) {
  // echo '<br>Get the data the user input</b>';
  $pwd = $_POST['pwd'];
  $npwd = $_POST['npwd'];
  $npwdCheck = $_POST['pwdCheck'];

  // echo '<br>Current Password value : <b>'.$pwd.'</b>';
  // echo '<br>Password value : <b>'.$npwd.'</b>';
  // echo '<br>Password check value : <b>'.$npwdCheck.'</b>';

  //CHECK: that none of the data is missing
  if (empty($pwd) || empty($npwd) || empty($npwdCheck)) {
    // echo '<b>You need to set password and confirm it!</b>';
    header("Location: ../company.php?C&rpw&pwe=4");
    exit();
  }else {
    // CHECK current password entered matches the file password
    // Get the current hased password
    // echo "<br>ALL elements have been set";

    // Start transaction
    // mysqli_begin_transaction($mysqli);
    // try {
      // to

      $sql = "SELECT pwd as cPWD
              FROM users
              WHERE ID = ?
              ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo "FAIL-fpra";
      } else {
        mysqli_stmt_bind_param($stmt, "s", $UID);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        $row = mysqli_fetch_assoc($result);
      }
      $cPWD = $row['cPWD'];

      // echo "<br>Current hashed password :<b> $cPWD</b>";

      $pwdCheck = password_verify($pwd, $cPWD);
      if ($pwdCheck == false) {
        // echo "<br>The Current password DOES NOT match the file password";
        // // HASH the password before sending it to the db
        // $pwd =password_hash($pwd, PASSWORD_DEFAULT);
        // //cleaned up the password so no nasties can get through
        // $pwd = mysqli_real_escape_string($con, $pwd);
        header("Location: ../company.php?C&rpw&pwe=1");
        exit();
      }elseif ($pwdCheck == true) {
        // echo "<br>The Current password MATCHES the file password";

        // HASH the password before sending it to the db
        $hnpwd =password_hash($npwd, PASSWORD_DEFAULT);
        //cleaned up the password so no nasties can get through
        $npwd = mysqli_real_escape_string($con, $npwd);
        // echo "<br>New password can be set";

        if(strlen($npwd)<6){
          // echo '<br>Password needs to be longer than 6 characters';
          header("Location: ../company.php?C&rpw&pwe=2");
          exit();
        }else{
          // echo "<br>Password is 6 or more characters long";
          if($npwd <> $npwdCheck) {
            // echo '<br>Password and check password : DO NOT MATCH</b>';
            header("Location: ../company.php?C&rpw&pwe=3");
            exit();
          }else {
            // echo '<br>Password and check password : Matching</b>';
            //SET: the current date/time to check that the request is not 'TIMED OUT'
            $td = date("U");

            $sql = "UPDATE users
            SET pwd = ?
            WHERE id = ?
            ;";
            $stmt = mysqli_stmt_init($con);
            if (!mysqli_stmt_prepare($stmt, $sql)) {
              echo "FAIL-fpra2";
            } else {
              mysqli_stmt_bind_param($stmt, "ss", $hnpwd, $UID);
              mysqli_stmt_execute($stmt);
              $result = mysqli_stmt_get_result($stmt);
            }
          }
        }
      }
    // } catch (mysqli_sql_exception $exception) {
    //   mysqli_rollback($mysqli);
    //
    //   throw $exception;
    // }

    // echo "<br><b>PASSWORD UPDATED</b>";
    header("Location: ../company.php?C&usr&u=$UID");
    exit();
  }
}
