<?php
include 'dbconnect.inc.php';
$CID = $_SESSION['CID'];
// echo "<b>include/from_CID_count_company_partners_pending.inc.php</b>";
// See how many partners you have had accepting
$sql = "SELECT COUNT(ID) as cCPID
        FROM company_partnerships
        WHERE ((acc_CID = ?) || (req_CID = ?))
        AND insCID NOT IN (?)
			  AND active = 0
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccacp2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cpCPIDp = $row['cCPID'];
}
