<?php
include 'dbconnect.inc.php';
// echo "<br><b>form_check_UID_login_count.inc.php</b>";
// echo "<br>fUID = $fUID";
$td = date('U');

// check if they are already logged in and get that ID
$sql = "SELECT COUNT(lr.ID) as cLRID
, lr.ID as LRID
        FROM login_registry lr
          , users u
        WHERE lr.logouttime = 0
        AND u.uid = ? OR u.email = ?
        AND lr.UID = u.ID;
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fculc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $fUID, $fUID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cLRID = $row['cLRID'];
  $LRID = $row['LRID'];
  echo "<br>cLRID = $cLRID :: LRID == $LRID";
}

if ($cLRID == 1) {
  // need to cancel this registration and set up the new one
  // echo "<br>Login count is 1";
  $sql = "UPDATE login_registry
          SET logouttime = ?
          WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-fculc2</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $td, $LRID);
    mysqli_stmt_execute($stmt);
  }

  include 'check_email_valid.inc.php';

}else {
  // move on with the registration as cLRID == 0
  // echo "<br>Login count is 0";
  include 'check_email_valid.inc.php';
}
 ?>
