<?php
// echo "form_edit_item_change_type.inc.php";

if (isset($_GET['cht'])) {
  $samProd = $_GET['cht'];
}

if (isset($_GET['OI'])) {
  $OIID = $_GET['OI'];
}

include 'from_OIID_get_order_details.inc.php';

if ($samProd == 1) {
  $type = 'Sample to Production';
}elseif ($samProd == 2) {
  $type = 'Production to Sample';
}

?>
<div class="overlay" style="z-index:2;"></div>

<div style="position:absolute; top:35%; height:25%; left:30%; width:40%; background-color: pink; font-size: 120%; font-weight: bold; border: 2px solid grey; border-radius: 5px; z-index:3;">
  <b>Do you really want to change this item type</b>
  <br>from <?php echo "$type" ?>
</div>

<form action="include/form_edit_item_change_type_act.inc.php" method="post">
  <input type="hidden" name="OID" value="<?php echo $OID ?>">
  <input type="hidden" name="OIID" value="<?php echo $OIID ?>">
  <input type="hidden" name="samProd" value="<?php echo $samProd ?>">
  <textarea autofocus required style="position:absolute; top:45%; left: 31%; width:38%; z-index:3;" name="canReas" value="" placeholder="Please say why the item type is being changed"></textarea>

  <button class="entbtn" type="submit" style="position:absolute; top:53%; left:35%; width:15%; background-color:<?php echo $fsvCol ?>; z-index:3;" name="YesBtn">Confirm Change</button>
  <button formnovalidate class="entbtn" type="submit" style="position:absolute; top:53%; left:55%; width:10%; background-color:<?php echo $fcnCol ?>; z-index:3;" name="NoBtn">Cancel</button>
</form>
