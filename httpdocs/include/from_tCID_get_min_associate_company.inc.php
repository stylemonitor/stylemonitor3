<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_tCID_get_min_associate_company.inc.php</b>";
$CID = $_SESSION['CID'];

$sql = "SELECT MIN(ac.ID) as mACID
          , ac.name as mACIDn
        FROM associate_companies ac
          , company c
        WHERE c.ID = ?
        AND ac.CID = c.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $tCID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $mtACID = $row['mACID'];
  $mtACIDn = $row['mACIDn'];
}
