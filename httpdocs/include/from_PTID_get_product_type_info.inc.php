<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_PTID_get_product_type_info.inc.php</b>";
// pt.DIDchanged

$sql = "SELECT pt.ID as PTID
          , pt.name as PTIDn
        FROM  product_type pt
        WHERE pt.ID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fpgpti</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $PTID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $PTID = $row['PTID'];
  $PTIDn = $row['PTIDn'];
}
