<?php
// echo "<b>include/from_CID_count_collection.inc.php</b>";
$CID = $_SESSION['CID'];

$sql = "SELECT COUNT(c.ID) as cCID
        FROM collection c
        WHERE c.ACID = ?
        AND c.name NOT IN ('Select Collection')
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cCLID = $row['cCID'];
}
