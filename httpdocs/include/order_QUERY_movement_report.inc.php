<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/order_QUERY.inc.php";

$CID = $_SESSION['CID'];

$sdate = date('d-M-Y', $sd);
$edate = date('d-M-Y', $ed);
$tdate = date('d-M-Y', $td);
// echo "<br><br><br><br><br><br><br><br><br><br><br>SHOW RESULTS BETWEEN";
// echo "<br>$sdate and $edate";
// echo "<br>Today = $tdate";

$sql = "SELECT DISTINCT
-- order_report_20201216.sql
     o.fCID AS cliCID
     , c.name AS cliCIDn
     , o.fACID AS cli_ACID
     , ac.name AS cli_ACIDn
     , o.fDID AS cli_DID
     , d.name AS cli_DIDn
     , o.tCID AS supCID
     , cS.name AS supCIDn
     , o.tACID AS sup_ACID
     , acS.name AS sup_ACIDn
     , o.tDID AS sup_DID
     , d.name AS sup_DIDn
     , Mvemnt.ORDER_ID AS OID
     , Mvemnt.O_ITEM_ID AS OIID
     , LPAD(o.orNos,6,0) AS ordNos
     , LPAD(Mvemnt.ord_item_nos,3,0) AS ordItemNos
     , o.OTID AS OTID
     , Mvemnt.samProd AS samProd
     , o.our_order_ref AS ourRef
     , odd.del_Date AS ODDIDdd
     , oidd.item_del_date AS item_due_date
     , o.CPID AS CPID
     , pr.ID AS PRID
     , pr.prod_ref AS PRIDs
     , Mvemnt.ordQty AS OIQIDq
-- CALCULATE INDIVIDUAL GARMENT MOVEMENT VALUES FOR EACH SECTION
     , SUM(Mvemnt.ORDER_PLACEMENT - Mvemnt.INTO_STG1 + Mvemnt.ORDER_PLACE_INCR - Mvemnt.ORDER_PLACE_DECR + Mvemnt.REJ_BY_STG1 + Mvemnt.COR_IN_STG1 - Mvemnt.COR_OUT_STG1) AS Awaiting
     , SUM(Mvemnt.INTO_STG1 - Mvemnt.INTO_STG2 - Mvemnt.REJ_BY_STG1 + Mvemnt.REJ_BY_STG2 - Mvemnt.COR_IN_STG1 + Mvemnt.COR_IN_STG2 + Mvemnt.COR_OUT_STG1 - Mvemnt.COR_OUT_STG2) AS Stage1
     , SUM(Mvemnt.INTO_STG2 - Mvemnt.INTO_STG3 - Mvemnt.REJ_BY_STG2 + Mvemnt.REJ_BY_STG3 - Mvemnt.COR_IN_STG2 + Mvemnt.COR_IN_STG3 + Mvemnt.COR_OUT_STG2 - Mvemnt.COR_OUT_STG3) AS Stage2
     , SUM(Mvemnt.INTO_STG3 - Mvemnt.INTO_STG4 - Mvemnt.REJ_BY_STG3 + Mvemnt.REJ_BY_STG4 - Mvemnt.COR_IN_STG3 + Mvemnt.COR_IN_STG4 + Mvemnt.COR_OUT_STG3 - Mvemnt.COR_OUT_STG4) AS Stage3
     , SUM(Mvemnt.INTO_STG4 - Mvemnt.INTO_STG5 - Mvemnt.REJ_BY_STG4 + Mvemnt.REJ_BY_STG5 - Mvemnt.COR_IN_STG4 + Mvemnt.COR_IN_STG5 + Mvemnt.COR_OUT_STG4 - Mvemnt.COR_OUT_STG5) AS Stage4
     , SUM(Mvemnt.INTO_STG5 - Mvemnt.INTO_WHOUSE - Mvemnt.REJ_BY_STG5 + Mvemnt.REJ_BY_WHOUSE - Mvemnt.COR_IN_STG5 + Mvemnt.COR_IN_WHOUSE + Mvemnt.COR_OUT_STG5 - Mvemnt.COR_OUT_WHOUSE) AS Stage5
     , SUM(Mvemnt.INTO_WHOUSE - Mvemnt.REJ_BY_WHOUSE - Mvemnt.TO_CLIENT + Mvemnt.FROM_CLIENT - Mvemnt.RETURNED_TO_SUPPLIER + Mvemnt.RECEIVED_FROM_SUPPLIER - Mvemnt.COR_IN_WHOUSE
           + Mvemnt.COR_OUT_WHOUSE + Mvemnt.CORR_OUT_CLIENT - Mvemnt.CORR_IN_CLIENT + Mvemnt.CORR_OUT_SUPPLIER - Mvemnt.CORR_IN_SUPPLIER) AS Warehouse
     , SUM(Mvemnt.TO_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT + Mvemnt.CORR_IN_CLIENT) AS CLIENT
     , SUM(Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.RECEIVED_FROM_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER) AS SUPPLIER
     , SUM(Mvemnt.TO_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT + Mvemnt.CORR_IN_CLIENT + Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.RECEIVED_FROM_SUPPLIER
           - Mvemnt.CORR_OUT_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER) AS SENT
     , Mvemnt.UID AS UID
     , Mvemnt.inputtime AS inputtime
FROM (
    SELECT DISTINCT oimr.ID AS OIMR_ID
         , o.ID AS ORDER_ID
         , oi.ID AS O_ITEM_ID
         , oi.PRID AS PRID
         , oi.ord_item_nos AS ord_item_nos
         , oi.samProd AS samProd
         , oiq.order_qty AS ordQty
         , last_update.UID
         , last_update.inputtime
    -- oimr1 not required at present - oiq.order_qty is used instead
         , IF((CASE WHEN oimr.ID = 2 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 2 THEN SUM(opm.omQty) END)) AS  ORDER_PLACEMENT
         , IF((CASE WHEN oimr.ID = 3 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 3 THEN SUM(opm.omQty) END)) AS INTO_STG1
         , IF((CASE WHEN oimr.ID = 4 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 4 THEN SUM(opm.omQty) END)) AS INTO_STG2
         , IF((CASE WHEN oimr.ID = 5 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 5 THEN SUM(opm.omQty) END)) AS INTO_STG3
         , IF((CASE WHEN oimr.ID = 6 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 6 THEN SUM(opm.omQty) END)) AS INTO_STG4
         , IF((CASE WHEN oimr.ID = 7 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 7 THEN SUM(opm.omQty) END)) AS INTO_STG5
         , IF((CASE WHEN oimr.ID = 8 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 8 THEN SUM(opm.omQty) END)) AS INTO_WHOUSE
    -- oimr9 to oimr10 not required at present
         , IF((CASE WHEN oimr.ID = 11 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 11 THEN SUM(opm.omQty) END)) AS ORDER_PLACE_INCR
         , IF((CASE WHEN oimr.ID = 12 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 12 THEN SUM(opm.omQty) END)) AS ORDER_PLACE_DECR
         , IF((CASE WHEN oimr.ID = 13 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 13 THEN SUM(opm.omQty) END)) AS REJ_BY_STG1
         , IF((CASE WHEN oimr.ID = 14 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 14 THEN SUM(opm.omQty) END)) AS REJ_BY_STG2
         , IF((CASE WHEN oimr.ID = 15 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 15 THEN SUM(opm.omQty) END)) AS REJ_BY_STG3
         , IF((CASE WHEN oimr.ID = 16 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 16 THEN SUM(opm.omQty) END)) AS REJ_BY_STG4
         , IF((CASE WHEN oimr.ID = 17 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 17 THEN SUM(opm.omQty) END)) AS REJ_BY_STG5
         , IF((CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END)) AS REJ_BY_WHOUSE
         , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
         , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
         , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
         , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
         , IF((CASE WHEN oimr.ID = 23 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 23 THEN SUM(opm.omQty) END)) AS COR_IN_STG1
         , IF((CASE WHEN oimr.ID = 24 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 24 THEN SUM(opm.omQty) END)) AS COR_IN_STG2
         , IF((CASE WHEN oimr.ID = 25 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 25 THEN SUM(opm.omQty) END)) AS COR_IN_STG3
         , IF((CASE WHEN oimr.ID = 26 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 26 THEN SUM(opm.omQty) END)) AS COR_IN_STG4
         , IF((CASE WHEN oimr.ID = 27 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 27 THEN SUM(opm.omQty) END)) AS COR_IN_STG5
         , IF((CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END)) AS COR_IN_WHOUSE
         , IF((CASE WHEN oimr.ID = 29 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 29 THEN SUM(opm.omQty) END)) AS COR_OUT_STG1
         , IF((CASE WHEN oimr.ID = 30 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 30 THEN SUM(opm.omQty) END)) AS COR_OUT_STG2
         , IF((CASE WHEN oimr.ID = 31 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 31 THEN SUM(opm.omQty) END)) AS COR_OUT_STG3
         , IF((CASE WHEN oimr.ID = 32 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 32 THEN SUM(opm.omQty) END)) AS COR_OUT_STG4
         , IF((CASE WHEN oimr.ID = 33 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 33 THEN SUM(opm.omQty) END)) AS COR_OUT_STG5
         , IF((CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END)) AS COR_OUT_WHOUSE
         , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
         , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
         , IF((CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END)) AS CORR_OUT_SUPPLIER
         , IF((CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END)) AS CORR_IN_SUPPLIER
    -- oimr39 to oimr40 not required at present
    FROM order_placed_move opm
       INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
       INNER JOIN order_placed op ON opm.OPID = op.ID
       INNER JOIN order_item oi ON op.OIID = oi.ID
       INNER JOIN orders o ON oi.OID = o.ID
       INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
       -- 'Last user update' sub-select
       INNER JOIN (SELECT opm_order.OPID AS OPID
                       , opm_order.UID AS UID
                       , opm_order.inputtime AS inputtime
                   FROM (SELECT opm.OPID
                         , opm.UID
                         , opm.inputtime
                       FROM order_placed_move opm
                         INNER JOIN order_placed op ON op.ID = opm.OPID
                         INNER JOIN order_item oi ON oi.ID = op.OIID
                         INNER JOIN orders o ON oi.OID = o.ID
                       ORDER BY opm.OPID, opm.ID DESC
                       ) opm_order
                   GROUP BY opm_order.OPID
                  ) last_update ON op.ID = last_update.OPID
       -- End of 'Last user update' sub-select
    WHERE oi.itComp = ?
    GROUP BY oi.ID, oimr.ID
   ) Mvemnt -- INNER JOIN order_item oi ON Mvemnt.O_ITEM_ID = oi.ID
        INNER JOIN orders o ON Mvemnt.ORDER_ID = o.ID
        INNER JOIN order_item_del_date oidd ON Mvemnt.O_ITEM_ID = oidd.OIID
        INNER JOIN prod_ref pr ON Mvemnt.PRID = pr.ID
        INNER JOIN orders_due_dates odd ON odd.OID = o.ID
        INNER JOIN company c ON o.fCID = c.ID
        INNER JOIN company cS ON o.tCID = cS.ID
        INNER JOIN associate_companies ac ON o.fACID = ac.ID
        INNER JOIN associate_companies acS ON o.fACID = acS.ID
        INNER JOIN division d ON o.fDID = d.ID
        INNER JOIN division dS ON o.fDID = dS.ID
   WHERE (o.fCID = ? OR o.tCID = ?)
-- NEXT LINE COMMENTED OUT - it is to be used in PHP as a variable
    -- AND oidd.item_del_date BETWEEN ? AND ?
/*
 ================================================================================================================================
 NEXT THREE LINES COMMENTED OUT:
       For the standard query use the WHERE clause above and ignore both AND statements
       For Client use o.fCID, o.fACID or o.fDID in the second AND statement
       For Orders use Mvemnt.ORDER_ID in the first AND statement and set the WHERE clause above to that required
       For Style use pr.ID in the third AND statement and set the WHERE clause above to that required
 ================================================================================================================================
*/
--  AND Mvemnt.ORDER_ID = 2
--  AND o.fACID = 2
--  AND pr.ID = 1
GROUP BY Mvemnt.O_ITEM_ID
/*
 ====================================================================================================================
 NEXT FOUR LINES COMMENTED OUT:
       For the standard query use the first ORDER BY statement
       For Client use second ORDER BY statement
       For Orders use third ORDER BY statement
       For Style use fourth ORDER BY statement
 ====================================================================================================================
*/
-- ORDER BY from_unixtime(item_due_date), ordNos, ordItemNos, OIQIDq ASC
-- ORDER BY ordNos, ordItemNos, PRID, cli_DID ASC
-- ORDER BY cli_DID, ordNos, ordItemNos, PRID ASC
-- ORDER BY PRID, cli_DID, ordNos, ordItemNos ASC
-- LIMIT ?
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-oQmr</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssssss",$OC, $CID, $CID, $sd, $ed, $r);
  // mysqli_stmt_bind_param($stmt, "ssssssss",$OC , $sd, $ed, $CID, $CID, $iniSelect, $CID, $r);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $cliCID = $row['cliCID'];
    $cliCIDn = $row['cliCIDn'];
    $cli_ACID = $row['cli_ACID'];
  $cli_ACIDn = $row['cli_ACIDn'];
    $supCID = $row['supCID'];
    $supCIDn = $row['supCIDn'];
    $sup_ACID = $row['sup_ACID'];
  $sup_ACIDn = $row['sup_ACIDn'];
    $TUID = $row['TUID'];
    $Ttd = $row['Ttd'];
    $OID = $row['OID'];
  $OIID = $row['OIID'];
  $OIDnos = $row['ordNos'];
  $OIIDnos = $row['ordItemNos'];
  $OTID = $row['OTID'];
  $samProd = $row['samProd'];
  $ourRef = $row['ourRef'];
    $ODDIDdd = $row['ODDIDdd'];
  $OIDDIDd = $row['item_due_date'];
    $CPID = $row['CPID'];
  $PRID = $row['PRID'];
  $PRIDs = $row['PRIDs'];
  $OIQIDq = $row['OIQIDq'];
  $Awaiting = $row['Awaiting'];
  $Stage1 = $row['Stage1'];
  $Stage2 = $row['Stage2'];
  $Stage3 = $row['Stage3'];
  $Stage4 = $row['Stage4'];
  $Stage5 = $row['Stage5'];
  $Warehouse = $row['Warehouse'];
    $CLIENT = $row['CLIENT'];
    $SUPPLIER = $row['SUPPLIER'];

// Taken OUT as DAL sorted in his sql
// LEFT IN UNTIL DAL UPDATES THE SQL
    if ($cliCID <> $CID) {
      $cli_ACIDn = $cli_ACIDn;
      $OTID = $OTID - 1;
    }else {
      $cli_ACIDn = $sup_ACIDn;
      $OTID = $OTID;
    }

    $idate = date('d-M-Y', $OIDDIDd);
    ?>
    <tr>
      <?php

      if ($samProd == 0) {
        $spc ='#76f8bc';
        $spbc = '#76f8bc';
      }else {
        $spc = '#fa7b7b';
        $spbc = '#fa7b7b';
      }
      // Shows you the orders placed within the last 7 days
      // if($od>($td-604740)){
      if ($OTID == 1) {
        $c = 'black';
        $bc = 'f7cc6c';
      }elseif ($OTID == 2) {
        $c = 'black';
        $bc = 'bbbbff';
      }elseif ($OTID == 3) {
        $c = 'white';
        $bc = 'f243e4';
      }elseif ($OTID == 4) {
        $c = 'white';
        $bc = '577268';
      }
      // elseif ($OTID == 5) {
      //   $c = 'black';
      //   $bc = 'f7cc6c';
      //   $SamProd = '#fa7b7b';
      //   $SC = 'S';
      // }elseif ($OTID == 6) {
      //   $c = 'black';
      //   $bc = 'bbbbff';
      //   $SamProd = '#fa7b7b';
      //   $SC = 'S';
      // }elseif ($OTID == 7) {
      //   $c = 'white';
      //   $bc = 'f243e4';
      //   $SamProd = '#fa7b7b';
      //   $SC = 'S';
      // }elseif ($OTID == 8) {
      //   $c = 'white';
      //   $bc = '577268';
      //   $SamProd = '#fa7b7b';
      //   $SC = 'S';
      // }
      if (strlen($ourRef)>30) {
        $ourRef = substr($ourRef,0,30);
        ?>
        <td style="text-align:left; text-indent:1%; border-left: thin solid grey; border-right:thin solid grey; color: <?php echo $c ?>;background-color: #<?php echo $bc ?>;"><?php echo $ourRef ?> ...</td>
        <?php
      }else {?>
        <td style="text-align:left; text-indent:1%; border-left: thin solid grey; border-right:thin solid grey; color: <?php echo $c ?>;background-color: #<?php echo $bc ?>;"><?php echo $ourRef ?></td>
        <?php
      } ?>
      <td style="text-align:left; text-indent:3%; border-right:thin solid grey;"><a style="text-decoration:none;" href="styles.php?S&sio&s=<?php echo $PRID ?>"> <?php echo $PRIDs;?></a></td>
      <td style="text-align:right; border-right:thin solid grey; padding-right:0.5%;"><?php echo $OIQIDq;?></td>
      <?php
      // AWAITING
      if ($Awaiting == 0) {
        ?>
        <td style="text-align: right; padding-right:0.5%; color:#83ebab; background-color:#83ebab;"><?php echo $Awaiting ?></td>
        <?php
      }elseif ($Awaiting < 0) {
        ?>
        <td style="text-align: right; padding-right:0.5%; border-right:thin solid grey; background-color:#fc4141;"><?php echo $Awaiting ?></td>
        <?php
      }else {
        ?>
        <td style="text-align: right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $Awaiting ?></td>
        <?php
      }
      // STAGE1
      if (($Awaiting == 0) && ($Stage1 == 0 )) {
        ?>
        <td style="text-align: right; padding-right:0.5%; color:#83ebab; background-color:#83ebab;"><?php echo $Stage1 ?></td>
        <?php
      }elseif ($Stage1 < 0) {
        ?>
        <td style="text-align: right; padding-right:0.5%; border-right:thin solid grey; background-color:#fc4141;"><?php echo $Awaiting ?></td>
        <?php
      }else {
        if ($Stage1 == 0) {
          ?>
          <td ></td>
          <?php
        }else {
          ?>
          <td style="text-align: right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $Stage1 ?></td>
          <?php
        }
      }
      // STAGE 2
      if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 )) {
        ?>
        <td style="text-align: right; padding-right:0.5%; color:#83ebab; background-color:#83ebab;"><?php echo $Stage2 ?></td>
        <?php
      }elseif ($Stage2 < 0) {
        ?>
        <td style="text-align: right; padding-right:0.5%; border-right:thin solid grey; background-color:#fc4141;"><?php echo $Stage2 ?></td>
        <?php
      }else {
        if ($Stage2 == 0) {
          ?>
          <td ></td>
          <?php
        }else {
          ?>
          <td style="text-align: right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $Stage2 ?></td>
          <?php
        }
      }
      // STAGE 3
      if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 )) {
        ?>
        <td style="text-align: right; padding-right:0.5%; color:#83ebab; background-color:#83ebab;"><?php echo $Stage3 ?></td>
        <?php
      }elseif ($Stage3 < 0) {
        ?>
        <td style="text-align: right; padding-right:0.5%; border-right:thin solid grey; background-color:#fc4141;"><?php echo $Stage3 ?></td>
        <?php
      }else {
        if ($Stage3 == 0) {
          ?>
          <td ></td>
          <?php
        }else {
          ?>
          <td style="text-align: right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $Stage3 ?></td>
          <?php
        }
      }
      // STAGE4
      if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 ) && ($Stage4 == 0 )) {
        ?>
        <td style="text-align: right; padding-right:0.5%; color:#83ebab; background-color:#83ebab;"><?php echo $Stage4 ?></td>
        <?php
      }elseif ($Stage4 < 0) {
        ?>
        <td style="text-align: right; padding-right:0.5%; border-right:thin solid grey; background-color:#fc4141;"><?php echo $Stage4 ?></td>
        <?php
      }else {
        if ($Stage4 == 0) {
          ?>
          <td ></td>
          <?php
        }else {
          ?>
          <td style="text-align: right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $Stage4 ?></td>
          <?php
        }
      }
      // STAGE5
      if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 ) && ($Stage4 == 0 ) && ($Stage5 == 0 )) {
        ?>
        <td style="text-align: right; padding-right:0.5%; color:#83ebab; background-color:#83ebab;"><?php echo $Stage5 ?></td>
        <?php
      }elseif ($Stage5 < 0) {
        ?>
        <td style="text-align: right; padding-right:0.5%; border-right:thin solid grey; background-color:#fc4141;"><?php echo $Stage5 ?></td>
        <?php
      }else {
        if ($Stage5 == 0) {
          ?>
          <td ></td>
          <?php
        }else {
          ?>
          <td style="text-align: right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $Stage5 ?></td>
          <?php
        }
      }
      // WAREHOUSE
      if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 ) && ($Stage4 == 0 ) && ($Stage5 == 0 ) && ($Warehouse == 0 )) {
        ?>
        <td style="text-align: right; padding-right:0.5%; color:#83ebab; background-color:#83ebab;"><?php echo $Warehouse ?></td>
        <?php
      }elseif ($Warehouse < 0) {
        ?>
        <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%; background-color:#fc4141;"><?php echo $Warehouse ?></td>
        <?php
      }else {
        if ($Warehouse == 0) {
          ?>
          <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"></td>
          <?php
        }else {
          ?>
          <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"><?php echo $Warehouse ?></td>
          <?php
        }
      }
      // Despatched
      ?>
      <td><?php echo $CLIENT ?></td>
      <?php
      if($OIDDIDd<$td){;?>
        <td style="text-align:right; padding-right:1%; background-color:#fc4141;"><?php echo $idate ;?></td>
        <?php
      }elseif($OIDDIDd<($td+604740)){;?>
        <td style="text-align:right; padding-right:1%; background-color:#f09595;"><?php echo $idate ;?></td>
        <?php
      }elseif($OIDDIDd>($td+121509600)){;?>
        <td style="text-align:right; padding-right:1%; background-color:#e2e8a8;"><?php echo $idate ;?></td>
        <?php
      }else{;?>
        <td style="text-align:right; padding-right:1%;"><?php echo $idate ;?></td>
      <?php
    }?>
    </tr>
    <?php
    if ($LRIDlast < $td) {
      ?>
      <td style="background-color:RED;"></td>
      <?php
    }else {
      ?>
      <td style="background-color:green;"></td>
      <?php
    }
  }
  } ?>
  </table>

  <?php
  if (isset($_GET['sum'])) {
    ?>
    <div style="position:absolute; bottom:2%; left:0%; width:100%; z-index:1;">
      The 'top' 5 are listed above - to see all items in a given time frame click on the tab at the top OR the name of the section
    </div>
    <?php
  }
  ?>
