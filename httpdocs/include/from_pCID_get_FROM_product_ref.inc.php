<?php
include 'dbconnect.inc.php';
// echo '<b>include/from_pCID_get_FROM_product_ref.inc.php</b>';
$CID = $_SESSION['CID'];

$sql = "SELECT pr.ID as pID
          , pr.prod_ref as pref
          , pt.name as ptn
        FROM company c
          , associate_companies ac
          , division d
          , prod_ref pr
          , product_type pt
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND pr.DID = d.ID
        AND pr.PTID = pt.ID
        ORDER BY 2;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br><b>FAIL-fpgfpr</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $fCID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $fPRID  = $row['pID'];
  $fPRIDn = $row['pref'];
  $fPTIDn = $row['ptn'];
}
?>
