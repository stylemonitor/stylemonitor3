<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_ORDER_STATUS_due_date.inc.php";

$CID = $_SESSION['CID'];

include 'set_urlPage.inc.php';
include 'from_UID_get_last_logout.inc.php'; // $LRIDout
include 'ot_sp_selection_movement.inc.php';

$td = date('U');

if (isset($_GET['home'])) {
  $hbc = '#b7d8ff';
  // $LtabTitle = 'ALL Orders Status';
  $pddhbc = '#b7d8ff';
  $pddh = 'SUMMARY ALL ORDER';
  $OC = 0;
  $dr = 'oidd.item_del_date';
  $sd = $td - 31536000;
  $ed = $td + 31536000;
}

include 'page_description_date_header.inc.php';
include 'page_selection_header.inc.php';
include 'order_QUERY_count.inc.php';

$rpp = 25;

// Check the URL to see what page we are on
if (isset($_GET['pa'])) {
  $pa = $_GET['pa'];
}else{
  $pa = 0;
}

// Set the start point for each page
if ($pa > 1) {
  $start = ($pa * $rpp) - $rpp;
}else {
  $start = 0;
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
// $cTOTAL is the count of the total items able to be viewed - cannot be changed
$PTV = ($cTOTAL / $rpp)-1;

// The number of pages to be viewed
// $PTV = ceil($PTV);

$iniSelect = TOTALS.cliCID;

$edate = date('d-M-Y', $td);

// echo "<br>Company ID : $CID";
// echo "<br>Order status : $OC";
// echo "<br>Today : $td";
// echo "<br>Start day : $sd";
// echo "<br>End day : $ed";
// echo "<br>Count_orders - due date source : $dr";

?>

<table class="trs" style="position:absolute; top:15%;">
  <tr>
    <?php
    include 'Table_headings_order_QUERY.inc.php';
    ?>
  </tr>
  <?php
  include 'order_QUERY_ALL.inc.php';
  ?>

<!-- TO BE ADDED to the bottom of each report and adjusted accordingly -->
<div style="position:absolute; top:85%; right:5%; font-size:150%;">
  <?php
  for ($x = 1 ; $x <= $PTV ; $x++){
    echo "<a style='color:blue; text-decoration:none;' href='$urlPage&tab=$tab&pa=$x'>  $x  </a>";
  }
  ?>
</div>
