<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/select_timeZones.inc.php";

$sql = "SELECT ID
          , TIMEZONE
          , UTC_Offset
        FROM timezones
;";
$res = mysqli_query($con, $sql);
while($row = mysqli_fetch_array($res)){
  $TID = $row['ID'];
  $TIDn = $row['TIMEZONE'];
  $TIDv = $row['UTC_Offset'];

  echo '<option value="'.$TID.'">'.$TIDn.' ('. $TIDv.')'.'</option>';
};
