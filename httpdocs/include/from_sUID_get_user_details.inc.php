<?php
include 'dbconnect.inc.php';
// echo "include/from_sUID_get_user_details.inc.php";

$sql = "SELECT u.firstname as sUIDf
          , u.surname as sUIDs
          , u.userInitial as sUIDi
          , u.UID as sUIDu
          , u.email as sUIDe
          , u.itemPref as itemPref
          , u.clientPref as clientPref
          , u.salesPref as salesPref
          , u.active as sUIDa
          , d.ID as sDID
          , d.name as sDIDn
          , cdu.position as sUIDp
        FROM users u
          , company_division_user cdu
          , division d
        WHERE u.ID = ?
        AND cdu.UID = u.ID
        AND cdu.DID = d.ID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fsgud1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $sUID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $sUIDf  = $row['sUIDf'];
  $sUIDs  = $row['sUIDs'];
  $sUIDi  = $row['sUIDi'];
  $sUIDu  = $row['sUIDu'];
  $sUIDe  = $row['sUIDe'];
  $itemPref  = $row['itemPref'];
  $clientPref  = $row['clientPref'];
  $salesPref  = $row['salesPref'];
  $sUIDa  = $row['sUIDa'];
  $sDID  = $row['sDID'];
  $sDIDn  = $row['sDIDn'];
  $sUIDp  = $row['sUIDp'];
}
