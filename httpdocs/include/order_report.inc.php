<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/order_report.inc.php";

include 'set_urlPage.inc.php';

$CID = $_SESSION['CID'];
$td = date('U');

include 'from_CID_count_order_overdue.inc.php';
include 'from_CID_count_order_thisweek.inc.php';
include 'from_CID_count_order_nextweek.inc.php';
include 'from_CID_count_order_later.inc.php';
include 'from_CID_count_order_new.inc.php'; // $cOIDnew
include 'from_CID_count_orders_in_work.inc.php'; // $cOIDiw
include 'from_UID_get_last_logout.inc.php'; // $LRIDout

// Check the URL to see what page we are on
if (isset($_GET['p'])) { $p = $_GET['p']; }else{ $p = 0; }
if (isset($_GET['aoo'])) {$r=30;
}else { $r = 10;
}

// Set the rows per page
// Check which page we are on
if ($p > 1) { $start = ($p * $r) - $r; }else { $start = 0; }

// sets the order type
// all open orders overdue
// if ($cOIDod == 0) {
// }
if (isset($_GET['aod'])) {
  $sp = 'aod';
  $OC = 0;
  $sd = $td - 31536000;
  $ed = $td;
  $dr = 'oidd.item_del_date';
  $cp = "orders that are OVERDUE";
  $hc = 'black';
  $hbc = '#f85b6b';
}

// all open orders placed within the last 7 days
// if ($cOIDnew <> 0) {
// }
if (isset($_GET['aoe'])) {
  $sp = 'aoe';
  $OC = 0;
  $sd = $td - 604800;
  $ed = $td + 86400;
  // $dr = 'oidd.item_del_date';
  $dr = 'ordate';
  $cp = "new orders that have been placed in the last week";
  $hc = 'white';
  $hbc = '#693663';
}

// orders due later than 2 weeks
// if ($cOIDla <> 0) {
// }
if (isset($_GET['aol'])) {
  $sp = 'aol';
  $OC = 0;
  $sd = $td + 1209600;
  $ed = $td + 31536000;
  $dr = 'oidd.item_del_date';
  $cp = "orders due out after 2 weeks";
  $hc = 'black';
  $hbc = '#92ef9d';
}

// orders due next week
// if ($cOIDnw <> 0) {
// }
if (isset($_GET['aon'])) {
  $sp = 'aon';
  $OC = 0;
  $sd = $td + 604800;
  $ed = $td + 1209600;
  $dr = 'oidd.item_del_date';
  $cp = "orders due out within the next week";
  $hc = 'black';
  $hbc = '#ecf5a9';
}

// orders due this week
// if ($cOIDtw <> 0) {
// }
if (isset($_GET['aot'])) {
  $sp = 'aot';
  $OC = 0;
  $sd = $td;
  $ed = $td + 604800;
  $dr = 'oidd.item_del_date';
  $cp = "orders out due within the next 7 days";
  $hc = 'black';
  $hbc = '#f7a0a0';
}

if (isset($_GET['aoo'])) {
  $sp = 'aoo';
  $OC = 0;
  $sd = $td - 31536000;
  $ed = $td + 31536000;
  $dr = 'oidd.item_del_date';
  $cp = "open order items";
  $hc = 'black';
  $hbc = '#b2b988';
}

// if ($cOIDcom <> 0) {
// }
if (isset($_GET['com'])) {
  $sp = 'com';
  $OC = 1;
  $sd = $td - 31536000;
  $ed = $td + 31536000;
  $dr = 'oidd.item_del_date';
  $cp = "Completed orders";
  $hc = 'black';
  $hbc = '#052700';
}

if(isset($_GET['ord'])){ $ord = $_GET['ord']; }else{ $ord = 'oidd.item_del_date'; }
if(isset($_GET['sort'])){ $sort = $_GET['sort']; }else{ $sort = 'ASC'; }

// echo "<br>Company ID : $CID";
// echo "<br>Order status : $OC";
// echo "<br>Today : $td";
// echo "<br>Start day : $sd";
// echo "<br>End day : $ed";
// echo "<br>Count_orders - due date source : $dr";

$sql = "SELECT COUNT(o.ID) as OID
        FROM orders o
          , company c
          , associate_companies ac
          , division d
          , order_item oi
          , order_item_del_date oidd
        WHERE o.CPID IN(SELECT cp.ID
            						FROM company_partnerships cp
            						WHERE cp.req_CID = ? OR cp.acc_CID = ?)
        AND o.orComp = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND o.tDID = d.ID
        AND oi.OID = o.ID
        AND oidd.OIID = oi.ID
        AND $dr BETWEEN $sd AND $ed
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-or1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $OC);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOID = $row['OID'];
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cOID / $r);
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;
// echo '<br>Start record is : '.$start;
// echo "<br>**** count_orders count : $cOID ****";

if ($cOID < 1) {
  // echo "<br>NO orders placed";
  ?>
  <div class="trs" style="position:relative; top:0%;">
    <div class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "There are 0 Orders"?></div>
  </div>
      <p>NO ORDERS TO REVIEW</p>
  <?php
  header("Location:../home.php?H");
  exit();

}else {
  ?>
  <div class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "There are $cOID $cp" ?></div>
  <table class="trs" style="position:relative; top:0%;">
  <tr>
    <th style="width:1%;"></th>
    <th style="width:5%;">Order</th>
    <th style="width:2%;">Item</th>
    <th style="width:15%; text-align:left; text-indent:5%;">Associate / Partner</th>
    <th style="width:17.5%; text-align:left; text-indent:5%;">OUR Order Ref</th>
    <th style="width:6%;">Short Code</th>
    <th style="width:4.5%;text-align:center;">Qty</th>
    <th style="width:32%;">What to put in here????</th>
    <th style="width:8%; text-align:right; padding-right:2%;">Due date</th>
  </tr>
  <?php

  $sql = "SELECT DISTINCT o.ID as OID
            , cp.req_CID as cliCID
            , oi.ID as OIID
            , LPAD(o.orNos,6,'0') as ordNos
            , LPAD(oi.ord_item_nos,3,'0') as ordItemNos
            , oi.samProd as OTID
            , cr.Assoc_Client_Name as cli_ACIDn
            , cr.Assoc_Supplier_name as sup_ACIDn
            , o.our_order_ref as ourRef
            , oiq.order_qty as ordQty
            , odd.del_Date as ODDIDdd
            , oidd.item_del_date as item_due_date
            , last_update.UID as UID
            , last_update.inputtime as inputtime
            , pr.prod_ref as PRIDs
            , pr.ID as PRID
          FROM company_relationships cr
          -- INNER JOIN orders o ON cr.Assoc_Client_Co = o.tACID
          -- AND cr.Assoc_Supplier_Co = o.fCID
          INNER JOIN orders o ON cr.Assoc_Supplier_Co = o.tACID
                             AND cr.Assoc_Client_Co = o.fCID
          INNER JOIN order_item oi ON oi.OID = o.ID
          INNER JOIN prod_ref pr ON pr.ID = oi.PRID
          INNER JOIN company_partnerships cp ON o.CPID = cp.ID
          INNER JOIN orders_due_dates odd ON odd.OID = o.ID
          INNER JOIN order_item_del_date oidd ON oidd.OIID = oi.ID
          INNER JOIN order_item_qty oiq ON oiq.OIID = oi.id
          INNER JOIN order_placed op ON oi.ID = op.OIID
          INNER JOIN (SELECT opm_order.OPID AS OPID
                               , opm_order.UID AS UID
                               , opm_order.inputtime AS inputtime
                      FROM (SELECT opm.OPID
                               , opm.UID
                               , opm.inputtime
                            FROM order_placed_move opm
                            INNER JOIN order_placed op ON op.ID = opm.OPID
                            INNER JOIN order_item oi ON oi.ID = op.OIID
                            INNER JOIN orders o ON oi.OID = o.ID
                            ORDER BY opm.OPID, opm.ID DESC
                            ) opm_order
                      GROUP BY opm_order.OPID
                      ) last_update ON op.ID = last_update.OPID
          WHERE cr.CPID IN (SELECT ID
                            FROM company_partnerships
                            WHERE ((req_CID = ?) || (acc_CID = ?))
                          )
          AND o.orComp = ?
          AND $dr BETWEEN $sd AND $ed
          ORDER BY $ord $sort
          LIMIT $start, $r
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-or2</b>';
  }else{
  mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $OC);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $sort == 'DESC' ? $sort == 'ASC' : $sort == 'DESC';
  while($row = mysqli_fetch_assoc($result)){
    $OID     = $row['OID'];
    $OIID     = $row['OIID'];
    $OTID     = $row['OTID'];
    $cliCID   = $row['cliCID'];
    $OIDnos   = $row['ordNos'];
    $OIIDnos  = $row['ordItemNos'];
    $OIID     = $row['OIID'];
    $OIDoor   = $row['ourRef'];
    $cli_ACID = $row['cli_ACID'];
    $ACID    = $row['ACID'];
    $CTID    = $row['CTID'];
    $ACIDn    = $row['cli_ACIDn'];
    $pACIDn   = $row['sup_ACIDn'];
    $PRIDpr   = $row['PRIDpr'];
    $OIQIDq   = $row['ordQty'];
    $OIDDIDd  = $row['item_due_date'];
    $PRIDs  = $row['PRIDs'];
    $PRID  = $row['PRID'];

    $idate = date('d-M-Y', $OIDDIDd);

// Taken OUT as DAL sorted in his sql
// LEFT IN UNTIL DAL UPDATES THE SQL
    if ($cliCID <> $CID) {
      $ACIDn = $ACIDn;
      $OTID = $OTID - 1;
    }else {
      $ACIDn = $pACIDn;
      $OTID = $OTID;
    }
    ?>
    <tr>
      <?php
      // Shows you the orders placed within the last 7 days
      // if($od>($td-604740)){
      if ($OTID == 1) {
        $c = 'black';
        $bc = 'f7cc6c';
        $SamProd = 'P';
        // $coName = $pACIDn;
      }elseif ($OTID == 2) {
        $c = 'black';
        $bc = 'bbbbff';
        $SamProd = 'P';
        // $coName = $pACIDn;
      }elseif ($OTID == 3) {
        $c = 'white';
        $bc = 'f243e4';
        $SamProd = 'P';
        // $coName = $pACIDn;
      }elseif ($OTID == 4) {
        $c = 'white';
        $bc = '577268';
        $SamProd = 'P';
        // $coName = $ACIDn;
      }elseif ($OTID == 5) {
        $c = 'black';
        $bc = 'f7cc6c';
        $SamProd = 'S';
        // $coName = $ACIDn;
      }elseif ($OTID == 6) {
        $c = 'black';
        $bc = 'bbbbff';
        $SamProd = 'S';
        // $coName = $ACIDn;
      }elseif ($OTID == 7) {
        $c = 'white';
        $bc = 'f243e4';
        $SamProd = 'S';
        // $coName = $ACIDn;
      }elseif ($OTID == 8) {
        $c = 'white';
        $bc = '577268';
        $SamProd = 'S';
        // $coName = $pACIDn;
      } ?>

      <!-- <td><?php echo "$OID : $OIID :: $cliCID/$CID" ?></td> -->
      <!-- <td><?php echo ($OIID) ?></td> -->
      <td style="width:1%; font-weight: bold;"><?php echo $SamProd?></td>
      <td style="width:5%; color:red; text-align: right; padding-right: 1%;"><a style="color:red; text-decoration:none; " href="<?php echo $urlPage ?>&orb&oi=<?php echo $OID;?>"><?php echo $OIDnos ?></a></td>
      <td style="width:2%;"><a style="color:red; text-decoration:none; " href="<?php echo $urlPage ?>&oed&oi=<?php echo $OIID;?>"><?php echo $OIIDnos ?></a></td>

      <!-- <td style="width:24%; text-align:left; text-indent:1%; border-left: thin solid grey; border-right:thin solid grey; color: <?php echo $c ?>; background-color: #<?php echo $bc ?>;"><a style="text-decoration:none;" href="associates.php?A&asf&id=<?php echo $ACID ?>"><?php echo $ACIDn ?> </a></td> -->

      <td style="width:15%; text-align:left; text-indent:1%; border-left: thin solid grey; border-right:thin solid grey; color: <?php echo $c ?>;background-color: #<?php echo $bc ?>;"><?php echo $ACIDn ?></td>
      <td style="width:17.5%; text-align:left; text-indent:1%; border-right:thin solid grey;"><?php echo $OIDoor;?></td>
      <td style="width:6%; text-align:left; text-indent:3%; border-right:thin solid grey;"><a style="color:red; text-decoration:none;" href="styles.php?S&sis&s=<?php echo $PRID ?>"> <?php echo $PRIDs;?></a></td>
      <td style="width:8%; text-align:right; padding-right:1%; border-right:thin solid grey;"><?php echo $OIQIDq;?></td>
      <td style="width:31%;">*******</td>
      <?php
      if($OIDDIDd<$td){;?>
        <td style="width:8%; text-align:center; border-right:thin solid grey; background-color:#d8081e;"><?php echo $idate ;?></td>
        <?php
      }elseif($OIDDIDd<($td+604740)){;?>
        <td style="width:8%; text-align:center; border-right:thin solid grey; background-color:#f09595;"><?php echo $idate ;?></td>
        <?php
      }elseif($OIDDIDd>($td+121509600)){;?>
        <td style="width:8%; text-align:center; border-right:thin solid grey; background-color:#e2e8a8;"><?php echo $idate ;?></td>
        <?php
      }else{;?>
        <td style="width:8%; text-align:center; border-right:thin solid grey;"><?php echo $idate ;
      } ?>
      </td>
      <?php
      if ($LRIDlast < $td) {
        ?>
        <td style="width:1%; background-color:RED;"></td>
        <?php
      }else {
        ?>
        <td style="width:1%; background-color:green;"></td>
        <?php
      }
    }
  } ?>
  </tr>
  <?php
}
  ?>
</table>

<div style="position:absolute; bottom:5%; right:10%; font-size:200%;">

  <?php
  if (isset($_GET['H'])) {
    $page = 'H';
  }elseif (isset($_GET['L'])) {
    $page = 'L';
  }
    for ($x = 1 ; $x <= $PTV ; $x++){
    echo "<a style='color:red; text-decoration:none;' href='?$page&$sp&p=$x'>  $x  </a>";
  }
  ?>
</div>
