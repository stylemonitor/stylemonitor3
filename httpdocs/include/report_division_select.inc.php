<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_division.inc.php";
$CID = $_SESSION['CID'];

include 'include/from_CID_count_division.inc.php';
include 'include/from_CID_get_min_division.inc.php';
// echo "Min DID $mDID";

include 'from_CID_count_divisions.inc.php';

if (isset($_GET['sdr'])) {
  $head = 'sdr';
}elseif (isset($_GET['sdn'])) {
  $head = 'sdn';
}

if (isset($_GET['d'])) {
  $DID = $_GET['d'];
}

?>
<div class="cpsty" style="color: white; background-color:Purple">Company Divisions</div>

<table class="trs" style="position:absolute; top:3%; left:0%; width:100%;">
  <tr>
    <th>Division Name</th>
    <th>Sections</th>
    <th>Product Types Count</th>
    <th>Style Count</th>
    <th>Order Count</th>
    <th></th>
  </tr>
  <?php
  $sql = "SELECT d.ID as DID
            , d.name as DIDn
          FROM division d
            , associate_companies ac
          WHERE d.ID = ?
          AND d.name NOT IN ('Select Division')
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-rd</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $DID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_assoc($result);
    $DID = $row['DID'];
    $DIDn = $row['DIDn'];
    ?>
    <tr>
      <td><?php echo $DIDn ?></td>
      <td>Sections </td>
      <td>$cPTIDd</td>
      <td>$cPRIDd</td>
      <td>$cOIDd</td>
      <td><a style="text-decoration:none;" href="company.php?C&sde&d=<?php echo $DID ?>">Edit</a></td>
    </tr>
    <?php
  }
  ?>
</table>
<div style="position:absolute; top:9%; left:95%;"><a style="color:black; text-decoration:none;" href="cmpany.php?C&sdr">More</a></div>
