<?php
// session_start();
// echo "<br><b>include/set_session_variables.inc.php</b>";
// echo "<br>Valid User UID == $UID";


$sql = "SELECT u.ID as UID
          , u.firstname as UIDf
          , u.privileges as UIDv
          , c.ID as CID
          , c.name as CIDn
          , ac.ID as ACID
          , cdu.ID as CDUID
          , d.ID as DID
        FROM users u
          , company c
          , associate_companies ac
          , division d
          , company_division_user cdu
        WHERE u.ID = ?
        AND c.UID = u.ID
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND cdu.DID = d.ID
        AND cdu.UID = u.ID
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-ssv</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $UID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $UID = $row['UID'];
  $UIDf = $row['UIDf'];
  $UIDv = $row['UIDv'];
  $CID = $row['CID'];
  $CIDn = $row['CIDn'];
  $ACID = $row['ACID'];
  $CDUID = $row['CDUID'];
  $DID = $row['DID'];
}
// SET THE SESSION VARIABLES
$_SESSION['UID']    = $UID;    //users ID
$_SESSION['UIDf']   = $UIDf;   //users firstname
$_SESSION['UIDv']   = $UIDv;   //users privileges
$_SESSION['CID']    = $CID;    //users company ID
$_SESSION['CIDn']   = $CIDn;   //users company name
$_SESSION['ACID']   = $ACID;   //associate company ID
$_SESSION['CDUID']  = $CDUID;  //users company division ID
$_SESSION['DID']    = $DID;    //division ID

// echo "<br>Session UID : ".$_SESSION['UID']."";
// echo "<br>Session UIDf : ".$_SESSION['UIDf']."";
// echo "<br>Session UIDv : ".$_SESSION['UIDv']."";
// echo "<br>Session CID : ".$_SESSION['CID']."";
// echo "<br>Session CIDn : ".$_SESSION['CIDn']."";
// echo "<br>Session ACID : ".$_SESSION['ACID']."";
// echo "<br>Session CDUID : ".$_SESSION['CDUID']."";

// clear any outstanding incorrect records NEW METHOD
$sql = "UPDATE users
        SET fail = 0
          , Ftime = 0
        WHERE ID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
 echo '<b>FAIL-ssv2</b>';
}else{
 mysqli_stmt_bind_param($stmt, "s", $UID);
 mysqli_stmt_execute($stmt);
}

// Checnk if there are any oreders open
include 'check_company_open_orders.inc.php';

if ($cOID > 0) {
  // echo "<br>Order Count = $cOID";
  header("Location:../home.php?H&home");
  exit();
}else {
  // echo "<br>No ORDERS in work";
  header("Location:../styles.php?S&spt");
  exit();
}
