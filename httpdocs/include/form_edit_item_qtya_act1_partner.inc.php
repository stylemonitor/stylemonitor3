<?php
session_start();
include 'dbconnect.inc.php';
echo "<br><b>include/form_edit_item_qtya_act1_partner.inc.php</b>";
$UID = $_SESSION['UID'];
$CID = $_SESSION['CID'];
$td = date('U');

if (!isset($_POST['YesBtn']) && !isset($_POST['NoBtn']) && !isset($_POST['chgBtn'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['NoBtn'])) {
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  header("Location:../home.php?H&rt3&o=$OID");
  exit();
}elseif (isset($_POST['chgBtn'])) {
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  header("Location:../home.php?H&rt3&o=$OID&OI=$OIID&eit=$OIID&qty");
  exit();
}elseif (isset($_POST['YesBtn'])) {
  // echo "CANCEL ITEM FROM ORDER";
  $incItem = $_POST['incItem'];
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  $OTID = $_POST['OTID'];
  $OPID = $_POST['OPID'];
  $oiQty = $_POST['oiQty'];
  $delDate = $_POST['del'];
  $Nqty = $_POST['Nqty'];
  $canReas = $_POST['canReas'];
  $td = date('U');

  if ($Nqty < $oiQty) {
    $OIMRIDreq = 47;
    $OIMRID = 10;
    $movQty = $oiQty - $Nqty;
    $accReas = "Item quantity reduction of $movQty ACCEPTED";
  }elseif ($Nqty > $oiQty) {
    $OIMRIDreq = 46;
    $OIMRID = 2;
    $movQty = $Nqty - $oiQty;

    // add as an extra item on the order
    include 'from_OIID_get_order_details.inc.php';
  }

  $OID = $OID;
  // $fPRID = $PRID;
  // $tPRID = $pPRID;
  // $sp = $samProd;
  $oItemDes = $canReas;
  // $tItemDes = $pItemRef;

  $orQTY = $movQty;
  // echo "<br>orQTY = $orQTY :: delDate = $delDate";

  $CID = $_SESSION['CID'];
  include 'from_CID_get_mSID.inc.php';
  if (($OTID == 1) || ($OTID == 2)) {
    include 'form_add_item_to_order_act1.inc.php';
  }elseif (($OTID == 3) || ($OTID == 4)) {
    echo "<br>PARTNERSHIP ORDER QTY CHANGE";
    include 'form_edit_item_qtya_act1_partner.inc.php';
  }
}
