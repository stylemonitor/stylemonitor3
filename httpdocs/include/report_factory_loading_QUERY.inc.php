<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_factory_loading_QUERY.inc.php";

$CID = $_SESSION['CID'];

$sql = "SELECT TOTALS.cliCID
-- SQL is 'order_report_with stage1 to stage5_loading_20201116.sql'
     , cr.Client_Name AS cliCIDn
     , cr.Assoc_Client_Co AS cli_ACID
     , cr.Assoc_Client_Name AS cli_ACIDn
     , TOTALS.supCID
     , cr.Supplier_Co_Name AS supCIDn
     , cr.Assoc_Supplier_Co AS sup_ACID
     , cr.Assoc_Supplier_name AS sup_ACIDn
     , TOTALS.UID AS TUID
     , TOTALS.inputtime AS Ttd
     , TOTALS.OID
     , TOTALS.OIID
     , TOTALS.ordNos
     , TOTALS.ordItemNos
     , TOTALS.OTID
     , TOTALS.ourRef
     , TOTALS.ODDIDdd
     , TOTALS.item_due_date
     , TOTALS.CPID
     , TOTALS.PRID
     , TOTALS.PRIDs
     , TOTALS.OIQIDq
     , TOTALS.Stage
FROM
  (
  SELECT DISTINCT
       o.fCID AS cliCID
       , cp.acc_CID AS supCID
       , Mvemnt.ORDER_ID AS OID
       , oi.ID AS OIID
       , o.orNos AS ordNos
       , oi.ord_item_nos AS ordItemNos
       , ot.ID AS OTID
       , o.our_order_ref AS ourRef
       , odd.del_Date AS ODDIDdd
       , oidd.item_del_date AS item_due_date
       , o.CPID AS CPID
       , pr.ID AS PRID
       , pr.prod_ref AS PRIDs
       , Mvemnt.ordQty AS OIQIDq
       , cp.req_CID
       , o.tACID
  -- CALCULATE INDIVIDUAL GARMENT MOVEMENT VALUES FOR EACH STAGE
       , SUM(Mvemnt.INTO_STG - Mvemnt.OUT_OF_NXT_STG - Mvemnt.REJ_BY_STG + Mvemnt.REJ_BY_NXT_STG - Mvemnt.COR_IN_STG + Mvemnt.COR_IN_NXT_STG + Mvemnt.COR_OUT_STG - Mvemnt.COR_OUT_NXT_STG) AS Stage
       , Mvemnt.UID AS UID
       , Mvemnt.inputtime AS inputtime
  FROM (
      SELECT oimr.ID AS OIMR_ID
           , o.ID AS ORDER_ID
           , o.ID AS Order_Placed_ID
           , oiq.order_qty AS ordQty
           , last_update.UID
           , last_update.inputtime
/*
USE THE FOLLOWING OIMRs FOR EACH STAGE:
           Stage 1 - OIMRs 3, 4, 13, 14, 23, 24, 29, 30
           Stage 2 - OIMRs 4, 5, 14, 15, 24, 25, 30, 31
           Stage 3 - OIMRs 5, 6, 15, 16, 25, 26, 31, 32
           Stage 4 - OIMRs 6, 7, 16, 17, 26, 27, 32, 33
           Stage 5 - OIMRs 7, 8, 17, 18, 27, 28, 33, 34
*/
           , IF((CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END)) AS INTO_STG
           , IF((CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END)) AS OUT_OF_NXT_STG
           , IF((CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END)) AS REJ_BY_STG
           , IF((CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END)) AS REJ_BY_NXT_STG
           , IF((CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END)) AS COR_IN_STG
           , IF((CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END)) AS COR_IN_NXT_STG
           , IF((CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END)) AS COR_OUT_STG
           , IF((CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = ? THEN SUM(opm.omQty) END)) AS COR_OUT_NXT_STG
      FROM order_placed_move opm
         INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
         INNER JOIN order_placed op ON opm.OPID = op.ID
         INNER JOIN order_item oi ON op.OIID = oi.ID
         INNER JOIN orders o ON oi.OID = o.ID
         INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
         INNER JOIN (SELECT opm_order.OPID AS OPID
                   , opm_order.UID AS UID
                   , opm_order.inputtime AS inputtime
               FROM (SELECT opm.OPID
                     , opm.UID
                     , opm.inputtime
                   FROM order_placed_move opm
                     INNER JOIN order_placed op ON op.ID = opm.OPID
                     INNER JOIN order_item oi ON oi.ID = op.OIID
                     INNER JOIN orders o ON oi.OID = o.ID
                   ORDER BY opm.OPID, opm.ID DESC
                 ) opm_order
               GROUP BY opm_order.OPID
              ) last_update ON op.ID = last_update.OPID
        WHERE o.orComp = ?
      GROUP BY o.ID, oimr.ID
     ) Mvemnt INNER JOIN order_item oi ON Mvemnt.Order_Placed_ID = oi.ID
          INNER JOIN orders o ON oi.OID = o.ID
          INNER JOIN order_type ot ON oi.OTID = ot.ID
          INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
          INNER JOIN order_item_del_date oidd ON oidd.OIID = oi.ID
          INNER JOIN prod_ref pr ON pr.ID = oi.PRID
          INNER JOIN company_partnerships cp ON o.CPID = cp.ID
                          AND cp.req_CID = o.fCID
          INNER JOIN orders_due_dates odd ON odd.OID = o.ID
  WHERE oidd.item_del_date BETWEEN (unix_timestamp(now()) - 3600000)
                               AND (unix_timestamp(now()) + 36000)
  GROUP BY Mvemnt.ORDER_ID
  ) TOTALS
              INNER JOIN company_relationships cr ON TOTALS.CPID = cr.CPID
                         AND TOTALS.req_CID = cr.Client_ID
                         AND TOTALS.tACID = cr.Assoc_Supplier_Co
WHERE TOTALS.cliCID = ?
  AND cr.CPID IN (SELECT ID
                  FROM company_partnerships
                  WHERE ((req_CID = ?) || (acc_CID = ?))
                 )
  AND TOTALS.stage NOT IN (0)
GROUP BY cr.Client_ID, OID
ORDER BY item_due_date DESC
LIMIT ?
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-flQ</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssssssssssssssssssss", $s1, $s1, $s2, $s2, $s3, $s3, $s4, $s4, $s5, $s5, $s6, $s6, $s7, $s7, $s8, $s8, $OC, $CID, $CID, $CID, $limit);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $cliCID = $row['cliCID'];
    $cliCIDn = $row['cliCIDn'];
    $cli_ACID = $row['cli_ACID'];
  $cli_ACIDn = $row['cli_ACIDn'];
    $supCID = $row['supCID'];
    $supCIDn = $row['supCIDn'];
    $sup_ACID = $row['sup_ACID'];
  $sup_ACIDn = $row['sup_ACIDn'];
    $TUID = $row['TUID'];
    $Ttd = $row['Ttd'];
    $OID = $row['OID'];
  $OIID = $row['OIID'];
  $OIDnos = $row['ordNos'];
  $OIIDnos = $row['ordItemNos'];
    $OTID = $row['OTID'];
  $ourRef = $row['ourRef'];
    $ODDIDdd = $row['ODDIDdd'];
  $OIDDIDd = $row['item_due_date'];
    $CPID = $row['CPID'];
  $PRIDs = $row['PRIDs'];
  $OIQIDq = $row['OIQIDq'];
  $Stage = $row['Stage'];

    $CLIENT = $row['CLIENT'];
    $SUPPLIER = $row['SUPPLIER'];

// Taken OUT as DAL sorted in his sql - LEFT IN UNTIL DAL UPDATES THE SQL
    if ($cliCID <> $CID) {
      $cli_ACIDn = $cli_ACIDn;
      $OTID = $OTID - 1;
    }else {
      $cli_ACIDn = $sup_ACIDn;
      $OTID = $OTID;
    }

    $idate = date('d-M-Y', $OIDDIDd);
    ?>
    <tr>
      <?php
      // Shows you the orders placed within the last 7 days
      // if($od>($td-604740)){
      if ($OTID == 1) {
        $c = 'black';
        $bc = 'f7cc6c';
        $SamProd = 'P';
        // $coName = $pACIDn;
      }elseif ($OTID == 2) {
        $c = 'black';
        $dferr = '';
        $bc = 'bbbbff';
        $SamProd = 'P';
        // $coName = $pACIDn;
      }elseif ($OTID == 3) {
        $c = 'white';
        $bc = 'f243e4';
        $SamProd = 'P';
        // $coName = $pACIDn;
      }elseif ($OTID == 4) {
        $c = 'white';
        $bc = '577268';
        $SamProd = 'P';
        // $coName = $ACIDn;
      }elseif ($OTID == 5) {
        $c = 'black';
        $bc = 'f7cc6c';
        $SamProd = 'S';
        // $coName = $ACIDn;
      }elseif ($OTID == 6) {
        $c = 'black';
        $bc = 'bbbbff';
        $SamProd = 'S';
        // $coName = $ACIDn;
      }elseif ($OTID == 7) {
        $c = 'white';
        $bc = 'f243e4';
        $SamProd = 'S';
        // $coName = $ACIDn;
      }elseif ($OTID == 8) {
        $c = 'white';
        $bc = '577268';
        $SamProd = 'S';
        // $coName = $pACIDn;
      }

      if ($Stage < 0) {
        $sbc = 'red';
      }elseif ($Stage == 0) {
        $sbc = 'green';
      }else {
        $sbc = '#f1f1f1';
      }
      ?>
      <!-- <td><?php echo "$OID : $OIID :: $cliCID/$CID" ?></td> -->
      <!-- <td><?php echo ($OIID) ?></td> -->
      <td style="width:2%; font-weight: bold;"><?php echo $SamProd?></td>
      <td style="width:6%; color:red; text-align: right; padding-right: 1%;"><a style="color:red; text-decoration:none; " href="loading.php?L&orb&oi=<?php echo $OIID;?>"><?php echo $OIDnos.':'.$OIIDnos;?></a></td>
      <td style="width:1%; font-weight: bold; background-color:yellow;"><a style="color:red; text-decoration:none; " href="loading.php?L&oed&oi=<?php echo $OIID;?>">e</a></td>
      <td style="width:26%; text-align:left; text-indent:1%; border-left: thin solid grey; border-right:thin solid grey; color: <?php echo $c ?>;background-color: #<?php echo $bc ?>;"><a style="text-decoration:none;" href="associates.php?A&asc&id=<?php echo $cli_ACID ?>"><?php echo $cli_ACIDn ?></a></td>
      <td style="width:37%; text-align:left; text-indent:1%; border-right:thin solid grey;"><?php echo $ourRef;?></td>
      <td style="width:6%; text-align:right; padding-right:1%; background-color: <?php echo $sbc ?>; border-right:thin solid grey;"><?php echo $Stage ?></td>
      <td style="width:8%; text-align:right; padding-right:1%; border-right:thin solid grey;"><?php echo $OIQIDq;?></td>
      <?php
      if($OIDDIDd<$td){;?>
        <td style="width:10%; text-align:right; padding-right:1%; background-color:#d8081e;"><?php echo $idate ;?></td>
        <?php
      }elseif($OIDDIDd<($td+604740)){;?>
        <td style="width:10%; text-align:right; padding-right:1%; background-color:#f09595;"><?php echo $idate ;?></td>
        <?php
      }elseif($OIDDIDd>($td+121509600)){;?>
        <td style="width:10%; text-align:right; padding-right:1%; background-color:#e2e8a8;"><?php echo $idate ;?></td>
        <?php
      }else{;?>
        <td style="width:10%; text-align:right; padding-right:1%;"><?php echo $idate ;?>
      </td>
      <?php
    }?>
    </tr>
    <?php
    if ($LRIDlast < $td) {
      ?>
      <td style="width:2%; background-color:RED;"></td>
      <?php
    }else {
      ?>
      <td style="width:2%; background-color:green;"></td>
      <?php
    }
  }
  } ?>
  </table>

  <?php
  if (isset($_GET['sum'])) {
    ?>
    <div style="position:absolute; bottom:2%; left:0%; width:100%; z-index:1;">
      The 'top' 3 are listed above - to see all items in a given section click on the tab at the top OR the name of the section
    </div>
    <?php
  }
  ?>
