<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_get_CPID.inc.php</b>";

$sql = "SELECT ID as CPID
        FROM company_partnerships
        WHERE req_CID = ?
        AND acc_CID = ?
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fCgc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $CPID  = $row['CPID'];
}
