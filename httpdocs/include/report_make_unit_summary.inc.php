<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_movement_log.inc.php";

// include 'R_on_detail_table.inc.php';
// include 'R_on_item_detail_table.inc.php';
include 'include/R_on_sum_MU_LOG_MP1.inc.php';
include 'include/R_on_sum_MU_LOG_MP2.inc.php';
include 'include/R_on_sum_MU_LOG_MP3.inc.php';
include 'include/R_on_sum_MU_LOG_MP4.inc.php';
include 'include/R_on_sum_MU_LOG_MP5.inc.php';
include 'include/R_on_sum_MU_LOG_MP6.inc.php';


if (isset($_GET['oi'])) {
  $OIID = $_GET['oi'];
// }
// // Get the posted order item nos
// if (isset($_GET['op'])) {
//   $OPID = $_GET['op'];
//   // echo '<br>Order Item nos is : '.$OIID;

  // Set the rows per page
  $rpp = 10;

  // Check the URL to see what page we are on
  if (isset($_GET['pa'])) {
    $pa = $_GET['pa'];
  }else{
    $pa = 0;
  }

  // Check which page we are on
  if ($pa > 1) {
    $start = ($pa * $rpp) - $rpp;
  }else {
    $start = 0;
  }

  // count the number of rows that will be in the table
  // REMEBER to count them for teh requirement of the table ie, this week, next week and Later
  $sql = "SELECT COUNT(oi.ID) as OIID
            , ac.name as ACIDn
            , d.name as DIDn
            , oiq.order_qty oiQTY
            , odd.del_Date as odDIDd
            , o.orDate as OIDs
            -- , o.UID as UID
            -- , o.orNos as OIDnos
            -- , oi.ord_item_nos as OIIDnos
            -- , opm.omQty as opmQTY
            -- , oimr.reason as oimrREAS
            -- , opm.inputtime as opmDATE
          FROM orders o
            , order_item oi
            , orders_due_dates odd
            , order_placed op
            , order_placed_move opm
            , order_item_movement_reason oimr
            , associate_companies ac
            , division d
            , order_item_qty oiq
          WHERE op.ID = ?
          AND oi.OID = o.ID
          AND op.OIID = oi.ID
          AND opm.OPID = op.ID
          AND opm.OIMRID = oimr.ID
          AND o.DID = d.ID
          AND d.ACID = ac.ID
          AND oiq.OIID = oi.ID
          AND odd.OID = o.ID;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    // echo '<b>FAIL</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $OPID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $COID = $row['OIID'];
    $ACIDn = $row['ACIDn'];
    $DIDn = $row['DIDn'];
    $oiQTY = $row['oiQTY'];
    $odDIDd = $row['odDIDd'];
    $OIDs = $row['OIDs'];

    // turn the order date into an easy to read format
    // $OIDs = strtotime($OIDs);
    $OIDs = date('D-d-M-Y', $OIDs);

    // turn the order delivery date into an easy to read format
    $odDIDd = date('D-d-M-Y', $odDIDd);

    // echo '<br><br><br>Count of orders for this company is : '.$COID;

    // How many pages will this give us Pages To View $PTV
    // ceil rounds up a number ie 1.1 becomes 2 1.9 is also 2
    $PTV = ($COID / $rpp);

    // echo '<br>The number of pages will be : '.$PTV;
    $PTV = ceil($PTV);
    // echo '<br>The number of pages will be : '.$PTV;

    // echo '<br>Start record is : '.$start;
  }
  ?>

  <!-- Get a table for the manufacturing unit details -->
  <?php
  $sql = "SELECT ac.name as ACIDnm
            , d.name as DIDnm
            , s.name as SIDnm
            , opm.omQty as OPMIDy
            -- , oiq.order_qty as OIQIDy
            , oidd.item_del_date as oidDIDd
            , op.inputtime as OPIDd
          FROM associate_companies ac
            , division d
            , section s
            , order_placed op
            , order_item oi
            , order_item_qty oiq
            , order_item_del_date oidd
            , order_placed_move opm
            , order_item_movement_reason oimr
          WHERE op.ID = ?
          AND op.SID = s.ID
          AND s.DID = d.ID
          AND d.ACID = ac.ID
          AND op.OIID = oi.ID
          AND oiq.OIID = oi.ID
          AND oidd.OIID = oi.ID
          AND opm.OPID = op.ID
          AND opm.OIMRID = oimr.ID
          AND oimr.ID IN (1,2,11);";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    // echo '<b>FAIL</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $OPID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_array($result)){


      $ACIDnm = $row['ACIDnm'];
      $DIDnm = $row['DIDnm'];
      $SIDnm = $row['SIDnm'];
      $OPMIDy = $row['OPMIDy'];
      $oidDIDd = $row['oidDIDd'];
      $OPIDd = $row['OPIDd'];

      // turn the order item placed date into an easy to read format
      // $OPIDd = strtotime($OPIDd);
      $OPIDd = date('d-M-Y', $OPIDd);

      // turn the order item delivery date into an easy to read format
      // $oidDIDd = strtotime($oidDIDd);
      $oidDIDd = date('d-M-Y', $oidDIDd);

      // You need to adjust the placed quantity IF the unit is the PRIMARY make unit
      ?>

      <table class="trs" style="position:absolute; top:37%; left:0%; width:100%; z-index:1;">
        <caption class="cpsty">Manufacturing unit/s</caption>
        <tr>
          <th>Company</th>
          <th>Division</th>
          <th>Section</th>
          <th>Placed Quantity</th>
          <th>Placed Date</th>
          <th>Due Date</th>
        </tr>
        <tr>
          <td><?php echo $ACIDnm ?></td>
          <td><?php echo $DIDnm ?></td>
          <td><?php echo $SIDnm ?></td>
          <td><?php echo $OPMIDy ?></td>
          <td><?php echo $OPIDd ?></td>
          <td><?php echo $oidDIDd ?></td>
        </tr>
      <?php } ?>
    </table>
  <?php } ?>

  <div style="position:absolute; bottom:10%; left:20%;">
    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a href='?D&ro&oi=$OPID&op=$OPID&pa=$x'>  $x  </a>";
    } ?>
  </div>
  <?php
} ?>
