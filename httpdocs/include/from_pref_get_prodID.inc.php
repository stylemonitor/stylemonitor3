<?php
// see if there is a product type from the product reference for the input name
// check is ONLY for the users company
// pt.CID replaced with pt.DIDchanged
$sql = "SELECT pt.ID as ID
        FROM product_type pt
        WHERE pt.CID = ?
        AND pt.name = ?
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo 'FAIL-fpgp';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $ACID, $pref);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  if($row = mysqli_fetch_array($result)) {
    $proDID = $row['ID'];
  }else {
    $proDID = '0';
  }
}
