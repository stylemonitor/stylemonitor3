<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "include/form_unlock_user.inc.php";
include 'set_urlPage.inc.php'; // $sLLIDt

// CHECK that the user is an admin user
include 'from_UID_get_user_details.inc.php';

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'User Suspension';
$pddhbc = $usaCol;
include 'page_description_date_header.inc.php';

if (($UIDa == 2) &&($UIDv == 1)) {
  // echo "CAN UNLOCK";
  if (isset($_GET['ulk'])) {
    $sUID = $_GET['ulk'];
    include 'from_sUID_get_user_details.inc.php';

    ?>
    <div style="position:absolute; top:20%; height:3.5%; left:29%; width:43%;font-size: 130%; font-weight: BOLD; background-color:#fff890; border-radius:10px; border-bottom: thick solid grey; z-index:1;">
      Unlock User Account
    </div>

    <div style="position:absolute; top:24.1%; height:33.5%; left:29%; width:43%; padding-top:0.5%; background-color:#fffddd;font-size: 150%; border-radius:10px; z-index:1;">
      <br><br><br>
      <?php echo "$sUIDf $sUIDs" ?>
    </div>

    <form class="" action="include/form_user_amendment_ulk_act.inc.php" method="post">
      <input type="hidden" name="url" value="<?php echo $urlPage ?>">
      <input type="hidden" name="sUID" value="<?php echo $sUID ?>">
      <button class="entbtn" type="submit" style="position:absolute; top:52%; height:3.8%; left:38.5%; width:10%; padding-top: 0.2%; background-color: <?php echo $fsvCol ?>; z-index:1;" name="change">Confirn</button>
      <button class="entbtn" type="submit" style="position:absolute; top:52%; height:3.8%; left:52.5%; width:10%; padding-top: 0.2%; background-color: <?php echo $fcnCol ?>; z-index:1;" name="cancel">CANCEL</button>
    </form>
    <?php
  }elseif (isset($_GET['sus'])) {
    $sUID = $_GET['sus'];
    // should not be able to suspend yourself
    if ($UID == $sUID) {
      // echo "CANNOT SUSPEND YOURSELF";
      ?>
      <div style="position:absolute; top:20%; height:3.5%; left:29%; width:43%;font-size: 130%; font-weight: BOLD; background-color:#fff890; border-radius:10px; border-bottom: thick solid grey; z-index:1;">
        YOU CANNOT SUSPEND YOURSELF
      </div>
      <?php
    }else {
      include 'from_sUID_get_user_details.inc.php';
      // echo "<br><br><br><br>USER SUSPENDED";
      // echo "<br>User First $sUIDf";
      ?>
      <div style="position:absolute; top:20%; height:3.5%; left:29%; width:43%;font-size: 130%; font-weight: BOLD; background-color:#fff890; border-radius:10px; border-bottom: thick solid grey; z-index:1;">
        Suspend User Account
      </div>

      <div style="position:absolute; top:24.1%; height:33.5%; left:29%; width:43%; padding-top:0.5%; background-color:#fffddd;font-size: 150%; border-radius:10px; z-index:1;">
        <br><br><br>
        <?php echo "$sUIDf $sUIDs" ?>
      </div>

      <form class="" action="include/form_user_amendment_sus_act.inc.php" method="post">
        <input type="hidden" name="url" value="<?php echo $urlPage ?>">
        <input type="hidden" name="sUID" value="<?php echo $sUID ?>">
        <button class="entbtn" type="submit" style="position:absolute; top:52%; left:33.5%; width:20%; padding-top: 0.2%; background-color: <?php echo $fsvCol ?>; z-index:1;" name="change">Confirm Suspension</button>
        <button class="entbtn" type="submit" style="position:absolute; top:52%; left:57.5%; width:10%; padding-top: 0.2%; background-color: <?php echo $fcnCol ?>; z-index:1;" name="cancel">CANCEL</button>
      </form>
      <?php
    }
  }elseif (isset($_GET['rev'])) {
    $sUID = $_GET['rev'];
    include 'from_sUID_get_user_details.inc.php';

    // echo "<br><br><br><br><br>USER RE-ACTIVATED";
    ?>
    <div style="position:absolute; top:20%; height:3.5%; left:29%; width:43%;font-size: 130%; font-weight: BOLD; background-color:#fff890; border-radius:10px; border-bottom: thick solid grey; z-index:1;">
      Re-activate User Account
    </div>

    <div style="position:absolute; top:24.1%; height:33.5%; left:29%; width:43%; padding-top:0.5%; background-color:#fffddd;font-size: 150%; border-radius:10px; z-index:1;">
      <br><br><br>
      <?php echo "$sUIDf $sUIDs" ?>
    </div>

    <form class="" action="include/form_user_amendment_rev_act.inc.php" method="post">
      <input type="hidden" name="url" value="<?php echo $urlPage ?>">
      <input type="hidden" name="sUID" value="<?php echo $sUID ?>">
      <button class="entbtn" type="submit" style="position:absolute; top:52%;  left:33.5%; width:20%; padding-top: 0.2%; background-color: <?php echo $fsvCol ?>; z-index:1;" name="re_act">Confirn Re-activation</button>
      <button class="entbtn" type="submit" style="position:absolute; top:52%; left:57.5%; width:10%; padding-top: 0.2%; background-color: <?php echo $fcnCol ?>; z-index:1;" name="cancel">CANCEL</button>
    </form>
    <?php
  }
  header("Location:../$company.php?C&usa");
  exit();
}else {
  header("Location:../$company.php?C&usa");
  exit();
}
