<?php
session_start();
// echo '<b>include/form_new_style_act.inc.php</b>';
// echo '<br>FROM:include/form_new_style.inc.php';

include 'set_urlPage.inc.php';

$OIID = $_POST['OIID'];
$UID = $_SESSION['UID'];
$td = date('U');

if (!isset($_POST['update']) && (!isset($_POST['cancel']))) {
  header("Location:../location:..index.php");
  exit();
}elseif (isset($_POST['cancel'])) {
  header("Location:../$urlPage&orb&oi=$OIID");
  exit();
}elseif(isset($_POST['update'])){
  include 'dbconnect.inc.php';

  $OPMID = $_POST['OPMID'];
  $oriQTY = $_POST['oriQTY'];
  $oirNOTE = $_POST['oirNOTE'];
  $revQTY = $_POST['revQTY'];
  $revNote = $_POST['revNote'];

  // echo "<br> Original Qty : $oriQTY";
  // echo "<br> Original Note : $oirNOTE";
  // echo "<br> Revised Qty : $revQTY";
  // echo "<br> Revised Qty : $revNote";
  // echo "<br> OIID : $OIID";
  // echo "<br> OPMID : $OPMID";

  if (empty($revQTY)) {$revQTY = $oriQTY;
  }else {$revQTY =$revQTY;
  }

  // if (empty($revNote)) {$revNote = $oirNOTE;
  // }else {$revNote =$revNote;
  // }

  // Insert original data into log and new data in to main file
  if (($revQTY == $oriQTY) && ($revNote == $oirNOTE)) {
    // echo "<br>NO Movement correction";
    // NO OMRID set - just the note changing
    // treat as CANCEL
    header("Location:../$urlPage&orb&oi=$OIID");
    exit();
  }elseif (($revQTY == $oriQTY) && ($revNote <> $oirNOTE)) {
    // update the comment

    // Start transaction
    mysqli_begin_transaction($mysqli);
    try {
      // to

      // echo "Update the comment only";
      $sql = "UPDATE order_placed_move
              SET type = ?
              WHERE ID = ?
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo '<b>FAIL-edda</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ss", $revNote, $OPMID);
        mysqli_stmt_execute($stmt);
      }

      $sql = "INSERT INTO log_despatch_details
                (UID, OPMID, type, inputtime)
              VALUES (?,?,?,?)
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo '<b>FAIL-edda2</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ssss", $UID, $OPMID, $oirNOTE, $td);
        mysqli_stmt_execute($stmt);
      }

      header("Location:../$urlPage&orb&oi=$OIID");
      exit();
    }elseif ($revQTY < $oriQTY) {
      // echo "<br>Movement correction REDUCTION";
      $chQty = $oriQTY - $revQTY;
      $OMIRID = 35;
      if (empty($revNote)) {
        $revNote = "Reduced quantity sent";
      }else {
        $revNote = $oirNOTE;
      }
      // echo "<br>Reduction of items sent by : $revQTY OMIRID = ??";
    }elseif ($revQTY > $oriQTY) {
      // echo "<br>Movement correction INCREASE";
      $chQty = $revQTY - $oriQTY;
      $OMIRID = 36;
      if (empty($revNote)) {
        $revNote = "Increased quantity sent";
      }else {
        $revNote = $oirNOTE;
      }
      // echo "<br>Increase of items sent by : $revQTY OMIRID = ??";
    }

    // update the order_placed_move table
    $sql = "INSERT INTO order_placed_move
              (OPID, OIMRID, UID, omQty, type, inputtime)
            VALUES
              (?,?,?,?,?,?)
            -- WHERE ID = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-edda</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssssss", $OIID, $OMIRID, $UID, $chQty, $revNote, $td);
      mysqli_stmt_execute($stmt);
    }

    // // add into the log_despatch_details
    // $sql = "
    // ;";
    // $stmt = mysqli_stmt_init($con);
    // if (!mysqli_stmt_prepare($stmt, $sql)) {
    //   echo '<b>FAIL-ecna</b>';
    // }else{
    //   mysqli_stmt_bind_param($stmt, "",);
    //   mysqli_stmt_execute($stmt);
    // }

  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  header("Location:../$urlPage&orb&oi=$OIID&des=$OPMID");
  exit();
}
