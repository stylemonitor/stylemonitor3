<?php
session_start();
// echo "include/page_selection_section_header.inc.php";
include 'set_urlPage.inc.php';

include 'from_CID_count_partner_clients.inc.php';    // get the admin user of the company $cpCPID
include 'from_CID_count_partner_suppliers.inc.php';    // get the admin user of the company $cpCPID
include 'from_UID_get_user_details.inc.php';    // get the admin user of the company $cpCPID
include 'SM_colours.inc.php';

$CID = $_SESSION['CID'];

$rtnPage = "home.php?H&tab=$tab&ot=$OTID&sp=$sp";

if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
}

if (isset($_GET['ot'])) {
  $OTID = $_GET['ot'];
}

if ($itemPref <> 1) {
    $itemPref = $itemPref;
}

if ($clientPref <> 1) {
    $itemPref = $clientPref;
}

if (isset($_GET['ot'])) {
  $ot = $_GET['ot'];
  if ($ot == 1) {
    $otbc = '#f7cc6c';
  }elseif ($ot == 2) {
    $otbc = '#bbbbff';
  }elseif ($ot == 3) {
    $otbc = '#d68e79';
  }elseif ($ot == 4) {
    $otbc = '#c9d1ce';
  }
}else {
  if (($clientPref == 1) && ($salesPref == 1)) {
    $ot = 1;
    $otbc = '#f7cc6c';
  }elseif (($clientPref == 1) && ($salesPref == 2)) {
    $ot = 2;
    $otbc = '#bbbbff';
  }elseif (($clientPref == 2) && ($salesPref == 1)) {
    $ot = 3;
    $otbc = '#d68e79';
  }elseif (($clientPref == 2) && ($salesPref == 2)) {
    $ot = 4;
    $otbc = '#c9d1ce';
  }
}

if (isset($_GET['sp'])) {
  if ($sp == 1) {
    $spbc = "#fa7b7b";
  }elseif ($sp == 2) {
    $spbc ="#76f8bc";
  }
}else {
  if ($itemPref == 1) {
    $sp = 1;
    $spbc = "#fa7b7b";
  }else {
    $sp = 2;
    $spbc ="#76f8bc";
  }
}

include 'page_selection_header.sql.php';

// $sql = "SELECT *
// FROM
// (
// SELECT CASE WHEN ORDER_STATUS.DEL_WEEK = yearweek(curdate(),7)   THEN IF(ORDER_STATUS.DEL_DATE < curdate(),'1','2')
// -- SQL is page_selection_header.sql
//             WHEN ORDER_STATUS.DEL_WEEK = yearweek(curdate(),7)+1 THEN '3'
//             WHEN ORDER_STATUS.DEL_WEEK > yearweek(curdate(),7)+1 THEN '4'
//             ELSE '1'
//        END AS STATUS
//        , ORDER_STATUS.OTID as OTID
//        , ORDER_STATUS.SAM_PROD AS SAM_PROD
//        , COUNT(DISTINCT ORDER_STATUS.OIID) AS COUNT_OIID
//        , COUNT(DISTINCT ORDER_STATUS.OID) AS COUNT_OID
//        , CASE WHEN ORDER_STATUS.OTID IN (1,3) THEN(SUM(ORDER_STATUS.ORDER_QTY) - ORDER_STATUS.CLIENT)
//               WHEN ORDER_STATUS.OTID IN (2,4) THEN(SUM(ORDER_STATUS.ORDER_QTY) - ORDER_STATUS.SUPPLIER)
//          END AS QTY
// FROM
// (
// SELECT DISTINCT
//    Mvemnt.OID AS OID
//    , Mvemnt.O_ITEM_ID AS OIID
//    , yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) AS DEL_WEEK
//    , from_unixtime(oidd.item_del_date,'%Y-%m-%d') AS DEL_DATE
//    , oiq.order_qty AS ORDER_QTY
//    , oi.samProd AS SAM_PROD
//    , CASE WHEN o.tCID = o.fCID
//           THEN o.OTID
//           ELSE CASE WHEN o.tCID = ? THEN 3
//                ELSE 4
//                END
//      END AS OTID
//    , SUM(Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT) AS CLIENT
//    , SUM(Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER) AS SUPPLIER
// FROM (
//   SELECT DISTINCT
//          oimr.ID AS OIMR_ID
//          , oimr.reason AS Reason
//          , oi.ID AS O_ITEM_ID
//          , opm.ID AS OPMID
//          , o.ID AS OID
//          , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
//          , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
//          , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
//          , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
//          , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
//          , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
//          , IF((CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END)) AS CORR_OUT_SUPPLIER
//          , IF((CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END)) AS CORR_IN_SUPPLIER
//   FROM order_placed_move opm
//        INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
//        INNER JOIN order_placed op ON opm.OPID = op.ID
//        INNER JOIN order_item oi ON op.OIID = oi.ID
//        INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
//        INNER JOIN orders o ON oi.OID = o.ID
//   GROUP BY opm.ID, oi.ID, oimr.ID
//  ) Mvemnt INNER JOIN order_item oi ON Mvemnt.O_ITEM_ID = oi.ID
//           INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
//           INNER JOIN orders o ON oi.OID = o.ID
//           INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
//           INNER JOIN company c ON o.fCID = c.ID
//           INNER JOIN company cS ON o.tCID = cS.ID
//           INNER JOIN associate_companies ac ON o.fACID = ac.ID
//           INNER JOIN associate_companies acS ON cS.ID = acS.CID
//           INNER JOIN division d ON o.fDID = d.ID
//           INNER JOIN division dS ON acS.ID = dS.ACID
// WHERE (o.fCID = ? OR o.tCID = ?)
//   AND oi.itComp = 0  -- Only include open order items
//   AND OTID = ?
// GROUP BY OIID
// ORDER BY OIID ASC
// ) ORDER_STATUS
// GROUP BY STATUS, SAM_PROD, OTID
// ) OTID_AND_STATUS
// WHERE OTID_AND_STATUS.STATUS = ?
// ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b><br><br><br><br><br><br>FAIL-fdcs</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssss", $CID, $CID, $CID, $OTID, $tab);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $OTID = $row['OTID'];
    $sp = $row['SAM_PROD'];
    $tab = $row['STATUS'];
    $QTY = $row['QTY'];
    $cOIID = $row['COUNT_OIID'];
    $cOID = $row['COUNT_OID'];

    ?>
    <div style="position:absolute; top:8.5%; height:2.9%; left:7%; width:20%; background-color: <?php echo $salAssCol ?>; border:thin solid black; z-index:1;">Saleswwww</div>
    <div style="position:absolute; top:8.5%; height:2.9%; left:29%; width:20%; background-color:  <?php echo $purAssCol ?>; border:thin solid black; z-index:1;">Purchases</div>
    <div style="position:absolute; top:8.5%; height:2.9%; left:51%; width:20%; background-color:  <?php echo $salPrtCol ?>; border:thin solid black; z-index:1;">Partner Sales</div>
    <div style="position:absolute; top:8.5%; height:2.9%; left:73%; width:20%; background-color:  <?php echo $purPrtCol ?>; border:thin solid black; z-index:1;">Partner Purchases</div>
    <?php

    if ($tab == 1) {
      // OVERDUE SAMPLES
      if (($tab == 1) && ($OTID == 1) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:11.5%; height: 2.5%; left:7%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:7%; width:10%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
      // PRODUCTION
      if (($tab == 1) && ($OTID == 1) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:17.1%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:17.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
      // SAMPLES
      if (($tab == 1) && ($OTID == 2) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:29%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:29%; width:10%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
      // PRODUCTION
      if (($tab == 1) && ($OTID == 2) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:39.1%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:39.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
      // SAMPLES
      if (($tab == 1) && ($OTID == 3) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:51%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:51%; width:10%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
      // PRODUCTION
      if (($tab == 1) && ($OTID == 3) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:61.1%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:61.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
      // SAMPLES
      if (($tab == 1) && ($OTID == 4) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:73%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:73%; width:10%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
      // PRODUCTION
      if (($tab == 1) && ($OTID == 4) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:83.1%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:83.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
    }elseif ($tab == 2) {
      if (($tab == 2) && ($OTID == 1) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:7%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:7%; width:10%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
      if (($tab == 2) && ($OTID == 1) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:17.1%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:17.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
      if (($tab == 2) && ($OTID == 2) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:29%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:29%; width:10%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
      if (($tab == 2) && ($OTID == 2) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:39.1%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:39.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
      if (($tab == 2) && ($OTID == 3) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:51%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:51%; width:10%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
      if (($tab == 2) && ($OTID == 3) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:61.1%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:61.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
      if (($tab == 2) && ($OTID == 4) && ($sp == 1)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:73%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:73%; width:10%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
      if (($tab == 2) && ($OTID == 4) && ($sp == 2)) {
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:83.1%; width:9.9%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
        </div>
        <?php
      }
        ?>
        <div style="position:absolute; top:11.5%; height:2.9%; left:83.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
        </div>
        <?php
    }
  elseif ($tab == 3) {
    if (($tab == 3) && ($OTID == 1) && ($sp == 1)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:7%; width:10%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:7%; width:10%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 3) && ($OTID == 1) && ($sp == 2)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:17.1%; width:9.9%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:17.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 3) && ($OTID == 2) && ($sp == 1)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:29%; width:10%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:29%; width:10%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 3) && ($OTID == 2) && ($sp == 2)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:39.1%; width:9.9%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:39.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 3) && ($OTID == 3) && ($sp == 1)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:51%; width:10%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:51%; width:10%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 3) && ($OTID == 3) && ($sp == 2)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:61.1%; width:9.9%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:61.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 3) && ($OTID == 4) && ($sp == 1)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:73%; width:10%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:73%; width:10%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 3) && ($OTID == 4) && ($sp == 2)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:83.1%; width:9.9%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:83.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
  }elseif ($tab == 4) {
    if (($tab == 4) && ($OTID == 1) && ($sp == 1)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:7%; width:10%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:7%; width:10%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 4) && ($OTID == 1) && ($sp == 2)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:17.1%; width:9.9%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:17.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 4) && ($OTID == 2) && ($sp == 1)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:29%; width:10%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:29%; width:10%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 4) && ($OTID == 2) && ($sp == 2)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:39.1%; width:9.9%; text-align:center; z-index: 1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:39.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 4) && ($OTID == 3) && ($sp == 1)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:51%; width:10%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:51%; width:10%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 4) && ($OTID == 3) && ($sp == 2)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:61.1%; width:9.9%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:61.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 4) && ($OTID == 4) && ($sp == 1)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:73%; width:10%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:73%; width:10%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    if (($tab == 4) && ($OTID == 4) && ($sp == 2)) {
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:83.1%; width:9.9%; text-align:center; z-index:1;">
        <a style="color:blue; text-decoration:none; z-index:1;" href="<?php echo $rtnPage ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
      ?>
      <div style="position:absolute; top:11.5%; height:2.9%; left:83.1%; width:9.9%; text-align:center; border:thin solid black; z-index:1;">
      </div>
      <?php
    }
  }
}
