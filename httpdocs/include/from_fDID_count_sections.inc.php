<?php
include 'dbconnect.inc.php';
// echo "include/from_fDID_count_sections.inc.php";
include 'from_DID_get_min_section.inc.php';
$CID = $_SESSION['CID'];

$sql ="SELECT s.ID as cSCID
       FROM division d
        , section s
       WHERE d.ID = ?
       AND s.DID = d.ID
       AND s.ID NOT IN (?)
       ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b><br>FAIL-fdcs</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $fDID, $mSID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $cfSCID = $row['cSCID'];
}
