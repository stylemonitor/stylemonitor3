<?php
include 'dbconnect.inc.php';

if (isset($_POST['fACID'])) {
  $ACID = $_POST['fACID'];
}else {
  $ACID = $_SESSION['ACID'];
}

include 'from_ACID_get_min_division_ddt.inc.php';

$sql = "SELECT *
        FROM division
        WHERE ACID = ?
        AND ID not in (?)
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fcgac</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $ACID, $mDID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_num_rows($result);
  // $row = mysqli_fetch_assoc($result);
  if ($row > 0) {
    ?>
    <!-- <option value="">Select Division</option> -->
    <?php
    while ($row = mysqli_fetch_assoc($result)) {
    ?>
    <option value="<?php echo $row['ID'] ?>"><?php echo $row['name'] ?></option>
    <?php
    }
  }elseif ($row == 0) {
    ?>
    <div class="">
      NO DIVISIONS
    </div>
    <!-- <option value="">None Available to Select</option> -->
    <?php
  }

}

?>
