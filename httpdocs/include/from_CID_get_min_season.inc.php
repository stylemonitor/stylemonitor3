<?php
// echo "<b>include/from_CID_get_min_season.inc.php</b>";
$CID = $_SESSION['CID'];

$sql = "SELECT MIN(s.ID) as SNID
        FROM season s
        	, associate_companies ac
          , company c
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND s.ACID = ac.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgms</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $mSNID = $row['SNID'];
}
