<?php
session_start();
include 'dbconnect.inc.php';

if (!isset($_POST['ed_ord'])) {
  // echo "WRONG METHOD";
}else {
  if (isset($_GET['oi'])) {
    $OIID = $_GET['oi'];}

  // echo "<b>GET NEW DATA</b>";
  $udate     = $_POST['udate'];
  $uitemdate = $_POST['uitemdate'];
  $uoref     = $_POST['uoref'];
  $uiref     = $_POST['uiref'];
  $uqty      = $_POST['uqty'];


  // echo "<br>New order del date is (udate) : $udate";
  // echo "<br>New item del date is (uitemdate) : $uitemdate";
  // echo "<br>New Ref is (uoref) : $uoref";
  // echo "<br>New Item Ref is (uiref) : $uiref";
  // echo "<br>New QTY is (uqty) : $uqty";

  // echo "<br><b>ORIGINAL DATA</b>";
  $oref    = $_POST['oref'];
  $OID    = $_POST['OID'];
  $OIID    = $_POST['OIID'];
  $dueDate = $_POST['dueDate'];
  $OIIDd   = $_POST['OIIDd'];
  $OIDcor  = $_POST['OIDcor'];
  $OIIDcr  = $_POST['OIIDcr'];
  $oiQty   = $_POST['oiQty'];

  $UID       = $_SESSION['UID'];
  $udate     = strtotime($udate,0);
  $dueDate   = strtotime($dueDate,0);
  $OIIDd     = strtotime($OIIDd,0);
  $uitemdate = strtotime($uitemdate,0);
  $td = date('U');

  // echo "<br>Order Nos (oref) : $oref";
  // echo "<br>Order Item Nos (OIIDnos) : $OIID";
  // echo "<br>Order due date (dueDate) : $dueDate";
  // echo "<br>Item due date (OIIDd) : $OIIDd";
  // echo "<br>OUR ref (OIDcor) : $OIDcor";
  // echo "<br>Their Order Ref OIIDcr : $OIIDcr";
  // echo "<br>Order Item Qty $oiQty : $oiQty";

  // to update the log_orders ONLY
  // update our_order_ref
  // update del_Date
  if (empty($udate)) {$udate = $dueDate;}else {$udate = $udate;}
  if (empty($uoref)) {$uoref = $OIDcor;}else {$uoref = $uoref;}

  // to update Log_order_item
  // update their_order_ref
  // update item_del_date
  // update order_qty
  if (empty($uiref)) {$uiref = $OIIDcr;}else {$uiref = $uiref;}
  if (empty($uitemdate)) {$uitemdate = $OIIDd;}else {$uitemdate = $uitemdate;}
  if (empty($uqty)) {$uqty = $oiQty;}else {$uqty = $uqty;}

  // echo "<br><b>NEW DATA TO INPUT</b>";
  // echo "<br>Order due date (udate/dueDate) : $udate";
  // echo "<br>Item due date (uitemdate/OIIDd) : $uitemdate";
  // echo "<br>Our Ref (uoref/OIDcor) : $uoref";
  // echo "<br>Their Order Ref (uiref/OIIDcr) : $uiref";
  // echo "<br>Item Qty (uqty/oiQty) : $uqty";

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to

    // is the order internal or external
    $sql = "SELECT o.CPID as CPID
              , cp.req_CID as req_CID
              , cp.acc_CID as acc_CID
            FROM orders o
              , order_item oi
              , company_partnerships cp
            WHERE oi.ID = ?
            AND oi.OID = o.ID
            AND o.CPID = cp.ID
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-eoa7</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "s", $OIID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_assoc($result);
      $req_CID = $row['req_CID'];
      $acc_CID = $row['acc_CID'];
    }

    if ($req_CID == $acc_CID) {
      $req_UID = $acc_UID;
    }


    // to update the log_orders ONLY
    $sql = "INSERT INTO log_orders
              (OID, our_order_ref, their_order_ref, del_Date, UID, inputtime)
            VALUES
              (?,?,?,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-eoa</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssssss", $OID, $OIDcor, $OIIDcr, $udate, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    $sql = "UPDATE orders
            SET our_order_ref = ?
              , their_order_ref = ?
            WHERE ID = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-eoa2</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "sss", $uoref, $uiref, $OID);
      mysqli_stmt_execute($stmt);
    }

    $sql = "INSERT INTO log_orders_due_date
              (OID, del_Date, UID, inputtime)
            VALUES
              (?,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-eoa3</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $OID, $udate, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    $sql = "UPDATE orders_due_dates
            SET del_date = ?
            WHERE OID = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-eoa4</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $udate, $OID);
      mysqli_stmt_execute($stmt);
    }

    $sql = "INSERT INTO log_order_item_del_date
              (OIID, item_del_date, UID, inputtime)
            VALUES
              (?,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-eoa5</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $OIID, $uitemdate, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    $sql = "UPDATE order_item_del_date
            SET item_del_date = ?
            WHERE OIID = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-eoa6</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $uitemdate, $OIID);
      mysqli_stmt_execute($stmt);
    }

  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  header("Location:../home.php?H&aot");
  exit();
}
