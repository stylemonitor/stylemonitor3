<?php
echo "<br><b>inlcude/from_CID_get_min_section.inc.php</b>";
// from the associate companies ID
// get the minimum section ID
// when the company has NO NAMED sections
$sql = "SELECT MIN(s.ID) as SCID
        FROM section s
          , division d
          , associate_companies ac
          , company c
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND s.DID = d.ID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-from_CID_get_min_section</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $mSCID = $row['SCID'];
}
