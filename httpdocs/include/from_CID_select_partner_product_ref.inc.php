<?php
include 'dbconnect.inc.php';
// echo "include/from_CID_select_partner_product_ref.inc.php";

if (isset($_GET['p'])) {
  $CPID = $_GET['p'];
  echo "Partnership ID is $CPID";
}

// from CPID get the partners CID as pCID
// use an include file!

if (isset($_GET['id'])) { $CID = $_GET['id'];}

// $acID = $_SESSION['acID'];
// $CID = $_SESSION['CID'];
// $CIDn = $_SESSION['CIDn'];

// echo '<br>Company name is <b>'.$CIDn.'</b> and their ID is <b>'.$CID.'</b>';

$sql = "SELECT COUNT(pr.ID) as cPRID
        FROM company c
          , associate_companies ac
          , division d
          , prod_ref pr
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.acID = ac.ID
        AND pr.DID = d.ID
        ORDER BY 2;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $nosC = $row['cPRID'];
}

$sql = "SELECT pr.ID
          , pr.prod_ref
        FROM company c
          , associate_companies ac
          , division d
          , prod_ref pr
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.acID = ac.ID
        AND pr.DID = d.ID
        ORDER BY 2;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $prID = $row['0'];
    $prRef = $row['1'];

    echo '<option value="'.$prID.'">'.$prRef.'</option>';
  }
}
  ?>
