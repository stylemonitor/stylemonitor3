<?php
include 'dbconnect.inc.php';
// echo "<br><b>include/report_ORDER_STATUS_due_date.inc.php</b>";

$CID = $_SESSION['CID'];

include 'set_urlPage.inc.php';
include 'from_UID_get_last_logout.inc.php'; // $LRIDout
include 'ot_sp_selection_movement.inc.php';

$td = date('U');

if (isset($_GET['H'])) {
  $hbc = '#b7d8ff';
  $LtabTitle = 'Order Status';
}elseif (isset($_GET['L'])) {
  $hbc = '#ffffd1';
  $LtabTitle = 'Factory Loading';
}

// sets the order type
if (isset($_GET['ot'])) {
  $ot = $_GET['ot'];
}

// sets the sample/production type
if (isset($_GET['sp'])) {
  $sp - $_GET['sp'];
}

if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  if (($tab == 0) || ($tab == 9)) {
    $pddh = 'Order Summary : <b>All Dates</b>';
    $pddhbc = '#b7d8ff';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td - 31536000;
    $ed = $td + 31536000;
  }elseif ($tab == 1) {
    $pddh = 'Order Item Summary : OVERDUE';
    $pddhbc = $odCol;
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td - 31536000;
    $ed = $td;
  }elseif ($tab == 2) {
    $pddh = 'Production Status Orders due : THIS WEEK';
    $pddhbc = $twCol;
    $dr = 'oidd.item_del_date';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td;
    $ed = $td + 604800;
  }elseif ($tab == 3) {
    $pddh = 'Production Status Orders due : NEXT WEEK';
    $pddhbc = $nwCol;
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td + 604800;
    $ed = $td + 1209600;
  }elseif ($tab == 4) {
    $pddh = 'Production Status Orders due : AFTER TWO WEEKS';
    $pddhbc = $ltCol;
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td + 1209600;
    $ed = $td + 31536000;
  }elseif ($tab == 9) {
    $pddh = 'Order Summary All Dates';
    $pddhbc = '#ffffd1';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td - 31536000;
    $ed = $td + 31536000;
  }
}
// include 'page_description_date_header.inc.php';
include 'page_selection_header.inc.php';
include 'order_QUERY_count.inc.php';

$rpp = 20;

// Check the URL to see what page we are on
if (isset($_GET['pa'])) {
  $pa = $_GET['pa'];
}else{
  $pa = 0;
}

// Set the start point for each page
if ($pa > 1) {
  $start = ($pa * $rpp) - $rpp;
}else {
  $start = 0;
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
// $cTOTAL is the count of the total items able to be viewed - cannot be changed
$PTV = ($cTOTAL / $rpp);
// $PTV = ($cTOTAL / $rpp) + 1;

// The number of pages to be viewed
$PTV = ceil($PTV) - 1;
// $PTV = ceil($PTV);

$iniSelect = "TOTALS.cliCID";

$edate = date('d-M-Y', $td);

// echo "<br>Company ID : $CID";
// echo "<br>Order status : $OC";
// echo "<br>Today : $td";
// echo "<br>Start day : $sd";
// echo "<br>End day : $ed";
// echo "<br>Count_orders - due date source : $dr";

?>

<table class="trs" style="position:absolute; top:15.5%;">
  <tr>
    <?php
    include 'Table_headings_order_QUERY.inc.php';
    ?>
  </tr>


<!-- TO BE ADDED to the bottom of each report and adjusted accordingly -->
<div style="position:absolute; top:85%; right:5%; font-size:150%;">
  <?php
  if ($PTV == 1) {
    // code...
  }else {
    for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a style='color:blue; text-decoration:none;' href='$urlPage&tab=$tab&ot=$ot&sp=$sp&pa=$x'>  $x  </a>";
    }
  }
  ?>
</div>

<!-- <form action="home.php?H&rt1&tab=<?php echo $tab ?>&ot=<?php echo $OTID ?>&sp=<?php echo $sp ?>&tabh" method="post">
  <button class="help" type="submit" name="help">HELP</button>
</form> -->
