<?php
session_start();
include 'dbconnect.inc.php';
// echo "<br>include/form_new_style_act.inc.php";

// WITH ATTEMPTED TRANSACTION

include 'from_CID_get_min_product_type.inc.php';
include 'from_CID_get_min_division_NO_DIV.inc.php';

$UID = $_SESSION['UID'];
$ACID = $_SESSION['ACID'];
$td = date('U');

if (!isset($_POST['reg_STY']) && !isset($_POST['cancel'])) {
  // echo "WRONG";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['cancel'])) {
  header("Location:../styles.php?S&snn");
  exit();
}elseif (isset($_POST['reg_STY'])) {

  // echo "<br>Register the new style";
  // Get the information from the form
  // $UID = $_SESSION['UID'];
  $DID = $_POST['DID'];
  $prod_ref = $_POST['ref'];
  $prod_desc = $_POST['dsc'];
  // $etm = $_POST['etm'];
  // $fst = $_POST['fst'];
  $PTID = $_POST['PTID'];
  $SNID = $_POST['SNID'];
  $CLID = $_POST['CLID'];

  if (empty($etm)) {$etm = 0;}else {$etm = $etm;}

  // echo "<br>User UID : $UID";
  // echo "<br>Associate Company ACID : $ACID";
  // echo "<br>Division DID : $DID";
  // echo "<br>Product ref prod_ref : $prod_ref";
  // echo "<br>Product description prod_desc : $prod_desc";
  // echo "<br>Time to make etm : $etm";
  // echo "<br>Product type PTID : $PTID";
  // echo "<br>Season SNID : $SNID";
  // echo "<br>Collection CLID : $CLID";
  // echo "<br>Date td : $td";

  include 'check_prod_ref_new_valid.inc.php';
  include 'check_PRID_PTID_combo_free.inc.php';

  /* Start transaction */
  // mysqli_begin_transaction($mysqli);
  // try {

    // // Check if the short code prod_ref is already in use
    // $sql = "SELECT ID as ID
    //         FROM prod_ref
    //         WHERE ACID = ?
    //         AND (prod_ref = ? OR prod_desc = ?)
    // ;";
    // $stmt = mysqli_stmt_init($con);
    // if (!mysqli_stmt_prepare($stmt, $sql)) {
    //   echo "FAIL-fnsa";
    // }else {
    //   mysqli_stmt_bind_param($stmt, "sss", $ACID, $prod_ref, $prod_desc);
    //   mysqli_stmt_execute($stmt);
    //   $result = mysqli_stmt_get_result($stmt);
    //   $row = mysqli_fetch_array($result);
    //   $PRID = $row['ID'];
    // }
    //
    // if (empty($PTID)) {$PTID = $mPTID;}else {$PTID = $PTID;}


    if (!empty($PRID)) {
      // echo "<br>Product reference already in use";
      // header("Location:../styles.php?S&snn&2&d=$prod_desc");
      // exit();
    }else {
      // echo "<br>Add to the list";
      if (empty($etm)) {
        $etm = 1;}else { $etm = $etm;
      }
      if (!is_numeric($etm)) {
        // echo "<br>Needs to be a positive number";
        header("Location:../styles.php?S&snn&1&r=$prod_ref&d=$prod_desc");
        exit();
      }elseif ($etm < 0) {
        // echo "<br>Needs to be a positive number!!!";
        header("Location:../styles.php?S&snn&1&r=$ref&d=$desc");
        exit();
      }
      // include 'include/SAVE_prod_ref.inc.php';
      $sql_insert_new_prod_ref = "INSERT INTO prod_ref
                                    (ACID, PTID, UID, prod_ref, prod_desc, etm, inputtime)
                                  VALUES
                                    (?,?,?,?,?,?,?)
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql_insert_new_prod_ref)) {
        echo "FAIL-fnsa1";
      }else {
        mysqli_stmt_bind_param($stmt, "sssssss", $ACID, $PTID, $UID, $prod_ref, $prod_desc, $etm, $td);
        mysqli_stmt_execute($stmt);
      }

      // include 'include/get_PRID_from_UID_time.inc.php';
      $sql_new_PRID = "SELECT ID as PRID
                       FROM prod_ref
                       WHERE UID = ?
                       AND inputtime = ?
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql_new_PRID)){
        echo 'FAIL-fnsa2';
      }else{
        mysqli_stmt_bind_param($stmt, "ss", $UID, $td);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        $row = mysqli_fetch_array($result);
        $PRID = $row['PRID'];
      }

      // include 'include/SAVE_prod_ref_to_division.inc.php';
      $sql_ins_pref_to_div = "INSERT INTO prod_ref_to_div
                                (PRID, DID, UID, inputtime)
                              VALUES
                                (?,?,?,?)
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql_ins_pref_to_div)){
        echo 'FAIL-fnsa3';
      }else{
        mysqli_stmt_bind_param($stmt, "ssss", $PRID, $NDmDID, $UID, $td);
        mysqli_stmt_execute($stmt);
      }

      // include 'include/SAVE_prod_ref_to_collection.inc.php';
      $sql_ins_pref_to_coll = "INSERT INTO prod_ref_to_collection
                                (PRID, CLID, UID, inputtime)
                               VALUES
                                (?,?,?,?)
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql_ins_pref_to_coll)){
        echo 'FAIL-fnsa4';
      }else{
        mysqli_stmt_bind_param($stmt, "ssss", $PRID, $CLID, $UID, $td);
        mysqli_stmt_execute($stmt);
      }

      // include 'include/SAVE_prod_ref_to_season.inc.php';
      $sql_ins_pref_to_seas = "INSERT INTO prod_ref_to_season
                                (PRID, SNID, UID, inputtime)
                              VALUES
                                (?,?,?,?)
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql_ins_pref_to_seas)){
        echo 'FAIL-fnsa5';
      }else{
        mysqli_stmt_bind_param($stmt, "ssss", $PRID, $SNID, $UID, $td);
        mysqli_stmt_execute($stmt);
      }
    }
  // }  catch (mysqli_sql_exception $exception) {
  //   mysqli_rollback($mysqli);
  //
  //   throw $exception;
  // }
  header("Location:../styles.php?S&snn");
  exit();
}
