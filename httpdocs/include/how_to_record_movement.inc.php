<?php
include 'dbconnect.inc.php';
$sm = '<b>S</b>tyle<b>M</b>onitor';

?>
<div class="cpsty" style="top:0%; height:3%; left:0%; width:100%; background-color:#e08df4;">
  <p>How to ... ORDERS - RECORD MOVEMENT</p>
</div>
<div class="" style="top:3%; height:97%; left:0%; width:100%; background-color:#f4f4f4;">
  <br>
  <p>Like most people, we hate work.  So we only have two little bits of information to enter here.</p>
  <br>
  <p>One you need to enter, the <b>quantity</b> and</p>
  <br>
  <p>Two, you select from the drop-down list</p>
  <br>
  <p>the list lets the system know from where the item is being moved and to which section, usually the next!</p>
  <br>
  <p>It also works for sending items back</p>
  <br>
  <p>Keep entering until all movements at the time have been completed, then click on CLOSE</p>
  <br>
  <p>You will go back to the review screen for that order</p>
  <br>
  <p>To see what was moved, when and by who click on the <a style="text-decoration:none:"href="?W&hml">Movement Log</a> button</p>
  <br>
  <p>to see more about who is making the order click on the <a style="text-decoration:none:"href="?W&hmu">Make Unit Details</a> button</p>
</div>
