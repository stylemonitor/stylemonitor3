<?php
include 'dbconnect.inc.php';
// echo '<b>include/form_set_pwd_questions_act.inc.php</b>';

if (!isset($_POST['ans']) && (!isset($_POST['cancel']))) {
  // echo "WRONG";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['cancel'])) {
  // echo "Cancel";
  header("Location:../company.php?C&usr&u=$UID");
  exit();
}elseif (isset($_POST['ans'])){
  // echo "Lets enter the data AFTER checking it";

  // $UID = $_POST['UID'];
  $FPQ1 = $_POST['FPQ1'];
  $FPQ2 = $_POST['FPQ2'];
  $FPQ3 = $_POST['FPQ3'];

  $FPQA1 = $_POST['FPQA1'];
  $FPQA2 = $_POST['FPQA2'];
  $FPQA3 = $_POST['FPQA3'];

  $td = date('U');

  // echo "<br>Forgotten Password questions are : $FPQ1";
  // echo "<br>Forgotten Password questions are : $FPQ2";
  // echo "<br>Forgotten Password questions are : $FPQ3";

  // echo "<br>Forgotten Password questions are : $FPQ1 ; $FPQA1";
  // echo "<br>Forgotten Password questions are : $FPQ2 ; $FPQA2";
  // echo "<br>Forgotten Password questions are : $FPQ3 ; $FPQA3";

  if (empty($FPQ1) || empty($FPQ2) || empty($FPQ3)) {
    // echo "<br>You need an answer for each question";
    $urlPage = $_POST['urlPage'];
    header("Location:../$urlPage&spq&ans&u=$UID&qqq");
    exit();
  }

  if (empty($FPQA1) || empty($FPQA2) || empty($FPQA3)) {
    // echo "<br>You need an answer for each question";
    $urlPage = $_POST['urlPage'];
    header("Location:../$urlPage&spq&ans&u=$UID&aaa");
    exit();
  }

  if (($FPQ1 == $FPQ2) || ($FPQ1 == $FPQ3) || ($FPQ2 == $FPQ3)) {
    $urlPage = $_POST['urlPage'];
    header("Location:../$urlPage&spq&dif&u=$UID");
    exit();
    // include 'form_set_pwd_dif_questions_act.inc.php';
  }else {
    // echo "<br>Continue!!!";
    // echo "<br>Enter the data";


    // Start transaction
    // mysqli_begin_transaction($mysqli);
    // try {
      // to
      // First question
      $sql = "INSERT INTO forgotten_password_user_answers
                (UID, FPQID, ANS, inputtime)
              VALUES
                (?,?,?,?)
              ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo "FAIL-fspqa";
      }else {
        mysqli_stmt_bind_param($stmt, "ssss", $UID, $FPQ1, $FPQA1, $td);
        mysqli_stmt_execute($stmt);
      }

      // Second question
      $sql = "INSERT INTO forgotten_password_user_answers
                (UID, FPQID, ANS, inputtime)
              VALUES
                (?,?,?,?)
              ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo "FAIL-fspqa";
      }else {
        mysqli_stmt_bind_param($stmt, "ssss", $UID, $FPQ2, $FPQA2, $td);
        mysqli_stmt_execute($stmt);
      }

      // Third question
      $sql = "INSERT INTO forgotten_password_user_answers
                (UID, FPQID, ANS, inputtime)
              VALUES
                (?,?,?,?)
              ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo "FAIL-fspqa";
      }else {
        mysqli_stmt_bind_param($stmt, "ssss", $UID, $FPQ3, $FPQA3, $td);
        mysqli_stmt_execute($stmt);
      }
    // } catch (mysqli_sql_exception $exception) {
    //   mysqli_rollback($mysqli);
    //
    //   throw $exception;
    // }
  }
  header("Location:../company.php?C&usr&u=$UID");
  exit();
}
