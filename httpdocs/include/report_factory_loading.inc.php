<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_factory_loading.inc.php";

$CID = $_SESSION['CID'];
$td = date('U');

include 'from_CID_count_order_overdue.inc.php';
include 'from_CID_count_order_thisweek.inc.php';
include 'from_CID_count_order_nextweek.inc.php';
include 'from_CID_count_order_later.inc.php';
include 'from_CID_count_order_new.inc.php'; // $cOIDnew
include 'from_CID_count_orders_in_work.inc.php'; // $cOIDiw
include 'from_UID_get_last_logout.inc.php'; // $LRIDout
include 'from_CID_count_section_qtys.inc.php';
include 'from_CID_count_section_totals.inc.php';

// Check the URL to see what page we are on
if (isset($_GET['p'])) { $p = $_GET['p']; }else{ $p = 0; }

// Set the rows per page
$r = 20;
// Check which page we are on
if ($p > 1) { $start = ($p * $r) - $r; }else { $start = 0; }

// sets the order type
// all open orders overdue
// if ($cOIDod == 0) {
// }

if (isset($_GET['mp1'])) {
  $s1 = 3;
  $s2 = 4;
  $s3 = 13;
  $s4 = 14;
  $s5 = 23;
  $s6 = 29;
  $secqty = $stotal1;
  $secord = $cORst1;
  $secbc = '#63a363';
}if (isset($_GET['mp2'])) {
  $s1 = 4;
  $s2 = 5;
  $s3 = 14;
  $s4 = 15;
  $s5 = 24;
  $s6 = 30;
  $secqty = $stotal2;
  $secord = $cORst2s;
  $secbc = '#63a3a3';
}if (isset($_GET['mp3'])) {
  $s1 = 5;
  $s2 = 6;
  $s3 = 15;
  $s4 = 16;
  $s5 = 25;
  $s6 = 31;
  $secqty = $stotal3;
  $secord = $cORst3;
  $secbc = '#6377a3';
}if (isset($_GET['mp4'])) {
  $s1 = 6;
  $s2 = 7;
  $s3 = 16;
  $s4 = 17;
  $s5 = 26;
  $s6 = 32;
  $secqty = $stotal4;
  $secord = $cORst4;
  $secbc = '#8f63a3';
}if (isset($_GET['mp5'])) {
  $s1 = 7;
  $s2 = 8;
  $s3 = 17;
  $s4 = 18;
  $s5 = 27;
  $s6 = 33;
  $secqty = $stotal5;
  $secord = $cORst5;
  $secbc = '#a3637c';
}if (isset($_GET['owh'])) {
  $s1 = 8;
  $s2 = 18;
  $s3 = 19;
  $s4 = 20;
  $s5 = 28;
  $s6 = 34;
  $secqty = $WAREHOUSE;
  $secord = $cORwhs;
  $secbc = '#ef80bb';
}

// Count how many orders in a section
// WHERE the qty of the order in the section <> 0

$sql = "SELECT COUNT(o.ID) as OID
        FROM orders o
        , order_item_movement_reason oimr
        , order_placed_move opm
        WHERE o.fCID = ?
        AND oimr.ID IN (1)
        AND opm.OIMRID = oimr.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rfl1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOID = $row['OID'];
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cOID / $r);
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;
// echo '<br>Start record is : '.$start;
// echo "<br>**** count_orders count : $cOID ****";

  ?>
    <div class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $secbc; ?>"><?php echo "There are $secqty items over $secord orders in the Section"?></div>
  <table class="trs" style="position:relative; top:0%;">

  <tr>
    <th style="width:2%;">O/T</th>
    <th style="width:7%; text-align:center;">O/r</th>
    <th style="width:25%; text-align:left; text-indent:5%;" style="width:7%;">Client</th>
    <th style="width:35%; text-align:left; text-indent:5%;">Reference</th>
    <th style="width:9%;">Section Qty</th>
    <th style="width:8%; padding-right:1%;">Order Qty</th>
    <th style="width:13%;">Due date</th>
    <th style="width:1%;"></th>
  </tr>
  <?php
  include 'report_factory_loading_QUERY.inc.php';
  ?>
  <div style="position:absolute; bottom:5%; left:10%;">

    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a href='?L&$sp&p=$x'>  $x  </a>";
    }
    ?>
  </div>
