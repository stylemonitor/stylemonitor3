<?php
include 'include/dbconnect.inc.php';
// echo "<br><b>from_CID_count_company_division.inc.php</b>";

$sql = "SELECT COUNT(d.ID) as cDID
        FROM division d
          , associate_companies ac
          , company c
        WHERE ac.ID = (SELECT min(ac.ID) as mACID
               			FROM associate_companies ac
            		   	, company c
	        	       	WHERE ac.CID = ?)
        AND d.ACID = ac.ID
        AND d.name NOT IN ('Select Division')
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcccd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $_SESSION['CID']);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cDID = $row['cDID'];
}
// echo "<br>Count Divisions (cDID) : $cDID";
