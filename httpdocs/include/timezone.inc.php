<?php
include 'dbconnect.inc.php';
// GET the timezone correction value from the db

$sql = "SELECT tz.Ucorval as cV
        FROM company_time_zone ctz
          , timezones tz
          , company c
        WHERE c.ID = ?
        AND c.ID = ctz.CID
        AND ctz.TID = tz.ID
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-tnnnnn</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
    $cV = $row['cV'];
}

$td = date('U');
// corrected time = ct
$ct = ($td + $cV);
$ct = date('d-M-Y H:i', $ct);
// echo "$cV - $ct";
