<?php
include 'dbconnect.inc.php';
// echo "include/from_CPID_get_client_data.inc.php";

$sql = "SELECT ID as cli_ACID
          , name as cli_ACIDn
        FROM company
        WHERE ID = (SELECT req_CID
                    FROM company_partnerships cp
                    WHERE ID = ?)
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fcgcd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CPID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $cli_ACID = $row['cli_ACID'];
    $cli_ACIDn = $row['cli_ACIDn'];
  }
}
