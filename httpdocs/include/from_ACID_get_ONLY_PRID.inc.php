<?php
include 'dbconnect.inc.php';
// echo "include/from_ACID_get_ONLY_PRID.inc.php";

if (isset($_GET['id'])) { $ACID = $_GET['id'];}

// echo "<br>ACID = $ACID";
// Check if the short code prod_ref is not already in use
$sql = "SELECT pt.ID as PTID
		      , pt.scode as PTIDs
          , pt.name as PTIDn
        FROM product_type pt
        	, associate_company_product_types aspt
        WHERE aspt.ACID = ?
        AND aspt.PTID = pt.ID
        -- AND pt.scode NOT IN ('0')
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo "FAIL-fACIDgop";
}else {
  mysqli_stmt_bind_param($stmt, "s", $ACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $PTID = $row['PTID'];
  $PTIDs = $row['PTIDs'];
  $PTIDref = $row['PTIDn'];
}
// echo "<br>Product short code (PTIDs) $PTIDs";
// echo "<br>Product description (PTIDn) $PTIDn";
