<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "include/order_report_STYLE_QUERY.inc.php";

include 'set_urlPage.inc.php';

$CID = $_SESSION['CID'];// echo "PRID brought forward : $PRID";
$ACID = $_SESSION['ACID'];// echo "PRID brought forward : $PRID";

if ($_GET['s']) {
  $PRID = $_GET['s'];
}

$sdate = date('d-M-Y', $sd);
$edate = date('d-M-Y', $ed);
$tdate = date('d-M-Y', $td);
// echo "<br><br><br><br><br><br><br><br><br><br><br>SHOW RESULTS BETWEEN";
// echo "<br>$sdate and $edate";
// echo "<br>Today = $tdate";

$start = 0;
$rpp = 10;

include 'sql/order_report_style_query.sql.php';

// $sql = "SELECT DISTINCT
//   -- SQL is order_report_STYLE_QUERY.sql
//        o.fCID AS cliCID
//        , c.name AS cliCIDn
//        , o.fACID AS cli_ACID
//        , ac.name AS cli_ACIDn
//        , o.tCID AS supCID
//        , cS.name AS supCIDn
//        , o.tACID AS sup_ACID
//        , acS.name AS sup_ACIDn
//        , Mvemnt.ORDER_ID AS OID
//        , LPAD(o.orNos,6,'0') AS ordNos
//        , o.our_order_ref AS ourRef
//        , o.their_order_ref AS theirRef
//        , pr.ID AS PRID
//        , pr.prod_ref AS PRIDs
//        , pt.ID AS PTID
//        , Mvemnt.OIID_COUNT AS OIID_COUNT
//        , MIN(oidd.item_del_date) AS earliest_date
//        , CASE WHEN o.tCID = o.fCID
//               THEN o.OTID
//               ELSE CASE WHEN o.tCID = ? THEN 3
//               ELSE 4
//               END
//          END AS OTID
//        , Mvemnt.ordQty AS OIQIDq
//        , SUM(Mvemnt.ORDER_INCR - Mvemnt.ORDER_DECR) AS QTY_CHANGE
//        , Mvemnt.ordQty + SUM(Mvemnt.ORDER_INCR - Mvemnt.ORDER_DECR) AS NEW_OIQIDq
//   FROM (
//       SELECT -- DISTINCT oimr.ID AS OIMR_ID
//            o.ID AS ORDER_ID
//            , COUNT(DISTINCT oi.ID) AS OIID_COUNT
//            , SUM(oiq.order_qty) AS ordQty
//            , IF((CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END)) AS ORDER_INCR
//            , IF((CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END)) AS ORDER_DECR
//       FROM order_placed_move opm
//          INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
//          INNER JOIN order_placed op ON opm.OPID = op.ID
//          INNER JOIN order_item oi ON op.OIID = oi.ID
//          INNER JOIN orders o ON oi.OID = o.ID
//          INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
//       WHERE oi.itComp = 0
//       GROUP BY o.ID
//      ) Mvemnt
//           INNER JOIN orders o ON Mvemnt.ORDER_ID = o.ID
//           INNER JOIN order_item oi ON o.ID = oi.OID
//           INNER JOIN prod_ref pr ON oi.PRID = pr.ID
//           INNER JOIN product_type pt ON pr.PTID = pt.ID
//           INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
//           INNER JOIN orders_due_dates odd ON odd.OID = o.ID
//           INNER JOIN order_type ot ON o.OTID = ot.ID
//           INNER JOIN company c ON o.fCID = c.ID
//           INNER JOIN company cS ON o.tCID = cS.ID
//           INNER JOIN associate_companies ac ON o.fACID = ac.ID
//           INNER JOIN associate_companies acS ON o.fACID = acS.ID
//           INNER JOIN division d ON o.fDID = d.ID
//           INNER JOIN division dS ON o.fDID = dS.ID
// WHERE (o.fCID = ? OR o.tCID = ?)
//   AND pr.ID = ?
// GROUP BY o.ID
// ORDER BY earliest_date, ordNos, NEW_OIQIDq ASC
// LIMIT ?,?
// ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-opSQ</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssssss", $CID, $CID, $CID, $PRID, $start, $rpp);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $cliCID = $row['cliCID'];
    $cliCIDn = $row['cliCIDn'];
    $cli_ACID = $row['cli_ACID'];
    $cli_ACIDn = $row['cli_ACIDn'];
    $supCID = $row['supCID'];
    $supCIDn = $row['supCIDn'];
    $sup_ACID = $row['sup_ACID'];
    $sup_ACIDn = $row['sup_ACIDn'];
    $OID = $row['OID'];
    $OTID = $row['OTID'];
    $OIDnos = $row['ordNos'];
    $cOIID = $row['OIID_COUNT'];
    $OIDDIDd = $row['earliest_date'];
    $ourRef = $row['ourRef'];
    $theirRef = $row['theirRef'];
    $OIQIDq = $row['NEW_OIQIDq'];

    if ($OTID == 1) {
      $bc = $salAssCol;
    }elseif ($OTID == 2) {
      $bc = $purAssCol;
    }elseif ($OTID == 3) {
      $bc = $salPrtCol;
    }elseif ($OTID == 4) {
      $bc = $purPrtCol;
    }

    $idate = date('d-M-Y', $OIDDIDd);

    ?>
    <tr style="font-family: monospace;">
      <td class="select" style="text-align:right; padding-right:0.5%;"><a class="hreflink" href="home.php?H&rt2&o=<?php echo $OID;?>"><?php echo $OIDnos ?></a></td>
      <td style="background-color: <?php echo $bc ?>; border-right:1px solid black;"><?php echo $cOIID ?></td>
      <?php
      if (strlen($cli_ACIDn)>40) {
        $cli_ACIDn = substr($cli_ACIDn,0,40);
        ?>
        <td class="select" style="text-align:left; text-indent:1%; border-right: thin solid grey;"><a class="hreflink" href="associates.php?A&asf&id=<?php echo $cli_ACID ?>"> <?php echo $cli_ACIDn ?> ...</a></td>
        <?php
      }else {?>
        <td class="select" style="text-align:left; text-indent:1%; border-right: thin solid grey;"><a class="hreflink" href="associates.php?A&asf&id=<?php echo $cli_ACID ?>"><?php echo $cli_ACIDn ?></a></td>
        <?php
      } ?>
      <?php
      if (strlen($ourRef)>50) {
        $ourRef = substr($ourRef,0,50);
        ?>
        <td style="text-align:left; text-indent:1%; border-right: thin solid grey;"><?php echo $ourRef ?> ...</td>
        <?php
      }else {?>
        <td style="text-align:left; text-indent:1%; border-right: thin solid grey;"><?php echo $ourRef ?></td>
        <?php
      } ?>
      <td style="text-align:left; text-indent:1%; border-right: thin solid grey;"><?php echo $theirRef ?></td>
      <!-- <td style="text-align:left; text-indent:1%; border-right: thin solid grey;"><?php echo $OIIDor ?></td> -->
      <td style="text-align:right; padding-right:0.5%; font-size: 120%; border-right:thin solid black;"><?php echo $OIQIDq;?></td>
      <td style="text-align:right; padding-right:0.5%;"><?php echo $idate ?></td>
    </tr>
    <?php
    }
  }
  ?>
  <tr>
    <td colspan="7" style="border:1px solid black"></td>
  </tr>
  </table>

  <?php
  if (isset($_GET['sum'])) {
    ?>
    <div style="position:absolute; bottom:2%; left:0%; width:100%; z-index:1;">
      The 'top' 5 are listed above - to see all items in a given time frame click on the tab at the top OR the name of the section
    </div>
    <?php
  }
  ?>
