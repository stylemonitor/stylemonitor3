<?php
include 'dbconnect.inc.php';
// echo "<br><b>create_company_SMIC.inc.php</b>";
// echo "<br>Country ID is $CYID";

// GET the 3 digit UN country code
  $sql = "SELECT UN_code3 as CCO
          FROM country
          WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-ccs</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $CYID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    if($row = mysqli_fetch_array($result));
    $CCO = $row['CCO'];
    // echo '<br>The 3 letter country code is : <b>'.$CCO.'</b>';
  }

  // echo '<br><br><b>Create and add the SMIC suffix code (266)</b>';

  // GET a count of the company's listed
  $sql = "SELECT count(inputtime)-1 as total
          FROM company
  ;";
  $result = mysqli_query($con, $sql);
  $row = mysqli_fetch_assoc($result);
  // echo '<br><b>COUNT : </b>'.$row['total'];
  // $RT = $row['total'];
  $alphabet = range('A','Z');

  $sql = "SELECT COUNT(ID) as cCID
          FROM company
          WHERE CYID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-ccs2</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $CYID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_assoc($result);
    $cCID = $row['cCID'];
  }

  // Then one of the following
  // echo '<br>Count of companies registered today : '.$cCID;

  // The number of 'rows per page' ie 26 letters of the alphabet!
  $cpp = 26;

  // Get the number of times 26 goes into the count of companies registered today from a particular country
  $div = intval($cCID/$cpp);

  // Get the number left over
  $res = fmod($cCID, $cpp);

  while ($div >= 26) {
    $div = intval($cCID/$cpp);
    $res = fmod($cCID, $cpp);
  }

  // Create the SMIC code for the company
  $abvCOM = substr($CIDn,0,2);
  $SMICd	= gmdate('myd');
  $SMIC = strtoupper($abvCOM.$CCO.'-'.$SMICd.'-'.$alphabet[$div].$alphabet[$res]);
  // echo '<br> The SMIC is <b>'.$SMIC.'</b>';

  // UPDATE the company table with the SMIC for that company
  $sql = "UPDATE company
          SET SMIC = ?
          WHERE UID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-ccs3</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $SMIC, $UID);
    mysqli_stmt_execute($stmt);
  }

  // echo "<br>SMIC (SMIC) = $SMIC";
  // echo "<br>UID (UID)= $UID";
