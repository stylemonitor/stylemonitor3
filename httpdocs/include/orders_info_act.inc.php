<?php
session_start();
include 'dbconnect.inc.php';
// echo "orders_info_act.inc.php";

include 'set_urlPage.inc.php';

$CID = $_SESSION['CID'];
$UID = $_SESSION['UID'];

// check that the form was entered from P_re.inc.php
if (!isset($_POST['sales']) && !isset($_POST['purchases']) && !isset($_POST['style']) && !isset($_POST['noshow'])) {
  // echo "Resubmit the request";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['sales'])) {
  // echo '<br>The CANCEL button was used';
  header("Location:../$urlPage&orn&sal=$CID");
  exit();
}elseif (isset($_POST['purchases'])) {
  // echo '<br>The CANCEL button was used';
  header("Location:../$urlPage&orn&pur=$CID");
  exit();
}elseif (isset($_POST['style'])) {
  // echo '<br>The CANCEL button was used';
  header("Location:../styles.php?S&snn");
  exit();
}elseif (isset($_POST['noshow'])) {
  // echo '<br>The NOSHOW button was used';
  // NEW method
  include 'from_UID_get_user_details.inc.php';
  $UIDis = $UIDis + 1;
  $sql = "UPDATE users
          SET iscreen = ?
          WHERE ID = ?
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo "FAIL-oia";
  }else {
    mysqli_stmt_bind_param($stmt, "ss", $UIDis, $UID);
    mysqli_stmt_execute($stmt);
  }

  header("Location:../$urlPage&orn&sal&PPP$UIDis");
  exit();
  // include 'check_no_show_pages.inc.php';
  // header("Location:../styles.php?S&snn");
  // exit();
}
