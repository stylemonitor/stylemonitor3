<?php
include 'dbconnect.inc.php';
// echo "include/from_CID_count_users.inc.php";
// $ACID = $_SESSION['ACID'];

// Lists the number of users
$sql = "SELECT DISTINCT u.ID as cUSERS
        FROM company c
          , associate_companies ac
          , users u
          , division d
          , company_division_user cdu
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND cdu.DID = d.ID
        AND cdu.UID = u.ID
        AND cdu.current = 1
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
     echo '<b>FAIL-facu</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $cUSERS = mysqli_num_rows($result);;
}

// Count Pending users
$sql = "SELECT DISTINCT u.ID as cUSERSp
        FROM company c
          , associate_companies ac
          , users u
          , division d
          , company_division_user cdu
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND cdu.DID = d.ID
        AND cdu.UID = u.ID
        AND cdu.current = 1
        AND u.active = 1
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
     echo '<b>FAIL-facu2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $cUSERSp = mysqli_num_rows($result);
}
// Count active users
$sql = "SELECT DISTINCT u.ID as cUSERS
        FROM company c
          , associate_companies ac
          , users u
          , division d
          , company_division_user cdu
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND cdu.DID = d.ID
        AND cdu.UID = u.ID
        AND cdu.current = 1
        AND u.active = 2
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
     echo '<b>FAIL-facu3</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $cUSERSa = mysqli_num_rows($result);
}

// Count Suspended users
$sql = "SELECT DISTINCT u.ID as cUSERS
        FROM company c
          , associate_companies ac
          , users u
          , division d
          , company_division_user cdu
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND cdu.DID = d.ID
        AND cdu.UID = u.ID
        AND cdu.current = 1
        AND u.active = 3
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
     echo '<b>FAIL-facu4</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $cUSERSs = mysqli_num_rows($result);
}
