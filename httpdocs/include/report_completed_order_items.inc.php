<?php
session_start();
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
include 'set_urlPage.inc.php';
// include 'page_selection_complete.inc.php';
// echo "include/report_completed_order_items.inc.php";

$CID = $_SESSION['CID'];
$td = date('U');

// include 'from_CID_count_section_totals.inc.php';
// include 'from_CID_count_order_overdue.inc.php';
// include 'from_CID_count_order_thisweek.inc.php';
// include 'from_CID_count_order_nextweek.inc.php';
// include 'from_CID_count_order_later.inc.php';
// include 'from_CID_count_order_new.inc.php'; // $cOIDnew
// include 'from_CID_count_orders_in_work.inc.php'; // $cOIDiw
// include 'from_UID_get_last_logout.inc.php'; // $LRIDout

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'Completed Order Items';
$pddhbc = '#fffddd';
include 'page_description_date_header.inc.php';
include 'SM_colours.inc.php';

// Set the rows per page
$rpp = 10;

// Check the URL to see what page we are on
if (isset($_GET['pa'])) {
  $pa = $_GET['pa'];
}else{
  $pa = 0;
}

// Check which page we are on
if ($pa > 1) {
  $start = ($pa * $rpp) - $rpp;
}else {
  $start = 0;
}

// get a count of the number of orders that are closed
$sql = "SELECT COUNT(Distinct o.ID)
        FROM order_item oi
          , orders o
          , associate_companies ac
          , company c
        WHERE c.ID = 1
        AND (o.tCID = c.ID or o.fCID = c.ID)
        AND oi.OID = o.ID
        AND o.orComp = 1
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-oQ</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
}





// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 1.9 is also 2
$PTV = ($cOIID / $rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV) - 1;
// echo '<br>The number of pages will be : '.$PTV;

// echo '<br>Start record is : '.$start;
?>
<table class="trs" style="position:absolute; top: 9%; width:100%;">
  <tr>
    <th>Company</th>
    <th>Order</th>
    <th>Reference</th>
    <th>Qty</th>
    <th>Opened</th>
    <th>Completed</th>
    <th>Despatched</th>
  </tr>

  <?php

  // the query for the table
  $sql = "SELECT o.ID AS OID
  -- sql is order_and order_items_status.sql
         , ac.ID AS ACID
         , ac.name AS ACIDn
         , oi.ID AS OIID
         , oiq.order_qty AS Qty
         , o.OTID AS OTID
         , o.orNos AS OIDnos
         , o.our_order_ref AS OIDref
         , o.OTID AS OIDtype
         , o.orComp AS OIDComp
         , oi.itComp AS OIIDComp
         , o.orDate AS OIDst
         , odd.del_date AS DUEDate
         , oi.itCompDate AS OIDcomp
         , oi.itDesDate AS OIDdes
         -- , from_unixtime(o.orDate,'%d-%m-%Y') AS OIDst
         -- , from_unixtime(odd.del_date,'%d-%m-%Y') AS DUEDate
         -- , from_unixtime(oi.itCompDate,'%d-%m-%Y') AS OIDcomp
  FROM order_item oi
       INNER JOIN orders o ON o.ID = oi.OID
       INNER JOIN orders_due_dates odd ON o.ID = odd.OID
       INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
       INNER JOIN company c ON o.fCID = c.ID
       INNER JOIN company cS ON o.tCID = cS.ID
       INNER JOIN associate_companies ac ON o.fACID = ac.ID
       INNER JOIN associate_companies acS ON o.tACID = acS.ID
       INNER JOIN division d ON o.fDID = d.ID
       INNER JOIN division dS ON o.tDID = dS.ID
  WHERE (o.fCID = 1 OR o.tCID = 1)
    -- AND (o.orComp = 0 OR o.orComp = 0)
    AND (oi.itComp = 7 OR oi.itComp = 9)
  -- LIMIT ?,?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-oQ</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $CID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      $OID = $row['OID'];
      $ACID = $row['ACID'];
      $ACIDn = $row['ACIDn'];
      $Qty = $row['Qty'];
      $OTID = $row['OTID'];
      $OIDnos = $row['OIDnos'];
      $OIDref = $row['OIDref'];
      $OIDst = $row['OIDst'];
      $OIDtype = $row['OIDtype'];
      $OIDComp = $row['OIDComp'];
      $OIIDComp = $row['OIIDComp'];
      $OIDst = $row['OIDst'];
      $DUEDate = $row['DUEDate'];
      $OIDcomp = $row['OIDcomp'];
      $OIDdes = $row['OIDdes'];

      $OIDst = date('d-M-Y',$OIDst);
      $OIDcomp = date('d-M-Y',$OIDcomp);
      $OIDdes = date('d-M-Y',$OIDdes);

  ?>
    <tr>
      <td><?php echo $ACIDn ?></td>
      <td><?php echo $OIDnos ?></td>
      <td><?php echo $OIDref ?></td>
      <td><?php echo $Qty ?></td>
      <td><?php echo $OIDst ?></td>
      <td><?php echo $OIDcomp ?></td>
      <td><?php echo $OIDdes ?></td>
    </tr>
    <?php
    }
  } ?>
</table>

<div style="position:absolute; bottom:5%; right:5%;font-size:200%">
  <?php for ($x = 1 ; $x <= $PTV ; $x++){
    if (isset($_GET['oi'])) {
      ?>
      <a style="color:black; text-decoration:none;" href='<?php echo $urlPage ?>&tab=<?php echo $tab ?>&oi=<?php echo $OIID ?>&pa=<?php echo $x ?>'><?php echo $x ?></a>
      <?php
    }elseif (isset($_GET['OI'])) {
      ?>
      <a style="color:black; text-decoration:none;" href='<?php echo $urlPage ?>&tab=<?php echo $tab ?>&OI=<?php echo $OIID ?>&pa=<?php echo $x ?>'><?php echo $x ?></a>
      <?php
    }
  } ?>
</div>

<?php
