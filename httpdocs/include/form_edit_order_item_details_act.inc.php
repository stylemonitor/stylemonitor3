<?php
session_start();
echo "<br><b>include/form_edit_order_item_details_act.inc.php</b>";
$UID = $_SESSION['UID'];
$CID = $_SESSION['CID'];
$td = date('U');

if (!isset($_POST['save']) && !isset($_POST['cancel'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['cancel'])) {
  $urlPage = $_POST['urlPage'];
  $tab = $_POST['tab'];
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  // echo "<br>Return to the Users page";
  header("Location:../home.php?H&o=$OID&OI=$OIID");
  exit();
}elseif (isset($_POST['save'])) {
  include 'dbconnect.inc.php';

  // From the form
  // FIXED data
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  $OTID = $_POST['OTID'];
  $nPRID = $_POST['nPRID'];
  $NitemRef = $_POST['NitemRef'];
  $oNpPRID = $_POST['oNpPRID'];
  $oNpPTID = $_POST['oNpPTID'];
  $oNpPRIDs = $_POST['oNpPRIDs'];
  $pItemRef = $_POST['pItemRef'];
  $pPTIDn = $_POST['pPTIDn'];
  $pStyle = $_POST['pStyle'];
  $td = date('U');

  include 'from_OIID_get_DISTINCT_order_details.inc.php';

  // has our product reference changed?
  if (empty($nPRID)) {
    $nPRID = $PRID;
  }else {
    $nPRID = $nPRID;
  }

  // has our OID changed
  if (empty($NitemRef)) {
    $NitemRef = $OIIDoir;
  }else {
    $NitemRef = $NitemRef;
  }


  // has the supplier/client's ITEM DESCRIPTION changed? (SID)
  if (empty($pItemRef)) {
    $pItemRef = $pIRef;
  }else {
    $pItemRef = $pItemRef;
  }

  // has the supplier/client's PRODUCT TYPE changed? (SPT)
  if (empty($pPTIDn)) {
    $pPTIDn = $pPType;
  }else {
    $pPTIDn = $pPTIDn;
  }

  // has the supplier/client's STYLE DESCRIPTION changed? (SSN)
  if (empty($pStyle)) {
    $pStyle = $pSRef;
  }else {
    $pStyle = $pStyle;
  }

  echo "<br>OID-$OID :: OIID-$OIID :: nPRID-$nPRID :: NitemRef-$NitemRef :: oNpPRID-$oNpPRID :: oNpPTID-$oNpPTID :: oNpPRIDs-$oNpPRIDs :: pItemRef-$pItemRef :: pPTIDn-$pPTIDn :: pStyle-$pStyle";


  // echo "<br> log_order_item  (OIID, PRID, pPRID, UID, itemRef, PitemRef, pProdType, PstyleRef, inputtime)";
  // echo "<br> log_order_item  $OIID, $nPRID, $pPRID, $UID, $NitemRef, $pItemRef, $pPTIDn, $pStyle, $td";
  // echo "<br> order_item  $nPRID, $nPRID, $NitemRef, $pItemRef, $pPTIDn, $pStyle, $OIID";

//   // insert into log_order_item
//   $sql = "INSERT INTO log_order_item
//             (OIID, PRID, pPRID, UID, itemRef, PitemRef, pProdType, PstyleRef, inputtime)
//           VALUES
//             (?,?,?,?,?,?,?,?,?)
//   ;";
//   $stmt = mysqli_stmt_init($con);
//   if (!mysqli_stmt_prepare($stmt, $sql)){
//     echo '<b>FAIL-feoda1</b>';
//   }else{
//     mysqli_stmt_bind_param($stmt, "sssssssss", $OIID, $nPRID, $pPRID, $UID, $NitemRef, $pIteRef, $pPTIDn, $pStyle, $td);
//     mysqli_stmt_execute($stmt);
//   }
//
//
// // echo "<br> $nPRID, $nPRID, $NitemRef, $NpPRID, $NpPTID, $NpPRIDs, $OIID";
//   // update order_item
//   $sql = "UPDATE order_item
//           SET PRID = ?
//             , pPRID = ?
//             , their_item_ref = ?
//             , pItemRef = ?
//             , pProdType = ?
//             , pStyleRef = ?
//           WHERE ID = ?
//   ;";
//   $stmt = mysqli_stmt_init($con);
//   if (!mysqli_stmt_prepare($stmt, $sql)){
//     echo '<b>FAIL-feoda2</b>';
//   }else{
//     // mysqli_stmt_bind_param($stmt, "sss", $nPRID, $NsamProd, $OIID);
//     mysqli_stmt_bind_param($stmt, "sssssss", $nPRID, $nPRID, $NitemRef, $pItemRef, $pPTIDn, $pStyle, $OIID);
//     mysqli_stmt_execute($stmt);
//   }
//   header("Location:../home.php?H&rt2&o=$OID");
//   exit();
}
