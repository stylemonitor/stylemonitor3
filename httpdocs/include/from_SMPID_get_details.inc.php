<?php
// echo "<b>include/from_SMPID_get_details.inc.php</b>";
include 'dbconnect.inc.php';

$sql = "SELECT sm.id as SMPID
		      , sm.problem as problem
          , sm.date_rec as date_rec
          , sm.file as file
					, sm.date_fixed as fxtd
          , u.ID as UID
          , u.firstname as firstname
          , u.surname as surname
        FROM SM_problems sm
        , users u
        WHERE sm.id = ?
        AND sm.found_by = u.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fccms</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $SMPID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $SMPID = $row['SMPID'];
  $problem = $row['problem'];
  $date_rec = $row['date_rec'];
  $file = $row['file'];
  $found_by = $row['found_by'];
  $UID = $row['UID'];
  $firstname = $row['firstname'];
	$surname = $row['surname'];
  $fxtd = $row['fxtd'];
}
