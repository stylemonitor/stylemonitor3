<?php
include 'dbconnect.inc.php';
// echo "include/VIEW_from_CID_count_CPID_supplier_data.inc.php";

if (isset($_GET['prt'])) {
  $CPID = $_GET['prt'];
  include 'include/from_CPID_get_partners_CID.inc.php';
  $CID = $tCID;
}else {
  $CID = $_SESSION['CID'];
}

$sql = "SELECT sc.SU_ID as sup_CID
          , sc.SU_ASSOC_CO as csup_ACID
          , sc.SU_ASSOC_DIV as csup_DID
          , c.name as CIDn
        FROM supplier_count sc
          , company c
        WHERE sc.SU_ID = ?
        AND c.ID = sc.SU_ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-vcccsd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $sup_CID = $row['sup_CID'];
  $csup_ACID = $row['csup_ACID'];
  $csup_DID = $row['csup_DID'];
  $sup_CIDn  = $row['CIDn'];
}
