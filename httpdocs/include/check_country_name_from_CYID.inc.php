<?php
include 'dbconnect.inc.php';
// echo  "<br><b>check_country_name_from_CYID.inc.php</b>";
// echo  "<br>ACID : $ACID";

$sql = "SELECT name as CYIDn
        FROM country
        WHERE ID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-ccnfc";
}else {
  mysqli_stmt_bind_param($stmt,"s", $CYID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $CYIDn  = $row['CYIDn'];
}
