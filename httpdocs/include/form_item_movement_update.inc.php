<?php
include 'dbconnect.inc.php';
// echo "<br><b>include/form_item_movement_update.inc.php</b>";
// include 'set_urlPage.inc.php';

$CID = $_SESSION['CID'];

// get the info from ther URL
// if (isset($_GET['tab'])) {$tab = $_GET['tab'];}

if (isset($_GET['ot'])) {
  // $tab = $_GET['tab'];
  $OTID = $_GET['ot'];
  $sp = $_GET['sp'];
  $OIID = $_GET['OI'];
  $OID = $_GET['o'];
}elseif (isset($_GET['OI'])) {
  $OID = $_GET['o'];
  $OIID = $_GET['OI'];
  $rtnPage = "$urlPage&OI=$OIID";
}elseif (isset($_GET['o'])) {
  $OIID = $_GET['OI'];
  $OID = $_GET['o'];
  $rtnPage = "$urlPage&o=$OID";
}

if ($ot == 1) {
  $bc = '#f7cc6c';
  $title = 'SALES ORDER : ';
}elseif ($ot == 2) {
  $bc = '#bbbbff';
  $title = 'PURCHASE ORDER : ';
}elseif ($ot == 3) {
  $bc = '#d68e79';
  $title = 'PARTNER SALES ORDER : ';
}elseif ($ot == 4) {
  $bc = '#c9d1ce';
  $title = 'PARTNER PURCHASE ORDER : ';
}

include 'rtnPage.inc.php';

include 'from_OIID_get_order_details.inc.php';
include 'from_OIID_min_make_unit.inc.php';

if ($OIIDComp == 1) {
  echo "This order is completed";

}elseif ($OIIDComp == 2) {
  ?>
  <div style="position:absolute; top:43%; height:3%; left:29%; width:43%; font-weight: BOLD; background-color:<?php echo $bc ?>; padding-top:0.5%; border-radius:10px; border-bottom: thick solid grey; z-index:1;">
    <?php echo "$title $oref/$OIIDnos" ?>
  </div>
  <div style="position:absolute; top:47.1%; height:5%; left:29%; width:43%; padding-top:0.5%; background-color:red; border: 2px ridge black; border-radius:10px; font-size: 150%; font-weight: bold; z-index:1;">
    NO ITEMS CAN BE MOVED
  </div>

  <form action="<?php echo $rtnPage ?>" method="post">
    <button class="entbtn" style="position:absolute; bottom:5%; right:5%; width:10%; z-index:1;" type="submit" name="button">Close</button>
  </form>
  <?php
}else {
  $OPID = $mOPID;

  if (isset($_GET['val'])) {$val = $_GET['val'];}
  if (isset($_GET['mp'])) {$mp = $_GET['mp'];}
  // if (isset($_GET['sp'])) {$sp = $_GET['sp'];}

  // set the monitoring/movement point
  if ($mp == 0) {
    $OIMRID = 3;
    $sectA = 'Movement from Awaiting';
    $section = 'Movement from Awaiting';
    $Lsect = 'Awaiting';
    $Rsect = 'MP1';
    $notes = 'Notes about the movement';
  }elseif ($mp == 1) {
    $OIMRID = 4;
    $section = 'Movement from MP1';
    $Lsect = 'Awaiting';
    $Rsect = 'MP2';
    $notes = 'Notes about the movement';
  }elseif ($mp == 2) {
    $OIMRID = 5;
    $sectA = 'Movement from MP2';
    $section = 'Movement from MP2';
    $Lsect = 'MP1';
    $Rsect = 'MP3';
    $notes = 'Notes about the movement';
  }elseif ($mp == 3) {
    $OIMRID = 6;
    $sectA = 'Movement from MP3';
    $section = 'Movement from MP3';
    $Lsect = 'MP2';
    $Rsect = 'MP4';
    $notes = 'Notes about the movement';
  }elseif ($mp == 4) {
    $OIMRID = 7;
    $sectA = 'Movement from MP4';
    $section = 'Movement from MP4';
    $Lsect = 'MP3';
    $Rsect = 'MP5';
  }elseif ($mp == 5) {
    $OIMRID = 8;
    $sectA = 'Movement from MP5';
    $section = 'Movement from MP5';
    $Lsect ='MP4';
    $Rsect ='Warehouse';
    $notes = 'Notes about the movement';
  }elseif ($mp == 6) {
    $OIMRID = 19;
    $sectA = 'Movement from Warehouse';
    $section = 'Movement from Warehouse';
    $Lsect ='MP5';
    $Rsect ='Client';
    $notes = 'AWB Details';
  }elseif ($mp == 7) {
    $OIMRID = 22;
    $sectA = 'GOODS Received';
    $section = 'GOODS Received';
    $Lsect ='Warehouse';
    $Rsect ='Client';
    $notes = 'Notes about the movement';
  }

  $boxc = '#ffffd1';
  if ($mp == 6) {
    $boxc = '#76ffb5';
  }elseif ($mp == 7) {
    $bc = '#f5f5f5';
    $boxc = '#aff3ff';
  }
  ?>
  <div class="overlay"></div>
  <?php

  if ($ot == 4) {
    ?>
    <div style="position:absolute; top:36%; height:10%; left:30%; width:40%; font-size:150%; background-color:pink; border-radius:10px; border:medium groove grey;">
      You cannot move a
      <br>PARTNER PURCHASE ORDER
    </div>
    <?php
  }else {
    if (!isset($val)) {
    }else {
      if ($val == 0) {
        $section = "NO ITEMS AVAILABLE TO MOVEgbgb"
        ?>
        <div style="position:absolute; top:61%; height:3%; left:35%; width:30%; font-weight: BOLD; background-color:<?php echo $bc ?>; padding-top:0.5%; border-radius:10px; border-bottom: thick solid grey; z-index:1;">
          <?php echo $section ?>
        </div>
      <div style="position:absolute; top:65.1%; height:4.3%; left:35%; width:30%; padding-top:0.2%; background-color:#<?php echo $bc ?>; border-radius:10px; z-index:1;">
        <a style="color:black; font-weight: bold; text-decoration:none;" href="<?php echo $url ?>">CANCEL</a>
      </div>
      <?php
    }else {
      ?>
      <div style="position:absolute; top:41%; height:3%; left:29%; width:43%; font-weight: BOLD; background-color:<?php echo $bc ?>; padding-top:0.5%; border-radius:10px; border-bottom: thick solid grey; z-index:1;">
        <?php echo "$title $oref/$OIIDnos" ?>
        <?php echo $section ?>
      </div>

      <div style="position:absolute; top:45.1%; height:33.5%; left:29%; width:43%; padding-top:0.5%; background-color:<?php echo $boxc ?>; border-radius:10px; z-index:1;">
      </div>

      <div style="position:absolute; top:47%; height:5%; left:30%; width:10%; font-size:140%; text-align:left; z-index:1;">
        Quantity :
      </div>
      <div style="position:absolute; top:47%; height:4%; left:38%; width:10%; font-size:140%; text-align:right; z-index:1;">
        <b><?php echo $val ?></b>
      </div>
      <div style="position:absolute; top:47%; height:5%; left:48.5%; width:4%; font-size:140%; text-align:center; z-index:1;">
        or
      </div>

      <div style="position:absolute; top:58%; height:4%; left:0%; width:100%; font-size:110%; font-weight: bold; text-align:center; z-index:1;">
        Reason for movement
      </div>

      <?php
      if (isset($_GET['Awt'])) {
        ?>
        <div style="position:absolute; top:61.3%; height:3%; left:40%; width:20%; font-size: 120%;  border-bottom:thin solid black; z-index:1;">
          To <b><?php echo $Rsect ?></b>
        </div>
        <?php
      }elseif (isset($_GET['Des'])) {
        ?>
        <div style="position:absolute; top:61.3%; height:4%; left:40%; width:20%; font-size: 120%;  border-bottom:thin solid black; z-index:1;">
          Return To <b><?php echo $Lsect ?></b>
        </div>
        <?php
      }else {
        ?>
        <div style="position:absolute; top:61.3%; height:4%; left:30%; width:20.5%; font-size: 120%; border-bottom:thin solid black; border-right:thin solid black; z-index:1;">
          Return To <b><?php echo $Lsect ?></b>
        </div>
        <div style="position:absolute; top:61.3%; height:4%; left:50.5%; width:20%; font-size: 120%;  border-bottom:thin solid black; z-index:1;">
          To <b><?php echo $Rsect ?></b>
        </div>
        <?php
      }
      ?>

      <form action="include/form_item_movement_update_act.inc.php" method="post">
        <input type="hidden" name="url" value="<?php echo $rtnPage ?>">
        <input type="hidden" name="OID" value="<?php echo $OID ?>">
        <input type="hidden" name="OIID" value="<?php echo $OIID ?>">
        <input type="hidden" name="OIQIDq" value="<?php echo $OIQIDq ?>">
        <input type="hidden" name="OPID" value="<?php echo $OPID ?>">
        <input type="hidden" name="val" value="<?php echo $val ?>">
        <input type="hidden" name="rej" value="<?php echo $rej ?>">
        <input type="hidden" name="mp" value="<?php echo $mp ?>">
        <input type="hidden" name="OIMRID" value="<?php echo $OIMRID ?>">
        <input type="hidden" name="moveType" value="1">

        <input autofocus type="text" style="position:absolute; top:46.5%; left:55%; width:14%;font-size:120%; text-align:right; padding-right:1%; z-index:1;" name="movQty" placeholder="Quantity" value="">
        <?php

        if ($mp == 6) {
          ?>
          <input required type="text" style="position:absolute; top:52%; height:3.5%; left:30%; width:40%; text-align:center; z-index:1;" placeholder="<?php echo $notes ?>" name="comm" value="">

          <?php
        }else {
          ?>
          <input type="text" style="position:absolute; top:52%; height:3.5%; left:30%; width:40%; text-align:center; z-index:1;" placeholder="<?php echo $notes ?>" name="comm" value="">
          <?php
        }

        if (isset($_GET['Awt'])) {
          ?>
          <div style="position:absolute; top:65.5%; height:7%; left:40.5%; width:25%; text-align:left; padding-left:1%; border-bottom:thin solid black; z-index:1;">
            <input type="radio" name="moveType" value="1">PASSED work
            <br><input type="radio" name="moveType" value="4">Over count REJECT work
          </div>
          <?php
        }elseif (isset($_GET['Des'])) {
          ?>
          <div style="position:absolute; top:65.5%; height:7%; left:40.5%; width:19%; text-align:left; padding-left:1%; border-bottom:thin solid black; z-index:1;">
            <input type="radio" name="moveType" value="2">REJECT back
            <br><input type="radio" name="moveType" value="3">Over count PASSED work
          </div>
          <?php
        }else {
          ?>
          <div style="position:absolute; top:65.5%; height:7%; left:30%; width:20.5%; text-align:left; border-bottom:thin solid black; border-right:thin solid black; z-index:1;">
            <input type="radio" name="moveType" value="2">REJECT back
            <br><input type="radio" name="moveType" value="3">Over count PASSED work
          </div>
          <div style="position:absolute; top:65.5%; height:7%; left:49.5%; width:20%; text-align:left; padding-left:1%; border-bottom:thin solid black; z-index:1;">
            <input type="radio" name="moveType" value="1">PASSED work
            <br><input type="radio" name="moveType" value="4">Over count REJECT work
          </div>
          <?php
        }

        if (isset($_GET['INS'])) {
          ?>
          <button class="entbtn" type="submit" style="position:absolute; top:74%; height:3.8%; left:45%; width:10%; padding-top: 0.2%; z-index:1;" name="canMove">CANCEL</button>
          <?php
        }else {
          ?>
          <button class="entbtn" type="submit" style="position:absolute; top:74%; height:4%; left:37.5%; width:10%; padding-top: 0%; background-color: <?php echo $fsvCol ?>; z-index:1;" name="move">MOVE</button>
          <button class="entbtn" type="submit" style="position:absolute; top:74%; height:4%; left:52.5%; width:10%; padding-top: 0%; background-color: <?php echo $fcnCol ?>; z-index:1;" name="canMove">CANCEL</button>
          <?php
        }
        ?>
      </form>
      <?php
    }
  }
}
}
