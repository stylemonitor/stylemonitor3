<?php
include 'dbconnect.inc.php';
// echo "include/from_CID_select_FROM_comp_div.inc.php";

$sql = "SELECT d.ID as cDID
            FROM division d
              , associate_companies ac
            WHERE ac.ID = (SELECT MIN(ac.ID) as mACID
    								        FROM associate_companies ac
    								          , company c
    								        WHERE c.ID = ?
    								        AND ac.CID = c.ID)
            AND d.ACID = ac.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fpspcd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $fCID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $nosC = $row['cDID'];
}

$sql = "SELECT d.ID as DID
              , d.name as DIDn
            FROM division d
              , associate_companies ac
            WHERE ac.ID = (SELECT MIN(ac.ID) as mACID
    								        FROM associate_companies ac
    								          , company c
    								        WHERE c.ID = ?
    								        AND ac.CID = c.ID)
            AND d.ACID = ac.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fpspcd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $fCID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $fDID = $row['DID'];
    $fDIDn = $row['DIDn'];

    echo '<option value="'.$fDID.'">'.$fDIDn.'</option>';
  }
}

?>
