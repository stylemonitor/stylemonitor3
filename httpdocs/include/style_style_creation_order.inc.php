<div style="position:absolute; top:2%; height:2.5%; right:5%; width:90%; background-color:#c9d1ce;">
  STYLE CREATION NOTES
  <p>Enter a nine character name/number
  in the
  <br><b>Short Code</b> box
  <br>and then add a longer
  <br><b>Description</b>
  <br>and then click on
  <br><b>SAVE</b>
</div>

<div style="position:absolute; top:26%; height:2.5%; right:20%; width:60%; background-color:#c9d1ce;">CREATE AN ORDER
  <p>Click on either the <br><b>Short Code</b> <br>OR the <br><b>Description</b> <br>of the item you want to order</p>
  <p>Then select the type of order you want either
  <br><b>Sales</b> or <b>Purchase</b></p>
</div>

<div style="position:absolute; top:60%; height:2.5%; right:20%; width:60%; background-color:#c9d1ce;">REVIEW A STYLE
  <p>To Review a style and the number and nature of its orders select
  <br><b>Review the Style</b></p>
</div>

<div style="position:absolute; top:80%; height:2.5%; right:20%; width:60%; background-color:#c9d1ce;">EDIT A STYLE
  <p>To Edit a style by changing its short code, description etc select
  <br><b>Edit the Style</b></p>
</div>
