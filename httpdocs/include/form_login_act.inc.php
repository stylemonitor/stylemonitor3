<?php
session_start();
// echo "<br><b>include/form_login_act.inc.php</b>";
include ("dbconnect.inc.php");

if (!isset($_POST['login']) && (!isset($_POST['forget']))) {
  // echo "<br>INCORRECT ACCESS METHOD";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['forget'])) {
  // echo "go to new page to input user uid";
  header("Location:../index.php?FPW");
  exit();
}
elseif (isset($_POST['login'])) {
  // echo "<br>Login method Ok";

  // GET the user input detail from the form
  $fUID = $_POST['UID'];
  $epwd = $_POST['pwd'];
  $td   = date('U');

  // echo "<br>Username : $fUID";
  // echo "<br>Password entered : $epwd";
  // echo "<br>Todays date is : $td";

  include 'form_check_UID_login_count.inc.php';
}
