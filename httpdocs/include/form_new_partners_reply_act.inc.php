<?php
include 'dbconnect.inc.php';
$UID = $_SESSION['UID'];
$td = date('U');

include 'from_UID_get_company_details.inc.php';
// echo "<b>include/form_new_company_user_act.inc.php</b>";

if (!isset($_POST['part_acc']) && !isset($_POST['part_rej'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['part_acc'])) {
  $CPID     = $_POST['CPID'];
  $req_code = $_POST['req_code'];
  $reply    = 1;
}elseif (isset($_POST['part_rej'])) {
  $CPID     = $_POST['CPID'];
  $req_code = $_POST['req_code'];
  $reply    = 2;
}
  //
  // if ($reply == 1) { $ans = a; $reply = 1; }
  // elseif ($reply == 2) { $ans = r; $reply = 2; };
  // echo "<br>CPID is : $CPID";
  // echo "<br>Reply is : $reply";
  // echo "<br>Request code is : req_code";

// Start transaction
mysqli_begin_transaction($mysqli);
try {
  // to

  // Get the information from the partnership request
  $sql = "SELECT acc_CID as aCID
            , acc_SEL as CPIDas
            , active as CPIDac
          FROM company_partnerships
          WHERE ID = ?
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-fnpra</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $CPID);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($res);
    $aCID = $row['aCID'];
    $CPIDas = $row['CPIDas'];
    $CPIDac = $row['CPIDac'];

    if ($CPIDac <> 0) {
      // echo "<br>The request has already been closed";
      header("Location:../partners.php?P&pap&7");
      exit();
    }else {
      // echo "<br>The request is still open";

      // if ($aCID <> $CID) {
      //   // echo "<br>The request is for a different company";
      //   header("Location:../partners.php?P&pap&8");
      //   exit();
      // }else {
      // echo "<br>The right company is replying";

      if ($CPIDas <> $req_code) {
        // echo "<br>An incorrect code has been used";
        header("Location:../partners.php?P&paa&p=$CPID&$ans&8");
        exit();
      }else {
        // echo "<br>The right code has been used";

        // update the request to accpted OR rejected
        $sql = "UPDATE company_partnerships
                SET active = ?
                  , accdate = ?
                WHERE ID = ?
                ;";
        $stmt = mysqli_stmt_init($con);
        if(!mysqli_stmt_prepare($stmt, $sql)){
          echo '<b>FAIL-fnpra2</b>';
        }else{
          mysqli_stmt_bind_param($stmt, "sss", $reply, $td, $CPID);
          mysqli_stmt_execute($stmt);
          $res = mysqli_stmt_get_result($stmt);
        }
      }
    }
  }
} catch (mysqli_sql_exception $exception) {
  mysqli_rollback($mysqli);

  throw $exception;
}

header("Location:../partners.php?P&pap");
exit();
