<?php
session_start();
// FOR FIRST TIME USE ONLY
// echo  '<b>input_new_product_type_reg.inc.php</b>';
// echo  '<br>Accessed from : <b>input_new_product_type.inc.php</b>';
// echo  '<br><br><br><b>Check the method of getting to the form entry (4) : </b>';

$td = date('U');
if (isset($_POST['cancel'])) {
  // header("Location:../home.php?S&pt");
  // exit();
}elseif (!isset($_POST['Y_re'])){
  // echo  'Incorrect method used';
  // header('Location:../index.php');
  // exit();
}else{

  include 'dbconnect.inc.php';

  // echo  '<br>The correct login method was used';
  // echo  '<br><br><b>Get the data from the form AND the session</b>';

  $UID = $_SESSION['UID'];
  $ACID = $_SESSION['ACID'];
  $scode = $_POST['scode'];
  $pref = $_POST['pref'];

  // include 'insert_prod_type.inc.php';

  // echo  '<br>The associate company ID ($ACID) is : <b>'.$ACID.'</b>';
  // echo  '<br>The product short code ($scode) is : <b>'.$scode.'</b>';
  // echo  '<br>The product description ($pref) is : <b>'.$pref.'</b>';
  // echo  '<br><br><b>Check the validity of the data the user has added</b>';

  include 'from_scode_get_scodEID.inc.php';
  include 'from_pref_get_proDID.inc.php';

  // Is the product is already listed

  //Check the data
  if ($scodEID <> 0) {
    echo '<br>The product type short code is taken';
    // header("Location:../home.php?S&pt&1");
    // exit();
  }else {
    echo "<br>The product type short code is new";
    if(strlen($scode)>=9){
      echo '<br>The code is too long';
      // header("Location:../home.php?S&pt&2");
      // exit();
    }else {
      echo "<br>The product type short code is OK for length";
      echo "<br>Add the product type";
      if ($proDID <> 0) {
        echo '<br>The product type name is already in use';
        // header("Location:../home.php?S&pt&3");
        // exit();
      }else {
        echo "<br>The product type name is new";

      }
    }

    include 'from_CID_get_min_section.inc.php';
    include 'insert_prod_type.inc.php';

    // get the product tpe ID * * * WHY * * *  ??? just to see if it worked
    // to see if it works - it does so comment out for now 13052020
      $sql = "SELECT pt.ID as PTID
              FROM product_type pt
              WHERE ACID = ?
              -- AND UID = ?
              AND scode = ?;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo '<b>FAIL4</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ss", $ACID, $scode);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        $row = mysqli_fetch_array($result);
        $PTID = $row['PTID'];
      }

    // get the product tpe ID * * * WHY * * *

    // echo  '<br>Product type is $PTID : <b>'.$PTID.'</b>';
    // echo  "<br>Product type should now be entered";
    // header("Location:../home.php?S&pt");
    // exit();
  }
}
