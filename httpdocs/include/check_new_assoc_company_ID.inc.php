<?php
// echo "<br><b>check_new_assoc_company_ID.inc.php</b>";

$sql = "SELECT ID as ACID
        FROM associate_companies
        WHERE CID = ?
        AND name = ?
        AND CTID = ?
        AND CYID = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-snacID</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $CID, $name, $CTID, $CYID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $chACID = $row['ACID'];
}
// echo "<br>Associate companies ID is ($chACID)<b> : $chACID</b>";
