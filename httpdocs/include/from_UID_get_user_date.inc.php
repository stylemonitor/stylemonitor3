<?php
include 'dbconnect.inc.php';
// echo "<br><b>include/from_UID_get_user_date.inc.php</b>";
// CHECK the email is registered

$UID = $_SESSION['UID'];

$sql = "SELECT TIMEZONE as TIDn
					, UTC_Offset as TIDoff
				FROM timezones
				WHERE id = (SELECT TID
										FROM users
										WHERE ID = ?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fugudate</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $UID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row    = mysqli_fetch_assoc($result);
	$TIDn = $row['TIDn'];
	$TIDoff = $row['TIDoff'];
}
