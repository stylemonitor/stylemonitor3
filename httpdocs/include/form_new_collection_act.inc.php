<?php
include 'dbconnect.inc.php';
// echo '<b>include/form_new_product_type_act.inc.php</b>';
// echo '<br>FROM:form_new_product_type.inc.php</b>';
if (!isset($_POST['newcoll']) && !isset($_POST['cancoll'])){
  // echo 'Incorrect method used';
  header('Location:../index.php');
  exit();
}elseif (isset($_POST['cancoll'])){
  // echo 'Cancel Season Adding';
  header('Location:../styles.php?S&col');
  exit();
}elseif (isset($_POST['newcoll'])){
  include 'from_CID_count_collection.inc.php';
  // echo 'Add Season name';
  // echo '<br>The correct login method was used';
  // echo '<br><br><b>Get the data from the form AND the session</b>';

  $coll = $_POST['coll'];
  $UID = $_SESSION['UID'];
  $ACID = $_SESSION['ACID'];
  $td = date('U');

  // echo '<br>The season name  ($seas) is : <b>'.$seas.'</b>';
  // echo '<br>The user ID ($UID) is : <b>'.$UID.'</b>';
  // echo '<br>The company ID ($ACID) is : <b>'.$ACID.'</b>';

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to

    $sql = "SELECT ID
            FROM collection
            WHERE name = ?
            AND ACID = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-fnca</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $coll, $ACID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $CLID = $row['ID'];
    }

    if ($CLID > 0) {
      echo "<br>Season name already listed";

    }else {
    // register the name
    $sql = "INSERT INTO collection
              (ACID, UID, name, inputtime)
            VALUES
              (?,?,?,?)
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-fnca</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $ACID, $UID, $coll, $td);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
    }

  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  // echo ' <br>The product type has been registered';
  header("Location:../styles.php?S&col");
  exit();
  }
}
?>
