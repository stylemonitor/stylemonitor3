<?php
// echo "<br><b>input_new_associate_company.inc.php</b>";

// Add the company
$sql = "INSERT INTO associate_companies
          (CID, CTID, CYID, TID, UID, name, inputtime)
        VALUES
          (?,?,?,?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-inac</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssssss", $CID, $CTID, $CYID, $TID, $UID, $name, $td);
  mysqli_stmt_execute($stmt);
}

// Get the new associate companies ID
$sql = "SELECT ID as newACID
        FROM associate_companies
        WHERE CID = ?
        AND name = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-inac2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $name);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $newACID = $row['newACID'];
}
// echo "<br>Associate company ID (newACID) : $newACID";

// Add the associate companies address
$sql = "INSERT INTO addresses
          (CYID, TID, UID, addn, add1, add2, city, pcode, county, tel, inputtime)
        VALUES
          (?,?,?,?,?,?,?,?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo "<br><b>FAIL-inac3</b>";
}else{
  mysqli_stmt_bind_param($stmt, "sssssssssss", $CYID, $TID, $UID, $addn, $add1, $add2, $city, $pcode, $county, $tel, $td);
  mysqli_stmt_execute($stmt);
}

// Get the new address
$sql = "SELECT ID as AID
        FROM addresses
        WHERE CYID = ?
        AND UID = ?
        AND inputtime = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-inac4</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $CYID, $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $AID = $row['AID'];
}
// echo "<br>Address ID (AID) : $AID";
// echo "<br>User ID (UID) : $UID";
// echo "<br>Inputtime (td) : $td";


// Link the associate company to an address
$sql = "INSERT INTO company_associates_address
          (ACID, AID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo "<br><b>FAIL-inac5</b>";
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $newACID, $AID, $UID, $td);
  mysqli_stmt_execute($stmt);
}

// Get the company_associates_address ID
$sql = "SELECT ID as CAAID
        FROM company_associates_address
        WHERE ACID = ?
        AND AID = ?
        AND UID = ?
        AND inputtime = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-inac6</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $newACID, $AID, $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $CAAID = $row['CAAID'];
}
// echo "<br> Associate company address ID is $CAAID";
