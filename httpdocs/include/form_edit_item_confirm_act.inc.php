<?php
session_start();
include 'dbconnect.inc.php';
// echo "<br><b>form_edit_item_confirm_act.inc.php</b>";
$CID=$_SESSION['CID'];
$UID=$_SESSION['UID'];

$td = date('U');

if (!isset($_POST['NoBtn']) && !isset($_POST['YesBtn']) && !isset($_POST['chgBtn'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['NoBtn'])) {
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  // echo "<br>Cancel the request";
  header("Location:../home.php?H&rt3&o=$OID&OI=$OID");
  exit();
}elseif (isset($_POST['chgBtn'])) {
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  $eit = $_POST['eit'];
  // echo "<br>Re-edit the request";
  header("Location:../home.php?H&rt3&o=$OID&OI=$OIID&eit=$eit");
  exit();
}elseif (isset($_POST['YesBtn'])) {
  // echo "<br>Request the changes654";
  $incItem = $_POST['incItem'];
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  $OIMRID = $_POST['OIMRID'];
  $OTID = $_POST['OTID'];
  $OPID = $_POST['OPID'];
  $oiQty = $_POST['oiQty'];
  $delDate = $_POST['del'];
  $Nqty = $_POST['Nqty'];
  $canReas = $_POST['canReas'];
  $eit = $_POST['eit'];
  $td = date('U');

  // echo "oiQty=$oiQty :: Nqty=$Nqty";

  $qty = ($oiQty-$Nqty);

  // echo "<br>incItem = $incItem :: OID = $OID :: oicCID = $CID :: OIID = $OIID :: OIMRID = $OIMRID :: OTID = $OTID :: OPID = $OPID :: oiQty = $oiQty :: delDate = $delDate :: Nqty = $Nqty :: canReas = $canReas :: td = $td";

  include 'from_OIID_get_order_details.inc.php';
  include 'action/select_CIDs_from_CPID.act.php';

  if ($cliCID <> $supCID) {
    // echo "<br>PARTNERSHIP REQUEST";
    include 'action/insert_into_OPM.act.php';
    include 'action/select_OPMID.act.php';
    include 'action/insert_into_order_item_change_table_PARTNER_delDate.act.php';

    // now we need to actually activate the change in the system

  }else {
    // echo "<br>Associate request";
    include 'action/insert_into_OPM.act.php';
    include 'action/select_OPMID.act.php';
    // echo "OPMID=$OPMID";
    // accept the changes
    include 'action/insert_into_OPM_assoc.act.php';
    include 'action/select_OPMID_response.act.php';
    // echo "::response=$response";
    // place the request AND approval in the order_item_change table
    include 'action/insert_into_order_item_change_date_change_assoc.act.php';
    // echo "delDate=$delDate";
    include 'action/select_OIDDID_from_OIID.act.php';
    include 'action/update_order_item_del_date.act.php';
  }
  header("Location:../home.php?H&o=$OID&OI=$OIID");
  exit();
}
