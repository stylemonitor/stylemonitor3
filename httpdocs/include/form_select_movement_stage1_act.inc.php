<?php
include 'dbconnect.inc.php';
// echo "include/form_select_movement_date_act.inc.php";

if (!isset($_POST['selDate']) && !isset($_POST['tdBtn'])) {
  // echo "HOME PAGE RE-DIRECT";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['selDate'])){
  // echo "Get the date and show the information";
  $tab =$_POST['tab'];
  $movDate = $_POST['movDate'];
  if (empty($movDate)) {
    $td = date('U');
    // echo "todays date $td";
    $movDate = date('Y-m-d', $td);
    // echo "Revised date $td";
  }
  // echo "<br>Selected date is $movDate";
  header("Location:../reports.php?R&tab=$tab&rep=$movDate");
  exit();
}elseif (isset($_POST['tdBtn'])) {
  $tab =$_POST['tab'];
  $td = date('U');
  $movDate = date('Y-m-d',$td);
  // include 'report_movement_date_awt.inc.php';
  header("Location:../reports.php?R&tab=$tab&rep=$movDate");
  exit();
}
