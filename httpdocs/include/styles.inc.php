<?php
// echo "<b>include/styles.inc.php</b>";
include 'dbconnect.inc.php';
$CID = $_SESSION['CID']; // get the company CID

// echo "<b>start_SM.php</b>";
// echo "<br>No orders have yet been placed on the system";

include 'include/from_CID_count_product_type.inc.php';    // has a product type has been created
include 'include/from_CID_check_product_ref_set.inc.php'; //  has a product reference been created
include 'include/from_CID_count_order_open.inc.php';  // has an order been created

if (isset($_GET['col'])) {
  include 'form_new_collection.inc.php';
  include 'report_collections.inc.php';
}elseif (isset($_GET['eit'])) {
  // echo "EDIT ITEM PAGE";
  include 'form_edit_order_item_details.inc.php';
  if (isset($_GET['cor'])) {
    include 'form_edit_order_item_details_can.inc.php';
  }
  if (isset($_GET['can'])) {
    include 'form_edit_item_cancel.inc.php';
  }
}elseif (isset($_GET['sni'])) {
  include 'style_info.inc.php';
}elseif (isset($_GET['snp'])) {
  include 'report_single_product_type_from_PTID.inc.php';
  if (isset($_GET['a']) || isset($_GET['b']) || isset($_GET['c'])) {
    include 'form_new_product_type.inc.php';
  }elseif ($cPTID == 1) {
    include 'form_new_product_type.inc.php';
  }else {
    // echo "Add and review";
    include 'form_select_product_type_action.inc.php';
  }
}elseif (isset($_GET['spo'])) {
  // echo "<br>Show the styles associated with the product type";
  include 'report_orders_per_product_type.inc.php';
}elseif (isset($_GET['spr'])) {
  // echo "<br>Show the styles associated with the product type";
  include 'report_styles_per_product_type.inc.php';
  if (isset($_GET['fso'])) {
    include 'form_style_option.inc.php';
  }
  if (isset($_GET['help'])) {
    include 'help/report_styles_per_product_type.help.inc.php';
  }
}elseif (isset($_GET['spt'])) {
  include 'report_product_type.inc.php';
  if (isset($_GET['spth'])) {
    include 'help/report_product_type.help.inc.php';
  }
  if (isset($_GET['fpto'])) {
    include 'form_product_type_option.inc.php';
  }elseif (isset($_GET['fnpt'])){
    include 'form_new_product_type.inc.php';
    if (isset($_GET['fnpth'])) {
      include 'help/form_new_product_type.help.inc.php';
    }
  }
  if (isset($_GET['sm'])) {
    include 'how_to_form_new_product_type_ADD.inc.php';
  }
}
elseif (isset($_GET['spe'])) {
  include 'report_single_product_type_from_PTID.inc.php';
  // include 'report_product_type.inc.php';
  include 'edit_product_type.inc.php';
}elseif (isset($_GET['sdv'])) {
  // include 'report_seasons.inc.php';
  include 'report_style_select.inc.php';
  // include 'report_style_division.inc.php';
  // include 'report_style_season.inc.php';
  // include 'report_style_collection.inc.php';
  include 'report_style_orders.inc.php';
  // include 'report_division_style.inc.php';
}elseif (isset($_GET['sea'])) {
  include 'form_new_season.inc.php';
  include 'report_seasons.inc.php';
}elseif (isset($_GET['ser'])) {
  include 'form_new_season.inc.php';
  include 'report_seasons.inc.php';
}elseif (isset($_GET['rpt'])) {
  include 'report_product_type_style.inc.php';
  // echo "<br>Review product type details";
}elseif (isset($_GET['see'])) {
  include 'edit_season.inc.php';
  include 'report_seasons.inc.php';
}elseif (isset($_GET['sep'])) {
  include 'include/report_product_type.inc.php';
}elseif (isset($_GET['sfl'])) {
  include 'report_style_selected.inc.php';
  include 'report_style_factory_loading.inc.php';
}elseif (isset($_GET['sns'])) {
  if ($cPRID == 0) {
    // echo "Add your first style";
    include 'form_new_style.inc.php';
  }else {
    include 'form_select_style_action.inc.php';
    // echo "Selection page for NEW or LIST Styles";
  }
}elseif (isset($_GET['snn'])) {
  include 'report_style.inc.php';
  if (isset($_GET['snnh'])) {
    include 'help/report_style.help.inc.php';
  }
  if(isset($_GET['fns'])) {
    include 'form_new_style.inc.php';
    if (isset($_GET['fnsh'])) {
      include 'form_new_style.inc.php';
      include 'help/form_new_style_ADD.help.inc.php';
    }
  }
  if (isset($_GET['pt'])) {
    include 'form_add_prod_type.inc.php';
  }
  if (isset($_GET['fso'])) {
    include 'form_style_option.inc.php';
  }
  if (isset($_GET['sm'])) {
    include 'how_to_form_new_style_ADD.inc.php';
  }
}elseif (isset($_GET['sio'])) {
  include 'order_report_STYLE.inc.php';
  if (isset($_GET['fso'])) {
    include 'form_style_option.inc.php';
  }
  if (isset($_GET['sioh'])) {
    include 'help/order_report_STYLE.help.inc.php';
  }

}elseif (isset($_GET['sis'])) {
  include 'order_report_STYLE.inc.php';
}elseif (isset($_GET['sne'])) {
  // include 'report_style_select.inc.php';
  include 'edit_style.inc.php';
}elseif (isset($_GET['snf'])) {
  include 'form_new_style_first_time.inc.php';
}elseif (isset($_GET['sno'])) {
  echo "Review a selected style for orders and clients";
}elseif (isset($_GET['she'])) {
  include 'start_1.help.inc.php';
}elseif (isset($_GET['sh2'])) {
  include 'start_2.help.inc.php';
}elseif (isset($_GET['orn'])) {
  if (isset($_GET['hto'])) {
    // include 'how_to_styles.inc.php';
  }else {
    include 'form_new_orders_combined.inc.php';
    // include 'form_order.inc.php';
  }
  // include 'form_new_orders_combined.inc.php';
  // include 'form_new_orders.inc.php';
}elseif (isset($_GET['orp'])) {
  // echo "ADD ANOTHER ITEM TO THE ORDER";
  include 'form_new_orders_combined_plus.inc.php';
}

if (isset($_GET['hto'])) {
  include 'how_to_styles_index.inc.php';
}elseif (isset($_GET['sty'])) {
  include 'how_to_form_new_style_ADD.inc.php';
}elseif (isset($_GET['pro'])) {
  include 'how_to_product_type.inc.php';
}elseif (isset($_GET['col'])) {
  include 'how_to_collection.inc.php';
}elseif (isset($_GET['hts'])) {
  include 'how_to_season.inc.php';
}elseif (isset($_GET['test'])) {
  include 'test_DDM.inc.php';
}
