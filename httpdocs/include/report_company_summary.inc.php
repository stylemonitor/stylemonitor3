<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/report_company_summary.inc.php</b>";

include 'set_urlPage.inc.php';

$CID  = $_SESSION['CID'];
$ACID  = $_SESSION['ACID'];
$CIDn  = $_SESSION['CIDn'];

include 'from_UID_get_company_details.inc.php';
include 'from_CID_count_users.inc.php';
include 'from_ACID_count_divisions.inc.php';
include 'from_CID_count_sections.inc.php';
// removed count monitoring points 050721
// include 'from_CID_count_monitoring_points.inc.php';
include 'from_CID_count_product_type.inc.php';
include 'from_CID_count_styles.inc.php';
include 'from_CID_count_ALL_orders_ALL_clients_OC.inc.php';
include 'from_CID_count_company_partners_accepted.inc.php';
include 'from_CID_count_season.inc.php';
include 'from_CID_count_collection.inc.php';
include 'from_CID_count_partner_clients.inc.php';
include 'from_CID_count_partner_suppliers.inc.php';

if ($cDID == 0) { $cDID = 0;}else { $cDID = $cDID;}
if ($cSID == 0) { $cSID = 0;}else { $cSID = $cSID;}
if ($cPTID == 0) { $cPTID = 0;}else { $cPTID = $cPTID;}

// Associate companies
include 'from_CID_count_associates_clients.inc.php';
include 'from_CID_count_associates_supplier.inc.php';
if ($cACIDc == 0) { $cACIDc = 0;}else { $cACIDc = $cACIDc;}
if ($cACIDs == 0) { $cACIDs = 0;}else { $cACIDs = $cACIDs;}

?>
<div style="position:absolute; top:0%; height:3%; left:0%; width:100%; background-color:#5ba2bc;">
</div>

<!-- table for client information -->
<table style="position:absolute; top:3.5%; left:0%; width:100%;">
  <tr style="background-color:#5dffa2;">
    <?php
    if ($cACIDc == 0) { ?> <th>Associate Client</th> <?php }else { ?>
      <th><a style="text-decoration:none;"  href="associates.php?A&asr&AC">Associate Clients</a></th>
    <?php }
    if ($cACIDs == 0) { ?> <th>Associate Supplier</th> <?php }else { ?>
      <th><a style="text-decoration:none;"  href="associates.php?A&asr&AS">Associate Supplier</a></th>
    <?php } ?>
    <!-- <th style="text-decoration:none;">Partner Client</th>
    <th style="text-decoration:none;">Partner Supplier</th> -->
  </tr>
  <tr>
    <?php
    // echo "$cACID";
    if ($cACIDc == 0) { ?>
      <td style="font-size:150%;"><?php echo "$cACIDc"; ?></td>
    <?php }else { ?>
      <td><a style="text-decoration:none; font-size:150%;" href="associates.php?A&asr&AC"><?php echo "$cACIDc"; ?></a></td>
    <?php
    }
    if ($cACIDs == 0) { ?>
      <td style="font-size:150%;"><?php echo "$cACIDs"; ?></td>
    <?php }else { ?>
      <td><a style="text-decoration:none; font-size:150%;" href="associates.php?A&asr&AS"><?php echo "$cACIDs"; ?></a></td>
    <?php }
    ?>
    <!-- <td style="text-decoration:none; font-size:130%;"><?php echo $cCPIDcli ?></td>
    <td style="text-decoration:none; font-size:130%;"><?php echo $cCPIDsup ?></td> -->
  </tr>
</table>

<!-- table for our divisional information etc -->
<table  style="position:absolute; top:23.5%; left:0%; width:100%;">
  <tr style="height:10%; color:white; background-color:#525fff;">
    <th>Divisions</th>
    <!-- <th>Sections</th>
    <th>Monitoring Points</th> -->
  </tr>
  <tr>
    <?php
    if ($cDID == 0) {
      ?>
      <td style="font-size:150%;"><?php echo $cDID ?></th>
      <?php
    }else {
      ?>
      <td style="font-size:150%;"><a style="text-decoration:none;" href="company.php?C&sdr"><?php echo $cDID ?></a></th>
      <?php
    }
    // if ($cSCID == 0) {
    //   ?>
       <!-- <td style="font-size:150%;"><?php echo $cSCID ?></th> -->
       <?php
    // }else {
    //   ?>
       <!-- <td style="font-size:150%;"><a style="text-decoration:none;" href="company.php?C&sdr"><?php echo $cSCID ?></a></th> -->
       <?php
    // }
    ?>
    <!-- <td style="font-size:150%;"><?php echo $cMPID ?></td> -->
  </tr>
</table>

<!-- table for style information -->
<table  style ="position:absolute; top:43%; left:0%; width:100%;">
  <tr style="background-color:#ffb974;">
    <th>Styles</th>
    <th>Product types</th>
    <th>Seasons</th>
    <th>Collections</th>
  </tr>

  <tr>
    <?php
    if ($cPRID == 0) {
      ?>
      <td style="font-size:150%;"><?php echo $cPRID ?></td>
      <?php
    }else {
      ?>
      <td style="font-size:150%;"><a style="text-decoration:none;" href="styles.php?S&snr"><?php echo $cPRID ?></a></td>
      <?php
    }
    ?>
    <?php
    if ($cPTID == 0) {
      ?>
      <td style="font-size:150%;"><?php echo $cPTID ?></td>
      <?php
    }else {
      ?>
      <td style="font-size:150%;"><a style="text-decoration:none;" href="styles.php?S&spr"><?php echo $cPTID ?></a></td>
      <?php
    }

    if ($cSNID == 0) {
      ?>
      <td style="font-size:150%;"><?php echo $cSNID ?></td>
      <?php
    }else {
      $cSNID = $cSNID-1;
      ?>
      <td style="font-size:150%;"><a style="text-decoration:none;" href="styles.php?S&ser"><?php echo $cSNID ?></a></td>
      <?php
    }

    if ($cCLID == 0) {
      ?>
      <td style="font-size:150%;"><?php echo $cCLID ?></td>
      <?php
    }else {
      $cCLID = $cCLID-1;
      ?>
      <td style="font-size:150%;"><a style="text-decoration:none;" href="styles.php?S&cor"><?php echo $cCLID ?></a></td>
      <?php
    }
    ?>

  </tr>
</table>



<!-- table for order information -->
<table class="tcs" style ="position:absolute; top:65%; left:0%; width:100%;">
  <tr>
    <th></th>
    <th>Associate Sales</th>
    <th>Associate Purchases</th>
    <!-- <th>Partner Sales</th>
    <th>Partner Purchases</th> -->
  </tr>
  <tr>
    <th style="font-size:115%;">Orders</th>
    <td style="font-size:130%;"><a style="text-decoration:none;" href="<?php echo $urlPage ?>&aoo"><?php echo $asOIDo ?></a> / <a style="text-decoration:none;" href="<?php echo $urlPage ?>&com"><?php echo $asOIDc ?></a></td>
    <td style="font-size:130%;"><a style="text-decoration:none;" href="<?php echo $urlPage ?>&aoo"><?php echo $apOIDo ?></a> / <a style="text-decoration:none;" href="<?php echo $urlPage ?>&com"><?php echo $apOIDc ?></td>
    <td style="font-size:130%;"><?php echo $nosOP.' / '.$nosCL ?></td>
    <td style="font-size:130%;"><?php echo 'XXX' ?></td>
  </tr>
</table>


<form class="" action="include/report_company_summary_act.inc.php" method="post">
  <button class="entbtn" style="position:absolute; top:12%; height:4%; left:19%; width:10%;" type="submit" name="add_client">Add Client</button>
  <!-- <button class="entbtn" style="position:absolute; top:12%; height:4%; left:7%; width:10%;" type="submit" name="add_client">Add Client</button> -->
  <button class="entbtn" style="position:absolute; top:12%; height:4%; left:69%; width:10%;" type="submit" name="add_supplier">Add Supplier</button>
  <!-- <button class="entbtn" style="position:absolute; top:12%; height:4%; left:34%; width:10%;" type="submit" name="add_supplier">Add Supplier</button> -->
  <!-- <button class="entbtn" style="position:absolute; top:12%; height:4%; left:57%; width:15%;" type="submit" name="add_partner_client">Add Partner Client</button> -->
  <!-- <button class="entbtn" style="position:absolute; top:12%; height:4%; left:80%; width:15%;" type="submit" name="add_partner_supplier">Add Partner Supplier</button> -->
  <button class="entbtn" style="position:absolute; top:32%; height:4%; left:45%; width:10%;" type="submit" name="add_div">Add Division</button>
  <!-- <button class="entbtn" style="position:absolute; top:32%; height:4%; left:18.5%; width:10%;" type="submit" name="add_sec">Add Section</button> -->
  <button class="entbtn" style="position:absolute; top:52%; height:4%; left:3%; width:10%;" type="submit" name="add_sty">Add Style</button>
  <button class="entbtn" style="position:absolute; top:52%; height:4%; left:26%; width:15%;" type="submit" name="add_typ">Add Product Type</button>
  <button class="entbtn" style="position:absolute; top:52%; height:4%; left:54%; width:15%;" type="submit" name="add_sea">Add Season</button>
  <button class="entbtn" style="position:absolute; top:52%; height:4%; left:78.5%; width:15%;" type="submit" name="add_col">Add Collection</button>
</form>
