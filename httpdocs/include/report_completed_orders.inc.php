<?php
session_start();
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
include 'set_urlPage.inc.php';
include 'from_UID_get_user_details.inc.php';
// include 'page_selection_complete.inc.php';
// echo "include/report_completed_orders.inc.php";

$CID = $_SESSION['CID'];
$ACID = $_SESSION['ACID'];// echo "PRID brought forward : $PRID";
$td = date('U');

if (isset($_GET['ot'])) {
  $OTID = $_GET['ot'];
}else {
  $OTID = $salesPref;
}

if (isset($_GET['oc'])) {
  $oc = $_GET['oc'];
}else {
  $oc = 1;
}

include 'page_selection_header.inc.php';

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'Completed Orders';
$pddhbc = $comCol;
include 'page_description_date_header.inc.php';
include 'SM_colours.inc.php';

// Set the rows per page
$rpp = 10;

// Check the URL to see what page we are on
if (isset($_GET['pa'])) {
  $pa = $_GET['pa'];
}else{
  $pa = 0;
}

// Check which page we are on
if ($pa > 1) {
  $start = ($pa * $rpp) - $rpp;
}else {
  $start = 0;
}

// get a count of the number of orders that are closed
$sql = "SELECT COUNT(Distinct o.ID)
        FROM order_item oi
          , orders o
          , associate_companies ac
          , company c
        WHERE c.ID = 1
        AND (o.tCID = c.ID or o.fCID = c.ID)
        AND oi.OID = o.ID
        AND o.orComp = 1
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-oQ</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 1.9 is also 2
$PTV = ($cOIID / $rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV) - 1;
// echo '<br>The number of pages will be : '.$PTV;

// echo '<br>Start record is : '.$start;
?>
<table class="trs" style="position:absolute; top: 16%; width:100%;">
  <tr>
    <th style="width:5%;">Order</th>
    <th style="width:30%; text-align: left; text-indent:3%;">Company</th>
    <th style="width:41%; text-align: left; text-indent:3%;">Reference</th>
    <th style="width:12%;">Opened</th>
    <th style="width:12%;">Closed</th>
  </tr>

  <?php

  // the query for the table
  $sql = "SELECT o.ID AS OID
  -- sql is order_and order_items_status.sql
         , ac.ID AS ACID
         , ac.name AS ACIDn
         , acS.id AS sACID
         , acS.name AS sACIDn
         , oi.ID AS OIID
         , oiq.order_qty AS Qty
         , o.OTID AS OTID
         , o.orNos AS OIDnos
         , o.our_order_ref AS OIDref
         , o.OTID AS OIDtype
         , o.orComp AS OIDComp
         , oi.itComp AS OIIDComp
         , o.orDate AS OIDst
         , odd.del_date AS DUEDate
         , oi.itCompDate AS OIDelivered
  FROM order_item oi
       INNER JOIN orders o ON o.ID = oi.OID
       INNER JOIN orders_due_dates odd ON o.ID = odd.OID
       INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
       INNER JOIN company c ON o.fCID = c.ID
       INNER JOIN company cS ON o.tCID = cS.ID
       INNER JOIN associate_companies ac ON o.fACID = ac.ID
       INNER JOIN associate_companies acS ON o.tACID = acS.ID
       INNER JOIN division d ON o.fDID = d.ID
       INNER JOIN division dS ON o.tDID = dS.ID
  WHERE (o.fCID = ? OR o.tCID = ?)
    AND (o.fACID = ? OR o.tACID = ?)
    AND o.orComp = ?
    -- AND (oi.itComp = 7 OR oi.itComp = 9)
    AND o.OTID = ?
  -- AND (oi.itComp = 7 OR oi.itComp = 9)
  -- LIMIT ?,?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-oQ</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssssss", $CID, $CID, $ACID, $ACID, $oc, $OTID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      $OID = $row['OID'];
      $ACID = $row['ACID'];
      $ACIDn = $row['ACIDn'];
      $sACID = $row['sACID'];
      $sACIDn = $row['sACIDn'];
      $Qty = $row['Qty'];
      $OTID = $row['OTID'];
      $OIDnos = $row['OIDnos'];
      $OIDref = $row['OIDref'];
      $OIDst = $row['OIDst'];
      $OIDtype = $row['OIDtype'];
      $OIDComp = $row['OIDComp'];
      $OIIDComp = $row['OIIDComp'];
      $OIDst = $row['OIDst'];
      $DUEDate = $row['DUEDate'];
      $OIDelivered = $row['OIDelivered'];

      $OIDst = date('d-M-Y',$OIDst);
      $OIDelivered = date('d-M-Y',$OIDelivered);

      if ($OTID == 1) {
        $OTIDn = 'SALES';
        $bc = $salAssCol;
      }elseif ($OTID == 2) {
        $OTIDn = 'PURC';
        $bc = $purAssCol;
      }elseif ($OTID == 3) {
        $OTIDn = 'PSALEs';
        $bc = $salPrtCol;
      }elseif ($OTID == 4) {
        $OTIDn = 'PPURC';
        $bc = $purPrtCol;
      }

  ?>
    <tr>
      <td><a style="color:blue; text-decoration:none;" href="completed.php?E&tab=1&ot=<?php echo $OTID ?>&o=<?php echo $OID ?>"><?php echo $OIDnos ?></a></td>
      <td style="text-align:left; text-indent:2%;"><?php echo $sACIDn ?></td>
      <!-- <td style="text-align:left; text-indent:2%;"><a style="color:blue; text-decoration:none;" href="completed.php?E&tab=1&id=<?php echo $sACID ?>&oc=1"><?php echo $sACIDn ?></a></td> -->
      <td style="text-align:left; text-indent:2%;"><?php echo $OIDref ?></td>
      <td style="text-align:right; padding-right:1%;"><?php echo $OIDst ?></td>
      <td style="text-align:right; padding-right:1%;"><?php echo $OIDelivered ?></td>
    </tr>
    <?php
    }
  } ?>
</table>

<div style="position:absolute; bottom:5%; right:5%;font-size:200%">
  <?php for ($x = 1 ; $x <= $PTV ; $x++){
    if (isset($_GET['oi'])) {
      ?>
      <a style="color:black; text-decoration:none;" href='<?php echo $urlPage ?>&tab=<?php echo $tab ?>&oi=<?php echo $OIID ?>&pa=<?php echo $x ?>'><?php echo $x ?></a>
      <?php
    }elseif (isset($_GET['OI'])) {
      ?>
      <a style="color:black; text-decoration:none;" href='<?php echo $urlPage ?>&tab=<?php echo $tab ?>&OI=<?php echo $OIID ?>&pa=<?php echo $x ?>'><?php echo $x ?></a>
      <?php
    }
  } ?>
</div>

<?php
