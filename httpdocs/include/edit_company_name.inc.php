<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
echo "<b>include/edit_company_name.inc.php</b>";
// $CID   = $_SESSION['CID'];
// $CIDn  = $_SESSION['CIDn'];
// $ACID  = $_SESSION['ACID'];

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'Edit Company Name';
$pddhbc = '#fffddd';
// include 'page_description_date_header.inc.php';

// Check if the user has the authority to change the name or not - MUST be ADMIN (1)
include 'from_UID_get_user_details.inc.php';
if ($UIDv <> 1) {
  echo "You don't have permission to change the Company name";
}else {
  ?>
  <div class="overlay"></div>

  <div style="position:absolute; top:29%; height:21%; left:28%; width:44%; background-color:<?php echo $edtCol ?>; border:2px ridge grey; border-radius:5px; padding-top:1%;">
    <b style="font-size:125%; font-weight:bold;">Enter the new name of the company</b>
    <br>
    <p>Your SMIC will remain the same as before</p>
  </div>


  <form action="include/edit_company_name_act.inc.php" method="POST">
    <input class="edit_user" type="hidden" style="top:2%;" name="CID" value="<?php echo "$CID"; ?>">
    <input class="edit_user" type="hidden" style="top:22%;" name="CIDn" value="<?php echo "$CIDn"; ?>">
    <input class="edit_user" type="hidden" style="top:42%;" name="ACID" value="<?php echo "$ACID"; ?>">

    <!-- <input readonly class="edit_user" type="text" style="position:absolute; top:25%; left:2%; width:27%; background-color:#f1f1f1; border:none; text-align:right; z-index:1;" value="<?php echo "$CIDn"; ?>"> -->

    <input autofocus type="text" style="position:absolute; top:39%; height:4%; left:32%; width:36%; font-size:125%; z-index:1;" name="nCIDn" placeholder="<?php echo "$CIDn"; ?>" value="">
    <button class="entbtn" style="position:absolute; top:45%; left:35%; width:10%; background-color:<?php echo $fsvCol ?>; z-index:1;" type="submit" name="update_company_name">Update</button>
    <button class="entbtn" style="position:absolute; top:45%; left:55%; width:10%; background-color:<?php echo $fcnCol ?>; z-index:1;" type="submit" name="cancel_company_name">Cancel</button>
  </form>
  <?php
}
?>
