<?php
include 'dbconnect.inc.php';

include 'set_urlPage.inc.php';

// echo "include/report_partners_agreed_suppliers.inc.php";
include 'from_CID_count_partner_clients.inc.php'; // $cpCPID
include 'from_CID_count_Partner_Sales_open.inc.php'; // $cpCPID
include 'from_CPID_get_partners_CID.inc.php';

$CID = $_SESSION['CID'];
$pstat = 1;
// if ($CID <> $aCID) { $pCID = $aCID;}else { $pCID = $rCID;}

// $ACID = $_SESSION['ACID'];
$td = date('U');

if (isset($_GET['id'])) {
}else {
  $pddh = 'Partner Client Register';
  $pddhbc = '#fffddd';
  include 'page_description_date_header.inc.php';
}

// SETS the ORDER unless changed
if(isset($_GET['or'])){ $ord = $_GET['or']; }else{ $ord = 'c.name'; }
// SETS the SORT sequence at ASC unless changed to DESC
if(isset($_GET['so'])){ $sort = $_GET['so']; }else{ $sort = 'ASC'; }

// Check the URL to see what page we are on
if (isset($_GET['pa'])) { $pa = $_GET['pa']; }else{ $pa = 0; }

// Check which page we are on
if ($pa > 1) { $start = ($pa * $rpp) - $rpp; }else { $start = 0; }
// Set the rows per page
$rpp = 20;

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($ccCPID / $rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;

// echo '<br>Start record is : '.$start;
?>

<!-- Partners YOU have requested to join with you -->
<!-- <div class="cpsty" style="background-color:#5199fc"><?php echo "We have $cCPIDcli CLIENT Partner" ?></div> -->
<table class="trs" style="position:relative; top:10%;">
  <tr>
    <th style="width:40%;text-align:left; text-indent:2%;">Client</th>
    <!-- <th style="width:20%;text-align:left; text-indent:2%;">Supplier</th> -->
    <th style="width:20%;text-align:left; text-indent:2%;">Date Instigated</th>
    <!-- <th style="width:20%;text-align:left; text-indent:2%;">Order</th> -->
    <th style="width:10%;text-align:left; text-indent:2%;">Open</th>
    <th style="width:10%;text-align:left; text-indent:2%;">Closed</th>
  </tr>
  <?php

  $sql = "SELECT cp.inputtime AS REQUESTED
  -- SQL is Pending_Partners_list.sql
         , cp.ID AS CPID
         , cp.insCID AS INITIATOR
         , cp.req_CID AS CLIENT_ID
         , rc.name AS CLIENT
         , cp.acc_CID AS SUPPLIER_ID
         , ac.name AS SUPPLIER
         , IF(cp.active=0,'PENDING',IF(cp.active=1,'ACCEPTED','REJECTED')) AS STATUS
         , IF(cp.active=0,'PENDING',IF(cp.active=1,cp.accdate,'REJECTED')) AS ACCEPTED
  FROM company_partnerships cp
       INNER JOIN company rc ON cp.req_CID = rc.ID
       INNER JOIN company ac ON cp.acc_CID = ac.ID
  WHERE cp.active = ?
    AND (cp.acc_CID = ?)
    AND (cp.req_CID != cp.acc_CID)
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-rpc</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $pstat, $CID);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
  }
  // if ($sort==DESC) { $sort = ASC } else { $sort = DESC }
  // $sort == DESC ? $sort = ASC : $sort = DESC;
  while($row = mysqli_fetch_array($res)){
    $CPID = $row['CPID'];
    $CIDi = $row['INITIATOR'];
    $CIDc = $row['CLIENT_ID'];
    $CIDcN = $row['CLIENT'];
    $CIDs = $row['SUPPLIER_ID'];
    $CIDsN = $row['SUPPLIER'];
    // $CPIDrel = $row['CPIDrel'];
    $CPIDt = $row['REQUESTED'];

    $CPIDt = date('d-M-Y', $CPIDt);

    if (($CID == $CIDi) && ($CIDi == $CIDc)) {
      $pCIDcN = $CIDsN;
      $pCIDsN = $CIDcN;
    }else {
      $pCIDcN = $CIDcN;
      $pCIDsN = $CIDsN;
    }
    ?>

    <tr>
      <td style="width:20%; text-align:left; text-indent:3%;"><a class="hreflink" href="partners.php?P&pac&id=<?php echo $CIDc ?>"><?php echo "$pCIDcN"; ?></a></td>
      <!-- <td style="width:20%; text-align:left; text-indent:3%;"><?php echo "$pCIDsN"; ?></td> -->
      <td style="width:20%; text-align:left; text-indent:3%;"><?php echo "$CPIDt"; ?></td>
      <td style="width:10%; text-align:left; text-indent:3%;"><a style="color:blue; text-decoration:none;" href="partners.php?P&pac&id=<?php echo $CIDc ?>"><?php echo $CpsOIDo ?></a></td>
      <td style="width:10%; text-align:left; text-indent:3%;">Closed</td>

    </tr>
    <?php
  }
  ?>
</table>

<div style="position:absolute; bottom:5%; left:10%;">

  <?php for ($x = 1 ; $x <= $PTV ; $x++){
    echo "<a href='?H&$sp&pa=$x'>  $x  </a>";
  }

  ?>
</div>
