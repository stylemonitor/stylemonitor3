<?php
include 'dbconnect.inc.php';
// echo  "<br>input_user_section_details.inc.php";

$sql = "INSERT INTO section
          (DID, UID, name, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudd";
}else {
  mysqli_stmt_bind_param($stmt,"ssss", $DID, $UID, $UIDc, $td);
  mysqli_stmt_execute($stmt);
}

// Get the DID
$sql = "SELECT ID as SID
        FROM section
        WHERE DID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudd";
}else {
  mysqli_stmt_bind_param($stmt,"s", $DID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $SID  = $row['SID'];
}
// echo  "<br>Section ID == $SID";
