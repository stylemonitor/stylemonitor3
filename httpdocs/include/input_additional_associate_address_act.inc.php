<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/input_additional_associate_address_act.inc.php</b>";

if (!isset($_POST['addADDRESS']) && !isset($_POST['cancel'])) {
  // echo "<br>WRONG METHOD USED";
}elseif (isset($_POST['cancel'])) {
  $ACID = $_POST['ACID'];
  $CTID = $_POST['ct'];
  header("Location:../associates.php?A&asf&id=$ACID&ct=$CTID");
  exit();
}elseif (isset($_POST['addADDRESS'])) {
  // echo "<br>Creat a new company name, record and replace the old one";

  $UID = $_SESSION['UID'];
  $td = date('U');

  // Get the data from the form
  $nCIDn = $_POST['nCIDn'];
  $CIDn = $_POST['CIDn'];
  $CID = $_POST['CID'];
  $ACID = $_POST['ACID'];

  include 'from_CID_get_company_address.inc.php';

  $addn = $_POST['addn'];
  $add1 = $_POST['add1'];
  $add2 = $_POST['add2'];
  $city = $_POST['city'];
  $pcode = $_POST['pcode'];
  $county = $_POST['county'];
  $CYID = $_POST['CYID'];
  $TZID = $_POST['TZID'];
  $tel = $_POST['tel'];

  if (empty($add1)) {$add1 = 'Address line 1';}
  if (empty($add2)) {$add2 = 'Address line 2';}
  if (empty($city)) {$city = 'Town / City';}
  if (empty($pcode)) {$pcode = 'Post Code / Zip Code';}
  if (empty($county)) {$county = 'County / State';}
  if (empty($tel)) {$tel = 'Main phone number';}

  // echo "<br>New address name $addn";
  // echo "<br>Associate company ID is $ACID";
  // echo "<br>Address Line 1 is $add1";
  // echo "<br>Address Line 2 is $add2";
  // echo "<br>Town/City is $city";
  // echo "<br>County/State is $county";
  // echo "<br>Country is $CYID";
  // echo "<br>TimeZone is $TZID";
  // echo "<br>Telephone is $tel";

  // Check if the address already exists
  include 'check_if_address_is_used.inc.php';

  if ($AID <> 0) {
    // Check if the address already exists for the company
    include 'check_if_address_is_used_for_company.inc.php';
    if ($CAAID <> 0) {
      // echo "This address is already in use with this company";
      header("Location:../associates.php?A&asf&id=$ACID&ct=$CTID&add=1");
      exit();
    }
  }



  // add to the address
  $sql = "INSERT INTO addresses
            (CYID, TID, UID, addn, add1, add2, city, pcode, county, tel, inputtime)
          VALUES
          (?,?,?,?,?,?,?,?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt,$sql)) {
    echo "FAIL-iaaaa";
  }else {
    mysqli_stmt_bind_param($stmt,"sssssssssss", $CYID, $TZID, $UID, $addn, $add1, $add2, $city, $pcode, $county, $tel, $td);
    mysqli_stmt_execute($stmt);
  }

  // get the new address AID
  $sql = "SELECT ID as AID
          FROM addresses
          WHERE UID = ?
          AND inputtime = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt,$sql)) {
    echo "FAIL-icdu2";
  }else {
    mysqli_stmt_bind_param($stmt,"ss", $UID, $td);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_assoc($result);
    $AID  = $row['AID'];
  }

  // set the associate company to the address
  $sql = "INSERT INTO company_associates_address
            (ACID, AID, UID, inputtime)
          VALUES
            (?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt,$sql)) {
    echo "FAIL-icdu3";
  }else {
    mysqli_stmt_bind_param($stmt,"ssss", $ACID, $AID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }
  header("Location:../associates.php?A&asf&id=$ACID&ct=$CTID");
  exit();
}
