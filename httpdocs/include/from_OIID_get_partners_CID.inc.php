<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_OIID_get_partners_CID.inc.php</b>";
if (isset($_GET['oi'])) {
  $OIID = $_GET['oi'];
}

$sql = "SELECT cp.req_CID as cCID
          , cp.acc_CID as sCID
        FROM company_partnerships cp
        , orders o
        , order_item oi
        WHERE oi.ID = ?
        AND oi.OID = o.ID
        AND o.CPID = cp.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fcgpc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OIID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $cCID = $row['cCID'];
  $sCID = $row['sCID'];
}
