<?php
echo "<br><b>movement_table_item_update.inc.php</b>";

if (isset($_GET['ot'])) {
  $OTID = $_GET['ot'];
  $sp = $_GET['sp'];
  $tab = $_GET['tab'];
}
if (isset($_GET['o'])) {
  $OID = $_GET['o'];
  // echo "o=$OID";
}
if (isset($_GET['OI'])) {
  $OIID = $_GET['OI'];
  // echo "::OI=$OIID";
}

// AWAITING
if ($Awaiting == 0) {
  ?>
  <td style="color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>; text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.5%;"><?php echo $Awaiting ?></td>
  <?php
}else {
  ?>
    <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"><?php echo $Awaiting ?></td>
  <?php
}

// STAGE1
if (($Awaiting == 0) && ($Stage1 == 0 )) {
  ?>
  <td style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.5%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage1 ?></td>
  <?php
}else {
  if ($Stage1 == 0) {
    ?>
    <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"></td>
    <?php
  }else {
    ?>
    <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"><?php echo $Stage1 ?></td>
    <?php
  }
}

// STAGE 2
if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 )) {
  ?>
  <td style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.5%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage2 ?></td>
  <?php
}else {
  if ($Stage2 == 0) {
    ?>
    <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"></td>
    <?php
  }else {
    ?>
    <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"><?php echo $Stage2 ?></td>
    <?php
  }
}

// STAGE 3
if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 )) {
  ?>
  <td style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.5%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage3 ?></td>
  <?php
}else {
  if ($Stage3 == 0) {
    ?>
    <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"></td>
    <?php
  }else {
    ?>
    <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"><?php echo $Stage3 ?></td>
    <?php
  }
}

// STAGE4
if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 ) && ($Stage4 == 0 )) {
  ?>
  <td style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.5%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage4 ?></td>
  <?php
}else {
  if ($Stage4 == 0) {
    ?>
    <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"></td>
    <?php
  }else {
    ?>
    <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"><?php echo $Stage4 ?></td>
    <?php
  }
}

// STAGE5
if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 ) && ($Stage4 == 0 ) && ($Stage5 == 0 )) {
  ?>
  <td style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.5%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage5 ?></td>
  <?php
}else {
  if ($Stage5 == 0) {
    ?>
    <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"></td>
    <?php
  }else {
    ?>
    <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"><?php echo $Stage5 ?></td>
    <?php
  }
}

// WAREHOUSE
if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 ) && ($Stage4 == 0 ) && ($Stage5 == 0 ) && ($Warehouse == 0 )) {
  ?>
  <td style="text-align: right; border-right:thin solid <?php echo $whsCol ?>; padding-right:0.5%; color:<?php echo $whsCol ?>; background-color:<?php echo $whsCol ?>;"><?php echo $Warehouse ?></td>
  <?php
}else {
  if ($Warehouse == 0) {
    ?>
    <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"></td>
    <?php
  }else {
    ?>
    <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"><?php echo $Warehouse ?></td>
    <?php
  }
}

// Despatched
if ($CLIENT == 0) {
  ?>
  <td style="border-right:thin solid grey;"></td>
  <?php
}else {
  ?>
  <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"><b><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&OI=<?php echo $OIID ?>&mp=7&val=<?php echo $CLIENT ?>"><?php echo $CLIENT ?></a></b></td>
  <?php
}
