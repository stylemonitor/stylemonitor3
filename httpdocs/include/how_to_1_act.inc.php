<?php
include 'dbconnect.inc.php';
// echo "<b>include/form_new_company_user_act.inc.php</b>";

if (!isset($_POST['stop_ht1'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['stop_ht1'])) {
  $UID = $_SESSION['UID'];
  $sql = "UPDATE users
          SET iscreen = 1
          WHERE ID = ?
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-ht1a</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UID);
    mysqli_stmt_execute($stmt);
  }
  // echo "<br>Return to the Users page";
  header("Location:../how_to.php?W&ht1");
  exit();
}
