<?php
include 'dbconnect.inc.php';
// echo  "<br>input_user_division_details.inc.php";
// echo  "<br>ACID : $ACID";

$sql = "INSERT INTO division
          (ACID, UID, name, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudd";
}else {
  mysqli_stmt_bind_param($stmt,"ssss", $ACID, $UID, $UIDc, $td);
  mysqli_stmt_execute($stmt);
}

// Get the DID
$sql = "SELECT ID as DID
        FROM division
        WHERE ACID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudd";
}else {
  mysqli_stmt_bind_param($stmt,"s", $ACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $DID  = $row['DID'];
}
// echo  "<br>Division ID == $DID";
