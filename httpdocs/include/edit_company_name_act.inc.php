<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/edit_company_name_act.inc.php</b>";

if (!isset($_POST['update_company_name']) && !isset($_POST['cancel_company_name'])) {
  // echo "<br>WRONG METHOD USED";
}elseif (isset($_POST['cancel_company_name'])) {
  header("Location:../company.php?C&coi");
  exit();
}elseif (isset($_POST['update_company_name'])) {
  // echo "<br>Creat a new company name, record and replace the old one";

  // Get the data from the form
  $nCIDn = $_POST['nCIDn'];
  $CIDn = $_POST['CIDn'];
  $CID = $_POST['CID'];
  $ACID = $_POST['ACID'];

  $UID = $_SESSION['UID'];
  $td = date('U');

  // echo "<br>New company/associate company name is $nCIDn";
  // echo "<br>Old company/associate company name is $CIDn";
  // echo "<br>Company ID is $CID";
  // echo "<br>Associate company ID is $ACID";

  if (empty($nCIDn)) {
    $nCIDn = $CIDn;
  }

  // mysqli_begin_transaction($mysqli);
  // try {
    // to

    $sql = "INSERT INTO log_company_name
              (CID, UID, ori_name, inputtime)
            VALUES
              (?,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-ecna</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $CID, $UID, $CIDn, $td);
      mysqli_stmt_execute($stmt);
    }

    $sql = "INSERT INTO log_assoc_co_name
              (ACID, ori_name, UID, inputtime)
            VALUES
              (?,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-ecna1</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $ACID, $CIDn, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    $sql = "UPDATE company
            SET name = ?
            WHERE name = ?
            AND ID = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-ecna2</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "sss", $nCIDn, $CIDn, $CID );
      mysqli_stmt_execute($stmt);
    }

    $sql = "UPDATE associate_companies
            SET name = ?
            WHERE name = ?
            AND CID = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-ecna3</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "sss", $nCIDn, $CIDn, $CID);
      mysqli_stmt_execute($stmt);
    }

    // get the original address name
    $sql = "SELECT ID as AID
              , addn as AIDn
            FROM addresses
            WHERE ID = ?
;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-ecna3</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "s", $CID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $AID = $row['AID'];
      $AIDn = $row['AIDn'];
    }
    // echo "AID = $AID :: AIDn = $AIDn";

    // update the log_addresses with the old detail
    $sql = "INSERT INTO log_addresses
              (UID, AID, ori_addn, inputtime)
            VALUES
              (?,?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-ecna3</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $UID, $AID, $AIDn, $td);
      mysqli_stmt_execute($stmt);
    }

    // iput the new company address name into addresses
    $nCIDn = "$nCIDn HQ";
    // echo "<br>nCIDn = $nCIDn";

    $sql = "UPDATE addresses
            SET addn = ?
            WHERE ID = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-ecna3</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $nCIDn, $CID);
      mysqli_stmt_execute($stmt);
    }

    $_SESSION['CIDn'] = $nCIDn;

  // } catch (mysqli_sql_exception $exception) {
    // mysqli_rollback($mysqli);

    // throw $exception;
  // }

  header("Location:../company.php?C&coi");
  exit();
}
