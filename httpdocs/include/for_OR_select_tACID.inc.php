<?php
session_start();
include 'dbconnect.inc.php';
//echo '<b>include/for_OR_select_tACID.inc.php</b>';

$sql = "SELECT  ac.ID as ACID
	        , ac.name as ACIDn
        FROM  associate_companies ac
            , company c
        WHERE c.ID = ?
        AND ac.CID = c.ID
				AND ac.CTID IN (4,6,7)
				ORDER BY ac.name
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br><b>FAIL-fcsacs</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $tCID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
  	$tACID = $row['ACID'];
  	$tACIDn = $row['ACIDn'];
		echo '<option value="'.$tACID.'">'.$tACID.':'.$tACIDn.'</option>';
	}
}
?>
