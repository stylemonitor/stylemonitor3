<?php
include 'include/dbconnect.inc.php';
// echo "include/from_ACID_count_associate_company_product_types.inc.php";
// include 'from_CID_get_min_division.inc.php';
// $CID = $_SESSION['CID'];

if (isset($_GET['id'])) { $ACID = $_GET['id'];}

$sql = "SELECT COUNT(acpt.ID) AS ACPTID
        FROM associate_companies ac
            , associate_company_product_types acpt
        WHERE ac.ID = ?
        AND acpt.ACID = ac.ID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-facapt</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $ACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $ACPTID = $row['ACPTID'];
  // reduced count by 1 as SELECT DIVISION is NOT a division
  // Only have divisions after the first one I give them!!!!!!
}

if ($ACPTID == 1) {
  include 'from_ACID_get_ONLY_PRID.inc.php';
  // echo "<br>Company has just one product";
}else {
  // echo "<br>The company has more than one product";
  $PTIDref = "Multiple";
}
