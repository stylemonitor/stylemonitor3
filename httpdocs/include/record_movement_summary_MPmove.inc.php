<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/R_on_sum_MP1d.inc.php</b";

$sql = "SELECT SUM(opm.omQty*oimr.numVal) as sQty1
        FROM order_placed op
          , order_placed_move opm
          , order_item_movement_reason oimr
          , order_item oi
        WHERE op.ID = ?
        AND op.OIID = oi.ID
        AND oimr.ID IN (3, 13)
        AND opm.OPID = op.ID
        AND opm.OIMRID = oimr.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rosmp1d</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OPID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $sMP1 = $row['sQty1'];
  if (empty($sMP1)) { $sMP1 = 0;}else { $sMP1 = $sMP1;}

  global $sMP1;
}
