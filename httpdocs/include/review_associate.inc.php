<?php
include 'dbconnect.inc.php';
// echo "<br><b>review_associate.inc.php</b>";

$CID = $_SESSION['CID'];
$td = date('U');


// Check the URL to see what page we are on
if (isset($_GET['p'])) { $p = $_GET['p']; }else{ $p = 0; }
if (isset($_GET['id'])) { $ACID = $_GET['id'];}
if (isset($_GET['ct'])) { $CTID = $_GET['ct'];}

// if ($CTID == 4) {$hbc = 'f7cc6c';}
// elseif ($CTID == 5) {$hbc = 'bbbbff';}

// Set the rows per page
$r = 20;
// Check which page we are on
if ($p > 1) { $start = ($p * $r) - $r; }else { $start = 0; }

if(isset($_GET['ord'])){ $ord = $_GET['ord']; }else{ $ord = 'oidd.item_del_date'; }
if(isset($_GET['sort'])){ $sort = $_GET['sort']; }else{ $sort = 'ASC'; }

// echo "<br>Company ID : $CID";
// echo "<br>Order status : $OC";
// echo "<br>Today : $td";
// echo "<br>Start day : $sd";
// echo "<br>End day : $ed";
// echo "<br>Count_orders - due date source : $dr";
// echo "<br>Associate Company ID is (ACID) : $ACID";

include 'SM_colours.inc.php';
include 'from_ACID_count_associate_divisions.inc.php';
include 'from_ACID_count_associate_company_product_types.inc.php';
include 'from_ACID_count_addresses.inc.php';
include 'from_ACID_count_associate_orders_open.inc.php';
include 'from_ACID_count_associate_orders_closed.inc.php';
include 'from_ACID_get_associate_company_details.inc.php';
include 'from_ACID_get_associate_company_address.inc.php';
include 'from_CID_count_product_type.inc.php';
include 'from_ACID_count_associate_product_types.inc.php';

$pddh = $ACIDn;

// include 'page_description_date_header.inc.php';

?>
<table class="trs" style="position:absolute; top:9%; left:2%;width:45%;">
  <caption>Main Company Address</caption>
  <tr>
    <th style="width:35%; text-align:right;padding-right:2%;">Name</th>
    <?php
    if ($addn == "TBC") {?>
      <td style="text-align:left; text-indent:2%; color:grey;"><?php echo "$addn"; ?></td>
      <?php
    }else {?>
      <td style="text-align:left; text-indent:2%;"><?php echo "$addn"; ?></td>
      <?php
    } ?>
  </tr>
  <tr>
    <th style="width:35%; text-align:right;padding-right:2%;">Address</th>
    <?php
    if ($add1 == "Address line 1") {?>
      <td style="text-align:left; text-indent:2%; color:grey;"><?php echo "$add1"; ?></td>
      <?php
    }else {?>
      <td style="text-align:left; text-indent:2%;"><?php echo "$add1"; ?></td>
      <?php
    } ?>
  </tr>
  <tr>
    <th style="width:35%; text-align:right;padding-right:2%;"></th>
    <?php
    if ($add2 == "Address line 2") {?>
      <td style="text-align:left; text-indent:2%; color:grey;"><?php echo "$add2"; ?></td>
      <?php
    }else {?>
      <td style="text-align:left; text-indent:2%;"><?php echo "$add2"; ?></td>
      <?php
    } ?>
  </tr><tr>
    <th style="width:35%; text-align:right;padding-right:2%;">City/Town</th>
    <?php
    if ($city == "Town / City") {?>
      <td style="text-align:left; text-indent:2%; color:grey;"><?php echo "$city"; ?></td>
      <?php
    }else {?>
      <td style="text-align:left; text-indent:2%;"><?php echo "$city"; ?></td>
      <?php
    } ?>
  </tr><tr>
    <th style="width:35%; text-align:right;padding-right:2%;">Post Code/Zip Code</th>
    <?php
    if ($pcode == "Post Code / Zip Code") {?>
      <td style="text-align:left; text-indent:2%; color:grey;"><?php echo "$pcode"; ?></td>
      <?php
    }else {?>
      <td style="text-align:left; text-indent:2%;"><?php echo "$pcode"; ?></td>
      <?php
    } ?>
  </tr><tr>
    <th style="width:35%; text-align:right;padding-right:2%;">County/State</th>
    <?php
    if ($county == "County / State") {?>
      <td style="text-align:left; text-indent:2%; color:grey;"><?php echo "$county"; ?></td>
      <?php
    }else {?>
      <td style="text-align:left; text-indent:2%;"><?php echo "$county"; ?></td>
      <?php
    } ?>
  </tr><tr>
    <th style="width:35%; text-align:right;padding-right:2%;">Telephone</th>
    <?php
    if ($tel == "Main phone number") {?>
      <td style="text-align:left; text-indent:2%; color:grey;"><?php echo "$tel"; ?></td>
      <?php
    }else {?>
      <td style="text-align:left; text-indent:2%;"><?php echo "$tel"; ?></td>
      <?php
    } ?>
</table>


<!-- __________________________________________________________________________________________ -->
<table class="trs" style="position:absolute; top:12.5%; left:50%; width:35%; font-size:120%;">
  <caption>Company Summary</caption>
  <!-- count divisions if 1 then don't show -->
  <?php
  if ($cDID > 0) {
    ?>
    <tr>
    <th >Divisions</th>
    <td style="background-color: pink;"><?php echo $cDID ?></td>
    <td ><a href="#"></a> Add</td>
    <td >Review</td>
    </tr>
    <?php
  }else {
  }
  ?>
  <tr>
    <th >Products</th>
    <td style="background-color: pink;"><?php echo $ACPTID ?></td>
    <?php
    if ($cPTID == $acPTID) {
      ?>
      <td ></td>
      <?php
    }else {
      // should not be able to add IF the number of product types equals the number already selected
      ?>
      <td ><a style="color:blue; text-decoration:none;" href="associates.php?A&asf&id=<?php echo $ACID ?>&pta">Add</a></td>
      <?php
    }
    if (isset($_GET['rpt'])) {
      ?>
      <td style="background-color:grey;"><a style="color:blue; text-decoration:none;" href="associates.php?A&asf&id=<?php echo $ACID ?>">Close</a></td>
      <?php
    }else {
      ?>
      <td ><a style="color:blue; text-decoration:none;" href="styles.php?S&spt">Review</a></td>
      <?php
    }
    ?>
  </tr>
  <?php
  if ($cOIDo > 0) {
    ?>
    <tr>
      <th >Orders - Open</th>
      <td style="background-color: pink;"><?php echo $cOIDo ?></td>
      <td></td>
      <td >Review</td>
    </tr>
    <?php
  }else {
  }
  if ($caaID > 0) {
    ?>
    <tr>
      <th >Addresses</th>
      <td style="background-color: pink;"><?php echo $caaID ?></td>
      <td style=""><a style="color:blue; text-decoration:none;" href="associates.php?A&asf&id=<?php echo $ACID ?>&add">Add</a></td>
      <?php
      if (isset($_GET['rad'])) {
        ?>
        <td style="background-color:grey;"><a style="color:blue; text-decoration:none;" href="associates.php?A&asf&id=<?php echo $ACID ?>">Close</a></td>
        <?php
      }else {
        ?>
        <td ><a style="color:blue; text-decoration:none;" href="associates.php?A&asf&id=<?php echo $ACID ?>&rad">Review</a></td>
        <?php
      }
      ?>
    </tr>
    <?php
  }else {
  }

  if ($cOIDc > 1) {
    ?>
    <tr>
      <th >Orders - Closed</th>
      <td style="background-color: pink;"><?php echo $cOIDc ?></td>
      <td ></td>
      <td >Review</td>
    </tr>
    <?php
  }else {
  }
  ?>
</table>

<div style="position:absolute; top:8%; left:40%; width:45%; font-size:150%;">
  <?php
  if ($ACPTID == 1) {
    ?>
    Product Type : <a style="color:blue; text-decoration:none; "href="styles.php?S&sis&s=<?php echo $PTID ?>"><?php echo "$PTIDref" ?></a>
    <?php
  }else {
    if (isset($_GET['rpt'])) {
      ?>
      Product Types : <a style="color:blue; text-decoration:none; "href="associates.php?A&asf&id=<?php echo $ACID ?>">Close</a>
      <?php
    }else {
      ?>
      Product Types : <a style="color:blue; text-decoration:none; "href="associates.php?A&asf&id=<?php echo $ACID ?>&rpt"><?php echo "$PTIDref" ?></a>
      <?php
    }
    ?>
    <?php
  }
  ?>
</div>

<?php
if (isset($_GET['add']) || (isset($_GET['pta']))) {
}else {
  ?>
  <form  action="include/review_associate_company_details_act.inc.php" method="POST">
    <input type="hidden" name="id" value="<?php echo $ACID ?>">
    <input type="hidden" name="ct" value="<?php echo $CTID ?>">
    <input type="hidden" name="AID" value="<?php echo $AID ?>">
    <button class="entbtn" style="position:absolute; top:10%; left:88%; width:10%; background-color:<?php echo $edtCol ?>;" type="submit" name="edit_assoc_co_name">Edit name</button>
    <button class="entbtn" style="position:absolute; top:15%; left:88%; width:10%; background-color:<?php echo $edtCol ?>;" type="submit" name="edit_assoc_co_address">Edit address</button>
    <button class="entbtn" style="position:absolute; top:20%; left:88%; width:10%;" type="submit" name="addADD">Add Address</button>
    <button class="entbtn" style="position:absolute; top:25%; left:88%; width:10%;" type="submit" name="addPT">Add Product</button>
  </form>
  <?php
}
 ?>
