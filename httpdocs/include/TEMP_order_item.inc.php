<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/TEMP_order_item.inc.php";

// get the next item number
$sql = "SELECT COUNT(ID) as ID
        FROM TEMP_order_item_record
        WHERE UID = ?
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br>FAIL-Troi';
}else {
  mysqli_stmt_bind_param($stmt, "s", $UID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($res);
  $cID = $row['ID'];
}

if ($cID == 0) {$cID = 1;
}else {
  $cID = $cID+1;
}

?>

<!-- Detail information about the item on order -->

<div style="position:absolute; top:49%; height:3%; left:51.5%; width:20%; text-align: left; text-indent: 2%;">
  <!-- <div style="position:absolute; top:49.5%; height:3%; left:3.3%; width:33%; "> -->
  Style Reference
</div>

<!-- A description of the item -->
<div style="position:absolute; top:49%; height:3%; left:5%; width:34%; text-align:left; text-indent:2%;">Item Description
  <!-- <div style="position:absolute; top:49.5%; height:3%; left:33.9%; width:40%; ">Order Item Description -->
</div>
  <?php
 ?>

<!-- The quantity of the item -->
<div style="position:absolute; top:49%; height:3%; left:72.5%; width:6%; text-align:center;">Qty
</div>

<!-- The item due by date if different from the order due by date -->
<!-- IF left blank will default to the ORDER DUE DATE -->
<div style="position:absolute; top:49%; height:3%; left:81.5%; width: 18.5%; text-align:left;">Due Date
</div>
<div style="position:absolute; top:49%; left: 1%; width:3%;">Item</div>

<form class="" action="include/TEMP_order_item_act.inc.php" method="post">
  <input="text" name="a" value="<?php echo $our_order_ref ?>">
  <input readonly type="text" style="position:absolute; top:51.5%; height:3%; left:1%; width:3%; text-align: center;" name="orItemNos" value="<?php echo "$cID"; ?>">
  <?php
  if (isset($_GET['des'])) {
    // get the clients item description
    $cid = $_GET['des'];
    ?>
    <input required autofocus type="text" class ="input_data" style="position:absolute; top:51%; height:3%; left:5%; width:45%; text-align:left;" type="text" name="their_item_ref" placeholder="Item Description"  value="<?php echo $cid ?>">
    <?php
  }else {
    ?>
    <input required autofocus type="text" class ="input_data" style="position:absolute; top:51%; height:3%; left:5%; width:45%; text-align:left;" type="text" name="their_item_ref" placeholder="Item Description">
    <?php
  }
  ?>

  <select required id="tPRID" name="tPRID" style="position:absolute; top:51.5%; height:3.8%; left:51.5%; width:20%;">
    <option value="">Select Style</option>
    <?php
    include 'from_pCID_select_TO_product_ref.inc.php';
    ?>
  </select>
  <input required class ="input_data"  style="position:absolute; top:51%; height:3%; left:72.5%; width:5%; padding-right:0.5%;" type="text" name="orQTY" placeholder="Qty">
  <input class ="sel_dropdown" style="position:absolute; top:51%; height:3%; left:79.5%; width:10%; text-align:center;" type="date" name="itemDelDate" placeholder="Item Del Date" >

  <button class="entbtn" type="submit" style="position:absolute; top:51.8%; height:3%; left:92%; width:6%;"name="addItem">Add Item</button>

</form>
