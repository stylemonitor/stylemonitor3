<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_count_ALL_orders_ALL_clients_OC.inc.php</b>";
$CID = $_SESSION['CID'];
$sql = "SELECT IF (CALC1.asOIDo IS NOT NULL,CALC1.asOIDo,0) AS asOIDo

       , IF (CALC2.asOIDc IS NOT NULL,CALC2.asOIDc,0) AS asOIDc

       , IF (CALC3.apOIDo IS NOT NULL,CALC3.apOIDo,0) AS apOIDo

       , IF (CALC4.apOIDc IS NOT NULL,CALC4.apOIDc,0) AS apOIDc

FROM

(

SELECT count(o.id) as asOIDo

       , c.ID

FROM orders o

     , company c

     , associate_companies ac

     , division d

WHERE c.ID = ?

  AND ac.CID = c.ID

  AND d.ACID = ac.ID

  AND o.DID = d.ID

  AND o.orComp in (0)

  AND o.OTID = 1

  AND ac.CTID in (4,6,7)

) CALC1

, (SELECT count(o.id) as asOIDc

        , c.ID

  FROM orders o

       , company c

       , associate_companies ac

       , division d

  WHERE c.ID = ?

    AND ac.CID = c.ID

    AND d.ACID = ac.ID

    AND o.DID = d.ID

    AND o.orComp in (1)

    AND o.OTID = 1

    AND ac.CTID in (4,6,7)

  ) CALC2

, (SELECT count(o.id) as apOIDo

        , c.id

   FROM orders o

        , company c

        , associate_companies ac

        , division d

   WHERE c.ID = ?

     AND ac.CID = c.ID

     AND d.ACID = ac.ID

     AND o.DID = d.ID

     AND o.orComp in (0)

     AND o.OTID = 2

     AND ac.CTID in (4,6,7)

   ) CALC3

, (SELECT count(o.id) as apOIDc

        , c.id

   FROM orders o

        , company c

        , associate_companies ac

        , division d

   WHERE c.ID = ?

     AND ac.CID = c.ID

     AND d.ACID = ac.ID

     AND o.DID = d.ID

     AND o.orComp in (1)

     AND o.OTID = 2

     AND ac.CTID in (4,6,7)

   ) CALC4

WHERE CALC1.ID = CALC2.ID

  AND CALC1.ID = CALC3.ID

  AND CALC1.ID = CALC4.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccaoacop</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $CID , $CID, $CID , $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $asOIDo = $row['asOIDo'];
  $asOIDc = $row['asOIDc'];
  $apOIDo = $row['apOIDo'];
  $apOIDc = $row['apOIDc'];
}
