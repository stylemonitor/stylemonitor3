<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/edit_product_type_act.inc.php</b>";

$CID = $_SESSION['CID'];

if (!isset($_POST['update_npt']) && !isset($_POST['cancel_npt'])) {
  echo "WRONG ACCESS METHOD epta";
}elseif (isset($_POST['cancel_npt'])) {
  header("Location:../styles.php?S&spt");
  exit();
}elseif (isset($_POST['update_npt'])) {
  $UID = $_SESSION['UID'];
  $td  = date('U');

  // get the information from the form
  $PTID    = $_POST['PTID'];
  $PTIDsc = $_POST['PTIDsc'];
  $nPTIDsc = $_POST['nPTIDsc'];
  $PTIDn  = $_POST['PTIDn'];
  $nPTIDn  = $_POST['nPTIDn'];
}

if (empty($nPTIDsc) && empty($nPTIDn)) {
  // echo "NO CHANGES MADE";
  header("Location:../styles.php?S&spt");
  exit();
}elseif (empty($nPTIDsc) || ($nPTIDsc == $PTIDsc)) {
  // echo "Short code either empty OR as before($nPTIDsc)";
  include 'check_product_description_valid.inc.php';
}


// check if the new product type is already in use
include 'check_product_short_code_valid.inc.php';

// check if the new product description is already in use


// echo "<br>User ID = $UID";
// echo "<br>Product type ID = $PTID";
// echo "<br>Ori Product short code ID = $PTIDsc";
// echo "<br>Ori Product name ID = $PTIDn";
// echo "<br>New Product short code ID = $nPTIDsc";
// echo "<br>New Product name ID = $nPTIDn";
// echo "<br>Division ID = $DID";
// echo "<br>Date stamp : $td";

// Start transaction
// mysqli_begin_transaction($mysqli);
// try {
  // to

  // taken out as don't thin k there is a need to check as
  // companies/associate companies/divisions and sections ALL now have a product type set
  // // check if the division is already linked to the product type
  // $sql = "SELECT COUNT(ID) as ID
  //         FROM division_product_types
  //         WHERE DID = ?
  //         AND PTID = ?
  // ;";
  // $stmt = mysqli_stmt_init($con);
  // if (!mysqli_stmt_prepare($stmt, $sql)) {
  //   echo '<b>FAIL-epta</b>';
  // }else{
  //   mysqli_stmt_bind_param($stmt, "ss", $DID, $PTID);
  //   mysqli_stmt_execute($stmt);
  //   $result = mysqli_stmt_get_result($stmt);
  //   $row = mysqli_fetch_array($result);
  //   $DPTID = $row['ID'];
  // }
  //
  // if ($DPTID == 1) {
  // }else {
  //   $sql = "INSERT INTO division_product_types
  //             (DID, PTID, UID, inputtime)
  //           VALUES
  //             (?,?,?,?)
  //   ;";
  //   $stmt = mysqli_stmt_init($con);
  //   if (!mysqli_stmt_prepare($stmt, $sql)) {
  //     echo '<b>FAIL-epta</b>';
  //   }else{
  //     mysqli_stmt_bind_param($stmt, "ssss", $DID, $PTID, $UID, $td);
  //     mysqli_stmt_execute($stmt);
  //   }
  // }


  include 'from_PTID_get_single_prod_type.inc.php';

  if (empty($nPTIDsc)) {$nPTIDsc = $PTIDsc;} else {$nPTIDsc = $nPTIDsc;}
  if (empty($nPTIDn)) {$nPTIDn = $PTIDn;} else {$nPTIDn = $nPTIDn;}

  $sql = "INSERT INTO log_product_type
            (PTID, ori_scode, ori_name, UID, inputtime)
          VALUES
            (?,?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-epta</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sssss", $PTID, $PTIDsc, $PTIDn, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  $sql = "UPDATE product_type
          SET name = ?
            , scode = ?
          WHERE name = ?
          AND scode = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-epta2</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $nPTIDn, $nPTIDsc, $PTIDn, $PTIDsc);
    mysqli_stmt_execute($stmt);
  }
// } catch (mysqli_sql_exception $exception) {
//   mysqli_rollback($mysqli);
//   throw $exception;
// }


header("Location:../styles.php?S&spt");
exit();
