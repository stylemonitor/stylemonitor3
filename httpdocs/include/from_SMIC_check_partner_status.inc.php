<?php
include 'include/dbconnect.inc.php';
// echo "<br><b>from_SMIC_check_partner_status.inc.php</b>";

// Check if they are already PARTNERS
$sql = "SELECT ID as CPID
          , active as CPIDa
          , accdate as CPIDd
          , rel as CPIDrel
        FROM company_partnerships
        WHERE (req_CID = ? AND acc_CID = ?)
        OR (req_CID = ? AND acc_CID = ?)
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL_fscps</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $CID, $ppCID, $ppCID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $CPID  = $row['CPID'];
  $CPIDa = $row['CPIDa'];
  $CPIDrel = $row['CPIDrel'];
}
if (empty($CPID)) {
  $CPID = 0;
  $CPIDa = 0;
  $CPIDrel = 0;
}
// echo "<br>CPDI = $CPID :: CPIDa = $CPIDa :: CPIDrel = $CPIDrel";
