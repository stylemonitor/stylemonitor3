<?php
include 'dbconnect.inc.php';
$UID = $_SESSION['UID'];
// echo "<b>include/form_new_company_user_act.inc.php</b>";

if (!isset($_POST['reg']) && !isset($_POST['for'])&& !isset($_POST['mis'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['reg'])) {
  // echo "<br>Return to the Users page";
  header("Location:../index.php?i");
  exit();
}elseif (isset($_POST['for'])) {
  header("Location:../index.php?f");
  exit();
}elseif (isset($_POST['mis'])) {
  header("Location:../index.php?l");
  exit();
}
