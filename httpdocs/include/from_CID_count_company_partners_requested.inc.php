<?php
include 'dbconnect.inc.php';
$CID = $_SESSION['CID'];
// echo "<b>include/from_CID_count_company_partners_requested.inc.php</b>";
// Check to see how many requests you are waiting a reply from
$sql = "SELECT COUNT(ID) as cCPID
        FROM company_partnerships
        WHERE ((req_CID = ?) || (acc_CID = ?))
        AND insCID IN (?)
			  AND active = 0
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccacp3</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $crCPIDr = $row['cCPID'];
}
