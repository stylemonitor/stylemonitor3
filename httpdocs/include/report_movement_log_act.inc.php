<?php
session_start();
include 'dbconnect.inc.php';

if (!isset($_POST['movLog']) && !isset($_POST['movUP'])) {
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['movLog'])) {
  // echo "WHAT THE HELL DO I DO NOW";
  if (isset($_POST['oi'])) {
    $OIID = $_POST['oi'];
    $urlPage = $_POST['urlPage'];
    // echo "<br>Order Item ID is $OIID";
    // include 'REPORT_ORDER_header.inc.php';
    header("Location:../$urlPage&mlg&oi=$OIID");
    exit();
  }else {
    echo "<br>OIID NOT SET";
  }
}elseif (isset($_POST['movUP'])) {
  if (isset($_POST['oi'])) {
    $OIID = $_POST['oi'];
    $urlPage = $_POST['urlPage'];
    // echo "<br>Order Item ID is $OIID";
    // include 'REPORT_ORDER_header.inc.php';
    header("Location:../$urlPage&orb&oi=$OIID");
    exit();
  }else {
    echo "<br>OIID NOT SET";
  }
}
