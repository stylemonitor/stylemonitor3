<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "<b>include/form_associate_address_full_add.inc.php</b>";
$CID = $_SESSION['CID'];
// include 'from_CID_select_prod_type.inc.php';

include 'set_urlPage.inc.php';

if (isset($_GET['c'])) {
  $bc   = $salAssCol;
  $h    = 'CLIENT';
  $CTID = 4;
  $asType = 'Client';
  $qas = "Select Clients Country";
}elseif (isset($_GET['s'])) {
  $bc   = $purAssCol;
  $h    = 'SUPPLIER';
  $asType = 'Supplier';
  $CTID = 5;
  $qas = "Select Suppliers Country";
}
?>

<div class="overlay"></div>

<div style="position:absolute; top:25%; height:5%; left:0%; width:99.5%; background-color:<?php echo $bc ?>; font-size: 150%; border-left:2px ridge grey; border-top:2px ridge grey; border-right:2px ridge grey; border-radius: 10px 10px 0px 0px; z-index:1;">Full Address add a <?php echo $h ?> Associate Company</div>
<div style="position:absolute; top:30%; height:39%; left:0%; width:99.5%; background-color:<?php echo $edtCol ?>; font-size: 150%; border-left:2px ridge grey; border-bottom:2px ridge grey; border-right:2px ridge grey; border-radius:0px 0px 10px 10px; z-index:1;"></div>

<!-- <label for="name">Company</label> -->
<div style="position:absolute; top:32%; left:38.5%; width:20%; font-size:130%; font-weight:bold; text-align:right; z-index:1;">Address</div>
<div style="position:absolute; top:38%; left:0%; width:21%; font-size:150%; font-weight:bold; text-align:right; z-index:1;">Associate Company</div>
<div style="position:absolute; top:48%; left:0%; width:21%; font-size:150%; font-weight:bold; text-align:right; z-index:1;">Country</div>
<div style="position:absolute; top:53%; left:0%; width:21%; font-size:150%; font-weight:bold; text-align:right; z-index:1;">Country TimeZone</div>
<div style="position:absolute; top:58%; left:0%; width:21%; font-size:150%; font-weight:bold; text-align:right; z-index:1;">Main Product</div>

<form action="include/form_associate_address_add_act.inc.php" method="POST">
  <input type="hidden" name="urlPage" value="<?php echo $urlPage ?>">
  <input type="hidden" name="comp" placeholder="<?php echo $_SESSION['CID']; ?>" value="<?php echo $_SESSION['CID']; ?>">
  <input type="hidden" name="rel" value="<?php echo $CTID ?>">
  <?php
  if (isset($_GET['n'])) {$name = $_GET['n'];
   ?>
   <input required autofocus style="position:absolute; top:37%; height:4%; left:22%; width:30%; text-align:left; text-indent:2%; font-size:150%;background-color:<?php echo $rqdCol ?>; z-index:1;" type="text" name="name" placeholder="Associate Company Name" value="<?php echo $name ?>">
  <?php }else {
    ?>
    <input required autofocus style="position:absolute; top:37%; height:4%; left:22%; width:30%; text-align:left; text-indent:2%; font-size:150%;background-color:<?php echo $rqdCol ?>; z-index:1;" type="text" name="name" placeholder="Associate Company Name">
    <?php
  } ?>

  <!-- Dropdown dependent menu START -->
    <select id="CYID" name="CYID" onchange="FetchTimeZone(this.value)" style="position:absolute; top:48%; height:4%; left:22%; width:30.8%; background-color:<?php echo $ddmCol ?>; z-index:1;">
      <option value="">Select Country</option>
      <?php
      include 'include/select_company_country_details.inc.php';
      ?>
    </select>
    <script type="text/javascript">
    function FetchTimeZone(id){
      $('#TZID').html('');
      // alert(id);
      // return false;
      $.ajax({
        type : 'POST',
        url : 'include/select_country_timezone.inc.php',
        data : { CYID:id},
        success : function(data){
          $('#TZID').html(data);
        }
      })
    }
    </script>
    <select required id="TZID" name="TZID" style="position:absolute; top:53%; height:4%; left:22%; width:30.8%; background-color:<?php echo $ddmCol ?>; z-index:1;">
      <option value="">Select TimeZone</option>
      <?php
      include 'include/select_country_timezone.inc.php';
      ?>
    </select>
  <!-- Dropdown dependent menu END -->

  <select style="position:absolute; top:58%; height:4%; left:21.9%; width:30.8%; background-color:<?php echo $ddmCol ?>; z-index:1;" name="PTID">
    <option value="">Select Main Product Type of Company</option>
    <?php
    include 'include/from_CID_select_prod_type.inc.php';
    ?>
  </select>

  <?php
  if(isset($_GET['addn'])){
    $addn = ($_GET['addn']);
    ?>
    <input  style="position:absolute; top:31.5%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="text" name="addn" placeholder="Address Name" value="<?php echo $addn ?>">
    <?php
  }else{
    ?>
    <input style="position:absolute; top:31.5%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="text" name="addn" placeholder="Address Name">
    <?php
  }
  if(isset($_GET['add1'])){
    $add1 = ($_GET['add1']);
    ?>
    <input style="position:absolute; top:36%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="text" name="add1" placeholder="Address line 1" value="<?php echo $add1 ?>">
    <?php
  }else{
    ?>
    <input style="position:absolute; top:36%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="text" name="add1" placeholder="Address line 1" >
    <?php
  }
  if(isset($_GET['add2'])){
    $add2 = ($_GET['add2']);
    ?>
    <input style="position:absolute; top:40.5%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="text" name="add2" placeholder="Address line 2" value="<?php echo $add2 ?>">
    <?php
  }else{
    ?><input style="position:absolute; top:40.5%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="text" name="add2" placeholder="Address line 2">
    <?php
  }
  if(isset($_GET['city'])){
    $city = ($_GET['city']);
    ?>
    <input style="position:absolute; top:45%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="text" name="city" placeholder="City/town" value="<?php echo $city ?>">
    <?php
  }else{
    ?>
    <input style="position:absolute; top:45%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="text" name="city" placeholder="City/town">
    <?php
  }

  if(isset($_GET['county'])){
    $pcode = ($_GET['county']);
    ?>
    <input style="position:absolute; top:49.5%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="text" name="county" placeholder="Post code" value="<?php echo $county ?>">
    <?php
  }else{
    ?>
    <input style="position:absolute; top:49.5%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="text" name="county" placeholder="County/State">
    <?php
  }

  if(isset($_GET['pcode'])){
    $pcode = ($_GET['pcode']);
    ?>
    <input style="position:absolute; top:54%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="text" name="pcode" placeholder="Post code" value="<?php echo $pcode ?>">
    <?php
  }else{
    ?>
    <input style="position:absolute; top:54%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="text" name="pcode" placeholder="Post/Zip code">
    <?php
  }
  if(isset($_GET['tel'])){
    $tel = ($_GET['tel']);
    ?>
    <input style="position:absolute; top:58.5%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="tel" name="tel" placeholder="Telephone number" value="<?php echo $tel ?>">
    <?php
  }else{
    ?>
    <input style="position:absolute; top:58.5%; height:3%; left:59.5%; width:38.5%; text-align:left; text-indent:2%; font-size:100%; background-color: <?php echo $optCol ?>; z-index:1;" type="tel" name="tel" placeholder="Telephone number">
    <?php
  }
  ?>

  <!-- _____________________________________________________________________      -->

    <button class="entbtn" style="position:absolute; top:64%; left:37.5%; width:10%; background-color:<?php echo $fsvCol ?>; z-index:1;" type="submit" name="adddets">Enter</button>
    <button formnovalidate class="entbtn" style="position:absolute; top:64%; left:52.5%; width:10%; background-color:<?php echo $fcnCol ?>; z-index:1;" type="submit" name="candets" formnovalidate>Cancel</button>

</form>
