<?php
include 'dbconnect.inc.php';
// echo "from_mp_get_reject_total.inc.php";
$OIMRIDs = 0;

if ($mp == 0) {
  $OIMRIDs == 12;
}elseif ($mp == 1) {
  $OIMRIDs == 13;
}elseif ($mp == 2) {
  $OIMRIDs == 14;
}elseif ($mp == 3) {
  $OIMRIDs == 15;
}elseif ($mp == 4) {
  $OIMRIDs == 16;
}elseif ($mp == 5) {
  $OIMRIDs == 17;
}

$sql = "SELECT IF(SUM(opm.omQty) IS NULL,0,SUM(opm.omQty)) as rejVal
        FROM order_placed op
          , order_placed_move opm
        WHERE opm.OPID = ?
        AND opm.OIMRID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fmgrt</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $OPID, $OIMRIDs);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $rejVal = $row['rejVal'];
}
