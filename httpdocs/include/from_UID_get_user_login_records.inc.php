<?php
include 'dbconnect.inc.php';
// echo "<br><b>include/from_UID_get_user_login_records.inc.php</b>";
include 'from_UID_sum_user_login_records.inc.php'; // $sLLIDt
// include 'from_UID_count_user_login_records.inc.php'; // $cLLID

if (isset($_GET['u'])) { $UID = $_GET['u'];}else { $UID = $_SESSION['UID'];}

$sql = "SELECT COUNT(ID) as UID
        FROM login_registry
        WHERE UID = ?
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rul</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $UID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
}
$cUID = $row['UID'];

// Check the URL to see what page we are on
if (isset($_GET['pa'])) { $pa = $_GET['pa'];}else{ $pa = 0;}

// Set the rows per page
$rpp = 16;
// Check which page we are on
if ($pa > 1) { $start = ($pa * $rpp) - $rpp;}else { $start = 0;}

// if(isset($_GET['sort'])){ $sort = $_GET['sort'];}else{ $sort = 'DESC';}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 1.9 is also 2
$PTV = ($cUID / $rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;

if ($sLLIDt < 3600) {
  $sLLIDt = intval(($sLLIDt)/60);
  $logontime = $sLLIDt .' minutes';
}elseif ($sLLIDt > 3600) {
  $sLLIDt = intval(($sLLIDt)/3600);
  $logontime = $sLLIDt .' hours';
}

?>
<div style="position:absolute; top:22%; height:60%; left:10%; width:80%; background-color:pink; border:2px ridge grey; font-size:150%; border-radius:10px; text-align:center;">
  Login details
</div>
<table class="trs" style="position:absolute; top:27%; height:3.5%; left:10.5%; width:79.5%;">
  <tr>
    <th style="width:25%; padding-top:0.4%; padding-right:2%; text-align:right;">Login Count</th>
    <td style="width:25%;"><?php echo $cUID ?></td>
    <th style="width:25%; padding-right:2%; text-align:right;">Total Time Logged In</th>
    <td style="width:25%; height:5%;"><?php echo $logontime ?></td>
  </tr>
</table>

<table class="trs" style="position:absolute; top:34%; height:13%; left:10.5%; width:79.5%;">
  <tr>
    <th style="width:25%;">Login Date</th>
    <th style="width:25%;">Login Time</th>
    <th style="width:25%;">Logout Time</th>
    <th style="width:25%;">Login Duration (mins)</th>
  </tr>
  <tr>

  </tr>
  <?php

  $sql = "SELECT u.firstname as UIDf
  	        , u.surname as UIDs
            , u.ID as UID
            , ll.logintime as LLIDi
            , ll.logouttime as LLIDo
          FROM login_registry ll
  	        , users u
          WHERE u.ID = ?
          AND ll.UID = u.ID
          AND ll.logouttime > 2
          ORDER BY ll.logintime DESC
          LIMIT $start, $rpp
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-fugulr</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while ($row = mysqli_fetch_assoc($result)){
    $LLIDi = $row['LLIDi'];
    $LLIDo = $row['LLIDo'];

    $LLIDit = date('H:i', $LLIDi);
    $LLIDot = date('H:i', $LLIDo);
    $LLIDld = intval(($LLIDo - $LLIDi)/60);
    $LLIDid = date('d-M-Y', $LLIDi);
    ?>

    <tr>
      <td style="width:25%;"><?php echo $LLIDid ?></td>
      <td style="width:25%;"><?php echo $LLIDit ?></td>
      <td style="width:25%;"><?php echo $LLIDot ?></td>
      <td style="width:25%;"><?php echo $LLIDld ?></td>
    </tr>
    <?php
    }
  }
  ?>
</table>
<div style="position:absolute; top:78%; right:15%; text-align:right;">
  <?php for ($x = 1 ; $x <= $PTV ; $x++){
    ?>
    <a style="color:blue; text-decoration:none;" href='company.php?C&usr&log&u=<?php echo $UID ?>&pa=<?php echo $x ?>'>  <?php echo $x ?>  </a>
    <?php
  } ?>
</div>
