<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_count_section_totals.inc.php</b>";
$CID = $_SESSION['CID'];

include 'order_report_section_totals.sql.php';

// CHECK if a PRID has been set
// $sql = "SELECT sum(TOTALS.OIQIDq) AS TOT_QTY
// -- SQL is order_report_section_totals.sql
//        , sum(TOTALS.NEW_OIQIDq) AS NEW_TOT_QTY
//        , sum(TOTALS.Awaiting) AS Awaiting
//        , sum(TOTALS.Stage1) AS Stage1
//        , sum(TOTALS.Stage2) AS Stage2
//        , sum(TOTALS.Stage3) AS Stage3
//        , sum(TOTALS.Stage4) AS Stage4
//        , sum(TOTALS.Stage5) AS Stage5
//        , sum(TOTALS.Warehouse) AS Warehouse
//        , sum(TOTALS.CLIENT) AS CLIENT
//        , sum(TOTALS.SUPPLIER) AS SUPPLIER
//        , sum(TOTALS.SENT) AS SENT
//        , count(distinct TOTALS.OID) AS TOT_ORDERS
//        , count(distinct TOTALS.OIID) AS TOT_ORDER_ITEMS
//        , sum(TOTALS.Orders_Awaiting) AS Orders_Awaiting
//        , sum(TOTALS.Orders_Stage1) AS Orders_Stage1
//        , sum(TOTALS.Orders_Stage2) AS Orders_Stage2
//        , sum(TOTALS.Orders_Stage3) AS Orders_Stage3
//        , sum(TOTALS.Orders_Stage4) AS Orders_Stage4
//        , sum(TOTALS.Orders_Stage5) AS Orders_Stage5
//        , sum(TOTALS.Orders_Warehouse) AS Orders_Warehouse
//        , sum(TOTALS.Orders_Client) AS Orders_Client
//        , sum(TOTALS.Orders_Supplier) AS Orders_Supplier
//        , sum(TOTALS.Orders_Sent) AS Orders_Sent
// FROM
// ( SELECT DISTINCT
//        Mvemnt.ORDER_ID AS OID
//        , Mvemnt.O_ITEM_ID AS OIID
//        , Mvemnt.ordQty AS OIQIDq
//        , Mvemnt.ordQty + SUM(Mvemnt.ORDER_INCR - Mvemnt.ORDER_DECR) AS NEW_OIQIDq
//   -- CALCULATE INDIVIDUAL GARMENT MOVEMENT VALUES FOR EACH SECTION
//        , SUM(Mvemnt.ORDER_PLACEMENT + Mvemnt.ORDER_INCR + Mvemnt.REJ_BY_STG1 + Mvemnt.ORDER_PLACE_INCR + Mvemnt.COR_OUT_STG1 - Mvemnt.ORDER_DECR - Mvemnt.INTO_STG1 - Mvemnt.ORDER_PLACE_DECR - Mvemnt.COR_IN_STG1) AS Awaiting
//        , SUM(Mvemnt.INTO_STG1 - Mvemnt.INTO_STG2 - Mvemnt.REJ_BY_STG1 + Mvemnt.REJ_BY_STG2 - Mvemnt.COR_IN_STG1 + Mvemnt.COR_IN_STG2 + Mvemnt.COR_OUT_STG1 - Mvemnt.COR_OUT_STG2) AS Stage1
//        , SUM(Mvemnt.INTO_STG2 - Mvemnt.INTO_STG3 - Mvemnt.REJ_BY_STG2 + Mvemnt.REJ_BY_STG3 - Mvemnt.COR_IN_STG2 + Mvemnt.COR_IN_STG3 + Mvemnt.COR_OUT_STG2 - Mvemnt.COR_OUT_STG3) AS Stage2
//        , SUM(Mvemnt.INTO_STG3 - Mvemnt.INTO_STG4 - Mvemnt.REJ_BY_STG3 + Mvemnt.REJ_BY_STG4 - Mvemnt.COR_IN_STG3 + Mvemnt.COR_IN_STG4 + Mvemnt.COR_OUT_STG3 - Mvemnt.COR_OUT_STG4) AS Stage3
//        , SUM(Mvemnt.INTO_STG4 - Mvemnt.INTO_STG5 - Mvemnt.REJ_BY_STG4 + Mvemnt.REJ_BY_STG5 - Mvemnt.COR_IN_STG4 + Mvemnt.COR_IN_STG5 + Mvemnt.COR_OUT_STG4 - Mvemnt.COR_OUT_STG5) AS Stage4
//        , SUM(Mvemnt.INTO_STG5 - Mvemnt.INTO_WHOUSE - Mvemnt.REJ_BY_STG5 + Mvemnt.REJ_BY_WHOUSE - Mvemnt.COR_IN_STG5 + Mvemnt.COR_IN_WHOUSE + Mvemnt.COR_OUT_STG5 - Mvemnt.COR_OUT_WHOUSE) AS Stage5
//        , SUM(Mvemnt.INTO_WHOUSE - Mvemnt.REJ_BY_WHOUSE - Mvemnt.TO_CLIENT + Mvemnt.FROM_CLIENT - Mvemnt.RETURNED_TO_SUPPLIER + Mvemnt.RECEIVED_FROM_SUPPLIER - Mvemnt.COR_IN_WHOUSE
//              + Mvemnt.COR_OUT_WHOUSE + Mvemnt.CORR_OUT_CLIENT - Mvemnt.CORR_IN_CLIENT + Mvemnt.CORR_OUT_SUPPLIER - Mvemnt.CORR_IN_SUPPLIER) AS Warehouse
//        , SUM(Mvemnt.TO_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT + Mvemnt.CORR_IN_CLIENT) AS CLIENT
//        , SUM(Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.RECEIVED_FROM_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER) AS SUPPLIER
//        , SUM(Mvemnt.TO_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT + Mvemnt.CORR_IN_CLIENT + Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.RECEIVED_FROM_SUPPLIER
//              - Mvemnt.CORR_OUT_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER) AS SENT
//   -- CALCULATE NUMBER OF ORDERS IN EACH SECTION
//        , CASE WHEN SUM(Mvemnt.ORDER_PLACEMENT - Mvemnt.INTO_STG1 + Mvemnt.ORDER_PLACE_INCR + Mvemnt.ORDER_INCR - Mvemnt.ORDER_PLACE_DECR - Mvemnt.ORDER_DECR + Mvemnt.REJ_BY_STG1 + Mvemnt.COR_IN_STG1 - Mvemnt.COR_OUT_STG1) > 0
//               THEN 1 ELSE 0
//          END AS Orders_Awaiting
//        , CASE WHEN SUM(Mvemnt.INTO_STG1 - Mvemnt.INTO_STG2 - Mvemnt.REJ_BY_STG1 + Mvemnt.REJ_BY_STG2 - Mvemnt.COR_IN_STG1 + Mvemnt.COR_IN_STG2 + Mvemnt.COR_OUT_STG1 - Mvemnt.COR_OUT_STG2) > 0
//               THEN 1 ELSE 0
//          END AS Orders_Stage1
//        , CASE WHEN SUM(Mvemnt.INTO_STG2 - Mvemnt.INTO_STG3 - Mvemnt.REJ_BY_STG2 + Mvemnt.REJ_BY_STG3 - Mvemnt.COR_IN_STG2 + Mvemnt.COR_IN_STG3 + Mvemnt.COR_OUT_STG2 - Mvemnt.COR_OUT_STG3) > 0
//               THEN 1 ELSE 0
//          END AS Orders_Stage2
//        , CASE WHEN SUM(Mvemnt.INTO_STG3 - Mvemnt.INTO_STG4 - Mvemnt.REJ_BY_STG3 + Mvemnt.REJ_BY_STG4 - Mvemnt.COR_IN_STG3 + Mvemnt.COR_IN_STG4 + Mvemnt.COR_OUT_STG3 - Mvemnt.COR_OUT_STG4) > 0
//               THEN 1 ELSE 0
//          END AS Orders_Stage3
//        , CASE WHEN SUM(Mvemnt.INTO_STG4 - Mvemnt.INTO_STG5 - Mvemnt.REJ_BY_STG4 + Mvemnt.REJ_BY_STG5 - Mvemnt.COR_IN_STG4 + Mvemnt.COR_IN_STG5 + Mvemnt.COR_OUT_STG4 - Mvemnt.COR_OUT_STG5) > 0
//               THEN 1 ELSE 0
//          END AS Orders_Stage4
//        , CASE WHEN SUM(Mvemnt.INTO_STG5 - Mvemnt.INTO_WHOUSE - Mvemnt.REJ_BY_STG5 + Mvemnt.REJ_BY_WHOUSE - Mvemnt.COR_IN_STG5 + Mvemnt.COR_IN_WHOUSE + Mvemnt.COR_OUT_STG5 - Mvemnt.COR_OUT_WHOUSE) > 0
//               THEN 1 ELSE 0
//          END AS Orders_Stage5
//        , CASE WHEN SUM(Mvemnt.INTO_WHOUSE - Mvemnt.REJ_BY_WHOUSE - Mvemnt.TO_CLIENT + Mvemnt.FROM_CLIENT - Mvemnt.RETURNED_TO_SUPPLIER + Mvemnt.RECEIVED_FROM_SUPPLIER - Mvemnt.COR_IN_WHOUSE
//              + Mvemnt.COR_OUT_WHOUSE + Mvemnt.CORR_OUT_CLIENT - Mvemnt.CORR_IN_CLIENT + Mvemnt.CORR_OUT_SUPPLIER - Mvemnt.CORR_IN_SUPPLIER) > 0
//               THEN 1 ELSE 0
//          END AS Orders_Warehouse
//        , CASE WHEN SUM(Mvemnt.TO_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT + Mvemnt.CORR_IN_CLIENT) > 0
//               THEN 1 ELSE 0
//          END AS Orders_Client
//         , CASE WHEN SUM(Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.RECEIVED_FROM_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER) > 0
//               THEN 1 ELSE 0
//          END AS Orders_Supplier
//        , CASE WHEN SUM(Mvemnt.TO_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT + Mvemnt.CORR_IN_CLIENT + Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.RECEIVED_FROM_SUPPLIER
//              - Mvemnt.CORR_OUT_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER) > 0
//               THEN 1 ELSE 0
//          END AS Orders_Sent
//   FROM (
//       SELECT DISTINCT
//            o.ID AS ORDER_ID
//            , oi.ID AS O_ITEM_ID
//            , oiq.order_qty AS ordQty
//       -- oimr1 not required at present - oiq.order_qty is used instead
//            , IF((CASE WHEN oimr.ID =  2 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  2 THEN SUM(opm.omQty) END)) AS  ORDER_PLACEMENT
//            , IF((CASE WHEN oimr.ID =  3 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  3 THEN SUM(opm.omQty) END)) AS INTO_STG1
//            , IF((CASE WHEN oimr.ID =  4 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  4 THEN SUM(opm.omQty) END)) AS INTO_STG2
//            , IF((CASE WHEN oimr.ID =  5 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  5 THEN SUM(opm.omQty) END)) AS INTO_STG3
//            , IF((CASE WHEN oimr.ID =  6 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  6 THEN SUM(opm.omQty) END)) AS INTO_STG4
//            , IF((CASE WHEN oimr.ID =  7 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  7 THEN SUM(opm.omQty) END)) AS INTO_STG5
//            , IF((CASE WHEN oimr.ID =  8 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  8 THEN SUM(opm.omQty) END)) AS INTO_WHOUSE
//            , IF((CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END)) AS ORDER_INCR
//            , IF((CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END)) AS ORDER_DECR
//            , IF((CASE WHEN oimr.ID = 11 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 11 THEN SUM(opm.omQty) END)) AS ORDER_PLACE_INCR
//            , IF((CASE WHEN oimr.ID = 12 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 12 THEN SUM(opm.omQty) END)) AS ORDER_PLACE_DECR
//            , IF((CASE WHEN oimr.ID = 13 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 13 THEN SUM(opm.omQty) END)) AS REJ_BY_STG1
//            , IF((CASE WHEN oimr.ID = 14 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 14 THEN SUM(opm.omQty) END)) AS REJ_BY_STG2
//            , IF((CASE WHEN oimr.ID = 15 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 15 THEN SUM(opm.omQty) END)) AS REJ_BY_STG3
//            , IF((CASE WHEN oimr.ID = 16 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 16 THEN SUM(opm.omQty) END)) AS REJ_BY_STG4
//            , IF((CASE WHEN oimr.ID = 17 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 17 THEN SUM(opm.omQty) END)) AS REJ_BY_STG5
//            , IF((CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END)) AS REJ_BY_WHOUSE
//            , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
//            , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
//            , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
//            , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
//            , IF((CASE WHEN oimr.ID = 23 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 23 THEN SUM(opm.omQty) END)) AS COR_IN_STG1
//            , IF((CASE WHEN oimr.ID = 24 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 24 THEN SUM(opm.omQty) END)) AS COR_IN_STG2
//            , IF((CASE WHEN oimr.ID = 25 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 25 THEN SUM(opm.omQty) END)) AS COR_IN_STG3
//            , IF((CASE WHEN oimr.ID = 26 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 26 THEN SUM(opm.omQty) END)) AS COR_IN_STG4
//            , IF((CASE WHEN oimr.ID = 27 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 27 THEN SUM(opm.omQty) END)) AS COR_IN_STG5
//            , IF((CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END)) AS COR_IN_WHOUSE
//            , IF((CASE WHEN oimr.ID = 29 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 29 THEN SUM(opm.omQty) END)) AS COR_OUT_STG1
//            , IF((CASE WHEN oimr.ID = 30 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 30 THEN SUM(opm.omQty) END)) AS COR_OUT_STG2
//            , IF((CASE WHEN oimr.ID = 31 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 31 THEN SUM(opm.omQty) END)) AS COR_OUT_STG3
//            , IF((CASE WHEN oimr.ID = 32 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 32 THEN SUM(opm.omQty) END)) AS COR_OUT_STG4
//            , IF((CASE WHEN oimr.ID = 33 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 33 THEN SUM(opm.omQty) END)) AS COR_OUT_STG5
//            , IF((CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END)) AS COR_OUT_WHOUSE
//            , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
//            , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
//            , IF((CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END)) AS CORR_OUT_SUPPLIER
//            , IF((CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END)) AS CORR_IN_SUPPLIER
//       -- oimr39 to oimr40 not required at present
//       FROM order_placed_move opm
//          INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
//          INNER JOIN order_placed op ON opm.OPID = op.ID
//          INNER JOIN order_item oi ON op.OIID = oi.ID
//          INNER JOIN orders o ON oi.OID = o.ID
//          INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
//       WHERE oi.itComp = 0
//       GROUP BY oi.ID, oimr.ID
//      ) Mvemnt
//           INNER JOIN orders o ON Mvemnt.ORDER_ID = o.ID
//           INNER JOIN company c ON o.fCID = c.ID
//           INNER JOIN company cS ON o.tCID = cS.ID
//           INNER JOIN associate_companies ac ON o.fACID = ac.ID
//           INNER JOIN associate_companies acS ON o.tACID = acS.ID
//      WHERE (o.fCID = ? OR o.tCID = ?)
// GROUP BY ORDER_ID, O_ITEM_ID
// ) TOTALS
// ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-1fcgoco</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $TOT_QTY = $row['TOT_QTY'];
  $TOT_ORDERS = $row['TOT_ORDERS'];
  $TOT_QTY = $row['TOT_QTY'];
  $AWAITING = $row['Awaiting'];
  $stotal1 = $row['Stage1'];
  $stotal2 = $row['Stage2'];
  $stotal3 = $row['Stage3'];
  $stotal4 = $row['Stage4'];
  $stotal5 = $row['Stage5'];
  $WAREHOUSE = $row['Warehouse'];

  // removed as not known in the above sql
  // suspect it should be total quantity but cannot remember
  // $OIQIDtq = $row['OIQIDtq'];
  // $cORtot    = $row['NO_OF_ORDERS'];

  $cORst_awt = $row['Orders_Awaiting'];
  $cORst1    = $row['Orders_Stage1'];
  $cORst2    = $row['Orders_Stage2'];
  $cORst3    = $row['Orders_Stage3'];
  $cORst4    = $row['Orders_Stage4'];
  $cORst5    = $row['Orders_Stage5'];
  $cORwhs    = $row['Orders_Warehouse'];
}
