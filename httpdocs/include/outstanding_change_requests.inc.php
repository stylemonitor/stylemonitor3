<?php
// echo "<br><b>outstanding_change_requests.inc.php</b>";

$pddh = 'Partner Order Changes';
include 'page_description_date_header.inc.php';

?>
<table class="trs" style="position:absolute; top:9%;">
  <tr>
    <th style="width:10%; text-align:center;">Date</th>
    <th style="width:24%; text-align:left;">Partner</th>
    <th style="width:6%; text-align:center;">Order</th>
    <th style="width:6%; text-align:center;">Item</th>
    <th style="width:38%; text-align:left;">Proposed change</th>
    <th style="width:18%; text-align:center;">Action</th>
  </tr>

<?php

include 'sql/outstanding_change_requests.sql.php';
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-ocr</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID , $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $OID = $row['OID'];
    $OIID = $row['OIID'];
    $OIDnos = $row['ordNos'];
    $CIDr = $row['CIDr'];
    $tCID = $row['tCID'];
    $tCIDn = $row['tCIDn'];
    $fCID = $row['fCID'];
    $fCIDn = $row['fCIDn'];
    $OICID = $row['OICID'];
    $OPMIDt = $row['OPMIDt'];
    $OPMIDtime = $row['OPMIDtime'];
    $OIMRIDr = $row['OIMRIDr'];
    $OIDDIDd = $row['OIDDIDd'];
    $Ndate = $row['Ndate'];
    $OIQIDq = $row['OIQIDq'];
    $Nqty = $row['Nqty'];
    $OIIDnos = $row['OIIDnos'];

    $OPMIDtime = date('d-M-Y', $OPMIDtime);

    if ($CID == $tCID) {
      $CIDn = $fCIDn;
    }elseif ($CID == $fCID) {
      $CIDn = $tCIDn;
    }

    if ($CID == $CIDr) {
      $task = "Awaiting a response";
    }else {
      $task = "Please respond";
    }

    $OIDDIDd = date('d-M-Y', $OIDDIDd);
    $Ndate = date('d-M-Y', $Ndate);

    if ($Nqty == 0) {
      $comm = "<b>$OIDDIDd</b> to <b>$Ndate</b>";
    }else {
      $comm = "<b>$OIQIDq</b> to <b>$Nqty</b>";
    }

    ?>
    <tr>
      <td style="text-align:center;"><?php echo $OPMIDtime ?></td>
      <td style="text-align:left;"><?php echo "$CIDn" ?></td>
      <td style="text-align:center;"><?php echo "$OIDnos" ?></td>
      <td style="text-align:center;"><?php echo $OIIDnos ?></td>
      <td style="text-align:left;"><?php echo "$OIMRIDr from $comm" ?></td>
      <td style="text-align:center;"><a class="hreflink" href="home.php?H&rt3&o=<?php echo $OID ?>&OI=<?php echo $OIID ?>"><?php echo $task ?></a></td>
    </tr>
    <?php
}
?>
</table>
<?php
}
