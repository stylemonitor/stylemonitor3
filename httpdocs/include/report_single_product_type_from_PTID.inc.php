<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "include/report_single_product_type_from_PTID.inc.php";
$CID = $_SESSION['CID'];

if (isset($_GET['a'])) {
  $PTID = $_GET['a'];
  $pddh = 'Current Product Type Listing';
}elseif (isset($_GET['b'])) {
  $PTID = $_GET['b'];
  $pddh = 'Current Product Type Listing';
}elseif (isset($_GET['c'])) {
  $PTID = $_GET['c'];
  $pddh = 'Current Product Type Listing';
}elseif (isset($_GET['p'])) {
  $PTID = $_GET['p'];
  $pddh = 'Edit Product Type';
}

$td = date('U');
$td = date('d M Y',$td);

$pddhbc = $prtCol;
include 'page_description_date_header.inc.php';
?>

<table class="trs" style="position:relative; top:9%;">
  <tr>
    <th style="width:10%;text-align:left; text-indent:2%;"><a style="color:black; text-decoration:none;" href="styles.php?S&spr&or=pt.scode&so=<?php echo $sort ?>">Short Code</a></th>
    <th style="width:73%;text-align:left; text-indent:1%;"><a style="color:black; text-decoration:none;" href="styles.php?S&spr&or=pt.name&so=<?php echo $sort ?>">Product Type Reference</a></th>
  </tr>
  <?php

  $sql = "SELECT ID as PTID
            , scode as PTIDsc
            , name as PTID
          FROM product_type
          WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-rpta</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $PTID);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($res);
      $PTID = $row['PTID'];
      $PTIDsc = $row['PTIDsc'];
      $PTIDn = $row['PTID'];
    }
    ?>
    <tr>
      <td style="border-right: thin solid grey; text-align:left; text-indent:3%;"><a style="color:blue; text-decoration:none;" href="styles.php?S&spt&fpto=<?php echo $PTID ?>"><?php echo "$PTIDsc"; ?></a></td>
      <td style="border-right: thin solid grey; text-align:left; text-indent:2%;"><a style="color:blue; text-decoration:none;" href="styles.php?S&spt&fpto=<?php echo $PTID ?>"><?php echo "$PTIDn"; ?></a></td>
    </tr>
  <tr>
    <td colspan="2" style="border-top:2px solid black;"></td>
  </tr>
</table>
