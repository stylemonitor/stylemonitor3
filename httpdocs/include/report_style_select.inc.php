<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/order_report.inc.php";
include 'include/from_CID_count_styles.inc.php';

// $ACID = $_SESSION['ACID'];
$CID = $_SESSION['CID'];
$ACID = $_SESSION['ACID'];
$td = date('U');

if (isset($_GET['s'])) {
  $PRID = $_GET['s'];
}

?>
<table class="trs" style="position:absolute; top:3.75%;">
<!-- <table class="trs" style="position:absolute; top:6.5%;"> -->
<tr>
  <th style="width:8%; text-align:left; text-indent:4%;">Short Code</th>
  <th style="width:30%; text-align:left; text-indent:3%;">Description</th>
  <th style="width:22%;  text-align:left; text-indent:3%;">Type</th>
  <th style="width:6%;">Div</th>
  <th style="width:6%;">Seas</th>
  <th style="width:6%;">Col</th>
  <th style="width:6%;">O/r OPEN</th>
  <th style="width:6%;">O/r TOT</th>
  <th style="width:6%; text-align:left; text-indent:10%;">ETM</th>
  <th style="width:2%;"></th>
  <th style="width:2%;"></th>
</tr>
<?php

$sql = "SELECT distinct(pr.ID) AS PRID
       , pr.prod_ref AS PRIDr
       , pr.prod_desc AS PRIDd
       , IF(pt.name='Select Product Type','Not Yet Chosen',pt.name) AS PTIDr
       , DIVISIONS.cDIV AS DIVISIONS
       , SEASON.cSN AS SEASONS
       , COLLECTION.cCN AS COLLECTIONS
       , pr.etm AS PRIDe
       , IF(O_ORDR.OUTST IS NULL,0,O_ORDR.OUTST) AS OUTSORD
       , IF(TOTAL_ORD.TOT_ORDER IS NULL,0,TOTAL_ORD.TOT_ORDER) AS TOTORD
FROM prod_ref pr
     INNER JOIN product_type pt ON pr.PTID = pt.ID
     INNER JOIN (SELECT distinct(pr.ID) AS PRID
                        , count(pr2d.DID) AS cDIV
                 FROM prod_ref pr
                       INNER JOIN prod_ref_to_div pr2d ON pr.ID = pr2d.PRID
                       INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                 WHERE ac.ID = ?
                 AND pr.ID = ?
                 GROUP BY pr.ID -- , pr2d.DID
                 ) DIVISIONS ON pr.ID = DIVISIONS.PRID
     INNER JOIN (SELECT distinct(pr.ID) AS PRID
                        , count(pr2sn.SNID) AS cSN
                 FROM prod_ref pr
                      INNER JOIN prod_ref_to_season pr2sn ON pr.ID = pr2sn.PRID
                      INNER JOIN season sn ON pr2sn.SNID = sn.ID
                      INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                      WHERE ac.ID = ?
                      AND pr.ID = ?
                 GROUP BY pr.ID -- , pr2sn.SNID
                 ) SEASON ON pr.ID = SEASON.PRID
     INNER JOIN (SELECT distinct(pr.ID) AS PRID
                        , count(pr2c.PRID) As cCN
                 FROM prod_ref pr
                      INNER JOIN product_type pt ON pr.PTID = pt.ID
                      INNER JOIN prod_ref_to_collection pr2c ON pr.ID = pr2c.PRID
                      INNER JOIN collection c ON pr2c.CLID = c.ID
                      INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                      WHERE ac.ID = ?
                      AND pr.ID = ?
                      GROUP BY pr.ID -- , pr2c.PRID
                ) COLLECTION ON pr.ID = COLLECTION.PRID
     LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                             , count(o.ID) AS OUTST
                      FROM prod_ref pr
                           INNER JOIN product_type pt ON pr.PTID = pt.ID
                           INNER JOIN order_item oi ON pr.ID = oi.PRID
                           INNER JOIN orders o ON oi.OID = o.ID
                           INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                      WHERE ac.ID = ?
                      AND pr.ID = ?
                      AND o.orComp = 0
                      GROUP BY pr.ID
                ) O_ORDR ON pr.ID = O_ORDR.PRID
     LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                             , count(o.ID) AS TOT_ORDER
                      FROM prod_ref pr
                           INNER JOIN product_type pt ON pr.PTID = pt.ID
                           INNER JOIN order_item oi ON pr.ID = oi.PRID
                           INNER JOIN orders o ON oi.OID = o.ID
                           INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                      WHERE ac.ID = ?
                      AND pr.ID = ?
                      GROUP BY pr.ID
                ) TOTAL_ORD ON pr.ID = TOTAL_ORD.PRID
     LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                             , count(o.ID) AS COMPL
                      FROM prod_ref pr
                           INNER JOIN product_type pt ON pr.PTID = pt.ID
                           INNER JOIN order_item oi ON pr.ID = oi.PRID
                           INNER JOIN orders o ON oi.OID = o.ID
                           INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                      WHERE ac.ID = ?
                      AND pr.ID = ?
                      AND o.orComp = 1
                      GROUP BY pr.ID
                ) COM_ORDR ON pr.ID = O_ORDR.PRID
WHERE pr.prod_ref != 'TBC'
ORDER BY pr.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rst</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssssssssssss", $ACID, $PRID, $ACID, $PRID, $ACID, $PRID, $ACID, $PRID, $ACID, $PRID, $ACID, $PRID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($res)){
    $PRID  = $row['PRID'];
    $PRIDr = $row['PRIDr'];
    $PRIDd = $row['PRIDd'];
    $PRIDe = $row['PRIDe'];
    $PTIDr = $row['PTIDr'];
    $DIDn  = $row['DIDn'];
    $cDID  = $row['DIVISIONS'];
    $cSNID = $row['SEASONS'];
    $cCLID = $row['COLLECTIONS'];
    $cOIDo = $row['OUTSORD'] ;
    $cOIDt = $row['TOTORD'] ;
    $cDID = $cDID - 1;
    $cSNID = $cSNID - 1;
    $cCLID = $cCLID - 1;

    ?>
    <!-- <div class="cpsty" style="position:absolute; top:0.25%; height:3%; left:0%; width:100%; background-color:#b2b988"><?php echo "Review style $PRIDr : $PRIDd" ?></div> -->

    <tr>
      <td hidden><?php echo "$PRID"; ?></td>
      <td style="width:8%; border-right:thin solid grey; text-align:left; text-indent:2%;"><?php echo "$PRIDr"; ?></td>
      <td style="width:30%; border-right:thin solid grey; text-align:left; text-indent:2%;"><?php echo "$PRIDd"; ?></td>
      <td style="width:22%; border-right:thin solid grey; text-align:left; text-indent:2%;"><?php echo "$PTIDr" ?></td>
      <td style="width:6%; border-right:thin solid grey;"><?php echo $cDID ?></td>
      <td style="width:6%; border-right:thin solid grey;"><?php echo $cSNID ?></td>
      <td style="width:6%; border-right:thin solid grey;"><?php echo $cCLID ?></td>
      <td style="width:6%; border-right:thin solid grey;"><?php echo $cOIDo ?></td>
      <td style="width:6%; border-right:thin solid grey;"><?php echo $cOIDt ?></td>
      <td style="width:6%; border-right:thin solid grey; text-align:right; padding-right: 2%;"><?php echo "$PRIDe"; ?></td>
      <td style="width:2%; border-right:thin solid grey; background-color:pink;"><a style="text-decoration: none;" href="?S&sis&s=<?php echo "$PRID"; ?>">v</a></td>
      <td style="width:2%; background-color:yellow;"><a style="text-decoration: none;" href="?S&sne&s=<?php echo "$PRID"; ?>">e</a></td>
    </tr>
    <?php
    }
    ?>
  </table>
<?php
}
?>

<div style="position:absolute; bottom:5%; right:5%;">

  <?php for ($x = 1 ; $x <= $PTV ; $x++){
    echo "<a style='font-size:200%; text-decoration:none;' href='?S&$head&pa=$x'>  $x  </a>";
  }
  ?>
</div>
