<?php
session_start();
include 'dbconnect.inc.php';

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'Order Report by Order Number';
$pddhbc = '#fffddd';
include 'page_description_date_header.inc.php';

include 'page_selection_open_closed.inc.php';
include 'page_selection_order_type.inc.php';

if (isset($_GET['op'])) {
  $op = $_GET['op'];
  if ($op == 0) {
    $OC = 0;
  }elseif ($op == 1) {
    $OC = 1;
  }
}

if (isset($_GET['ot'])) {
  $ot = $_GET['ot'];
  if ($ot == 1) {
    // code...
  }
}

?>
<table class="trs" style="position:absolute; top:15%;">
  <tr>
    <td colspan="4" style="background-color:#f1f1f1;"></td>
    <th colspan="2" style="border-radius:10px 10px 0px 0px;">Count of</th>
  </tr>
  <tr>
    <th style="width:6%;">O/r</th>
    <th style="width:25%; text-align:left; text-indent:5%;" style="width:7%;">Associate / Partner</th>
    <th style="width:33%; text-align:left; text-indent:5%;">OUR Order Ref</th>
    <th style="width:8%;">O/T</th>
    <th style="width:6%;">Items</th>
    <th style="width:6%;">Quantity</th>
    <th style="width:14%;">Due date</th>
  </tr>

  <?php
  $sql = "SELECT distinct ac.name AS ACIDn
  -- SQL is outstanding_orders_per_CID.sql
         , ac.ID AS ACID
         , o.ID AS OID
         , LPAD(o.orNos,6,0) AS ORNOS
         , o.our_order_ref AS OREF
         , o.OTID AS OTID
         , pr.prod_ref AS STYLE
         , COUNT(DISTINCT oi.ID) AS ITEM_COUNT
         , SUM(oiq.order_qty) AS TOT_QTY
         , from_unixTIME(MAX(oidd.item_del_date),'%d %b %Y') as due
  FROM order_item oi
  	 INNER JOIN orders o ON oi.OID = o.ID
  	 INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
  	 INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
  	 INNER JOIN prod_ref pr ON oi.PRID = pr.ID
  	 INNER JOIN orders_due_dates odd ON odd.OID = o.ID
  	 INNER JOIN company c ON o.fCID = c.ID
       INNER JOIN associate_companies ac ON o.fACID = ac.ID
       INNER JOIN division d ON o.fDID = d.ID
  WHERE (o.fCID = ? OR o.tCID = ?)
    AND oi.itComp = ?
  GROUP BY ac.ID, OID
  ORDER BY ORNOS, OTID
  -- LIMIT ?,?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-oQ</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $OC);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      $ACID = $row['ACID'];
      $ACIDn = $row['ACIDn'];
      $OID = $row['OID'];
      $OTID = $row['OTID'];
      $OIDn = $row['ORNOS'];
      $OIDref = $row['OREF'];
      $OTID = $row['OTID'];
      $cOID = $row['OID_COUNT'];
      $cOIID = $row['ITEM_COUNT'];
      $cPRID = $row['STYLE_COUNT'];
      $cQTY = $row['TOT_QTY'];
      $OIIDd = $row['due'];
      // $ACID = $row['ACID'];

      if ($OTID == 1) {
        $OTIDn = 'Sales';
        $OTID = $OTID;
      }elseif ($OTID == 2) {
        $OTIDn = 'Purchases';
        $OTID = $OTID;
      }

      ?>
      <tr>
        <td style=""><a style="color:blue; text-decoration:none;" href="home.php?H&tab=0&o=<?php echo $OID ?>"><?php echo $OIDn ?></a></td>
        <td style="text-align:left; text-indent:2%;"><?php echo $ACIDn ?></td>
        <td style="text-align:left; text-indent:2%;"><?php echo $OIDref ?></td>
        <td style=""><?php echo $OTIDn ?></td>
        <!-- <td style=""><?php echo $cOID ?></td> -->
        <td style=""><a style="color:blue; text-decoration:none;" href="home.php?H&tab=0&o=<?php echo $OID ?>"><?php echo $cOIID ?></a></td>
        <td style="text-align:right;"><?php echo $cQTY ?></td>
        <td style="text-align:right;"><?php echo $OIIDd ?></td>
      <?php
    }
  }
  ?>
</table>
<?php
