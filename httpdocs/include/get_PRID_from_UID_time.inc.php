<?php
session_start();
include 'dbconnect.inc.php';

// echo "include/get_PRID_from_UID_time.inc.php;";

// Get prod_ref ID
$sql = "SELECT ID as PRID
FROM prod_ref
WHERE UID = ?
AND inputtime = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fnsa2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $PRID = $row['PRID'];
}
