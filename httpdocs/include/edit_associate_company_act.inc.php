 <?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/edit_associate_company_act.inc.php</b>";
$CID = $_SESSION['CID'];
$UID = $_SESSION['UID'];
$td = date('U');

if (!isset($_POST['update_associate_name']) && !isset($_POST['cancel_associate_name'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['cancel_associate_name'])) {
  $type = $_POST['type'];
  header("Location:../associates.php?A&asr&$type");
  exit();
  // echo "<br>Return to the Users page $type";
}elseif (isset($_POST['update_associate_name'])) {
  include 'dbconnect.inc.php';
  // include 'include/dbconnect.inc.php';
  // echo "Prepare and input the data";

  // get the data from the form
  // the original data
  $ACID = $_POST['ASACID'];
  $ACIDn = $_POST['ACIDn'];
  $asCTID = $_POST['asCTID'];
  $asCYID = $_POST['asCYID'];

  // the revised data
  $ACIDn = $_POST['ACIDn'];
  $NCTID = $_POST['NCTID'];
  $NCYID = $_POST['NCYID'];

  if (empty($ACIDn)) {
    $ACIDn = $ACIDn;
  }else {
    $ACIDn = $ACIDn;
  }

  if ($NCYID == 1) {
    $NCYID = $asCYID;
  }else {
    $NCYID = $NCYID;
  }

  // echo "ORIGINAL DATA IS";
  // echo "<br>ACID : $ACID";
  // echo "<br>ACIDn : $ACIDn";
  // echo "<br>asCTID : $asCTID";
  // echo "<br>asCYID : $asCYID";
  //
  // echo "<br><br> REVISED DATA IS";
  // echo "<br>ACIDn : $ACIDn";
  // echo "<br>NCYID : $NCYID";
  // echo "<br>NCTID : $NCTID";

  // ADDING a Division
  $DIDn = $_POST['add_div'];

  // SET an order tolerance value
  $orTol = $_POST['ordtol'];

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to
    if (empty($ACIDn)) {
      $ACIDn = $ACIDn;
    }else {
      $ACIDn = $ACIDn;
    }

    // update the associate_companies table
    $sql = "UPDATE associate_companies
            SET CYID = ?
              , name = ?
            WHERE ID = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-eaca2</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "sss", $NCYID, $ACIDn, $ACID );
      mysqli_stmt_execute($stmt);
    }

    if (!isset($orTol)) {
    }
    else {
      $sql = "UPDATE associate_companies
              SET ord_tol = ?
              WHERE ID = ?
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b>FAIL-eaca21</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ss", $orTol , $ACID );
        mysqli_stmt_execute($stmt);
      }
    }

    if (empty($DIDn)) {
      // echo "NO Division added";
    }else {
      // echo "Add Division";
      $sql = "INSERT INTO division
                (ACID, UID, name, inputtime)
              VALUES
                (?,?,?,?)
      ;";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b>FAIL-eaca1</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ssss", $ACID, $UID, $DIDn, $td);
        mysqli_stmt_execute($stmt);
      }
    }

    // Now add the section as well!!!
    // GET div ID
    $sql = "SELECT MAX(ID)
            FROM division
            WHERE ACID = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-faafaa7</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "s", $ACID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $EDID = $row['0'];
      // echo '<br>The division ID is <b>$EDID : '.$EDID.'</b>';
    }

    // Add a section
    $sql = "INSERT INTO section
              (DID, UID, inputtime)
            VALUES
              (?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-faafaa12</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "sss", $EDID, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    // echo "Division there to be added";

    //     _____________________________________________________________________

    // echo "<br>CID : $CID";
    // echo "<br>ACID : $ACID";
    // echo "<br>td : $td";

    // echo "<br>Original name : $ACIDn";
    // echo "<br>Original Relationship : $asCTID";
    // echo "<br>Original country : $asCYID";

    // echo "<br>Revised name : $ACIDn";
    // echo "<br>Revised Relationship : $NCTID";
    // echo "<br>Revised country : $NCYID";
    // echo "<br>DIVISION NAME : $DIDn";


    if (empty($ACIDn)) { $ACIDn = $ACIDn;}else { $ACIDn = $ACIDn;}

    if ($NCTID == $asCTID) { $NCTID = $asCTID;}
    elseif ($NCTID == 1) { $NCTID = $asCTID;}else { $NCTID = $NCTID;}

    if ($NCYID == $asCYID) { $NCYID = $asCYID;
    }elseif ($NCYID == 1) { $NCYID = $asCYID;}else { $NCYID = $NCYID;}

    // echo "<br>New input data name : $ACIDn";
    // echo "<br>New input data Relationship : $NCTID";
    // echo "<br>New input data country : $NCYID";

    // add teh data to the log table
    $sql = "INSERT INTO log_associate_company_quick_info
              (CID, ACID, ori_name, new_name, ori_type, new_type, ori_country, new_country, UID, inputtime)
            VALUES
              (?,?,?,?,?,?,?,?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-eaca</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssssssssss", $CID, $ACID, $ACIDn, $ACIDn, $asCTID, $NCTID, $asCYID, $NCYID, $UID, $td);
      mysqli_stmt_execute($stmt);
    }
  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  header("Location:../associates.php?A&acn");
  // header("Location:../associates.php?A&asr&AC");
  exit();
}
