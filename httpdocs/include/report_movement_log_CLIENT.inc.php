<?php
include 'dbconnect.inc.php';
// echo "<br><b>report_movement_log_CLIENT.inc.php</b>";
include 'SM_colours.inc.php';
include 'set_urlPage.inc.php';

if (isset($_GET['o'])) {
  $OID = $_GET['o'];
}

if (isset($_GET['OI'])) {
  $OIID = $_GET['OI'];
}

// Set the rows per page
if (isset($_GET['mp'])) {
  $rpp = 4;
}else {
  $rpp = 15;
}

// Check the URL to see what page we are on
if (isset($_GET['pa'])) {
  $pa = $_GET['pa'];
}else{
  $pa = 0;
}

// Check which page we are on
if ($pa > 1) {
  $start = ($pa * $rpp) - $rpp;
}else {
  $start = 0;
}

// count the number of rows that will be in the table
// REMEBER to count them for teh requirement of the table ie, this week, next week and Later
$sql = "SELECT COUNT(oi.ID) as OIID
          , ac.name as ACIDn
          , d.name as DIDn
          , oiq.order_qty oiQTY
          , odd.del_Date as odDIDd
          , o.orDate as OIDs
        FROM orders o
          , order_item oi
          , orders_due_dates odd
          , order_placed op
          , order_placed_move opm
          , order_item_movement_reason oimr
          , associate_companies ac
          , division d
          , order_item_qty oiq
        WHERE op.ID = ?
        AND oi.OID = o.ID
        AND op.OIID = oi.ID
        AND opm.OPID = op.ID
        AND opm.OIMRID = oimr.ID
        AND o.tDID = d.ID
        AND d.ACID = ac.ID
        AND oiq.OIID = oi.ID
        AND odd.OID = o.ID
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rmp</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OPID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $cOIID = $row['OIID'];
  $ACIDn = $row['ACIDn'];
  $DIDn = $row['DIDn'];
  $oiQTY = $row['oiQTY'];
  $odDIDd = $row['odDIDd'];
  $OIDs = $row['OIDs'];

  // turn the order date into an easy to read format
  // $OIDs = strtotime($OIDs);
  $OIDs = date('D-d-M-Y', $OIDs);

  // turn the order delivery date into an easy to read format
  $odDIDd = strtotime($odDIDd);
  $odDIDd = date('D-d-M-Y', $odDIDd);

  // echo '<br><br><br>Count of orders for this company is : '.$cOIID;

  // How many pages will this give us Pages To View $PTV
  // ceil rounds up a number ie 1.1 becomes 2 1.9 is also 2
  $PTV = ($cOIID / $rpp);

  // echo '<br>The number of pages will be : '.$PTV;
  $PTV = ceil($PTV);
  // $PTV = ceil($PTV) - 1;
  // echo '<br>The number of pages will be : '.$PTV;

  // echo '<br>Start record is : '.$start;
}

if ($OTID == 1) {
  $bcCol = $salAssCol;
}elseif ($OTID == 2) {
  $bcCol = $purAssCol;
}

?>

<!-- Get a table for the manufacturing unit details -->
<table class="trs" style="position:absolute; top:44%; left:0%; width:100%;">
  <caption style="font-weight:bold; text-align:left; text-indent:2%; background-color:<?php echo $bcCol ?>;"><a class="hreflink" href="home.php?H&rt3&o=<?php echo $OID ?>&OI=<?php echo $OIID ?>&mvt"> Item Changes Log</a></caption>
  <tr>
    <th style="width:13%; text-align:left; text-indent:2%; background-color:#b5fcdb; ">Activity Date</th>
    <th style="width:15%; text-align:left; text-indent:5%; background-color:<?php echo $movComCol ?>;">Type of Change</th>
    <th style="width:31%; background-color:#f1f1f1;">Requested by</th>
    <th style="width:31%; background-color:#f1f1f1;">Responded by</th>
    <th style="width:10%; background-color:#fffddd;">Date</th>
  </tr>
<?php

// Get those records

include 'sql/report_movement_log_CLIENT.sql.php';
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rmp2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssss", $CID, $CID, $OIID, $start, $rpp);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $OIMRID = $row['OIMRID'];
    $OICIDd = $row['Date_REQUESTED'];

    $eit = $row['OICID'];
    $OICIDq = $row['rqtOICIC'];
    $OIQIDq = $row['oqtOIQID'];

    $opmDATE = $row['Date_REQUESTED'];
    $resDATE = $row['Date_RESPONDED'];

    $UIDreq = $row['Requestor'];
    $UIDres = $row['Responder'];

    $status = $row['status'];

    if ($status == 0) {
      $statCol = $OIMRqty;
    }elseif ($status == 1) {
      $statCol = $rejCol;
    }elseif ($status == 2) {
      $statCol = $appCol;
    }

    // ADDING colour to the reason
    if (($OIMRID == 3) || ($OIMRID == 4) || ($OIMRID == 5) || ($OIMRID == 6) || ($OIMRID == 7) || ($OIMRID == 8)) {
      $resbc = $opmColfwd;
    }elseif (($OIMRID == 13) || ($OIMRID == 14) || ($OIMRID == 15) || ($OIMRID == 16) || ($OIMRID == 17) || ($OIMRID == 18)) {
      $resbc = $opmColbck;
    }elseif (($OIMRID == 22) || ($OIMRID == 23) || ($OIMRID == 24) || ($OIMRID == 25) || ($OIMRID == 26) || ($OIMRID == 27) || ($OIMRID == 28) || ($OIMRID == 29) || ($OIMRID == 30) || ($OIMRID == 31) || ($OIMRID == 32) || ($OIMRID == 33) || ($OIMRID == 34)) {
      $resbc = $opmColbck;
    }elseif (($OIMRID == 19) || ($OIMRID == 21)) {
      $resbc = $sentCol;
    }elseif (($OIMRID == 39) || ($OIMRID == 43) || ($OIMRID == 45)) {
      $resbc = $opmColapp;
    }elseif (($OIMRID == 40) ||($OIMRID == 10) || ($OIMRID == 46)) {
      $resbc = $opmColapp;
    }elseif (($OIMRID == 41) || ($OIMRID == 44) || ($OIMRID == 47)) {
      $resbc = $opmColapp;
    }else {
      $bc = 'rgba(255, 255, 255, 0)';
    }

    if ($OIMRID == 40) {
      $OIMReas = 'Date Change';
      $OIMReasCol = $OIMRdte;
    }elseif ($OIMRID == 10) {
      $OIMReas = 'Quantity Reduction';
      $OIMReasCol = $OIMRqty;
    }elseif (($OIMRID == 41) || ($OIMRID == 44) || ($OIMRID == 47)) {
      $OIMReas = 'REJECT';
      $OIMReasCol = "RED";
    }elseif (($OIMRID == 10) || ($OIMRID == 39) || ($OIMRID == 46))  {
      $OIMReas = 'AGREED';
      $OIMReasCol = $fsvCol;
    }elseif (($OIMRID == 2) || ($OIMRID == 43)) {
      $OIMReas = 'Quantity Reduction';
      $OIMReasCol = $OIMRdte;
    }elseif (($OIMRID == 40) || ($OIMRID == 43) || ($OIMRID == 45)) {
      $OIMReas = 'Request';
      $OIMReasCol = $OIMRdte;
    }elseif ($OIMRID == 42) {
      $OIMReas = 'Sam/Prod';
      $OIMReasCol = $OIMRdte;
    }elseif ($OIMRID == 2) {
      $OIMReas = 'Quantity Reduction';
      $OIMReasCol = $OIMRdte;
    }

    $opmDATE = date('d-M-Y', $opmDATE);

    if (empty($resDATE)) {
      $resDATE = "";
    }else {
      $resDATE = date('d-M-Y', $resDATE);
    }

    if ($OIMRID == 2) {
      $OIMnature = 'Initial Order Placement';
    }elseif ($OIMRID == 42) {
      $OIMnature = 'Correction';
    }elseif (($OIMRID == 39) || ($OIMRID == 41) || ($OIMRID == 40)) {
      $OIMnature = "From <b>$OIDDIDd</b> to <b>$OICIDd</b>";

    }elseif (($OIMRID == 10) || ($OIMRID == 44) || ($OIMRID == 43)) {
      $OIMnature = "Reduction from <b>$OIQIDq</b> to <b>$OICIDq</b>";

    }elseif (($OIMRID == 45) || ($OIMRID == 46) || ($OIMRID == 47)) {
      $OIMnature = 'Item Cancellation';
    }


    // do calculations to show the actual amount being changed
    // either in days FROM TO
    // or in Quantity Reduction FRTOM TO





    // include 'action/select_date_changes_from_OIID.act.php';
    // if ($OIDDd > $OICIDd) {
    //   $OIDDd = date('d-M-Y', $OIDDd);
    //   $OICIDd = date('d-M-Y', $OICIDd);
    //   $dateChange = "Brought forward : $OIDDd to $OICIDd";
    //   $dcCol = "pink";
    // }else {
    //   $OIDDd = date('d-M-Y', $OIDDd);
    //   $OICIDd = date('d-M-Y', $OICIDd);
    //   $dateChange = "Increased from $OIDDd to $OICIDd";
    //   $dcCol = "green";
    // }

    ?>
    <tr style="font-family:monospace;">
      <td style="text-align:left; text-indent:2%; border-right:thin solid grey;"><?php echo "$opmDATE"?> </td>
      <td style="text-align:left; text-indent:2%; background-color:<?php echo $statCol ?>;"><a class="hreflink" href="home.php?H&o=<?php echo $OID ?>&OI=<?php echo $OIID ?>&eir=<?php echo $eit ?>"><?php echo "$OIMReas" ?></a></td>
      <td style="text-align:left; text-indent:2%; background-color:<?php echo $dcCol ?>; border-right:thin solid grey;"><?php echo $UIDreq ?></td>
      <td style="text-align:left; text-indent:2%; background-color:<?php echo $ubc ?>"><?php echo $UIDres ?></td>
      <td><?php echo $resDATE ?></td>
    </tr>
    <?php
  }
}
 ?>
</table>

<?php
if (isset($_GET['mp'])) {
}else {
  ?>
  <div style="position:absolute; bottom:0%; right:5%;font-size:200%">
    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      if (isset($_GET['oi'])) {
        ?>
        <a style="color:black; text-decoration:none;" href='<?php echo $urlPage ?>&o=<?php echo $OID ?>&oi=<?php echo $OIID ?>&pa=<?php echo $x ?>'><?php echo $x ?></a>
        <?php
      }elseif (isset($_GET['OI'])) {
        ?>
        <a style="color:black; text-decoration:none;" href='<?php echo $urlPage ?>&o=<?php echo $OID ?>&OI=<?php echo $OIID ?>&pa=<?php echo $x ?>'><?php echo $x ?></a>
        <?php
      }
    } ?>
  </div>
  <?php
}
?>
