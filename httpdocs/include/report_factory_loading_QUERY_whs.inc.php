<?php
session_start();
include 'dbconnect.inc.php';
echo "include/report_factory_loading_QUERY.inc.php";

include 'set_urlPage.inc.php';

if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
}

$url = "$urlPage&tab=$tab";

$CID = $_SESSION['CID'];

$sql = "SELECT TOTALS.*
FROM
(
SELECT DISTINCT
  -- SQL is 'order_report_order_items_per_WHOUSE.sql'
       Mvemnt.ORDER_ID AS OID
       , o.fCID AS cliCID
       , c.name AS cliCIDn
       , o.fACID AS cli_ACID
       , ac.name AS cli_ACIDn
       , o.fDID AS cli_DID
       , d.name AS cli_DIDn
       , o.tCID AS supCID
       , cS.name AS supCIDn
       , o.tACID AS sup_ACID
       , acS.name AS sup_ACIDn
       , o.tDID AS sup_DID
       , d.name AS sup_DIDn
       , Mvemnt.samProd AS samProd
       , o.our_order_ref AS ourRef
       , Mvemnt.PRID AS PRID
       , pr.prod_ref AS PRIDs
       , o.OTID AS OTID
       , Mvemnt.ordQty AS OIQIDq
       , Mvemnt.O_ITEM_ID AS OIID
       , LPAD(o.orNos,6,'0') AS ordNos
       , LPAD(Mvemnt.ord_item_nos,3,'0') AS ordItemNos
       , oidd.item_del_date AS item_due_date
       , SUM(Mvemnt.INTO_WHOUSE - Mvemnt.REJ_BY_WHOUSE - Mvemnt.TO_CLIENT + Mvemnt.FROM_CLIENT
             - Mvemnt.RETURNED_TO_SUPPLIER + Mvemnt.RECEIVED_FROM_SUPPLIER - Mvemnt.COR_IN_WHOUSE
             + Mvemnt.COR_OUT_WHOUSE + Mvemnt.CORR_OUT_CLIENT - Mvemnt.CORR_IN_CLIENT + Mvemnt.CORR_OUT_SUPPLIER
             - Mvemnt.CORR_IN_SUPPLIER) AS Warehouse

       , SUM(Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT) AS CLIENT
       , SUM(Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER) AS SUPPLIER
  FROM (
      SELECT DISTINCT
           o.ID AS ORDER_ID
           , oi.ID AS O_ITEM_ID
           , oi.ord_item_nos AS ord_item_nos
           , oi.samProd AS samProd
           , oi.PRID AS PRID
           , oiq.order_qty AS ordQty
           , IF((CASE WHEN oimr.ID =  8 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  8 THEN SUM(opm.omQty) END)) AS INTO_WHOUSE
           , IF((CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END)) AS REJ_BY_WHOUSE
           , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
           , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
           , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
           , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
           , IF((CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END)) AS COR_IN_WHOUSE
           , IF((CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END)) AS COR_OUT_WHOUSE
           , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
           , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
           , IF((CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END)) AS CORR_OUT_SUPPLIER
           , IF((CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END)) AS CORR_IN_SUPPLIER
      FROM order_placed_move opm
         INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
         INNER JOIN order_placed op ON opm.OPID = op.ID
         INNER JOIN order_item oi ON op.OIID = oi.ID
         INNER JOIN orders o ON oi.OID = o.ID
         INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
         -- WHERE oi.itComp = ?
         WHERE oi.itComp in (0,7)
      GROUP BY oi.ID, oimr.ID
     ) Mvemnt
          INNER JOIN orders o ON Mvemnt.ORDER_ID = o.ID
          INNER JOIN prod_ref pr ON Mvemnt.PRID = pr.ID
          INNER JOIN company c ON o.fCID = c.ID
          INNER JOIN company cS ON o.tCID = cS.ID
          INNER JOIN associate_companies ac ON o.fACID = ac.ID
          INNER JOIN associate_companies acS ON o.tACID = acS.ID
          INNER JOIN division d ON o.fDID = d.ID
          INNER JOIN division dS ON o.tDID = dS.ID
          INNER JOIN order_item_del_date oidd ON Mvemnt.O_ITEM_ID = oidd.OIID
     WHERE (o.fCID = ? OR o.tCID = ?)
       -- AND o.ID = 3
       AND Mvemnt.samProd = ?
       AND o.OTID = ?
GROUP BY O_ITEM_ID
) TOTALS
WHERE TOTALS.Warehouse != 0
ORDER BY from_unixtime(TOTALS.item_due_date),TOTALS.ordNos,TOTALS.ordItemNos,TOTALS.OIQIDq
Limit ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-flQ</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssss", $CID, $CID, $sp, $ot, $limit);
  // mysqli_stmt_bind_param($stmt, "ssssss", $OC, $CID, $CID, $sp, $ot, $limit);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $cliCID = $row['cliCID'];
    $cliCIDn = $row['cliCIDn'];
    $cli_ACID = $row['cli_ACID'];
  $cli_ACIDn = $row['cli_ACIDn'];
    $supCID = $row['supCID'];
    $supCIDn = $row['supCIDn'];
    $sup_ACID = $row['sup_ACID'];
  $sup_ACIDn = $row['sup_ACIDn'];
    $TUID = $row['TUID'];
    $Ttd = $row['Ttd'];
    $OID = $row['OID'];
  $OIID = $row['OIID'];
  $OIDnos = $row['ordNos'];
  $OIIDnos = $row['ordItemNos'];
    $OTID = $row['OTID'];
  $ourRef = $row['ourRef'];
    $ODDIDdd = $row['ODDIDdd'];
  $OIDDIDd = $row['item_due_date'];
    $CPID = $row['CPID'];
  $PRID = $row['PRID'];
  $PRIDs = $row['PRIDs'];
  $OIQIDq = $row['OIQIDq'];
  $Awaiting = $row['Awaiting'];
  $Stage1 = $row['Stage1'];
  $Stage2 = $row['Stage2'];
  $Stage3 = $row['Stage3'];
  $Stage4 = $row['Stage4'];
  $Stage5 = $row['Stage5'];
  $Warehouse = $row['Warehouse'];
  $Sent = $row['SENT'];

  // $TDa = $OIQIDq - $Stage1 - $Stage2 - $Stage3 - $Stage4 - $Stage5 - $Warehouse - $Sent;
  $DSa = $OIQIDq - $Warehouse;

  $CLIENT = $row['CLIENT'];
  $SUPPLIER = $row['SUPPLIER'];

  if ($OIDDIDd < $td) {
    $bcdate = '#fc4141';
  }elseif ($OIDDIDd < ($td + 604800)) {
    $bcdate = '#f7a0a0';
  }elseif ($td + 604800 < $OIDDIDd) {
    $bcdate = '#ecf5a9';
  }elseif ($OIDDIDd > $td + 1209600) {
    $bcdate = '#92ef9d';
  }

// Taken OUT as DAL sorted in his sql
// LEFT IN UNTIL DAL UPDATES THE SQL
    if ($cliCID <> $CID) {
      $cli_ACIDn = $cli_ACIDn;
      $OTID = $OTID - 1;
    }else {
      $cli_ACIDn = $sup_ACIDn;
      $OTID = $OTID;
    }

    $idate = date('d-M-Y', $OIDDIDd);
    ?>
    <tr>
      <?php
      if ($samProd == 0) {
        $spc ='#76f8bc';
        $spbc = '#76f8bc';
      }else {
        $spc = '#fa7b7b';
        $spbc = '#fa7b7b';
      }

      if ($OTID == 1) {
        $c = 'black';
        $bc = 'f7cc6c';
      }elseif ($OTID == 2) {
        $c = 'black';
        $bc = 'bbbbff';
      }elseif ($OTID == 3) {
        $c = 'white';
        $bc = 'f243e4';
      }elseif ($OTID == 4) {
        $c = 'white';
        $bc = '577268';
      }

      if ($Stage < 0) {
        $sbc = '#dd9090';
      }elseif ($Stage == 0) {
        $sbc = '#80c780';
      }else {
        $sbc = '#f1f1f1';
      }

      ?>
      <?php
      if (strlen($cli_ACIDn)>25) {
        $cli_ACIDn = substr($cli_ACIDn,0,25);
        ?>
        <td style="text-align:left; text-indent:1%; border-right: thin solid grey; color: <?php echo $c ?>;"><a style="color:blue; text-decoration:none;" href="associate.php?A&asc&id=<?php echo $sup_ACID ?>"><?php echo $cli_ACIDn ?></a></td>
        <?php
      }else {?>
        <td style="text-align:left; text-indent:1%; border-right: thin solid grey; color: <?php echo $c ?>;"><a style="color:blue; text-decoration:none;" href="associate.php?A&asc&id=<?php echo $sup_ACID ?>"><?php echo $cli_ACIDn ?></a></td>
        <?php
      }
      ?>
      <!-- <td style="background-color:<?php echo $spbc?>;"></td> -->
      <td style="text-align: right; background-color: <?php echo $bcdate ?>; border-right:thin solid grey; padding-right:0.5%;"><a style="color:blue; font-size: 100%; text-decoration:none;" href="home.php?H&tab=<?php echo $tab ?>&o=<?php echo $OID;?>"><?php echo $OIDnos ?></a></td>
      <!-- <td style="text-align: right; border-right:thin solid grey; padding-right:0.5%;"><a style="color:red; font-size: 110%; text-decoration:none; " href="loading.php?L&tab=<?php echo $tab ?>&o=<?php echo $OID;?>"><?php echo $OIDnos ?></a></td> -->
      <td style="text-align: right; padding-right:0.2%; border-right:thin solid grey;"><a style="color:blue; font-size: 110%; text-decoration:none; " href="loading.php?L&tab=<?php echo $tab ?>&oi=<?php echo $OIID;?>"><?php echo $OIIDnos;?></a></td>
      <?php
      if (strlen($ourRef)>30) {
        $ourRef = substr($ourRef,0,30);
        ?>
        <td style="text-align:left; text-indent:1%; border-right: thin solid grey; color: <?php echo $c ?>;"><a style="color:blue; text-decoration:none;" href="home.php?H&tab=<?php echo $tab ?>&o=<?php echo $OID;?>"><?php echo $ourRef ?> ...</a></td>
        <?php
      }else {?>
        <td style="text-align:left; text-indent:1%; border-right: thin solid grey; color: <?php echo $c ?>;"><a style="color:blue; text-decoration:none;" href="associate.php?A&asc&id=<?php echo $sup_ACID ?>"><?php echo $ourRef ?></a></td>
        <?php
      } ?>
      <td style="text-align: left; text-indent:5%; border-right:thin solid grey;"><a style="color:blue; text-decoration:none;"href="styles.php?S&sio&s=<?php echo $PRID ?>"><?php echo $PRIDs ?></a></td>
      <td style="text-align:right; padding-right:0.2%; border-right:thin solid grey;"><?php echo $OIQIDq;?></td>
      <td style="text-align:right; padding-right:0.2%; border-right:thin solid grey;"><?php echo $DSa ?></td>
      <?php
      if ($OTID == 4) {
        ?>
        <td style="text-align:right; padding-right:0.2%; border-right:thin solid grey;"><?php echo $Warehouse ?></td>
        <?php
      }else {
        ?>
        <td style="text-align:right; padding-right:0.2%; border-right:thin solid grey;"><a href="home.php?H&tab=0&OI=<?php echo $OIID ?>&ot=<?php echo $ot ?>&sp=<?php echo $sp ?>&mp=6&val=<?php echo $Warehouse ?>"><?php echo $Warehouse ?></a></td>
        <?php
      }
      ?>
      <td style="text-align:right; padding-right:0.2%; border-right:thin solid grey;"><?php echo $CLIENT ?></td>

      <?php
      if($OIDDIDd<$td){;?>
        <td style="width:10%; text-align:right; padding-right:0.5%; background-color:#fc4141;"><?php echo $idate ;?></td>
        <?php
      }elseif($OIDDIDd<($td+604740)){;?>
        <td style="width:10%; text-align:right; padding-right:0.5%; background-color:#f09595;"><?php echo $idate ;?></td>
        <?php
      }elseif($OIDDIDd>($td+121509600)){;?>
        <td style="width:10%; text-align:right; padding-right:0.5%; background-color:#e2e8a8;"><?php echo $idate ;?></td>
        <?php
      }
      else{;?>
        <td style="width:10%; text-align:right; padding-right:0.5%;"><?php echo $idate ;?></td>
        <?php
      } ?>
      </tr>
      <?php
      }
      } ?>
      <?php
      // include 'report_factory_loading_QUERY_awaiting.inc.php';
      include 'table_end_11.inc.php';
       ?>
