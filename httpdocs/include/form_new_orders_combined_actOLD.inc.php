<?php
include 'dbconnect.inc.php';

$CID = $_SESSION['CID'];
$ACID = $_SESSION['ACID'];
$a = $_POST['OTID'];

include 'from_CID_get_min_make_unit.inc.php';
include 'from_CID_get_min_associate_company.inc.php';
include 'from_CID_get_min_division.inc.php';
include 'from_CID_get_next_order_nos.inc.php';
include 'from_CPID_get_next_order_item_nos.inc.php';
include 'from_ACID_get_associate_company_details.inc.php';

include 'set_urlPage.inc.php';

$UID = $_SESSION['UID'];
// echo "<b>include/form_new_orders_combined_act.inc.php</b>";

if (!isset($_POST['orders_combi'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['orders_combi'])) {
  // echo "<br>Return to the Users page";

  // _____________________________________________________________________________________________
  // Gathering the data from the form
  // $ACID = $_SESSION['ACID'];
  // LEFT / FROM Company
  $fCID  = $_POST['fCID'];
  $fACID = $_POST['fACID'];
  $fDID  = $_POST['fDID'];
  $fSID  = $_POST['fSID'];

  // LEFT side data
  $tCID = $_POST['tCID'];
  $tACID = $_POST['tACID'];
  $tDID  = $_POST['tDID'];
  $tSID  = $_POST['tSID'];

  $urlPage = $_POST['urlPage'];

  // echo "tACID = $tACID :: ACID = $ACID";

  // echo "CID : $CID";
  // echo "<br>tACID : $tACID";
  // check if the ACID has been set and set to min ACID if not
  // if ($tACID == 'Select Associate Company') {
  //   // echo "<br>NO ACID set";
  //   $tACID = $mACID;
  //   // echo "<br>tACID = mACID :: $mACID";
  // }

  // CHECK that the to HAS BEEN SET
  if ($tACID == 'Please select Associate Company') {
    $tACID = $ACID;
  }else {
    $tACID = $tACID;
  }

  // check if the division has been set and set to min div if not
  if (empty($tDID)) {
    $tDID = $mDID;
    // echo "<br>tDID = mDID :: $tDID";
  }

  // if the TO company is empty use the min ACID value
  // Relationship between the companies
  $CPID   = $_POST['CPID'];

  // If NO division selected OR if the associate copmany has NO divisions
  // TO associate
  // if (empty($tDID)) {
  //   include 'from_ACID_get_min_division_ddt.inc.php';
  //   $tDID = $mDID;
  // }

  // If NO division selected OR if the associate copmany has NO divisions
  // FROM associate
  if (empty($fDID)) {
    include 'from_ACID_get_min_division_ddf.inc.php';
    $fDID = $mDID;
  }

  // Dates
  $orDate  = $_POST['orDate'];
  $orDate  = strtotime($orDate,0);
  $delDate = $_POST['delDate'];
  $delDate = strtotime($delDate,0);
  $IDD     = $_POST['itemDelDate'];
  // echo "IDD : $IDD";
  $IDD     = strtotime($IDD,0);
  // echo "<br>IDD() : $IDD";

  // Todays date is set as
  $td = date('U');
  // echo "<br>td :: $td";

  // IF THE ITEM DELIVERY DATE IS NOT SET USETHE DATE PLUS
  // IF THE ITEM DELIVERY DATE IS SET USE THAT
  // echo "<br>item delivery date is : $IDD";
  if (empty($IDD)) {
    $IDD = $td + ($Ltime * 86400);
  }else {
    $IDD = $IDD;
  }
  // echo "<br>item delivery date is : $IDD";


  // Order information
  $OTID   = $_POST['OTID'];
  $orNos  = $cOID;
  // $orNos  = $_POST['orNos'];
  $OOR = $_POST['our_order_ref'];
  $TOR = $_POST['their_order_ref'];

  // Product information
  $PRID  = $_POST['tPRID'];
  $orQTY = $_POST['orQTY'];
  $OrIM  = $ccpOIID;
  // $OrIM  = $_POST['orItemNos'];
  $TIR   = $_POST['their_item_ref'];
  $sp    = $_POST['sam_prod'];

  //Additional data for order_item
  $tPRIDx = $_POST['tPRIDx'];
  $tPRID = $_POST['tPRID'];

  if (empty($TIR)) {$TIR = $OOR;}else {$TIR = $TIR;}

  // Get the next order nuumber
  $sql = "SELECT COUNT(ID) as cOID
          FROM orders
          WHERE CPID = ?
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-fnoa5';
  }else {
    mysqli_stmt_bind_param($stmt, "s", $CPID);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($res);
    $cOID = $row['cOID'];
  }

  $cOID = $cOID + 1;

  // _______________________________________________________________________________________________
  // The checking that the information is there
  //
  // echo "<br><b>NEW ORDERS FROM COMBINED DEPENDENCY DROPDOWN FORM</b>";
  // echo "<br>Basic order information";
  // echo "<br>Order Number orNos: $cOID";
  // echo "<br>Order type (Sales/Purchase) OTID : $OTID";
  // echo "<br>Order type (Sample/Production) sp : $sp";
  // echo "<br>Order Date orDate : $orDate";
  // echo "<br>Order Due by Date delDate : $delDate";
  //
  // echo "<br><br>Relationship CPID : <b>$CPID</b>";
  //
  // echo "<br><br><b>LEFT Associate Company</b>";
  // echo "<br>MEMBER company fCID as $fCID";
  // echo "<br>Company ACID is fACID : $fACID";
  // echo "<br>Division DID is fDID : $fDID";
  // echo "<br>Order Reference (optional) OOR : $OOR";
  //
  // echo "<br><br><b>RIGHT Company</b>";
  // echo "<br>MEMBER company CID as $tCID";
  // echo "<br>Company ACID is tACID : $tACID";
  // echo "<br>Division DID is tDID : $tDID";
  // echo "<br>Order Reference (optional) TOR : $TOR";
  //
  // // Product detail
  // echo "<br><br>Item nos OrIM: $OrIM";
  // echo "<br>Style PRID : $PRID";
  // echo "<br>Item Description TIR : $TIR";
  // echo "<br>Quantity orQTY: $orQTY";
  // echo "<br>Item Due by Date itemDelDate: $IDD";

  // // // Section Information
  include 'from_tDID_get_min_section.inc.php';
  include 'from_fDID_get_min_section.inc.php';

  // echo "<br>From db Section information";
  // echo "<br>Section SID is tSID : $mtSCID";
  // echo "<br>Section SID is fSID : $mfSCID";


  // _______________________________________________________________________________________________
  // Now the actual work is to be done

  if (empty($fDID)) {$fDID = $tDID;} else {$fDID = $fDID;}
  if (empty($mfDID)) {$mfDID = $mtDID;} else {$mfDID = $mfDID;}
  if (empty($fPRID)) {$fPRID = $tPRID;} else {$fPRID = $fPRID;}

  if (empty($pPRID)) {
    $pPRID = $PRID;
  }

  // echo "<br>Production lead time is : $acLtime";
  // echo "<br>Sample lead time is : $acsLtime";
  // echo "<br>Original order delivery date is : $delDate";
  // echo "<br>Original item delivery date is : $IDD";

  if (empty($sp)) {
    $sp = 0;
    $Ltime = $acLtime;
    // if not set then PRODUCTION
    // set the automatic delivery date as $acLtime * 86400
  }else {
    $sp = $sp;
    $Ltime = $acsLtime;
  }

// if the item delivery date is not set
// use todays date PLUS the current setting for the lead time for sample/production
  // if (empty($IDD)) {
  //   $IDD = $td + ($Ltime * 86400);
  // }else {
  //   $IDD = $IDD;
  // }

  // echo "<br>Revised order delivery date is : $delDate";
  // echo "<br>Revised item delivery date is : $IDD";

  // $delDate = date('d,M,Y',$delDate);
  // echo "<br>Revised order date in English is : $delDate";

  // $IDD = date('d,M,Y',$IDD);
  // echo "<br>Revised item delivery date in English is : $IDD";

  //
  // if (empty($sp)) {
  //   $sp = '0';
  // } else {
  //   $sp = $sp;
  // }

  // converts the calendar date to UTC format - DO NOT CHANGE
  // $delDate = strtotime($delDate,0);
  // $IDD = strtotime($IDD,0);

  // get the other company/division details IF
  // they are NOT from our Company
  if ($fCID <> $tCID) {
    // select the minimum DID based on the fCID
    include 'from_fCID_get_first_division.inc.php';
    $fDID = $fDID;
    // echo "<br> fDID comes back as : $fDID";
  }

  if ($OTID == 1) { $DID = $fDID; $PDID = $tDID;}
  if ($OTID == 2) { $DID = $tDID; $PDID = $fDID;}
  if ($OTID == 3) { $DID = $fDID; $PDID = $tDID;}
  if ($OTID == 4) { $DID = $tDID; $PDID = $fDID;}
  //

  // if (empty($tPRID)) { $tPRID = $oPRID;}else { $tPRID = $tPRID;}
  // if (empty($TOR)) { $TOR = $OOR;}else { $TOR = $TOR;}
  if (empty($OOR)) { $OOR = $TOR;}else { $OOR = $OOR;}

  if (!is_numeric($orQTY) || ($orQTY < 1)) {
    // echo "<br>The value is NOT a number";
    header("location: ../$urlPage&orn&s&cor=$OOR&sor=$OOR&des=$TIR&1");
    exit();
  }else {
    //information gained from the form
    // echo '<br><br><b>Empty date fields will automatically be filled in as follows</b>';
    // set the order date as todays date
    if (empty($orDate)) { $orDate = $td; }else { $ordate = $orDate; }
    // echo "<br>Order date is : $orDate";

    // // sets the delivery date 91 days in the future
    // if (empty($delDate) && empty($IDD)) { $delDate = ($orDate + 7862400);}
    // elseif (empty($delDate) && isset($IDD)) { $delDate = $IDD;}
    // // echo '<br>The delivery date is ($delDate) : <b>'.$delDate.'</b>';
    //
    // // set the delievery date for each item on the order
    // if (empty($IDD)) { $IDD = $delDate;}
    // // echo '<br>The order item delivery date(Call off) is ($IDD) : <b>'.$IDD.'</b>';
    //
    // // checks if the order delivery date is BEFORE the order date
    // if ($delDate < $orDate) {
    //   // echo '<br>(2a)The order delivery date is ($delDate) : <b>'.$delDate.'</b>';
    //   // echo '<br>(2b)The order delivery date is BEFORE the order date';
    // }

    // echo "<br>Order Date 2 orDate : $orDate";
    // echo "<br>Order Due by Date 2 delDate : $delDate";
    // echo "<br>Item Due by Date 2 itemDelDate: $IDD";
    //
    // echo "<br>CPID   = $CPID
    // <br>tCID   = $tCID
    // <br>Our member company CID fCID   = $fCID
    // <br>Our associate company fACID  = $fACID
    // <br>Our associate division fDID   = $fDID
    // <br>Our associate section fSID   = $mfSCID
    // <br>Our reference OOR    = $OOR
    // <br><br>Their associate company tACID  = $tACID
    // <br>Their associate division tDID   = $tDID
    // <br>Their associate section tSID   = $mtSCID
    // <br>Their reference TOR    = $TOR
    // <br><br>Order tpye OTID   = $OTID
    // <br>User UID    = $UID
    // <br>Order Number orNos  = $orNos
    // <br>Order due Date orDate = $orDate
    // <br>Todays date td     = $td";

//
  //Put the data into the db
    $sql = "INSERT INTO orders
              (CPID, tCID, tACID, tDID, tSID, fCID, fACID, fDID, fSID, OTID, UID, orNos, orDate, our_order_ref, their_order_ref, inputtime)
              VALUES
              (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa';
    }else {
      mysqli_stmt_bind_param($stmt, "ssssssssssssssss", $CPID, $tCID, $tACID, $tDID, $mtSCID, $fCID, $fACID, $fDID, $mfSCID, $OTID, $UID, $orNos, $orDate, $OOR, $TOR, $td);
      mysqli_stmt_execute($stmt);
    }

    // GET the order ID as $OID
    $sql = "SELECT MAX(ID) as OID
            FROM orders
            WHERE UID = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa2';
    }else {
      mysqli_stmt_bind_param($stmt, "s", $UID);
      mysqli_stmt_execute($stmt);
      $res = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($res);
      $OID = $row['OID'];
    }
    // echo "<br>Order ID is $OID";
    // Add the order due date
    $sql = "INSERT INTO orders_due_dates
              (OID, UID, del_date, inputtime)
            VALUES
              (?,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa3';
    }else {
      mysqli_stmt_bind_param($stmt, "ssss", $OID, $UID, $IDD, $td);
      // mysqli_stmt_bind_param($stmt, "ssss", $OID, $UID, $delDate, $td);
      mysqli_stmt_execute($stmt);
    }
    // echo "Order ID is ::: $OID";

    // Add the item details to the order_item table
    $sql = "INSERT INTO order_item
              (OID, PRID, pPRID, samProd, UID, ord_item_nos, their_item_ref, inputtime)
            VALUES
              (?,?,?,?,?,?,?,?)
              ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa4';
    }else {
      mysqli_stmt_bind_param($stmt, "ssssssss", $OID, $tPRID, $fPRID, $sp, $UID, $OrIM, $TIR, $td);
      mysqli_stmt_execute($stmt);
    }

    // GET the order item ID
    $sql = "SELECT ID
            FROM order_item
            WHERE OID = ?
            AND ord_item_nos = ?
            AND inputtime = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa5';
    }else {
      mysqli_stmt_bind_param($stmt, "sss", $OID, $OrIM, $td);
      mysqli_stmt_execute($stmt);
      $res = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($res);
      $OIID = $row['ID'];
    }
    // echo "Order Item ID is :/: $OIID";

    // Add the order item delivery date
    $sql = "INSERT INTO order_item_del_date
              (OIID, UID, item_del_date, inputtime)
            VALUES
              (?,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa6';
    }else {
      mysqli_stmt_bind_param($stmt, "ssss", $OIID, $UID, $IDD, $td);
      mysqli_stmt_execute($stmt);
    }

    // GET the SECTION id for the TO company ie the one that is going to make it
    $DID = $tDID;
    include 'from_DID_get_min_section.inc.php';
    // echo "<br>Make Division fDID = DID : $tDID/$DID ";
    // echo "<br>Section ID : $mSCID";

    // Add the make unit
    $sql = "INSERT INTO order_placed
              (OIID, SID, UID, inputtime)
            VALUES
              (?,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa7';
    }else {
      mysqli_stmt_bind_param($stmt, "ssss", $OIID, $mSCID, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    // get the OPID
    $sql = "SELECT op.ID as OPID
            FROM order_placed op
            WHERE op.SID = ?
            AND op.OIID = ?
            ANd op.UID = ?
            AND op.inputtime = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa8';
    }else {
      mysqli_stmt_bind_param($stmt, "ssss", $mSCID, $OIID, $UID, $td);
      mysqli_stmt_execute($stmt);
      $res = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($res);
      $OPID = $row['OPID'];
    }
  // echo "Order Placed ID is :: $OPID";
  // echo "<br>Check if it has worked";

    // add into the order_placed_move table
    $sql = "INSERT INTO order_placed_move
              (OPID, OIMRID, UID, omQty, inputtime)
            VALUES
              (?,2,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa9';
    }else {
      mysqli_stmt_bind_param($stmt, "ssss", $OPID, $UID, $orQTY, $td);
      mysqli_stmt_execute($stmt);
    }

    // Add the item quantity to the order item qty table
    $sql = "INSERT INTO order_item_qty
              (OIID, UID, order_Qty, inputtime)
            VALUES
              (?,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa1';
    }else {
      mysqli_stmt_bind_param($stmt, "ssss", $OIID, $UID, $orQTY, $td);
      mysqli_stmt_execute($stmt);
    }

    // echo "<br>order_placed ID is $OPID";
    // echo "<br>EVERYTHING WORKED OK!!!";
    // $OID = $OID -;
    header("Location:../$urlPage&orp=$OID");
    // header("Location:../styles.php?S&orp=$OID");
    exit();

  }
}
