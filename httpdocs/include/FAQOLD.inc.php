<?php
session_start();
include 'dbconnect.inc.php';
include 'include/from_UIDc_get_company_admin.inc.php';
// echo "<b>include/about.inc.php</b>";

?>
<p></p>
<?php
if (empty($UID)) {
  ?>
  <div style="position:absolute; top:0%; height:100%; left:5%; width:90%; text-align:left;">
    <br><br><br><br>
    <h1>Isn't StyleMonitor just another PLM</h1>
    <p>No, StyleMonitor is <b>NOT</b> another <b>P</b>roduct <b>L</b>ifecycle <b>M</b>anagement tool
    <br>Instead it fills a gap by <b>monitoring</b> an order through the sections of a factory, from start to finish, so it is a <b>P</b>roduction <b>L</b>ine <b>M</b>onitor</p>
    <br>
    <h2>Is StyleMonitor suitable for me as a manufacturer</h2>
    <p><b>YES</b>, StyleMonitor can be used by any manufacturing company to monitor the progress of any order it has, either sales OR purchases</p>
    <br>
    <h2>Is StyleMonitor suitable for me as a retailer</h2>
    <p><b>YES</b>, StyleMonitor can be used by any retailer to monitor the progress of any order it has, either sales OR purchases</p>
    <br>
    <h2>So why should I use StyleMonitor</h2>
    <p>StyleMonitor is best used with a PARTNER (another StyleMonitor member) whereby the retailer can see their orders at a manufacturer who can update them as they progress though their factory
    <br>The retailer can only see what is happening, they cannot alter the production status
    <br><b>No more</b> multiple copies of out of date spreadsheets
    <br><b>No more</b> 'what's happening to my order' phone calls or emails</p>
    <br>
    <h2>We don't have overseas partners</h2>
    <p>Not a issue, it monitors production regardless of location</p>
    <br>
    <h2>Will anybody be able to see my orders</h2>
    <p>If they are authorised by your company, OR, by your PARTNERS company, then they will have full visability, if not they will not even know you are on the system</p>
    <br>
    <h2>What do I need to use StyleMonitor</h2>
    <p>As StyleMonitor is Cloud based, all you need is a computer with internet access</p>

  </div>
  <img src="include/SM-web02.jpg" style="position:absolute; top:5%; height:90%; left:0%; width:100%; opacity:0.1;">
<?php
}else {
  echo "How it works pages";
}
