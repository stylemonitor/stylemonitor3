<?php
include 'dbconnect.inc.php';
// echo "<br><b>input_address_details_HQ.inc.php</b>";

// add an address
// check that the user is not listed before
include 'check_user_address.inc.php';
// echo "<br>AID :: $AID";
// echo "<br>CYID :: $CYID";
// echo "<br>TZID :: $TZID";
// echo "<br>UID :: $UID";

// Add the company name + HQ as addn to the db
$name = "$CIDn HQ";
// add the country to the db

// echo "$CYID, $TZID, $UID, $name, $CYID, $td";
$sql = "INSERT INTO addresses
          (CYID, TID, UID, addn, inputtime)
        VALUES
          (?,?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-icdu";
}else {
  mysqli_stmt_bind_param($stmt,"sssss", $CYID, $TZID, $UID, $name, $td);
  mysqli_stmt_execute($stmt);
}

// get the address ID
$sql = "SELECT ID as AID
        FROM addresses
        WHERE UID = ?
        AND inputtime = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-icdu2";
}else {
  mysqli_stmt_bind_param($stmt,"ss", $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $AID  = $row['AID'];
}
// echo "<br>Address ID = $AID";

// NO company address added as associate_companies is the company
// add to associate company
$sql = "INSERT INTO company_associates_address
          (ACID, AID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-icdu3";
}else {
  mysqli_stmt_bind_param($stmt,"ssss", $ACID, $AID, $UID, $td);
  mysqli_stmt_execute($stmt);
}

// add to division
$sql = "INSERT INTO company_division_address
          (DID, AID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-icdu4";
}else {
  mysqli_stmt_bind_param($stmt,"ssss", $DID, $AID, $UID, $td);
  mysqli_stmt_execute($stmt);
}
// add to section
$sql = "INSERT INTO company_section_address
          (SID, AID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-icdu3";
}else {
  mysqli_stmt_bind_param($stmt,"ssss", $SID, $AID, $UID, $td);
  mysqli_stmt_execute($stmt);
}
