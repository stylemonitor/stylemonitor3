<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_get_min_division.inc.php</b>";

// $CID = $_SESSION['CID'];

$sql = "SELECT MIN(d.ID) as mDID
          , d.name as mDIDn
        FROM division d
          , associate_companies ac
          , company c
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND d.name NOT IN ('Select Division')
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $mDID = $row['mDID'];
  $mDIDn = $row['mDIDn'];
}
