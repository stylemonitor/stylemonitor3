<?php
// echo "include/licence_limit.inc.php";
?>
<br><br><br>
<p>You have had <b>10 Trial Licences</b> and, unfortunately, don't appear to have been impressed by the system.</p>
<br>
<p>This account has now been frozen and <b>NO MORE</b> licences will be issued to you.</p>
<br>
<p>Should you want us to review this, please use the <b>Contact</b> tab at the top.</p>
<br>
<p>We will then review the matter and will get back to you.</p>
<br><br>
<p style="font-size:800%; color:#840c0c"><b>S</b>tyle<b>M</b>onitor</p>
