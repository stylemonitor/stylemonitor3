<?php
session_start();
include 'dbconnect.inc.php';

// echo "review_associate_company_addresses.inc.php"
if (isset($_GET['id'])) {
  $ACID = $_GET['id'];
}

?>
<div class="cpsty" style="position:absolute; top:31%; left:0%; width:100%; background-color:<?php echo $hbc ?>; padding-top:0.2%;"><a style="color:<?php echo $hc ?>; text-decoration:none;" href="home.php?H&aot">Product Type Review</a>
</div>
<table class="trs" style="position:absolute; top:35%; left:0%; width:100%; z-index:1;">
  <tr>
    <th>Name</th>
    <th>Address</th>
    <th>Town/City</th>
    <th>Country</th>
    <th>Telephone</th>
  </tr>
<?php

// get the information
$sql = "SELECT a.ID as AID
          , a.addn as AIDn
          , a.add1 as AID1
          , a.city as AIDc
          , c.name as CYIDn
          , a.tel as AIDt
        FROM company_associates_address caa
          , addresses a
          , country c
        WHERE caa.ACID = 2
        AND caa.AID = a.ID
        AND a.CYID = c.ID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcspt</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $ACID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($res)){
    $AID = $row['AID'];
    $AIDn = $row['AIDn'];
    $AID1 = $row['AID1'];
    $AIDc = $row['AIDc'];
    $CYIDn = $row['CYIDn'];
    $AIDt = $row['AIDt'];

    if ($AID1 == 'Address line 1') {
      $AID1 = '';
    }else {
      $AID1 = $AID1;
    }

    if ($AIDc == 'Town / City') {
      $AIDc = '';
    }else {
      $AIDc = $AIDc;
    }

    if ($AIDt == 'Main phone number') {
      $AIDt = '';
    }else {
      $AIDt = $AIDt;
    }
    ?>
    <tr>
      <td><?php echo $AID ?></td>
      <td><?php echo "$AIDn" ?></td>
      <td><?php echo "$AID1" ?></td>
      <td><?php echo "$AIDc" ?></td>
      <td><?php echo "$CYIDn" ?></td>
      <td><?php echo "$AIDt" ?></td>
      <td><a href="associates.php?A&eca=<?php echo $AID ?>&id=<?php echo $ACID ?>">Edit</a></td>
    </tr>

    <?php
  }
  ?>
</table>
<?php
}
