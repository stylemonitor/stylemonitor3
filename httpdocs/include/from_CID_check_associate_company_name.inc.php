<?php
include 'dbconnect.inc.php';
echo "<br><b>include/from_CID_check_associate_company_name.inc.php</b>";

// Is the associate company already registered with this company?
$sql = "SELECT ID
        FROM associate_companies
        WHERE CID = ?
        AND name = ?
        AND CTID = ?
        AND CYID = ?;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccacn</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $CID, $name, $CTID, $CYID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $EACID = $row['ID'];
}
