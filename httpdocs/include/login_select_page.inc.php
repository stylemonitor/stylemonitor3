<?php
session_start();
include 'dbconnect.inc.php';
$CID = $_SESSION['CID']; // get the company CID
// echo "<b>include/login_select_page.inc.php</b>";

include 'from_CID_count_order_open.inc.php';
include 'from_CID_count_order_overdue.inc.php';
include 'from_CID_count_order_thisweek.inc.php';
include 'from_CID_count_order_nextweek.inc.php';
include 'from_CID_count_order_later.inc.php';
include 'from_CID_count_order_new.inc.php';
include 'from_CID_count_order_complete.inc.php';

//  get user preference for opening page
include 'from_UID_get_user_details.inc.php';

if (($clientPref == 1) && ($salesPref == 1)) {
  $OTID = 1;
}elseif (($clientPref == 1) && ($salesPref == 2)) {
  $OTID = 2;
}elseif (($clientPref == 2) && ($salesPref == 1)) {
  $OTID = 3;
}elseif (($clientPref == 2) && ($salesPref == 2)) {
  $OTID = 4;
}
$sp = $itemPref;

if ($cORIDop == 0) {
  // echo "<br>CID = $CID";
  // echo "<br>No orders have been placed";
  header("Location:../company.php?C&coi&0");
  exit();
}else {
  header("Location:../home.php?H&tab=0&ot=$OTID&sp=$sp");
  exit();
}
