<?php
session_start();
include 'dbconnect.inc.php';
// // echo "<b>include/edit2_associate_company_name_act.inc.php</b>";

if (isset($_POST['id'])) {
  $ACID = $_POST['id'];
  $ct = $_POST['ct'];
}

if (!isset($_POST['update_associate_name']) && !isset($_POST['cancel_associate_name'])) {
  // echo "<br>WRONG METHOD USED";
}elseif (isset($_POST['cancel_associate_name'])) {
  header("Location:../associates.php?A&asf&id=$ACID&ct=$ct");
  exit();
}elseif (isset($_POST['update_associate_name'])) {
  // echo "<br>Creat a new company name, record and replace the old one";


  // Get the data from the form
  $nCIDn = $_POST['nCIDn'];
  $CIDn = $_POST['CIDn'];
  $UID = $_SESSION['UID'];
  $td = date('U');

  // echo "<br>New company/associate company name is $nCIDn";
  // echo "<br>Old company/associate company name is $CIDn";
  // echo "<br>Company ID is $CID";
  // echo "<br>Associate company ID is $ACID";

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to

    $sql = "INSERT INTO log_assoc_co_name
              (ACID, ori_name, UID, inputtime)
            VALUES
              (?,?,?,?)
              ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-ecna1</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $ACID, $CIDn, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    $sql = "UPDATE associate_companies
            SET name = ?
            WHERE name = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-ecna3</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $nCIDn, $CIDn );
      mysqli_stmt_execute($stmt);
    }

  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  header("Location:../associates.php?A&asf&id=$ACID&ct=$ct");
  exit();
}
