<?php
// echo "<br><b>check_password_matches.inc.php</b>";

$sql = "SELECT ID as UID
        , pwd as UIDp
        FROM users
        WHERE uid = ? OR email = ?
        AND active = 2
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-cul</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $fUID, $fUID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $UID = $row['UID'];
  $UIDp = $row['UIDp'];
}
// echo "<br>UID :: $UID";
// from UID get the users details
include 'check_login_user_details.inc.php';

// CHECK that the password used matches that in the db
$pwdCheck = password_verify($epwd, $UIDp);

// check if the password is correct for the user
if($pwdCheck == false){
  // echo "<br>The password used <b>does not match</b> their system held password";
  include 'pwd_lockout_count.inc.php';
}elseif($pwdCheck == true){
  // echo "<br>Password matches system user password";
  // echo "<br>Valid User UID == $UID";
  include 'check_company_licence_validity.inc.php';
}
