<?php
session_start();
include 'dbconnect.inc.php';
// // echo "<b>include/edit_company_name_act.inc.php</b>";
$id = $_POST['id'];
$ct = $_POST['ct'];

if (!isset($_POST['update_div_name']) && !isset($_POST['cancel_div_name'])) {
  // echo "<br>WRONG METHOD USED";
}elseif (isset($_POST['cancel_div_name'])) {
  header("Location:../associates.php?A&ald&id=$id&ct=$ct");
  exit();
}elseif (isset($_POST['update_div_name'])) {
  // echo "<br>Creat a new company name, record and replace the old one";

  // Get the data from the form
  $id = $_POST['id'];
  $ct = $_POST['ct'];
  $nDIDn = $_POST['nDIDn'];
  $DIDn = $_POST['DIDn'];
  $DID = $_POST['DID'];

  $UID = $_SESSION['UID'];
  $td = date('U');

  // echo "<br>New company/associate company name is $nCIDn";
  // echo "<br>Old company/associate company name is $CIDn";
  // echo "<br>Company ID is $CID";
  // echo "<br>Associate company ID is $ACID";

  if (empty($nDIDn)) {
    $nDIDn = $DIDn;
  }

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to

    $sql = "INSERT INTO log_division
              (DID, ori_name, UID, inputtime)
            VALUES
              (?,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-eda</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $DID, $DIDn, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    $sql = "UPDATE division
            SET name = ?
            WHERE ID = ?;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-eda2</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $nDIDn, $DID );
      mysqli_stmt_execute($stmt);
    }

  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  header("Location:../associates.php?A&ald&id=$id&ct=$ct");
  exit();
}
