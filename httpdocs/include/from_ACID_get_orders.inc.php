<?php
include 'dbconnect.inc.php';
echo "include/from_ACID_get_orders.inc.php";

$CID = $_SESSION['CID'];
if (isset($_GET['aol'])) {
  $ACID = $_GET['id'];
}
// echo "$CID :: $id";
?>

<table class="trs">
  <tr>
    <th>Order Nos</th>
    <th>Our Ref</th>
    <th>Qty</th>
    <th>Due Date</th>
  </tr>
  <?php

  $sql = "SELECT o.ID as OID
            , o.orNos as orNos
            , o.our_order_ref as ourRef
            , oi.ord_item_nos as orItemnos
            , oiq.order_qty as qty
            , o.orComp as orComp
          FROM orders o
            , order_item oi
            , order_item_qty oiq
            , order_item_del_date oidd
            , company_partnerships cp
          WHERE cp.ID IN (SELECT cp.ID
            FROM company_partnerships cp
            WHERE cp.req_CID = ? OR cp.acc_CID = ?
          )
          AND o.tACID = ?
          AND o.CPID = cp.ID
          AND oi.OID = o.ID
          AND oiq.OIID = oi.id
          AND oidd.OIID = oi.ID
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-fACgo</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $ACID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
  }
  while ($row = mysqli_fetch_assoc($result)) {
    $OID = $row['OID'];
    $orNos = $row['orNos'];
    $ourRef = $row['ourRef'];
    $orItemnos = $row['orItemnos'];
    $qty = $row['qty'];
    $orComp = $row['orComp'];
  ?>

  <tr>
    <td><?php echo "$orNos:$orItemnos" ?></td>
    <td><?php echo $ourRef ?></td>
    <td><?php echo $qty ?></td>
    <td><?php echo $orComp ?></td>
  </tr>
</table>
<?php
}
