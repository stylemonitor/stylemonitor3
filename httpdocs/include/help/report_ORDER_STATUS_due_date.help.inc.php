<?php
// echo "style_prod_ref.help.inc.php";
if (isset($_GET['tab'])) {
  ?>
  <div class="overlay">   </div>

  <div style="position:absolute; top:6%; height:9%; left:5%; width:90%; border: 4px solid green; z-index:1;"></div>
  <div style="position:absolute; top:16%; left:5%; width:90.8%; font-weight: bold; background-color: <?php echo $hlpCol ?>; z-index:1;">
    These will tell you the quantity of work and the number of items on order that are in each time frame
  </div>

  <div class="help" style="position:absolute; top:30%; left:1%; width:50%;">
    <b>Short Code and Description</b>
    <br>Click on the BLUE Short Code value
    <br>or the BLUE description
    <br>to get an option table of what you can do for that product type
  </div>
  <div class="help" style="position:absolute; top:30%; left:59%; width:20%;">
    <b>Type</b>
    <br>Click on the BLUE type
    <br>to see all styles in that product type
  </div>
  <div class="help" style="position:absolute; top:45%; left:70%; width:20%;">
    <b>ORDERS</b>
    <br>Click on the BLUE orders quantity
    <br>to see all orders in that product type
  </div>
  <?php
}
?>
<form action="home.php?H&rt1&tab=<?php echo $tab ?>&ot=<?php echo $OTID ?>&sp=<?php echo $sp ?>" method="post">
  <button class="help" type="submit" style="background-color:<?php echo $fcnCol ?>" name="button">Close</button>
</form>
