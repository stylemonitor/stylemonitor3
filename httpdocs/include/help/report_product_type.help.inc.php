<?php
// echo "report_style.help.inc.php";

include 'from_CID_count_product_type.inc.php';
?>
<div class="overlay"></div>

<?php
if ($cPTID == 0) {
  ?>
  <div class="help" style="position:absolute; bottom:5%; height:6%; left:77.5%; width:20%; z-index:1;">
    <b>Click here</b>
    <br>to add a product type
  </div>
  <form action="styles.php?S&spt" method="post">
    <button class="help" type="submit" style="background-color:<?php echo $fcnCol ?>; z-index:1;" name="button">Close</button>
  </form>
  <?php
}else {
  ?>
  <div class="help" style="position:absolute; top:20%; left:1%; width:48%; z-index:1;">
    <b>Short Code and Description</b>
    <br>Click here
    <br>to get an option table for that product type
  </div>

  <div class="help" style="position:absolute; top:20%; left:51%; width:18%; z-index:1;">
    <b>ORDERS</b>
    <br>Click here
    <br>to see all related orders
  </div>
  <div class="help" style="position:absolute; top:20%; left:71%; width:18%; z-index:1;">
    <b>STYLES</b>
    <br>Click here
    <br>to see all related styles
  </div>

  <div class="help" style="position:absolute; bottom:10%; left:77.5%; width:20%; z-index:1;">
    <b>Click here</b>
    <br>to add a product type
  </div>
  <?php

  ?>
  <form action="styles.php?S&spt" method="post">
    <button class="help" type="submit" style="background-color:<?php echo $fcnCol ?>; z-index:1;" name="button">Close</button>
  </form>
  <?php
}
?>
