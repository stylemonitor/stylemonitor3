<?php
// echo "report_styles_per_product_type.help.inc.php";
if (isset($_GET['spr'])) {
  $PTID = $_GET['p'];
  ?>
  <div class="overlay">   </div>

  <div class="help" style="position:absolute; top:20%; left:1%; width:50%;">
    <b>Short Code and Description</b>
    <br>Click on the BLUE Short Code value
    <br>or the BLUE description
    <br>to get an option table of what you can do for that product type
  </div>
  <div class="help" style="position:absolute; top:20%; left:70%; width:20%;">
    <b>ORDERS</b>
    <br>Click on the BLUE orders quantity
    <br>to see all orders in that product type
  </div>
  <div class="help" style="position:absolute; bottom:15%; left:70%; width:20%;">
    <b>Add Style</b>
    <br>Click on the Add Style button
    <br>to add another style to this product type
  </div>
  <?php
}
?>
<form action="styles.php?S&spr&p=<?php echo $PTID ?>" method="post">
  <button class="help" type="submit" style="background-color:<?php echo $fcnCol ?>" name="button">Close</button>
</form>
