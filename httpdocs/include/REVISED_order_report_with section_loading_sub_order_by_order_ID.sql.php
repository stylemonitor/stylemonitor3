<?php
// echo "REVISED_order_report_with section_loading_sub_order_by_order_ID.sql";

$sql = "SELECT DISTINCT
-- REVISED_order_report_with section_loading_sub_order_by_order_ID.sql
-- Original is REVISED_order_report_with section_loading_sub_order_by_order_item_ID-20210129.sql
     Mvemnt.ORDER_ID AS OID
     , Mvemnt.O_ITEM_ID AS OIID
     , Mvemnt.ordQty AS OIQIDq
     , Mvemnt.ordQty + SUM(Mvemnt.ORDER_INCR - Mvemnt.ORDER_DECR) AS NEW_OIQIDq
-- CALCULATE INDIVIDUAL GARMENT MOVEMENT VALUES FOR EACH SECTION
     , SUM(Mvemnt.INTO_WHOUSE + Mvemnt.FROM_CLIENT + Mvemnt.COR_IN_WHOUSE + Mvemnt.CORR_OUT_CLIENT - Mvemnt.REJ_BY_WHOUSE - Mvemnt.TO_CLIENT
           - Mvemnt.COR_OUT_WHOUSE - Mvemnt.CORR_IN_CLIENT) AS Warehouse
     , SUM(Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT) AS CLIENT
     , IF(SUM(Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT) + SUM(Mvemnt.INTO_WHOUSE + Mvemnt.FROM_CLIENT
              + Mvemnt.COR_IN_WHOUSE + Mvemnt.CORR_OUT_CLIENT - Mvemnt.REJ_BY_WHOUSE - Mvemnt.TO_CLIENT - Mvemnt.COR_OUT_WHOUSE - Mvemnt.CORR_IN_CLIENT) = Mvemnt.ordQty
              , 7, 0) AS OI_STATUS
FROM (
    SELECT DISTINCT oimr.ID AS OIMR_ID
         , o.ID AS ORDER_ID
         , oi.ID AS O_ITEM_ID
         , oiq.order_qty AS ordQty
         , oi.itComp AS O_ITEM_COMP
         , IF((CASE WHEN oimr.ID =  8 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  8 THEN SUM(opm.omQty) END)) AS INTO_WHOUSE
         , IF((CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END)) AS ORDER_INCR
         , IF((CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END)) AS ORDER_DECR
         , IF((CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END)) AS REJ_BY_WHOUSE
         , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
         , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
         , IF((CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END)) AS COR_IN_WHOUSE
         , IF((CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END)) AS COR_OUT_WHOUSE
         , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
         , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
    FROM order_placed_move opm
       INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
       INNER JOIN order_placed op ON opm.OPID = op.ID
       INNER JOIN order_item oi ON op.OIID = oi.ID
       INNER JOIN orders o ON oi.OID = o.ID
       INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
    WHERE o.ID = 5
    GROUP BY oi.ID, oimr.ID
   ) Mvemnt
        INNER JOIN orders o ON Mvemnt.ORDER_ID = o.ID
GROUP BY Mvemnt.O_ITEM_ID
ORDER BY o.ID, Mvemnt.O_ITEM_ID ASC
;";
