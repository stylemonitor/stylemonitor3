<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/order_report.inc.php";

include 'set_urlPage.inc.php';

$CID = $_SESSION['CID'];
$td = date('U');

include 'from_CID_count_order_overdue.inc.php';
include 'from_CID_count_order_thisweek.inc.php';
include 'from_CID_count_order_nextweek.inc.php';
include 'from_CID_count_order_later.inc.php';
include 'from_CID_count_order_new.inc.php'; // $cOIDnew
include 'from_CID_count_orders_in_work.inc.php'; // $cOIDiw
include 'from_UID_get_last_logout.inc.php'; // $LRIDout

// Check the URL to see what page we are on
if (isset($_GET['p'])) { $p = $_GET['p']; }else{ $p = 0; }

// Set the rows per page
$r = 20;
// Check which page we are on
if ($p > 1) { $start = ($p * $r) - $r; }else { $start = 0; }

// sets the order type
// all open orders overdue
// if ($cOIDod == 0) {
// }
if (isset($_GET['aod'])) {
  $sp = 'aod';
  $OC = 0;
  $sd = $td - 31536000;
  $ed = $td;
  $dr = 'oidd.item_del_date';
  $cp = "orders that are OVERDUE";
  $hc = 'black';
  $hbc = '#d8081e';
}

// all open orders placed within the last 7 days
// if ($cOIDnew <> 0) {
// }
if (isset($_GET['aoe'])) {
  $sp = 'aoe';
  $OC = 0;
  $sd = $td - 604800;
  $ed = $td + 86400;
  $dr = 'COMPANY.orDate';
  // $dr = 'oidd.item_del_date';
  $cp = "new orders that have been placed in the last week";
  $hc = 'white';
  $hbc = '#693663';
}

// orders due later than 2 weeks
// if ($cOIDla <> 0) {
// }
if (isset($_GET['aol'])) {
  $sp = 'aol';
  $OC = 0;
  $sd = $td + 1209600;
  $ed = $td + 31536000;
  $dr = 'oidd.item_del_date';
  $cp = "orders due out after 2 weeks";
  $hc = 'black';
  $hbc = '#92ef9d';
}

// orders due next week
// if ($cOIDnw <> 0) {
// }
if (isset($_GET['aon'])) {
  $sp = 'aon';
  $OC = 0;
  $sd = $td + 604800;
  $ed = $td + 1209600;
  $dr = 'oidd.item_del_date';
  $cp = "orders due out within the next week";
  $hc = 'black';
  $hbc = '#ecf5a9';
}

// orders due this week
// if ($cOIDtw <> 0) {
// }
if (isset($_GET['aot'])) {
  $sp = 'aot';
  $OC = 0;
  $sd = $td;
  $ed = $td + 604800;
  $dr = 'oidd.item_del_date';
  $cp = "orders out due within the next 7 days";
  $hc = 'black';
  $hbc = '#f7a0a0';
}

if (isset($_GET['aoo'])) {
  $sp = 'aoo';
  $OC = 0;
  $sd = $td - 31536000;
  $ed = $td + 31536000;
  $dr = 'oidd.item_del_date';
  $cp = "open orders";
  $hc = 'black';
  $hbc = '#b2b988';
}

// if ($cOIDcom <> 0) {
// }
if (isset($_GET['com'])) {
  $sp = 'com';
  $OC = 1;
  $sd = $td - 31536000;
  $ed = $td + 31536000;
  $dr = 'oidd.item_del_date';
  $cp = "Completed orders";
  $hc = 'black';
  $hbc = '#052700';
}

if(isset($_GET['ord'])){ $ord = $_GET['ord']; }else{ $ord = 'oidd.item_del_date'; }
if(isset($_GET['sort'])){ $sort = $_GET['sort']; }else{ $sort = 'ASC'; }

// echo "<br>Company ID : $CID";
// echo "<br>Order status : $OC";
// echo "<br>Today : $td";
// echo "<br>Start day : $sd";
// echo "<br>End day : $ed";
// echo "<br>Count_orders - due date source : $dr";

if ($dr == 'COMPANY.orDate') {$dra = 'o.orDate'; $dr = $dr;}
elseif ($dr <> 'COMPANY.orDate') {$dra = $dr; $dr = $dr;}


$sql = "SELECT COUNT(o.ID) as OID
        FROM orders o
          , company c
          , associate_companies ac
          , division d
          , order_item oi
          , order_item_del_date oidd
        WHERE c.ID = ?
        AND o.orComp = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND o.DID = d.ID
        AND oi.OID = o.ID
        AND oidd.OIID = oi.ID
        AND $dra BETWEEN $sd AND $ed
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>KKFAIL-lr</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $OC);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOID = $row['OID'];
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cOID / $r);
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;
// echo '<br>Start record is : '.$start;
// echo "<br>**** count_orders count : $cOID ****";

if ($cOID < 1) {
  echo "<br>NO orders placed";
  ?>
  <div class="trs" style="position:relative; top:0%;">
    <div class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "There are 0 Orders"?></div>
  </div>
      <p>NO ORDERS TO REVIEW</p>
  <?php
  header("Location:../home.php?H");
  exit();

}else {
  ?>
  <div class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "There are $cOID $cp" ?></div>
  <table class="trs" style="position:relative; top:0%;">
  <tr>
    <th style="width:3%;">O/T</th>
    <th style="width:4%;">O/r</th>
    <th style="width:25%; text-align:left; text-indent:5%;" style="width:7%;">Client</th>
    <th style="width:36%; text-align:left; text-indent:5%;">Reference</th>
    <th style="width:10%;">Product</th>
    <th style="width:7%; padding-right:1%;">Qty</th>
    <th style="width:11%;">Due date</th>
    <th style="width:3%;"></th>
  </tr>
  <?php

  $sql = "SELECT oi.ID            as OIID

       , ot.ID                       as OTID

       , COMPANY.order_no            as OIDon

       , oi.ord_item_nos        as OIIDon

       , COMPANY.Order_CO            as ACID

       , COMPANY.name        as ACIDn

       , PARTNER_CO.CO_Partner  as ACIDp

       , PARTNER_CO.ASSOC_name  as ACIDpn

       , oi.their_item_ref           as OIIDtir

       , COMPANY.oref        as oref

       , pr.prod_ref        as PRIDpr

       , oidd.item_del_date      as OIDDIDd

       , oiq.order_qty        as OIQIDq

       , PARTNER_CO.OIDon            as OIDon

    -- , PARTNER_CO.ASSOC_CO_Partner as ACIDpn

    -- , COMPANY.ASSOC_CO     as ACID

    -- , COMPANY.division     as OIDd

    -- , COMPANY.odate       as odate

    , COMPANY.orDate

    -- , PARTNER_CO.partner_division as OIDp

       , oidd.item_del_date AS oidel

       , last_update.inputtime AS Last_updated

       , last_update.UID AS User

FROM (SELECT o.ID AS order_no

                 , o.orNos as OIDon

                 , c.ID AS CO_Partner

                 , ac.ID AS ASSOC_CO_Partner

                 , ac.name As ASSOC_name

                 , o.DID AS division

                 , o.CPID AS partner_company

                 , o.PDID AS partner_division

        FROM company c

             INNER JOIN associate_companies ac ON ac.CID = c.ID

             INNER JOIN division d ON d.ACID = ac .ID

             INNER JOIN orders o ON o.PDID = d.ID

       ) PARTNER_CO INNER JOIN

                   (SELECT o.ID AS order_no

                           , c.ID AS Order_CO

                           , ac.ID AS ASSOC_CO

                           , o.DID AS division

                           , o.CPID AS company

                           , o.PDID AS partner_division

                           , o.OTID AS order_type

                           , o.orNos as oref

                           , ac.name as name

                           , o.orDate

                     FROM company c

                          INNER JOIN associate_companies ac ON ac.CID = c.ID

                          INNER JOIN division d ON d.ACID = ac .ID

                          INNER JOIN orders o ON o.DID = d.ID

                     WHERE o.orComp = ?

                   ) COMPANY ON PARTNER_CO.order_no = COMPANY.order_no

       INNER JOIN order_item oi ON oi.OID = COMPANY.order_no

       INNER JOIN order_type ot ON COMPANY.order_type = ot.ID

       INNER JOIN prod_ref pr ON oi.PRID = pr.ID

       INNER JOIN order_item_qty oiq ON oiq.ID = oi.ID

       INNER JOIN order_item_del_date oidd ON oidd.OIID = oi.ID

       INNER JOIN order_placed op ON oi.ID = op.OIID

       -- Last update with user

       INNER JOIN (SELECT opm_order.OPID AS OPID

                          , opm_order.UID AS UID

                          , opm_order.inputtime AS inputtime

                   FROM (SELECT opm.OPID

                                , opm.UID

                                , opm.inputtime

                         FROM order_placed_move opm

                              INNER JOIN order_placed op ON op.ID = opm.OPID

                              INNER JOIN order_item oi ON oi.ID = op.OIID

                              INNER JOIN orders o ON oi.OID = o.ID

                         ORDER BY opm.OPID, opm.ID DESC

                        ) opm_order

                   GROUP BY opm_order.OPID

                   ) last_update ON op.ID = last_update.OPID

    WHERE COMPANY.Order_CO = ?

  -- ORDER BY COMPANY.order_no
          AND $dr BETWEEN $sd AND $ed
          ORDER BY $ord $sort
          LIMIT $start, $r
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>KKFAIL-lr</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $OC, $CID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $sort == 'DESC' ? $sort == 'ASC' : $sort == 'DESC';
    while($row = mysqli_fetch_assoc($result)){
      $OIID    = $row['OIID'];
      $OTID    = $row['OTID'];
      $OIDon   = $row['OIDon'];
      $OIIDon  = $row['OIIDon'];
      $ACID    = $row['ACID'];
      $ACIDn   = $row['ACIDn'];
      $ACIDp   = $row['ACIDp'];
      $ACIDpn  = $row['ACIDpn'];
      $OIIDtir = $row['OIIDtir'];
      $PRIDpr  = $row['PRIDpr'];
      $OIQIDq  = $row['OIQIDq'];
      $OIDDIDd = $row['OIDDIDd'];
      $LU = $row['Last_updated'];

      $idate = date('d-M-Y', $OIDDIDd)
      ?>
      <tr>
        <?php
        // Shows you the orders placed within the last 7 days
        // if($od>($td-604740)){
          if ($OTID == 1) {
            ?>
            <td style="font-weight: bold; background-color:#f7cc6c;"><?php echo 'P' ?></td>
          <?php }elseif ($OTID == 2) { ?>
            <td style="font-weight: bold; background-color:#bbbbff;"><?php echo 'P' ?></td>
          <?php }elseif ($OTID == 3) { ?>
            <td style="font-weight: bold; background-color:#f243e4;"><?php echo 'P' ?></td>
          <?php }elseif ($OTID == 4) { ?>
            <td style="font-weight: bold; background-color:#577268;"><?php echo 'P' ?></td>
          <?php }elseif ($OTID == 5) { ?>
            <td style="font-weight: bold; background-color:#f7cc6c;"><?php echo 'S' ?></td>
          <?php }elseif ($OTID == 6) { ?>
            <td style="font-weight: bold; background-color:#bbbbff;"><?php echo 'S' ?></td>
          <?php }elseif ($OTID == 7) { ?>
            <td style="font-weight: bold; background-color:#f243e4;"><?php echo 'S' ?></td>
          <?php }elseif ($OTID == 8) { ?>
            <td style="font-weight: bold; background-color:#577268;"><?php echo 'S' ?></td>
          <?php } ?>
            <td><a style="color:red; text-decoration:none;" href="<?php echo $urlPage ?>&orb&oi=<?php echo $OIID;?>"><?php echo $OIDon.':'.$OIIDon;?></a></td>

            <?php
            if ($ACIDp <> $CID) {$ACID = $ACIDp; $ACIDn = $ACIDpn;}

            ?>

            <td style="text-align:left; text-indent:1%;"><a class="hreflink" href="associates.php?A&dir&ac=<?php echo $ACID;?>"><?php echo $ACIDpn?></a></td>
            <td style="text-align:left;"><?php echo $OIIDtir;?></td>
            <td style="text-align:left;"><?php echo $PRIDpr;?></td>
            <td style="text-align:right; padding-right:1%;"><?php echo $OIQIDq;?></td>
            <?php
            if($OIDDIDd<$td){;?>
              <td style="text-align:center; background-color:#d8081e;"><?php echo $idate ;?></td>
              <?php
            }elseif($OIDDIDd<($td+604740)){;?>
              <td style="text-align:center; background-color:#f09595;"><?php echo $idate ;?></td>
              <?php
            }elseif($OIDDIDd>($td+121509600)){;?>
              <td style="text-align:center; background-color:#e2e8a8;"><?php echo $idate ;?></td>
              <?php
            }else{;?>
              <td><?php echo $idate ;
            } ?>
            </td>
            <?php
            if ($LU < $td) {
              ?>
              <td style="background-color:RED;"></td>
              <?php
            }else {
              ?>
              <td style="background-color:green;">zzzz</td>
              <?php
            }
          }
      } ?>
    </tr>
    <?php
  }
    ?>
  </table>

  <div style="position:absolute; bottom:5%; left:10%;">

    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a href='?H&$sp&p=$x'>  $x  </a>";
    }
    ?>
  </div>
