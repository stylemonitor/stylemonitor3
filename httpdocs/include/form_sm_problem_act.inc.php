<?php
include 'dbconnect.inc.php';
// echo '<b>include/form_set_pwd_questions_act.inc.php</b>';
$UID = $_SESSION['UID'];
$td = date('U');

if (!isset($_POST['add']) && !isset($_POST['comp']) && !isset($_POST['open']) && !isset($_POST['can'])) {
  // echo "WRONG";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['can'])) {
  // echo "Cancel";
  header("Location:../becarri.php?B");
  exit();
}elseif (isset($_POST['comp'])){
  $UID = $_SESSION['UID'];
  $SMPID = $_POST['SMPID'];

  // echo "Completed";
  // echo "<br>Fixed by : $UID";
  // echo "<br>Date : $td";
  // echo "<br>Problem ID : $SMPID";


  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to
    $sql = "UPDATE SM_problems
            SET fixed_by = ?
              , date_fixed = ?
            WHERE ID = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-fspa1</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "sss", $UID, $td, $SMPID);
      mysqli_stmt_execute($stmt);
    }

    $note = 'COMPLETED';

    // ADD a note Showing COMPLETED
    $sql = "INSERT INTO SM_problem_notes
              (note, SMPID, UID, inputtime)
            VALUES
              (?,?,?,?)
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-fspa2</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $note, $SMPID, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    header("Location:../becarri.php?B");
    exit();
  }elseif (isset($_POST['open'])){
    $UID = $_SESSION['UID'];
    $SMPID = $_POST['SMPID'];

    // echo "Completed";
    // echo "<br>Fixed by : $UID";
    // echo "<br>Date : $td";
    // echo "<br>Problem ID : $SMPID";

    $td = 0;

    $sql = "UPDATE SM_problems
            SET fixed_by = ?
              , date_fixed = ?
            WHERE ID = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-fspa1</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "sss", $UID, $td, $SMPID);
      mysqli_stmt_execute($stmt);
    }

    $note = 'Re-opened';

    $td = date('U');

    // ADD a note Showing COMPLETED
    $sql = "INSERT INTO SM_problem_notes
              (note, SMPID, UID, inputtime)
            VALUES
              (?,?,?,?)
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-fspa3</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $note, $SMPID, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    header("Location:../becarri.php?B");
    exit();
  }elseif (isset($_POST['add'])){
    // echo "Add comment";
    $UID = $_POST['UID'];
    $SMPID = $_POST['SMPID'];
    $td = $_POST['td'];
    $note = $_POST['note'];

    // echo "Comment : $note";
    // echo "<br>UID : $UID";
    // echo "<br>SMPID : $SMPID";
    // echo "<br>td : $td";

    // ADD a note to the problem if one is needed
    $sql = "INSERT INTO SM_problem_notes
              (note, SMPID, UID, inputtime)
            VALUES
              (?,?,?,?)
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-fspa4</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $note, $SMPID, $UID, $td);
      mysqli_stmt_execute($stmt);
    }
    header("Location:../becarri.php?B&prb=$SMPID");
    exit();
  }
}  catch (mysqli_sql_exception $exception) {
  mysqli_rollback($mysqli);

  throw $exception;
}
