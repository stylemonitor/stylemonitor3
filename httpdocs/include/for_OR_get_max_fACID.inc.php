<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/for_OR_get_max_fACID.inc.php</b>";

$sql = "SELECT ac.ID as ACID
          , ac.name as ACIDn
        FROM associate_companies ac
        WHERE ac.ID = (SELECT MAX(ac.ID) as ACID
                        FROM company c
                          , associate_companies ac
                        WHERE c.ID = ?
                        AND ac.CID = c.ID)
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fOgcfA</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $fCID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $maxfACID = $row['ACID'];
  $maxfACIDn = $row['ACIDn'];
}
