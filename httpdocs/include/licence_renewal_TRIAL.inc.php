<?php
include "dbconnect.inc.php";
echo "<br><b>licence_renewal_TRIAL.inc.php</b>";

include "from_UID_get_company_details.inc.php";

$CLIDc  =  $CLIDc + 1;
$td = date('U');

// put the old info into the log_company_licence to keep the history going
$sql  =  "INSERT INTO log_company_licence
            (CID, licType, cTrial, cUsers, cDate, UID)
          VALUES
            (?,?,?,?,?,?)
";
$stmt  =  mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo "FAIL-lrT";
}else{
  mysqli_stmt_bind_param($stmt, "ssssss", $CID, $clLTID, $CLIDc, $cCIDu , $td, $UID);
  mysqli_stmt_execute($stmt);
}

// update the company_licence file
$sql  =  "UPDATE company_licence
        SET LTID = ?
          , UID = ?
          , cTrial = ?
          , cUsers = ?
          , st_date = ?
        WHERE ID  =  ?
";
$stmt  =  mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo "FAIL-lrT2";
}else{
  mysqli_stmt_bind_param($stmt, "ssssss", $clLTID, $UID, $CLIDc, $cCIDu , $td, $CID);
  mysqli_stmt_execute($stmt);
}
// echo "<br>input and update done";
// header("Location:../home.php?H&home");
// exit();
