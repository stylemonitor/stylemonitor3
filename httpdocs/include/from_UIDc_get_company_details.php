<?php
$sql = "SELECT cl.ID as ID
          , c.name as CIDn
          , c.CYID as CYID
          , lt.ID as LTID
          , lt.type LTIDn
          , cl.st_date as clstd
          , lt.timelimit as lttl
        FROM company c
          , division d
          , associate_companies ac
          , company_division_user cdu
          , company_licence cl
          , country cy
          , licence_type lt
        WHERE c.name = ?
        AND cdu.DID = d.ID
        AND d.ACID = ac.ID
        AND ac.CID = c.ID
        AND cl.CID = c.ID
        AND cl.LTID = lt.ID
        AND c.CYID = cy.ID
        ORDER BY cl.id DESC limit 1
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $UIDc);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  if($row = mysqli_fetch_assoc($result)){
    $CID  = $row['ID'];
    $clLTID = $row['LTID'];
    $LTIDn  = $row['LTIDn'];
    $clstd  = $row['clstd'];
    $lttl   = $row['lttl'];
    $CYID = $row['CYID'];
    // echo "<br>The input company is matching that <b>registered</b> to the user";
  }
}
