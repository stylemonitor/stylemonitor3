<?php
session_start();
// echo "include/page_selection_header.inc.php";
include 'set_urlPage.inc.php';

include 'from_CID_count_partner_clients.inc.php';    // get the admin user of the company $cpCPID
include 'from_CID_count_partner_suppliers.inc.php';    // get the admin user of the company $cpCPID
include 'from_UID_get_user_details.inc.php';    // get the admin user of the company $cpCPID

if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
}

if ($itemPref <> 1) {
    $itemPref = $itemPref;
}

if ($clientPref <> 1) {
    $itemPref = $clientPref;
}

if (isset($_GET['ot'])) {
  $ot = $_GET['ot'];
  if ($ot == 1) {
    $otbc = '#f7cc6c';
  }elseif ($ot == 2) {
    $otbc = '#bbbbff';
  }elseif ($ot == 3) {
    $otbc = '#d68e79';
  }elseif ($ot == 4) {
    $otbc = '#c9d1ce';
  }
}else {
  if (($clientPref == 1) && ($salesPref == 1)) {
    $ot = 1;
    $otbc = '#f7cc6c';
  }elseif (($clientPref == 1) && ($salesPref == 2)) {
    $ot = 2;
    $otbc = '#bbbbff';
  }elseif (($clientPref == 2) && ($salesPref == 1)) {
    $ot = 3;
    $otbc = '#d68e79';
  }elseif (($clientPref == 2) && ($salesPref == 2)) {
    $ot = 4;
    $otbc = '#c9d1ce';
  }
}

if (isset($_GET['sp'])) {
  if ($sp == 1) {
    $spbc = "#fa7b7b";
  }elseif ($sp == 2) {
    $spbc ="#76f8bc";
  }
}else {
  if ($itemPref == 1) {
    $sp = 1;
    $spbc = "#fa7b7b";
  }else {
    $sp = 2;
    $spbc ="#76f8bc";
  }
}

$sql = "SELECT CASE WHEN ORDER_STATUS.DEL_WEEK = yearweek(curdate(),7)   THEN IF(ORDER_STATUS.DEL_DATE < curdate(),'1','2')
-- SQL is order_status_summary_REVISED.sql
            WHEN ORDER_STATUS.DEL_WEEK = yearweek(curdate(),7)+1 THEN '3'
            WHEN ORDER_STATUS.DEL_WEEK > yearweek(curdate(),7)+1 THEN '4'
            ELSE '1'
       END AS STATUS
       , ORDER_STATUS.OTID as OTID
       , ORDER_STATUS.SAM_PROD AS SAM_PROD
       , COUNT(DISTINCT ORDER_STATUS.OIID) AS COUNT_OIID
       , COUNT(DISTINCT ORDER_STATUS.OID) AS COUNT_OID
       , CASE WHEN ORDER_STATUS.OTID IN (1,3) THEN(SUM(ORDER_STATUS.ORDER_QTY) - ORDER_STATUS.CLIENT)
              WHEN ORDER_STATUS.OTID IN (2,4) THEN(SUM(ORDER_STATUS.ORDER_QTY) - ORDER_STATUS.SUPPLIER)
         END AS QTY
FROM
(
SELECT DISTINCT
   Mvemnt.OID AS OID
   , Mvemnt.O_ITEM_ID AS OIID
   , yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) AS DEL_WEEK
   , from_unixtime(oidd.item_del_date,'%Y-%m-%d') AS DEL_DATE
   , oiq.order_qty AS ORDER_QTY
   , oi.samProd AS SAM_PROD
   , CASE WHEN o.tCID = o.fCID
          THEN o.OTID
          ELSE CASE WHEN o.tCID = ? THEN 3
               ELSE 4
               END
     END AS OTID
   , SUM(Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT) AS CLIENT
   , SUM(Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER) AS SUPPLIER
FROM (
  SELECT DISTINCT
         oimr.ID AS OIMR_ID
         , oimr.reason AS Reason
         , oi.ID AS O_ITEM_ID
         , opm.ID AS OPMID
         , o.ID AS OID
         , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
         , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
         , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
         , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
         , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
         , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
         , IF((CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END)) AS CORR_OUT_SUPPLIER
         , IF((CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END)) AS CORR_IN_SUPPLIER
  FROM order_placed_move opm
       INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
       INNER JOIN order_placed op ON opm.OPID = op.ID
       INNER JOIN order_item oi ON op.OIID = oi.ID
       INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
       INNER JOIN orders o ON oi.OID = o.ID
  GROUP BY opm.ID, oi.ID, oimr.ID
 ) Mvemnt INNER JOIN order_item oi ON Mvemnt.O_ITEM_ID = oi.ID
          INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
          INNER JOIN orders o ON oi.OID = o.ID
          INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
          INNER JOIN company c ON o.fCID = c.ID
          INNER JOIN company cS ON o.tCID = cS.ID
          INNER JOIN associate_companies ac ON o.fACID = ac.ID
          INNER JOIN associate_companies acS ON o.tACID = acS.ID
          INNER JOIN division d ON o.fDID = d.ID
          INNER JOIN division dS ON o.tDID = dS.ID
WHERE (o.fCID = ? OR o.tCID = ?)
  AND oi.itComp = 0  -- Only include open order items
GROUP BY OIID
ORDER BY OIID ASC
) ORDER_STATUS
GROUP BY STATUS, SAM_PROD, OTID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b><br><br><br><br><br><br>FAIL-fdcs</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $OTID = $row['OTID'];
    $sp = $row['SAM_PROD'];
    $tab = $row['STATUS'];
    $QTY = $row['QTY'];
    $cOIID = $row['COUNT_OIID'];
    $cOID = $row['COUNT_OID'];



?>
<div style="position:absolute; top:8.5%; height:5.9%; left:7%; width:20%;background-color: #fffddd; border:thin solid grey; z-index:1;"></div>

<div style="position:absolute; top:8.5%; height:2.9%; left:7%; width:20%; border:thin solid black; z-index:1;">
  Sales
</div>
<div style="position:absolute; top:11.5%; height:2.9%; left:7%; width:10%; border:thin solid black; z-index:1;">
  Sample
</div>
<div style="position:absolute; top:11.5%; height:2.9%; left:17.1%; width:9.9%; border:thin solid black; z-index:1;">
  Production
</div>

<div style="position:absolute; top:8.5%; height:2.9%; left:29%; width:20%; border:thin solid black; z-index:1;">
  Purchases
</div>
<div style="position:absolute; top:11.5%; height:2.9%; left:29%; width:10%; border:thin solid black; z-index:1;">
  Sample
</div>
<div style="position:absolute; top:11.5%; height:2.9%; left:39.1%; width:9.9%; border:thin solid black; z-index:1;">
  Production
</div>

<div style="position:absolute; top:8.5%; height:2.9%; left:51%; width:20%; border:thin solid black; z-index:1;">
  Partner Sales
</div>
<div style="position:absolute; top:11.5%; height:2.9%; left:51%; width:10%; border:thin solid black; z-index:1;">
  Sample
</div>
<div style="position:absolute; top:11.5%; height:2.9%; left:61.1%; width:9.9%; border:thin solid black; z-index:1;">
  Production
</div>

<div style="position:absolute; top:8.5%; height:2.9%; left:73%; width:20%; border:thin solid black; z-index:1;">
  Partner Purchases
</div>
<div style="position:absolute; top:11.5%; height:2.9%; left:73%; width:10%; border:thin solid black; z-index:1;">
  Sample
</div>
<div style="position:absolute; top:11.5%; height:2.9%; left:83.1%; width:9.9%; border:thin solid black; z-index:1;">
  Production
</div>

<?php

if ($cCPIDcli == 0) {
  // code...
}else {
  if ($ot == 3) {
    ?>
    <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&ot=3&sp=<?php echo $sp ?>"><div class="" style="position:absolute; top:10%; height:3%; left:51%; width:14%; background-color:<?php echo $otbc ?>; z-index:1;">
    Partner Sales</div></a>
    <?php
  }else {
    ?>
    <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&ot=3&sp=<?php echo $sp ?>"><div class="" style="position:absolute; top:10%; height:3%; left:51%; width:14%; border:thin solid black; z-index:1;">
      Partner Sales</div></a>
      <?php
  }
}

if ($cCPIDsup == 0) {
}else {
  if ($ot == 4) {
    ?>
    <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&ot=4&sp=<?php echo $sp ?>"><div class="" style="position:absolute; top:10%; height:3%; left:83%; width:14%; background-color:<?php echo $otbc ?>; z-index:1;">
      Partner Purchases</div></a>
      <?php
  }else {
    ?>
    <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&ot=4&sp=<?php echo $sp ?>"><div class="" style="position:absolute; top:10%; height:3%; left:83%; width:14%; border:thin solid black; z-index:1;">
    Partner Purchases</div></a>
    <?php
  }
}
}
}
