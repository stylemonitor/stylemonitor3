<?php
include 'dbconnect.inc.php';
// echo "<b>incluDe/from_ACID_get_min_division_mtDID.inc.php</b>";

if (isset($_POST['tACID'])) {
  $ACID = $_POST['tACID'];
}elseif (isset($_GET['aid'])) {
  $ACID = $_GET['aid'];
}

// $CID = $_SESSION['CID'];

$sql = "SELECT MIN(d.ID) as DID
          , d.name as mDIDn
        FROM division d
          , associate_companies ac
        WHERE ac.ID = ?
        AND d.ACID = ac.ID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $ACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $mtDID = $row['DID'];
}
