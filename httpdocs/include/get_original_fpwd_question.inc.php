<?php
session_start();
include 'dbconnect.inc.php';

if (isset($_GET['fp'])) {
  $FPQID = $_GET['fp'];
}

$sql = "SELECT fpq.ID as FPQID
          , fpq.question as FPQIDq
        FROM forgotten_password_questions fpq
        WHERE fpq.ID = ?
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fugufpd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $FPQID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $FPQID = $row['FPQID'];
  $FPQIDq = $row['FPQIDq'];
}
?>
