 <?php
session_start();
include 'dbconnect.inc.php';
// echo "include/order_report.inc.php";

include 'set_urlPage.inc.php';

$CID = $_SESSION['CID'];
$td = date('U');

// Check the URL to see what page we are on
if (isset($_GET['lt'])) { $lt = $_GET['lt']; }else{ $lt = 0; }

// Set the rows per page
$r = 5;
// Check which page we are on
if ($lt > 1) { $start = ($lt * $r) - $r; }else { $start = 0; }

// sets the order type
// all open orders overdue

if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  if ($tab == 5) {
    $sp = 5;
    $divpos = 0;
    $tabpos = 3.2;
    $pagepos = 93;
    $OC = 0;
    $sd = $td - 604800;
    $ed = $td + 86400;
    $dr = 'o.inputtime';
    $cp = "New orders that have been placed in the last week";
    $hc = 'white';
    $hbc = '#693663';
    $r = 30;
  }
}

$sdate = date('d-M-Y', $sd);
$edate = date('d-M-Y', $ed);
$tdate = date('d-M-Y', $td);

if(isset($_GET['ord'])){ $ord = $_GET['ord']; }else{ $ord = 'oidd.item_del_date'; }
if(isset($_GET['sort'])){ $sort = $_GET['sort']; }else{ $sort = 'ASC'; }

// echo "<br>Company ID : $CID";
// echo "<br>Order status : $OC";
// echo "<br>Today : $td";
// echo "<br>Start day : $sd";
// echo "<br>End day : $ed";
// echo "<br>Count_orders - due date source : $dr";

include 'order_page_count.inc.php';

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cOID / $r);
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;
// echo '<br>Start record is : '.$start;
// echo "<br>**** count_orders count : $cOID ****";

if ($cOID < 1) {
  // echo "<br>NO orders placed";
  ?>
  <div class="trs" style="position:absolute; top:<?php echo $divpos ?>%; left:0%; width:100%;">
    <div class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "There are no orders"?></div>
  </div>
  <?php
}else {
  ?>
  <div class="cpsty" style="position:absolute; top:<?php echo $divpos ?>%; left:0%; width:100%; background-color:<?php echo $hbc ?>; padding-top:0.2%;"><a style="color:white; text-decoration:none;" href="home.php?H&aoe"><?php
    echo "Orders placed between $sdate and $edate";
    ?></a></div>
  <table class="trs" style="position:absolute; top:<?php echo $tabpos ?>%;">
    <tr>
      <th style="width:1%;"></th>
      <th style="width:5%;">Order</th>
      <th style="width:2%;">Item</th>
      <th style="width:15%; text-align:left; text-indent:5%;">Associate / Partner</th>
      <th style="width:17.5%; text-align:left; text-indent:5%;">OUR Order Ref</th>
      <th style="width:6%;">Short Code</th>
      <th style="width:4.5%; text-align: center;">Qty</th>
      <th style="width:4%; text-align: center;">Awt</th>
      <th style="width:4%; text-align: center;">MP1</th>
      <th style="width:4%; text-align: center;">MP2</th>
      <th style="width:4%; text-align: center;">MP3</th>
      <th style="width:4%; text-align: center;">MP4</th>
      <th style="width:4%; text-align: center;">MP5</th>
      <th style="width:4%; text-align: center;">Whse</th>
      <th style="width:4%; text-align: center;">Sent</th>
      <th style="width:8%; text-align:right; padding-right:2%;">Due date</th>
    </tr>
  <?php
  include 'order_QUERY.inc.php';
  ?>
  <div style="position:absolute; top:<?php echo $pagepos ?>%; right:5%; font-size:200%;">

    <?php
    // if (isset($_GET['H'])) {
    //   $page = 'H';
    // }elseif (isset($_GET['L'])) {
    //   $page = 'L';
    // }
      for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a style='text-decoration:none;' href='$urlPage&$sp&lt=$x'>  $x  </a>";
    }
    ?>
  </div>
  <?php
  } ?>
