<?php
include 'dbconnect.inc.php';
// echo "include/from_ACID_select_division_dd.inc.php";
include 'from_ACID_get_min_division.inc.php';
//
// if (isset($_POST['tCID'])) {
//   $ACID = $_POST['tCID'];
// }elseif (isset($_POST['fCID'])) {
//   $ACID = $_POST['fCID'];
// }else {
//   $ACID = $_SESSION['ACID'];
// }

$sql = "SELECT *
        FROM division
        WHERE ACID = ?
        AND name not in ('Select Division')
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fcgac</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $ACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_num_rows($result);
  if ($row == 0) {
    echo "No divisions";
    ?>
    <!-- <option value="">No Division to Select</option> -->
    <?php
  }elseif ($row > 0) {
    ?>
    <!-- <option value="">Please select a division</option> -->
    <?php
    while ($row = mysqli_fetch_assoc($result)) {
    ?>
    <option value="<?php echo $row['ID'] ?>"><?php echo $row['name'] ?></option>
    <?php
    }
  }
}
?>
