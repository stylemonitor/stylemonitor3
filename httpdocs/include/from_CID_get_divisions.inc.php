<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_get_divisions.inc.php</b>";

$sql = "SELECT d.ID as DID
          , d.name as DIDn
        FROM division d
        , associate_companies ac
          , company c
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  // echo '<b>FAIL-fcgd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $DID = $row['DID'];
    $DIDn = $row['DIDn'];
  }
}
