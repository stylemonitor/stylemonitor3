<?php
include ("dbconnect.inc.php");
// echo "<br><b>new_partner_response.inc.php</b>";
// echo "Acceptance of Client Partnership Request";
// echo "<br>The verification code is ";
if (isset($_GET['sa'])) {
  $acc_SEL = $_GET['sa'];
  $npr = a;
}elseif (isset($_GET['sr'])) {
  $acc_SEL = $_GET['sr'];
  $npr = r;
}elseif (isset($_GET['ca'])) {
  $acc_SEL = $_GET['ca'];
  $npr = a;
}elseif (isset($_GET['cr'])) {
  $acc_SEL = $_GET['cr'];
  $npr = r;
}

// echo "<br>acc_SEL value is $acc_SEL";
// echo "<br>Response value is $npr";

// // Start transaction
// mysqli_begin_transaction($mysqli);
// try {

  // Check that the verification code matches the code in the file
  $sql = "SELECT ID as CPID
            , used as CPIDu
          FROM company_partnerships
          WHERE acc_SEL = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-npr</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $acc_SEL);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $CPID = $row['CPID'];
    $CPIDu = $row['CPIDu'];
    echo "<br>Company Partnership ID (CPID) is $CPID";
  }

  if (empty($CPID)) {
    // echo "<br>This is NOT a valid confirmation";
    // header("Location:../index.php?err=050");
    // exit();
  }else {
    if ($CPIDu == 1) {
      // echo "<br>The validation key has already been used";
    }else {
      // echo "<br>This acceptance is valid";

      // Get todays date
      $td = date('U');

      // Update the file to link the two companies`
      $sql = "UPDATE company_partnerships
              SET active = ?
                , used = 1
              WHERE acc_SEL = ?
              ;";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b>FAIL-npr2</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ss", $npr, $acc_SEL);
        mysqli_stmt_execute($stmt);
      }

      header("Location:../index.php?P");
      exit();
    }
  }
// }  catch (mysqli_sql_exception $exception) {
//   mysqli_rollback($mysqli);
//
//   throw $exception;
// }
