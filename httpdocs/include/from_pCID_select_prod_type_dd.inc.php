<?php
include 'dbconnect.inc.php';
// include 'from_CID_select_prod_type_dd.inc.php';
//echo '<b>include/from_pCID_select_prod_type_dd.inc.php</b>';
// replaced pt.CID with pt.DIDchanged

$CID = $_SESSION['CID'];

$sql = "SELECT DISTINCT pt.ID as PTID
-- from_CID_select_prod_type_dd.sql
-- shows only product types where there is a product reference as well
          , pt.name as PTIDn
          , pt.scode as PTIDsc
          , c.ID as CID
        FROM product_type pt
          , prod_ref pr
          , company c
        WHERE c.ID = ?
        AND pt.CID = c.ID
        AND pt.name NOT IN ('Select Product Type')
        AND pr.PTID = pt.ID
        ORDER BY pt.name
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcsptd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($res)){
    $pPTID = $row['PTID'];
    $pPTIDn = $row['PTIDn'];
    $pPTIDsc = $row['PTIDsc'];

    ?>
    <option value="<?php echo $pPTID ?>"><?php echo "$pPTIDsc : $pPTIDn"?></option>
    <?php
  }
}
?>
