<?php
include 'dbconnect.inc.php';
$CID = $_SESSION['CID'];
// echo "include/from_CID_get_min_unit.inc.php";
// GET the company default make unit ID
$sql = "SELECT MIN(s.ID) as ID
        FROM company c
        , associate_companies ac
        , division d
        , section s
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND s.DID = d.ID;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br>FAIL-fagmmu';
}else {
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($res);
  $SID = $row['ID'];
  //echo '<br>The default make unit ID is ($SID) : <b>'.$SID.'</b>';
}
