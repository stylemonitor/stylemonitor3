<?php
// echo "<br><b>movement_table_item_update.inc.php</b>";

if (isset($_GET['rt1'])) {
  $rtnPage = "$urlPage&rt1&o=$OID&OI=$OIID&tab=$tab&ot=$OTID&sp=$sp";
}elseif (isset($_GET['rt2'])) {
  $rtnPage = "$urlPage&rt2&o=$OID&OI=$OIID";
}elseif (isset($_GET['rt3'])) {
  $rtnPage = "$urlPage&rt3&o=$OID&OI=$OIID";
}elseif (isset($_GET['rt4'])) {
  $rtnPage = "associates.php?A&rt4&o=$OID&OI=$OIID";
}elseif (isset($_GET['rt5'])) {
  $rtnPage = "home.php?H&rt5&o=$OID";
}

// for associate sales and purchases and for partner sales
if(($OTID == 1) || ($OTID == 2) || ($OTID == 3)) {

  // AWAITING
  if ($Awaiting == 0) {
    ?>
    <td  style="text-align: right;color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.1%;"><?php echo $Awaiting ?></td>
    <?php
  }else {
    ?>
    <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><a class="hreflink" href="<?php echo $rtnPage ?>&mp=0&val=<?php echo $Awaiting ?>"> <?php echo $Awaiting ?></a></td>
    <?php
  }

  // STAGE1
  if (($Awaiting == 0) && ($Stage1 == 0 )) {
    ?>
    <td  style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.1%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage1 ?></td>
    <?php
  }else {
    if ($Stage1 == 0) {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"></td>
      <?php
    }else {
      ?>
      <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><a class="hreflink" href="<?php echo $rtnPage ?>&mp=1&val=<?php echo $Stage1 ?>"><?php echo $Stage1 ?></a></td>

      <!-- <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><?php echo $Stage1 ?></td> -->
      <?php
    }
  }

  // STAGE 2
  if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 )) {
    ?>
    <td  style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.1%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage2 ?></td>
    <?php
  }else {
    if ($Stage2 == 0) {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"></td>
      <?php
    }else {
      ?>
      <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><a class="hreflink" href="<?php echo $rtnPage ?>&mp=2&val=<?php echo $Stage2 ?>"> <?php echo $Stage2 ?></a></td>

      <!-- <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><?php echo $Stage2 ?></td> -->
      <?php
    }
  }

  // STAGE 3
  if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 )) {
    ?>
    <td  style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.1%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage3 ?></td>
    <?php
  }else {
    if ($Stage3 == 0) {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"></td>
      <?php
    }else {
      ?>
      <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><a class="hreflink" href="<?php echo $rtnPage ?>&mp=3&val=<?php echo $Stage3 ?>"> <?php echo $Stage3 ?></a></td>

      <!-- <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><?php echo $Stage3 ?></td> -->
      <?php
    }
  }

  // STAGE4
  if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 ) && ($Stage4 == 0 )) {
    ?>
    <td  style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.1%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage4 ?></td>
    <?php
  }else {
    if ($Stage4 == 0) {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"></td>
      <?php
    }else {
      ?>
      <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><a class="hreflink" href="<?php echo $rtnPage ?>&mp=4&val=<?php echo $Stage4 ?>"> <?php echo $Stage4 ?></a></td>

      <!-- <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><?php echo $Stage4 ?></td> -->
      <?php
    }
  }

  // STAGE5
  if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 ) && ($Stage4 == 0 ) && ($Stage5 == 0 )) {
    ?>
    <td  style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.1%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage5 ?></td>
    <?php
  }else {
    if ($Stage5 == 0) {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"></td>
      <?php
    }else {
      ?>
      <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><a class="hreflink" href="<?php echo $rtnPage ?>&mp=5&val=<?php echo $Stage5 ?>"> <?php echo $Stage5 ?></a></td>

      <!-- <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><?php echo $Stage5 ?></td> -->
      <?php
    }
  }

  // WAREHOUSE
  if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 ) && ($Stage4 == 0 ) && ($Stage5 == 0 ) && ($Warehouse == 0 )) {
    ?>
    <td  style="text-align: right; border-right:thin solid <?php echo $whsCol ?>; padding-right:0.1%; color:<?php echo $whsCol ?>; background-color:<?php echo $whsCol ?>;"><?php echo $Warehouse ?></td>
    <?php
  }else {
    if ($Warehouse == 0) {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"></td>
      <?php
    }else {
      ?>
      <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><a class="hreflink" href="<?php echo $rtnPage ?>&mp=6&val=<?php echo $Warehouse ?>"><?php echo $Warehouse ?></a></td>

      <!-- <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><?php echo $Warehouse ?></td> -->
      <?php
    }
  }

  // Despatched
  if ($CLIENT == 0) {
    ?>
    <td  style="border-right:thin solid grey;"></td>
    <?php
  }else {
    ?>
    <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><b><?php echo $CLIENT ?></b></td>
    <!-- <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><b><a class="hreflink" href="<?php echo $rtnPage ?>&mp=7&val=<?php echo $CLIENT ?>"><?php echo $CLIENT ?></a></b></td> -->
    <?php
  }
}elseif ($OTID == 4) {
  // AWAITING
  if ($Awaiting == 0) {
  ?>
  <td  style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.1%;"><?php echo $Awaiting ?></td>
  <?php
  }else {
  ?>
    <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><?php echo $Awaiting ?></td>
  <?php
  }

  // STAGE1
  if (($Awaiting == 0) && ($Stage1 == 0 )) {
  ?>
  <td  style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.1%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage1 ?></td>
  <?php
  }else {
    if ($Stage1 == 0) {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"></td>
      <?php
    }else {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><?php echo $Stage1 ?></td>
      <?php
    }
  }

  // STAGE 2
  if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 )) {
  ?>
  <td  style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.1%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage2 ?></td>
  <?php
  }else {
    if ($Stage2 == 0) {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"></td>
      <?php
    }else {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><?php echo $Stage2 ?></td>
      <?php
    }
  }

  // STAGE 3
  if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 )) {
  ?>
  <td  style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.1%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage3 ?></td>
  <?php
  }else {
    if ($Stage3 == 0) {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"></td>
      <?php
    }else {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><?php echo $Stage3 ?></td>
      <?php
    }
  }

  // STAGE4
  if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 ) && ($Stage4 == 0 )) {
  ?>
  <td  style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.1%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage4 ?></td>
  <?php
  }else {
    if ($Stage4 == 0) {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"></td>
      <?php
    }else {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><?php echo $Stage4 ?></td>
      <?php
    }
  }

  // STAGE5
  if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 ) && ($Stage4 == 0 ) && ($Stage5 == 0 )) {
  ?>
  <td  style="text-align: right; border-right:thin solid <?php echo $mpCol ?>; padding-right:0.1%; color:<?php echo $mpCol ?>; background-color:<?php echo $mpCol ?>;"><?php echo $Stage5 ?></td>
  <?php
  }else {
    if ($Stage5 == 0) {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"></td>
      <?php
    }else {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><?php echo $Stage5 ?></td>
      <?php
    }
  }

  // WAREHOUSE
  if (($Awaiting == 0) && ($Stage1 == 0 ) && ($Stage2 == 0 ) && ($Stage3 == 0 ) && ($Stage4 == 0 ) && ($Stage5 == 0 ) && ($Warehouse == 0 )) {
  ?>
  <td  style="text-align: right; border-right:thin solid <?php echo $whsCol ?>; padding-right:0.1%; color:<?php echo $whsCol ?>; background-color:<?php echo $whsCol ?>;"><?php echo $Warehouse ?></td>
  <?php
  }else {
    if ($Warehouse == 0) {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"></td>
      <?php
    }else {
      ?>
      <td  style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><?php echo $Warehouse ?></td>
      <?php
    }
  }

  // Despatched
  if ($CLIENT == 0) {
  ?>
  <td  style="border-right:thin solid grey;"></td>
  <?php
  }else {
  ?>
  <td class="select" style="text-align: right; border-right:thin solid grey; padding-right:0.1%;"><b><a class="hreflink" href="<?php echo $rtnPage ?>&mp=7&val=<?php echo $CLIENT ?>"><?php echo $CLIENT ?></a></b></td>
  <?php
  }
}
