<?php
session_start();
// echo "include/page_selection_header.inc.php";
include 'SM_colours.inc.php';
include 'set_urlPage.inc.php';

include 'from_CID_count_partner_clients.inc.php';    // get the admin user of the company $cpCPID
include 'from_CID_count_partner_suppliers.inc.php';    // get the admin user of the company $cpCPID
include 'from_UID_get_user_details.inc.php';    // get the admin user of the company $cpCPID

if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
}

if ($itemPref <> 1) {
  $itemPref = $itemPref;
}

if ($clientPref <> 1) {
  $itemPref = $clientPref;
}

if (isset($_GET['ot'])) {
  // if ot is set then follow that
  $ot = $_GET['ot'];
  if ($ot == 1) {
    $otbc = '#f7cc6c';
  }elseif ($ot == 2) {
    $otbc = '#bbbbff';
  }elseif ($ot == 3) {
    $otbc = '#d68e79';
  }elseif ($ot == 4) {
    $otbc = '#c9d1ce';
  }
}else {
  // if it is NOT set then follow the users preference
  if (($clientPref == 1) && ($salesPref == 1)) {
    $ot = 1;
    $otbc = $salAssCol;
  }elseif (($clientPref == 1) && ($salesPref == 2)) {
    $ot = 2;
    $otbc = $purAssCol;
  }elseif (($clientPref == 2) && ($salesPref == 1)) {
    $ot = 3;
    $otbc = $salPrtCol;
  }elseif (($clientPref == 2) && ($salesPref == 2)) {
    $ot = 4;
    $otbc = $purPrtCol;
  }
}

if (isset($_GET['sp'])) {
  // if sp is set then follow that
  if ($sp == 1) {
    $spbc = $samCol;
  }elseif ($sp == 2) {
    $spbc = $proCol;
  }
}else {
  // if it is NOT set then follow the users preference
  if ($itemPref == 1) {
    $sp = 1;
    $spbc = "#fa7b7b";
  }elseif ($itemPref == 2) {
    $sp = 2;
    $spbc ="#76f8bc";
  }
}

// setting the background colour of the ORDER TIME FRAME selection boxes
if (isset($_GET['W'])) {
  if ($tab == 0) {
    $selCol = $goCol;
  }elseif ($tab == 1) {
    $selCol = $goCol;
  }elseif ($tab == 2) {
    $selCol = $giCol;
  }elseif ($tab == 3) {
    $selCol = $suCol;
  }

  // the box for MAKE TYPE
  ?>
  <div style="position:absolute; top:9%; height:5%; left:1%; width:24.25%;background-color: #f1f1f1; border:thin solid grey; border-bottom:thick solid <?php echo $selCol ?>"></div>
  <div style="position:absolute; top:13.5%; left:8.125%; width:10%; background-color: #f1f1f1; font-size: 80%; z-index:1;">Make Type</div>
  <?php
  if ($sp == 1) {
    ?>
    <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=1&ot=<?php echo $ot ?>"><div class="" style="position:absolute; top:10.5%; left:2%; width:10.625%; font-weight: bold; background-color: <?php echo $spbc ?>; z-index:1;">Sample</div></a>
    <?php
  }else {
    ?>
    <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=1&ot=<?php echo $ot ?>"><div class="" style="position:absolute; top:10.5%; left:2%; width:10.625%; border:thin solid black; z-index:1;">Sample</div></a>
    <?php
  }
  if ($sp == 2) {
    ?>
    <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=2&ot=<?php echo $ot ?>"><div class="" style="position:absolute; top:10.5%; left:13.625%; width:10.625%; font-weight: bold;  background-color: <?php echo $spbc ?>; z-index:1;">Production</div></a>
    <?php
  }else {
    ?>
    <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=2&ot=<?php echo $ot ?>"><div class="" style="position:absolute; top:10.5%; left:13.625%; width:10.625%; border:thin solid black; z-index:1;">Production</div></a>
    <?php
  }
  ?>
  <!-- <div style="position:absolute; top:9%; height:5%; left:26.25%; width:47.5%;background-color: #f1f1f1; border:thin solid grey; border-bottom:thick solid <?php echo $selCol ?>"></div>
  <div style="position:absolute; top:13.5%; left:45%; width:10%;background-color: #f1f1f1; font-size: 80%; z-index:1;">Order Type</div> -->
  <?php
}

?>
<div style="position:absolute; top:8.25%; left:31.25%; width:13.750%;background-color: #f1f1f1; font-size: 80%; z-index:1;">Associate</div>

<?php
if (($cCPIDcli == 0) && ($cCPIDsup == 0)) {
  ?>
  <div style="position:absolute; top:8.25%; left:55%; width:13.750%;color:grey; background-color: #f1f1f1; font-size: 80%; z-index:1;">Partner</div>
  <?php
}else {
?>
<div style="position:absolute; top:8.25%; left:55%; width:13.750%; background-color: #f1f1f1; font-size: 80%; z-index:1;">Partner</div>
<?php
}
?>

<div style="position:absolute; top:9%; height:5%; left:26.25%; width:47.5%;background-color: #f1f1f1; border:thin solid grey; border-bottom:thick solid <?php echo $selCol ?>"></div>
<div style="position:absolute; top:13.5%; left:45%; width:10%;background-color: #f1f1f1; font-size: 80%; z-index:1;">Order Type</div>
<?php

// GOODS OUTWARD WAREHOUSE
if ($tab == 1) {
  if ($ot == 1) {
    ?>
    <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=1"><div class="" style="position:absolute; top:10.5%; left:27.25%; width:10.625%; font-weight: bold; background-color:<?php echo $otbc ?>; z-index:1;">Sales</div></a>
      <?php
  }else {
      ?>
      <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=1"><div class="" style="position:absolute; top:10.5%; left:27.25%; width:10.625%; border:thin solid black; z-index:1;">Sales</div></a>
      <?php
  }

  ?>
  <div class="" style="position:absolute; top:10.5%; left:38.875%; width:10.625%; color:grey; z-index:1;">Purchases</div>

  <?php
  if ($cCPIDcli == 0) {
    ?>
    <div class="" style="position:absolute; top:10.5%; left:50.5%; width:10.625%; color:grey; z-index:1;">Sales</div>
    <?php
  }else {
    if ($ot == 4) {
      ?>
      <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=4"><div style="position:absolute; top:10.5%; left:50.5%; width:10.625%; font-weight: bold; background-color:<?php echo $otbc ?>; z-index:1;">Sales</div></a>
      <?php
    }else {
      ?>
      <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=4"><div class="" style="position:absolute; top:10.5%; left:50.5%; width:10.625%; border:thin solid black; z-index:1;">Sales</div></a>
      <?php
    }
  }

  if ($cCPIDsup == 0) {
    ?>
    <div class="" style="position:absolute; top:10.5%; left:62.125%; width:10.625%; color:grey; z-index:1;">Purchases</div>
    <?php
  }else {
    if ($ot == 4) {
      ?>
      <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=4"><div style="position:absolute; top:10.5%; left:62.125%; width:10.625%; font-weight: bold; background-color:<?php echo $otbc ?>; z-index:1;">Purchases</div></a>
      <?php
    }else {
      ?>
      <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=4"><div class="" style="position:absolute; top:10.5%; left:62.125%; width:10.625%; border:thin solid black; z-index:1;">Purchases</div></a>
      <?php
    }
  }
}

// GOODS INWARDS WAREHOUSE
if ($tab == 2) {
  ?>
  <div class="" style="position:absolute; top:10.5%; left:27.25%; width:10.625%; color:grey; z-index:1;">Sales</div>
  <?php

  if ($ot == 2) {
    ?>
    <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=2"><div class="" style="position:absolute; top:10.5%; left:38.875%; width:10.625%; font-weight: bold; background-color:<?php echo $otbc ?>; z-index:1;">Purchases</div></a>
      <?php
  }else {
      ?>
      <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=2"><div class="" style="position:absolute; top:10.5%; left:38.875%; width:10.625%; border:thin solid black; z-index:1;">Purchases</div></a>
      <?php
  }

  ?>
  <div class="" style="position:absolute; top:10.5%; left:50.5%; width:10.625%;color:grey; z-index:1;">Sales</div>
  <?php

  if ($cCPIDsup == 0) {
    ?>
    <div class="" style="position:absolute; top:10.5%; left:62.125%; width:10.625%; color:grey; z-index:1;">Purchases</div>
    <?php
  }else {
    if ($ot == 4) {
      ?>
      <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=4"><div style="position:absolute; top:10.5%; left:62.125%; width:10.625%; font-weight: bold; background-color:<?php echo $otbc ?>; z-index:1;">Purchases</div></a>
      <?php
    }else {
      ?>
      <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=4"><div class="" style="position:absolute; top:10.5%; left:62.125%; width:10.625%; border:thin solid black; z-index:1;">Purchases</div></a>
      <?php
    }
  }
}

// SUPPLIERS WAREHOUSE
if ($tab == 3) {
    ?>
    <div class="" style="position:absolute; top:10.5%; left:27.25%; width:10.625%;color:grey; z-index:1;">Sales</div>
    <?php

  if ($ot == 2) {
    ?>
    <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=2"><div class="" style="position:absolute; top:10.5%; left:38.875%; width:10.625%; font-weight: bold; background-color:<?php echo $otbc ?>; z-index:1;">Purchases</div></a>
      <?php
  }else {
      ?>
      <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=2"><div class="" style="position:absolute; top:10.5%; left:38.875%; width:10.625%; border:thin solid black; z-index:1;">Purchases</div></a>
      <?php
  }

  ?>
  <div class="" style="position:absolute; top:10.5%; left:50.5%; width:10.625%;color:grey; z-index:1;">Sales</div>
  <?php

  if ($cCPIDsup == 0) {
    ?>
    <div class="" style="position:absolute; top:10.5%; left:62.125%; width:10.625%; color:grey; z-index:1;">Purchases</div>
    <?php
  }else {
    if ($ot == 4) {
      ?>
      <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=4"><div style="position:absolute; top:10.5%; left:62.125%; width:10.625%; font-weight: bold; background-color:<?php echo $otbc ?>; z-index:1;">Purchases</div></a>
      <?php
    }else {
      ?>
      <a href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&sp=<?php echo $sp ?>&ot=4"><div class="" style="position:absolute; top:10.5%; left:62.125%; width:10.625%; border:thin solid black; z-index:1;">Purchases</div></a>
      <?php
    }
  }
}
