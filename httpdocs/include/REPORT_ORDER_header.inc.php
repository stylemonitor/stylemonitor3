<?php
// echo "<br><b>include/report_order_header.inc.php</b>";
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
include 'set_urlPage.inc.php';
include 'URL_selections.inc.php';

$CID = $_SESSION['CID'];

if (isset($_GET['o'])) {
  $OID = $_GET['o'];
  $tab = "&o=$OID";
  include 'from_OID_get_order_details.inc.php';
  $head2 = "Summary";
  if (isset($_GET['OI'])) {
    $head2 = "Item Activity Log";
  }
}else {
  $OID = $OID;
}

if ($OTID == 1) {
  $pddh = "Sales Order $head2";
  $pddhbc = $salAssCol;
  $cli_sup = 'Client';
  $compName = $ACIDnC;
}elseif ($OTID == 2) {
  $pddh = "Purchase Order $head2";
  $pddhbc = $purAssCol;
  $cli_sup = 'Supplier';
  $compName = $ACIDnC;
}elseif ($OTID == 3) {
  $pddh = "Partner Sales Order $head2";
  $pddhbc = $salPrtCol;
  $cli_sup = 'Partner Client';
  $compName = $ACIDnC;
}elseif ($OTID == 4) {
  $pddh = "Partner Purchase Order $head2";
  $pddhbc = $purPrtCol;
  $cli_sup = 'Partner Supplier';
  $compName = $ACIDnC;
}

// include 'page_description_date_header.inc.php';

include 'from_CID_count_divisions.inc.php';
include 'from_OIID_count_make_unit.inc.php';
include 'from_OIID_get_partners_CID.inc.php';

// echo "OTID=$OTID";

?>

<div style="position:absolute; top:9%; height:22%; left:1%; width:98%;
  border-left:thin solid grey;
  border-top:thin solid grey;
  border-right:thin solid grey;
  border-bottom:thick solid <?php echo $pddhbc ?>;"></div>
<div style="position:absolute; top:10%; left:3%; width:20%; font-size: 110%; font-weight:bold; text-align:left; color:#887f7f;"><?php echo $cli_sup ?></div>

<?php
if ($cDID == 0) {
}else {
  ?>
  <!-- our divisional detail -->
  <div style="position:absolute; top:10%; left:39%; width:38%; color:#887f7f; text-align:left; z-index:1;">Division</div>
  <div style="position:absolute; top:13%; height:5%; left:40%; width:25%; background-color:<?php echo $bbCol ?>; font-size:200%; text-align: left;">
    <?php echo $DIDnC ?>
  </div>
  <?php
}
?>
<!-- TO Company name -->
<div style="position:absolute; top:13%; height:2%; left:3%; width:35%; background-color:<?php echo $bbCol ?>; font-size:200%; font-weight: bold; text-align: left;">
  <?php echo $ACIDnC ?>
</div>
<?php
if ($DIDnC == "Select Division") {$DIDnC = "";}else { $DIDnC = $DIDnC;}
?>
<div style="position:absolute; top:11.5%; height:6%; left:65%; width:15%; color:#887f7f; background-color:<?php echo $bbCol ?>; font-size:200%; text-align: right;">Order :</div>
<div style="position:absolute; top:10.5%; height:5%; right:2%; width:12.8%; background-color:<?php echo $bbCol ?>; font-size:200%; text-align: right;"><a style="color:blue; text-decoration:none;" href="home.php?H&o=<?php echo $OID ?>"><?php echo $oref ?></a></div>

<div style="position:absolute; top:18.5%; left:1.5%; width:19%; color:#887f7f; background-color:<?php echo $bbCol ?>; font-size:100%; text-align: right;">Their Order Reference :</div>
<div style="position:absolute; top:18.5%; left:21%; width:50%; color:#887f7f; background-color:<?php echo $bbCol ?>; font-size:100%; text-align: left;"><?php echo $OIDtor ?></div>

<div style="position:absolute; top:16.5%; height:5%; left:65%; width:15%; color:#887f7f; background-color:<?php echo $bbCol ?>; font-size:150%; text-align: right;">
  Order Taken:
</div>
<div style="position:absolute; top:16.5%; height:5%; right:2%; width:12.8%; color:black; background-color:<?php echo $bbCol ?>; font-size:150%; text-align: right;">
  <?php echo $odel ?>
</div>

<!-- _____________________________________________________________ -->
<div style="position:absolute; top:21.5%; left:1%; width:98%; border-bottom:thin solid <?php echo $pddhbc ?>; z-index:1;"></div>

<!-- <div style="position:absolute; top:21%; left:1%; width:38%; color:#887f7f; text-align:left; z-index:1;"></div> -->
<div style="position:absolute; top:24%; left:3%; width:35%; font-weight:bold; font-size: 140%; text-align:left; background-color:<?php echo $bbCol ?>; z-index:1;"><?php echo $ACIDnS ?></div>

<!-- THEIR DIVISIONAL DETAIL -->
<!-- <div style="position:absolute; top:24%; left:40%; width:40%; font-size: 140%; text-align:left; background-color:<?php echo $bbCol ?>; z-index:1;"><?php echo $DIDnS ?></div> -->

<div style="position:absolute; top:27.5%; left:1.5%; width:19%; color:#887f7f; background-color:<?php echo $bbCol ?>; font-size:100%; text-align: right;">Our Order Reference :</div>
<div style="position:absolute; top:27.5%; left:21%; width:50%; color:#887f7f; background-color:<?php echo $bbCol ?>; font-size:100%; text-align: left;"><?php echo $OIDcor ?></div>
<?php
// if ($OTID == 3) {
  ?>
  <div style="position:absolute; top:27.5%; left:61%; width:30%; color:#887f7f; background-color:<?php echo $bbCol ?>; font-size:100%; text-align: left;">Our Item Ref : </div>
  <div style="position:absolute; top:27.5%; left:71%; width:15%; color:#887f7f; background-color:<?php echo $bbCol ?>; font-size:100%; text-align: left;">hhhh</div>
  <?php
// }else {
// }
?>

<!-- _____________________________________________________________ -->

<form action="home.php?H&o=<?php echo $OID ?>&e" method="post">
<!-- <form action="include/form_edit_order_details.inc.php" method="post"> -->
  <input type="hidden" name="OID" value="<?php echo $OID ?>">
  <input type="hidden" name="url" value="<?php echo $urlPage ?>">
  <input type="hidden" name="tab" value="<?php echo $tab ?>">
  <button class="entbtn" type="submit" style="position:absolute; top:26.2%; height:4%; left:92%; width:6%; background-color: <?php echo $edtCol ?>" name="editOrdetails">Edit</button>
</form>

<?php
if (($OTID == 3) || ($OTID == 4)) {
  $sql = "SELECT COUNT(oic.ID) as cOICID
          -- report_order_header.sql
          FROM order_item_change oic
            , order_placed_move opm
            , order_placed op
            , orders o
            , order_item oi
            , order_item_movement_reason oimr
          WHERE o.ID = ?
          AND oic.OPMID = opm.ID
          AND opm.OPID = op.ID
          AND op.OIID = oi.ID
          AND oi.OID = o.ID
          AND oic.status = 0
          AND opm.OIMRID = oimr.ID;
  ";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-ROh</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $OID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_assoc($result);
    $cOICID = $row['cOICID'];
  }
  if (!empty($cOICID)) {
    include 'form_change_request_approval.inc.php';
  }
}
