<?php
include 'dbconnect.inc.php';
// echo "<br>input_company_address.inc.php";

// echo "<br>rCID : $rec_CID";
// echo "<br>CTID : $CTID";
// echo "<br>UIDy : $UIDy";
// echo "<br>UID : $UID";
// echo "<br>UIDc : $UIDc";
// echo "<br>td : $td";

$sql = "INSERT INTO addresses
          (CYID, TID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-ica";
}else {
  mysqli_stmt_bind_param($stmt,"ssss", $UIDy, $TID, $UID, $td);
  mysqli_stmt_execute($stmt);
}

// Get the address ID
$sql = "SELECT ID as AID
        FROM addresses
        WHERE CYID = ?
        AND UID = ?
        AND inputtime = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-ica2";
}else {
  mysqli_stmt_bind_param($stmt,"sss", $UIDy, $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $AID  = $row['AID'];
}
// echo "<br>Adress ID == $AID";

// Add an address to the company
$sql = "INSERT INTO company_associates_address
          (ACID, AID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-ica3";
}else {
  mysqli_stmt_bind_param($stmt,"ssss", $ACID, $AID, $UID, $td);
  mysqli_stmt_execute($stmt);
}

// Add an address to the division
$sql = "INSERT INTO company_division_address
          (DID, AID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-ica4";
}else {
  mysqli_stmt_bind_param($stmt,"ssss", $DID, $AID, $UID, $td);
  mysqli_stmt_execute($stmt);
}

// Add an address to the section
$sql = "INSERT INTO company_section_address
          (SID, AID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iucd";
}else {
  mysqli_stmt_bind_param($stmt,"ssss", $SID, $AID, $UID, $td);
  mysqli_stmt_execute($stmt);
}
