<?php
include 'dbconnect.inc.php';

session_start();

// echo 'LEAVING SO SOON';

// $UID = $_SESSION['UID'];

if (!isset($_POST['all-out'])) {
  echo "WRONG METHOD";
}else {
  $UID = $_GET['MU'];
  $daytimeend = date("U");

  // Change the login_registry entry
  // from 0 to 1
  $sql = "UPDATE login_registry
        SET loginstatus = 1
        , logouttime = ?
        WHERE UID = ?
        AND loginstatus = 0;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
   echo '<b>FAIL6</b>';
  }else{
  mysqli_stmt_bind_param($stmt, "ss", $daytimeend, $UID);
  mysqli_stmt_execute($stmt);
  }

  session_unset();
  session_destroy();

  header("Location: ../index.php?h");
  exit();
}
