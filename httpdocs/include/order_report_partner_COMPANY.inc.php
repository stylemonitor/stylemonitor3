<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "include/order_report_partner_COMPANY.inc.php";

$CID = $_SESSION['CID'];

if (isset($_GET['oc'])) {
  $OC = $_GET['oc'];
}else {
  $OC = 0;
}

$td = date('U');

include 'from_ACID_get_associate_company_details.inc.php';
// echo "ACID is $ACID";

$pddh = "Company Review : $ACIDn";
$pddhbc = '#fffddd';
include 'page_description_date_header.inc.php';

// Check the URL to see what page we are on
if (isset($_GET['lt'])) { $lt = $_GET['lt']; }else{ $lt = 0; }

// Set the rows per page
$r = 5;
// Check which page we are on
if ($lt > 1) { $start = ($lt * $r) - $r; }else { $start = 0; }

// sets the order type
// all open orders overdue
if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  if ($tab == 1) {
    $bc ='f7cc6c';
  }elseif ($tab == 2) {
    $bc ='bbbbff';
  }elseif ($tab == 3) {
    $bc ='d68e79';
  }elseif ($tab == 4) {
    $bc ='c9d1ce';
  }
  $sp = 'asc';
  $divpos = 10;
  $tabpos = 13.2;
  $pagepos = 92;
  // $OC = 0;
  $cp = "orders due out after 2 weeks";
  $hc = 'black';
  $hbc = '#92ef9d';
  $r = 20;
}elseif (isset($_GET['asf'])) {
  $sp = 'asc';
  $divpos = 32;
  $tabpos = 35.2;
  $pagepos = 92;
  // $OC = 0;
  $cp = "orders due out after 2 weeks";
  $hc = 'black';
  $hbc = '#92ef9d';
  $r = 20;
}elseif (isset($_GET['asc'])) {
  $sp = 'asc';
  $divpos = 9;
  $tabpos = 12.2;
  $pagepos = 92;
  // $OC = 0;
  $cp = "orders due out after 2 weeks";
  $hc = 'black';
  $hbc = '#92ef9d';
  $r = 20;
}

$sdate = date('d-M-Y', $sd);

if(isset($_GET['ord'])){ $ord = $_GET['ord']; }else{ $ord = 'oidd.item_del_date'; }
if(isset($_GET['sort'])){ $sort = $_GET['sort']; }else{ $sort = 'ASC'; }

// echo "<br>Company ID : $CID";
// echo "<br>Order status : $OC";
// echo "<br>Today : $td";
// echo "<br>Start day : $sd";
// echo "<br>End day : $ed";
// echo "<br>Count_orders - due date source : $dr";

// include 'order_page_count.inc.php';
$sql = "SELECT COUNT(o.ID) as OID
        FROM orders o
          , company c
          , associate_companies ac
          , division d
          , order_item oi
          , order_item_del_date oidd
        WHERE o.CPID IN(SELECT cp.ID
            						FROM company_partnerships cp
            						WHERE cp.req_CID = ? OR cp.acc_CID = ?)
        AND o.orComp = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND o.tDID = d.ID
        AND oi.OID = o.ID
        AND oidd.OIID = oi.ID
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-or1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $OC);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOID = $row['OID'];
}


// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cOID / $r);
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;
// echo '<br>Start record is : '.$start;
// if ($p > 1) { $start = ($p * $r) - $r; }else { $start = 0; }

if ($cOID < 1) {
  // echo "<br>NO orders placed";
  ?>
  <div class="trs" style="position:absolute; top:16%; left:0%; width:100%;">
    <div class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "There are 0 Orders"?></div>
  </div>
      <!-- <p>NO ORDERS TO REVIEW count = <?php echo $cOID ?></p> -->
  <?php
}else {
  ?>
  <div class="cpsty" style="position:absolute; top:30%; left:0%; width:100%; background-color:<?php echo $hbc ?>; padding-top:0.2%;">Order/Items Reviewddt</div>
  <table class="trs" style="position:absolute; top:34%;">
    <tr>
      <?php
      include 'Table_headings_order_QUERY_company.inc.php';
      ?>
    </tr>
    <?php
    include 'order_query_company.inc.php';
    ?>
    <div style="position:absolute; top:<?php echo $pagepos ?>%; right:5%; font-size:200%;">

      <!-- <?php
      if (isset($_GET['H'])) {
        $page = 'H';
      }elseif (isset($_GET['L'])) {
        $page = 'L';
      }
        for ($x = 1 ; $x <= $PTV ; $x++){
        echo "<a style='color:red; text-decoration:none;' href='?$page&$sp&lt=$x'>  $x  </a>";
      }
      ?> -->
    </div>
  <?php
} ?>
