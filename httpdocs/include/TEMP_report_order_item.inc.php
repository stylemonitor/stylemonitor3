<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/TEMP_report_order_item.inc.php";
?>

<table class="trs" style="position:absolute; top:61%;">
  <tr>
    <th>Item Nos</th>
    <th>Ref</th>
    <th>PRID</th>
    <th>Qty</th>
    <th>Due Date</th>
  </tr>
  <tr>
    <?php
    $sql = "SELECT UID as UID
              , itemNos as itemNos
              , PRID as PRID
              , their_ref as their_ref
              , qty as qty
              , item_del_date as delDate
            FROM TEMP_order_item_record
            WHERE UID = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-Troi';
    }else {
      mysqli_stmt_bind_param($stmt, "s", $UID);
      mysqli_stmt_execute($stmt);
      $res = mysqli_stmt_get_result($stmt);
      while($row = mysqli_fetch_array($res)){
        $UID = $row['UID'];
        $itemNos = $row['itemNos'];
        $PRID = $row['PRID'];
        $their_ref = $row['their_ref'];
        $qty = $row['qty'];
        $delDate = $row['delDate'];

        $delDate = date('d-M-Y', $delDate);
        ?>
        <td><?php echo $itemNos ?></td>
        <td><?php echo $their_ref ?></td>
        <td><?php echo $PRID ?></td>
        <td><?php echo $qty ?></td>
        <td><?php echo $delDate ?></td>
      </tr>
        <?php
      }
    }
    ?>
</table>
