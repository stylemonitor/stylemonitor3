<?php
session_start();
include 'dbconnect.inc.php';
// echo '<br><b>form_reg_act.inc.php</b>';

if (!isset($_POST['reg']) AND !isset($_POST['reg_can'])) {
  // echo "<br>INCORRECT ACCESS METHOD";
  header('Location:../index.php?h');
  exit();
}elseif (isset($_POST['reg_can'])) {
  // echo "<br>Cancel Registration";
   header("Location:../index.php?h");
   exit();
}elseif (isset($_POST['reg'])) {
  // echo "<br>Correct register method used";

  // GET the info from the form
  $UIDf = $_POST['first'];
  $UIDs = $_POST['sur'];
  $CIDn = $_POST['company'];
  $CTID = 7;
  // $CTID = $_POST['CTID'];
  $UIDn = $_POST['position'];
  $UIDe = $_POST['email'];
  $CYID = $_POST['CYID'];
  $TZID = $_POST['TZID'];
  $rec_CID = $_POST['rec_CID'];
  // data set when the enter button is used

  include 'set_vkey.inc.php';

  $td = date('U');

  $ans = $_POST['check_ans'];
  $ab1 = $_POST['ab1'];
  $ab2 = $_POST['ab2'];

  $ab3 = $ab1 + $ab2;

  // GET the rec_CID ID number
  if (empty($rec_CID)) {
    $rec_CID = 1;
  }else {
    $sql = "SELECT ID
            FROM company
            WHERE SMIC = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-frad2</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "s", $rec_CID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $rec_CID = $row['ID'];
    }
  }

  // echo '<br>User first name (UIDf) : <b>'.$UIDf.'</b>';
  // echo '<br>User surname (UIDs) : <b>'.$UIDs.'</b>';
  // echo '<br>User email (UIDe) : <b>'.$UIDe.'</b>';
  // echo '<br>User verification code name (vkey) : <b>'.$vkey.'</b>';
  // echo '<br>Todays date (td)<b>'.$td.'</b>';
  // echo '<br>User company name (CIDn) : <b>'.$CIDn.'</b>';
  // echo '<br>Company type (CTID) : <b>'.$CTID.'</b>';
  // echo '<br>User position name (UIDn) : <b>'.$UIDn.'</b>';
  // echo '<br>User country ID (CYID) : <b>'.$CYID.'</b>';
  // echo '<br>TimeZone ID (TZID) : <b>'.$TZID.'</b>';
  // echo '<br>Recommended by (rec_CID): <b>'.$rec_CID.'</b>';

  // Check if they have got hte right maths answer
  if ($ans <> $ab3) {
    // echo "<br>Maths value incorrect ans/ab3 = $ans::$ab3";
    header("Location:../index.php?i&SM&fn=$UIDf&s=$UIDs&co=$CIDn&p=$UIDn&e=$UIDe&ma");
    exit();
  }else {
    // echo "<br>Maths value correct ans/ab3 = $ans::$ab3";
    // echo "<br>Check if the email has been used before";
    include 'check_smauthorised_user.inc.php';
  }
include 'check_email_use.inc.php';
}
