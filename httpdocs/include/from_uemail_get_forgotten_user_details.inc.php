<?php
include 'dbconnect.inc.php';
// CHECK the email is registered
// echo "include/from_uemail_get_forgotten_user_details.inc.php";

$sql = "SELECT ID as UID
          , firstname as UIDf
          , active as UIDa
          , privileges as UIDv
          , email as email
        FROM users
        WHERE email = ?
        OR uid = ?
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fugfud</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $uid, $uid);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  if($row = mysqli_fetch_assoc($result)){
    $UID  = $row['UID'];
    $UIDf  = $row['UIDf'];
    $UIDa = $row['UIDa'];
    $UIDv = $row['UIDv'];
    $email = $row['email'];
  }else {
    $UID = '0';
  }
}
