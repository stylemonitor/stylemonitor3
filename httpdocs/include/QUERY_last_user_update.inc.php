<?php
include 'dbconnect.inc.php';
?>
SELECT opm_order.OPID AS OPI
                , opm_order.UID AS UID
                , opm_order.inputtime AS inputtime
            FROM (SELECT opm.OPID
                  , opm.UID
                  , opm.inputtime
                FROM order_placed_move opm
                  INNER JOIN order_placed op ON op.ID = opm.OPID
                  INNER JOIN order_item oi ON oi.ID = op.OIID
                  INNER JOIN orders o ON oi.OID = o.ID
                ORDER BY opm.OPID, opm.ID DESC
                ) opm_order
            GROUP BY opm_order.OPID
