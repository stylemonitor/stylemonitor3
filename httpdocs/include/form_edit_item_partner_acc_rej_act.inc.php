<?php
include 'dbconnect.inc.php';
session_start();
$UID = $_SESSION['UID'];
$td = date('U');
// echo "<br><b>form_edit_item_partner_acc_rej_act.inc.php</b>";

if (!isset($_POST['appRqt']) && !isset($_POST['rjtRqt']) && !isset($_POST['NoBtn'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['rjtRqt']) || isset($_POST['appRqt'])) {
  echo "<br>Reject OR Approval";
  // select if PARTNERSHIP or ASSOCIATE
  // CAN only be a partnership if you have gotten this far!!!

  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  $OIMRID = $_POST['OIMRIDv'];
  $canReas = $_POST['canReas'];
  $OICID = $_POST['OICID'];
  $OPID = $_POST['OPID'];
  $Nqty = $_POST['Nqty'];
  $status = $_POST['status'];
  $itComp = $_POST['itComp'];

  $Ndate = $_POST['Ndate'];
  $OIDDIDd = $_POST['OIDDIDd'];
  $opmUID = $_POST['opmUID'];

  // echo "<br>OID=$OID::OIID=$OIID::OIMRID=$OIMRID::Nqty=$Nqty::canReas=$canReas::OICID=$OICID::OPID=$OPID::status=$status::itComp=$itComp";

  if (empty($Nqty)) {
    $Nqty = 0;
  }else {
    $Nqty = $Nqty;
  }
  // echo ":::qty=$Nqty::UID=$UID";

  include 'action/insert_into_order_place_move.act.php';
  // get the OPMID for the new update
  include 'action/select_OPMID_acc_rej.act.php';
  include 'action/update_order_item_change_table.act.php';

  // IF the item has been completed update the order_item table
  // include 'action/update_order_item_table_ITEM_COMPLETED.act.php';

  // DATE CHANGE APPROVAL
  include 'action/insert_into_log_order_item_del_date.act.php';
  include 'action/select_OIDDID_from_OIID.act.php';
  include 'action/update_order_item_del_date.act.php';

}elseif (isset($_POST['NoBtn'])) {
  // echo "NO button pressed";
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  header("Location:../home.php?H&rt3&o=$OID&OI=$OIID");
  exit();
}

  // return to the page you left!
  header("Location:../home.php?H&rt3&o=$OID&OI=$OIID&iii");
  exit();
