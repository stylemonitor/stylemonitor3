<?php
echo "<b>form_edit_item_partner_acc_rej.inc.php</b>";
include 'SM_colours.inc.php';

if (isset($_GET['oiy'])) {
  $OICID = $_GET['oiy'];
  $OIID = $_GET['OI'];
  $answer = "APPROVAL";
  $bc = $fsvCol;
  $abc = "white";
  $rbc = "$fsvCol";
  $btnLabel = "appRqt";
  $askWhy = "Comment";
}elseif (isset($_GET['oin'])) {
  $OICID = $_GET['oin'];
  $OIID = $_GET['OI'];
  $answer = "REJECTION";
  $bc = $fcnCol;
  $abc = "white";
  $rbc = $fcnCol;
  $btnLabel = "rjtRqt";
  $askWhy = "Please state why you are refusing this request";
}

include 'from_OICID_get_request_data.inc.php';
include 'action/select_OIMRID_from_OPMID.act.php';
echo "<br>OIMRID=$OIMRID";

// $OIDDIDd = date('d-M-Y', $OIDDIDd);
$NdateView = date('d-M-Y', $Ndate);
$OIDDIDdView = date('d-M-Y', $OIDDIDd);
echo "//$OIDDIDd($OIDDIDdView)::$Ndate($NdateView)";

if ($OIMRID == 40) {

  $rqt = "a <b>DELIVERY DATE CHANGE</b>";
  $rqtNote = "From : $OIDDIDdView to <b>$NdateView";
  if (isset($_GET['oiy'])) {
    $OIMRIDv = 39;
  }else {
    $OIMRIDv = 41;
  }
}elseif ($OIMRID == 43) {

  $rqt = "an <b>ITEM QUANTITY CHANGE</b>";
  $redQty = $oiQty - $Nqty;
  $rqtNote = "From : <b>$oiQty</b> to <b>$redQty</b>";
  // Reduce item quantity by redQty

  if (isset($_GET['oiy'])) {
    $OIMRIDv = 10;
  }else {
    $OIMRIDv = 44;
  }
}elseif ($OIMRID == 45) {
  // selects approval or rejection OIMRID value to be put into the db
  $rqt = "an <b>ITEM CANCELLATION</b>";
  $rqtNote = "";

  // need to select whether it is a DATE/QTY/CANCELLATION reqest and set status accordingly

  if (isset($_GET['oiy'])) {
    $OIMRIDv = 46;
  }else {
    $OIMRIDv = 47;
  }
}

// STATUS is the order_item_change value status = 0 = pending :: status = 1 = reject :: stauts = 2 = approved
if (($OIMRIDv == 39) || ($OIMRIDv == 10) || ($OIMRIDv == 46)) {
  $status = 2;
  $itComp = 0;
}elseif (($OIMRIDv == 41) || ($OIMRIDv == 44) || ($OIMRIDv == 47)) {
  $status = 1;
  $itComp = 0;
}

// // $Ndate = date('d-M-Y', $Ndate);
// if (isset($_GET['oiy'])){
//   $OIMRIDv = 46;
// }elseif(isset($_GET['oin'])) {
//   $OIMRIDv = 47;
// }

?>
<div class="overlay" style="z-index:2;"></div>

<div style="position:absolute; top:0%; height:100%; left:0%; width:100%; background-blend-mode:overlay; z-index:3;"></div>

<div style="position:absolute; top:30%; height:30%; left:30%; width:40%; background-color: <?php echo $bc ?>; font-size:120%; border: 2px solid grey; border-radius: 5px; z-index:3;">
  <b>Confirm <?php echo $answer ?> of </b>
  <br><br><?php echo "$rqt" ?> request
  <br><br><?php echo $rqtNote ?></b>
</div>

<form action="include/form_edit_item_partner_acc_rej_act.inc.php" method="post">
  <input type="hidden" name="OID" value="<?php echo $OID ?>">
  <input type="hidden" name="OIID" value="<?php echo $OIID ?>">
  <input type="hidden" name="OICID" value="<?php echo $OICID ?>">
  <input type="hidden" name="OPID" value="<?php echo $OPID ?>">
  <input type="hidden" name="Nqty" value="<?php echo $Nqty ?>">
  <input type="hidden" name="OIMRIDv" value="<?php echo $OIMRIDv ?>">
  <input type="hidden" name="status" value="<?php echo $status ?>">
  <input type="hidden" name="itComp" value="<?php echo $itComp ?>">
  <input type="text" name="Ndate" value="<?php echo $Ndate ?>">
  <input type="text" name="OIDDIDd" value="<?php echo $OIDDIDd ?>">
  <input type="text" name="opmUID" value="<?php echo $opmUID ?>">
  <textarea autofocus required style="position:absolute; top:47%; left: 31%; width:38%; z-index:3;" name="canReas" value="" placeholder="<?php echo $askWhy ?>"></textarea>

  <button class="entbtn" type="submit" style="position:absolute; top:55%; left:37.5%; width:10%; background-color:<?php echo $abc ?>; z-index:3;" name="<?php echo $btnLabel ?>">CONFIRM222</button>
  <button formnovalidate class="entbtn" type="submit" style="position:absolute; top:55%; left:52.5%; width:10%; background-color:<?php echo $rbc ?>; z-index:3;" name="NoBtn">CANCEL</button>
</form>
