<?php
session_start();
include 'dbconnect.inc.php';
// echo '<b>include/form_new_product_type_act.inc.php</b>';
// echo '<br>FROM:form_new_product_type.inc.php</b>';
if (!isset($_POST['prob'])){
  echo 'Incorrect method used';
  // header('Location:../index.php');
  // exit();
}elseif (isset($_POST['prob'])){
  // echo 'Problem RECORDED';

  $UID = $_POST['UID'];
  $td = $_POST['td'];
  $problem = $_POST['problem'];
  $note = $_POST['note'];
  $file = $_POST['file'];

  // echo "<br>$UID";
  // echo "<br>$td";
  // echo "<br>$problem";
  // echo "<br>$file";

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to

    $sql = "INSERT INTO SM_problems
              (found_by, date_rec, problem, file)
            VALUES
              (?,?,?,?)
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-awtda</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $UID, $td, $problem, $file);
      mysqli_stmt_execute($stmt);
    }

    // Get the problem ID
    $sql = "SELECT ID
            FROM SM_problems
            WHERE problem = ?
            AND date_rec = ?;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-awtda1</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $problem, $td);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $SMPID = $row['ID'];
    }

    // ADD a note to the problem if one is needed
    $sql = "INSERT INTO SM_problem_notes
              (note, SMPID, UID, inputtime)
            VALUES
              (?,?,?,?)
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-awtda2</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $note, $SMPID, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  header('Location:../becarri.php?B');
  exit();
}
?>
