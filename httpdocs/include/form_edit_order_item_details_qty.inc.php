<?php
echo "include/form_edit_order_item_details_qty.inc.php";

// include 'from_OIID_get_order_details.inc.php';
include 'SM_colours.inc.php';

if ($OIIDComp == 2) {
  $title = "Re-instate Item on Order";
  $bc = $edtCol;
}else {
  $title = "Confirm Cancellation <br>of Item from Order";
  $bc = pink;
}
?>

<div style="position:absolute; top:50%; height:20%; left:30%; width:40%;font-size: 160%; font-weight: bold; background-color:<?php echo $bc ?>; border:2px solid red; border-radius:10px; z-index:1;">
  <?php echo $title ?>
</div>
<div style="position:absolute; top:59%; left:30%; width:40%; border-top:2px solid black; z-index:1;"></div>
<form style="z-index:1;" action="include/form_edit_order_item_details_can_act.inc.php" method="post">
  <input type="hidden" name="OIID" value="<?php echo $OIID ?>">
  <input type="hidden" name="itComp" value="<?php echo $OIIDComp ?>">
  <button class="entbtn" type="submit" style="position:absolute; top:63%; left:38%; width:10%; background-color: <?php echo $fsvCol ?>; z-index:1;" name="YES">Confirm</button>
  <button class="entbtn" type="submit" style="position:absolute; top:63%; left:52%; width:10%;; background-color: <?php echo $fcnCol ?>; z-index:1;" name="NO">Cancel</button>
</form>
