<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_count_company_partners_accepted2.inc.php</b>";
$CID = $_SESSION['CID'];
// Check to see how many requests you have accepted
$sql = "SELECT COUNT(ID) as cpCPID
        FROM company_partnerships
        WHERE (acc_CID = ?)
			  AND active = 1
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccacp2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cpCPID = $row['cpCPID'];
}
