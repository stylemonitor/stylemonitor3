<?php
include 'dbconnect.inc.php';
// echo "include/from_pCID_select_FROM_division.inc.php";

$sql = "SELECT COUNT(d.ID) as cDID
        FROM company c
          , associate_companies ac
          , division d
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fpsfd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result):
  $nosC = $row['cDID'];
}

$sql = "SELECT d.ID as DID
          , d.name as DIDn
        FROM company c
        , associate_companies ac
          , division d
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fpsfd1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $fDID = $row['DID'];
    $fDIDn = $row['DIDn'];
    echo '<option value="'.$fDID.'">'.$fDIDn.'</option>';
  }
}

?>
