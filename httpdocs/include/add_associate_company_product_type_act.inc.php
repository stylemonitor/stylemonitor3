<?php
include 'dbconnect.inc.php';
// echo "add_associate_company_product_type_act.inc.php";

if (!isset($_POST['addPT']) && !isset($_POST['cancelADDpt'])) {
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['cancelADDpt'])) {
  $ACID = $_POST['ACID'];
  header("Location:../associates.php?A&asf&id=$ACID");
  exit();
}elseif (isset($_POST['addPT'])) {
  // get the data from the form
  // echo "<br>Add the product type";
  $td = date('U');
  $UID = $_POST['UID'];

  $ptACID = $_POST['ptACID'];
  $DID = $_POST['DID'];
  $SID = $_POST['SID'];
  $PTID = $_POST['PTID'];

  // echo "<br>ACID = $ptACID";
  // echo "<br>DID = $DID";
  // echo "<br>SID = $SID";
  // echo "<br>PTID = $PTID";
  // echo "<br>UID = $UID";
  // echo "<br>td = $td";

// Add to the associate company
  $sql = "INSERT INTO associate_company_product_types
            (ACID, PTID, UID, inputtime)
          VALUES
            (?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-aacpta';
  }else {
    mysqli_stmt_bind_param($stmt, "ssss", $ptACID, $PTID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

// Add to the associate company
  $sql = "INSERT INTO division_product_types
            (DID, PTID, UID, inputtime)
          VALUES
            (?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-aacpta1';
  }else {
    mysqli_stmt_bind_param($stmt, "ssss", $DID, $PTID, $UID, $td);
    mysqli_stmt_execute($stmt);
}

// Add to the associate company
  $sql = "INSERT INTO section_product_types
            (SID, PTID, UID, inputtime)
          VALUES
            (?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-aacpta2';
  }else {
    mysqli_stmt_bind_param($stmt, "ssss", $SID, $PTID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }





  header("Location:../associates.php?A&asf&id=$ptACID");
  exit();
}
?>
