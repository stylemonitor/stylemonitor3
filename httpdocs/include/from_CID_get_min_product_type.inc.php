<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_get_min_product_type.inc.php</b>";
// pt.DIDchanged
// $CID = $_SESSION['CID'];

$sql = "SELECT MIN(pt.ID) as PTID
        FROM product_type pt
          , company c
        WHERE c.ID = ?
        AND pt.CID = c.ID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmpt</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $mPTID = $row['PTID'];
}
