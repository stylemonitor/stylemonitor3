<?php
session_start();
// echo "<b>include/reports.inc.php</b>";
include 'dbconnect.inc.php';
$CID = $_SESSION['CID']; // get the company CID

// echo "REPORTS OF MOVEMENTS HOME PAGE";

if (!isset($_GET['rep'])) {
  ?>
  <br><br><br>
  <br><br><br>
  <br><br><br>
  <p style="font-size:200%;">Select the date you want to view a report of</p>
  <br>
  <p style="font-size:200%;">OR just click on <b>Today</b> or <b>Enter</b> for todays date</p>
  <br><br><br>
  <p>You can go to a Monitoring Point via the tabs along the top and then enter a date</p>
  <?php
}


if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  // echo "<br>TAB SECTION is $tab";
  if ($tab == 0) {
    // include 'form_select_movement_date_Awt.inc.php';
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date_awt.inc.php';
    }
  }elseif ($tab == 1) {
    // include 'form_select_movement_date_stage1.inc.php';
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date_stage1.inc.php';
    }
  }elseif ($tab == 2) {
    // include 'form_select_movement_date_stage2.inc.php';
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date_stage2.inc.php';
    }
  }elseif ($tab == 3) {
    // include 'form_select_movement_date_stage3.inc.php';
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date_stage3.inc.php';
    }
  }elseif ($tab == 4) {
    // include 'form_select_movement_date_stage4.inc.php';
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date_stage4.inc.php';
    }
  }elseif ($tab == 5) {
    // include 'form_select_movement_date_stage5.inc.php';
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date_stage5.inc.php';
    }
  }elseif ($tab == 6) {
    // include 'form_select_movement_date_whs.inc.php';
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date_whs.inc.php';
    }
  }elseif ($tab == 9) {
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date.inc.php';
    }
  }
}
include 'form_select_movement_date.inc.php';
