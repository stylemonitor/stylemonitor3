<?php
session_start();
include 'dbconnect.inc.php';
include 'from_CID_count_season.inc.php';
// echo "include/report_division.inc.php";

if (isset($_GET['s'])) {
  $PRID = $_GET['s'];
}
?>

<form class="" style="position:absolute; top:10%; height:4%; left:34%; width:30%;" action="include/report_style_season_act.inc.php" method="post">
  <input type="hidden" name="PRID" value="<?php echo $PRID ?>">
  <select style="position:absolute; top:6%; height:80%; left:3%; width:50%;" name="SNID">
    <option value="">Select Season</option>
    <?php
      include 'from_ACID_select_season.inc.php';
    ?>
  </select>
  <button class="entbtn" style="position:absolute; top:6%; height:80%; left:60%; width:35%;" type="submit" name="addseas">Add Season</button>
</form>

<table class="trs" style="position:absolute; top:15%; left:34%; width:30%;">
  <tr>
    <th>Season</th>
  </tr>
  <?php
  $sql = "SELECT s.ID as SNID
            , s.name as SNIDn
          FROM season s
          	, prod_ref pr
	          , prod_ref_to_season prs
          WHERE pr.id = ?
          AND prs.PRID = pr.ID
          AND prs.SNID = s.ID
          AND s.name NOT IN ('Select Season')
          ORDER BY s.name
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-rss</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $PRID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      $SNID = $row['SNID'];
      $SNIDn = $row['SNIDn'];
      ?>
      <tr>
        <td><?php echo $SNIDn ?></td>
      </tr>
      <?php
    }
  }
  ?>
</table>
