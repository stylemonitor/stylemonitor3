<?php
session_start();
include 'dbconnect.inc.php';
// echo '<b>include/report_style_division_act.inc.php</b>';
if (!isset($_POST['addcoll'])){
  // echo 'Incorrect method used';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['addcoll'])){
  // echo 'Add a division to the style';

  $PRID = $_POST['PRID'];
  $CLID = $_POST['CLID'];
  $UID = $_SESSION['UID'];
  $td = date('U');

  // echo "PRID is $PRID";
  // echo "<br>CLID is $CLID";

  if (empty($CLID)) {
    header('Location:../styles.php?S&sis');
    exit();
  }else {
    // Check that the divisioin is not already listed
    $sql = "SELECT prc.id as PRCLID
            FROM prod_ref_to_collection prc
              , prod_ref pr
              , season sn
            WHERE pr.ID = ?
            AND sn.ID = ?
            AND prc.PRID = pr.ID
            AND prc.CLID = sn.ID
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-rsd</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $PRID, $CLID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_assoc($result);
      $PRCLID = $row['PRCLID'];
    }
    // ADD if not alreaDy listed
    if ($PRCLID <> 0) {
      // echo "<br>The style/division has been set";
    }else {
      $sql = "INSERT INTO prod_ref_to_collection
                (PRID, CLID, UID, inputtime)
              VALUES
                (?,?,?,?)
              ;";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b>FAIL-rsda2<b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ssss", $PRID, $CLID, $UID, $td);
        mysqli_stmt_execute($stmt);
      }

    }
  }
  header("Location:../styles.php?S&sis&s=$PRID");
  exit();
}
