<?php
session_start();
include 'include/dbconnect.inc.php';
// // echo '<b>include/record_order_movement_act.inc.php</b>';

include 'set_urlPage.inc.php';

// if (isset($_POST['OPID'])) {
//   // get the order placed ID
//   $OPID = $_POST['OPID'];
// }

if (isset($_POST['OIID'])) {
  // get the order item ID
  $OIID = $_POST['OIID'];
}

if (isset($_POST['clMove'])) {
  // // echo "Close form";
  // // echo $OIID.':'.$OPID;
  header("Location:../$urlPage&orb&oi=$OIID");
  exit();
}elseif (isset($_POST['revMove'])) {
  // echo "go to Review Log";
  // header("Location:../mm_D.php?D&ro&oi=$OIID&op=$OPID");
  exit();
}elseif (isset($_POST['orProMove'])){
  // // echo '<br>Move the item about the factory';

  include 'dbconnect.inc.php';

  $OPID   = $_POST['OPID'];
  $OIMRID = $_POST['OIMRID'];
  $UID    = $_SESSION['UID'];
  $mpQty  = $_POST['mpQty'];
  $td = date('U');

  // echo '<br>The order item ID is ($OIID) : <b>'.$OIID.'</b>';
  // echo '<br>The order place ID is ($OPID) : <b>'.$OPID.'</b>';
  // echo '<br>The order movement reason ID is ($OIMRID) : <b>'.$OIMRID.'</b>';
  // echo '<br>The Use UID is ($UID) : <b>'.$UID.'</b>';
  // echo '<br>The quantity to be moved is ($mpQty) : <b>'.$mpQty.'</b>';

  if (!is_numeric($mpQty) || $mpQty < 1) {
    // echo "The value is NOT a number";
    header("location: ../$urlPage&orf&oi=$OIID&v");
    exit();
  }

  $sql = "INSERT INTO order_placed_move
            (OPID, OIMRID, UID, omQty, inputtime)
          VALUES
            (?,?,?,?,?)";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-roma</b>';
  }else{
  mysqli_stmt_bind_param($stmt, "sssss", $OPID, $OIMRID, $UID, $mpQty, $td);
  mysqli_stmt_execute($stmt);
  }

  // // echo '<br>'.$OPID;
  // // echo '<br>'.$OIID;

  header("Location:../$urlPage&orf&oi=".$OIID);
  exit();
}
