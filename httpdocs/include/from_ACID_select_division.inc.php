<?php
include 'dbconnect.inc.php';
//echo '<b>include/from_CID_select_prod_type.inc.php</b>';
// $ACID = $_SESSION['ACID'];
$sql = "SELECT  d.ID as DID
	        , d.name as DIDn
        FROM division d
        	, associate_companies ac
        WHERE ac.ID = (SELECT MIN(ac.ID) as mACID
								        FROM associate_companies ac
								          , company c
								        WHERE c.ID = ?
								        AND ac.CID = c.ID)
        AND d.ACID = ac.ID
        ORDER BY d.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmc</b>';
}else{
	// changed $CID to $fCID in the hope that it then picks up the division for the associate company set
  mysqli_stmt_bind_param($stmt, "s", $tCID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  // $row = mysqli_fetch_array($result);
	while($row = mysqli_fetch_array($res)){
	  $DID = $row['DID'];
	  $DIDn = $row['DIDn'];

	  // echo '<option value="'.$PTID.'">'.$PTIDn.' : '.$PTIDsc.'</option>';
	  echo '<option value="'.$DID.'">'.$DIDn.'</option>';
	}
};
?>
