<?php
include 'dbconnect.inc.php';
//echo '<b>include/from_CPID_get_next_order_item_nos.inc.php</b>';

//Get the last order item number for the associate company
$sql = "SELECT COUNT(oi.ID) as cOIID
        FROM order_item oi
          , orders o
          , order_type ot
        WHERE o.orNos = (SELECT max(o.orNos)+1 as MorNos
                         FROM orders o
                          , company_partnerships cp
                         WHERE cp.ID = ?
                         AND o.CPID = cp.ID)
        AND oi.OID = o.ID
        AND ot.ID = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fcgoin</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CPID, $OTID);
  mysqli_stmt_execute($stmt);
  $result  = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $ccpOIID = $row['cOIID']+1;
}
