<?php
// echo "include/form_pwd_reset.inc.php";
$UID = $_SESSION['UID'];

if (isset($_GET['pwe'])) {
  $pwe = $_GET['pwe'];
  if ($pwe == 1) {
    $pwerr = "Your password <b>did not match</b> the file password";
    $bc = 'red';
  }elseif ($pwe == 2) {
    $pwerr = "Your new password was not long enough";
    $bc = 'red';
  }elseif ($pwe == 3) {
    $pwerr = "Your confirmation password did not match your new password";
    $bc = 'red';
  }elseif ($pwe == 4) {
    $pwerr = "You have not set anything";
    $bc = 'red';
  }elseif ($pwe == 5) {
    $pwerr = "<b>Your password has been changed</b>";
    $bc = '';
  }
}
?>
  <div class="overlay"></div>

  <div style="position:absolute; top:20%; height:55%; left:5%; width:90%; border: 2px ridge grey; border-radius:10px; background-color:<?php echo $edtCol ?>; font-size:200%; font-style: bold;">
    Change your password
  </div>
  
  <form action="include/form_pwd_reset_act.inc.php" method="POST">
    <input autofocus type="hidden" name="UID" value="<?php echo $UID ?>">
    <input class ="input_data" style="position:absolute; top:28%; height:4%; left:34%; width:30%; font-size:120%; text-align:left; text-indent:5%;" type="password" name="pwd" placeholder="Your current password"><br>
    <input class ="input_data" style="position:absolute; top:34%; height:4%; left:34%; width:30%; font-size:120%; text-align:left; text-indent:5%;" type="password" name="npwd" placeholder="Your new password"><br>
    <input class ="input_data" style="position:absolute; top:40%; height:4%; left:34%; width:30%; font-size:120%; text-align:left; text-indent:5%;" type="password" name="pwdCheck" placeholder="Re-type your new password"><br>
    <button class="entbtn" style="position:absolute; top:70%; height:4%; left:32.5%; width:15%; background-color: <?php echo $fsvCol ?>;" type="submit" name="reset_pwd_submit">Reset Password</button>
    <button class="entbtn" style="position:absolute; top:70%; height:4%; left:52.5%; width:15%; background-color: <?php echo $fcnCol ?>;" type="submit" name="cancel_pwd_submit">Cancel</button>
  </form>

  <!-- <div style="position:absolute; top:50%; height:3.5%; left:34.1%; width:30%; padding-top: 0.5%; background-color:pink; border-radius: 10px; border: thin solid red; z-index:1;" type="text" name="" value="">
    <?php echo "$pwerr"; ?>
  </div> -->

  <div style="position:absolute; top:50%; left:10%; width:80%; border-bottom: thick; border-color: black; font-size: 200%; font-style: bold; text-align:center;">
    Security Questions
  </div>

  <table class="trs" style="position:absolute; top:57%; left:10%; width:80%;">
    <tr>
      <th>Question</th>
      <th>Answer</th>
      <th>Change</th>
    </tr>
    <?php
    include 'from_UID_get_user_forgotten_pwd_details.inc.php';
    ?>
  </table>
