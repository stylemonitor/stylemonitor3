<?php
session_start();
include 'dbconnect.inc.php';
// echo "order_report_STYLE_count_query.inc.php";

if (isset($_GET['s'])) {
  $PRID = $_GET['s'];
}

$sql = "SELECT OPEN_ORDERS.PRID AS STYLE_ID
-- SQL is order_report_style_counts2.sql
       , OPEN_ORDERS.prod_ref AS STYLE
       , count(distinct o.fCID, o.tCID) AS CUSTOMERS
       , count(o.ID) AS TOTAL_ORDERS
       , OPEN_ORDERS.ORDERS AS OPEN_ORDERS
       , count(o.ID) - OPEN_ORDERS.ORDERS AS CLOSED_ORDERS
       , concat(round(((count(o.ID) - OPEN_ORDERS.ORDERS)/count(o.ID)) * 100,0),'%') AS PER_OCOMP
       , sum(oiq.order_qty) AS TOTAL_ITEMS
       , OPEN_ORDERS.ITEMS AS OPEN_ITEMS
       , sum(oiq.order_qty) - OPEN_ORDERS.ITEMS AS CLOSED_ITEMS
       , concat(round(((sum(oiq.order_qty) - OPEN_ORDERS.ITEMS)/sum(oiq.order_qty)) * 100,0),'%') AS PER_ICOMP
FROM (
      SELECT o.fCID
             , count(o.ID) AS ORDERS
             , pr.prod_ref
             , pr.ID as PRID
             , sum(distinct oiq.order_qty) AS ITEMS
             , o.orComp
      FROM orders o
      INNER JOIN order_item oi ON o.ID = oi.ID
      INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
      INNER JOIN prod_ref pr on oi.PRID = pr.ID
      WHERE o.orComp = 0 -- DO NOT CHANGE AS THE QUERY RELIES ON THIS
        AND pr.ID = ?
      GROUP BY o.fCID
     ) OPEN_ORDERS INNER JOIN orders o ON OPEN_ORDERS.fCID = o.fCID
                   INNER JOIN order_item oi ON o.ID = oi.ID
                   INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
WHERE (o.fCID = ? OR o.tCID = ?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-orScq</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $PRID, $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $PRID = $row['STYLE_ID'];
    $PRIDr = $row['STYLE'];
    $cCUST = $row['CUSTOMERS'];
    $tORD = $row['TOTAL_ORDERS'];
    $oORD = $row['OPEN_ORDERS'];
    $cORD = $row['CLOSED_ORDERS'];
    $pORD = $row['PER_COMP'];
    $tITM = $row['TOTAL_ITEMS'];
    $oITM = $row['OPEN_ITEMS'];
    $cITM = $row['CLOSED_ITEMS'];
    $pITM = $row['PER_COMP'];
    ?>
    <tr>
      <th><?php echo $cCUST ?></th>
      <th><?php echo $oORD ?></th>
      <th><?php echo "$oITM" ?></th>
      <th><?php echo $cORD ?></th>
      <th><?php echo "$cITM" ?></th>
      <th><?php echo $tORD ?></th>
      <th><?php echo "$tITM" ?></th>
    </tr>
    <?php
    }
  } ?>
    </table>
    <?php
