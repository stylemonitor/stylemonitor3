<?php
session_start();
include 'dbconnect.inc.php';
// echo "<br><b>form_associate_address_add_act.inc.php</b>";

if (!isset($_POST['adddets']) && (!isset($_POST['candets']))) {
  // echo "<br>INCORRECT ACCESS METHOD";
  header('Location:../index.php');
  exit();
}elseif (isset($_POST['candets'])) {
  header('Location:../associates.php?A&acn');
  exit();
}elseif (isset($_POST['adddets'])) {

  // MUST BE CONNECTED TO THE $CID not the $ACID
  $CID = $_SESSION['CID'];
  $UID = $_SESSION['UID'];
  $urlPage = $_POST['urlPage'];

  // echo '<br>Get the data from the form';
  $name = $_POST['name'];
  // $div  = $_POST['div'];
  $CTID  = $_POST['rel'];
  $CYID = $_POST['CYID'];
  $TID = $_POST['TZID'];
  $addn = $_POST['addn'];
  $add1 = $_POST['add1'];
  $add2 = $_POST['add2'];
  $city = $_POST['city'];
  $county = $_POST['county'];
  $pcode = $_POST['pcode'];
  $tel = $_POST['tel'];
  $PTID = $_POST['PTID'];
  // $DIDn = $_POST['add_div'];
  // echo "DIDn : $DIDn";

  // // the time zone of the company
  // $TID = 201;

  $td = date('U');

  if ($CTID == 4) {
    // CLIENT
    $actype = 'acc';
  }elseif ($CTID == 5) {
    // SUPPLIER
    $actype = 'acs';
  }

  if (empty($addn)) { $addn = 'TBC';}else { $addn = $addn;}
  if (empty($add1)) { $add1 = 'Address line 1';}else { $add1 = $add1;}
  if (empty($add2)) { $add2 = 'Address line 2';}else { $add2 = $add2;}
  if (empty($city)) { $city = 'Town / City';}else { $city = $city;}
  if (empty($county)) { $county = 'County / State';}else { $county = $county;}
  if (empty($pcode)) { $pcode = 'Post Code / Zip Code';}else { $pcode = $pcode;}
  if (empty($tel)) { $tel = 'Main phone number';}else { $tel = $tel;}

  // echo '<br>The associate company is (name) : '.$name.'';
  // echo '<br>The users country ID is (CYID) : '.$CYID.'';
  // echo '<br>Product type ID is (PTID) : '.$PTID.'';
  // echo '<br>The sponsor companyID is (CID) : '.$CID.'';
  // echo '<br>Their relationship ID is (CTID) : '.$CTID.'';
  // echo '<br>The timezone ID is (TID) : '.$TID.'';
  // echo '<br>The users ID is (UID) : '.$UID.'';
  // echo '<br>The site name is (addn) : '.$addn.'';
  // echo '<br>The users address 1 is (add1) : '.$add1.'';
  // echo '<br>The users address 2 is (add2) : '.$add2.'';
  // echo '<br>The users city is (city) : '.$city.'';
  // echo '<br>The users postcode is (pcode) : '.$pcode.'';

  include 'check_new_assoc_company_ID.inc.php';

  if (empty($chACID)) {
    // echo "<br>New company add to the  list";
    include 'input_new_associate_company.inc.php';
    include 'input_associate_company_division.inc.php';
    include 'input_associate_company_section.inc.php';
    include 'input_company_division_section_addresses.inc.php';
    include 'input_associate_divison_section_product_types.inc.php';
    if ($CTID == 4) {
      header("Location:../associates.php?A&acc");
    }elseif ($CTID == 5) {
      header("Location:../associates.php?A&acs");
      // code...
    }
    exit();
  }else {
    // echo "<br>They are an existing company";
    header("Location:../associates.php?A&asf&id=$chACID&ct=$CTID");
    exit();
  }
  // echo "<br><br>Still need to set divisions and section for the associate company.<br>Also product types for ac, div and section, as well as addresses and anything else I remember!!!";

  // CHECKING the none REQUIRED fields are filled in
  // The telephone numer
  if (!is_numeric($tel)) {
    // echo "check if the telephgone is a number";
    header("location:../associates.php?A&adf&1&n=$name&div=$div&addn=$addn&add1=$add1&add2=$add2&city=$city&pcode=$pcode");
    exit();
  }elseif(($CYID == 1) && ($CTID == 1)){
    // echo "Neither relatinship OR country have been set";
    header("location:../associates.php?A&adf&2&name=$name&div=$div&addn=$addn&add1=$add1&add2=$add2&city=$city&pcode=$pcode&tel=$tel");
    exit();
  }elseif ($CTID == 1) {
    // echo "NO relationship set";
    header("location:../associates.php?A&adf&2&name=$name&div=$div&addn=$addn&add1=$add1&add2=$add2&city=$city&pcode=$pcode&tel=$tel");
    exit();
  }elseif ($CYID == 1) {
    // echo "NO country has been set";
    header("location:../associates.php?A&adf&2&name=$name&div=$div&addn=$addn&add1=$add1&add2=$add2&city=$city&pcode=$pcode&tel=$tel");
    exit();
  }
}
