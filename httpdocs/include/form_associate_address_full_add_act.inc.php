<?php
include 'dbconnect.inc.php';
// echo "<b>include/form_associates_address_full_add_act.inc.php</b>";

if (!isset($_POST['acq_dets']) && (!isset($_POST['can_acq_dets']))) {
  // echo "<br>INCORRECT ACCESS METHOD";
  header('Location:../index.php');
  exit();
}elseif (isset($_POST['can_acq_dets'])) {
  header('Location:../associates.php?A&acn');
  exit();
}elseif (isset($_POST['acq_dets'])) {
  include 'include/from_CID_check_associate_company_name.inc.php';
  include 'include/from_CID_check_company_address.inc.php';

  // echo '<br>Get the data from the form';
  // MUST BE CONNECTED TO THE $CID not the $ACID
  $CID = $_SESSION['CID'];
  $UID = $_SESSION['UID'];
  $urlPage = $_POST['urlPage'];

  $name = $_POST['name'];
  $div  = $_POST['div'];
  $CTID  = $_POST['rel'];
  $CYID = $_POST['CYID'];
  $TID = $_POST['TZID'];

  $addn = $_POST['addn'];
  $add1 = $_POST['add1'];
  $add2 = $_POST['add2'];
  $city = $_POST['city'];
  $pcode = $_POST['pcode'];
  $tel = $_POST['tel'];

  $PTID = $_POST['PTID'];
  // $DIDn = $_POST['add_div'];
  // echo "DIDn : $DIDn";

  if (empty($addn)) { $addn = 'Site name to be confirmed';}else { $addn = $addn;}
  if (empty($add1)) { $add1 = 'Add line 1';}else { $add1 = $add1;}
  if (empty($add2)) { $add2 = 'Add line 2';}else { $add2 = $add2;}
  if (empty($city)) { $city = 'City/town';}else { $city = $city;}
  if (empty($pcode)) { $pcode = 'Post Code';}else { $pcode = $pcode;}
  if (empty($tel)) { $tel = '0000';}else { $tel = $tel;}

  // the time zone of the company
  $TID = 201;

  $td = date('U');

  if ($CTID == 4) {
    // CLIENT
    $actype = 'acc';
  }elseif ($CTID == 5) {
    // SUPPLIER
    $actype = 'acs';
  }

  // echo '<br>The sponsor companyID is : <b>'.$CID.'';
  // echo '<br>The users country ID is : <b>'.$CYID.'';
  // echo '<br>Their relationship ID is : <b>'.$CTID.'';
  // echo '<br>The timezone ID is : <b>'.$CTID.'';
  // echo '<br>The users ID is : <b>'.$UID.'';
  // echo '<br>Roduct type ID is : <b>'.PTID.'';
  //
  // echo '<br>The associate company is : <b>'.$name.'';
  // echo '<br>The companies division is : <b>'.$div.'';
  //
  // echo '<br>The site name is : <b>'.$addn.'';
  // echo '<br>The users address 1 is : <b>'.$add1.'';
  // echo '<br>The users address 2 is : <b>'.$add2.'';
  // echo '<br>The users city is : <b>'.$city.'';
  // echo '<br>The users postcode is : <b>'.$pcode.'';
  // echo '<br><br>Prepare the data for checking';

  // CHECKING the none REQUIRED fields are filled in
  // The telephone numer
  if (!is_numeric($tel)) {
    // echo "check if the telephgone is a number";
    header("location:../associates.php?A&adf&1&n=$name&div=$div&addn=$addn&add1=$add1&add2=$add2&city=$city&pcode=$pcode");
    exit();
  }elseif(($CYID == 1) && ($CTID == 1)){
    // echo "Neither relatinship OR country have been set";
    header("location:../associates.php?A&adf&2&name=$name&div=$div&addn=$addn&add1=$add1&add2=$add2&city=$city&pcode=$pcode&tel=$tel");
    exit();
  }elseif ($CTID == 1) {
    // echo "NO relationship set";
    header("location:../associates.php?A&adf&2&name=$name&div=$div&addn=$addn&add1=$add1&add2=$add2&city=$city&pcode=$pcode&tel=$tel");
    exit();
  }elseif ($CYID == 1) {
    // echo "NO country has been set";
    header("location:../associates.php?A&adf&2&name=$name&div=$div&addn=$addn&add1=$add1&add2=$add2&city=$city&pcode=$pcode&tel=$tel");
    exit();
  }

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to

    // echo '<br><br><b>Check the data for empty/incorrect data ()</b>';
    // ADD THE ASSOCIATE COMPANY TO THE DB
    $sql = "INSERT INTO associate_companies
              (CID, CTID, CYID, TID, UID, name, inputtime)
            VALUES
              (?,?,?,?,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-faafaa4</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "sssssss", $CID, $CTID, $CYID, $TID, $UID, $name, $td);
      mysqli_stmt_execute($stmt);
    }

    $sql = "SELECT ID as ACID
            FROM associate_companies
            WHERE CID = ?
            AND name = ?
            AND CTID = ?
            AND CYID = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-faafaa5</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $CID, $name, $CTID, $CYID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $NEACID = $row['ACID'];
    }
    // echo '<br>Associate companies ID is ($NEACID)<b> : '.$NEACID.'</b>';

    include 'input_associate_company_PTID.inc.php';

    // Add the first 'SELECT DIVISION' option
    if (empty($DIDn)) {
      // echo "NO Division added";
      $sql = "INSERT INTO division
      (ACID, UID, inputtime)
      VALUES
      (?,?,?);";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b>FAIL-faafaa6</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "sss", $NEACID, $UID, $td);
        mysqli_stmt_execute($stmt);
      }
    }else {
      // Add the first 'SELECT DIVISION' option
      $sql = "INSERT INTO division
      (ACID, UID, inputtime)
      VALUES
      (?,?,?);";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b>FAIL-faafaa6</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "sss", $NEACID, $UID, $td);
        mysqli_stmt_execute($stmt);
      }
      // Then add the named division
      $sql = "INSERT INTO division
                ( UID, name, inputtime)
              VALUES
                (?,?,?)
      ;";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b>FAIL-eaca1</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "sss", $UID, $DIDn, $td);
        mysqli_stmt_execute($stmt);
      }
    }

    // GET div ID
    $sql = "SELECT MAX(ID)
            FROM division
            WHERE ACID = ?;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-faafaa7</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "s", $NEACID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $EDID = $row['0'];
      // echo '<br>The division ID is <b>$EDID : '.$EDID.'</b>';
    }

    include 'input_new_associate_company_address.inc.php';

    // // ADD THE ASSOCIATE COMPANY ADDRESS TO THE DB
    // $sql = "INSERT INTO addresses
    //             (CYID, TID, ACID, addn, add1, add2, city, pcode, tel, UID, inputtime)
    //           VALUES
    //             (>,?,?,?,?,?,?,?,?,?,?);";
    // $stmt = mysqli_stmt_init($con);
    // if(!mysqli_stmt_prepare($stmt, $sql)){
    //   // echo '<b>FAIL-faafaa8</b>';
    // }else{
    //   mysqli_stmt_bind_param($stmt, "sssssssssss", $CYID, $TID, $NEACID, $addn, $add1, $add2, $city, $pcode, $tel, $UID, $td);
    //   mysqli_stmt_execute($stmt);
    // }

    // GET the data (again!!!!)
    $sql = "SELECT ID
            FROM addresses
            WHERE addn=?
            AND add1=?
            AND add2=?
            AND city=?
            AND pcode=?
            AND tel=?
            AND UID=?;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-faafaa9</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssssssss", $NEACID, $addn, $add1, $add2, $city, $pcode, $tel, $UID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $AID = $row['0'];
      // echo '<br>The address ID is <b>$AID : '.$AID.'</b>';
    }

    // echo '<br>Link the division to the address';

    //LINK THE COMPANY DIVISION TO THE ADDRESS
    $sql = "INSERT INTO company_division_address
              (DID, AID, UID, inputtime)
            VALUES
              (?,?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-faafaa10</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $EDID, $AID, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    // GET the new company address ID
    $sql = "SELECT ID
            FROM company_division_address
            WHERE DID=?
            AND AID=?;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-faafaa11</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $EDID, $AID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $acDAID = $row['0'];
    }
    // echo '<br>The associate company division address ID is <b>$acDAID : '.$acDAID.'</b>';

    // set a section for the company
    // if a Client this could be Tailoring etc
    // if Supplier this could be Line 1
    // but is TBC for now!!!
    $sql = "INSERT INTO section
              (DID, UID, inputtime)
            VALUES
              (?,?,?);";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-faafaa12</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "sss", $EDID, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    // GET the section ID SID
    $sql = "SELECT ID as SID
            FROM section
            WHERE UID = ?
            AND DID = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-faafaa13</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $UID, $EDID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $SID = $row['SID'];
    }

    // echo "<br>Section ID is SID : $SID";

    // not sure what this next bit does OR why
    // include ('include/insert_product_type.php');

  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  header("Location:../$urlPage&$actype");
  // header("Location:../$urlPage&$actype");
  exit();
}
