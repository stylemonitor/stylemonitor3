<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_get_SOLE_product_type.inc.php</b>";
// pt.DIDchanged
$CID = $_SESSION['CID'];

$sql = "SELECT pt.ID as PTID
          , pt.name as PTIDn
        FROM company c
        , product_type pt
        WHERE c.ID = ?
        AND pt.CID = c.ID
        AND pt.name NOT IN ('Select Product Type')
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgspt</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $PTID = $row['PTID'];
  $PTIDn = $row['PTIDn'];
}
