<?php
session_start();
// echo '<b>include/edit_style_act.inc.php</b>';

if (!isset($_POST['can_edit_style']) && (!isset($_POST['edit_style']))) {
  header("Location:../location:..index.php");
  exit();
}elseif (isset($_POST['can_edit_style'])) {
  header("Location:../styles.php?S&snn");
  exit();
}elseif(isset($_POST['edit_style'])){
  include 'dbconnect.inc.php';
  $UID = $_SESSION['UID'];
  $CID = $_SESSION['CID'];
  $ACID = $_SESSION['ACID'];
  $td  = date('U');

  include 'from_CID_get_min_product_type.inc.php';
  include 'from_CID_get_min_division.inc.php';
  include 'from_CID_get_min_season.inc.php';
  include 'from_CID_get_min_collection.inc.php';

  // from edit_style.inc.php get all the information
  $ACID = $_POST['ACID'];
  $PRID = $_POST['PRID'];
  $PRIDr = $_POST['PRIDr'];
  $PRIDd = $_POST['PRIDd'];
  $PRIDe = $_POST['PRIDe'];
  $PTID = $_POST['PTID'];
  $DID = $_POST['DID'];
  $SCID = $_POST['SCID'];
  $SNID = $_POST['SNID'];
  $CLID = $_POST['CLID'];

  include 'from_DID_get_min_section.inc.php';

  // the new data
  $nPRIDr = $_POST['nPRIDr'];
  $nPRIDd = $_POST['nPRIDd'];
  $nPRIDe = $_POST['nPRIDe'];
  $nPTID = $_POST['nPTID'];
  $nDID = $_POST['nDID'];
  $nSCID = $_POST['nSCID'];
  $nSNID = $_POST['nSNID'];
  $nCLID = $_POST['nCLID'];

  if (empty($nPRIDr) && empty($nPRIDd) && empty($nPTID)) {
    // echo "<br>NO CHANGES HAVE BEEN MADE";
    header("Location:../sytles.php?S&snn");
    exit("<br>No changes made");
  }

  if (empty($nPRIDr)) {
    // echo "<br>No change in the product short code just a new name and/or product type";
    if (empty($nPRIDr)) {$nPRIDr = $PRIDr; }else { $nPRIDr = $nPRIDr; };
    if (empty($nPRIDd)) {$nPRIDd = $PRIDd; }else { $nPRIDd = $nPRIDd; };
    if (empty($nPTID)) {$nPTID = $PTID; }else { $nPTID = $nPTID; };
    // echo "<br>Product description now = $nPRIDd :: Product type is now = $nPTID";

    include 'update_style.inc.php';
  }

  if (!empty($nPRIDr)) {
    // echo "<br>check that the reference is NOT already in use";
    if (empty($nPRIDd)) {$nPRIDd = $PRIDd; }else { $nPRIDd = $nPRIDd; };
    if (empty($nPTID)) {$nPTID = $PTID; }else { $nPTID = $nPTID; };
    include 'check_prod_ref_valid.inc.php';
    // echo "<br>Product ref is = $nPRIDr :: Product description now = $nPRIDd :: Product type is now = $nPTID";

    include 'update_style.inc.php';
  }
}
