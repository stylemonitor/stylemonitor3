<?php
include 'dbconnect.inc.php';
// echo "<br><b>include/form_new_orders_combined.inc.php</b>";
?>
<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
<script src='https://https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js'></script>
<?php
// _______________________________________________________________________________________________
// * * * FOR BOTH SALES AND PURCHASES THE FROM IS ALWAYS WHO SETS THE ORDER * * *
include 'include/from_CID_count_associates_companies.inc.php';

if (isset($_GET['sal'])){
  $CPID = $_GET['sal'];
  include 'include/form_new_order_INFO_SALES.inc.php';
  // include 'include/from_CID_count_associates_clients_ALL.inc.php';
  // $cPOTcompanys = $cACIDcALL;
  $DEL_SEL = 'from_CID_select_associate_company_sales.inc.php';
  $pddh = 'Sales Record Form';
  // $pddhbc = '#7ffad9';
  // include 'include/from_CID_select_associate_company_sales.inc.php';
}elseif (isset($_GET['pur'])) {
  $CPID = $_GET['pur'];
  include 'include/form_new_order_INFO_PURCHASES.inc.php';
  include 'include/from_CID_count_associates_supplier_ALL.inc.php';
  $cPOTcompanys = $cACIDsALL;
  $DEL_SEL = 'include/from_CID_select_associate_company_purchases.inc.php';
  $pddh = 'Purchase Record Form';
  // $pddhbc = '#7ffad9';
}elseif (isset($_GET['prt'])) {
  $CPID = $_GET['prt'];
  include 'include/form_new_order_INFO_PARTNERS.inc.php';
  $OTID = 4;
  $pddh = 'Partner Purchase Record Form';
  $mACID = $fACID;
  $mACIDn = $fACIDn;
  $mfDID = $fDIDn;
  $mfSID = $fSIDn;
}
include 'page_description_date_header.inc.php';
?>
<!--  ______________________________________________________________________________________________ -->
<!-- the 'header' -->
<!-- Order number, order date and initial due by date -->
<div style="position:absolute; top:9.5%; left:0.5%; width:12%; text-align: left; background-color:#f6ffb9; padding-left:0.5%; padding-top:0.2%; border-radius:5px 0px 0px 0px;">SM Record Info</div>
<div style="position:absolute; top:9.5%; left:20%; width:40%; font-size:100%; text-align:center;"><b>To be issued when the order has been recorded</b></div>

<!-- The date the order was placed -->
<!-- Will default to todays date IF left blank -->
<div style="position:absolute; top:9%; left:71%; width:15%; text-align:left;">Order Date</div>

<!-- The date the order is due -->
<!-- Will default to 91 days from todays date IF left blank -->
<!-- <div style="position:absolute; top:30%; left:80%; width:15%; text-align:left;">Order Due Date</div> -->

<!-- _______________________________________________________________________________________________  -->
<form class="" action="include/form_new_orders_combined_act.inc.php" method="post">
  <!-- Sets the type of order (OTID) - Sales(1) or Purchase(2) or Partner (3)-->
  <input type="hidden" name="OTID" value="<?php echo $OTID ?>">
  <input type="hidden" name="CPID" value="<?php echo $CPID ?>">
  <input type="hidden" name="fCID" value="<?php echo $fCID ?>">
  <input type="hidden" name="tCID" value="<?php echo $tCID ?>">
  <input type="hidden" name="urlPage" value="<?php echo $urlPage ?>">

  <!-- SET THE DATES -->
  <!-- the detail -->
  <input readonly class ="RNbtn" style="position:absolute; top:3.5%; height:7%; left:79.5%; width:18%; padding-right: 0.5%; font-size:175%; text-align: right;" type="hidden" name="orNos" value="<?php echo $cOID ?>">
  <input class ="sel_dropdown" style="position:absolute; top:8%; height:3.5%; left:80.5%; width:18%; text-align:center; font-weight:bold; z-index:1;" type="date" name="orDate" placeholder="Order Date" value="date()" >
  <!-- <input class ="sel_dropdown" style="position:absolute; top:33%; height:3.5%; left:79.5%; width:18%; text-align:center;" type="date" name="delDate" value="Date()+91" placeholder="Delivery Date" > -->

  <!-- _______________________________________________________________________________________________  -->
  <!-- * * * * * LEFT / FROM side of the form * * * * * -->
  <!-- Associate company -->
  <div style="position:absolute; top:13%; left:0.5%; width:10%; text-align: left; text-indent: 2%; background-color:#7ffad9; padding-left:0.5%; padding-top:0.2%; border-radius: 5px 5px 0px 0px; z-index:1;"><b>FROM</b></div>
  <div style="position:absolute; top:16%; height:13%; left:0.5%; width:54.25%; font-size:140%; font-weight:bold; text-align: left; text-indent:2%; border:thin solid black;"><?php echo $mACIDn ?></div>
  <!-- both fACID and tACID are teh same when there are no ACID to select from -->
  <input type="text" name="fACID" value="<?php echo $mACID ?>">
  <input type="text" name="tACID" value="<?php echo $mACID ?>">

  <!-- to be taken out when Divisions in use -->
  <?php include 'include/from_ACID_get_min_division_mfDID.inc.php';?>
  <input type="hidden" name="fDID" value="<?php echo $mfDID ?>">

  <!-- SECTION ID -->
  <?php include 'include/from_mfDID_get_mfSID.inc.php';?>
  <input type="hidden" name="fSID" value="<?php echo $mfSID ?>">



<!-- ONLY TO BE ADDED WHEN FULLY WORKING ON NEXT VERSION -->
  <!-- FROM Division -->
  <div style="position:absolute; top:20%; height:4%; left:1%; width:38.5%; text-align: left; text-indent: 2%; z-index:1;">
    <!-- <?php
      if ($cDID == 0) {
        // echo "NO DIVISIONS";
        echo " Division Count == 1:: $cDID";
        ?>
        <?php
      }elseif ($cDID == 1) {
        // echo " Division Count == 1:: $cDID";
        include 'from_CID_get_min_division.inc.php';
        ?>
        <input type="hidden" name="fDID" value="<?php echo $mDID ?>">
        <div style="position:absolute; top:15%; left:3%; width:90%; font-size:130%;">Division : <?php echo $mDIDn ?></div>
        <?php
      }elseif ($cDID > 1) {
        echo " Division";
        // echo "$Fdiv :: $cDIV";
        ?>
        <select required id="fDID" name="fDID" style="position:absolute; top:40%; height:55%; left:1%; width:98%;" >
          <option value="">Select Division from list</option>
          <?php
          // include 'include/from_ACID_select_division_dd.inc.php'
          ?>
        </select>
        <?php
      }
     ?> -->
  </div>

<!-- _______________________________________________________________________________________________  -->
<!-- * * * * * RIGHT / TO SIDE OF THINGS * * * * * -->
<!-- SET the TO Associate Company -->
<div style="position:absolute; top:13%; left:55.5%; width:10%; text-align:left; text-indent:2%; padding-left:0.5%; padding-top:0.2%; background-color: #<?php echo $hc ?>; border-radius: 5px 5px 0px 0px;"><b>To</b></div>

<?php
if (isset($_GET['aid'])) {
  $aACID = $_GET['aid'];
  include 'include/from_aACID_get_associate_company_details.inc.php';
  include 'from_ACID_get_min_division_mtDID.inc.php';
  include 'from_tDID_get_mtSID.inc.php';
  include 'from_ACID_get_min_division_mtDID.inc.php';
  include 'from_tDID_get_mtSID.inc.php';
  ?>
  <input type="hidden" name="tACID" value="<?php echo $aACID ?>">
  <input type="hidden" name="tDID" value="<?php echo $mtDID ?>">
  <input type="hidden" name="tSID" value="<?php echo $mtSID ?>">
  <div style="position:absolute; top:16%; height:11.3%; left:55.25%; width:44.25%; font-size:140%; font-weight:bold; text-align: left; text-indent: 2%; padding-top: 1%; border:thin solid black; z-index:1;">
    <?php echo $aACIDn ?>(<?php echo $aACID ?>)
  </div>

  <!-- to be taken out when Divisions in use -->
    <!-- <input type="hidden" name="tDID" value="<?php echo $mDID ?>"> -->

  <!-- ONLY TO BE ADDED WHEN FULLY WORKING ON VERSION 2.2 -->
  <!-- NEED TO ADD THE DIVISION bit -->
  <!-- TO DIVISION ?????  -->
  <!-- <div style="position:absolute; top:20%; height:4%; left:40%; width:38.5%; text-align: left; text-indent: 2%;"> -->
  <!-- <?php
  if ($acDID == 0) {
    // echo " ********** No Selection Available";
    ?>
    <input type="hidden" name="tDID" value="<?php echo $tDID ?>">
    <?php
  }elseif ($acDID == 1) {
    include 'include/from_CID_get_min_division.inc.php';
    ?>
    <input type="hidden" name="tDID" value="<?php echo $mDID ?>">
      <div style="position:absolute; top:15%; left:5%; width:90%; font-size:130%;">Division : <?php echo $mDIDn ?></div>
    <?php
  }elseif ($acDID > 1) {
    // echo "<b>$Ldivtitle</b>";
    ?>
    Division
    <select required id="tDID" name="tDID" style="position:absolute; top:40%; height:55%; left:1%; width:98%;" >
      <!-- <option value="">Select Division form list444</option> -->
      <?php
      // $ACID = $_SESSION['ACID'];
      // include 'include/from_ACID_select_division_dd.inc.php';
      // include 'include/from_ACID_select_division_dd.inc.php'
      // ?>
    <!-- </select> -->
  <?php
  }
  ?>
  <!-- </div> -->
  <?php
}elseif (isset($_GET['prt'])) {
  $CPID = $_GET['prt'];
  // include 'include/from_CPID_get_supplier_data.inc.php';
  // include 'include/from_CPID_get_partners_CID.inc.php';
  include 'include/form_new_order_INFO_PARTNERS.inc.php';
  ?>

  <!-- TO the Supplier -->
  <input type="hidden" name="tCID" value="<?php echo $tCID ?>">
  <input type="hidden" name="fCID" value="<?php echo $fCID ?>">
  <input type="hidden" name="tACID" value="<?php echo $tACID ?>">
  <input type="hidden" name="tACIDn" value="<?php echo $tACIDn ?>">
  <input type="hidden" name="tDID" value="<?php echo $tDID ?>">
  <input type="hidden" name="tSID" value="<?php echo $tSID ?>">

  <div style="position:absolute; top:16%; height:13%; left:55.25%; width:44.25%; font-size:140%; font-weight:bold; text-align: left; background-color: pink; text-indent:2%; border:thin solid black;"><?php echo $tACIDn ?></div>

  <!-- <div style="position:absolute; top:17%; height:13.8%; left:55.5%; width:44.25%; font-size:120;">
    <?php echo $tACIDn ?>(prt)
  </div> -->

  <?php
}else {
    // if ($cPOTcompanys == 0){
      ?>

      <!-- <?php
      echo "<b>$Ltitle</b>"
      ?> -->
      <?php
      if ($cacACID > 1) {
        ?>
        <div style="position:absolute; top:16%; height:13%; left:55.25%; width:44.25%; border:thin solid black; z-index:0;">
        </div>
        <select style="position:absolute; top:16.5%; left:55.5%; width:43.75%; font-size:120%; text-align: left; text-indent: 2%; margin-bottom: 1.5%; background-color:<?php echo $ddmCol ?>; border:thin solid black; z-index:1;" name="tACID">
          <option value="">Select a Client</option>
          <?php
          include 'include/from_CID_select_associate_company.inc.php';
          ?>
        </select>
        <?php
        include 'from_ACID_get_min_division_mtDID.inc.php';
        include 'from_tDID_get_mtSID.inc.php';
        ?>
        <input type="hidden" name="tDID" value="<?php echo $mtDID ?>">
        <input type="hidden" name="tSID" value="<?php echo $mtSID ?>">
        <?php
      }else {
        include 'from_CID_get_min_associate_company.inc.php';
        include 'from_ACID_get_min_division_mtDID.inc.php';
        include 'from_tDID_get_mtSID.inc.php';
        ?>
        <input type="hidden" name="tACID" value="<?php echo $mACID ?>">
        <input type="hidden" name="tDID" value="<?php echo $mtDID ?>">
        <input type="hidden" name="tSID" value="<?php echo $mtSID ?>">
        <div style="position:absolute; top:16%; height:13%; left:55.25%; width:44.25%; font-size:140%; font-weight:bold; text-align: left; text-indent: 2%; border:thin solid black; z-index:1;">
          <!-- <?php echo $mACIDn ?> -->
          <?php echo $mACIDn ?>
        </div>
        <!-- </div> -->
        <?php
      }
    // }
    // elseif ($cPOTcompanys > 0) {
    //
    //   // echo "Count of Clients :: $cPOTcompanys";
    //   include 'form_new_orders_combined_TO_ddm.inc.php';
    //   ?>
       <!-- <div style="position:absolute; top:13.2%; height:25.4%; left:40%; width:38.5%; font-weight:bold; border: thin solid black; z-index:1;">
       <select required id="tACID" onchange="FetchDIV(this.value)" style="position:absolute; top:5%; height:20%; left:1%; width:98%; font-size:125%;" name="tACID">
         <option value =" ">Please select Associate Company</option> -->
           <?php
    //       include $DEL_SEL;
    //       ?>
         <!-- </select>
       </div>
       <div style="position:absolute; top:20%; height:5%; left:40%; width:38.5%; font-weight:bold; border: thin solid black; z-index:1;">
         <select required id="tDID" onchange="FetchSECTION(this.value)" style="position:absolute; top:5%; height:80%; left:1%; width:98%;" name="tDID">
           <option value="">Select Division</option>
         </select>
       </div>
       <div style="position:absolute; top:25%; height:5%; left:40%; width:38.5%; font-weight:bold; border: thin solid black; z-index:1;">
         <select required id="tSID" style="position:absolute; top:5%; height:80%; left:1%; width:98%;" name="tSID">
           <option value="">Select Section</option>
         </select>
       </div> -->
       <!-- <script type="text/javascript">
     function FetchDIV(id){
         $('#tDID').html('<option>SEL tttt div</option>');
         $('#tSID').html('<option>SEL SEC</option>');
         // alert(id);
         // return false;
         $.ajax({
           type : 'post'
           , url : 'form_new_orders_combined_TO_ddm.inc.php'
           // , url : 'include/form_new_orders_combined_TO_ddm.inc.php'
           , data : {tDID:id}
           , success : function(data){
             $('#tDID').html(data);
           }
         })
       }
       function FetchSECTION(id){
         $('#tSID').html;
         // alert(id);
         // return false;
         $.ajax({
           type : 'POST',
           url : 'include/form_new_orders_combined_TO_ddm.inc.php',
           data : {tDID:id},
           success : function(data){
             $('#tSID').html(data);
           }
         })
       }
       </script> -->
       <?php
    // }else {
    //   // echo "Count of Clients :: $cPOTcompanys";
    // }
  }

// include 'order_FORM_div_select.inc.php';
?>
<!--
      <script type="text/javascript">
        function FetchFromDiv(id){
          $('#fDID').html('<option>Select Division</option>');
          // alert(id);
          // return false;
          $.ajax({
            type : 'POST',
            url : 'include/from_ACID_select_division_dd.inc.php',
            data : {fCID:id},
            success : function(data){
              $('#fDID').html(data);
            }
          })
        }
      </script> -->
        <!-- </div> -->



      <!-- SET the LEFT/TO Division -->
      <!-- <div style="position:absolute; top:14%; height:8%; left:3%; width:34%; background-color: #d6d6d6; text-align: left; text-indent: 2%;"> -->
      <?php
      // if (empty($tACID)) {
        // echo "No campany selected";
      // }
// include 'order_FORM_div_select.inc.php';
      //
      // if (empty($tACID)) {
      //   echo "Nothing to select";
      // }elseif (!empty($tACID)) {
      //   include 'order_FORM_div_select.inc.php';
      // }
      ?>
      <!-- </div> -->

    <!-- SET the TO Section -->

    <!-- <div style="position:absolute; top:22.5%; height:8%; left:3%; width:34%; text-align: left; text-indent: 2%; font-weight: bold; background-color:pink;">Section (if applicable)</div> -->
    <!-- <input type="text" name="tSID" value="204"> -->
    <!-- <?php
    if ($countSEC == 1) {
      echo "No sections to choose from";
    }elseif ($countSEC == 2) {
      echo "Only 1 section : ";
    }elseif ($countSEC > 1) {
      echo "Select from dropdown list";
    }
    ?> -->

    <!-- ORDER REFERENCES RIGHT SIDE -->
    <!-- <div  style="position:absolute; top:22%; height:5%; left:40.5%; width:<?php echo $refwidth ?>%; border-radius: 0px 10px 0px 0px; border:thin solid grey;"></div> -->
    <!-- <div style="position:absolute; top:15%; height:4%; left:40%; width:38.5%; text-align: left; text-indent: 2%;"><?php echo $Tref ?></div> -->
      <?php
      if (isset($_GET['cor'])) {
        // get the client order reference
        $cor = $_GET['cor'];
        if (empty($cor)) {
          $cor = "Their Order Reference";
          ?>
          <input tabindex="2" class ="input_data" type="text" style="position:absolute; top:24.5%; height:3%; left:55.25%; width:43%; background-color: <?php echo $optCol ?>;  text-align:left;  font-weight:bold;" name="their_order_ref"  placeholder="<?php echo $cor ?>" value="<?php echo $oor; ?>">
          <?php
        }else {
          ?>
          <input tabindex="2" class ="input_data" type="text" style="position:absolute; top:24.5%; height:3%; left:55.25%; width:43%; background-color: <?php echo $optCol ?>;  text-align:left;  font-weight:bold;" name="their_order_ref" value="<?php echo $cor; ?>">
          <?php
        }
      }else{
        ?>
        <input tabindex="2" class ="input_data" type="text" style="position:absolute; top:24.5%; height:3%; left:55.25%; width:43%; background-color: <?php echo $optCol ?>; text-align:left;  font-weight:bold; z-index:1" name="their_order_ref" placeholder="Their Order Reference">
        <?php
      }
      ?>

    <!-- ________________________________________________ -->
    <!-- FROM Section -->
    <!-- <div style="position:absolute; top:22.5%; height:8%; left:0.5%; width:34%; text-align: left; text-indent: 2%; font-weight: bold; background-color:yellow;">Section (if applicable)</div> -->
    <!-- <input type="text" name="fSID" value="314"> -->

    <!-- <?php
    if ($countSEC == 1) {
      echo "No sections to choose from";
    }elseif ($countSEC == 2) {
      echo "Only 1 section : ";
    }elseif ($countSEC > 1) {
      echo "Select from dropdown list";
    }
    ?> -->

<!-- ________________________________________________ -->
    <!-- ORDER REFERENCES FROM and TO -->
    <!-- <div style="position:absolute; top:30%; height:4%; left:0.5%; width:38.5%; text-align: left; text-indent: 2%; "><?php echo $Fref ?></div> -->
      <?php
      if (isset($_GET['r'])) {
        // get the client order reference
        $oor = $_GET['r'];
        ?>
        <input autofocus required class ="input_data" type="text" style="position:absolute; top:24.5%; height:3%; left:0.5%; width:53%; text-align:left;  font-weight:bold;  background-color:<?php echo $rqdCol ?>" name="our_order_ref"  placeholder="<?php echo $oor ?>" value="<?php echo $oor ?>">
        <?php
      }else{
        ?>
        <input autofocus required class ="input_data" type="text"  style="position:absolute; top:24.5%; height:3%; left:0.5%; width:53%; text-align:left;  font-weight:bold; background-color:<?php echo $rqdCol ?>"  name="our_order_ref" placeholder="Our Order Reference">
        <?php
      }
      ?>

    <!-- _______________________________________________________________________________________________  -->

    <?php
    include 'form_new_order_item_entry.inc.php';
    ?>
    <button class="entbtn" type="submit" name="orders_combi" style="position:absolute; bottom:5%; left:72.5%; width:10%; background-color: <?php echo $fsvCol ?>; z-index:1;">Place Order</button>
  <button formnovalidate class="entbtn" type="submit" name="canItem" style="position:absolute; bottom:5%; left:85%; width:10%; background-color: <?php echo $fcnCol ?>; z-index:1;">Cancel</button>

</form>
