<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/order_report.inc.php";
include 'include/from_CID_count_styles.inc.php';

// $ACID = $_SESSION['ACID'];
$CID = $_SESSION['CID'];
$td = date('U');

// Check the URL to see what page we are on
if (isset($_GET['pa'])) { $pa = $_GET['pa'];}
else{ $pa = 0;}

// Check which page we are on
if ($pa > 1) { $start = ($pa * $rpp) - $rpp;}
else { $start = 0;}

if(isset($_GET['ord'])){ $ord = $_GET['ord'];}
else{ $ord = 'oidel';}

if(isset($_GET['sort'])){ $sort = $_GET['sort'];}
else{ $sort = 'ASC';}

// Set the rows per page
$rpp = 20;


// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cPTID / $rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;

// echo '<br>Start record is : '.$start;

// echo "<br>**** count_orders count : $cPTID ****";

if (empty($_GET['s'])) {
  echo "You need to select a PRID";
}else {
  $PRID = $_GET['s'];
  include 'from_PRID_count_orders_open.inc.php';
  include 'from_PRID_count_orders_closed.inc.php';
  include 'from_PRID_count_divisions.inc.php';
  include 'from_PRID_count_seasons.inc.php';
  include 'from_PRID_count_collections.inc.php';

  $cDID = $cDID - 1;
  $cSNID = $cSNID - 1;
  $cCLID = $cCLID - 1;
?>



<table class="trs" style="position:relative; top:6%; height:10%; left:0%; width:50%;">
  <tr>
    <th style="width:20%; text-align:left; text-indent:4%;">Short Code</th>
    <th style="width:35%; text-align:left; text-indent:3%;">Description</th>
    <th style="width:16%; text-align:left; text-indent:3%;">Type</th>
  </tr>
<?php

$sql = "SELECT pr.ID as PRID
          , pr.prod_ref as PRIDr
          , pr.prod_desc as PRIDd
          , pr.etm as PRIDe
          , pt.scode as PTIDr
        FROM prod_ref pr
        	, product_type pt
          , associate_companies ac
        WHERE pr.ID = ?
        AND pr.ACID = ac.ID
        AND pr.PTID = pt.ID
        ;";
        // AND pt.name NOT IN ('Any')";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rss</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $PRID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($res)){
    $PRID  = $row['PRID'];
    $PRIDr = $row['PRIDr'];
    $PRIDd = $row['PRIDd'];
    $PRIDe = $row['PRIDe'];
    $PTIDr = $row['PTIDr'];

    ?>
    <div class="cpsty" style="position:absolute; top:26.6%; width:100%; background-color:pink; z-index:1;">
    REVIEW - Style Reference <?php echo $PRIDr ?>
    </div>

    <tr>
      <td hidden><?php echo "$PRID"; ?></td>
      <td style="width:20%; border-right:thin solid grey; text-align:left; text-indent:2%;"><?php echo "$PRIDr"; ?></td>
      <td style="width:35%; border-right:thin solid grey; text-align:left; text-indent:2%;"><?php echo "$PRIDd"; ?></td>
      <td style="width:16%; text-align:left; text-indent:2%;"><?php echo $PTIDr ?></td>
    </tr>
    <?php
    }
    ?>
  </table>
<?php }
}
?>
<table class="trs" style="position:relative; top:6%; height:10%; left:0%; width:50%;">
  <tr>
    <th>Orders - Open</th>
    <th>Orders - Closed</th>
    <th>Divisions</th>
    <th>Seasons</th>
    <th>Collections</th>
  </tr>
  <tr>
    <td style="border-right: thin solid grey;"><a style="text-decoration:none;" href="?S&sfl&s=<?php echo $PRID ?>"><?php echo $cOIDo ?></a></td>
    <td style="border-right: thin solid grey;"><a style="text-decoration:none;" href="?S&sfl&s=<?php echo $PRID ?>"><?php echo $cOIDc ?></a></td>
    <td style="border-right: thin solid grey;"><?php echo $cDID ?></td>
    <td style="border-right: thin solid grey;"><?php echo $cSNID ?></td>
    <td><?php echo $cCLID ?></td>
  </tr>
</table>

<div style="position:absolute; bottom:5%; left:10%;">

  <?php for ($x = 1 ; $x <= $PTV ; $x++){
    echo "<a href='?H&$sp&pa=$x'>  $x  </a>";
  }
  ?>
</div>
