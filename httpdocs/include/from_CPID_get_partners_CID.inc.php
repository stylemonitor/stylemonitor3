<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CPID_get_partners_CID.inc.php</b>";

$sql = "SELECT req_CID as fCID
          , acc_CID as tCID
          , rel as CTID
        FROM company_partnerships
        WHERE ID = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fcgpc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CPID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $fCID = $row['fCID'];
  $tCID = $row['tCID'];
  $CTID = $row['CTID'];
}
