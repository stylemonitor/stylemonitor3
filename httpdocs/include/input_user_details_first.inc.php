<?php
include 'dbconnect.inc.php';
// echo  "<br><b>input_user_details_first.inc.php</b>";

$status = 1;

$sql = "INSERT INTO users
          (CYID, TID, firstname, surname, userInitial, email, vKey, inputtime)
        VALUES
          (?,?,?,?,?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudf";
}else {
  mysqli_stmt_bind_param($stmt,"ssssssss", $CYID, $TZID, $UIDf, $UIDs, $UIDi, $UIDe, $vkey, $td);
  mysqli_stmt_execute($stmt);
}

$sql = "SELECT id as UID
        FROM users
        WHERE vKey = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudf2";
}else {
  mysqli_stmt_bind_param($stmt,"s", $vkey);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $UID = $row['UID'];
}

$sql = "UPDATE `users`
        SET inputuserID = ?
        WHERE UID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudf3";
}else {
  mysqli_stmt_bind_param($stmt,"ss", $UID, $UID);
  mysqli_stmt_execute($stmt);
}

// echo "<br>UID = $UID";
