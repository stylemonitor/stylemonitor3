<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/ORDER_REPORT_by_style.inc.php";

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'Order Report by Style';
$pddhbc = '#fffddd';
include 'page_description_date_header.inc.php';

include 'set_urlPage.inc.php';

?>
<table class="trs" style="position:absolute; top:9%;">
  <tr>
    <td colspan="3" style="background-color:#f1f1f1;"></td>
    <th colspan="3" style="border-radius:10px 10px 0px 0px;">Count of</th>
  </tr>

  <?php


  ?>

  <tr>
    <th style="width:10%; text-align:left; text-indent:5%;" style="width:7%;">Short Code</th>
    <th style="width:51%; text-align:left; text-indent:5%;" style="width:7%;">Description</th>
    <th style="width:15%; text-align:left; text-indent:5%;" style="width:7%;">Product Type</th>
    <th style="width:8%; text-align:left; text-indent:5%;" style="width:7%;">Orders</th>
    <th style="width:8%;">Items</th>
    <th style="width:8%;">Quantity</th>
  </tr>

  <?php
  $sql = "SELECT distinct ac.name AS ACIDn
  -- SQL is style_count_per_CID_for_open_orders.sql
         , ac.ID AS ACID
         , pr.ID AS PRID
         , pr.prod_ref AS SHORTCODE
         , pr.prod_desc AS DESCRIPTION
         , IF(pt.ID = 1,'NOT YET CHOSEN',pt.name) AS PROD_TYPE
         , COUNT(DISTINCT o.ID) AS OID_COUNT
         , COUNT(DISTINCT oi.ID) AS ITEM_COUNT
         , SUM(oiq.order_qty) AS QTY_ORDERED
  FROM order_item oi
  	 INNER JOIN orders o ON oi.OID = o.ID
  	 INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
  	 INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
  	 INNER JOIN prod_ref pr ON oi.PRID = pr.ID
  	 INNER JOIN company c ON o.fCID = c.ID
     INNER JOIN associate_companies ac ON o.fACID = ac.ID
     INNER JOIN division d ON o.fDID = d.ID
     RIGHT OUTER JOIN product_type pt ON ac.ID = pt.ACID
  WHERE (o.fCID = ? OR o.tCID = ?)
    AND oi.itComp = ?
    AND pr.ID != 1
  GROUP BY pr.ID
  ORDER BY SHORTCODE
  -- LIMIT ?,?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-oQ</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $OC);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      $ACID = $row['ACID'];
      $ACIDn = $row['ACIDn'];
      $PRID = $row['PRID'];
      $PRIDsc = $row['SHORTCODE'];
      $PRIDn = $row['DESCRIPTION'];
      $PTIDn = $row['PROD_TYPE'];
      $cOID = $row['OID_COUNT'];
      $cOIID = $row['ITEM_COUNT'];
      $cQTY = $row['QTY_ORDERED'];

      ?>
      <tr>
        <td style="text-align:left; text-indent:2%;"><a style="color:blue; text-decoration:none;" href="styles.php?S&sio&s=<?php echo $PRID ?>"><?php echo $PRIDsc ?></a></td>
        <td style="text-align:left; text-indent:2%;"><a style="color:blue; text-decoration:none;" href="styles.php?S&sio&s=<?php echo $PRID ?>"><?php echo $PRIDn ?></a></td>
        <td style="text-align:left; text-indent:2%;"><?php echo $PTIDn ?></td>
        <!-- <td><?php echo $cPRID ?></td> -->
        <td ><a style="color:blue; text-decoration:none;" href="styles.php?S&sio&s=<?php echo $PRID ?>"><?php echo $cOID ?></a></td>
        <td><?php echo $cOIID ?></td>
        <td style="text-align:right;"><?php echo $cQTY ?></td>
      </tr>
      <?php
    }
  }
  ?>
</table>
