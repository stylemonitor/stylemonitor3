<?php

// I think you only need to wrap
// Start transaction
mysqli_begin_transaction($mysqli);
try {
  // to
} catch (mysqli_sql_exception $exception) {
  mysqli_rollback($mysqli);

  throw $exception;
}


// AROUND THE queries that are already there fo rthis to work
// BUT I have no idea what th excpetion is and how that works
// but I guess that for whenit doesn't work!!!

// Below was used in form_new_style_act.inc.php
// and seems to work!!!




// Start transaction
mysqli_begin_transaction($mysqli);
try {

  // Check if the short code prod_ref is already in use
  $sql = "SELECT ID as ID
          FROM prod_ref
          WHERE ACID = ?
          AND (prod_ref = ? OR prod_desc = ?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo "FAIL-fnsa";
  }else {
    mysqli_stmt_bind_param($stmt, "sss", $ACID, $prod_ref, $prod_desc);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $PRID = $row['ID'];
  }

  if (empty($PTID)) {$PTID = $mPTID;}else {$PTID = $PTID;}

  // echo "<br>User UID : $UID";
  // echo "<br>Associate Company ACID : $ACID";
  // echo "<br>Division DID : $DID";
  // echo "<br>Product ref prod_ref : $prod_ref";
  // echo "<br>Product description prod_desc : $prod_desc";
  // echo "<br>Time to make etm : $etm";
  // echo "<br>Product type PTID : $PTID";
  // echo "<br>Season SNID : $SNID";
  // echo "<br>Collection CLID : $CLID";
  // echo "<br>Date td : $td";

  if (!empty($PRID)) {
    // echo "<br>Product reference already in use";
    // header("Location:../styles.php?S&snn&2&d=$prod_desc");
    // exit();
  }else {
    // echo "<br>Add to the list";
    if (empty($etm)) {
      $etm = 1;}else { $etm = $etm;
    }
    if (!is_numeric($etm)) {
      // echo "<br>Needs to be a positive number";
      header("Location:../styles.php?S&snn&1&r=$prod_ref&d=$prod_desc");
      exit();
    }elseif ($etm < 0) {
      // echo "<br>Needs to be a positive number!!!";
      header("Location:../styles.php?S&snn&1&r=$ref&d=$desc");
      exit();
    }
    // include 'include/SAVE_prod_ref.inc.php';
    $sql_insert_new_prod_ref = "INSERT INTO prod_ref
                                  (ACID, PTID, UID, prod_ref, prod_desc, etm, inputtime)
                                VALUES
                                  (?,?,?,?,?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql_insert_new_prod_ref)) {
      echo "FAIL-fnsa1";
    }else {
      mysqli_stmt_bind_param($stmt, "sssssss", $ACID, $PTID, $UID, $prod_ref, $prod_desc, $etm, $td);
      mysqli_stmt_execute($stmt);
    }

    // include 'include/get_PRID_from_UID_time.inc.php';
    $sql_new_PRID = "SELECT ID as PRID
                     FROM prod_ref
                     WHERE UID = ?
                     AND inputtime = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql_new_PRID)){
      echo 'FAIL-fnsa2';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $UID, $td);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $PRID = $row['PRID'];
    }

    // include 'include/SAVE_prod_ref_to_division.inc.php';
    $sql_ins_pref_to_div = "INSERT INTO prod_ref_to_div
                              (PRID, DID, UID, inputtime)
                            VALUES
                              (?,?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql_ins_pref_to_div)){
      echo 'FAIL-fnsa3';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $PRID, $NDmDID, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    // include 'include/SAVE_prod_ref_to_collection.inc.php';
    $sql_ins_pref_to_coll = "INSERT INTO prod_ref_to_collection
                              (PRID, CLID, UID, inputtime)
                             VALUES
                              (?,?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql_ins_pref_to_coll)){
      echo 'FAIL-fnsa4';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $PRID, $CLID, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    // include 'include/SAVE_prod_ref_to_season.inc.php';
    $sql_ins_pref_to_seas = "INSERT INTO prod_ref_to_season
                              (PRID, SNID, UID, inputtime)
                            VALUES
                              (?,?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql_ins_pref_to_seas)){
      echo 'FAIL-fnsa5';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $PRID, $SNID, $UID, $td);
      mysqli_stmt_execute($stmt);
    }
  }
}  catch (mysqli_sql_exception $exception) {
  mysqli_rollback($mysqli);

  throw $exception;
}
