<?php
session_start();
// echo "<br><b>include/form_new_order_item_entry_act.inc.php</b>";
include 'dbconnect.inc.php';

if (!isset($_POST['addItem']) && !isset($_POST['canItem'])) {
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['canItem'])) {
  // echo "<br>CANCEL ORDER ITEM ENTRY";
  $urlPage = $_POST['url'];
  header("Location:../$urlPage&snn");
  exit();
}elseif (isset($_POST['addItem'])) {
  // echo "<br>ORDER ITEM ENTRY";
  $UID = $_SESSION['UID'];
//
  include 'set_urlPage.inc.php';
  include 'from_CID_get_min_division.inc.php';
  include 'from_DID_get_min_section.inc.php';
  $DID = $mDID;

    // get the data from the form
  $ACID  = $_POST['ACID'];
  $OID  = $_POST['OID'];
  $OIID = $_POST['OIID'];
  $cOIID = $_POST['cOIID'];
  $mSID  = $_POST['mSID'];
  $oItemDes = $_POST['oItemDes'];
  $tItemDes = $_POST['tItemDes'];
  $fPRID    = $_POST['fPRID'];
  $orQTY    = $_POST['orQTY'];
  $IDD      = $_POST['itemDelDate'];
  // $delDate  = $_POST['delDate'];
  $urlPage  = $_POST['url'];
  $IDD      = strtotime($IDD,0);
  $td       = date('U');
  $sp = $_POST['sam_prod'];

  include 'from_ACID_get_associate_company_details.inc.php';

  $OIMRID = 2;

  include 'form_add_item_to_order_act1.inc.php';
}
