<?php
session_start();
include 'dbconnect.inc.php';

// echo "<br><b>include/form_new_orders_combined_act.inc.php</b>";
$UID = $_SESSION['UID'];
$CID = $_SESSION['CID'];
$ACID = $_SESSION['ACID'];

include 'set_urlPage.inc.php';

include 'from_CID_get_min_make_unit.inc.php';
include 'from_CID_get_min_associate_company.inc.php';
include 'from_CID_get_min_division.inc.php';
include 'from_CID_get_next_order_nos.inc.php';
include 'from_CPID_get_next_order_nos.inc.php';
include 'from_CPID_get_next_order_item_nos.inc.php';
include 'from_ACID_get_associate_company_details.inc.php';


if (!isset($_POST['orders_combi']) && !isset($_POST['canItem'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['canItem'])) {
  header("Location:../styles.php?S&snn");
  exit();
}
elseif (isset($_POST['orders_combi'])) {
  // echo "<br>Return to the Users page";

  // _____________________________________________________________________________________________
  // Gathering the data from the form
  $CPID   = $_POST['CPID'];

  // FROM company information
  $fCID  = $_POST['fCID'];
  $fACID = $_POST['fACID'];
  $fDID  = $_POST['fDID'];
    // If NO division selected OR if the associate copmany has NO divisions
    if (empty($fDID)) {
      include 'include/from_ACID_get_min_division_mfDID.inc.php';
      $fDID = $mDID;
    }
  $fSID = $_POST['fSID'];

  // TO company information
  $tCID = $_POST['tCID'];
  $tACID = $_POST['tACID'];
  $tDID  = $_POST['tDID'];

  // If NO division selected OR if the associate company has NO divisions
  if (empty($fDID)) {
    include 'include/from_ACID_get_min_division_mtDID.inc.php';
    $tDID = $mtDID;
  }

  $tSID  = $_POST['tSID'];

  $urlPage = $_POST['urlPage'];

  // Dates
  $orDate  = $_POST['orDate'];
  $orDate  = strtotime($orDate,0);
  $delDate = $_POST['delDate'];
  $delDate = strtotime($delDate,0);
  $IDD   = $_POST['itemDelDate'];
  $IDD   = strtotime($IDD,0);

  // Order information
  $td = date('U');

  $OTID   = $_POST['OTID'];
  $orNos  = $cOID;
  // $orNos  = $_POST['orNos'];
  $OOR = $_POST['our_order_ref'];
  $TOR = $_POST['their_order_ref'];

  // Product information
  $PRID  = $_POST['fPRID'];
  $orQTY = $_POST['orQTY'];
  $OrIM  = $ccpOIID;
  // $OrIM  = $_POST['orItemNos'];
  $TIR   = $_POST['oItemDes'];
  $sp    = $_POST['sam_prod'];
  $their_item_ref = $_POST['oItemDes'];
  $pItemRef = $_POST['tItemDes'];

  if (empty($sp)) {
    $sp = 2;
    $Ltime = $acLtime;
  }else {
    $sp = 1;
    $Ltime = $acsLtime;
  }



  //Additional data for order_item
  $fPRID = $_POST['fPRID'];
  // -------------------------------------------------------------------------------------------------------------------
  // echo out that information
  // The checking that the information is there

  // echo "<br>FROM Company Information";
  // echo "<br>MEMBER company fCID as $fCID";
  // echo "<br>Company ACID is fACID : $fACID";
  // echo "<br>Division DID is fDID : $fDID";
  // echo "<br> Section SID is fSID : $fSID";
  //
  // echo "<br>TO Company Information";
  // echo "<br>MEMBER company tCID as $tCID";
  // echo "<br>Company ACID is tACID : $tACID";
  // echo "<br>Division DID is tDID : $tDID";
  // echo "<br>Section SID is tSID : $tSID";
  //
  // echo "<br>Order Reference (optional) OOR : $OOR";
  //
  // echo "<b>NEW ORDERS FROM COMBINED DEPENDENCY DROPDOWN FORM</b>";
  // echo "<br>Basic order information";
  // echo "<br>Order Number orNos: $cOID";
  // echo "<br>Order type (Sales/Purchase) OTID : $OTID";
  // echo "<br>Order type (Sample/Production) sp : $sp";
  // echo "<br>Order Due by Date delDate : $delDate";
  // echo "<br>Relationship CPID : <b>$CPID</b>";
  // echo "<br>Order Reference (optional) TOR : $TOR";
  //
  // // Product detail
  // echo "<br><br>Item nos (OrIM) : $OrIM";
  // echo "<br>Style (PRID) : $PRID";
  // echo "<br>Item Description (TIR) : $TIR";
  // echo "<br>Quantity (orQTY) : $orQTY";
  // echo "<br>Item Due by Date (itemDelDate) : $IDD";
  // -------------------------------------------------------------------------------------------------------------------


  // check if the ACID has been set and set to min ACID if not
  if ($tACID == 'Select Associate Company') {
    echo "<br>NO ACID set";
    $tACID = $mACID;
    // echo "<br>tACID = mACID :: $mACID";
  }

  // CHECK that the to HAS BEEN SET
  // echo "Client $tACID";
  // echo "<br>OUR ACID = $ACID";
  if ($tACID == 0) {
    $tACID = $ACID;
  }else {
    $tACID = $tACID;
  }

  // if the TO company is empty use the min ACID value
  // Relationship between the companies



  // If NO division selected OR if the associate copmany has NO divisions
  // FROM associate
  if (empty($fDID)) {
    include 'from_ACID_get_min_division_ddf.inc.php';
    $fDID = $mDID;
  }

  if (empty($TIR)) {$TIR = $OOR;}else {$TIR = $TIR;}

  // Get the next order number
  $sql = "SELECT COUNT(ID) as cOID
          FROM orders
          WHERE CPID = ?
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-fnoa5';
  }else {
    mysqli_stmt_bind_param($stmt, "s", $CPID);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($res);
    $cOID = $row['cOID'];
  }

  $cOID = $cOID + 1;


  // _______________________________________________________________________________________________


  // // Section Information
  include 'from_tDID_get_min_section.inc.php';
  include 'from_fDID_get_min_section.inc.php';

  // echo "<br>From db Section information";
  // echo "<br>Section SID is tSID : $mtSCID";
  // echo "<br>Section SID is fSID : $mfSCID";

  // _______________________________________________________________________________________________
  // Now the actual work is to be done

// THIS NEEDS CHECKING AS ALL THE to and from might not be exactly right!!!!!!
  if (empty($fDID)) {$fDID = $tDID;} else {$fDID = $fDID;}
  // if (empty($mfDID)) {$mfDID = $mtDID;} else {$mfDID = $mfDID;}
  if (empty($tPRID)) {$tPRID = $fPRID;} else {$tPRID = $tPRID;}

  if (empty($pPRID)) {$pPRID = $PRID;}

  // echo "<br>Production lead time is : $acLtime";
  // echo "<br>Sample lead time is : $acsLtime";
  // echo "<br>Original order delivery date is : $delDate";
  // echo "<br>Original item delivery date is : $IDD";

  if ($sp <> 1) {
    $sp = 2;
    $Ltime = $acLtime;
    // if not set then PRODUCTION
    // set the automatic delivery date as $acLtime * 86400
  }else {
    $sp = $sp;
    $Ltime = $acsLtime;
  }

  if (empty($delDate)) {
    $delDate = $td + ($Ltime * 86400);
  }else {
    $delDate = $delDate;
  }

  if (empty($IDD)) {
    $IDD = $td + ($Ltime * 86400);
  }else {
    $IDD = $IDD;
  }

  // echo "<br>Re-set item delivery date is : $IDD";

  // set the order date as todays date
  if (empty($orDate)) {
    $orDate = $td;
  }else {
    $ordate = $orDate;
  }

  // echo "<br>Sample/production type (sp) : $sp";
  // echo "<br>Revised order delivery date is : $delDate";
  // echo "<br>Revised item delivery date is : $IDD";

  // converts the calendar date to UTC format - DO NOT CHANGE
  $delDate = strtotime($delDate,0);
  // $IDD = strtotime($IDD,0);
  // echo "<br>Re-set2 item delivery date is : $IDD";

  // get the other company/division details IF
  // they are NOT from our Company
  if ($fCID <> $tCID) {
    // select the minimum DID based on the fCID
    include 'from_fCID_get_first_division.inc.php';
    include 'from_tCID_get_first_division.inc.php';
    // $fDID = $fDID;
    // echo "<br> fDID comes back as : $fDID";
  }

  if ($OTID == 1) { $DID = $fDID; $PDID = $tDID;}
  if ($OTID == 2) { $DID = $tDID; $PDID = $fDID;}
  if ($OTID == 3) { $DID = $fDID; $PDID = $tDID;}
  if ($OTID == 4) { $DID = $tDID; $PDID = $fDID;}
  //


  // temp fix!!!!
  if (empty($tACID)) {
    $tACID = $fACID;
  }else {
    $tACID = $tACID;
  }

  if (empty($OOR)) { $OOR = $TOR;}else { $OOR = $OOR;}

  if (!is_numeric($orQTY) || ($orQTY < 1)) {
    // echo "<br>The value is NOT a number";
    header("location: ../$urlPage&orn&s&cor=$OOR&sor=$OOR&des=$TIR&1");
    exit();
  }else {

    // Start transaction
    // mysqli_begin_transaction($mysqli);
    // try {
    // to

    // echo "<br>$CPID, $tCID, $tACID, $tDID, $tSID, $fCID, $fACID, $fDID, $fSID, $OTID, $UID, $orNos, $orDate, $OOR, $TOR, $td";
    //Put the data into the db
    $sql = "INSERT INTO orders
              (CPID, tCID, tACID, tDID, tSID, fCID, fACID, fDID, fSID, OTID, UID, orNos, orDate, our_order_ref, their_order_ref, inputtime)
              VALUES
              (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa';
    }else {
      mysqli_stmt_bind_param($stmt, "ssssssssssssssss", $CPID, $tCID, $tACID, $tDID, $tSID, $fCID, $fACID, $fDID, $fSID, $OTID, $UID, $orNos, $orDate, $OOR, $TOR, $td);
      mysqli_stmt_execute($stmt);
    }

    // GET the order ID as $OID
    $sql = "SELECT MAX(ID) as OID
            FROM orders
            WHERE fCID = ?
            AND tCID = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa2';
    }else {
      mysqli_stmt_bind_param($stmt, "ss", $fCID, $tCID);
      mysqli_stmt_execute($stmt);
      $res = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($res);
      $OID = $row['OID'];
    }

    // $OID = $OID + 1;

    // echo "<br>Order ID is $OID";
    // echo "<br>Insert into orders_due_date :: $OID, $UID, $delDate, $td";
    // Add the order due date
    // Still needed until removed from another query for home.php?H&tab = etc
    $sql = "INSERT INTO orders_due_dates
              (OID, UID, del_date, inputtime)
            VALUES
              (?,?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa3';
    }else {
      mysqli_stmt_bind_param($stmt, "ssss", $OID, $UID, $delDate, $td);
      mysqli_stmt_execute($stmt);
    }

    // echo "Order ID is ::: $OID";
    // Add the item details to the order_item table
    // itCompDate and itDesDate sent as todays date as a default so that db accepts them
    // pItemRef initially set as PRID for both associate and partner as needs setting
    // CAN CHANGE FOR PARTNERS
    // WILL NEED TO SET RULES THAT IF THEY MATCH INPUTTIME THEN IGNORE THEM LATER!!!!
    // echo " $OID, $fPRID, $tPRID, $sp, $UID, $OrIM, $TIR, $PRID, $td";
    // echo "<br>INSERT INTO order_item :: $OID, $fPRID, $tPRID, $sp, $UID, $OrIM, $TIR, $TIR, $td";
    $sql = "INSERT INTO order_item
              (OID, PRID, pPRID, samProd, UID, ord_item_nos, their_item_ref, pItemRef, inputtime)
            VALUES
              (?,?,?,?,?,?,?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa4';
    }else {
      mysqli_stmt_bind_param($stmt, "sssssssss", $OID, $fPRID, $tPRID, $sp, $UID, $OrIM, $their_item_ref, $pItemRef, $td);
      mysqli_stmt_execute($stmt);
    }

    // echo "<br>select ID order_item :: $OID, $OrIM, $td";

    // GET the order item ID
    $sql = "SELECT ID
            FROM order_item
            WHERE OID = ?
            AND ord_item_nos = ?
            AND inputtime = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa5';
    }else {
      mysqli_stmt_bind_param($stmt, "sss", $OID, $OrIM, $td);
      mysqli_stmt_execute($stmt);
      $res = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($res);
      $OIID = $row['ID'];
    }

    // echo "Order Item ID is :/: $OIID";
    // echo "<br>order_item_del_date :: $OIID, $UID, $IDD, $td";
    // Add the order item delivery date
    $sql = "INSERT INTO order_item_del_date
              (OIID, UID, item_del_date, inputtime)
            VALUES
              (?,?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa6';
    }else {
      mysqli_stmt_bind_param($stmt, "ssss", $OIID, $UID, $IDD, $td);
      mysqli_stmt_execute($stmt);
    }

    // GET the SECTION id for the TO company ie the one that is going to make it
    $DID = $tDID;
    include 'from_DID_get_min_section.inc.php';
    // echo "<br>Make Division fDID = DID : $tDID/$DID ";
    // echo "<br>Section ID : $mSCID";

    // Add the make unit

    // echo "<br>order_placed :: $OIID, $mSCID, $UID, $td";
    $sql = "INSERT INTO order_placed
              (OIID, SID, UID, inputtime)
            VALUES
              (?,?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa7';
    }else {
      mysqli_stmt_bind_param($stmt, "ssss", $OIID, $mSCID, $UID, $td);
      mysqli_stmt_execute($stmt);
    }

    // get the OPID
    $sql = "SELECT op.ID as OPID
            FROM order_placed op
            WHERE op.SID = ?
            AND op.OIID = ?
            ANd op.UID = ?
            AND op.inputtime = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa8';
    }else {
      mysqli_stmt_bind_param($stmt, "ssss", $mSCID, $OIID, $UID, $td);
      mysqli_stmt_execute($stmt);
      $res = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($res);
      $OPID = $row['OPID'];
    }

    include 'action/insert_into_OPM_initial_item.act.php';

    // $OIMRID = 2;
    // $note = "Initial Order Placement";
    // // echo "<br>Order Placed ID is :: $OPID";
    // // echo "<br>order_placed_move :: $OPID, $UID, $orQTY, $td";
    // // add into the order_placed_move table
    // $sql = "INSERT INTO order_placed_move
    //           (OPID, OIMRID, UID, omQty, type, inputtime)
    //         VALUES
    //           (?,?,?,?,?,?);
    // ";
    // $stmt = mysqli_stmt_init($con);
    // if(!mysqli_stmt_prepare($stmt, $sql)){
    //   echo '<br>FAIL-fnoa9';
    // }else {
    //   mysqli_stmt_bind_param($stmt, "ssssss", $OPID, $OIMRID, $UID, $orQTY, $note, $td);
    //   mysqli_stmt_execute($stmt);
    // }

    // echo "<br>order_item_qty :: $OIID, $UID, $orQTY, $td";
    // Add the item quantity to the order item qty table
    $sql = "INSERT INTO order_item_qty
              (OIID, UID, order_Qty, inputtime)
            VALUES
              (?,?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoa1';
    }else {
      mysqli_stmt_bind_param($stmt, "ssss", $OIID, $UID, $orQTY, $td);
      mysqli_stmt_execute($stmt);
    }

    // echo "<br>order_placed ID is $OPID";
    // echo "<br>EVERYTHING WORKED OK!!!";
    // $OID = $OID -;

    // } catch (mysqli_sql_exception $exception) {
    //   mysqli_rollback($mysqli);
    //
    //   throw $exception;
    // }

    // IF it is a PARTNERSHIP ORDER
    if ($fCID <> $tCID) {
      // echo "SEND EMAIL cpid = $CPID";
      include 'email/email_partner_order_placed_SM_p3.inc.php';
    }

    header("Location:../$urlPage&orp=$OID");
    exit();

  }
}
