<?php
include 'dbconnect.inc.php';
// echo "include/report_ORDER_STATUS_summary.inc.php";

$CID = $_SESSION['CID'];

include 'set_urlPage.inc.php';
include 'from_UID_get_last_logout.inc.php'; // $LRIDout
include 'ot_sp_selection_movement.inc.php';

$td = date('U');

if (isset($_GET['H'])) {
  $hbc = '#b7d8ff';
  $LtabTitle = 'Order Status';
}elseif (isset($_GET['L'])) {
  $hbc = '#ffffd1';
  $LtabTitle = 'Factory Loading';
}

// sets the order type
if (isset($_GET['ot'])) {
  $ot = $_GET['ot'];
}

// sets the sample/production type
if (isset($_GET['sp'])) {
  $sp - $_GET['sp'];
}

if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  if (($tab == 0) || ($tab == 9)) {
    $pddh = 'Order Summarytytytyty : <b>All Dates</b>';
    $pddhbc = '#b7d8ff';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td - 31536000;
    $ed = $td + 31536000;
  }elseif ($tab == 1) {
    $pddh = 'Order Item Summary : OVERDUE';
    $pddhbc = '#fc4141';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td - 31536000;
    $ed = $td;
  }elseif ($tab == 2) {
    $pddh = 'Order Summary Item : THIS WEEK';
    $pddhbc = '#f7a0a0';
    $dr = 'oidd.item_del_date';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td;
    $ed = $td + 604800;
  }elseif ($tab == 3) {
    $pddh = 'Order Summary Item : NEXT WEEK';
    $pddhbc = '#ecf5a9';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td + 604800;
    $ed = $td + 1209600;
  }elseif ($tab == 4) {
    $pddh = 'Order Summary Item : AFTER TWO WEEKS';
    $pddhbc = '#cef8d3';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td + 1209600;
    $ed = $td + 31536000;
  }elseif ($tab == 9) {
    $pddh = 'Order Summary All Dates';
    $pddhbc = '#ffffd1';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td - 31536000;
    $ed = $td + 31536000;
  }
}
include 'page_description_date_header.inc.php';
include 'page_selection_header.inc.php';
include 'order_QUERY_count.inc.php';

include 'from_CID_count_section_totals.inc.php';

?>
<table style="position:absolute; top:18%; left:3%; width:94%;">
  <tr>
    <th colspan="2">Awaiting</th>
    <th colspan="2">MP1</th>
    <th colspan="2">MP2</th>
    <th colspan="2">MP3</th>
    <th colspan="2">MP4</th>
    <th colspan="2">MP5</th>
    <th colspan="2">Warehouse</th>
  </tr>
  <tr>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
  </tr>

  <tr>
    <td><?php echo $cORst_awt ?></td>
    <td><?php echo $AWAITING ?></td>
    <td><?php echo $cORst1 ?></td>
    <td><?php echo $stotal1 ?></td>
    <td><?php echo $cORst2 ?></td>
    <td><?php echo $stotal2 ?></td>
    <td><?php echo $cORst3 ?></td>
    <td><?php echo $stotal3 ?></td>
    <td><?php echo $cORst4 ?></td>
    <td><?php echo $stotal4 ?></td>
    <td><?php echo $cORst5 ?></td>
    <td><?php echo $stotal5 ?></td>
    <td><?php echo $cORwhs ?></td>
    <td><?php echo $WAREHOUSE ?></td>
  </tr>
</table>
<?php
