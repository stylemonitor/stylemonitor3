<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_PRID_count_orders_open.inc.php</b>";
$sql = "SELECT COUNT(o.ID) as cOID
				FROM orders o
					, order_item oi
					, prod_ref pr
				WHERE pr.ID = ?
				AND oi.PRID = pr.ID
				AND oi.OID = o.ID
				AND o.orComp = 0
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fpro</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $PRID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $cOIDo = $row['cOID'];
}
