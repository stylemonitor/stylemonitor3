<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_ACID_get_associate_company_details.inc.php</b>";

if (isset($_GET['id'])) {
  $ACID = $_GET['id'];
}

$sql = "SELECT ac.name as ACIDn
          , ac.acLtime as acLtime
          , ac.acsLtime as acsLtime
          , ac.CTID as CTID
        FROM associate_companies ac
        WHERE ac.id = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $ACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $ACIDn = $row['ACIDn'];
  $acLtime = $row['acLtime'];
  $acsLtime = $row['acsLtime'];
  $CTID = $row['CTID'];
}
