<?php
session_start();
include 'dbconnect.inc.php';
$UID = $_SESSION['UID'];
$CID = $_SESSION['CID'];
$td = date('U');
echo "<br><b>include/form_edit_item_can_act.inc.php</b>";

if (!isset($_POST['YesBtn']) && !isset($_POST['NoBtn'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['NoBtn'])) {
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  header("Location:../home.php?H&o=$OID&eit=$OIID");
  exit();
}elseif (isset($_POST['YesBtn'])) {
  // echo "CANCEL ITEM FROM ORDER";
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  $OPID = $_POST['OPID'];
  $canReas = $_POST['canReas'];
  $td = date('U');

  // remove item from order
  $sql = "UPDATE order_item
          SET itComp = 2
            , itCompDate = ?
            , note = ?
          WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-feoidca</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $td, $canReas, $OIID);
    mysqli_stmt_execute($stmt);
  }

  $OIMRID = 41;
  $qty = 0;

  $sql = "INSERT INTO order_placed_move
            (OPID, OIMRID, UID, omQty, type, inputtime)
          VALUES
            (?,?,?,?,?,?)
  ;";
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-feoidca</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssssss", $OPID, $OIMRID, $UID, $qty, $canReas, $td);
    mysqli_stmt_execute($stmt);
  }

  header("Location:../home.php?H&o=$OID&eit=$OIID");
  exit();
}
