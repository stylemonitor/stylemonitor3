<?php
include 'from_CID_count_product_type.inc.php';

if (!isset($_GET['fpto']) && !isset($_GET['fnpt'])) {
  if ($cPTID == 0) {
    ?>
    <div style="position:absolute; top:37%; height:2.5%; right:5%; width:90%; background-color:#c9d1ce;">
      Product Type NOTES 1a
      <br><br>
      <p>To <a style="color:blue; text-decoration:none; font-weight:bold;" href="styles.php?S&spt&fnpt">Add a Product Type</a></p>
      <br>Click on the button in the
      <br>bottom right corner
    </div>
    <?php
  }else {
    ?>
    <div style="position:absolute; top:37%; height:2.5%; right:5%; width:90%; background-color:#c9d1ce;">
      Product Type NOTES 1b
      <br><br>
      <p>To <a style="color:blue; text-decoration:none; font-weight:bold;" href="styles.php?S&spt&fnpt">Add a Product Type</a></p>
      <br>
      <p>To <a style="color:blue; text-decoration:none; font-weight:bold;" href="styles.php?S&snn&fns">Add a Style</a></p>
      <br>Click on the button in the
      <br>bottom right corner
    </div>
    <?php
  }
  ?>
  <?php
}elseif(isset($_GET['fnpt'])) {
  ?>
  <div style="position:absolute; top:37%; height:2.5%; right:5%; width:90%; background-color:#c9d1ce;">
    Product Type NOTES 2
    <p>Enter a nine character name/number
      in the
      <br><br><b>Short Code</b> box
      <br><br>and then add a longer
      <br><br><b>Description</b>
      <br><br>and then click on
      <br><br><b>SAVE</b>
    </div>
  <?php
}elseif (isset($_GET['fpto'])) {
?>
<div style="position:absolute; top:27%; height:2.5%; right:20%; width:60%; background-color:#c9d1ce;">REVIEW A PRODUCT TYPE
  <p>To Review a style and the number and nature of its orders select
    <br><b>Review the Product Type</b></p>
  </div>

  <div style="position:absolute; top:57%; height:2.5%; right:20%; width:60%; background-color:#c9d1ce;">EDIT A PRODUCT TYPE
    <p>To Edit a style by changing its short code, description etc select
      <br><b>Edit the Product Type</b></p>
    </div>
<?php
}
?>
