<?php
include 'dbconnect.inc.php';
$sm = '<b>S</b>tyle<b>M</b>onitor';

?>
<div class="cpsty" style="top:0%; height:3%; left:0%; width:100%; background-color:#e08df4;">
  <p>How to ... ORDERS</p>
</div>
<div class="" style="top:3%; height:97%; left:0%; width:100%; background-color:#f4f4f4;">
  <br>
  <p>No information to add here, just reports to view and to investigate.</p>
  <br>
  <p>When the cursor changes to a pointing finger, you've guessed it, you go somewhere else related to that change.</p>
  <br>
  <p>Happy hunting.</p>
  <br>
  <p>When you click on the order number you will get taken to that order.  You are then shown that order and all the relevent information regarding the order.</p>
  <br>
  <p>At the foot of the page are several choices.</p>
  <br>
  <p>* * * <a style="text-decoration:none:"href="?W&hrm">Record Movement</a> * * *</p>
  <br>
  <p>* * * <a style="text-decoration:none:"href="?W&hml">Movement Log</a> * * *</p>
  <br>
  <p>* * * <a style="text-decoration:none:"href="?W&hmu">Make Unit Details</a> * * *</p>
</div>
