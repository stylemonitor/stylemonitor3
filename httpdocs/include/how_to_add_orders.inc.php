<?php
include 'dbconnect.inc.php';
$sm = '<b>S</b>tyle<b>M</b>onitor';

?>
<div class="cpsty" style="top:0%; height:3%; left:0%; width:100%; background-color:#e08df4;">
  <p>How to ... ORDERS - New</p>
</div>
<div class="" style="top:3%; height:97%; left:0%; width:100%; background-color:#f4f4f4;">
  <br>
  <p>First select whether it is a <b>Sales</b> or <b>Purchase</b> order</p>
  <br>
  <p>Then you can add what you can in the spaces provided ie</p>
  <br>
  <p><b>Our Order Ref</b></p>
  <p>What ever you want, names, numbers etc</p>
  <br>
  <p><b>Their Order Ref</b></p>
  <p>What ever you want, names, numbers etc</p>
  <br>
  <p><b>Their Item Ref</b></p>
  <p>What ever you want, names, numbers etc</p>
  <br>
  <p><b>Quantity</b></p>
  <br>
  <p><b>Order Date</b> - select from the calendar OR leave alone</p>
  <p>It will automatically put in todays date</p>
  <br>
  <p><b>Order Due date</b>  - select from the calendar OR leave alone</p>
  <p>it will atuomatically put in a date 3 months from today</p>
  <p>unless you have a Due Date, then it uses that one</p>
  <br>
  <p><b>Due Date</b> - this is for the line and can be different from the Order Due Date</p>
  <p>If you don't put anything in, it takes the Order Due Date and uses that</p>
  <br>
  <p><b>IF</b>you have more than one Client or Supplier, then a drop-down list will be there for you to select from.</p>
  <br>
  <p>When you have more than one style to order, again a drop-down list will appear.</p>
</div>
