<?php
// echo "<br><i>form_reg_act_db.php</i>";
// the working of the file form_action_reg.php
// ADD the user to the users table

// echo "<br>Recommended company CID is $rec_CID";

// mysqli_begin_transaction($mysqli);
// try {
  // to
  $sql = "INSERT INTO users
          (firstname, surname, email, vKey, inputtime)
          VALUES
          (?,?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad1</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sssss", $UIDf, $UIDs, $UIDe, $vkey, $td);
    mysqli_stmt_execute($stmt);
  }

  // GET the rec_CID ID number
  if ($rec_CID == 1) {
  }else {
    $sql = "SELECT ID
    FROM company
    WHERE SMIC = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-frad1</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "s", $rec_CID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $rec_CID = $row['ID'];
    }
  }

  // echo '<br><br><b>UPDATE uid AND UID in the users table (81)</b>';

  // GET the users new ID as $UID
  $sql = "SELECT ID
          FROM users
          WHERE email = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad2www</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UIDe);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $UID = $row['0'];
  }

  // echo '<br>The Users ID is set as : <b>'.$UID.'</b>';
  // echo '<br><br><b>Set the Users uid (98)</b>';


  $abvfirst = substr($UIDf,0,1);
  $abvsur = substr($UIDs,0,2);
  $UIDu = strtoupper($abvfirst.'-'.$UID.'-'.$abvsur);
  // echo '<br>User Abv first: <b>'.$abvfirst.'</b>';
  // echo '<br>User Abv sur : <b>'.$abvsur.'</b>';
  // echo '<br>User uid code : <b>'.$UIDu.'</b>';

  $UIDi = strtoupper($abvfirst.$abvsur);
  // echo '<br>User initials are set at : '.$UIDi;

  // UPDATE the NEWLY registered user with the uid and initials of the user
  $sql = "UPDATE users
          SET uid = ?
            , userInitial = ?
            , inputuserID = ?
          WHERE ID = ?;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad3</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $UIDu, $UIDi, $UID, $UID);
    mysqli_stmt_execute($stmt);
  }

  // GET the uid from the users table as $UIDu1
  $sql = "SELECT uid
          FROM users
          WHERE email=?;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad4</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UIDe);
    mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $UIDu1 = $row['0'];
  }
  // echo '<br>The Users uid code is : <b>'.$UIDu1.'</b>';
  // echo '<br>The country id is : <b>'.$UIDy.'</b>';
  // echo '<br><br><b>Insert and get the company information into the company table (153)</b>';
  // ADD the company into the company table
  $sql = "INSERT INTO company
            (rCID, CYID, UID, name, inputtime)
          VALUES
            (?,?,?,?,?)";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad5</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sssss", $rec_CID, $UIDy, $UID, $UIDc, $td);
    mysqli_stmt_execute($stmt);
  }

  // GET the company ID as $CID
  $sql = "SELECT ID as CID
          FROM company
          WHERE UID = ?;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad6</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $CID = $row['CID'];
  }

  // echo '<br>The company ID ($CID) is now : <b>'.$CID.'</b>';
  // TRIAL licence information
  $LTID = 1;
  $cTrial = 1;
  $cUsers = 10;

  // Add the licence type to the company_licence table
  $sql = "INSERT INTO company_licence
            (CID, LTID, UID, st_date)
          VALUES
            (?,?,?,?);";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad7</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $CID, $LTID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // Add the licence type to the log_company_licence table
  $sql = "INSERT INTO log_company_licence
            (CID, LTID, cTrial, cUsers, cDate, UID)
          VALUES
            (?,?,?,?,?,?);";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad71</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssssss", $CID, $LTID, $cTrial, $cUsers, $td, $UID);
    mysqli_stmt_execute($stmt);
  }

  // Create an address for the Sponsor Company
  // ADD the company as an associate company
  $sql = "INSERT INTO associate_companies
            (CID, name, CYID, UID, inputtime)
          VALUES
            (?,?,?,?,?);";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad8</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sssss", $CID, $UIDc, $UIDy, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // GET the new associate company ID as ACID
  $sql = "SELECT ID as ACID
          FROM associate_companies
          WHERE CID = ?;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad9/b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $CID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $ACID = $row['ACID'];
  }
  // echo '<br>The associate company ID (ACID) is now : <b>'.$ACID.'</b>';

  // NEED to create a Partnership with themselves!
  $req_SEL = bin2hex(random_bytes(16));
  $req_SEL = password_hash($req_SEL, PASSWORD_DEFAULT);
  // echo "<br>Our code (req_SEL) : $req_SEL";
  // echo '<br>req_SEL value as : '.$req_SEL;

  // prepare a hashed verifying code for the requested (the other member) member
  $acc_SEL = bin2hex(random_bytes(16));
  $acc_SEL = password_hash($acc_SEL, PASSWORD_DEFAULT);
  // echo "<br>Partners code (acc_SEL) : $acc_SEL";

  // Add the date to the company_partnership table
  $sql = "INSERT INTO company_partnerships
          (insCID, req_CID, req_UID, req_SEL, rel, acc_CID, acc_UID, acc_SEL, active, accdate, inputtime)
          VALUES
          (?,?,?,?,'7',?,?,?,'1',?,?)
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad-fnpa</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sssssssss", $CID, $CID, $UID, $req_SEL, $CID, $UID, $acc_SEL, $td, $td);
    mysqli_stmt_execute($stmt);
  }

  // ADD an initial address into the addresses table
  $sql = "INSERT INTO addresses
            (ACID, CYID, TID, UID,inputtime)
          VALUES
            (?,1,201,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad10</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $ACID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  //Get the ID for the sponsor company to division address
  $sql = "SELECT ID as AID
          FROM addresses
          WHERE ACID = ?
          AND UID = ?
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad11</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $ACID, $UID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $AID = $row['AID'];
  }

  // echo '<br>The address ID for this company is : <b>'.$AID.'</b>';
  // echo '<br><br><b>Insert and get the company details from the division table (206)</b>';

  // ADD a divison to the company / associate company
  $sql = "INSERT INTO division
            (ACID, UID, inputtime)
          VALUES
            (?,?,?)
            ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad12</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $ACID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // GET the division ID DID
  $sql = "SELECT ID as DID
          FROM division
          WHERE UID = ?
          ;";
    $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad13</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $DID = $row['DID'];
  }

  // echo '<br>The division.ID is : <b>'.$DID.'</b>';
  // ADD a section to the division
  $sql = "INSERT INTO section
          (DID, UID, name, inputtime)
          VALUES
          (?,?, 'Confirmation',?)
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad14</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $DID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // GET the section ID SID
  $sql = "SELECT ID as SID
          FROM section
          WHERE UID=?
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad15</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $SID = $row['SID'];
  }

  //Add a company division address to the company
  $sql = "INSERT INTO company_division_address
            (DID, AID, UID, inputtime)
          VALUES
            (?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad16</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $DID, $AID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // echo '<br><br><b>Add and get a company_division_user ID (231)</b>';
  // ADD the user as the company user
  $sql = "INSERT INTO company_division_user
            (DID, UID, cUID, position, inputtime)
          VALUES
            (?,?,?,?,?);";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad17</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sssss", $DID, $UID, $UID, $UIDn, $td);
    mysqli_stmt_execute($stmt);
  }

  // ADD a default collection to the company
  $sql = "INSERT INTO collection
          (ACID, uid, inputtime)
          VALUES
          (?,?,?);";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad18</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $ACID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // Get Collection ID
  $sql = "SELECT ID as CLID
  FROM collection
  WHERE ACID = ?
  AND UID = ?
  AND inputtime = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad200</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $ACID, $UID, $td);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $CLID = $row['CLID'];
  }

  // ADD a default season to the company
  $sql = "INSERT INTO season
          (ACID, uid, inputtime)
          VALUES
          (?,?,?);";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad19</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $ACID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // Get Season ID
  $sql = "SELECT ID as SNID
  FROM season
  WHERE ACID = ?
  AND UID = ?
  AND inputtime = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad200</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $ACID, $UID, $td);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $SNID = $row['SNID'];
  }

  $sql = "INSERT INTO product_type
            (ACID, UID, inputtime)
          VALUES
            (?,?,?)
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad20</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $ACID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  $sql = "SELECT ID as PTID
          FROM product_type
          WHERE ACID = ?
          and UID = ?;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad21</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $ACID, $UID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $PTID = $row['PTID'];
  }

  // Add a product reference for use by a PARTNER company when placing an order
  // the reference tells them they need to select a prod ref from their list
  $sql = "INSERT INTO prod_ref
          (ACID, PTID, UID, prod_ref, prod_desc, etm, inputtime)
          VALUES
          (?,?,?,'TBC','To be selected',1,?)
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad220</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $ACID, $PTID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // Get prod_ref ID
  $sql = "SELECT ID as PRID
  FROM prod_ref
  WHERE UID = ?
  AND inputtime = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad201</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $UID, $td);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $PRID = $row['PRID'];
  }

  // LINK prod_ref to 1st division
  $sql = "INSERT INTO prod_ref_to_div
          (PRID, DID, UID, inputtime)
          VALUES
          (?,?,?,?)
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad221</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $PRID, $DID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // LINK prod_ref to 1st collection
  $sql = "INSERT INTO prod_ref_to_collection
          (PRID, CLID, UID, inputtime)
          VALUES
          (?,?,?,?)
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad222</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $PRID, $CLID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // LINK prod_ref to 1st season
  $sql = "INSERT INTO prod_ref_to_season
          (PRID, SNID, UID, inputtime)
          VALUES
          (?,?,?,?)
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad223</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $PRID, $SNID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // ADD product_type to company
  $sql = "INSERT INTO company_product_types
          (CID, PTID, UID, inputtime)
          VALUES
          (?,?,?,?)
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad22</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $CID, $PTID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // ADD product_type to associate_companies
  $sql = "INSERT INTO associate_company_product_types
          (ACID, PTID, UID, inputtime)
          VALUES
          (?,?,?,?)
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad22</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $ACID, $PTID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // ADD product_type to division
  $sql = "INSERT INTO division_product_types
          (DID, PTID, UID, inputtime)
          VALUES
          (?,?,?,?)
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad22</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $DID, $PTID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // ADD a default season to the company
  $sql = "INSERT INTO section_product_types
          (SID, PTID, UID, inputtime)
          VALUES
          (?,?,?,?)
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad22</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ssss", $SID, $PTID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // GET the ID of the user to use as the uid value in the users table
  $sql = "SELECT ID
          FROM company_division_user
          WHERE UID = ?;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad23</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $CIDc = $row['0'];
  }
    // echo '<br>The company division code is : <b>'.$CIDc.'</b>';
    // echo '<br><br><b>Create the SMIC Code (248)</b>';

  // GET the 3 digit UN country code
  $sql = "SELECT UN_code3 as CCO
          FROM country
          WHERE ID=?;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad24</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UIDy);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    if($row = mysqli_fetch_array($result));
    $CCO = $row['CCO'];
    // echo '<br>The 3 letter country code is : <b>'.$CCO.'</b>';
  }

  // echo '<br><br><b>Create and add the SMIC suffix code (266)</b>';

  // GET a count of the company's listed
  $sql = "SELECT count(inputtime)-1 as total
          FROM company ;";
  $result = mysqli_query($con, $sql);
  $row = mysqli_fetch_assoc($result);
  // echo '<br><b>COUNT : </b>'.$row['total'];
  // $RT = $row['total'];
  $alphabet = range('A','Z');

  $sql = "SELECT COUNT(ID) as cCID
          FROM company
          WHERE CYID = ?";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad25</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UIDy);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_assoc($result);
    $cCID = $row['cCID'];
  }

  // Then one of the following
  // echo '<br>Count of companies registered today : '.$cCID;

  // The number of 'rows per page' ie 26 letters of the alphabet!
  $cpp = 26;

  // Get the number of times 26 goes into the count of companies registered today from a particular country
  $div = intval($cCID/$cpp);

  // Get the number left over
  $res = fmod($cCID, $cpp);

  while ($div >= 26) {
    $div = intval($cCID/$cpp);
    $res = fmod($cCID, $cpp);
  }

  // Create the SMIC code for the company
  $abvCOM = substr($UIDc,0,2);
  $SMICd	= gmdate('myd');
  $SMIC = strtoupper($abvCOM.$CCO.'-'.$SMICd.'-'.$alphabet[$div].$alphabet[$res]);
  // echo '<br> The SMIC is <b>'.$SMIC.'</b>';

  // UPDATE the company table with the SMIC for that company
  $sql = "UPDATE company
          SET SMIC = ?
          WHERE UID = ?
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad3</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $SMIC, $UID);
    mysqli_stmt_execute($stmt);
  }

  // echo '<br><br><b>Check the db SMIC (294)</b>';

  // GET the SMIC for the company
  $sql = "SELECT SMIC as CIDs
            , name CIDn
          FROM company
          WHERE UID = ?;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad26></b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UID);
    mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $CIDs = $row['CIDs'];
  $CIDn = $row['CIDn'];
  }

  // echo '<br>The db company SMIC ref is : <b>'.$SMIC.'</b>' ;
  // echo "<br>They should be registered by now";
  // Add this back in on the interenet version so that the email gets sent
  //
  // if (empty($rec_CID)) {
  //   $CID = 1;
  //   $sql = "SELECT SMIC as CIDs
  //           FROM company
  //           WHERE ID = ?;";
  //   $stmt = mysqli_stmt_init($con);
  //   if (!mysqli_stmt_prepare($stmt, $sql)){
      // echo '<b>FAIL-frad26></b>';
  //   }else{
  //     mysqli_stmt_bind_param($stmt, "s", $CID);
  //     mysqli_stmt_execute($stmt);
  //   $result = mysqli_stmt_get_result($stmt);
  //   $row = mysqli_fetch_array($result);
  //   $rec_CID = $row['CIDs'];
  //   }
  // }else {
  //   $rec_CID = $_POST['rec_CID'];
  // }

  // $sql = "INSERT INTO recon_by
  //           (rec_CID, CID, UID, inputtime)
  //         VALUES
  //           (?,?,?,?)
  //         ;";
  // $stmt = mysqli_stmt_init($con);
  // if (!mysqli_stmt_prepare($stmt, $sql)){
  //   // echo '<b>FAIL-frad23</b>';
  // }else{
  //   mysqli_stmt_bind_param($stmt, "ssss", $rec_CID, $CIDs, $UID, $td);
  //   mysqli_stmt_execute($stmt);
  // }

  // echo "Recommending CID : $rec_CID";
  // echo "Interested CID : $CIDs";
// } catch (mysqli_sql_exception $exception) {
//   mysqli_rollback($mysqli);
//
//   throw $exception;
// }

include 'email_reg.inc.php';
