<?php
session_start();
include 'dbconnect.inc.php';
// echo "include\TEMP_order_item_act.inc.php";

include 'set_urlPage.imc.php';

if (!isset($_POST['addItem'])) {
  header("Location:../index.php");
  exit();
}else {
  // echo "ADD THE ITEM TO THE TEMP TABLE";
  $UID = $_SESSION['UID'];
  $td = date('U');
  $orItemNos = $_POST['orItemNos'];
  $their_item_ref = $_POST['their_item_ref'];
  $tPRID = $_POST['tPRID'];
  $orQTY = $_POST['orQTY'];
  $itemDelDate = $_POST['itemDelDate'];

  $itemDelDate = strtotime($itemDelDate,0);

  if (empty($itemDelDate)) {
    $itemDelDate = ($td + 7862400);
  }

  // echo "<br>Item number : $orItemNos";
  // echo "<br>Their reference : $their_item_ref";
  // echo "<br>Style ref : $tPRID";
  // echo "<br>QTY : $orQTY";
  // echo "<br>Del date : $itemDelDate";

  // Add the data to the TEMP table
  $sql = "INSERT INTO TEMP_order_item_record
            (UID, itemNos, PRID, their_ref, qty, item_del_date)
          VALUES
            (?,?,?,?,?,?)
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-fnoa3';
  }else {
    mysqli_stmt_bind_param($stmt, "ssssss", $UID, $orItemNos, $tPRID, $their_item_ref, $orQTY, $itemDelDate);
    mysqli_stmt_execute($stmt);
  }
  header("Location:../'.$urlPage.'&orn&sal=1");
  exit();
}
