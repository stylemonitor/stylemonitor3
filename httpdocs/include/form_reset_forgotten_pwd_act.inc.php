<?php
include 'dbconnect.inc.php';
// echo "include/form_reset_forgotten_pwd_act.inc.php";

if (!isset($_POST['respwdq'])) {
  // echo "Go to home page";
  header("Location:../index.php");
  exit();
}else {
  // get the information from the form
  $UID = $_POST['UID'];
  $oriFPQID = $_POST['oriFPQID'];
  $revFPQID = $_POST['revFPQID'];
  $revFPQIDa = $_POST['revFPQIDa'];

  $td = date('U');

  // echo "User ID is $UID";
  // echo "<br>Original question ID is $oriFPQID";
  // echo "<br>Revised question ID is $revFPQID";
  // echo "<br>Revised answer is $revFPQIDa";
  // echo "<br>Date is $td";

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to
    // Cancel the original question
    $sql = "UPDATE forgotten_password_user_answers
            SET current = 1
            WHERE UID = ?
            AND FPQID = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-fucl</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $UID, $oriFPQID);
      mysqli_stmt_execute($stmt);
    }

    // check that that question is not still in use
    $sql = "SELECT ID
            FROM forgotten_password_user_answers
            WHERE UID = ?
            AND FPQID = ?
            AND current = 0
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-fucl</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $UID, $revFPQID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $FPQID = $row['ID'];
    }

    if (!empty($FPQID)) {
      echo "That question is already in use";
    }

    // Set the new question
    $sql = "INSERT INTO forgotten_password_user_answers
              (UID, FPQID, ANS, inputtime)
            VALUES (?,?,?,?)
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-fucl</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $UID, $revFPQID, $revFPQIDa, $td);
      mysqli_stmt_execute($stmt);
    }

  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  header("Location:../company.php?C&rpw&u=$UID");
  exit();
}
 ?>
