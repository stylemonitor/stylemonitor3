<?php
session_start();
include 'dbconnect.inc.php';
echo "<br><b>select_country_timezone_dd.inc.php</b>";

$sql = "SELECT cy.ID as CYID
          , tz.ID  as TZID
          , cy.name as CYIDn
          , tz.TIMEZONE as TZIDn
        FROM timezones tz
          , country cy
          , country_timezone ct
        WHERE ct.TZID = tz.ID
        AND ct.CYID = cy.ID
;";
$stmt = mysqli_stmt_init($con);
$res = mysqli_query($con, $sql);
while ($row = mysqli_fetch_array($res)) {
  $TZID = $row['TZID'];
  $CYIDn = $row['CYIDn'];
  $TZIDn = $row['TZIDn'];
  echo '<option value="'.$TZID.'">'.$TZIDn.'</option>';
}
