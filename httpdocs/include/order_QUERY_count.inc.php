<?php
include 'dbconnect.inc.php';
// echo "<br><b>order_QUERY_count.inc.php</b>";

if (isset($_GET['ot'])) {
  $ot = $_GET['ot'];
}

if (empty($ot)) {
  $ot = 1;
}else {
  $ot = $ot;
}

$sql = "SELECT COUNT(o.ID) as OID
        FROM orders o
          , company c
          , associate_companies ac
          , division d
          , order_item oi
          , order_item_del_date oidd
        WHERE o.CPID IN(SELECT cp.ID
            						FROM company_partnerships cp
            						WHERE cp.req_CID = ? OR cp.acc_CID = ?)
        AND oi.itComp = ?
        AND o.OTID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND o.tDID = d.ID
        AND oi.OID = o.ID
        AND oidd.OIID = oi.ID
        AND $dr BETWEEN ? AND ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-or1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssssss", $CID, $CID, $OC, $ot, $sd, $ed);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cTOTAL = $row['OID'];
}
