<?php
include 'include/dbconnect.inc.php';
// echo "include/from_tCID_count_divisions.inc.php";
include 'from_tCID_get_min_division.inc.php';
$CID = $tCID;

$sql = "SELECT COUNT(d.ID) AS cDID
        FROM associate_companies ac
          , division d
        WHERE  ac.ID = ?
        AND d.ACID = ac.ID
        AND d.ID NOT IN (?)
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-ftcd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sS", $tCID, $mtDID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $ctDID = $row['cDID'];
}
