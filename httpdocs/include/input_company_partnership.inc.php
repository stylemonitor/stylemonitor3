<?php
include 'dbconnect.inc.php';
// echo "<br>input_company_partnership.inc.php";
// echo "<br>Partnership CID : $CID";

// NEED to create a Partnership with themselves!
$req_SEL = bin2hex(random_bytes(16));
$req_SEL = password_hash($req_SEL, PASSWORD_DEFAULT);
// echo "<br>Our code (req_SEL) : $req_SEL";
// echo '<br>req_SEL value as : '.$req_SEL;

// prepare a hashed verifying code for the requested (the other member) member
$acc_SEL = bin2hex(random_bytes(16));
$acc_SEL = password_hash($acc_SEL, PASSWORD_DEFAULT);
// echo "<br>Partners code (acc_SEL) : $acc_SEL";

// Add the date to the company_partnership table
$sql = "INSERT INTO company_partnerships
        (insCID, req_CID, req_UID, req_SEL, rel, acc_CID, acc_UID, acc_SEL, active, accdate, inputtime)
        VALUES
        (?,?,?,?,'7',?,?,?,'1',?,?)
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-icp</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssssssss", $CID, $CID, $UID, $req_SEL, $CID, $UID, $acc_SEL, $td, $td);
  mysqli_stmt_execute($stmt);
}
