<?php
include 'dbconnect.inc.php';
// echo "<br><b>include/basic_order_details.inc.php</b>";

if (isset($_GET['orp'])) {
  $OID = $_GET['orp'];
}

$sql = "SELECT distinct o.ID AS Order_ID
-- sql file is Basic_order_details.sql
          , o.OTID AS OTID
           , o.orNos AS OrNos
           , o.our_order_ref AS ourRef
           , o.their_order_ref AS theirRef
           , from_unixtime(o.orDate,'%d-%b-%Y') AS Order_Date
           , from_unixtime(odd.del_Date,'%d-%b-%Y') AS Delivery_Date
           , ac.ID AS tACID
           , ac.name AS tACIDn
           , ac1.ID AS fACID
           , ac1.name AS fACIDn
           , d.ID AS tDID
           , d.name AS tDIDn
           , d.ID AS fDID
           , d1.name AS fDIDn
           , s.ID AS tSID
           , s.name AS tSIDn
           , s1.ID AS fSID
           , s1.name AS fSIDn
        FROM orders o
          INNER JOIN orders_due_dates odd ON o.ID = odd.OID
          INNER JOIN associate_companies ac ON o.tACID = ac.ID
          INNER JOIN associate_companies ac1 ON o.fACID = ac1.ID
          INNER JOIN division d ON o.tDID = d.ID
          INNER JOIN division d1 ON o.fDID = d1.ID
          INNER JOIN section s ON o.tSID = s.ID
          INNER JOIN section s1 ON o.tSID = s1.ID
        WHERE o.ID = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br>FAIL-fnocaT';
}else {
  mysqli_stmt_bind_param($stmt, "s", $OID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $OID = $row['Order_ID'];
  $OTID = $row['OTID'];
  $orNos = $row['OrNos'];
  $ourRef = $row['ourRef'];
  $theirRef = $row['theirRef'];
  $orderDate = $row['Order_Date'];
  // $delDate = $row['delDate'];
  $tACID = $row['tACID'];
  $tACIDn = $row['tACIDn'];
  $fACID = $row['fACID'];
  $fACIDn = $row['fACIDn'];
  $tDID = $row['tDID'];
  $tDIDn = $row['tDIDn'];
  $fDID = $row['fDID'];
  $fDIDn = $row['fDIDn'];
  $tSID = $row['tSID'];
  $tSIDn = $row['tSIDn'];
  $fSID = $row['fSID'];
  $fSIDn = $row['fSIDn'];
  // $tCID = $row['tCID'];
  // $tDID = $row['tDID'];
  // $fCID = $row['fCID'];
  // $fDID = $row['fDID'];
}
