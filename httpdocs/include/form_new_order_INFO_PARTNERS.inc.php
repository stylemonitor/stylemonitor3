<?php
// echo "form_new_order_INFO_PARTNERS.inc.php";

$sql = "SELECT cS.ID AS tCID
       , acS.ID AS tACID -- sup_ACID
-- partners_order_details.sql
       , acS.name AS tACIDn -- sup_ACIDn
       , MIN(dS.ID) AS divisionS
       , MIN(sS.ID) AS sectionS
       , c.ID AS fCID
       , c.name as fCIDn
       , ac.ID AS fACID -- cli_ACID
       , ac.name AS fACIDn -- cli_ACIDn
       , MIN(d.ID) AS divisionC
       , MIN(d.ID) AS divisionCn
       , MIN(s.ID) AS sectionC
       , MIN(s.ID) AS sectionCn
FROM company_partnerships cp
     INNER JOIN company cS ON cp.acc_CID = cS.ID
     INNER JOIN company c ON cp.req_CID = c.ID
     INNER JOIN associate_companies acS ON cS.ID = acS.CID
     INNER JOIN associate_companies ac ON c.ID = ac.CID
     INNER JOIN division dS ON acS.ID = dS.ACID
     INNER JOIN division d ON ac.ID = d.ACID
     INNER JOIN section sS ON dS.ID = sS.DID
     INNER JOIN section s ON d.ID = s.DID
WHERE cp.ID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b><br>FAIL-fnda2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CPID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $tCID = $row['tCID'];
  $tACID = $row['tACID'];
  $tACIDn = $row['tACIDn'];
  $tDID = $row['divisionS'];
  $tSID = $row['sectionS'];
  $fCID = $row['fCID'];
  $fACID = $row['fACID'];
  $fACIDn = $row['fACIDn'];
  $fCIDn = $row['fCIDn'];
  $fDID = $row['divisionC'];
  $fDIDn = $row['divisionCn'];
  $fSID = $row['sectionC'];
  $fSIDn = $row['sectionCn'];
}
