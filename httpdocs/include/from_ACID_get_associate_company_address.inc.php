<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_ACID_get_associate_company_address.inc.php</b>";
if (isset($_GET['id'])) {
  $ACID = $_GET['id'];
}
$sql = "SELECT MIN(a.ID) as AID
          , a.addn as addn
          , a.add1 as add1
          , a.add2 as add2
          , a.city as city
          , a.pcode as pcode
          , a.county as county
          , a.tel as tel
        FROM addresses a
        , associate_companies ac
        , company_associates_address caa
        WHERE ac.ID = ?
        AND caa.ACID = ac.ID
        AND caa.AID = a.ID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-facgca</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $ACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row    = mysqli_fetch_assoc($result);
  $AID    = $row['AID'];
  $addn   = $row['addn'];
  $add1   = $row['add1'];
  $add2   = $row['add2'];
  $city   = $row['city'];
  $pcode  = $row['pcode'];
  $county = $row['county'];
  $tel    = $row['tel'];
}
