<?php
include 'include/dbconnect.inc.php';
echo "<b>include/record_order_summary.inc.php</b>";
if(isset($_GET['oi'])){
  // $OID = $_GET['OID'];
  $OIID = $_GET['oi'];

  // GETS THE TOTALS FOR THE SUMMARY INFORMATION AND DOES THE CALCULATIONS ON THEM
  include 'include/R_on_place_summ.inc.php';
  // include 'include/get_upQTY.inc.php';
  include 'include/R_on_over_sum_MP1.inc.php';
  include 'include/R_on_over_sum_MP2.inc.php';
  include 'include/R_on_over_sum_MP3.inc.php';
  include 'include/R_on_over_sum_MP4.inc.php';
  include 'include/R_on_over_sum_MP5.inc.php';
  include 'include/R_on_over_sum_MP6.inc.php';

  $B1 = ($oiQty - $sMP1);
  $B2 = ($sMP1 - $sMP2);
  $B3 = ($sMP2 - $sMP3);
  $B4 = ($sMP3 - $sMP4);
  $B5 = ($sMP4 - $sMP5);
  $B6 = ($sMP5 - $sMP6);

  if(isset($_GET['rp'])){
    // include 'include/R_on_place_summ.inc.php';
    ?>
    <form class="pprevtab" action="mm_R.php?R&rv&oi=<?php echo $OIID ?>&ot=<?php echo $OTID ?>" method="post">

    <table class="trs"  style="position:relative; top:2%;">
      <caption class="cpsty">Overall progress the order (record_order_summary)</caption>
      <colgroup>
        <col width:28%;>
        <col width:12%;>
        <col width:12%;>
        <col width:12%;>
        <col width:12%;>
        <col width:12%;>
        <col width:12%;>
      </colgroup>
      <tr>
        <td style="width:28%;"></td>
        <th style="width:12%;">Cut</th>
        <th style="width:12%;">On-line</th>
        <th style="width:12%;">Mid-line</th>
        <th style="width:12%;">End-line</th>
        <th style="width:12%;">Final</th>
        <th style="width:12%;">W/house</th>
      </tr>

      <tr>
        <th style="background-color:#f1f1f1;">Between monitoring points</th>
        <!-- BUFFER 1 -->
        <?php
        if ($B1 < 0){
          echo '<td style="background-color:#f4632e;">'.($B1*-1).'</td>';
        }elseif ($sMP1 < $oiQty) {
          echo '<td style="background-color:#ffffba;">'.$B1.'</td>';
        }elseif ($sMP1 == $oiQty) {
          echo '<td style="background-color:#a6ffa6;">'.$B1.'</td>';
        }elseif ($sMP1 > $oiQty) {
          echo '<td style="background-color:#ffa2a2;">'.$B1.'</td>';
        }elseif ($B1 > 0) {
          echo '<td style="background-color:#ffa2a2;">'.$B1.'</td>';
          // code...
        } ?>

        <!-- BUFFER 2 -->
        <?php
        if ($B2 < 0){
          echo '<td style="background-color:#f4632e;">'.($B2*-1).'</td>';
        }elseif ($sMP2 < $oiQty){
          echo '<td style="background-color:#ffffba;">'.$B2.'</td>';
        }elseif ($sMP2 == $oiQty){
          echo '<td style="background-color:#a6ffa6;">'.$B2.'</td>';
        }elseif ($sMP2 > $oiQty){
          echo '<td style="background-color:#ffa2a2;">'.$B2.'</td>';
        }elseif ($B2 > 0) {
          echo '<td style="background-color:#ffa2a2;">'.$B2.'</td>';
          // code...
        } ?>

        <!-- BUFFER 3 -->
        <?php
        if ($B3 < 0){
          echo '<td style="background-color:#f4632e;">'.($B3*-1).'</td>';
        }elseif ($sMP3 < $oiQty){
          echo '<td style="background-color:#ffffba;">'.$B3.'</td>';
        }elseif ($sMP3 == $oiQty){
          echo '<td style="background-color:#a6ffa6;">'.$B3.'</td>';
        }elseif ($sMP3 > $oiQty){
          echo '<td style="background-color:#ffa2a2;">'.$B3.'</td>';
        }elseif ($B3 > 0) {
          echo '<td style="background-color:#ffa2a2;">'.$B3.'</td>';
          // code...
        } ?>

        <!-- BUFFER 4 -->
        <?php
        if ($B4 < 0){
          echo '<td style="background-color:#f4632e;">'.($B4*-1).'</td>';
        }elseif ($sMP4 < $oiQty){
          echo '<td style="background-color:#ffffba;">'.$B4.'</td>';
        }elseif ($sMP4 == $oiQty){
          echo '<td style="background-color:#a6ffa6;">'.$B4.'</td>';
        }elseif ($sMP4 > $oiQty){
          echo '<td style="background-color:#ffa2a2;">'.$B4.'</td>';
        }elseif ($B4 > 0) {
          echo '<td style="background-color:#ffa2a2;">'.$B4.'</td>';
          // code...
        } ?>

        <!-- BUFFER 5 -->
        <?php
        if ($B5 < 0){
          echo '<td style="background-color:#f4632e;">'.($B5*-1).'</td>';
        }elseif ($sMP5 < $oiQty){
          echo '<td style="background-color:#ffffba;">'.$B5.'</td>';
        }elseif ($sMP5 == $oiQty){
          echo '<td style="background-color:#a6ffa6;">'.$B5.'</td>';
        }elseif ($sMP5 > $oiQty){
          echo '<td style="background-color:#ffa2a2;">'.$B5.'</td>';
        }elseif ($B5 > 0) {
          echo '<td style="background-color:#ffa2a2;">'.$B5.'</td>';
          // code...
        } ?>

        <!-- BUFFER 6 -->
        <?php
        if ($B6 < 0){
          echo '<td style="background-color:#f4632e;">'.($B6*-1).'</td>';
        }elseif ($sMP6 < $oiQty){
          echo '<td style="background-color:#ffffba;">'.$B6.'</td>';
        }elseif ($sMP6 == $oiQty){
          echo '<td style="background-color:#a6ffa6;">'.$B6.'</td>';
        }elseif ($sMP6 > $oiQty){
          echo '<td style="background-color:#ffa2a2;">'.$B6.'</td>';
        }elseif ($B6 > 0) {
          echo '<td style="background-color:#ffa2a2;">'.$B6.'</td>';
          // code...
        }elseif ($B6 > 0) {
          echo '<td style="background-color:#f53e3e;">'.$sMP6.'</td>';
        }  ?>

      </tr>
      <tr>
        <th style="background-color:#f1f1f1;">Past monitoring point</th>
        <!-- MP1 -->
        <?php
        if ($sMP1 < $oiQty){
          echo '<td style="background-color:#ffffba;">'.$sMP1.'</td>';
        }elseif ($sMP1 == $oiQty){
          echo '<td style="background-color:#a6ffa6;">'.$sMP1.'</td>';
        }elseif ($sMP1 > $oiQty){
          echo '<td style="background-color:#ffa2a2;">'.$sMP1.'</td>';
        } ?>
        <!-- MP2 -->
        <?php
        if ($sMP2 < $oiQty){
          echo '<td style="background-color:#ffffba;">'.$sMP2.'</td>';
        }elseif ($sMP2 == $oiQty){
          echo '<td style="background-color:#a6ffa6;">'.$sMP2.'</td>';
        }elseif ($sMP2 > $oiQty){
          echo '<td style="background-color:#ffa2a2;">'.$sMP2.'</td>';
        } ?>
        <!-- MP3 -->
        <?php
        if ($sMP3 < $oiQty){
          echo '<td style="background-color:#ffffba;">'.$sMP3.'</td>';
        }elseif ($sMP3 == $oiQty){
          echo '<td style="background-color:#a6ffa6;">'.$sMP3.'</td>';
        }elseif ($sMP3 > $oiQty){
          echo '<td style="background-color:#ffa2a2;">'.$sMP3.'</td>';
        } ?>
        <!-- MP4 -->
        <?php
        if ($sMP4 < $oiQty){
          echo '<td style="background-color:#ffffba;">'.$sMP4.'</td>';
        }elseif ($sMP4 == $oiQty){
          echo '<td style="background-color:#a6ffa6;">'.$sMP4.'</td>';
        }elseif ($sMP4 > $oiQty){
          echo '<td style="background-color:#ffa2a2;">'.$sMP4.'</td>';
        } ?>
        <!-- MP5 -->
        <?php
        if ($sMP5 < $oiQty){
          echo '<td style="background-color:#ffffba;">'.$sMP5.'</td>';
        }elseif ($sMP5 == $oiQty){
          echo '<td style="background-color:#a6ffa6;">'.$sMP5.'</td>';
        }elseif ($sMP5 > $oiQty){
          echo '<td style="background-color:#ffa2a2;">'.$sMP5.'</td>';
        } ?>
        <!-- MP6 -->
        <?php
        if ($sMP6 < $oiQty){
          echo '<td style="background-color:#ffffba;">'.$sMP6.'</td>';
        }elseif ($sMP6 == $oiQty){
          echo '<td style="background-color:#a6ffa6;">'.$sMP6.'</td>';
        }elseif ($sMP6 > $oiQty){
          echo '<td style="background-color:#ffa2a2;">'.$sMP6.'</td>';
        }elseif ($sMP6 > 0) {
          echo '<td style="background-color:#f53e3e;">'.$sMP6.'</td>';
        } ?>
      </tr>
    </table>
    <div class="formbtn" style="top:90%; height:5%;">
      <button class="sub_btn" type="submit" name="close">Close</button>
    </div>
  </form>
  <?php }
} ?>
