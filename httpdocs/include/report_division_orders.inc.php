<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_division_orders.inc.php";

include 'set_urlPage.inc.php';

if (isset($_GET['i'])) {
  $DID = $_GET['i'];
}

$sql = "SELECT COUNT(o.ID) as OID
        FROM orders o
        	, order_item oi
          , order_item_qty oiq
          , order_item_del_date oidd
          , prod_ref_to_div prd
          , prod_ref pr
          , division d
        WHERE d.ID = ?
        AND prd.DID = d.ID
        AND prd.PRID = pr.ID
        AND oi.PRID = pr.ID
        AND oi.OID = o.ID
        AND oiq.OIID = oi.ID
        AND oidd.OIID = oi.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rdo</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $DID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOID = $row['OID'];
}

// set the return pages section
if (isset($_GET['sde'])) {$head = 'sde'; $rpp = 5;
}

// Check the URL to see what page we are on
if (isset($_GET['pa'])) { $pa = $_GET['pa'];
}else{ $pa = 0;
}

// Set the rows per page
// $rpp = 30;

// Check which page we are on
if ($pa > 1) { $start = ($pa * $rpp) - $rpp;
}else { $start = 0;
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cOID / $rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV);
?>

<table class="trs" style="position:absolute; top:13%; left:0%; width:100%;">
  <tr>
    <th style="width:4%; text-align:right;">O/r</th>
    <th style="width:28%;">Associate/Partner</th>
    <th style="width:28%;">OUR Order Ref</th>
    <th style="width:4%;">Qty</th>
    <th style="width:4%;">1</th>
    <th style="width:4%;">2</th>
    <th style="width:4%;">3</th>
    <th style="width:4%;">4</th>
    <th style="width:4%;">5</th>
    <th style="width:4%;">W/house</th>
    <th style="width:4%;">Sent</th>
    <th style="width:8%;">Date due</th>
  </tr>
  <?php
  $sql = "SELECT o.ID as OID
	          , o.orNos as OIDn
            , o.our_order_ref as OIDref
            , oi.ord_item_nos as OIIDn
            , oiq.order_qty as OQTY
            , oidd.item_del_date as ODD
          FROM orders o
          	, order_item oi
            , order_item_qty oiq
            , order_item_del_date oidd
            , prod_ref pr
            , prod_ref_to_div prd
            , division d
          WHERE d.ID = ?
          AND prd.DID = d.ID
          AND prd.PRID = pr.ID
          AND oi.PRID = pr.ID
          AND oi.OID = o.ID
          AND oiq.OIID = oi.ID
          AND oidd.OIID = oi.ID
          LIMIT $start, $rpp
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-rdo2</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $DID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      $OID = $row['OID'];
      $OIDn = $row['OIDn'];
      $OIDref = $row['OIDref'];
      $OIIDn = $row['OIIDn'];
      $OQTY = $row['OQTY'];
      $ODD = $row['ODD'];

      $ODD = date('d-M-Y', $ODD);
      ?>
      <tr>
        <td style="width:4%; color:red; text-align: right; padding-right: 1%;"><a style="text-decoration:none; " href="<?php echo $urlPage ?>&orb&oi=<?php echo $OID;?>"><?php echo $OIDn.':'.$OIIDn;?></a></td>
        <td style="width:28%; text-align:left; text-indent:2%; border-right:thin solid grey;"><?php echo "Client name" ?></td>
        <td style="width:28%; text-align:left; text-indent:2%; border-right:thin solid grey;"><?php echo $OIDref ?></td>
        <td style="width:4%; border-right:thin solid grey;"><?php echo $OQTY ?></td>
        <td style="width:4%; border-right:thin solid grey;"><?php echo $#1 ?></td>
        <td style="width:4%; border-right:thin solid grey;"><?php echo $#2 ?></td>
        <td style="width:4%; border-right:thin solid grey;"><?php echo $#3 ?></td>
        <td style="width:4%; border-right:thin solid grey;"><?php echo $#4 ?></td>
        <td style="width:4%; border-right:thin solid grey;"><?php echo $#5 ?></td>
        <td style="width:4%; border-right:thin solid grey;"><?php echo $whouse ?></td>
        <td style="width:4%; border-right:thin solid grey;"><?php echo $desp ?></td>
        <td style="width:8%;"><?php echo $ODD ?></td>
      </tr>
      <?php
    }
  }
  ?>
</table>
<?php
if (isset($_GET['sde'])) {
  ?>
  <div style="position:absolute; top:70%; right:5%;">555
    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a style='font-size:200%; text-decoration:none;' href='?S&$head&pa=$x&s=$PRID'>  $x  </a>";
    }
    ?>
  </div>
  <?php
}else {
  ?>
  <div style="position:absolute; top:70%; right:5%;">555
    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a style='font-size:200%; text-decoration:none;' href='?S&$head&pa=$x&s=$PRID'>  $x  </a>";
    }
    ?>
  </div>
  <?php
}
