<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_SM_problems.inc.php";
if (isset($_GET['prb'])) {
  $SPID = $_GET['prb'];
}

include 'from_SMPID_get_details.inc.php';
$date_rec = date('d-M-Y',$date_rec);

if ($fxtd <> 0) {
  $bc = '';
}else {
  $bc = 'background-color: red;';
}
?>
<div style="position:absolute; top:0%; left:0%; width:100%; font-weight:bold; text-align:left; text-indent:1%; <?php echo $bc ?>">
  <?php echo $problem ?>
</div>
<div style="position:absolute; top:4%; left:3%;">
  <a style="text-decoration:none;" href="<?php echo $file ?>"><?php echo $file ?></a>

</div>
<div style="position:absolute; top:4%; left:57%;">
  <?php echo $date_rec ?>
</div>
<div style="position:absolute; top:4%; left:77%;">
  <?php echo "$firstname $surname"?>
</div>

<table class="trs" style="position:absolute; top:7%; left:0%; width:100%;">
  <tr>
    <th style="width:70%; text-align:left; text-indent:2%;">Comment</th>
    <th style="width:20%; text-align:left; text-indent:20%;">by</th>
    <th style="width:10%; text-align:center;">Date</th>
  </tr>
  <?php

  $sql = "SELECT spn.ID as SPNID
            , spn.note as SPNIDn
            , spn.UID as SPNIDu
            , spn.inputtime as SPNIDu
            , u.firstname as UIDf
            , u.surname as UIDs
          FROM SM_problems sp
            , SM_problem_notes spn
            , users u
          WHERE sp.id = ?
          AND spn.SMPID = sp.id
          AND spn.UID = u.ID
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-rsp</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $SPID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      $SPNID = $row['SPNID'];
      $note = $row['SPNIDn'];
      $UIDf = $row['UIDf'];
      $UIDs = $row['UIDs'];
      $time = $row['SPNIDu'];

      $time = date('d-M-Y',$time)
      ?>
      <tr>
        <td style="width:70%; text-align:left; text-indent:2%;"><?php echo $note ?></td>
        <td style="width:20%; text-align:left; text-indent:2%;"><?php echo "$UIDf $UIDs" ?></td>
        <td style="width:10%; text-align:left; text-indent:2%;"><?php echo $time ?></td>
      </tr>
    <?php
    }
  } ?>
</table>
