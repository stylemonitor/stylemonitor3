<?php
include 'dbconnect.inc.php';
// echo "from_PRID_get_order_records.inc.php";

$CID = $_SESSION['CID'];

if (isset($_GET['s'])) {
  $PRID = $_GET['s'];
}

$sql = "SELECT distinct
-- SQL is order_report_style_counts4.sql
       o.fCID
       , o.tCID
       , OPEN_ORDERS.prod_ref
       , OPEN_ORDERS.ORDERS
       , OPEN_ORDERS.ITEMS AS OPEN_ORDERS
       , count(distinct o.ID) - OPEN_ORDERS.ORDERS AS CLOSED_ORDERS
       , count(distinct o.ID) AS TOT_ORDERS
       , concat(round(((count(distinct o.ID) - OPEN_ORDERS.ORDERS)/count(o.ID)) * 100,0),'%') AS PER_OCOMP
       , OPEN_ORDERS.ITEMS AS OPEN_ITEMS
       , sum(oiq.order_qty) - OPEN_ORDERS.ITEMS AS CLOSED_ITEMS
       , sum(oiq.order_qty) AS TOTAL_ITEMS
       , concat(round(((sum(oiq.order_qty) - OPEN_ORDERS.ITEMS)/sum(oiq.order_qty)) * 100,0),'%') AS PER_ICOMP
FROM
(
SELECT distinct
       oi.id AS OIID
       , oi.OID AS OID
       , oi.PRID AS PRID
       , pr.prod_ref AS prod_ref
       , o.fCID
       , o.tCID
       , count(distinct o.ID) AS ORDERS
       , sum(distinct oiq.order_qty) AS ITEMS
FROM order_item oi
    INNER JOIN orders o ON oi.OID = o.ID
    INNER JOIN prod_ref pr on oi.PRID = pr.ID
    INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
WHERE pr.ID = ?
  AND o.orComp = 0 -- DO NOT CHANGE THIS VALUE
  AND (o.fCID = ? OR o.tCID = ?)
GROUP BY pr.ID
) OPEN_ORDERS INNER JOIN prod_ref pr ON OPEN_ORDERS.PRID = pr.ID
              INNER JOIN order_item oi ON oi.PRID = pr.ID
              INNER JOIN orders o ON oi.OID = o.ID
              INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
GROUP BY pr.ID;
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fPRgor</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $PRID, $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $cOIDo = $row['OPEN_ORDERS'];
    $cOIDc = $row['CLOSED_ORDERS'];
    $cOIDt = $row['TOT_ORDERS'];
    $cOIIDo = $row['OPEN_ITEMS'];
    $cOIIDc = $row['CLOSED_ITEMS'];
    $cOIIDt = $row['TOTAL_ITEMS'];



    // $cOIQIDo = $row['ITEMS_TO_COMPLETE'];
    // $cOIQIDc = $row['COMPLETED_ITEMS'];
    // $cOIQIDt = $row['TOTAL_ITEMS'];
    // $cACID = $row['NO_OF_CUSTOMERS'];
  }
}
