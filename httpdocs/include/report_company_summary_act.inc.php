<?php
session_start();
include 'dbconnect.inc.php';
$UID = $_SESSION['UID'];
// echo "<b>include/report_company_summary_act.inc.php</b>";

if (!isset($_POST['add_div']) && !isset($_POST['add_sec']) && !isset($_POST['add_typ']) && !isset($_POST['add_sty']) && !isset($_POST['add_sea']) && !isset($_POST['add_col']) && !isset($_POST['add_client']) && !isset($_POST['add_supplier']) && !isset($_POST['add_partner_client']) && !isset($_POST['add_partner_supplier'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['add_client'])) {
  // echo "<br>Return to the Users page";
  header("Location:../associate.php?A&adq&c");
  exit();
}elseif (isset($_POST['add_supplier'])) {
  // echo "<br>Return to the Users page";
  header("Location:../associate.php?A&adq&s");
  exit();
}elseif (isset($_POST['add_partner_client'])) {
  // echo "<br>Return to the Users page";
  header("Location:../partner.php?P&pan");
  exit();
}elseif (isset($_POST['add_partner_supplier'])) {
  // echo "<br>Return to the Users page";
  header("Location:../partner.php?P&pan");
  exit();
}elseif (isset($_POST['add_div'])) {
  // echo "<br>Return to the Users page";
  header("Location:../company.php?C&sdn");
  exit();
}elseif (isset($_POST['add_sec'])) {
  echo "<br>Need to create this page";
  // header("Location:../styles.php?S&spt");
  // exit();
}elseif (isset($_POST['add_typ'])) {
  // echo "<br>Return to the Users page";
  header("Location:../styles.php?S&spt");
  exit();
}elseif (isset($_POST['add_sty'])) {
  header("Location:../styles.php?S&snn");
  exit();
}elseif (isset($_POST['add_sea'])) {
  header("Location:../styles.php?S&sea");
  exit();
}elseif (isset($_POST['add_col'])) {
  header("Location:../styles.php?S&col");
  exit();
}
