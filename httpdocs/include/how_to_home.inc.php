<?php
include 'dbconnect.inc.php';
$sm = '<b>S</b>tyle<b>M</b>onitor';

?>
<div class="cpsty" style="top:0%; height:3%; left:0%; width:100%; background-color:#e08df4;">
  <p>How to ... HOME</p>
</div>
<div class="" style="top:3%; height:97%; left:0%; width:100%; background-color:#f4f4f4;">
  <br>
  <p>You don't actually 'do' anything here.</p>
  <br>
  <p>It just shows you your orders.</p>
  <br>
  <p>Each order is sorted by when it is due, and the index tab will only be there <b>IF</b> there is something to show</p>
  <br>
  <p>Within the report the orders are initially sorted by their due by date, so the most urgent are at the top and the least urgent at the bottom</p>
  <br>
  <p>When the pointy arrow changes to a pointing finger, you can click on it and, depending where you are, it will take you to what you want to see.</p>
  <br>
  <p>So, if you click on an order number, it will show you that order in more detail.</p>
  <br>
  <p>If you click on a Clients name, it will show you more about that client, how many orders they have etc.</p>
  <br>
  <p>Try it and see.</p>
  <br>
  <p>This change of cursor rule applies throughout <?php echo $sm ?>.</p>

</div>
