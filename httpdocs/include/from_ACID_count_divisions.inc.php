<?php
include 'include/dbconnect.inc.php';
// echo "include/from_CID_count_divisions.inc.php";
include 'from_CID_get_min_division.inc.php';
$CID = $_SESSION['CID'];

if (isset($_GET['ac'])) { $ACID = $_GET['ac'];}else { $ACID = $_SESSION['CID'];}

$sql = "SELECT COUNT(d.ID) AS DID
        FROM associate_companies ac
          , division d
        WHERE ac.ID = (SELECT MIN(ac.ID) as mACID
								        FROM associate_companies ac
								          , company c
								        WHERE c.ID = ?
								        AND ac.CID = c.ID)
        AND d.ACID = ac.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-facd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $ACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cDID = $row['DID'];
  // reduced count by 1 as SELECT DIVISION is NOT a division
  // Only have divisions after the first one I give them!!!!!!
  $cDID = $cDID - 1;
}
