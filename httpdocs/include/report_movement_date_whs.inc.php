<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_movement_date_awt.inc.php";

include 'set_urlPage.inc.php';

if (isset($_GET['rep'])) {
  $movDate = $_GET['rep'];
}

$CID = $_SESSION['CID'];

include 'from_movement_count_entries_whs.inc.php';

// Set the rows per page
$rpp = 25;

// Check the URL to see what page we are on
if (isset($_GET['pa'])) {
  $pa = $_GET['pa'];
}else{
  $pa = 0;
}

// Check which page we are on
if ($pa > 1) {
  $start = ($pa * $rpp) - $rpp;
}else {
  $start = 0;
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 1.9 is also 2
$PTV = ($cOPMID / $rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;

$MD = strtotime($movDate,0);
$MD = date('d M Y', $MD);
$td = date('d M Y',$td);

$pddh = 'Movement History : Warehouse';
$pddhbc = '#fffddd';
include 'page_description_date_header.inc.php';
include "page_selection_header.inc.php";
?>

<table class="trs" style="position:absolute; top:15%; left:0%; width:100%;">
  <tr>
    <th style="width:1%;"></th>
    <th style="width:5%;">Order</th>
    <th style="width:3%;">Item</th>
    <th style="width:16%;">Client</th>
    <th style="width:10%;">Short Code</th>
    <th style="width:20%;">Comments</th>
    <th style="width:4%;">Qty</th>
    <th style="width:4%;"><a style="color:black; text-decoration:none;" href="<?php echo $urlPage ?>&tab=5&rep=<?php echo $movDate ?>">#5</a></th>
    <th style="width:4%;"><a style="color:black; text-decoration:none;" href="<?php echo $urlPage ?>&tab=6&rep=<?php echo $movDate ?>">Whs</a></th>
    <th style="width:4%;">Sent</th>
    <th style="width:9%;">User</th>
  </tr>
<?php

// GET THE DATA
$sql = "SELECT *
FROM
(
SELECT DISTINCT
-- WHOUSE_Movement_report.sql
     o.fCID AS cliCID
     , Mvemnt.OPMID AS OPMID
     , c.name AS cliCIDn
     , o.fACID AS cli_ACID
     , ac.name AS cli_ACIDn
     , o.fDID AS cli_DID
     , d.name AS cli_DIDn
     , o.tCID AS supCID
     , cS.name AS supCIDn
     , o.tACID AS sup_ACID
     , acS.name AS sup_ACIDn
     , o.tDID AS sup_DID
     , d.name AS sup_DIDn
     , Mvemnt.ORDER_ID AS OID
     , Mvemnt.O_ITEM_ID AS OIID
     , Mvemnt.OIMR_ID AS OIMR_ID
     , Mvemnt.Reason AS Reason
     , LPAD(o.orNos,6,0) AS ordNos
     , LPAD(Mvemnt.ord_item_nos,3,0) AS ordItemNos
     , pr.ID AS PRID
     , pr.prod_ref AS Short_Code
     , o.OTID AS OTID
     , Mvemnt.samProd AS samProd
     , o.our_order_ref AS ourRef
-- CALCULATE INDIVIDUAL GARMENT MOVEMENT VALUES
     , SUM(Mvemnt.REJ_BY_WHOUSE + Mvemnt.COR_OUT_WHOUSE - Mvemnt.INTO_WHOUSE - Mvemnt.COR_IN_WHOUSE) AS Stage5
     , SUM(Mvemnt.INTO_WHOUSE + Mvemnt.FROM_CLIENT + Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.COR_IN_WHOUSE + Mvemnt.CORR_OUT_CLIENT + Mvemnt.CORR_OUT_SUPPLIER
           - Mvemnt.REJ_BY_WHOUSE - Mvemnt.TO_CLIENT - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.COR_OUT_WHOUSE - Mvemnt.CORR_IN_CLIENT - Mvemnt.CORR_IN_SUPPLIER) AS Warehouse
     , SUM(Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT) AS CLIENT
     , SUM(Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER) AS SUPPLIER
     , Mvemnt.comments AS Comments
     , Mvemnt.uInitials AS uInitials
     , Mvemnt.User AS User
     , substr(Mvemnt.Date_Time,12,7) AS Time
     , substr(Mvemnt.Date_Time,1,10) AS Date_only
FROM (
    SELECT DISTINCT oimr.ID AS OIMR_ID
         , oimr.reason AS Reason
         , o.ID AS ORDER_ID
         , oi.ID AS O_ITEM_ID
         , oi.PRID AS PRID
         , opm.ID AS OPMID
         , oi.ord_item_nos AS ord_item_nos
         , IF(oi.samProd = 0,1,0) AS samProd
         , IF((CASE WHEN oimr.ID = 8 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 8 THEN SUM(opm.omQty) END)) AS INTO_WHOUSE
         , IF((CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END)) AS REJ_BY_WHOUSE
         , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
         , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
         , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
         , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
         , IF((CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END)) AS COR_IN_WHOUSE
         , IF((CASE WHEN oimr.ID = 33 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 33 THEN SUM(opm.omQty) END)) AS COR_OUT_STG5
         , IF((CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END)) AS COR_OUT_WHOUSE
         , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
         , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
         , IF((CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END)) AS CORR_OUT_SUPPLIER
         , IF((CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END)) AS CORR_IN_SUPPLIER
         , opm.type AS comments
         , concat(u.firstname,' ',u.surname) AS User
         , u.userInitial AS uInitials
         , from_unixtime(opm.inputtime,'%Y-%m-%d (%H:%i)') AS Date_Time
    FROM order_placed_move opm
         INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
         INNER JOIN order_placed op ON opm.OPID = op.ID
         INNER JOIN order_item oi ON op.OIID = oi.ID
         INNER JOIN orders o ON oi.OID = o.ID
         INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
         INNER JOIN users u ON o.UID = u.ID
    GROUP BY opm.ID, oi.ID, oimr.ID
   ) Mvemnt INNER JOIN order_item oi ON Mvemnt.O_ITEM_ID = oi.ID
            INNER JOIN orders o ON Mvemnt.ORDER_ID = o.ID
            INNER JOIN order_item_del_date oidd ON Mvemnt.O_ITEM_ID = oidd.OIID
            INNER JOIN prod_ref pr ON Mvemnt.PRID = pr.ID
            INNER JOIN orders_due_dates odd ON odd.OID = o.ID
            INNER JOIN company c ON o.fCID = c.ID
            INNER JOIN company cS ON o.tCID = cS.ID
            INNER JOIN associate_companies ac ON o.fACID = ac.ID
            INNER JOIN associate_companies acS ON o.tACID = acS.ID
            INNER JOIN division d ON o.fDID = d.ID
            INNER JOIN division dS ON o.tDID = dS.ID
WHERE (o.fCID = ? OR o.tCID = ?)
  AND substr(Mvemnt.Date_Time,1,10) = ?
GROUP BY Mvemnt.OPMID
ORDER BY Mvemnt.OPMID DESC
) TOTALS
WHERE TOTALS.Warehouse !=0
LIMIT ?,?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL - fsmda</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssss", $CID, $CID, $movDate, $start, $rpp);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $cliCID = $row['cliCID'];
    $cliCIDn = $row['cliCIDn'];
    $cli_ACID = $row['cli_ACID'];
    $cli_ACIDn = $row['cli_ACIDn'];
    $cli_DID = $row['cli_DID'];
    $cli_DIDn = $row['cli_DIDn'];

    $supCID = $row['supCID'];
    $supCIDn = $row['supCIDn'];
    $sup_ACID = $row['sup_ACID'];
    $sup_ACIDn = $row['sup_ACIDn'];
    $sup_DID = $row['sup_DID'];
    $sup_DIDn = $row['sup_DIDn'];

    $OID = $row['OID'];
    $OIID = $row['OIID'];
    $ordNos = $row['ordNos'];
    $ordItemNos = $row['ordItemNos'];
    $OTID = $row['OTID'];
    $samProd = $row['samProd'];
    $OIMRID = $row['OIMR_ID'];

    $ourRef = $row['ourRef'];
    $Item_Status = $row['Item_Status'];
    $Short_Code = $row['Short_Code'];

    $Awaiting = $row['Awaiting'];
    $Stage1 = $row['Stage1'];
    $Stage2 = $row['Stage2'];
    $Stage3 = $row['Stage3'];
    $Stage4 = $row['Stage4'];
    $Stage5 = $row['Stage5'];
    $Warehouse = $row['Warehouse'];
    $SUPPLIER = $row['SUPPLIER'];

    $Comments = $row['Comments'];
    $User = $row['User'];
    $Time = $row['Time'];

    $UIDi = $row['uInitials'];
    $OIQIDq = $row['OIQIDq'];

    if ($OTID == 1) {
      $c = 'black';
      $bc = 'f7cc6c';
    }elseif ($OTID == 2) {
      $c = 'black';
      $bc = 'bbbbff';
    }elseif ($OTID == 3) {
      $c = 'white';
      $bc = 'd68e79';
    }elseif ($OTID == 4) {
      $c = 'white';
      $bc = 'c9d1ce';
    }

    if ($samProd == 0) {
      $spc ='#76f8bc';
      $spbc = '#76f8bc';
    }else {
      $spc = '#fa7b7b';
      $spbc = '#fa7b7b';
    }

    // ADDING colour to the quantities
    if ($Awaiting < 0) { $colA = 'red'; $weiA = 'bold';}else { $colA = 'green'; $weiA = 'bold'; }
    if ($Stage1 < 0) { $col1 = 'red'; $wei1 = 'bold';}else { $col1 = 'green'; $wei1 = 'bold'; }
    if ($Stage2 < 0) { $col2 = 'red'; $wei2 = 'bold';}else { $col2 = 'green'; $wei2 = 'bold'; }
    if ($Stage3 < 0) { $col3 = 'red'; $wei3 = 'bold';}else { $col3 = 'green'; $wei3 = 'bold'; }
    if ($Stage4 < 0) { $col4 = 'red'; $wei4 = 'bold';}else { $col4 = 'green'; $wei4 = 'bold'; }
    if ($Stage5 < 0) { $col5 = 'red'; $wei5 = 'bold';}else { $col5 = 'green'; $wei5 = 'bold'; }
    if ($Warehouse < 0) { $colW = 'red'; $weiW = 'bold';}else { $colW = 'green'; $weiW = 'bold'; }

    // ADDING colour to the reason
    if (($OIMRID == 13) || ($OIMRID == 14) || ($OIMRID == 15) || ($OIMRID == 16) || ($OIMRID == 17) || ($OIMRID == 18)) {
      $resbc = '#ff9494';
    }elseif (($OIMRID == 23) || ($OIMRID == 24) || ($OIMRID == 25) || ($OIMRID == 26) || ($OIMRID == 27) || ($OIMRID == 28) || ($OIMRID == 29) || ($OIMRID == 30) || ($OIMRID == 31) || ($OIMRID == 32) || ($OIMRID == 33) || ($OIMRID == 34)) {
      $resbc = '#ebf0aa';
    }else {
      $resbc = '#aaf0aa';
    }
?>

  <tr>
    <td style="background-color:<?php echo $spbc ?>;"></td>
    <td style="border-right:thin solid grey;"><a style="color:black; text-decoration:none;" href="<?php echo $urlPage ?>&o=<?php echo $OID ?>"><?php echo $ordNos ?></a></td>
    <td style="border-right:thin solid grey;"><a style="color:black; text-decoration:none;" href="<?php echo $urlPage ?>&oi=<?php echo $OIID ?>"><?php echo $ordItemNos ?></a></td>
    <?php
    if (strlen($cli_ACIDn)>19) {
      $cli_ACIDn = substr($cli_ACIDn,0,19);
      ?>
      <td style="font-size: 100%;text-align:left; text-indent:1%;background-color:#ffffd1;  border-right: thin solid grey; color: <?php echo $c ?>;background-color: #<?php echo $bc ?>;"><a style="text-decoration:none;" href="associate.php?A&tab=<?php echo $OTID ?>&id=<?php echo $sup_ACID ?>"><?php echo $cli_ACIDn ?> ...</a></td>
      <!-- <td style="font-size: 100%;text-align:left; text-indent:1%;background-color:#ffffd1;  border-right: thin solid grey; color: <?php echo $c ?>;background-color: #<?php echo $bc ?>;"><a style="text-decoration:none;" href="associate.php?A&asc&id=<?php echo $sup_ACID ?>"><?php echo $cli_ACIDn ?> ...</a></td> -->
      <?php
    }else {?>
      <td style="font-size: 100%;text-align:left; text-indent:1%;background-color:#ffffd1;  border-right: thin solid grey; color: <?php echo $c ?>;background-color: #<?php echo $bc ?>;"><a style="text-decoration:none;" href="associate.php?A&tab=<?php echo $OTID ?>&id=<?php echo $sup_ACID ?>"><?php echo $cli_ACIDn ?></a></td>
    <?php
    } ?>
      <td style="border-right:thin solid grey; text-align:left;"><a style="color:black; text-decoration:none;" href="<?php echo $urlPage ?>?rep&s=<?php echo $PRID ?>"><?php echo $Short_Code ?></a></td>
    <?php
    if (strlen($Comments) < 22) {
      ?>
      <td style="text-align:left; text-indent:1%; background-color:<?php echo $resbc ?>; border-right:thin solid grey;"><?php echo "$Comments" ?></td>
      <?php
    }else {
      $CommentsS = substr($Comments,0,22);
      ?>
      <td style="text-align:left; text-indent:1%; background-color:<?php echo $resbc ?>; border-right:thin solid grey;">
        <span class="h1"> <?php echo "$CommentsS" ?> ...</span>
        <!-- <div>
          <?php echo $Comments ?>
        </div> -->
      </td>
      <?php
    }
    ?>
    <td style="border-right:thin solid grey; text-align:right; padding-right:0.5%;"><?php echo $OIQIDq ?></td>
    <?php
    if ($Stage5 == 0 ) {
      ?>
      <td style="color:<?php echo $col1 ?>; font-weight:<?php echo $wei1 ?>; border-right:thin solid grey; text-align:right; padding-right:0.5%;"></td>
      <?php
    }else {
      ?>
      <td style="color:<?php echo $col1 ?>; font-weight:<?php echo $wei1 ?>; border-right:thin solid grey; text-align:right; padding-right:0.5%;"><?php echo $Stage5 ?></td>
      <?php
    }
    ?>
    <?php
    if ($Warehouse == 0 ) {
      ?>
      <td style="color:<?php echo $col1 ?>; font-weight:<?php echo $wei1 ?>; border-right:thin solid grey; text-align:right; padding-right:0.5%;"></td>
      <?php
    }else {
      ?>
      <td style="color:<?php echo $col1 ?>; font-weight:<?php echo $wei1 ?>; border-right:thin solid grey; text-align:right; padding-right:0.5%;"><?php echo $Warehouse ?></td>
      <?php
    }?>
    <?php
    if ($Sent == 0 ) {
      ?>
      <td style="color:<?php echo $col1 ?>; font-weight:<?php echo $wei1 ?>; border-right:thin solid grey; text-align:right; padding-right:0.5%;"></td>
      <?php
    }else {
      ?>
      <td style="color:<?php echo $col1 ?>; font-weight:<?php echo $wei1 ?>; border-right:thin solid grey; text-align:right; padding-right:0.5%;"><?php echo $Sent ?></td>
      <?php
    }
    ?>
    <td style="text-align:right; padding-right:0.5%;"><?php echo "$UIDi $Time" ?></td>
  </tr>
  <?php
} ?>
</table>
<?php
}
?>
<div style="position:absolute; bottom:11%; right:5%; color:black; font-size:200%; z-index:1;">
  <?php for ($x = 1 ; $x <= $PTV ; $x++){
    ?>
    <a style="text-decoration:none;" href='<?php echo $urlPage ?>&rep=<?php echo $movDate ?>&pa=<?php echo $x ?>'><?php echo $x ?></a>
    <?php
  } ?>
</div>
