<?php
session_start();
include 'dbconnect.inc.php';
// echo "<br><b>form_new_partner_act.inc.php</b>";

// check that the form was entered from P_re.inc.php
if (!isset($_POST['req_part']) && !isset($_POST['can_req_part'])) {
  // echo "Resubmit the request";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['can_req_part'])) {
  // echo '<br>The CANCEL button was used';
  header('Location:../partner.php?P');
  exit();
}elseif (isset($_POST['req_part'])) {

  $UID = $_SESSION['UID'];

  include 'from_UID_get_user_details.inc.php';
  include 'from_UID_get_company_details.inc.php';

  $pSMIC = $_POST['pSMIC'];
  $rel = $_POST['rel'];
  $td = date('U');

  include 'from_SMIC_get_partner_admin_user.inc.php';
  include 'from_SMIC_check_partner_status.inc.php';

  // echo "<br><b>form_new_partner_act.inc.php</b>";
  // echo "<br>get the data :: pSMIC = $pSMIC :: rel = $rel :: td = $td";


  if (empty($ppCID)) {
    // echo "<br>The SMIC does not exist";
    header("Location:../partners.php?P&pan&3");
    exit();
  }elseif ($pSMIC == $SMIC) {
    // echo "<br>It is OUR SMIC";
    header("Location:../partners.php?P&pan&2");
    exit();
  }else {
    if ($rel == 1) {
      // echo "<br>A relationship is needed";
      header("Location:../partners.php?P&pan&s=$pSMIC&4");
      exit();
    }else {
      echo "<br>Relationship :: $rel";
      if ($CPIDrel == $rel) {
        // echo "The relationship is established";
        // check what type of partnership
        if ($CPIDa == 1) {
          // echo "<br>Pending reply";
        }elseif ($CPIDa == 2) {
          // echo "<br>Accepted";
        }elseif ($CPIDa == 3) {
          // echo "<br>Rejected";
        }
      }elseif ($CPIDrel <> $rel) {
        // echo "<br>$CPIDrel :: $rel";
        // echo "<br>Create the relationship";
        // echo "<br>Their is an SMIC of that code";
        // echo "<br>Our company ID (CID) is $CID";
        // echo "<br>Our admin user ID (UID) is $UID";
        // echo "<br>Our admin user email (UIDe) is $UIDe";
        // echo "<br>Our SMIC is $SMIC";
        // echo "<br>Relationship ID is $rel";
        // echo "<br>The potential Partners ID is $ppCID";
        // echo "<br>The potential Partners company name is $ppadUCIDn";
        // echo "<br>The potential Partners ADMINID is $ppadUID";
        // echo "<br>The potential Partners Admin firstname is $ppadUIDf";
        // echo "<br>Their email address is $ppadUIDe";
        // echo "<br><br>Our Admin persons ID is $UID";
        // echo "<br>Their first name is $UIDf";
        // echo "<br>And their email is $UIDe";

        // why do I need to check if they have responded IF this is the request and no email has yet been sent!
        // include 'new_partner_response.inc.php';


        if ($CPIDu == 1) {
          // echo "<br>The validation key has already been used";
        }else {
          // echo "<br>Check if they are a current Partner";
          if ($CPID <> 0) {
            // echo "<br>They are either PENDING or REPLIED";
            // Check their partnership status
            if ($CPIDa == '0') {
              // echo "<br>PENDING";
              header("Location:../partners.php?P&pan&5");
              exit();
            }elseif ($CPIDa == '1') {
              // echo "<br>ACTIVE";
            }elseif ($CPIDa == '2') {
              // echo "<br>CLOSED/REJECTED";
            }
          }else {
            // echo "<br>They are NOT currently a Partner";
            // echo "<br>Sign them up!!!";

            // prepare a hashed verifying code for the requesting (you) member
            $req_SEL = bin2hex(random_bytes(16));
            $req_SEL = password_hash($req_SEL, PASSWORD_DEFAULT);
            // echo "<br>Our code (req_SEL) : $req_SEL";
            // echo '<br>req_SEL value as : '.$req_SEL;

            // prepare a hashed verifying code for the requested (the other member) member
            $acc_SEL = bin2hex(random_bytes(16));
            $acc_SEL = password_hash($acc_SEL, PASSWORD_DEFAULT);
            // echo "<br>Partners code (acc_SEL) : $acc_SEL";

            // instigating CID insCID
            $insCID = $CID;

            // if the rel is CLIENT then req_CID = $CID else req_CID = $pp_CID
            if ($rel == 4) {
              // echo "<br>Line 121";
              $CID = $CID;
              $UID = $UID;
              // $req_SEL = $req_SEL;
              $ppCID = $ppCID;
              $ppadUID = $ppadUID;
              // $acc_SEL = $acc_SEL;
            }elseif ($rel == 5){
              // echo "<br>Line 129";
              $swCID = $CID;
              $swUID = $UID;
              // $swreq_SEL = $req_SEL;
              $CID = $ppCID;
              $UID = $ppadUID;
              // $req_SEL = $acc_SEL;
              $ppCID = $swCID;
              $ppadUID = $swUID;
              // $acc_SEL = $swreq_SEL;
            }

            // Add the date to the company_partnership table
            $sql = "INSERT INTO company_partnerships
                    (insCID, req_CID, req_UID, req_SEL, rel, acc_CID, acc_UID, acc_SEL, inputtime)
                    VALUES
                    (?,?,?,?,?,?,?,?,?)";
            $stmt = mysqli_stmt_init($con);
            if (!mysqli_stmt_prepare($stmt, $sql)){
                echo '<b>FAIL-fnpa</b>';
            }else{
              mysqli_stmt_bind_param($stmt, "sssssssss", $insCID, $CID, $UID, $req_SEL, $rel, $ppCID, $ppadUID, $acc_SEL, $td);
              mysqli_stmt_execute($stmt);
            }
            // echo "<br>Send out an email to BOTH companies";
            if ($rel == 4) {
              include 'email/email_partner_client.inc.php';
            }elseif ($rel == 5) {
              include 'email/email_partner_supplier.inc.php';
            }
            // echo "<br>end of file";
            header("Location:../partners.php?P&pap");
            exit();
          }
        }
      }
    }
  }
}
