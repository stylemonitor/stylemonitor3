<?php
include 'dbconnect.inc.php';
//echo '<b>include/from_CID_select_associate_company_sales.inc.php</b>';

if (isset($_POST['rCID'])) {
	$CID = $_POST['rCID'];
}else {
	$CID = $_SESSION['CID'];
}

if (isset($_POST['lCID'])) {
	$CID = $_POST['lCID'];
}else {
	$CID = $_SESSION['CID'];
}

// if SALES then do not want anyone BUT us
// $CID = $_SESSION['CID'];
$sql = "SELECT  ac.ID as ACID
	        , ac.name as ACIDn
        FROM  associate_companies ac
          , company c
        WHERE c.ID = ?
        AND ac.CID = c.ID
				AND ac.CTID IN (4,6,7)
				ORDER BY ac.name
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br><b>FAIL-fcsacs</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
  	$ACID = $row['ACID'];
  	$ACIDn = $row['ACIDn'];
		echo '<option value="'.$ACID.'">'.$ACIDn.'</option>';
	}
}
?>
