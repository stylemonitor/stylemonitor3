<?php
include 'include/dbconnect.inc.php';
// echo "<b>include/from_CID_count_associate_sales_open.inc.php</b>";
$sql = "SELECT COUNT(o.ID) as cOID
        FROM orders o
          , company c
          , associate_companies ac
          , division d
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND o.DID = d.ID
        AND o.orComp = 0
        -- AND o.// echo = 1
        AND ac.CTID in (4,6,7)
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  // echo '<b>FAIL</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $asOIDo = $row['cOID'];
}
