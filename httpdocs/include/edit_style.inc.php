<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "<b>include/edit_style.inc.php</b>";
// include 'from_CID_count_styles.inc.php';
$CID = $_SESSION['CID'];
if (isset($_GET['s'])) { $PRID = $_GET['s'];}

include 'from_PRID_get_prod_details.inc.php';
if ($PTIDn =="Select Product Type") {
  $PTIDn = "No Product Type Selected";
}else {
  $PTIDn = $PTIDn;
}

$td = date('U');
$td = date('d M Y',$td);
$pddh = "Edit Style : $PRIDr : $PRIDd";
$pddhbc = $styCol;
// include 'page_description_date_header.inc.php';


// GET things
include 'from_CID_get_min_product_type.inc.php';
include 'from_CID_get_min_division.inc.php';
include 'from_DID_get_min_section.inc.php';
include 'from_CID_get_min_season.inc.php';
include 'from_CID_get_min_collection.inc.php';

// COUNT things
include 'from_CID_count_product_type.inc.php';
include 'from_CID_count_divisions.inc.php';
// include 'from_DID_count_section.inc.php';
include 'from_CID_count_season.inc.php';
include 'from_CID_count_collection.inc.php';

if (($cPTID > 1) && ($PTID == $mPTID)) {
  $PTIDn = "Please select a product type";
}

if (isset($_GET['apt'])) {
  $oPRIDsc = $_GET['apt'];

  // echo "Product reference already in use";
  ?>
  <div style="position:absolute; bottom:9%; height: 12%; left:30%; width:40%; background-color:pink; font-size: 150%; border-radius: 10px; border: thick solid RED; z-index:1;">
    Please try again
    <br>The Product Reference is already in use in <b><?php echo $oPRIDsc ?></b>
  </div>
  <?php
}

?>

<!-- <div class="cpsty" style="position:absolute; top:0.25%; height:3%; width:100%; background-color: <?php echo $edtCol ?>; z-index:1;">
  <b>EDIT</b> - Style Reference <?php echo $PRIDr ?>
</div> -->

<div  style="position: absolute; top:25%; height:50%; left:20%; width:60%;background-color: <?php echo $edtCol ?>; border:medium ridge grey; border-radius: 30px;">
  <form action="include/edit_style_act.inc.php" method="POST">
    <!-- The ORIGINAL data -->
    <input  readonly type="hidden" name="ACID" value="<?php echo $ACID ?>">
    <input  readonly type="hidden" name="PRID" value="<?php echo $PRID ?>">
    <input  readonly type="hidden" name="PRIDr" value="<?php echo $PRIDr ?>">
    <input  readonly type="hidden" name="PRIDd" value="<?php echo $PRIDd ?>">
    <input  readonly type="hidden" name="PRIDe" value="<?php echo $PRIDe ?>">
    <input  readonly type="hidden" name="PTID" value="<?php echo $PTID ?>">
    <input  readonly type="hidden" name="SNID" value="<?php echo $SNID ?>">
    <input  readonly type="hidden" name="SCID" value="<?php echo $mSCID ?>">
    <input  readonly type="hidden" name="CLID" value="<?php echo $mCLID ?>">
    <input  readonly type="hidden" name="nDID" value="">
    <input  readonly type="hidden" name="nSNID" value="">
    <input  readonly type="hidden" name="nCLID" value="">

    <div style="position:absolute; top:4%; height:8%; left:3%; width:20%; font-size:100%; text-align:left; padding-right:1%;"><b>Short Code</b></div>
    <div style="position:absolute; top:10%; height:8%; left:4%; width:20%; font-size:100%; text-align:left; padding-right:1%;">Original code</div>
    <!-- initial information re style -->
    <div style="position:absolute; top:10%; height:8%; left:31%; width:50%; font-size:100%; font-weight:bold; text-align:left; padding-right:1%;"><?php echo $PRIDr ?></div>
    <div style="position:absolute; top:17%; height:8%; left:4%; width:20%; font-size:100%; text-align:left; padding-right:1%;">New code
    </div>
    <!-- THE NEW DATA -->
    <?php
    if(isset($_GET['r'])){
      $nPRIDr = ($_GET['r']);
      echo '<input autofocus class ="input_data" style="position:absolute; top:15.5%; height:6%; left:30%; width:50%; font-size:100%; text-align:left; backgroun: antiquewhite;" type="text" name="nPRIDr" placeholder="New Short Code" value="'.$nPRIDr.'""><br>';
    }else{
      echo '<input autofocus class ="input_data" style="position:absolute; top:15.5%; height:6%; left:30%; width:50%; font-size:100%; text-align:left; background-color: antiquewhite;" type="text" name="nPRIDr" placeholder="New Short Code" value=""><br>';
    }
    ?>

    <div style="position:absolute; top:25%; height:8%; left:3%; width:20%; font-size:100%; text-align:left; padding-right:1%;"><b>Description</b></div>
    <div style="position:absolute; top:31%; height:8%; left:4%; width:30%; font-size:100%; text-align:left; padding-right:1%;">Original description</div>
    <!-- initial information re style -->
    <div style="position:absolute; top:31%; height:10%; left:31%; width:50%; font-size:100%; font-weight:bold; text-align:left; padding-right:1%;"><?php echo $PRIDd ?></div>
    <div style="position:absolute; top:38%; height:8%; left:4%; width:30%; font-size:100%; text-align:left; padding-right:1%;">New description</div>
    <!-- NEW DATA  -->
    <?php
    if(isset($_GET['d'])){
      $nPRIDd = ($_GET['d']);
      echo '<input class ="input_data" style="position:absolute; top:36.5%; height:6%; left:30%; width:50%; font-size: 100%; text-align:left; background-color: antiquewhite;" type="text" name="nPRIDd" placeholder="New Description" value="'.$nPRIDd.'"><br>';
    }else{
      echo '<input class ="input_data" style="position:absolute; top:36.5%; height:6%; left:30%; width:50%; font-size: 100%; text-align:left; background-color: antiquewhite;" type="text" name="nPRIDd" placeholder="New Description" value=""><br>';
    }
    ?>

    <div style="position:absolute; top:46%; height:8%; left:3%; width:20%; font-size:100%; text-align:left; padding-right:1%;"><b>Product Type</b></div>
    <div style="position:absolute; top:52%; height:8%; left:4%; width:20%; font-size:100%; text-align:left; padding-right:1%;">Original Type</div>
    <!-- initial information re style -->
    <div style="position:absolute; top:52%; height:10%; left:31%; width:50%; font-size:100%; font-weight:bold; text-align:left; padding-right:1%;"><?php echo $PTIDn ?></div>
    <div style="position:absolute; top:59%; height:8%; left:4%; width:20%; font-size:100%; text-align:left; padding-right:1%;">New Type</div>
    <!-- NEW DATA  -->
    <?php
    if ($cPTID > 1) {
      ?>
      <select style="position:absolute; top:58.5%;height:8%; left:31.3%; width:49.6%;" name="nPTID">
        <option value="">Select Product Type</option>
        <?php
        include 'from_CID_select_prod_type.inc.php'; ?>
      </select>
      <?php
    }else {
      // do need for anything
    }
     ?>

    <!-- <div style="position:absolute; top:67%; height:8%; left:3%; width:20%; font-size:100%; text-align:left; padding-right:1%;"><b>Time to make</b></div>
    <div style="position:absolute; top:73%; height:8%; left:4%; width:20%; font-size:100%; text-align:left; padding-right:1%;">Original T/m</div> -->
    <!-- initial information re style -->
    <!-- <div style="position:absolute; top:73%; height:10%; left:31%; width:50%; font-size:100%; font-weight:bold; text-align:left; padding-right:1%;"><?php echo $PRIDe ?></div>
    <div style="position:absolute; top:80%; height:8%; left:4%; width:20%; font-size:100%; text-align:left; padding-right:1%;">New T/m</div> -->
    <!-- NEW DATA  -->
    <!-- <?php
    if(isset($_GET['e'])){
      $nPRIDe = ($_GET['e']);
      echo '<input class ="input_data" style="position:absolute; top:78.5%; height:6%; left:30%; width:50%; font-size: 100%; text-align:left; background-color: antiquewhite;"   type="text" name="nPRIDe" placeholder="New Make Time" value="'.$nPRIDe.'"><br>';
    }else{
      echo '<input class ="input_data" style="position:absolute; top:78.5%; height:6%; left:30%; width:50%; font-size: 100%; text-align:left; background-color: antiquewhite;" type="text" name="nPRIDe" placeholder="New Make Time" value=""><br>';
    }
    ?> -->

    <?php
    include 'from_CID_count_divisions.inc.php';
    include 'from_CID_count_season.inc.php';
    include 'from_CID_count_collection.inc.php';
    ?>
    <button class="entbtn" style="bottom:2%; left:35%; width:10%; background-color:<?php echo $fsvCol ?>;" type="submit" name="edit_style">SAVE</button>
    <button class="entbtn" style="bottom:2%; left:55%; width:10%; background-color:<?php echo $fcnCol ?>;" type="submit" name="can_edit_style">Cancel</button>
  </form>
</div>
