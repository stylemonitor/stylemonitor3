<?php
include 'dbconnect.inc.php';
// echo "<b>include/form_associate_address_quick_add.inc.php</b>";

include 'set_urlPage.inc.php';
include 'SM_colours.inc.php';

if (isset($_GET['c'])) {
  $bc = "#f7cc6c";
  $h = 'CLIENT';
  $qas = "Select Clients Country";
}elseif (isset($_GET['s'])) {
  $bc = "#bbbbff";
  $h = 'SUPPLIER';
  $qas = "Select Suppliers Country";
}

?>
<div class="overlay"></div>

<div style="position:absolute; top:25%; height:5%; left:20%; width:60%; background-color:<?php echo $bc ?>; font-size: 150%; border-left: 2px ridge grey; border-top: 2px ridge grey; border-right: 2px ridge grey; border-radius:10px 10px 0px 0px; z-index:1;">Quick add a <?php echo $h ?> Associate Company</div>
<div style="position:absolute; top:30%; height:35%; left:20%; width:60%; background-color:<?php echo $edtCol ?>; font-size: 150%; border-right:2px ridge grey; border-bottom:2px ridge grey; border-left:2px ridge grey; border-radius:0px 0px 10px 10px; z-index:1;"></div>

<form action="include/form_associate_address_add_act.inc.php" method="POST">
  <input type="hidden" name="urlPage" value="<?php echo $urlPage ?>">
  <input type="hidden" name="comp" value="<?php echo $_SESSION['CID']; ?>">
  <?php
  if (isset($_GET['c'])) {// set the relationship to CLIENT?>
    <input type="hidden" name="rel" value="4"><?php
  }elseif (isset($_GET['s'])) {// set the relationship to SUPPLIER?>
    <input type="hidden" name="rel" value="5"><?php
  }
  ?>

  <!-- <label for="name">Company</label> -->
  <div style="position:absolute; top:35%; height:4%; right:65%; width:40%; font-weight:bold; text-align:right; z-index:1;">
    Associate Company
  </div>
  <?php
  if (isset($_GET['name'])) {
    $name = $_GET['name'];
    ?>
    <input required autofocus style="position:absolute; top:34%; height:3%; left:37%; width:40%; text-align:left; text-indent:2%; z-index:1;" type="text" name="name" value="<?php echo "$name"; ?>">
    <?php
  }else {
    ?>
    <input required autofocus style="position:absolute; top:34%; height:3%; left:37%; width:40%; text-align:left; text-indent:2%; z-index:1;" type="text" name="name" placeholder="Associate Company Name">
    <?php
  }
  ?>

  <div style="position:absolute; top:40%; right:65%; width:40%; font-weight:bold; text-align:right; z-index:1;">
    Country
  </div>

  <!-- Dropdown dependent menu START -->
    <select id="CYID" name="CYID" onchange="FetchTimeZone(this.value)" style="position:absolute; top:39%; height:4%; left:37%; width:40.8%; background-color:<?php echo $ddmCol ?>; z-index:1;">
      <option value="">Select Country</option>
      <?php
      include 'include/select_company_country_details.inc.php';
      ?>
    </select>
    <script type="text/javascript">
    function FetchTimeZone(id){
      $('#TZID').html('');
      // alert(id);
      // return false;
      $.ajax({
        type : 'POST',
        url : 'include/select_country_timezone.inc.php',
        data : { CYID:id},
        success : function(data){
          $('#TZID').html(data);
        }
      })
    }
    </script>
    <div style="position:absolute; top:45%; right:65%; width:40%; font-weight:bold; text-align:right; z-index:1;">
      Country Timezone
    </div>
    <select required id="TZID" name="TZID" style="position:absolute; top:44%; height:4%; left:37%; width:40.8%; background-color:<?php echo $ddmCol ?>; z-index:1;">
      <option value="">Select TimeZone</option>
      <?php
      include 'include/select_country_timezone.inc.php';
      ?>
    </select>
  <!-- Dropdown dependent menu END -->

  <div style="position:absolute; top:50%; right:65%; width:40%; font-weight:bold; text-align:right; z-index:1;">
    Main Product Type
  </div>
  <select style="position:absolute; top:49%; height:4%; left:37%; width:40.8%; background-color:<?php echo $ddmCol ?>; z-index:1;" name="PTID">
    <option value="">Select Main Product Type of Company</option>
    <?php
    include 'include/from_CID_select_prod_type.inc.php';
    ?>
  </select>

  <button class="entbtn" style="position:absolute; bottom:5%; left:70%; width:10%; background-color: <?php echo $fsvCol ?>; z-index:1;" type="submit" name="adddets">Enter</button>
  <button formnovalidate class="entbtn" style="bottom:5%; left:85%; width:10%; background-color: <?php echo $fcnCol ?>; z-index:1;" type="submit" name="candets" formnovalidate>Cancel</button>
</form>

<form action="associates.php?acn&adq&adqh" method="post">
  <button class="help" type="submit" name="button">HELP</button>
</form>
