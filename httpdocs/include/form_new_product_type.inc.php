<?php
include 'dbconnect.inc.php';
// echo "<b>include/form_new_product_type.inc.php</b>";
include 'from_CID_count_product_type.inc.php';

if($cPTID == 0){
  ?>
  <div style="position:absolute; top:64%; height:25%; left:30%; width:40%;">
    <p style="font-size:150%;">Add a <b>Product type short code</b></p>
    <br>
    <p style="font-size:150%;">Then a full <b>Product type description</b></p>
    <br>
    <p style="font-size:150%;">Finally click on <b>SAVE</b> <br>to add this product type to your company</p>
  </div>
  <?php
  // echo "Product type count = $cPTID";
}

if (isset($_GET['a'])) {
  // include 'report_single_product_type.inc.php';
  ?>
  <div style="position:absolute; bottom:11%; height: 12%; left:30%; width:40%; background-color:pink; font-size: 150%; border-radius: 10px; border: thick solid RED; z-index:1;">
    Please try again
    <br>The <b>Description</b> is already in use
  </div>
  <?php
}elseif (isset($_GET['b'])) {
  // include 'include/report_single_product_type.inc.php';
  ?>
  <div style="position:absolute; bottom:11%; height: 12%; left:30%; width:40%; background-color:pink; font-size: 150%; border-radius: 10px; border: thick solid RED;  z-index:1;">
    Please try again
    <br>The <b>Short Code</b> is already in use
  </div>
  <?php
}elseif (isset($_GET['c'])) {
  // include 'report_single_product_type.inc.php';
  ?>
  <div style="position:absolute; bottom:11%; height: 12%; left:30%; width:40%; background-color:pink; font-size: 150%; border-radius: 10px; border: thick solid RED; padding-top:1.5%; z-index:1;">
    Please try again
    <br>BOTH are already in use
  </div>
  <?php
}elseif (isset($_GET['d'])) {
  // include 'report_single_product_type.inc.php';
  ?>
  <div style="position:absolute; bottom:11%; height: 12%; left:30%; width:40%; background-color:pink; font-size: 150%; border-radius: 10px; border: thick solid RED; padding-top:1.5%; z-index:1;">
    Please try again
    <br>The <b>Short Code</b> is too long
  </div>
  <?php
}
?>

<div class="overlay"></div>

<div style="position:absolute; top:30%; height:24%; left:26%; width:48%; font-size: 175%; font-weight:bold; text-align: center; background-color:#36c969; border: 2px ridge black; border-radius: 10px; z-index:1;">Create new product type</div>

<form action="include/form_new_product_type_act.inc.php" method="POST">
  <?php
  if (isset($_GET['sc'])) {
    $PRIDs = $_GET['sc'];
    ?>
    <input required autofocus class ="input_data" style="position:absolute; top:37%; height:4%; left:40%; width:20%; text-align:center; background-color: #f1f1f1; z-index:1;" type="text" name="scode" placeholder="Product type short code" value="<?php echo $PRIDs ?>"><br>
    <?php
  }else {
    ?>
    <input required autofocus class ="input_data" style="position:absolute; top:37%; height:4%; left:40%; width:20%; text-align:center; background-color: #f1f1f1; z-index:1;" type="text" name="scode" placeholder="Product type short code"><br>
    <?php
  }
  if (isset($_GET['p'])) {
    $pref = $_GET['p']
    ?>
    <input required class ="input_data" style="position:absolute; top:44%; height:4%; left:30%; width:40%; text-align:center; background-color: #f1f1f1; z-index:1;" type="text" name="pref" placeholder="Product type description" value="<?php echo $pref ?>"><br>
    <?php
  }else {
    ?>
    <input required class ="input_data" style="position:absolute; top:44%; height:4%; left:30%; width:40%; text-align:center; background-color: #f1f1f1; z-index:1;" type="text" name="pref" placeholder="Product type description"><br>
    <?php
  } ?>

  <button class="entbtn" style="bottom:5%; left:70%; width: 10%; font-weight: bold; background-color: <?php echo $fsvCol ?>; z-index:1;" type="submit" name="newpt">SAVE</button>
  <button formnovalidate class="entbtn" style="bottom:5%; left:85%; width: 10%; font-weight: bold; background-color: <?php echo $fcnCol ?>; z-index:1;" type="submit" name="cancel">Cancel</button>
</form>

<form action="styles.php?S&spt&fnpt&fnpth" method="post">
  <button class="entbtn" type="submit" style="position:absolute; bottom:5%; left:5%; width:15%; background-color: <?php echo $hlpCol ?>; z-index:1;" name="button">HELP</button>
</form>
