<?php
session_start();
include 'dbconnect.inc.php';

include 'from_OIID_get_make_unit.inc.php';
include 'from_OPID_get_qty.inc.php';
include 'R_on_place_summd.inc.php';
include 'R_on_sum_MP1d.inc.php';
include 'R_on_sum_MP2d.inc.php';
include 'R_on_sum_MP3d.inc.php';
include 'R_on_sum_MP4d.inc.php';
include 'R_on_sum_MP5d.inc.php';
include 'R_on_sum_MP6d.inc.php';

$B1 = ($upQTY - $sMP1d);
$B2 = ($sMP1d - $sMP2d);
$B3 = ($sMP2d - $sMP3d);
$B4 = ($sMP3d - $sMP4d);
$B5 = ($sMP4d - $sMP5d);
$B6 = ($sMP5d - $sMP6d);

// echo "include/report_order_movement.inc.php";
$UID = $_SESSION['UID'];
$CID = $_SESSION['CID'];
// echo "<b>include/form_new_company_user_act.inc.php</b>";
// echo "form_new_order_movement";
?>

<table class="trs" style="position:absolute; top:34.1%; z-index:1;">
  <tr >
    <th style="width:2%;">Nos</th>
    <th style="width:6%; text-align:left; text-indent:2%;">S/code</th>
    <th style="width:30%; text-align:left; text-indent:2%;">Description</th>
    <th style="width:22%;">Product Type</th>
    <th style="width:4%;"><a style="color:red; text-decoration:none;" href="orders.php?L&awt">Awt</a></th>
    <th style="width:4%;"><a style="color:red; text-decoration:none;" href="orders.php?L&mp1">#1</a></th>
    <th style="width:4%;"><a style="color:red; text-decoration:none;" href="orders.php?L&mp2">#2</a></th>
    <th style="width:4%;"><a style="color:red; text-decoration:none;" href="orders.php?L&mp3">#3</a></th>
    <th style="width:4%;"><a style="color:red; text-decoration:none;" href="orders.php?L&mp4">#4</a></th>
    <th style="width:4%;"><a style="color:red; text-decoration:none;" href="orders.php?L&mp5">#5</a></th>
    <th style="width:4%;"><a style="color:red; text-decoration:none;" href="orders.php?L&whs">Whs</a></th>
    <th style="width:4%;">Sent</th>
    <th style="width:8%;">Due Date</th>
  </tr>

  <?php
  $sql = "SELECT oi.ord_item_nos as OIIDnos
            , oi.their_item_ref as cref
            , oi.ord_item_nos as oin
            , oi.their_item_ref as idesc
            , oidd.item_del_date as iddate
            , oiq.order_qty as oiQty
            , pt.ID as PTID
            , pt.scode as ptype
            , pr.ID as PRID
            , pr.prod_ref as prRef
            , pr.prod_desc as prDesc
            , oi.samProd as OTID
            , o.CPID as CPID
            , cp.req_CID as CPIDr
            , cp.acc_CID as CPIDa
          FROM orders o
            , order_item oi
            , order_item_del_date oidd
            , order_item_qty oiq
            , product_type pt
            , prod_ref pr
            , company_partnerships cp
          WHERE oi.ID = ?
          AND oi.OID = o.ID
          AND oiq.OIID = oi.ID
          AND oidd.OIID = oi.ID
          AND pr.ID = oi.PRID
          AND pt.ID = pr.PTID
          AND o.CPID = cp.ID
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-rom</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $OIID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      // $OIID = $row['OIID'];
      $OIIDnos = $row['OIIDnos'];
      $cref    = $row['cref'];
      $oin     = $row['oin'];
      $idesc   = $row['idesc'];
      $iddate  = $row['iddate'];
      $oiQty   = $row['oiQty'];
      $ptype   = $row['ptype'];
      $PTID    = $row['PTID'];
      $PRID   = $row['PRID'];
      $PRIDn   = $row['prRef'];
      $prDesc  = $row['prDesc'];
      $OTID    = $row['OTID'];
      $CPID    = $row['CPID'];
      $CPIDr   = $row['CPIDr'];

      // $WIPin = $oiQty - $sMP1;
      $WIPin = $oiQty - $sMP1d;
      // $WIPin = $sMP1d - $sMP6d;
      ?>

      <tr style="background-color:#f1f1f1;">
        <td><?php echo $OIIDnos ?></td>
        <td style="text-align:left; text-indent:1%;"><a style="color:red; text-decoration:none;" href="styles.php?S&sio&s=<?php echo $PRID ?>"><?php echo $PRIDn ?></a></td>
        <td style="text-align:left; text-indent:1%;"><?php echo $prDesc ?></td>
        <td style="text-transform:uppercase;"><?php echo $ptype ?></td>
        <!-- <td><?php echo $oiQty ?></td> -->

        <?php
        if ($B1 < 0){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#f4632e;">'.($B1*(-1)).'</td>';
        }elseif ($sMP1d < $upQTY) {
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffffba;">'.$B1.'</td>';
        }elseif ($sMP1d == $upQTY) {
          echo '<td style="text-align: right; padding-right:0.5%; color:#a6ffa6; background-color:#a6ffa6;">'.$B1.'</td>';
        }elseif ($sMP1d > $upQTY) {
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffa2a2;">'.$B1.'</td>';
        }elseif ($B1 > 0) {
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffa2a2;">'.$B1.'</td>';
        } ?>

        <!-- BUFFER 2 -->
        <?php
        if ($B2 < 0){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#f4632e;">'.($B2*-1).'</td>';
        }elseif ($sMP2d < $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffffba;">'.$B2.'</td>';
        }elseif ($sMP2d == $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; color:#a6ffa6; background-color:#a6ffa6;">'.$B2.'</td>';
        }elseif ($sMP2d > $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffa2a2;">'.$B2.'</td>';
        }elseif ($B2 > 0) {
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffa2a2;">'.$B2.'</td>';
        } ?>

        <!-- BUFFER 3 -->
        <?php
        if ($B3 < 0){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#f4632e;">'.($B3*-1).'</td>';
        }elseif ($sMP3d < $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffffba;">'.$B3.'</td>';
        }elseif ($sMP3d == $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; color:#a6ffa6; background-color:#a6ffa6;">'.$B3.'</td>';
        }elseif ($sMP3d > $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffa2a2;">'.$B3.'</td>';
        }elseif ($B3 > 0) {
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffa2a2;">'.$B3.'</td>';
        } ?>

        <!-- BUFFER 4 -->
        <?php
        if ($B4 < 0){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#f4632e;">'.($B4*-1).'</td>';
        }elseif ($sMP4d < $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffffba;">'.$B4.'</td>';
        }elseif ($sMP4d == $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; color:#a6ffa6; background-color:#a6ffa6;">'.$B4.'</td>';
        }elseif ($sMP4d > $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffa2a2;">'.$B4.'</td>';
        }elseif ($B4 > 0) {
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffa2a2;">'.$B4.'</td>';
          // code...
        } ?>

        <!-- BUFFER 5 -->
        <?php
        if ($B5 < 0){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#f4632e;">'.($B5*-1).'</td>';
        }elseif ($sMP5d < $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffffba;">'.$B5.'</td>';
        }elseif ($sMP5d == $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; color:#a6ffa6; background-color:#a6ffa6;">'.$B5.'</td>';
        }elseif ($sMP5d > $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffa2a2;">'.$B5.'</td>';
        }elseif ($B5 > 0) {
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffa2a2;">'.$B5.'</td>';
        } ?>

        <!-- BUFFER 6 -->
        <!-- calculates the number of garments between MP5 and MP6       -->
        <?php
        if ($B6 < 0){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#f4632e;">'.($B6*-1).'</td>';
        }elseif ($sMP6d < $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffffba;">'.$B6.'</td>';
        }elseif ($sMP6d == $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; color:#a6ffa6; background-color:#a6ffa6;">'.$B6.'</td>';
        }elseif ($sMP6d > $upQTY){
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffa2a2;">'.$B6.'</td>';
        }elseif ($sMP6d > 0) {
          echo '<td style="text-align: right; padding-right:0.5%; background-color:#ffa2a2;">'.$B5.'</td>';
        } ?>

<!--
        <td><?php echo $WIPin ?></td>
        <td><?php echo $WIPin ?></td>
        <td><?php echo $WIPin ?></td>
        <td><?php echo $WIPin ?></td>
        <td><?php echo $WIPin ?></td>
        <td><?php echo $WIPin ?></td> -->

        <?php
        if ($oiQty == $sMP6d){
          ?>
          <!-- change colour to pale green IF the order has been completed -->
          <td style="text-align: right; padding-right:0.5%; background-color:#a6ffa6">
            <?php
            echo 'Comp';
          }else{
            ?>
            <td  style="text-align: right; padding-right:0.5%;">
              <?php
              echo $sMP6d;
              // echo "here";
          }
          ?>
          </td>
          <td>sent</td>
          <?php

          $td = date('U');
          $dd = $iddate;

          // Item due date
          $DuDate = date('d-M-Y', $dd);

          if($dd<$td){;?>
            <td style="background-color:#d8081e;"><?php echo $DuDate ;?></td>
            <?php
          }elseif($dd  <($td+604740)){;?>
            <td style="background-color:#eb3333;"><?php echo $DuDate ;?></td>
            <?php
          }elseif($dd>($td+604740) && $dd<($td+1209660)){;?>
            <td style="background-color:#def532;"><?php echo $DuDate ;?></td>
            <?php
          }else{;?>
            <td><?php echo $DuDate ;?></td>
            <?php
          }?>
        </tr>
        <?php
      }
    }
      ?>
    </table>
    <?php
    if ($nosMU == 1) {
      // echo "Get the OPID for the order";
      include 'from_OIID_get_make_unit.inc.php';
      // echo "<br>OPID = $OPID";
      // include 'record_order_movement.inc.php';
    }else {
      echo "Select from a list of make units which one to show";
    }


    // IF it is a PARTNERSHIP then the CLIENT cannot update the movements OR see the movement log
    if ($CPIDr <> $CID) {
      // echo "DO NOT SHOW CPID : $CPID : CPIDr : $CPIDr :::: CID : $CID";
      ?>
      <div style="position:absolute; top:80%; height:15%; left:5%; width:90%; background-color:pink;">
        CLIENT Data Request
        <form class="" action="index.html" method="post">
          <div class="">
            Change of Delivery date
          </div>
          <div class="">
            Change of order Quantity
          </div>
        </form>
      </div>
      <?php
    }else {
      // echo "SHOW CPID : $CPID : CPIDr : $CPIDr :::: CID : $CID";

      ?>
      <!-- <div style="position:absolute; top:80%; height:15%; left:5%; width:90%; background-color:pink;">
        SUPPLIER Data Request
        <form class="" action="index.html" method="post">
          <div class="">
            Change of Delivery date
          </div>
          <div class="">
            Change of order Quantity
          </div>
        </form>
      </div> -->

      <?php
    }
    if ($sMP6d >= ($oiQty*0.1)){
      include 'report_order_despatch.inc.php';
    }
