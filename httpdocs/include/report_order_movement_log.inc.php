<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/REPORT_ORDER_header.inc.php";

include 'from_CID_count_divisions.inc.php';
include 'from_OIID_count_make_unit.inc.php';
include 'from_OIID_get_order_details.inc.php';
include 'from_OIID_get_partners_CID.inc.php';
include 'R_on_over_sum_MP1.inc.php';
include 'R_on_over_sum_MP6.inc.php';

include 'set_urlPage.inc.php';

if (isset($_GET['oi'])) {
  $OIID = $_GET['oi'];
}

// if ($cDID == 1) {
//   echo "<br>One division get mDID";
// }else {
//   echo "<br>Multiple divisions";
//   // echo "nosMU :: $nosMU";
//   // from OIID check how many op's have been made
// }
if ($nosMU == 1) {
  // get the single order placed ID
  include 'from_OIID_get_make_unit.inc.php';
  include 'report_order_movement.inc.php';
  // include 'from_OPID_order_movement_summary.inc.php';
}else {
  // Show the list of make units and then select one
  include 'from_OIID_get_make_unit.inc.php';
  include 'report_order_movement.inc.php';
}

if (empty($sMP1)) {$sMP1 = 0;}else { $sMP1 = $sMP1;}
if (empty($sMP6)) {$sMP6 = 0;}else { $sMP6 = $sMP6;}

$WIP = ($sMP1 - $sMP6);

if ($OTID == 1) {
  $col  = 'f7cc6c';
  $type = 'Associate Sales Order';
  $OT   = 'Production Order';
}elseif ($OTID == 2) {
  $col = 'bbbbff';
  $type = 'Associate Purchase Order';
  $OT   = 'Production Order';
}elseif ($OTID == 3) {
  $col = 'f7cc6c';
  $type = 'Partner Sales Order';
  $OT   = 'Production Order';
}elseif ($OTID == 4) {
  $col = 'bbbbff';
  $type = 'Partner Purchase Order';
  $OT   = 'Production Order';
}elseif ($OTID == 5) {
  $col = 'f7cc6c';
  $type = 'Associate Sales Order';
  $OT   = 'Sample Request';
}elseif ($OTID == 6) {
  $col = 'bbbbff';
  $type = 'Associate Purchase Order';
  $OT   = 'Sample Request';
}elseif ($OTID == 7) {
  $col = 'f7cc6c';
  $type = 'Partner Sales Order';
  $OT   = 'Sample Request';
}elseif ($OTID == 8) {
  $col = 'bbbbff';
  $type = 'Partner Purchase Order';
  $OT   = 'Sample Request';
}
?>
<div class="cpsty" style="height:3%; font-size: 120%; font-weight:bold; color:#887f7f; background-color:#<?php echo $col ?>"><?php echo $type ?></div>
<div style="position:absolute; top:4%; height:10%; left:1%; width:50%; padding-right: 1%; background-color:#f1f1f1; font-size:250%; text-align: left;">
  <?php echo $OT ?>
</div>
<div style="position:absolute; top:4.5%; height:10%; right:15%; width:12.8%; padding-right: 1%; color:#887f7f; background-color:#f1f1f1; font-size:200%; text-align: right;">O/r</div>
<div style="position:absolute; top:4%; height:9%; right:0%; width:12.8%; padding-right: 1%; background-color:#f1f1f1; font-size:250%; text-align: right;"><?php echo $oref ?></div>

<table class="trs" style="position:absolute; top:11%;">
  <!-- <caption class="cpsty" style="background-color:#<?php echo $col ?>"></caption -->

  <tr>
    <th style="width:35%; color:#887f7f; text-align:left; text-indent:5%;">Client</th>
    <th style="width:12%; color:#887f7f; text-align:center;">Order Date</th>
    <th style="width:12%; color:#887f7f; text-align:center;">Due Date</th>
  <tr>

  <tr>
    <?php
    if ($DIDnC == "Select Division") {$DIDnC = "";}else { $DIDnC = $DIDnC;}
    ?>
    <!-- <td style="top:5%; width:35%;font-weight:bold; font-size: 200%; text-align:left; text-indent:2%; background-color:#f1f1f1;"><a style="color:black; text-decoration:none;" href="mm_A.php?A&cd&PID=<?php echo $ACIDC ?>"><?php echo $ACIDnC.' : '.$DIDnC ?></a></td> -->
    <td style="top:5%; width:35%;font-weight:bold; font-size: 200%; text-align:left; text-indent:2%; background-color:#f1f1f1;"><a style="color:black; text-decoration:none;"><?php echo $ACIDnC.' : '.$DIDnC ?></td>

    <td style="width:12%; font-size: 120%; text-align:center; background-color:#f1f1f1;"> <?php echo $odel ?></td>
    <?php

    $td = date('U');
    $dd = $dueDate;

    $DuDate = date('d-M-Y', $dd);

    if($dd<$td){;?>
      <td style="width:12%; font-size: 160%; text-align:center; background-color:#d8081e;"><?php echo $DuDate ;?></td>
      <?php
    }elseif($dd<($td+604740)){;?>
      <td style="width:12%; font-size: 160%; text-align:center; background-color:#eb3333;"><?php echo $DuDate ;?></td>
      <?php
    }elseif($dd>($td+604740) && $dd<($td+1209660)){;?>
      <td style="width:12%; font-size: 160%; text-align:center; background-color:#def532;"><?php echo $DuDate ;?></td>
      <?php
    }else{;?>
      <td style="background-color:#f1f1f1; width:12%; font-size: 160%; text-align:center;"><?php echo $DuDate ;?></td>
      <?php
    }
    ?>
  </tr>
</table>

<table class="trs" style="position:absolute; top:21%;">
  <tr>
    <th style="width:31%; text-align:left; text-indent:5%; color:#887f7f;">Client Ref</th>
    <th style="wi:10%; text-align:center; text-indent:5%; color:#887f7f;">Quantity</th>
  </tr>
  <tr>
    <td style="background-color:#f1f1f1; width:31%; font-size: 160%; text-align:left; text-indent:2%;"><?php echo $OIDcor ?></td>
    <td style="background-color:#f1f1f1; width:10%; font-size: 160%; text-align:center;"><?php echo $oiQty ?></td>
  </tr>
</table>
<!--
<form style="position:absolute; bottom:1%; height:3.5%; left:5%; width:90%;" action="include/report_movement_log_act.inc.php" method="post">
  <input type="hidden" name="urlPage" value="<?php echo $urlPage ?>">
  <input type="hidden" name="oi" value="<?php echo $OIID ?>">
  <button class="sub_btn" type="submit" name="movUP">BACK</button>
</form> -->

<?php
?>
