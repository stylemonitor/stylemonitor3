<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "<b>include/form_new_style_ADD.inc.php</b>";

?>
<!-- <div style="position:absolute; top:0; height:100%; left:0%; width:100%; background-blend-mode:overlay;"></div> -->
<?php
include 'from_CID_check_product_ref_set.inc.php';

if($cPRID == 0){
  ?>
  <div style="position:absolute; top:64%; height:30%; left:30%; width:40%; z-index:1;">
    <p style="font-size:150%;">Add a style <b>Short Code</b></p>
    <br>
    <p style="font-size:150%;">Then a fuller <b>Description</b></p>
    <br>
    <p>(We've added the product type for you)</p>
    <br>
    <p style="font-size:150%;">Finally click on <b>SAVE</b> <br>to add this style to your company</p>
  </div>
  <?php
  // echo "Product type count = $cPTID";
}

if (isset($_GET['apt'])) {
  $oPRIDsc = $_GET['apt'];

  // echo "Product reference already in use";
  ?>
  <div class="overlay"></div>
  <div style="position:absolute; bottom:12%; height: 12%; left:30%; width:40%; background-color:pink; font-size: 150%; border-radius: 10px; border: thick solid RED; z-index:1;">
    Please try again
    <br>The Product Reference is already in use in <b><?php echo $oPRIDsc ?></b>
  </div>
  <?php
}

if (isset($_GET['pdx'])) {
  ?>
  <div style="position:absolute; bottom:12%; height: 12%; left:30%; width:40%; background-color:pink; font-size: 150%; border-radius: 10px; border: thick solid RED; z-index:1;">
    Please try again
    <br>The Product Description/ Product Type is already in use</b>
  </div>
  <?php
}

?>
<div class="overlay"></div>

<div style="position:absolute; top:25%; height:30%; left:26%; width:48%; font-size: 175%; font-weight:bold; text-align: center; background-color:#36c969; border: 2px ridge black; border-radius: 10px; z-index:1;">Add style reference</div>

<form action="include/form_new_style_act.inc.php" method="POST">
  <input type="hidden" name="ACID" value="<?php echo $ACID ?>">
  <input type="hidden" name="DID" value="<?php echo $NDmDID ?>">
  <input type="hidden" name="SNID" value="<?php echo $mSNID ?>">
  <input type="hidden" name="CLID" value="<?php echo $mCLID ?>">
  <?php

  if(isset($_GET['r'])){
    $ref = ($_GET['r']);
    ?>
    <input required autofocus class ="input_data" style="position:absolute; top:33%; height:4%; left:40%; width:20%; font-size:120%; text-align:center; background-color: <?php echo $rqdCol ?>; z-index:1;" type="text" name="ref" placeholder="Short Code" value= <?php echo $ref ?>>
    <?php
  }else{
    ?>
    <input required autofocus class ="input_data" style="position:absolute; top:33%; height:4%; left:40%; width:20%; font-size:120%; text-align:center; background-color: <?php echo $rqdCol ?>; z-index:1;" type="text" name="ref" placeholder="Short Code">
    <?php
    // echo '<input required autofocus class ="input_data" style="position:absolute; top:20%; height:12%; left:00%; width:20%;  text-align:center;" type="text" name="ref" placeholder="Short Code"><br>';
  }

  if(isset($_GET['d'])){
    $desc = ($_GET['d']);
    ?>
    <input required class ="input_data" style="position:absolute; top:40%; height:4%; left:31%; width:37%; font-size:120%; text-align:center; background-color: <?php echo $rqdCol ?>; z-index:1;" type="text" name="dsc" placeholder="Description" value= <?php echo $desc ?>
    <?php
  }else{
    ?>
    <input required class ="input_data" style="position:absolute; top:40%; height:4%; left:31%; width:37%; font-size:120%; text-align:center; background-color: <?php echo $rqdCol ?>; z-index:1;" type="text" name="dsc" placeholder="Description">
    <?php
  }
  if ($cPTID == 1) {
    include 'from_CID_get_SOLE_product_type.inc.php';
    ?>
      <input type="hidden" name="PTID" value="<?php echo $PTID ?>">
      <div style="position:absolute; top:47%; left:35%; width:30%; font-size:120%; text-align:center; background-color:#36c969; z-index:1;">
        <?php echo $PTIDn ?>
      </div>
    <?php
  }elseif (isset($_GET['p'])) {
    $PTID = $_GET['p'];
    include 'from_PTID_get_product_type_info.inc.php';
    ?>
      <input type="hidden" name="PTID" value="<?php echo $PTID ?>">
      <div style="position:absolute; top:48%; left:35%; width:30%; font-size:120%; text-align:center; background-color:<?php echo $rqdCol ?>; z-index:1;">
        <?php echo $PTIDn ?>
      </div>
    <?php
  }
  else {
    ?>
    <select required style="position:absolute; top:48%; height:4%; left:31.5%; width:37.5%; font-size:100%; text-align:center; background-color: <?php echo $ddmCol ?>; z-index:1;" name="PTID">
      <option value="">Select Product Type</option>
      <?php
      include 'from_CID_select_prod_type.inc.php';
      ?>
    </select>
    <?php
  }
  ?>
  <button class="entbtn" style="position:absolute; bottom:5%; left:70%; width: 10%; background-color: <?php echo $fsvCol ?>; z-index:1;" type="submit" name="reg_STY">SAVE</button>
  <button formnovalidate class="entbtn" style="position:absolute; bottom:5%; left:85%; width: 10%; background-color: <?php echo $fcnCol ?>; z-index:1;" type="submit" name="cancel">Cancel</button>
</form>

<?php
  if (isset($_GET['p'])) {
    $PTID = $_GET['p'];
    $spage = "styles.php?S&snn&fns&p=$PTID&fnsh";
  }else {
    $spage = "styles.php?S&snn&fns&fnsh";
  }

?>
<form action="<?php echo $spage ?>" method="post">
  <button class="entbtn" type="submit" style="position:absolute; bottom:5%; left:5%; width:15%; background-color:<?php echo $hlpCol ?>; z-index:1;" name="addPtypebutton">HELP</button>
</form>
