<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/for_OR_get_max_tSID.inc.php</b>";

$sql = "SELECT MAX(s.ID) as SID
        FROM division d
        , section s
        WHERE d.ID = ?
        AND s.DID = d.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fOgcfA</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $tDID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $maxtSID = $row['SID'];
}
