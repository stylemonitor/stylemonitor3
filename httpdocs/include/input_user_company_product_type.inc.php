<?php
include 'dbconnect.inc.php';
// echo  "<br>input_user_company_product_type.inc.php";
// echo  "<br>ACID : $ACID";

$sql = "INSERT INTO product_type
          (CID, UID, inputtime)
        VALUES
          (?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudd";
}else {
  mysqli_stmt_bind_param($stmt,"sss", $CID, $UID, $td);
  mysqli_stmt_execute($stmt);
}

// Get the DID
$sql = "SELECT ID as PTID
        FROM product_type
        WHERE CID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudd";
}else {
  mysqli_stmt_bind_param($stmt,"s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $PTID  = $row['PTID'];
}
// echo "<br>Product type ID == $PTID";

$sql = "INSERT INTO company_product_types
          (CID, PTID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudd";
}else {
  mysqli_stmt_bind_param($stmt,"ssss", $CID, $PTID, $UID, $td);
  mysqli_stmt_execute($stmt);
}
