<?php
include 'include/dbconnect.inc.php';
// echo "<br><b>include/from_OID_get_order_details.inc.php</b>";

if (isset($_GET['o'])) {
  $OID = $_GET['o'];
}

// echo "<br>OID = $OID :: CID = $CID";
// GET CLIENT ORDER DETAILS
$sql = "SELECT o.ID AS OID
-- SQL is 'from_OID_get_order_details.sql'
      , o.orNos AS oref
      , o.orDate AS orDate
      , odd.del_Date AS dueDate
      , oidd.item_del_date AS OIIDd
      , oiq.order_qty AS oiQty
      , o.fCID AS fCID
      , o.tCID AS tCID
      , cac.ID AS ACIDC
      , cac.name AS ACIDnC
      , sac.ID AS ACIDS
      , sac.name AS ACIDnS
      , cd.ID AS DIDC
      , IF(cd.name = 'Select Division','Unknown',cd.name) AS DIDnC
      , sd.ID AS DIDS
      , IF(sd.name = 'Select Division','Unknown',sd.name) AS DIDnS
      , cs.ID AS SIDC
      , ss.ID AS SIDS
      , o.CPID AS CPID
      , oi.samProd AS samProd
      , CASE WHEN o.tCID = o.fCID
             THEN o.OTID
             ELSE CASE WHEN o.tCID = ? THEN 3
             ELSE 4
             END
        END AS OTID
      , o.our_order_ref OIDcor
      , IF(o.their_order_ref = '','Unknown',o.their_order_ref) OIDtor
      , oi.ID AS OIID
      , oi.ord_item_nos AS OIIDnos
      , oi.their_item_ref AS OIIDcr
      , oi.pItemRef AS pOIIDcr
      , pr.prod_ref AS prRef
      , oi.PRID AS PRID
      , oi.pPRID AS pPRID
FROM order_item oi
     INNER JOIN orders o ON oi.OID = o.ID
     INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
     INNER JOIN orders_due_dates odd ON o.ID = odd.OID
     INNER JOIN order_item_del_date oidd ON oi.OID = oidd.OIID
     INNER JOIN prod_ref pr ON pr.ID = oi.PRID
     INNER JOIN product_type pt ON pt.ID = pr.PTID
     INNER JOIN division cd ON o.tDID = cd.ID
     INNER JOIN division sd ON o.fDID = sd.ID
     INNER JOIN section cs ON o.tSID = cs.ID
     INNER JOIN section ss ON o.tSID = ss.ID
     INNER JOIN associate_companies cac ON o.tACID = cac.ID
     INNER JOIN associate_companies sac ON o.fACID = sac.ID
WHERE o.ID = ?
ORDER BY OIIDd  DESC
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fOIgod</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $OID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $OID     = $row['OID'];
  $OIID    = $row['OIID'];
  $CPID    = $row['CPID'];
  $oref    = $row['oref'];
  $odel    = $row['orDate'];
  $OIIDd   = $row['OIIDd'];
  $OIIDnos = $row['OIIDnos'];
  $oiQty   = $row['oiQty'];
  $dueDate = $row['dueDate'];
  $ACIDC   = $row['ACIDC'];
  $ACIDnC  = $row['ACIDnC'];
  $DIDc   = $row['DIDC'];
  $SIDc   = $row['SIDC'];
  $DIDs   = $row['DIDS'];
  $SIDs   = $row['SIDS'];
  $DIDnC   = $row['DIDnC'];
  $ACIDS   = $row['ACIDS'];
  $ACIDnS  = $row['ACIDnS'];
  $DIDnS   = $row['DIDnS'];
  $samProd = $row['samProd'];
  $OTID    = $row['OTID'];
  $OIDcor  = $row['OIDcor'];
  $OIDtor  = $row['OIDtor'];
  $OIIDcr  = $row['OIIDcr'];
  $PRIDn   = $row['prRef'];
  $odel = date('d-M-Y', $odel);
}
