<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_count_order_thisweek.inc.php</b>";
$CID = $_SESSION['CID'];

// orders due this week
$OC = 0;
$sd = $td;
$ed = $td + 604800;
$dr = 'oidd.item_del_date';

// echo "count_orders - CID : $CID";
// echo "<br>count_orders - Order type : $OTID";
// echo "<br>count_orders - Order status : $OC";
// echo "<br>count_orders - today : $td";
// echo "<br>count_orders - start day : $sd";
// echo "<br>count_orders - end day : $ed";
// echo "<br>count_orders - due date source : $dr";

$sql = "SELECT COUNT(o.ID) as cOID
        FROM  company_partnerships cp
          , orders o
          , order_item oi
          , order_item_del_date oidd
        WHERE ((cp.req_CID = ?) OR (cp.acc_CID = ?))
        AND o.CPID = cp.ID
        AND oi.OID = o.ID
        AND oidd.OIID = oi.ID
        AND o.orComp = ?
        AND $dr BETWEEN $sd AND $ed
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fcgoctw</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $OC);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOIDtw = $row['cOID'];
}
