<?php
include 'dbconnect.inc.php';
// echo "<b>include/edit_user_details.inc.php</b>";

include 'set_urlPage.inc.php';
include 'from_UID_count_forgotten_pwd.inc.php';

$CID = $_SESSION['CID'];
$UID = $_SESSION['UID'];

if (isset($_GET['u'])) {$sUID = $_GET['u'];}else {$UID = $_SESSION['UID'];}

// echo "<br>Selected User ID is $sUID";
include 'include/from_sUID_get_user_details.inc.php';
include 'include/from_CID_count_company_division.inc.php';

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'Edit Personal User Information';
$pddhbc = '#fffddd';
// include 'page_description_date_header.inc.php';

$ACID = $_SESSION['ACID'];
if ($sUIDa == 1 ) {
  $sUIDa = "Pending";
}elseif ($sUIDa == 2) {
  $sUIDa = "Active";
}elseif ($sUIDa == 3) {
  $sUIDa = "Suspended";
}

if ($itemPref == 1) {
  $itemPref = "Sample";
  $ipCol = $samCol;
}else {
  $itemPref = "Production";
  $ipCol = $proCol;
}

if ($clientPref == 1) {
  $clientPref = "Associate";
  $cpCol = $assCol;
}else {
  $clientPref = "Partner";
  $cpCol = $parCol;
}

if ($salesPref == 1) {
  $salesPref = "Sales";
  $spCol = $salAssCol;
}else {
  $salesPref = "Purchase";
  $spCol = $purAssCol;
}
?>

<div class="overlay"></div>

<div style="position:absolute; top:20%; height:40%; left:5%; width:90%; border: 2px ridge grey; border-radius:10px; background-color:<?php echo $edtCol ?>; font-size:150%; font-weight:bold;">
  Edit Your Details
</div>

<!-- Original details -->
<div style="position:absolute; top:26%; left:5%; width:9%; border:none; font-size:110%; font-style: bold;font-size: 110%; font-style: bold; text-align:right;" ><?php echo "Firstname"; ?></div>
<div style="position:absolute; top:31%; left:5%; width:9%; border:none;font-size:110%; font-style: bold; text-align:right;" ><?php echo "Surname"; ?></div>
<div style="position:absolute; top:36%; left:5%; width:9%; border:none;font-size:110%; font-style: bold; text-align:right;" ><?php echo "Username"; ?></div>
<div style="position:absolute; top:41%; left:5%; width:9%; border:none;font-size:110%; font-style: bold; text-align:right;" ><?php echo "Initials"; ?></div>
<div style="position:absolute; top:46%; left:5%; width:9%; border:none;font-size:110%; font-style: bold; text-align:right;" ><?php echo "Country"; ?></div>
<div style="position:absolute; top:51%; left:5%; width:9%; border:none;font-size:110%; font-style: bold; text-align:right;" ><?php echo "Timezone"; ?></div>

<form action="include/edit_user_details_act.inc.php" method="POST">
  <input type="hidden" name="sUID" value="<?php echo "$sUID"; ?>">
  <input type="hidden" name="eUID" value="<?php echo "$sUID"; ?>">
  <input type="hidden" name="sDID" value="<?php echo "$sDID"; ?>">
  <input type="hidden" name="ACID" value="<?php echo "$ACID"; ?>">
  <input type="hidden" name="eDID" value="0">
  <input type="hidden" name="eUIDe" value="0">
  <input type="hidden" name="eUIDt" value="0">
  <input type="hidden" name="itemType" value="0">
  <input type="hidden" name="clientType" value="0">
  <input type="hidden" name="orderType" value="0">


  <!-- the new/revised data -->
  <input autofocus class="edit_user" type="text" style="top:25%; height:3.5%; left:15%; width:44%; font-size:110%;" name="eUIDf" placeholder="<?php echo "$sUIDf"; ?>" value="">
  <input class="edit_user" type="text" style="top:30%; height:3.5%; left:15%; width:44%; font-size:110%;;" name="eUIDs" placeholder="<?php echo "$sUIDs"; ?>" value="">
  <input class="edit_user" type="text" style="top:35%; height:3.5%; left:15%; width:44%; font-size:110%;" name="eUIDu" placeholder="<?php echo "$sUIDu"; ?>" value="">
  <input class="edit_user" type="text" style="top:40%; height:3.5%; left:15%; width:44%; font-size:110%;" name="eUIDi" placeholder="<?php echo "$sUIDi"; ?>" value="">
  <select style="position:absolute; top:45%; height:4%; left:15%; width:44.7%; text-align: right; padding-right:0.5%; background-color:pink;" name="TID">
    <option value="">Set the Country you are currently in</option>
    <?php
    include 'include/select_company_country_details.inc.php';
    ?>
  </select>
  <select style="position:absolute; top:50%; height:4%; left:15%; width:44.7%; text-align: right; padding-right:0.5%; background-color:pink;" name="TID">
    <option value="">Set the approriate Timezone</option>
    <?php
    include 'include/select_timeZones.inc.php';
    ?>
  </select>

  <!-- Personal Preferences -->
  <div style="position:absolute; top:26%; height:8%; left:61%; width:30%; border:thin solid black; font-weight:bold;">
    Current Preferences
  </div>
  <div style="position:absolute; top:28.5%; left:61%; width:10%; border:thin solid black; font-weight:bold;">
    Item
  </div>
  <div style="position:absolute; top:28.5%; left:71%; width:10%; border:thin solid black; font-weight:bold;">
    Client
  </div>
  <div style="position:absolute; top:28.5%; left:81%; width:10%; border:thin solid black; font-weight:bold;">
    Order
  </div>
  <div style="position:absolute; top:31.5%; left:61%; width:10%; border:thin solid black; font-weight:bold; background-color: <?php echo $ipCol ?>;">
    <?php echo $itemPref ?>
  </div>
  <div style="position:absolute; top:31.5%; left:71%; width:10%; border:thin solid black; font-weight:bold; background-color: <?php echo $cpCol ?>;">
    <?php echo $clientPref ?>
  </div>
  <div style="position:absolute; top:31.5%; left:81%; width:10%; border:thin solid black; font-weight:bold; background-color: <?php echo $spCol ?>;">
    <?php echo $salesPref ?>
  </div>


  <div style="position:absolute; top:36%; height:16%; left:61%; width:30%; border:thin solid black; font-weight:bold;">
    Order Status Perferences
  </div>
  <div style="position:absolute; top:40%; left:62%; width:10%; border:none; font-size: 110%; font-style: bold;font-size: 110%; font-style: bold; text-align:right;" >Item Type :</div>
  <div style="position:absolute; top:44%; left:62%; width:10%; border:none;font-size: 110%; font-style: bold; text-align:right;" >Client Type :</div>
  <div style="position:absolute; top:48%; left:62%; width:10%; border:none;font-size: 110%; font-style: bold; text-align:right;" >Order Type :</div>

  <div style="position:absolute; top:39.5%; left:73%;">
    <input type="radio" name="itemType" value="1">Sample
    <input type="radio" name="itemType" value="2">Production
  </div>

  <div style="position:absolute; top:43.5%; left:73%;">
    <input type="radio" name="clientType" value="1">Associate
    <input type="radio" name="clientType" value="2">Partner
  </div>

  <div style="position:absolute; top:47.5%; left:73%;">
    <input type="radio" name="orderType" value="1">Sales
    <input type="radio" name="orderType" value="2">Purchase
  </div>

  <?php
  if ($sUID <> $UID) {
    // echo "Can now change PRIVILEGES for the user and ACTIVE STATUS";
    ?>
    <div style="position:absolute; top:61%; left:5%; width:9%; border:none;font-size: 110%; font-style: bold; text-align:right;" ><?php echo "Position"; ?></div>
    <div style="position:absolute; top:66%; left:5%; width:9%; border:none;font-size: 110%; font-style: bold; text-align:right;" ><?php echo "e-mail"; ?></div>
    <input class="edit_user" type="text" style="top:60%; height:3.5%; left:15%; width:44%; font-size:110%;" name="eUIDp" placeholder="<?php echo "$sUIDp"; ?>eUID" value="0">
    <input class="edit_user" type="email" style="top:65%; height:3.5%; left:15%; width:44%; font-size:110%;" name="eUIDe" placeholder="<?php echo "$sUIDe"; ?>" value="">

    <div style="position:absolute; top:71%; left:5%; width:9%; border:none;font-size: 110%; font-style: bold; text-align:right;" ><?php echo "Status"; ?></div>
    <div style="position:absolute; top:76%; left:5%; width:9%; border:none;font-size: 110%; font-style: bold; text-align:right;" ><?php echo "Level"; ?></div>
    <select class="" style="position:absolute; top:70%; height:4%; left:15%; width:44.7%; text-align:center; background-color:pink; z-index:1; " name="userStatus">
      <option value="">Change User Status</option>
      <?php
      include 'include/select_user_status.inc.php';
      ?>
    </select>
    <select class="" style="position:absolute; top:75%; height:4%; left:15%; width:44.7%; text-align:center; background-color:pink; z-index:1; " name="userType">
      <option value="">Change User Privilege Level</option>
      <?php
      include 'include/from_CID_select_user_authority_level.inc.php';
      ?>
    </select>
    <?php
  }else {
    ?>
    <input type="hidden" name="eUIDp" value="0">
    <?php
  }
   ?>

  <button class="entbtn" style="position:absolute; top:55%; left:37.5%; width:10%; background-color:<?php echo $upRecCol ?>" type="submit" name="update_user">Update</button>
  <button class="entbtn" style="position:absolute; top:55%; left:52.5%; width:10%; background-color:<?php echo $fcnCol ?>" type="submit" name="update_user_cancel">Cancel</button>
</form>

<?php
if ($sUID <> $UID) {
  ?>
  <a style="color:blue; text-decoration:none;" href="company.php?C&ulu=<?php echo $sUID ?>">
    <div style="position:absolute; top: 85%;  left:40%; width:20%; padding-bottom: 0.5%; background-color: #76f481; border-radius: 10px; z-index:1; border:thin solid grey;padding-top:0.5%">
      UNLOCK USER</a>
    </div>
  </a>
  <?php
}elseif ($cFPUAID == 3) {
  // echo "<br>Able to change password questions";
}else {
  // echo "Password questions not yet set";
  ?>
  <a style="color:blue; text-decoration:none;" href="company.php?C&spq&u=<?php echo $UID ?>">
    <div style="position:absolute; top: 65%;  left:40%; width:20%; padding-bottom: 0.5%; background-color: #76f481; border-radius: 10px; z-index:1; border:thin solid grey;padding-top:0.5%">
      Set Password Questions</a>
    </div>
  </a>
  <?php
}
 ?>
