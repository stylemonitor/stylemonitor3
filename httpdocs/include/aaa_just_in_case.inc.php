<?php

// List of Companies, Assoc Companies, Divisions and their Partners
// Just copy and run in the db to see whats happening


SELECT cR.ID as company_ID
       , cR.name AS company
       , acR.ID AS assoc_ID
       , acR.name AS assoc_name
       , dR.id AS div_ID
       , IF(dR.name = 'Select Division', "-",dR.name) AS div_name
       , cA.ID as P_Co_ID
       , cA.name AS P_company
       , acA.ID AS P_assoc_ID
       , acA.name AS P_assoc_name
       , dA.id AS P_div_ID
       , IF(dA.name = 'Select Division', "-",dA.name) AS P_div_name
FROM company_partnerships cp
     INNER JOIN company cR ON cR.ID = cp.req_CID
     INNER JOIN company cA ON cA.ID = cp.acc_CID
     INNER JOIN company_type ct ON cp.rel = ct.ID
     INNER JOIN associate_companies acR ON acR.CID = cR.ID
     INNER JOIN associate_companies acA ON acA.CID = cA.ID
     INNER JOIN division dR ON acR.ID = dR.ACID
     INNER JOIN division dA ON acA.ID = dA.ACID
