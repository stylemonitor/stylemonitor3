<?php

include 'include/dbconnect.inc.php';

// $OID = $_GET['OID'];
$OIID = $_GET['oi'];

//              GET CLIENT ORDER DETAILS
$sql ="SELECT o.orNos as ref
        , o.orDate as odel
        , oidd.item_del_date as idel
        , oi.ord_item_nos as oin
        , oiq.order_qty as oiQty
        , ac.name as ACIDnC
        , d.name as DIDnC
        , oi.item_gen_desc as gdesc
        , pr.prod_ref as pref
        , oi.their_item_ref cref
        , pt.ID as PTID
        , (oiq.order_qty*(ac.order_tolerance/100)) as TOL
      FROM order_item oi
      	, order_item_del_date oidd
        , associate_companies ac
        , division d
        , orders o
        , order_item_qty oiq
        , prod_ref pr
        , product_type pt
      WHERE oi.ID = ?
      AND oi.OID =o.ID
      AND oidd.OIID = oi.ID
      AND d.ID = o.DID
	    AND ac.ID = d.ACID
	    AND oiq.OIID = oi.ID
      AND oi.PRID = pr.ID
      AND pr.PTID = pt.ID
      ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  // echo '<b>FAIL</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OIID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $ref = $row['ref'];
  $oin = $row['oin'];
  $ACIDnC = $row['ACIDnC'];
  $DIDnC = $row['DIDnC'];
  $idel = $row['idel'];
  $oiQty = $row['oiQty'];
  $pref = $row['pref'];
  $gdesc = $row['gdesc'];
  $cref = $row['cref'];
  $PTID = $row['PTID'];
  $tol = intval($row['TOL']);

  // GET SUPPLIER DETAILS
  $sql ="SELECT opm.id as id
          , ac.name as ACIDnS
          , d.name as DIDnS
          , s.name as SIDnS
          , opm.omQty as pQty
          , opm.OPID as mID
        FROM order_item oi
          , order_placed op
          , associate_companies ac
          , division d
          , section s
          , orders o
          , order_placed_move opm
          , order_item_movement_reason oimr
          , order_item_qty oiq
        WHERE oi.ID = ?
        AND oimr.ID = 2
        AND oi.OID = o.ID
        AND op.OIID = oi.ID
        AND s.DID = d.ID
        AND d.ACID = ac.ID
        AND opm.OPID = op.ID
        AND oiq.OIID = oi.ID
        AND oiq.OIMRID = oimr.ID;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)) {
  // echo '<b>FAILq</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OIID);
  mysqli_stmt_execute($stmt);
  while($result = mysqli_stmt_get_result($stmt)){
    $row = mysqli_fetch_assoc($result);
    $ACIDnS = $row['ACIDnS'];
    $DIDnS = $row['DIDnS'];
    $SIDnS = $row['SIDnS'];
    $pQty = $row['pQty'];
    }
  }
}
?>
