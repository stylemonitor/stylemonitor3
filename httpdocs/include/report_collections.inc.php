<?php
session_start();
// include 'dbconnect.inc.php';
include 'dbconnect.inc.php';
// echo "include/report_seasons.inc.php";
include 'include/from_CID_count_collection.inc.php';
$CID = $_SESSION['CID'];

// $cCLID = $cCLID - 1;
$td = date('U');

// set the return pages section
if (isset($_GET['col'])) {$head = 'col'; $rpp = 30;
}elseif (isset($_GET['cor'])) {$head = 'cor'; $rpp = 30;
}

// Check the URL to see what page we are on
if (isset($_GET['pa'])) { $pa = $_GET['pa'];
}else{ $pa = 0;
}

// Set the rows per page
// $rpp = 30;

// Check which page we are on
if ($pa > 1) { $start = ($pa * $rpp) - $rpp;
}else { $start = 0;
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cSNID / $rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV);

if (isset($_GET['cl'])) {
  $CLID = $_GET['cl'];

  $sql = "SELECT cl.ID as CLID
            , cl.name as CLIDn
          FROM collection cl
          WHERE cl.ID = ?
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-rc</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $CLID);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_assoc($res);
    $CLID = $row['CLID'];
    $CLIDn = $row['CLIDn'];
  }
  ?>
  <form class="" style="position:absolute; top:50%; height:10%; left:0%; width:100%; z-index:1;" action="include/form_revise_collection_act.inc.php" method="POST">
    <div style="position:absolute; top:-30%; height:30%; left:0%; width:100%; font-weight: bold; text-align:center;">
      Amend Collection Name
    </div>
    <input type="hidden" name="CLID" value="<?php echo $CLID ?>">
    <input type="hidden" name="CLIDn" value="<?php echo $CLIDn ?>">
    <div style="position:absolute; bottom:22%; height:50%; left:0%; width:23%; text-align:right;">
      <?php echo $CLIDn ?>
    </div>
    <input class="input_data" type="text" style="position:absolute; bottom:22%; height:50%; left:25%; width:30%; text-align:left; text-indent:5%;" type="text" name="coll" value="">
    <button class="entbtn "type="submit" style="bottom:35%; height:40%; left:60%; width:10%;" name="revName">Update</button>
    <button class="entbtn "type="submit" style="bottom:35%; height:40%; left:75%; width:10%;" name="canName">Cancel</button>
  </form>
  <?php
}

// Set the rows per page
$rpp = 10;

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
// $PTV = ($cPTID/$rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;
// echo '<br>Start record is : '.$start;
// echo "<br>**** count_orders count : $cPTID ****";
?>
<?php
if ($cCLID == 0) {
  $td = date('d M Y',$td);
  $pddh = 'Create a Collection';
  $pddhbc = '#fffddd';
  include 'page_description_date_header.inc.php';
  ?>

  <br><br><br>
  <br><br><br>
  <br><br><br>
  <p style="font-size:200%;">You can add a COLLECTION whenever you want</p>
  <br>
  <p style="font-size:200%;">When you have, you need to then EDIT your Styles accordingly</p>
  <br>
  <p style="font-size:200%;">You do not need to have any Collections for the system to work</p>
  <?php

}else {
  // $cPTID = $cPTID - 1;
  if ($cCLID == 1) {
    $coltitle = 'collection';
  }else {
    $coltitle = 'collections';
  }
  $td = date('d M Y',$td);
  $pddh = 'Collection Register';
  $pddhbc = '#fffddd';
  include 'page_description_date_header.inc.php';
  ?>

  <table class="trs" style="position:relative; top:12%;">
    <tr>
      <th style="width:32%; text-align:left; text-indent:2%;">Collection</th>
      <th style="width:16%;">Count Styles</th>
      <th style="width:16%;">Count Clients</th>
      <th style="width:16%;">Count Orders</th>
      <th style="width:6%;">Details</th>
      <th style="width:6%;">Edit</th>
      <th style="width:8%;">Created</th>
    </tr>
    <?php

    $sql = "SELECT cl.ID as CLID
              , cl.name as CLIDn
              , cl.inputtime as input
            FROM collection cl
              , associate_companies ac
              , company c
            WHERE cl.ACID = ?
            AND cl.ACID = ac.ID
            AND ac.CID = c.ID
            AND cl.name NOT IN ('Select Collection')
            ORDER BY cl.name
            LIMIT $start, $rpp
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-rc</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "s", $ACID);
      mysqli_stmt_execute($stmt);
      $res = mysqli_stmt_get_result($stmt);
      $sort == DESC ? $sort = ASC : $sort = DESC;

      while($row = mysqli_fetch_array($res)){
        $CLID = $row['CLID'];
        $CLIDn = $row['CLIDn'];
        $input = $row['input'];

        $input = date('d-M-Y',$input);
        ?>
        <tr>
          <td style="text-align:left; text-indent:3%;"><?php echo $CLIDn ?></td>
          <td style="border-left:thin solid grey;">$cCLcli</td>
          <td style="border-left:thin solid grey;">$cCLord</td>
          <td style="border-left:thin solid grey;">$cCLsty</td>
          <td style="background-color:pink;"><a style="text-decoration:none;" href="styles.php?S&cor&cl=<?php echo $CLID ?>">Open</a></td>
          <td style="background-color:yellow;"><a style="text-decoration:none;" href="styles.php?S&cor&cl=<?php echo $CLID ?>">Edit</a></td>
          <td><?php echo $input ?></td>
        </tr>
        <?php
      }
    }
    ?>
    <tr>
      <td style="border-top:2px solid black;"></td>
      <td style="border-top:2px solid black;"></td>
      <td style="border-top:2px solid black;"></td>
      <td style="border-top:2px solid black;"></td>
      <td style="border-top:2px solid black;"></td>
      <td style="border-top:2px solid black;"></td>
      <td style="border-top:2px solid black;"></td>
    </tr>
  </table>
<?php }

if (isset($_GET['cor'])) {
  ?>
  <div style="position:absolute; bottom:10%; right:5%;">
    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a style='font-size:200%; text-decoration:none;' href='?S&$head&pa=$x'>  $x  </a>";
    }
    ?>
  </div>
  <?php
}else {
  ?>
  <div style="position:absolute; top:65%; right:5%;">
    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a style='font-size:200%; text-decoration:none;' href='?S&$head&pa=$x'>  $x  </a>";
    }
    ?>
  </div>
  <?php
}
?>
