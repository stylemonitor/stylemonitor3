<?php
session_start();
include 'dbconnect.inc.php';

// get the next item number
$sql = "SELECT DATE_FORMAT(CURDATE(),'%d-%m-%Y') AS TODAY
       , YEARWEEK(CURDATE() - 7) AS LAST_WEEK
       , YEARWEEK(CURDATE()) AS THIS_WEEK
       , YEARWEEK(CURDATE() + 7) AS NEXT_WEEK
       , YEARWEEK(CURDATE() + 14) AS TWO_WEEKS
;";
$result = mysqli_query($con, $sql);
$row = mysqli_fetch_assoc($result);
$today = $row['TODAY'];
$LAST_WEEK = $row['LAST_WEEK'];
$THIS_WEEK = $row['THIS_WEEK'];
$NEXT_WEEK = $row['NEXT_WEEK'];
$TWO_WEEKS = $row['TWO_WEEKS'];

echo "Last weeks number is $LAST_WEEK";
echo "<br>This weeks number is $THIS_WEEK";
echo "<br>Next weeks number is $LAST_WEEK";
echo "<br>Later number is $TWO_WEEKS";
