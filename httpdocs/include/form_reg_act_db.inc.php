<?php
include 'dbconnect.inc.php';
// echo "<br><b>form_reg_act_db.inc.php</b>";
// the working of the file form_action_reg.php
// ADD the user to the users table

// echo "<br>Recommended company CID is $rec_CID";

$abvfirst = substr($UIDf,0,1);
$abvsur = substr($UIDs,0,2);
// echo '<br>User Abv first: <b>'.$abvfirst.'</b>';
// echo '<br>User Abv sur : <b>'.$abvsur.'</b>';

$UIDi = strtoupper($abvfirst.$abvsur);
// echo "<br>User initials UIDi : <b>$UIDi</b>";

// mysqli_begin_transaction($mysqli);
// try {
  // to
  include 'input_user_details_first.inc.php';

  // GET the rec_CID ID number
  if ($rec_CID == 1) {
  }else {
    $sql = "SELECT ID
            FROM company
            WHERE SMIC = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-frad2</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "s", $rec_CID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $rec_CID = $row['ID'];
    }
  }

  // echo '<br><br><b>UPDATE uid AND UID in the users table (81)</b>';

  // GET the users new ID as $UID
  $sql = "SELECT ID as UID
          FROM users
          WHERE email = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL2</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UIDe);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $UID = $row['UID'];
  }

  // echo "<br>User ID (UID) : <b>$UID</b>";
  $UIDu = strtoupper($abvfirst.'-'.$UID.'-'.$abvsur);
  // echo '<br>User uid code : <b>'.$UIDu.'</b>';

  // UPDATE the NEWLY registered user with the uid and initials of the user
  include 'update_user_uid.inc.php';

  // echo "<br>Add the company information into the company table";
  // echo "<br>Company name CIDn : <b>$CIDn</b>";
  // echo "<br>Company country UIDy: <b>$CYID</b>";
  // echo "<br>Recommending company ID rec_CID : <b>$rec_CID</b>";

  include 'input_user_company_details.inc.php';

  // GET the company ID as $CID
  $sql = "SELECT ID as CID
          FROM company
          WHERE UID = ?;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad6</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $CID = $row['CID'];
  }

  // echo '<br>Company ID (CID) : <b>'.$CID.'</b>';

  // Add the licence type to the company_licence table
  $sql = "INSERT INTO company_licence
            (CID, LTID, UID, st_date)
          VALUES
            (?,1,?,?);";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad7</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $CID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // echo "<br>CID = $CID; CTID = $CTID; CYID = $CYID; TID = $TID; UID = $UID; CIDn = $CIDn; td = $td";

  // Add the associate company
  $sql = "INSERT INTO associate_companies
            (CID, CTID, CYID, TID, UID, name, inputtime)
          VALUES
            (?,?,?,?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-ccs4</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sssssss",$CID, $CTID, $CYID, $TZID, $UID, $CIDn, $td);
    mysqli_stmt_execute($stmt);
  }

  // Add a division
  $sql = "SELECT ID as ACID
          FROM associate_companies
          WHERE UID = ?
          AND inputtime = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-frad7</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $UID, $td);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $ACID = $row['ACID'];
  }
  // echo "<br>ACID :: $ACID";

  include 'input_associate_company_division.inc.php';
  include 'input_associate_company_section.inc.php';
  include 'create_company_product_types.inc.php';
  // create a 'self' partnership
  include 'input_company_partnership.inc.php';
  include 'input_company_division_user.inc.php';
  // Add addresses to company/associate company/division/section
  include 'input_address_details_HQ.inc.php';

  // echo "<br>*************WORKING UP TO HERE**********************************************************************";

// // } catch (mysqli_sql_exception $exception) {
// //   mysqli_rollback($mysqli);
// //
// //   throw $exception;
// // }
//
include 'email_reg.inc.php';
header("Location:../interest.php?1&f=$UIDf&c=$CIDn");
exit();
