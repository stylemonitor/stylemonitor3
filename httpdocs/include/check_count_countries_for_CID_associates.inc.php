<?php
include 'dbconnect.inc.php';
// echo  "<br><b>include/check_count_countries_for_CID_associates.inc.php</b>";

$CID = $_SESSION['CID'];

// echo  "<br>ACID : $ACID";

$sql = "SELECT COUNT(CYID) as cCYID
        FROM associate_companies
        WHERE CID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-ceu";
}else {
  mysqli_stmt_bind_param($stmt,"s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cCYID  = $row['cCYID'];
}

// echo "<br>Count of associate companies (cCYID) = $cCYID";

if ($cCYID == 1) {
  $sql = "SELECT CYID as CYID
            , TID as TZID
          FROM associate_companies
          WHERE CID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt,$sql)) {
    echo "FAIL-ceu";
  }else {
    mysqli_stmt_bind_param($stmt,"s", $CID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_assoc($result);
    $CYID  = $row['CYID'];
    $TZID  = $row['TZID'];
  }
}
// echo "<br>Country ID (CYID) = $CYID";
// echo "<br>Country timezone ID (TZID) = $TZID";
