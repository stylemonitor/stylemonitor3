<?php
include 'dbconnect.inc.php';
$sm = '<b>S</b>tyle<b>M</b>onitor';

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'How to ... Create a STYLE';
$pddhbc = '#e08df4';
include 'page_description_date_header.inc.php';

?>
<div class="" style="position:absolute; top:9%; height:97%; left:0%; width:100%; background-color:#f4f4f4;">
  <br>
  <p>Before you can add an <b>ORDER</b> to follow within <?php echo $sm ?>, you need to have a <b>STYLE</b> to order.</p>
  <br>
  <p>There are only 2 bits of information you really have to add.</p>
  <br>
  <p>Firstly a short <b>Reference</b>, ie 123456 or JKT1, of upto 10 characters.</p>
  <br>
  <p>Then a <b>Brief Description</b>, ie Mens Oxford Trouser, of upto 60 characters</p>
  <br>
  <br>
  <p>Later, when you are more confident about using <?php echo $sm ?> you can add select from the a drop-down list but you have to add them first.</p>
  <br>
  <p>* * * <b>Product Types</b> * * *</p>
  <br>
  <p>* * * <b>Divisions</b> * * *</p>
  <br>
  <p>* * * <b>Sections</b> * * *</p>
  <br>
  <p>* * * <b>Seasons</b> * * *</p>
  <br>
  <p>* * * <b>Collection</b> * * *</p>
  <br>
  <br>
  <p>These will only appear when you have a choice to make.  You'll need to ADD them in the relevant section first.</p>
  <br>
  <br>
  <p>You can add an <b>ETM</b> (estimated time to make) as well, but again, it's not necessary</p>
  <br>
  <p>If you don't, don't worry the system won't crash, but it will add 1 for you.</p>
  <br>
  <p>The Review takes you to a report of ALL the styles you have.</p>
  <br>

  <?php
  if (isset($_GET['S'])) {
    ?>
    <p><a href="how_to.php?S&htp"><b>Product Type</b></a></p>
    <?php
  }else {
    ?>
    <p><a href="how_to.php?W&htp"><b>Product Type</b></a></p>
    <?php
  }
  ?>
</div>
