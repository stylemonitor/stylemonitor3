<?php
// echo "<br><b>include/check_prod_ref_valid.inc.php</b>";
// echo "<br>Original short code = $PRIDr :: New short code = $nPRIDr";

if ($PRIDr <> $nPRIDr) {
  // check if short code in use
  $sql = "SELECT ID as cPRID
            , prod_ref as cPRIDr
            , prod_desc as cPRIDd
          FROM prod_ref
          WHERE ACID = ?
          AND prod_ref = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-cprv</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $ACID, $nPRIDr);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_assoc($result);
    $cPRID = $row['cPRID'];
    $cPRIDr = $row['cPRIDr'];
    $cPRIDd = $row['cPRIDd'];
  }

  // echo "<br>Original PRID for item with this PRIDr = $cPRID";

  $oPRIDsc = "$cPRIDr / $cPRIDd";

  if (!empty($cPRID)) {
    // echo "<br>Product Ref short code in use";
    header("Location:../styles.php?S&sne&s=$PRID&apt=$oPRIDsc&d=$nPRIDd");
    exit("product reference in use");
  }else {
    // update the system
    // echo "<br>New product reference go to update_style.inc.php";
    include 'update_style.inc.php';
  }
}
