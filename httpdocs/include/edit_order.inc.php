<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/edit_order.inc.php";

// get the order details from the oi.ID
include 'from_OIID_get_order_details.inc.php';

// _________________________________________________________________________
// Check if it is an INTERNAL order or an EXTERNAL order
if (isset($_GET['oi'])) {
  $OID = $_GET['oi'];
}

// is the order internal or external
$sql = "SELECT o.CPID as CPID
          , cp.req_CID as req_CID
          , cp.acc_CID as acc_CID
        FROM orders o
          , order_item oi
          , company_partnerships cp
        WHERE oi.ID = ?
        AND oi.OID = o.ID
        AND o.CPID = cp.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-eoa7</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OIID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $req_CID = $row['req_CID'];
  $acc_CID = $row['acc_CID'];
}

if ($req_CID == $acc_CID) {
  $req_UID = $acc_UID;
  echo "This is an INTERNAL order being changed";
  $OIDcor = $OIDcor;
}else {
  echo "This is a PARTNERSHIP order being changed";
  if ($req_CID == $CID) {
    echo "<br>We are the CLIENT";
  }else {
    echo "<br>We are the SUPPLIER";
  }
}
// _____________________________________________________________

echo "Order Details";
echo "<br>Order Nos : $oref:$OIIDnos";
echo "<br>Company name : $ACIDnC";
echo "<br>Order Delivery Date : $dueDate";
echo "<br>Item Delivery Date : $OIIDd";
echo "<br>OUR ORDER Reference : $OIDcor";
echo "<br>THEIR ORDER Reference : $OIDtor";
echo "<br>Their Item Reference : $OIIDcr";
echo "<br>Order Qty : $oiQty";

$dueDate = date('d-M-Y', $dueDate);
$OIIDd   = date('d-M-Y', $OIIDd);

?>
<form style="position:absolute; top:24%; height:50%; left:5%; width:90%; background-color:pink;" action="include/edit_order_act.inc.php" method="post">
  <div style="position:absolute; top:1%; height:8%; left:10%; width:28%;">Original Information</div>
  <div style="position:absolute; top:10%; height:8%; left:10%; width:28%;">Order Nos</div>
  <input type="hidden" name="OID" value="<?php echo $OID ?>"style="position:absolute; top:10%; height:8%; left:25%; width:28%;">
  <input type="hidden" name="OIID" value="<?php echo $OIID ?>"style="position:absolute; top:10%; height:8%; left:25%; width:28%;">
  <div style="position:absolute; top:10%; height:8%; left:25%; width:28%;"><?php echo $oref ?> : <?php echo $OIIDnos ?></div>
  <div style="position:absolute; top:19%; height:8%; left:10%; width:28%;">Company Name</div>
  <div style="position:absolute; top:19%; height:8%; left:25%; width:28%;"><?php echo $ACIDnC ?></div>
  <div style="position:absolute; top:28%; height:8%; left:2%; width:28%;">Order Due Date</div>
  <input type="text" name="dueDate" style="position:absolute; top:25%; height:8%; left:25%; width:28%;" value="<?php echo $dueDate ?>">
  <div style="position:absolute; top:37%; height:8%; left:2%; width:28%;">Order Item Due Date</div>
  <input type="text" name="OIIDd" style="position:absolute; top:34%; height:8%; left:25%; width:28%;" value="<?php echo $OIIDd ?>">
  <div style="position:absolute; top:46%; height:8%; left:2%; width:28%;">Our Order Reference</div>
  <input type="text" name="OIDcor" style="position:absolute; top:43%; height:8%; left:25%; width:28%;" value="<?php echo $OIDcor ?>">
  <div style="position:absolute; top:55%; height:8%; left:2%; width:28%;">Item Reference</div>
  <input type="text" name="OIIDcr" style="position:absolute; top:52%; height:8%; left:25%; width:28%;" value="<?php echo $OIIDcr ?>">
  <div style="position:absolute; top:64%; height:8%; left:2%; width:28%;">Order Quantity</div>
  <input type="text" name="oiQty" style="position:absolute; top:61%; height:8%; left:25%; width:28%;" value="<?php echo $oiQty ?>">
  <div style="position:absolute; top:73%; height:8%; left:10%; width:28%;">Their Order Reference</div>
  <div style="position:absolute; top:73%; height:8%; left:25%; width:28%;"><?php echo $OIDtor ?></div>


  <div style="position:absolute; top:1%; height:8%; left:55%; width:28%;">Revised Information</div>
  <!-- <input type="text" style="position:absolute; top:10%; height:8%; left:55%; width:28%;" name="" value=""> -->
  <!-- <input type="text" style="position:absolute; top:19%; height:8%; left:55%; width:28%;" name="" value=""> -->
  <input type="date" style="position:absolute; top:25%; height:8%; left:55%; width:28%;" name="udate" value="">
  <input type="date" style="position:absolute; top:34%; height:8%; left:55%; width:28%;" name="uitemdate" value="">
  <input type="text" style="position:absolute; top:43%; height:8%; left:55%; width:28%;" name="uoref" value="">
  <input type="text" style="position:absolute; top:52%; height:8%; left:55%; width:28%;" name="uiref" value="">
  <input type="text" style="position:absolute; top:61%; height:8%; left:55%; width:28%;" name="uqty" value="">
  <!-- udate = updated date -->
  <!-- uoref = updated our order ref -->
  <!-- uiref = updated item ref -->
  <!-- uqty = updated quantity -->

  <button type="submit" style="position:absolute; bottom:2%; height:6%; left:45%; width:10%;" name="ed_ord">UPDATE</button>

</form>

<?php
