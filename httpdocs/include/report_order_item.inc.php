<?php
include 'dbconnect.inc.php';
// echo "include/report_order_item.inc.php";

include 'set_urlPage.inc.php';

if (isset($_GET['orp'])) {
  $OID = $_GET['orp'];
}
// if (isset($_GET['odr'])){
//   $tb = 50;
// }else {
//   $tb = 50;
// }
$tb = 55;
?>

<table class="trs" style="position:absolute; top:<?php echo $tb ?>%; font-family:monospace;">
  <tr>
    <th style="width:3%;">Item</th>
    <th style="width:27%; text-align:left; text-indent:2%;">Our Style Ref</th>
    <th style="width:27%; text-align:left; text-indent:2%;">Item Description</th>
    <th style="width:27%; text-align:left; text-indent:2%;">Their Item Description</th>
    <!-- <th style="width:5%; text-align:left; text-indent:2%;">Type</th> -->
    <th style="width:6%;">Qty</th>
    <th style="width:10%;text-align:right; padding-right:2%;">Due Date</th>
  </tr>
    <?php
    $sql = "SELECT oi.ID as OIID
              , oi.ord_item_nos as itemNos
              , oi.PRID as PRID
              , oi.samProd as type
              , oi.their_item_ref as their_ref
              , oi.pItemRef as pItemRef
              , pr.prod_ref as PRIDr
              , pr.prod_desc as PRIDd
              , pt.name as PTIDn
              , oiq.order_qty as qty
              , oidd.item_del_date as delDate
            FROM order_item oi
              , orders o
              , order_item_qty oiq
              , order_item_del_date oidd
              , prod_ref pr
              , product_type pt
            WHERE o.ID = ?
            AND oi.OID = o.ID
            AND oiq.OIID = oi.ID
            AND oidd.OIID = oi.ID
            AND oi.PRID = pr.ID
            AND pr.PTID = pt.ID
            ORDER BY oidd.item_del_date, oi.ord_item_nos
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-roi';
    }else {
      mysqli_stmt_bind_param($stmt, "s", $OID);
      mysqli_stmt_execute($stmt);
      $res = mysqli_stmt_get_result($stmt);
      while($row = mysqli_fetch_array($res)){
        $OIID      = $row['OIID'];
        $itemNos   = $row['itemNos'];
        $PTIDn      = $row['PTIDn'];
        $PRID      = $row['PRID'];
        $PRIDr     = $row['PRIDr'];
        $PRIDd     = $row['PRIDd'];
        $pItemRef  = $row['pItemRef'];
        $their_ref = $row['their_ref'];
        $sp        = $row['type'];
        $qty       = $row['qty'];
        $OIIDdelDate   = $row['delDate'];

        if ($sp == 1) {
          $col = $samCol;
          $type = 'Samp';
        }elseif ($sp == 2) {
          $col = $proCol;
          $type = 'Prod';
        }

        $OIIDdelDate = date('d-M-Y', $OIIDdelDate);
        $ref = "$PRIDr - $PRIDd";

        ?>
        <td style="background-color:<?php echo $col ?>;"><?php echo $itemNos ?></td>
        <?php
        if (strlen($ref) > 28) {
          $ref = substr($ref,0,28)
          ?>
          <td style="text-align:left;"><?php echo "$ref ..." ?></td>
          <?php
        }else {
          ?>
          <td style="text-align:left;"><?php echo "$ref" ?></td>
          <?php
        }
        ?>
        <?php
        if (strlen($their_ref) > 28) {
          $their_ref = substr($their_ref,0, 28);
          ?>
          <td style="text-align:left;"><?php echo $their_ref ?> ...</td>
          <?php
        }else {
          ?>
          <td style="text-align:left;"><?php echo $their_ref ?></td>
          <?php
        }
        if (strlen($pItemRef) > 28) {
          $pItemRef = substr($pItemRef,0, 28);
          ?>
          <td style="text-align:left;"><?php echo $pItemRef ?> ...</td>
          <?php
        }else {
          ?>
          <td style="text-align:left;"><?php echo $pItemRef ?></td>
          <?php
        }
        ?>
        <td style="text-align:right; padding-right:1%;"><?php echo $qty ?></td>
        <td style="text-align:right; padding-right:.5%;"><?php echo $OIIDdelDate ?></td>
      </tr>
        <?php
      }
    }
    ?>
</table>
