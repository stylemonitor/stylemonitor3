  <?php
include 'dbconnect.inc.php';
$CID = $_SESSION['CID'];
// echo "<b>include/home.inc.php</b>";

// Setthe report date
$td = date('U');
$MD = date('d-M-Y', $td);

if (isset($_GET['home'])) {
  include 'include/order_status_summary.inc.php';
}elseif (isset($_GET['order'])) {
  // echo "Get the orders sorted by Order Number";
  include 'include/ORDER_REPORT_by_order_number.inc.php';
}elseif (isset($_GET['company'])) {
  // echo "Get the orders sorted by Company";
  include 'include/ORDER_REPORT_by_company.inc.php';
}elseif (isset($_GET['style'])) {
  // echo "Get the orders sorted by Style";
  // include 'include/ORDER_REPORT_by_company.inc.php';
  include 'include/ORDER_REPORT_by_style.inc.php';
}elseif (isset($_GET['OI'])) {
  $OIID = $_GET['OI'];
  include 'REPORT_ORDER_header.inc.php';
  include 'report_order_movement.inc.php';
  if (isset($_GET['eit'])) {
    $eit = $_GET['eit'];
    if ($eit==0) {
      include 'form_edit_order_item_details.inc.php';
    }elseif ($eit == 1) {
      include 'form_edit_item_cancel.inc.php';
    }elseif ($eit == 2) {
      include 'form_edit_item_change_type.inc.php';
    }elseif ($eit == 3) {
      if (!isset($_GET['cty'])) {
        include 'form_edit_item_qty.inc.php';
      }else {
        include 'form_edit_item_qty_confirm.inc.php';
      }
    }elseif ($eit == 4) {
      if (!isset($_GET['cte'])) {
        include 'form_edit_item_delDate.inc.php';
      }else {
        include 'form_edit_item_delDate_confirm.inc.php';
      }
    }
  }elseif (isset($_GET['eir'])) {
    include 'report_edit_item_movement.inc.php';
  }elseif (isset($_GET['oiy']) ||(isset($_GET['oin'])) ) {
    include 'form_edit_item_partner_acc_rej.inc.php';
  }
}elseif (isset($_GET['o'])) {
  $OID = $_GET['o'];
  include 'REPORT_ORDER_header.inc.php';
  include 'report_order_by_item.inc.php';
}elseif (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  if (($tab == 0) || ($tab == 1) || ($tab == 2) || ($tab == 3) || ($tab == 4)) {
    include 'include/report_ORDER_STATUS_due_date.inc.php';
    if (isset($_GET['tabh'])) {
      include 'help/report_ORDER_STATUS_due_date.help.inc.php';
    }
  }elseif ($tab == 5) {
    include 'include/order_report_recent.inc.php';
  }elseif ($tab == 6) {
    if (isset($_GET['id'])) {
      $ACID = $_GET['id'];
      include 'order_report_COMPANY.inc.php';
    }
  }elseif ($tab == 99) {
    // echo "SHOE THE HOW TO PAGE";
    include 'how_to_home.inc.php';
  }
}
if (isset($_GET['ppr'])) {
  include 'input_partners_PRID.inc.php';
}

if (isset($_GET['mp'])) {
  include 'form_item_movement_update.inc.php';
}

if (isset($_GET['prq'])) {
  include 'outstanding_change_requests.inc.php';
}
