<?php
include 'dbconnect.inc.php';
// echo "<br><b>from_UID_get_company_details.inc.php</b>";
// $UID = $_SESSION['UID'];
// echo " :: UID = $UID";

$sql = "SELECT c.ID as CID
          , c.name as CIDn
          , c.SMIC as SMIC
          , u.uid as uid
          , c.CYID as CYID
          , c.TID as TID
          , cl.cTrial as CLIDc
          , cl.cUsers as cCIDu
          , cl.st_date as clstd
          , cy.name as CYIDn
          , lt.ID as LTID
          , lt.type LTIDt
          , lt.timelimit as lttl
          , cdu.ID as CDUID
          , ac.ID as ACID
          , d.ID as DID
          FROM users u
          , company c
          , division d
          , associate_companies ac
          , company_division_user cdu
          , company_licence cl
          , country cy
          , licence_type lt
        WHERE u.ID = ?
        AND cdu.UID = u.ID
        AND cdu.DID = d.ID
        AND d.ACID = ac.ID
        AND ac.CID = c.ID
        AND cl.CID = c.ID
        AND cl.LTID = lt.ID
        AND c.CYID = cy.ID
        ORDER BY cl.id DESC LIMIT 1
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fugcd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $UID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  if($row = mysqli_fetch_assoc($result)){
    $CID    = $row['CID'];
    $CIDn   = $row['CIDn'];
    $SMIC   = $row['SMIC'];
    $uid    = $row['uid'];
    $cCIDu  = $row['cCIDu'];
    $CLIDc = $row['CLIDc'];
    $clLTID = $row['LTID'];
    $LTIDt  = $row['LTIDt'];
    $clstd  = $row['clstd'];
    $lttl   = $row['lttl'];
    $CYID  = $row['CYID'];
    $TID    = $row['TID'];
    $CYIDn  = $row['CYIDn'];
    $CDUID  = $row['CDUID'];
    $ACID   = $row['ACID'];
    $DID    = $row['DID'];
  }
}
