<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_section_loading_status_summary.inc.php";

$CID = $_SESSION['CID'];

include 'set_urlPage.inc.php';
include 'from_UID_get_last_logout.inc.php'; // $LRIDout
include 'ot_sp_selection_movement.inc.php';

$td = date('U');

if (isset($_GET['H'])) {
  $hbc = '#b7d8ff';
  $LtabTitle = 'Order Status';
}elseif (isset($_GET['L'])) {
  $hbc = '#ffffd1';
  $LtabTitle = 'Factory Loading';
}

// sets the order type
if (isset($_GET['ot'])) {
  $ot = $_GET['ot'];
}

// sets the sample/production type
if (isset($_GET['sp'])) {
  $sp - $_GET['sp'];
}

if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  if (($tab == 0) || ($tab == 9)) {
    $pddh = 'Order Summarytytytyty : <b>All Dates</b>';
    $pddhbc = '#b7d8ff';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td - 31536000;
    $ed = $td + 31536000;
  }elseif ($tab == 1) {
    $pddh = 'Order Item Summary : OVERDUE';
    $pddhbc = '#fc4141';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td - 31536000;
    $ed = $td;
  }elseif ($tab == 2) {
    $pddh = 'Order Summary Item : THIS WEEK';
    $pddhbc = '#f7a0a0';
    $dr = 'oidd.item_del_date';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td;
    $ed = $td + 604800;
  }elseif ($tab == 3) {
    $pddh = 'Order Summary Item : NEXT WEEK';
    $pddhbc = '#ecf5a9';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td + 604800;
    $ed = $td + 1209600;
  }elseif ($tab == 4) {
    $pddh = 'Order Summary Item : AFTER TWO WEEKS';
    $pddhbc = '#cef8d3';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td + 1209600;
    $ed = $td + 31536000;
  }elseif ($tab == 9) {
    $pddh = 'Order Summary All Dates';
    $pddhbc = '#ffffd1';
    $OC = 0;
    $dr = 'oidd.item_del_date';
    $sd = $td - 31536000;
    $ed = $td + 31536000;
  }
}
include 'page_description_date_header.inc.php';
include 'page_selection_header.inc.php';
include 'order_QUERY_count.inc.php';

include 'from_CID_count_section_totals.inc.php';

?>
<table style="position:absolute; top:18%; left:3%; width:94%;">
  <tr>
    <th colspan="2"><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=0">Awaiting</a></th>
    <th colspan="2"><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=1">MP1</a></th>
    <th colspan="2"><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=2">MP2</a></th>
    <th colspan="2"><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=3">MP3</a></th>
    <th colspan="2"><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=4">MP4</a></th>
    <th colspan="2"><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=5">MP5</a></th>
    <th colspan="2"><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=6">Warehouse</a></th>
  </tr>
  <tr>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
    <th style="width:3%;">Orders</th>
    <th style="width:10%;">Qty</th>
  </tr>

  <tr>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=0"><?php echo $cORst_awt ?></a></td>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=0"><?php echo $AWAITING ?></a></td>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=1"><?php echo $cORst1 ?></a></td>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=1"><?php echo $stotal1 ?></a></td>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=2"><?php echo $cORst2 ?></a></td>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=2"><?php echo $stotal2 ?></a></td>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=3"><?php echo $cORst3 ?></a></td>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=3"><?php echo $stotal3 ?></a></td>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=4"><?php echo $cORst4 ?></a></td>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=4"><?php echo $stotal4 ?></a></td>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=5"><?php echo $cORst5 ?></a></td>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=5"><?php echo $stotal5 ?></a></td>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=6"><?php echo $cORwhs ?></a></td>
    <td><a style="color:blue; text-decoration:none;" href="loading.php?L&tab=6"><?php echo $WAREHOUSE ?></a></td>
  </tr>
</table>
<?php
