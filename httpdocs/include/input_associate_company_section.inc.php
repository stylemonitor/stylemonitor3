<?php
// echo "<br><b>input_associate_company_section.inc.php</b>";

$sql = "INSERT INTO section
          (DID, UID, inputtime)
        VALUES
          (?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-ccs4</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $DID, $UID, $td);
  mysqli_stmt_execute($stmt);
}
$sql = "SELECT ID as SID
        FROM section
        WHERE UID = ?
        AND inputtime = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-frad7</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $SID = $row['SID'];
}
// echo "<br>SID :: $SID";
