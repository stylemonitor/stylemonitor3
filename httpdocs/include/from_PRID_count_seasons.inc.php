<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_PRID_count_seasons.inc.php</b>";
$sql = "SELECT COUNT(sn.ID) as cSNID
        FROM prod_ref pr
          , prod_ref_to_season prts
          , season sn
        WHERE pr.ID = ?
        AND prts.PRID = pr.ID
        AND prts.SNID = sn.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fprs</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $PRID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $cSNID   = $row['cSNID'];
}
