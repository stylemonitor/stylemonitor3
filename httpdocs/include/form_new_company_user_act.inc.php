<?php
// echo "<br><b>form_new_company_user_act.inc.php</b>";

if (!isset($_POST['new_user']) && !isset($_POST['can_new_user']) && !isset($_POST['can_new_user1'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['can_new_user'])) {
  // echo "<br>Return to the Users page";
  header("Location:../company.php?C&coi");
  exit();
}elseif (isset($_POST['can_new_user1'])) {
  // echo "<br>Return to the Users page";
  header("Location:../company.php?C&usp");
  exit();
}else {
  include 'dbconnect.inc.php';
  // echo "<br>Register the new user";

  $NEWUSER = 2;
  $UID  = $_SESSION['UID'];
  $CID  = $_SESSION['CID'];
  $UIDe = $_SESSION['UIDe'];
  $td   = date('U');

  // include 'from_CID_count_users.inc.php';
  // include 'from_UID_get_company_details.inc.php';
  // include 'from_CID_get_min_division.inc.php';

  // echo '<br> Access OK';

  // access from ?
  $ACID  = $_POST['ACID'];
  $UIDf  = $_POST['first'];
  $UIDs  = $_POST['sur'];
  $UIDo  = $_POST['job_title'];
  $UIDe = $_POST['email'];
  $userType = $_POST['userType'];
  $DID   = $_POST['DID'];
  $CYID = $_POST['CYID'];
  $TZID = $_POST['TZID'];
  $regUser = 1;

  $abvfirst = substr($UIDf,0,1);
  $abvsur = substr($UIDs,0,2);
  // echo '<br>User Abv first: <b>'.$abvfirst.'</b>';
  // echo '<br>User Abv sur : <b>'.$abvsur.'</b>';
  $UIDi = strtoupper($abvfirst.$abvsur);

  // echo '<br>Country ID ($CYID) : '.$CYID;
  // echo '<br>TimeZone ID ($TZID) : '.$TZID;
  // echo '<br>User first name ($UIDf) : '.$UIDf;
  // echo '<br>User surname ($UIDs) : '.$UIDs;
  // echo '<br>User initials are set at : '.$UIDi;
  // echo '<br>User level ($userType) : '.$userType;
  // echo '<br>User email ID ($UIDe) : '.$UIDe;

  include 'set_vkey.inc.php';

  // echo '<br>Inputuser ID ($UID) : '.$UID;
  // echo '<br>Inputtime ($td) : '.$td;
  // echo '<br>Position ($UIDo) : '.$UIDo;
  // echo '<br>Associate company ID ($ACID) : '.$ACID;
  // echo '<br>User division ID ($DID) : '.$DID;

  include 'check_smauthorised_user.inc.php';

  $td = date('U');

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to

    // CHECK if the email provided is already in use
    $sql = "SELECT u.ID as UID
    FROM users u
    WHERE u.email = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b><br>FAIL-fncua1</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "s", $UIDe);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $uUID = $row['UID'];
    }

    if ($uUID > 0) {
      // echo "<br>email already in use";
      // header("Location:../company.php?C&usc&1&f=$UIDf&s=$UIDs&o=$UIDo");
      // exit();
    }else {
      // echo "<br>email <b>NOT</b> in use by anyone on the system";

      $sql = "INSERT INTO users
                (CYID, TID, firstname, surname, userInitial, email, privileges, vKey, inputuserID, inputtime)
                VALUES
                  (?,?,?,?,?,?,?,?,?,?)
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo "<br><b>FAIL-fncua01</b>";
      }else {
        mysqli_stmt_bind_param($stmt, "ssssssssss", $CYID, $TZID, $UIDf, $UIDs, $UIDi, $UIDe, $userType, $vkey, $UID, $td);
        mysqli_stmt_execute($stmt) ;
      }

      // echo '<br><br><b>UPDATE uid AND UID in the users table (81)</b>';
      // GET the users new ID as $UID
      $sql = "SELECT ID as ID
              FROM users
              WHERE email = ?
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b><br>FAIL-fncua3</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "s", $UIDe);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        $row = mysqli_fetch_array($result);
        $nUID = $row['ID'];
      }
      // echo "<br>New users ID is $nUID";
      // echo '<br>The Users ID is set as : <b>'.$UID.'</b>';
      $UIDu = strtoupper($abvfirst.'-'.$nUID.'-'.$abvsur);
      // echo '<br>User uid code : <b>'.$UIDu.'</b>';

      include 'update_NEW_user_uid.inc.php';




      // echo "<br>DID = $DID :: UID - $nUID :: cUID == $UID :: position == $UIDo :: td = $td";
      // ADD the user as the company user
      $sql = "INSERT INTO company_division_user
                (DID, UID, cUID, position, inputtime)
              VALUES
                (?,?,?,?,?)
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b><br>FAIL-fncua7</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "sssss", $DID, $nUID, $UID, $UIDo, $td);
        mysqli_stmt_execute($stmt);
      }
    }
  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }
}
