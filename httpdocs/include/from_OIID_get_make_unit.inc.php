<?php
include 'dbconnect.inc.php';
// echo "<br><b>include/from_OIID_get_make_unit.inc.php</b>";

if (isset($_GET['oi'])) {
  $OIID = $_GET['oi'];
}

$sql = "SELECT op.ID as OPID
        FROM order_placed op
          , order_item oi
        WHERE oi.ID = ?
        AND op.OIID = oi.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-focmu</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OIID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $OPID = $row['OPID'];
}
