<div class="notes">
  <div class="cpsty" style="background-color:#d4c50d">
    <b>REGISTER</b> - Notes
  </div>
  <br>
  <p>Now you have registered some Product types and some Styles, you can start adding orders</p>
  <br>
  <p>Your company already has 1 Client and 1 Supplier - YOU</p>
  <br>
  <p>But you might want to add more, so you can under the Client/Supplier tab</p>
  <br>
  <p>This is the QUICK add page, there is a FULL add page which includes addresses, but you don't really need them just now</p>
  <br>
  <p>There is just one piece of information you need to add, the company name</p>
  <br>
  <p>The first two we have created, with the last one being your Product type list</p>
  <br>
  <p>When you add an order, either a Sales order OR a Purchase order<p>
  <br>
  <p>You need to select from the drop-down menu's the following information</p>
  <br>
  <p>Client/Supplier name</p>
  <br>
  <p>Product being ordered</p>
  <br>
  <p>The dates you can leave blank, the system will fill them in if you don't</p>
  <br>
  <p>For the Delivery date, you need to have a LEAD TIME set</p>
  <br>
  <p>Initially it is 91 days, but you can change that in 'Your company name' - EDIT</p>
  <br>
  <p>Well you now have the 'basic' covered, everything else should just follow!!!</p>
  <br>
  <p>If it doesn't try thinking about what you want to do and look in the tab on the left</p>
  <br>
  <p>One thing, wherever the mouse changes to a pointed finger, the system will give you more information</p>
</div>
