<?php
include 'dbconnect.inc.php';
// echo "<br><b>include/check_company_licence_validity.inc.php</b>";

$sql = "SELECT lt.timelimit as LTIDt
          , cl.st_date as CLIDstd
          , c.ID as CID
        FROM company c
          , company_licence cl
          , licence_type lt
          , users u
        WHERE u.ID = ?
        AND c.UID = u.ID
        AND cl.CID = c.ID
        AND cl.LTID = lt.ID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-cclv</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $UID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  if($row = mysqli_fetch_assoc($result)){
  $CID = $row['CID'];
  $CLIDstd = $row['CLIDstd'];
  $LTIDt = $row['LTIDt'];
  }
}
// echo "<br>Company start date (CLIDstd) is : $CLIDstd";
// echo "<br>Licence time limit (LTIDt) : $LTIDt";
// echo "<br>Todays date is (td) : $td";
$expire = ($LTIDt + $CLIDstd);
// echo "<br>Expiry date is (expire) : $expire";

if ($td > $expire) {
  echo "<br>Company Licence has expired";
  header("Location:../index.php?LXP=$UID");
  exit();
}else {
  // echo "<br>Company Licence is VALID";
  // echo "<br>Valid User UID == $UID";
  // LOG the user in
  $sql = "INSERT INTO login_registry
            (UID, logintime, logouttime)
          VALUES
            (?,?,'0')
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL1</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  include 'set_session_variables.inc.php';
}
