<?php
include 'dbconnect.inc.php';

// echo "review_associate_company_product_types.inc.php"
if (isset($_GET['id'])) {
  $ACID = $_GET['id'];
}

?>
<div class="overlay"></div>

<div class="cpsty" style="position:absolute; top:31%; left:0%; width:100%; background-color:<?php echo $hbc ?>; padding-top:0.2%;"><a style="color:<?php echo $hc ?>; text-decoration:none;" href="home.php?H&aot">Product Type Review</a>
</div>
<table class="trs" style="position:absolute; top:35%; left:30%; width:40%; z-index:1;">
  <tr>
    <th>Product type</th>
    <th>Short Code</th>
    <th>Orders</th>
    <th>Place Order</th>
  </tr>
<?php

// get the information
$sql = "SELECT pt.ID as PTID
, pt.name as PTIDn
, pt.scode as PTIDsc
        FROM associate_company_product_types acpt
          , product_type pt
        WHERE acpt.ACID = ?
        AND acpt.PTID = pt.ID
        AND pt.name NOT IN ('Select Product Type')
        ORDER BY pt.name
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcspt</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $ACID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($res)){
    $PTID = $row['PTID'];
    $PTIDn = $row['PTIDn'];
    $PTIDsc = $row['PTIDsc'];
    ?>
    <tr>
      <td><?php echo "$PTIDn" ?></td>
      <td><?php echo "$PTIDsc" ?></td>
      <td></td>
      <td><a href="styles.php?S&orn&sal=1&aci=<?php echo $ACID ?>">Order</a></td>
    </tr>

    <?php
  }
  ?>
</table>
<?php
}
