<?php
include 'dbconnect.inc.php';
// echo '<b>include/form_reg_act.inc.php</b>';

if (!isset($_POST['reg']) AND !isset($_POST['reg_can'])) {
  // echo "<br>INCORRECT ACCESS METHOD";
  header('Location:../index.php');
  exit();
}elseif (isset($_POST['reg_can'])) {
  // echo "<br>Cancel Registration";
   header("Location:../index.php");
   exit();
}elseif (isset($_POST['reg'])) {
  // echo "<br>Correct register method used";

  // GET the info from the form
  $UIDf = $_POST['first'];
  $UIDs = $_POST['sur'];
  $UIDc = $_POST['company'];
  $CTID = $_POST['CTID'];
  $UIDn = $_POST['position'];
  $UIDe = $_POST['email'];
  $UIDy = $_POST['country'];
  $TID = $_POST['TID'];
  $rec_CID = $_POST['rec_CID'];
  // data set when the enter button is used
  $vkey = password_hash((time()), PASSWORD_DEFAULT);
  $td = date('U');

  $ans          = $_POST['check_ans'];
  $ab1          = $_POST['ab1'];
  $ab2          = $_POST['ab2'];

  $ab3 = $ab1 + $ab2;

  // GET the rec_CID ID number
  if (empty($rec_CID)) {
    $rec_CID = 1;
  }else {
    $sql = "SELECT ID
            FROM company
            WHERE SMIC = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-frad2</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "s", $rec_CID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $rec_CID = $row['ID'];
    }
  }

  // echo '<br><br><b>Register Interest form information (22)</b>';
  // echo '<br>User first name (UIDf) : <b>'.$UIDf.'</b>';
  // echo '<br>User surname (UIDs) : <b>'.$UIDs.'</b>';
  // echo '<br>User email (UIDe) : <b>'.$UIDe.'</b>';
  // echo '<br>User verification code name (vkey) : <b>'.$vkey.'</b>';
  // echo '<br>Todays date (td)<b>'.$td.'</b>';
  // echo '<br>User company name (UIDc) : <b>'.$UIDc.'</b>';
  // echo '<br>Company type (CTID) : <b>'.$CTID.'</b>';
  // echo '<br>User position name (UIDn) : <b>'.$UIDn.'</b>';
  // echo '<br>User country ID (UIDy) : <b>'.$UIDy.'</b>';
  // echo '<br>TimeZone ID (TID) : <b>'.$TID.'</b>';
  // echo '<br>Recommended by (rec_CID): <b>'.$rec_CID.'</b>';

  // Check if they have got hte right maths answer
  if ($ans <> $ab3) {
    // echo "<br>Maths value incorrect ans/ab3 = $ans::$ab3";
    header("Location:../index.php?i&SM&fn=$UIDf&s=$UIDs&co=$UIDc&p=$UIDn&e=$UIDe");
    exit();
  }else {
    echo "<br>Maths value correct ans/ab3 = $ans::$ab3";
    echo "Check a count of the use of teh email";

    // mysqli_begin_transaction($mysqli);
    // try {
      // to
      include 'check_smauthorised_user.inc.php';
      // if (($UIDe == 'jrellieye@gmail.com') || ($UIDe == 'hlellieye@gmail.com') || ($UIDe == 'hlellieye@googlemail.com') || ($UIDe == 'desandy135@outlook.com') || ($UIDe == 'staythorpe@aol.com') || ($UIDe == 'lesandandy.leighton@gmail.com')) {
      //   // Get a count of the times jrellieye@gmail.com has been used
      //   $sql = "SELECT COUNT(email) AS cemail
      //           FROM users
      //           WHERE email LIKE ?
      //   ;";
      //   $stmt = mysqli_stmt_init($con);
      //   if (!mysqli_stmt_prepare($stmt, $sql)){
      //     echo '<b>FAIL-fnpa</b>';
      //   }else{
      //     mysqli_stmt_bind_param($stmt, "s", $UIDe);
      //     mysqli_stmt_execute($stmt);
      //   }
      //   $res = mysqli_stmt_get_result($stmt);
      //   $row = mysqli_fetch_array($res);
      //   $cemail = $row['cemail'];
      //   echo "email count = $cemail";
      // }

      if ($cemail <> 0) {
        // Change the email address last used
        // Get the last ID that has used jrell...
        $sql = "SELECT MAX(ID) AS mID
        FROM users
        WHERE email LIKE ?
        ;";
        $stmt = mysqli_stmt_init($con);
        if (!mysqli_stmt_prepare($stmt, $sql)){
          echo '<b>FAIL-fnpa</b>';
        }else{
          mysqli_stmt_bind_param($stmt, "s", $UIDe);
          mysqli_stmt_execute($stmt);
          $res = mysqli_stmt_get_result($stmt);
          $row = mysqli_fetch_array($res);
          $mID = $row['mID'];
        }
        // echo "<br>Last User ID = $mID and the last email was $cemail";
        $cemail = $cemail + 1;
        $changedEmail = "$UIDe$cemail";
        // echo "<br>This has now changed to $changedEmail";

        // Change it by adding 1 to the end
        $sql = "UPDATE users
        SET email = ?
        WHERE ID = ?
        ;";
        $stmt = mysqli_stmt_init($con);
        if(!mysqli_stmt_prepare($stmt, $sql)){
          echo '<b>FAIL-fasocn</b>';
        }else{
          mysqli_stmt_bind_param($stmt, "ss", $changedEmail, $mID,);
          mysqli_stmt_execute($stmt);
        }
echo "Multiple user email now register with email";

      }else {
        // continue to register in the normal manner
      // }
      //
      //
      // }else {
        // echo "<br>Go with new user as the Email not been used before";

        // NOW add my next user
        // include 'from_UIDe_get_user_details.php';
        include 'from_UIDc_get_company_details.php';
        include 'from_UIDc_get_company_admin.inc.php';

        // 1
        // CHECK if the country has been set
        // if($UIDy == 1){
        //   // echo "<br>No country has been selected";
        //   header("Location:../interest.php?f=$UIDf&s=$UIDs&c=$UIDc&p=$UIDn&e=$UIDe");
        //   exit();
        // }else{


        // echo "<br>A country has been selected";

        // 2
        // CHECK a valid email has been used
        // if(filter_var($UIDe, FILTER_VALIDATE_EMAIL)){
        //   // echo '<br>An <b>invalid email</b> has been used';
        //   header("Location:../interest.php?f=$UIDf&s=$UIDs&c=$UIDc&p=$UIDn");
        //   exit('you need to enter a valid email address');
        // }else{
        //   echo "<br>A valid email has been used";
        // }

        // echo "<br><b>Check if the email is already in use</b>";
        // echo "<br>User UID (UID): $UID";
        // echo "<br>User active (UIDa): $UIDa";
        // echo "<br>User privilege level (UIDv): $UIDv";

        if ($UID == 0 ) {
          // echo "<br>The email is <b>NOT</b> registered";

          // A
          // CHECK if the company is known to us
          // IS the company registered
          if (empty($CID)) {
            // echo "<br>The company IS not known";
            // echo "<br>Company CID is $CID";
            // echo "<br>Company country ID is $CYID";
            // echo "<br><b>*** 1 ***</b>";
            include 'form_reg_act_db.inc.php';
            header("Location:../interest.php?1&f=$UIDf&c=$UIDc");
            exit();
          }else {
            // echo "<br>The company <b>IS</b> known";
            // echo "<br>Company CID is $CID";
            // echo "<br>Company country ID is $CYID";

            // 7
            // Check the licence is valid
            if ($td > ($clstd + $lttl)) {
              // echo "<br>The licence has expired";
              // echo "<br>Company Admin User ID : $caUID";
              // echo "<br><b>*** 2 ***</b>";
              header("Location:../interest.php?2&f=$UIDf&c=$UIDc&a=$caUID");
              exit();
            }else {
              // echo "<br>The licence is <b>current</b>";

              // 8
              // CHECK that the companys country registered is the same as the input info
              if ($UIDy == $CYID) {
                // echo "<br>Form country : $UIDy";
                // echo "<br>Registered country : $CYID";
                // echo "<br>Company Admin User ID : $caUID";
                // echo "<br>The country matches that registered";
                // echo "<br><b>*** 4 ***</b>";
                header("Location:../interest.php?4&f=$UIDf&a=$caUID");
                exit();
              }else {
                // echo "<br>Form country : $UIDy";
                // echo "<br>Registered country : $CYID";
                // echo "<br>Company Admin User ID : $caUID";
                // echo "<br>This is a <b>NEW</b> country for the company";// code...
                // echo "<br><b>*** 3 ***</b>";
                header("Location:../interest.php?3&f=$UIDf&c=$UIDc&a=$caUID");
                exit();
              }
            }
          }
        }else {
          // echo "<br>The email is registered";

          // 5
          // IS the user active $UIDa <> 0
          if (($UIDa == 1) || ($UIDa == 2)) {
            // echo "<br>The user is active";

            // 6
            // IS the company registered
            if (empty($CID)) {
              // echo "<br>The company IS not known";
              // echo "<br>Company CID is $CID";
              // echo "<br>Company country ID is $CYID";
              // echo "<br><b>*** 6 ***</b>";
              header("Location:../interest.php?6&f=$UIDf&c=$UIDc&a=$cUID");
              exit();
            }else {
              // echo "<br>The company <b>IS</b> known";
              // echo "<br>Company CID is $CID";
              // echo "<br>Company country ID is $CYID";

              // 7
              // Check the licence is valid
              if ($td > ($clstd + $lttl)) {
                // echo "<br>The licence has expired";
                // echo "<br>Company Admin User ID : $caUID";
                // echo "<br><b>*** 7 ***</b>";
                header("Location:../interest.php?7&f=$UIDf&c=$UIDc&ca=$caUID");
                exit();
              }else {
                // echo "<br>The licence is <b>current</b>";

                // 8
                // CHECK that the companys country registered is the same as the input info
                if ($UIDy == $CYID) {
                  // echo "<br>Form country : $UIDy";
                  // echo "<br>Registered country : $CYID";
                  // echo "<br>Company Admin User ID : $caUID";
                  // echo "<br>The country matches that registered";
                  // echo "<br><b>*** 9 ***</b>";
                  header("Location:../interest.php?9&f=$UIDf&a=$caUID");
                  exit();
                }else {
                  // echo "<br>Form country : $UIDy";
                  // echo "<br>Registered country : $CYID";
                  // echo "<br>Company Admin User ID : $caUID";
                  // echo "<br>This is a <b>NEW</b> country for the company";// code...
                  // echo "<br><b>*** 8 ***</b>";
                  header("Location:../interest.php?8&f=$UIDf&c=$UIDc&caUID");
                  exit();
                }
              }
            }
          }elseif ($UIDa == 3) {
            // echo "<br>The user is suspended";
            // echo "<br>Company Admin User ID : $caUID";
            // echo "<br><b>*** 5 ***</b>";
            header("Location:../interest.php?5&f=$UIDf&c=$UIDc&a=$caUID");
            exit();
          }
        }
        // }
      }
    }
  }
