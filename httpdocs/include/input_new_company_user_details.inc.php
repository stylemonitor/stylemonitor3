<?php
session_start();
include 'dbconnect.inc.php';
// echo  "<br><b>input_new_company_user_details.inc.php</b>";

$UIDin = $_SESSION['UID'];
// echo "<br>UIDin = $UIDin";
// get CYID from company
include 'from_UID_get_company_details.inc.php';
// echo "<br>CYID : $CYID :: TID : $TID";
// set initials
$abvfirst = substr($UIDf,0,1);
$abvsur = substr($UIDs,0,2);
// echo '<br>User Abv first: <b>'.$abvfirst.'</b>';
// echo '<br>User Abv sur : <b>'.$abvsur.'</b>';

$UIDi = strtoupper($abvfirst.$abvsur);
// echo "<br>User initials UIDi : <b>$UIDi</b>";

include 'set_vkey.inc.php';

// $status = 1;

$sql = "INSERT INTO users
          (CYID, TID, firstname, surname, userInitial, email, vKey, inputtime)
        VALUES
          (?,?,?,?,?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudf";
}else {
  mysqli_stmt_bind_param($stmt,"ssssssss", $CYID, $TZID, $UIDf, $UIDs, $UIDi, $UIDe, $vkey, $td);
  mysqli_stmt_execute($stmt);
}

$sql = "SELECT id as UID
        FROM users
        WHERE vKey = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudf2";
}else {
  mysqli_stmt_bind_param($stmt,"s", $vkey);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $UID = $row['UID'];
}

$UIDu = strtoupper($abvfirst.'-'.$UID.'-'.$abvsur);
// echo "<br>User uid code : $UIDu :: Inputuser : $UIDin :: UID : $UID";

include 'update_user_uid.inc.php';

// echo "<br>DID = $DID: UID = $UID: UIDin = $UIDin: UIDo = $UIDo: td = $td";

$sql = "INSERT INTO company_division_user
          (DID, UID, cUID, position, inputtime)
        VALUES
          (?,?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudf4";
}else {
  mysqli_stmt_bind_param($stmt,"sssss", $DID, $UID, $UIDin, $UIDo, $td);
  mysqli_stmt_execute($stmt);
}

// Get division id
$sql = "SELECT id as DID
        FROM company_division_user
        WHERE UID = ?
        AND inputtime = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-iudf5";
}else {
  mysqli_stmt_bind_param($stmt,"ss", $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $DID = $row['DID'];
}
include ('email_new_user.inc.php');
header("Location:../company.php?C&usp");
exit();
