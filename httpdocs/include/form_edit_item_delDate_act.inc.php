<?php
session_start();
include 'dbconnect.inc.php';
$UID = $_SESSION['UID'];
$CID = $_SESSION['CID'];
$td = date('U');
echo "<br><b>form_edit_item_delDate_act.inc.php</b>";

if (!isset($_POST['YesBtn']) && !isset($_POST['NoBtn'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['NoBtn'])) {
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  header("Location:../home.php?H&o=$OID&eit=$OIID");
  exit();
}elseif (isset($_POST['YesBtn'])) {
  echo "<br>Change the item delivery date";
  $oicCID = $_POST['oicCID'];
  $OID = $_POST['OID'];
  $OPID = $_POST['OPID'];
  $OIID = $_POST['OIID'];
  $OTID = $_POST['OTID'];
  $OIDDIDd = $_POST['OIDDIDd'];
  $canReas = $_POST['canReas'];
  $Ndate = $_POST['Ndate'];
  $td = date('U');

  $Ndate = strtotime($Ndate,0);

  if (empty($Ndate)) {
    $Ndate = $OIDDIDd;
  }else {
    $Ndate = $Ndate;
  }

  $OIMRID = 40;
  $qty = 0;

  echo "<br>OTID = $OTID";
  if (($OTID == 1) || ($OTID == 2)) {
    // del date request
    // include 'action/insert_into_order_place_move.act.php';

    // del date APPROVAL
    $OIMRID = 39;
    $td = $td+1;
    // include 'action/insert_into_order_place_move.act.php';

    // echo "<br>This is an ASSOCIATE order and does not need partner approval";
    $accCode = 'Associate Order Approved Change';

    // add to the log_order_item_del_date the original date etc
    // echo "<br>log_order_item_del_date :: $OIID, $OIDDIDd, $Ndate, $UID, $canReas, $accCode, $UID, $accCode, $td, $td";
    $sql = "INSERT INTO log_order_item_del_date
              (OIID, item_del_date, new_item_dd, req_UID, note, reqKey, UID, acc_UID, accTime, inputtime)
            VALUES
              (?,?,?,?,?,?,?,?,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-feida1</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssssssssss", $OIID, $OIIDd, $Ndate, $UID, $canReas, $accCode, $UID, $accCode, $td, $td);
      mysqli_stmt_execute($stmt);
    }


    // add the new dateto the order_item_del_date table
    // echo "<br>order_item_del_date :: $Ndate, $OIID";
    $sql = "UPDATE order_item_del_date
            SET item_del_date = ?
            WHERE OIID = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-feida2</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $Ndate, $OIID);
      mysqli_stmt_execute($stmt);
    }

    // add the reason to the order_placed_move table like a good boy!
    // echo "<br>order_placed_move :: $OPID, $UID, $canReas, $td";
    $sql = "INSERT INTO order_placed_move
              (OPID, OIMRID, UID, omQty, type, inputtime)
            VALUES
              (?,39,?,0,?,?)
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-feida1</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $OPID, $UID, $canReas, $td);
      mysqli_stmt_execute($stmt);
    }

    header("Location:../home.php?H&rt3&o=$OID&OI=$OIID");
    exit();
  }elseif (($OTID == 3) || ($OTID == 4)) {
    // go to the page relating to PARTNERS
    include 'form_edit_item_delDate_partner_act.inc.php';
  }
}
