<?php
session_start();
include 'dbconnect.inc.php';
// echo '<b>include/report_style_division_act.inc.php</b>';
if (!isset($_POST['adddiv'])){
  // echo 'Incorrect method used';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['adddiv'])){
  // echo 'Add a division to the style';

  $PRID = $_POST['PRID'];
  $DID = $_POST['DID'];
  $UID = $_SESSION['UID'];
  $td = date('U');

  // echo "PRID is $PRID";
  // echo "<br>DID is $DID";

  if (empty($DID)) {
    header('Location:../styles.php?S&sis');
    exit();
  }else {
    // Check that the divisioin is not already listed
    $sql = "SELECT prd.id as PRDID
            FROM prod_ref_to_div prd
              , prod_ref pr
              , division d
            WHERE pr.ID = ?
            AND d.ID = ?
            AND prd.PRID = pr.ID
            AND prd.DID = d.ID
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-rsd</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $PRID, $DID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_assoc($result);
      $PRDID = $row['PRDID'];
    }
    // ADD if not alreaDy listed
    if ($PRDID <> 0) {
      // echo "<br>The style/division has been set";
    }else {
      $sql = "INSERT INTO prod_ref_to_div
                (PRID, DID, UID, inputtime)
              VALUES
                (?,?,?,?)
              ;";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b>FAIL-rsda2<b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ssss", $PRID, $DID, $UID, $td);
        mysqli_stmt_execute($stmt);
      }

    }
  }
  header("Location:../styles.php?S&sis&s=$PRID");
  exit();
}
