<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_count_order_open.inc.php</b>";
$CID = $_SESSION['CID'];

// CHECK if a PRID has been set
$sql = "SELECT COUNT(o.ID) as cOID
        FROM  company_partnerships cp
          , orders o
        WHERE ((cp.req_CID = ?) OR (cp.acc_CID = ?))
        AND o.CPID = cp.ID;
";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fccoob</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cORIDop = $row['cOID'];
}
