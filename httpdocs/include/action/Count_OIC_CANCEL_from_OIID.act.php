<?php
// echo "<br><b>action/Count_OIC_CANCEL_from_OIID.act.php</b>";

$sql = "SELECT COUNT(oic.ID) as cOICID
          , oic.status as OICIDs
        FROM order_item_change oic
          , order_placed_move opm
          , order_placed op
        WHERE oic.OPMID = opm.ID
        AND opm.OIMRID = 45
        AND opm.OPID = op.ID
        AND oic.status = 0
        AND op.OIID = ?;
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-feidpa</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OIID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOICID = $row['cOICID'];
}
