<?php
session_start();
// echo "<br><b>action/select_UID_privilege.act.php</b>";
$UID = $_SESSION['UID'];
// echo "<br>$UID";

$sql = "SELECT privileges as UIDp
        FROM users
        WHERE ID = ?
        AND active = 2;
";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccacp2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $UID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $UIDp = $row['UIDp'];
}
