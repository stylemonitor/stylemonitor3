<?php
echo "<br><b>action/select_CID_for_user.act.php</b>";
$UID = $_SESSION['UID'];
// echo "<br>$UID";

$sql = "SELECT c.ID as CID
FROM company c
, company_division_user cdu
, division d
, associate_companies ac
, users u
WHERE u.ID = ?
AND cdu.UID = u.ID
AND cdu.DID = d.ID
AND d.ACID = ac.ID
AND ac.CID = c.ID;
";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccacp2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $UID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $reqCID = $row['CID'];
}
