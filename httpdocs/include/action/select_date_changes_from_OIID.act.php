<?php
// echo "<br><b>action/select_date_changes_from_OIID.act.php</b>";
$UID = $_SESSION['UID'];
// echo "<br>$UID";

$sql = "SELECT oidd.item_del_date as OIDDd
          , oic.Ndate as OICIDd
          FROM order_item oi
          , order_item_del_date oidd
          , order_item_change oic
          , order_placed op
          , order_placed_move opm
        WHERE oi.ID = ?
        AND oidd.OIID = oi.ID
        AND oic.OPMID = opm.ID
        AND opm.OPID = op.ID
        AND op.OIID = oi.ID;
";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccacp2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OIID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $OIDDd = $row['OIDDd'];
  $OICIDd = $row['OICIDd'];
}
