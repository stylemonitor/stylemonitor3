<?php
// echo "<br><b>action/select_OPID_from_OIID.act.php</b>";

$UID = $_SESSION['UID'];
// echo "<br>$UID";

$sql = "SELECT ID as OPID
        FROM order_placed
        WHERE OIID = ?;
";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccacp2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OIID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $OPID = $row['OPID'];
}
