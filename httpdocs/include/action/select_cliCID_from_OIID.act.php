<?php
echo "<br><b>action/select_cliCID_from_OIID.act.php</b>";

$sql = "SELECT o.tCID as cliCID
        FROM order_item oi
          , orders o
        WHERE oi.ID = ?
        AND oi.OID = o.ID;
";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccacp2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OIID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cliCID = $row['cliCID'];
}
echo "cliCID=$cliCID :: CID = $CID :: ";
