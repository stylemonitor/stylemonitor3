<?php
// echo "<br><b>action/select_CIDs_from_CPID.act.php</b>";

$sql = "SELECT fCID as cliCID
          , tCID as supCID
        FROM orders
        WHERE ID = ?;
";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fccacp2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cliCID = $row['cliCID'];
  $supCID = $row['supCID'];
}
