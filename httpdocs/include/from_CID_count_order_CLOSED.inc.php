<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_count_order_CLOSED.inc.php</b>";
$CID = $_SESSION['CID'];

// echo "count_orders - CID : $CID";
// echo "<br>count_orders - Order type : $OTID";
// echo "<br>count_orders - Order status : $OC";
// echo "<br>count_orders - today : $td";
// echo "<br>count_orders - start day : $sd";
// echo "<br>count_orders - end day : $ed";
// echo "<br>count_orders - due date source : $dr";

$sql = "SELECT COUNT(ID) as cOID
        FROM orders
        WHERE orComp = 1
        AND (tCID = ? OR fCID = ?)
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fcgocn</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOIDclosed = $row['cOID'];
}
