<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_division.inc.php";

if (isset($_GET['s'])) {
  $PRID = $_GET['s'];
}
if (isset($_GET['d'])) {
  $DID = $_GET['d'];
}
?>

<form class="" style="position:absolute; top:10%; height:4%; left:2%; width:30%;" action="include/report_style_division_act.inc.php" method="post">
  <input type="hidden" name="PRID" value="<?php echo $PRID ?>">
  <select style="position:absolute; top:6%; height:80%; left:3%; width:50%;" name="DID">
    <?php
      include 'from_ACID_select_division_dd.inc.php';
    ?>
  </select>
  <button class="entbtn" style="position:absolute; top:6%; height:80%; left:60%; width:35%;" type="submit" name="adddiv">Add Division</button>
</form>

<table class="trs" style="position:absolute; top:15%; left:2%; width:30%;">
  <tr>
    <th>Style Name</th>
    <th></th>
  </tr>
  <?php
  $sql = "SELECT d.ID as DID
            , pr.ID as PRID
            , pr.prod_ref as PRIDsc
            , pr.prod_desc as PRIDn
          FROM division d
            , prod_ref_to_div prd
            , prod_ref pr
          WHERE d.ID = ?
          AND prd.DID = d.ID
          AND prd.PRID = pr.ID
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-rsd</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $DID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      $DID = $row['DID'];
      $DIDn = $row['DIDn'];
      $PRIDn = $row['PRIDn'];
      ?>
      <tr>
        <td><?php echo $DID ?></td>
        <td><?php echo $DIDn ?></td>
        <td><?php echo $PRIDn ?></td>
      </tr>
      <?php
    }
  }
  ?>
</table>

<div style="position:absolute; bottom:5%; right:5%;">
    <?php for ($x = 1 ; $x <= $PTV ; $x++){
    echo "<a style='font-size:200%; text-decoration:none;' href='?C&$head&pa=$x'>  $x  </a>";
  }
  ?>
</div>

<?php
