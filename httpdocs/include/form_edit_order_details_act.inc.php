<?php
include 'dbconnect.inc.php';
$UID = $_SESSION['UID'];
$td = date('U');
// echo "<b>include/form_new_company_user_act.inc.php</b>";

if (!isset($_POST['save']) && !isset($_POST['cancel'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['cancel'])) {
  $OID = $_POST['OID'];
  $urlPage = $_POST['urlPage'];
  $tab = $_POST['tab'];

  // echo "<br>Return to the Users page";
  header("Location:../$urlPage&tab=$tab&o=$OID");
  exit();
}elseif (isset($_POST['save'])) {
  include 'dbconnect.inc.php';
  $OID = $_POST['OID'];

  include 'from_OID_get_order_details.inc.php';
  $ofACID   = $row['ACIDC'];
  $ofDID   = $DIDc;
  $ofSID   = $SIDc;
  $otACID   = $row['ACIDS'];
  $otDID   = $DIDs;
  $otSID   = $SIDs;
  $oOIDcor  = $row['OIDcor'];
  $oOIDtor  = $row['OIDtor'];

  // echo "Order ID = $OID";
  // echo "<br>ORIGINAL INFORMATION";
  // echo "<br>From ACID = $ofACID";
  // echo "<br>FROM DID = $ofDID";
  // echo "<br>From SID = $ofSID";
  // echo "<br>To ACID = $otACID";
  // echo "<br>To DID = $otDID";
  // echo "<br>To SID = $otSID";



// From the form
  $OID = $_POST['OID'];
  $urlPage = $_POST['urlPage'];
  $tab = $_POST['tab'];
  $fACID = $_POST['fACID'];
  $fDID = $_POST['fDID'];
  // $fSID = $_POST['fSID'];
  $tACID = $_POST['tACID'];
  $tDID = $_POST['tDID'];
  // $tSID = $_POST['tSID'];
  $fnewRef = $_POST['fnewRef'];
  $tnewRef = $_POST['tnewRef'];

  // echo "<br><br>FROM THE FORM";
  // echo "<br>urlPage = $urlPage";
  // echo "<br>FORM fACID : $fACID";
  // echo "<br>FORM fDID : $fDID";
  // echo "<br>FORM fSID : $fSID";
  // echo "<br>FORM tACID : $tACID";
  // echo "<br>FORM tDID : $tDID";
  // echo "<br>FORM tSID : $tSID";
  // echo "<br>FORM fnewRef : $fnewRef";
  // echo "<br>FORM tnewRef : $tnewRef";

  // FROM
  if (empty($fACID)) {
    $fACID = $ofACID;
  }
  if (empty($fDID)) {
    // echo "<br>DID empty";
    if ($fACID <> $ofACID) {
      // echo "new ACID = $fACID :: ori ACID = $ofACID :::";
      // get min DID for that ACID
      // get min SID for that DID
      $ACID = $fACID;
      // echo "ACID == $ACID";
      include 'from_ACID_get_min_division_EDIT.inc.php';
      $fDID = $mDID;
      $ofSID = $mSID;
      // echo "<br>Min DID : $fDID / min SID : $fSID";
    }else {
      $fDID = $ofDID;
      // echo "<br>1a/$fDID";
    }
    $fDID = $fDID;
    // echo "<br>2b/$fDID";
  }else {
    $fDID = $ofDID;
    // echo "<br>3c/$fDID";
  }

  if (empty($fSID)) {
    $fSID = $ofSID;
    // echo "<br>fs1/$fSID";
  }else {
    $fSID = $ofSID;
    // echo "<br>fs2/$fSID";
  }
  $fSID = $ofSID;
  // echo "<br>fs3/$fSID";

// TO
if (empty($tACID)) {
  $tACID = $otACID;
}
if (empty($tDID)) {
  // echo "<br>DID empty";
  if ($tACID <> $otACID) {
    // echo "new ACID = $tACID :: ori ACID = $otACID :::";
    // get min DID for that ACID
    // get min SID for that DID
    $ACID = $tACID;
    // echo "ACID == $ACID";
    include 'from_ACID_get_min_division_EDIT.inc.php';
    $tDID = $mDID;
    $otSID = $mSID;
    // echo "<br>Min DID : $tDID / min SID : $tSID";
  }else {
    $tDID = $otDID;
    // echo "<br>1a/$tDID";
  }
  $tDID = $tDID;
  // echo "<br>2b/$tDID";
}else {
  $tDID = $otDID;
  // echo "<br>3c/$tDID";
}

if (empty($tSID)) {
  $tSID = $otSID;
  // echo "<br>ts1/$tSID";
}else {
  $tSID = $otSID;
  // echo "<br>ts2/$tSID";
}
$tSID = $otSID;
// echo "<br>fs3/$tSID";

  if (empty($fnewRef)) {
    $fnewRef = $OIDcor;
  }else {
    $fnewRef = $fnewRef;
  }

  if (empty($tnewRef)) {
    $tnewRef = $OIDtor;
  }else {
    $tnewRef = $tnewRef;
  }

  // echo "<br><br>REVISED DATA FOR THE db";
  // echo "<br>urlPage = $urlPage";
  // echo "<br>REVISED fACID : $fACID";
  // echo "<br>REVISED fDID : $fDID";
  // echo "<br>REVISED fSID : $fSID";
  // echo "<br>REVISED tACID : $tACID";
  // echo "<br>REVISED tDID : $tDID";
  // echo "<br>REVISED tSID : $tSID";
  // echo "<br>REVISED fnewRef : $fnewRef";
  // echo "<br>REVISED tnewRef : $tnewRef";

  // NEED to get ID details from Andrew
  // Need to set queries if empty etc 260321

  $sql = "INSERT INTO log_orders
            (OID, ofACID, ofDID, ofSID, otACID, otDID, otSID, ofRef, otRef, UID, inputtime)
            -- (OID, ofACID, ofDID, ofSID, otACID, otDID, otSID, ofRef, otRef, UID, inputtime)
          VALUES
            (?,?,?,?,?,?,?,?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-feoda1</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sssssssssss", $OID, $ofACID, $ofDID, $ofSID, $otACID, $otDID, $otSID, $oOIDcor, $oOIDtor, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  $sql = "UPDATE orders
          SET tACID = ?
            , tDID = ?
            , tSID = ?
            , fACID = ?
            , fDID = ?
            , fSID = ?
            , our_order_ref = ?
            , their_order_ref = ?
            WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-feoda2</b>';
  }else{
    // mysqli_stmt_bind_param($stmt, "sssss", $fACID, $tACID, $fnewRef, $tnewRef, $OID );
    mysqli_stmt_bind_param($stmt, "sssssssss", $fACID, $fDID, $fSID, $tACID, $tDID, $tSID, $fnewRef, $tnewRef, $OID );
    mysqli_stmt_execute($stmt);
  }
  //
  header("Location:../$urlPage&tab=$tab&o=$OID");
  exit();
  //

}
