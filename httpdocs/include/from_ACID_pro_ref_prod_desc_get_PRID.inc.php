<?php
include 'dbconnect.inc.php';

// echo "include/from_ACID_pro_ref_prod_desc_get_PRID.inc.php";

// Check if the short code prod_ref is not already in use
$sql = "SELECT ID as ID
        FROM prod_ref
        WHERE ACID = ?
        AND prod_ref = ?
        AND prod_desc = ?
       ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo "FAIL-fnsa";
}else {
  mysqli_stmt_bind_param($stmt, "sss", $ACID, $prod_ref, $prod_desc);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $PRID = $row['ID'];
}
