<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/order_QUERY.inc.php";

include 'include/from_CID_get_min_associate_company.inc.php</b>';
$mACID;

$CID = $_SESSION['CID'];

include 'set_urlPage.inc.php';

if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
}

if (isset($_GET['ot'])) {
  $ot = $_GET['ot'];
}

if (isset($_GET['sp'])) {
  $sp = $_GET['sp'];
}

// $sdate = date('d-M-Y', $sd);
// $edate = date('d-M-Y', $ed);
// $tdate = date('d-M-Y', $td);
// echo "<br><br><br><br><br><br><br><br><br><br><br>SHOW RESULTS BETWEEN";
// echo "<br>$sdate and $edate";
// echo "<br>Today = $tdate";

$sql = "SELECT DISTINCT
-- order_report_select_order_type.sql
     o.fCID AS cliCID
     , c.name AS cliCIDn
     , o.fACID AS cli_ACID
     , ac.name AS cli_ACIDn
     , o.fDID AS cli_DID
     , d.name AS cli_DIDn
     , o.tCID AS supCID
     , cS.name AS supCIDn
     , o.tACID AS sup_ACID
     , acS.name AS sup_ACIDn
     , o.tDID AS sup_DID
     , d.name AS sup_DIDn
     , Mvemnt.ORDER_ID AS OID
     , Mvemnt.O_ITEM_ID AS OIID
     , LPAD(o.orNos,6,0) AS ordNos
     , LPAD(Mvemnt.ord_item_nos,3,0) AS ordItemNos
     , o.OTID AS OTID
     , Mvemnt.samProd AS samProd
     , o.our_order_ref AS ourRef
     , oi.their_item_ref AS theirRef
     , odd.del_Date AS ODDIDdd
     , oidd.item_del_date AS item_due_date
     , date_format(from_unixtime(oidd.item_del_date),'%d %b %Y') AS item_del_date
     , IF(oi.itComp = 0,'Open','Closed') AS Item_Status
     , o.CPID AS CPID
     , pr.ID AS PRID
     , pr.prod_ref AS PRIDs
     , Mvemnt.ordQty AS OIQIDq
-- CALCULATE INDIVIDUAL GARMENT MOVEMENT VALUES FOR EACH SECTION
     , SUM(Mvemnt.ORDER_PLACEMENT + Mvemnt.REJ_BY_STG1 + Mvemnt.ORDER_PLACE_INCR + Mvemnt.COR_OUT_STG1 - Mvemnt.INTO_STG1 - Mvemnt.ORDER_PLACE_DECR - Mvemnt.COR_IN_STG1) AS Awaiting
     , SUM(Mvemnt.INTO_STG1 + Mvemnt.REJ_BY_STG2 + Mvemnt.COR_IN_STG1 + Mvemnt.COR_OUT_STG2 - Mvemnt.INTO_STG2 - Mvemnt.REJ_BY_STG1 - Mvemnt.COR_OUT_STG1 - Mvemnt.COR_IN_STG2) AS Stage1
     , SUM(Mvemnt.INTO_STG2 + Mvemnt.REJ_BY_STG3 + Mvemnt.COR_IN_STG2 + Mvemnt.COR_OUT_STG3 - Mvemnt.INTO_STG3 - Mvemnt.REJ_BY_STG2 - Mvemnt.COR_OUT_STG2 -Mvemnt.COR_IN_STG3) AS Stage2
     , SUM(Mvemnt.INTO_STG3 + Mvemnt.REJ_BY_STG4 + Mvemnt.COR_IN_STG3 + Mvemnt.COR_OUT_STG4 - Mvemnt.INTO_STG4 - Mvemnt.REJ_BY_STG3  - Mvemnt.COR_OUT_STG3 - Mvemnt.COR_IN_STG4) AS Stage3
     , SUM(Mvemnt.INTO_STG4 + Mvemnt.REJ_BY_STG5 + Mvemnt.COR_IN_STG4 + Mvemnt.COR_OUT_STG5 - Mvemnt.INTO_STG5 - Mvemnt.REJ_BY_STG4  - Mvemnt.COR_OUT_STG4 - Mvemnt.COR_IN_STG5) AS Stage4
     , SUM(Mvemnt.INTO_STG5 + Mvemnt.REJ_BY_WHOUSE + Mvemnt.COR_IN_STG5 + Mvemnt.COR_OUT_WHOUSE - Mvemnt.INTO_WHOUSE - Mvemnt.REJ_BY_STG5  - Mvemnt.COR_OUT_STG5 - Mvemnt.COR_IN_WHOUSE) AS Stage5
     , SUM(Mvemnt.INTO_WHOUSE + Mvemnt.FROM_CLIENT + Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.COR_IN_WHOUSE + Mvemnt.CORR_OUT_CLIENT + Mvemnt.CORR_OUT_SUPPLIER
           - Mvemnt.REJ_BY_WHOUSE - Mvemnt.TO_CLIENT - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.COR_OUT_WHOUSE - Mvemnt.CORR_IN_CLIENT - Mvemnt.CORR_IN_SUPPLIER) AS Warehouse
     , SUM(Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT) AS CLIENT
     , SUM(Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER) AS SUPPLIER
--     , SUM(Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT + Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.CORR_OUT_SUPPLIER
--           - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.CORR_IN_SUPPLIER) AS SENT
     , Mvemnt.revUID AS revUID
     , Mvemnt.last_revised_time AS last_revised_time
FROM (
    SELECT DISTINCT oimr.ID AS OIMR_ID
         , o.ID AS ORDER_ID
         , oi.ID AS O_ITEM_ID
         , oi.PRID AS PRID
         , oi.ord_item_nos AS ord_item_nos
         , oi.samProd AS samProd
         , oiq.order_qty AS ordQty
         , last_update.revUID
         , last_update.last_revised_time
         , o.OTID AS OTID
    -- oimr1 not required at present - oiq.order_qty is used instead
         , IF((CASE WHEN oimr.ID = 2 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 2 THEN SUM(opm.omQty) END)) AS  ORDER_PLACEMENT
         , IF((CASE WHEN oimr.ID = 3 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 3 THEN SUM(opm.omQty) END)) AS INTO_STG1
         , IF((CASE WHEN oimr.ID = 4 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 4 THEN SUM(opm.omQty) END)) AS INTO_STG2
         , IF((CASE WHEN oimr.ID = 5 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 5 THEN SUM(opm.omQty) END)) AS INTO_STG3
         , IF((CASE WHEN oimr.ID = 6 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 6 THEN SUM(opm.omQty) END)) AS INTO_STG4
         , IF((CASE WHEN oimr.ID = 7 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 7 THEN SUM(opm.omQty) END)) AS INTO_STG5
         , IF((CASE WHEN oimr.ID = 8 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 8 THEN SUM(opm.omQty) END)) AS INTO_WHOUSE
    -- oimr9 to oimr10 not required at present
         , IF((CASE WHEN oimr.ID = 11 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 11 THEN SUM(opm.omQty) END)) AS ORDER_PLACE_INCR
         , IF((CASE WHEN oimr.ID = 12 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 12 THEN SUM(opm.omQty) END)) AS ORDER_PLACE_DECR
         , IF((CASE WHEN oimr.ID = 13 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 13 THEN SUM(opm.omQty) END)) AS REJ_BY_STG1
         , IF((CASE WHEN oimr.ID = 14 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 14 THEN SUM(opm.omQty) END)) AS REJ_BY_STG2
         , IF((CASE WHEN oimr.ID = 15 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 15 THEN SUM(opm.omQty) END)) AS REJ_BY_STG3
         , IF((CASE WHEN oimr.ID = 16 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 16 THEN SUM(opm.omQty) END)) AS REJ_BY_STG4
         , IF((CASE WHEN oimr.ID = 17 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 17 THEN SUM(opm.omQty) END)) AS REJ_BY_STG5
         , IF((CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END)) AS REJ_BY_WHOUSE
         , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
         , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
         , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
         , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
         , IF((CASE WHEN oimr.ID = 23 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 23 THEN SUM(opm.omQty) END)) AS COR_IN_STG1
         , IF((CASE WHEN oimr.ID = 24 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 24 THEN SUM(opm.omQty) END)) AS COR_IN_STG2
         , IF((CASE WHEN oimr.ID = 25 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 25 THEN SUM(opm.omQty) END)) AS COR_IN_STG3
         , IF((CASE WHEN oimr.ID = 26 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 26 THEN SUM(opm.omQty) END)) AS COR_IN_STG4
         , IF((CASE WHEN oimr.ID = 27 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 27 THEN SUM(opm.omQty) END)) AS COR_IN_STG5
         , IF((CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END)) AS COR_IN_WHOUSE
         , IF((CASE WHEN oimr.ID = 29 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 29 THEN SUM(opm.omQty) END)) AS COR_OUT_STG1
         , IF((CASE WHEN oimr.ID = 30 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 30 THEN SUM(opm.omQty) END)) AS COR_OUT_STG2
         , IF((CASE WHEN oimr.ID = 31 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 31 THEN SUM(opm.omQty) END)) AS COR_OUT_STG3
         , IF((CASE WHEN oimr.ID = 32 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 32 THEN SUM(opm.omQty) END)) AS COR_OUT_STG4
         , IF((CASE WHEN oimr.ID = 33 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 33 THEN SUM(opm.omQty) END)) AS COR_OUT_STG5
         , IF((CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END)) AS COR_OUT_WHOUSE
         , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
         , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
         , IF((CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END)) AS CORR_OUT_SUPPLIER
         , IF((CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END)) AS CORR_IN_SUPPLIER
    -- oimr39 to oimr40 not required at present
    FROM order_placed_move opm
         INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
         INNER JOIN order_placed op ON opm.OPID = op.ID
         INNER JOIN order_item oi ON op.OIID = oi.ID
         INNER JOIN orders o ON oi.OID = o.ID
         INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
       -- 'Last user update' sub-select
       INNER JOIN (SELECT opm_order.OIID
                          , opm_order.OPID
                          , opm_order.OPMID_MAX
                          , opm_order.revUID
                          , date_format(from_unixtime(opm.inputtime),'%d %b %Y') AS last_revised_time
                    FROM
                         (
                          SELECT oi.ID AS OIID
                                 , opm.OPID
                                 , MAX(opm.ID) AS OPMID_MAX
                                 , opm.UID AS revUID
                          FROM order_placed_move opm
                               INNER JOIN order_placed op ON op.ID = opm.OPID
                               INNER JOIN order_item oi ON oi.ID = op.OIID
                               INNER JOIN orders o ON oi.OID = o.ID
                          GROUP BY oi.ID
                         ) opm_order INNER JOIN order_placed_move opm ON opm.ID = opm_order.OPMID_MAX
                  ) last_update ON oi.ID = last_update.OIID
       -- End of 'Last user update' sub-select
    WHERE oi.samProd = ?
    GROUP BY oi.ID, oimr.ID
   ) Mvemnt INNER JOIN order_item oi ON Mvemnt.O_ITEM_ID = oi.ID
            INNER JOIN orders o ON Mvemnt.ORDER_ID = o.ID
            INNER JOIN order_item_del_date oidd ON Mvemnt.O_ITEM_ID = oidd.OIID
            INNER JOIN prod_ref pr ON Mvemnt.PRID = pr.ID
            INNER JOIN orders_due_dates odd ON odd.OID = o.ID
            INNER JOIN company c ON o.fCID = c.ID
            INNER JOIN company cS ON o.tCID = cS.ID
            INNER JOIN associate_companies ac ON o.fACID = ac.ID
            INNER JOIN associate_companies acS ON o.tACID = acS.ID
            INNER JOIN division d ON o.fDID = d.ID
            INNER JOIN division dS ON o.tDID = dS.ID
WHERE (o.fCID = ? OR o.tCID = ?)
AND oidd.item_del_date BETWEEN ? AND ?
  AND oi.itComp = ?
/*
  AND CASE WHEN @OTID_CHOICE = 0
           THEN o.OTID in (1,2,3,4)
           ELSE o.OTID = @OTID_CHOICE
      END
*/
  AND o.OTID = ?
GROUP BY Mvemnt.O_ITEM_ID
ORDER BY from_unixtime(item_due_date), ordNos, ordItemNos, OIQIDq ASC
LIMIT ?,?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-oQ</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssssssss", $sp, $CID, $CID, $sd, $ed, $OC, $ot, $start, $rpp);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $cliCID = $row['cliCID'];
    $cliCIDn = $row['cliCIDn'];
    $cli_ACID = $row['cli_ACID'];
  $cli_ACIDn = $row['cli_ACIDn'];
    $supCID = $row['supCID'];
    $supCIDn = $row['supCIDn'];
    $sup_ACID = $row['sup_ACID'];
  $sup_ACIDn = $row['sup_ACIDn'];
    $TUID = $row['TUID'];
    $Ttd = $row['Ttd'];
    $OID = $row['OID'];
  $OIID = $row['OIID'];
  $OIDnos = $row['ordNos'];
  $nos = $row['ordItemNos'];
  $OTID = $row['OTID'];
  $samProd = $row['samProd'];
  $ourRef = $row['ourRef'];
  $theirRef = $row['theirRef'];
    $ODDIDdd = $row['ODDIDdd'];
  $OIDDIDd = $row['item_due_date'];
    $CPID = $row['CPID'];
  $PRID = $row['PRID'];
  $PRIDs = $row['PRIDs'];
  $OIQIDq = $row['OIQIDq'];
  $Awaiting = $row['Awaiting'];
  $Stage1 = $row['Stage1'];
  $Stage2 = $row['Stage2'];
  $Stage3 = $row['Stage3'];
  $Stage4 = $row['Stage4'];
  $Stage5 = $row['Stage5'];
  $Warehouse = $row['Warehouse'];
    $CLIENT = $row['CLIENT'];
    $SUPPLIER = $row['SUPPLIER'];
  $revUID = $row['revUID'];
  $revTime = $row['last_revised_time'];

  // LEFT IN UNTIL DAL UPDATES THE SQL
    if ($cliCID <> $CID) {
      $cli_ACIDn = $cli_ACIDn;
      $OTID = $OTID - 1;
    }else {
      $cli_ACIDn = $sup_ACIDn;
      $OTID = $OTID;
    }

    $cdate = 'black';

    if ($OIDDIDd < $td) {
      $bcdate = '#fc4141';
    }elseif ($OIDDIDd < ($td + 604800)) {
      $bcdate = '#f7a0a0';
    }elseif ($td + 604800 < $OIDDIDd) {
      $bcdate = '#ecf5a9';
    }elseif ($OIDDIDd > $td + 1209600) {
      $bcdate = '#92ef9d';
    }

    $OIDDIDd = date('d-M-Y', $OIDDIDd);

    ?>
    <tr>
      <?php

      if ($samProd == 0) {
        $spc ='#76f8bc';
        $spbc = '#76f8bc';
      }else {
        $spc = '#fa7b7b';
        $spbc = '#fa7b7b';
      }

      if ($CID <> $fCID) {
        $ot = 3;
      }else {
        $ot = $ot;
      }


      // Shows you the orders placed within the last 7 days
      // if($od>($td-604740)){
      if ($ot == 1) {
        $c = 'black';
        $cbc = 'f7cc6c';
      }elseif ($ot == 2) {
        $c = 'black';
        $cbc = 'bbbbff';
      }elseif ($ot == 3) {
        $c = 'white';
        $cbc = 'd68e79';
      }elseif ($ot == 4) {
        $c = 'white';
        $cbc = 'c9d1ce';
      }

      if (isset($_GET['H'])) {
        $cheader = 'UPDATED';
        $bc = '#b7d8ff';
        $Date = $revTime;
      }elseif (isset($_GET['L'])) {
        $cheader = 'DUE DATE';
        $bc = '#ffffd1';
        $Date = $OIDDIDd;
      }

      if ($sup_ACID <> $mACID) {
        $compACID = $sup_ACID;
        $compACIDn = $sup_ACIDn;
      }else {
        $compACID = $cliACID;
        $compACIDn = $cliACIDn;
      }



      ?>
      <td style="text-align: right; color:blue; background-color: <?php echo $bcdate ?>; border-right:thin solid grey; padding-right:0.5%;"><a style="color:<?php echo $cdate ?>; font-size: 100%; text-decoration:none;" href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&o=<?php echo $OID;?>"><?php echo $OIDnos ?></a></td>
      <td class="OIID" style="font-size: 105%; text-align: right; padding-right:0.2%; color:blue; border-right:thin solid grey;"><a style="color:blue; text-decoration:none; " href="<?php echo $urlPage ?>&tab=<?php echo $tab ?>&oi=<?php echo $OIID;?>"><?php echo $nos ?></a></td>
      <?php
      if (strlen($compACIDn)>19) {
        $compACIDn = substr($compACIDn,0,19);
        ?>
        <td style="font-size: 100%;text-align:left; text-indent:1%; border-right: thin solid grey;"><a style="color:blue; text-decoration:none;" href="associate.php?A&tab=<?php echo $ot ?>&id=<?php echo $compACID ?>"><?php echo $compACIDn ?> ...</a></td>
        <?php
      }else {?>
        <td style="color:blue; font-size: 100%;text-align:left; text-indent:1%; border-right: thin solid grey;"><a style="color:blue; text-decoration:none;" href="associate.php?A&tab=<?php echo $ot ?>&id=<?php echo $compACID ?>"><?php echo $compACIDn ?></a></td>
        <?php
      } ?>
      <?php
      if (strlen($theirRef)>30) {
        $theirRef = substr($theirRef,0,30);
        ?>
        <td style="text-align:left; text-indent:1%; border-right: thin solid grey;"><?php echo $theirRef ?> ...</td>
        <?php
      }else {?>
        <td style="text-align:left; text-indent:1%; border-right: thin solid grey;"><?php echo $theirRef ?></td>
        <?php
      } ?>
      <td style="text-align:left; text-indent:1.5%; border-right:thin solid grey;"><a style="color:blue; text-decoration:none;" href="styles.php?S&sio&s=<?php echo $PRID ?>"> <?php echo $PRIDs;?></a></td>
      <td style="text-align:right; border-right:thin solid grey; padding-right:0.5%;"><?php echo $OIQIDq;?></td>
      <?php
      include 'TABLE_section_item_update.inc.php';
      ?>
      <td style="text-align:right; padding-right:0.1%;"><?php echo $Date ;?></td>
    </tr>
    <?php
    }
  } ?>
  <?php
  include 'table_end_16.inc.php';
  ?>
  <?php
  include 'order_QUERY_Section_TOTALS.inc.php';
  if ($Awaiting == 0) { $Awaiting = " ";}
  if ($Stage1 == 0) { $Stage1 = " ";}
  if ($Stage2 == 0) { $Stage2 = " ";}
  if ($Stage3 == 0) { $Stage3 = " ";}
  if ($Stage4 == 0) { $Stage4 = " ";}
  if ($Stage5 == 0) { $Stage5 = " ";}
  if ($whs == 0) { $whs = " ";}
  ?>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td style="text-align:right; padding-right:0.5%; border-right:thin solid grey;"></td>
    <td style="text-align:right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $ORDERS ?></td>
    <td style="text-align:right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $Awaiting ?></td>
    <td style="text-align:right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $Stage1 ?></td>
    <td style="text-align:right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $Stage2 ?></td>
    <td style="text-align:right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $Stage3 ?></td>
    <td style="text-align:right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $Stage4 ?></td>
    <td style="text-align:right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $Stage5 ?></td>
    <td style="text-align:right; padding-right:0.5%; border-right:thin solid grey;"><?php echo $whs ?></td>
    <td style="text-align:right; padding-right:0.5%; border-right:thin solid grey;"></td>
    <td></td>
  </tr>
  </table>
