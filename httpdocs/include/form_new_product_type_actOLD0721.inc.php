<?php
// echo '<b>include/form_new_product_type_act.inc.php</b>';
// echo '<br>FROM:form_new_product_type.inc.php</b>';
if (!isset($_POST['newpt'])){
  // echo 'Incorrect method used';
  header('Location:../index.php');
  exit();
}else{
  include 'dbconnect.inc.php';

  // echo '<br>The correct login method was used';
  // echo '<br><br><b>Get the data from the form AND the session</b>';

  $UID = $_SESSION['UID'];
  $ACID = $_SESSION['ACID'];
  $scode = $_POST['scode'];
  $pref = $_POST['pref'];
  $td = date('U');

  // echo '<br>The associate company ID ($ACID) is : <b>'.$ACID.'</b>';
  // echo '<br>The product description ($pref) is : <b>'.$pref.'</b>';
  // echo '<br>The product short code ($scode) is : <b>'.$scode.'</b>';
  // echo '<br><br><b>Check the validity of the data the user has added</b>';
  // pt.DIDchanged
  // GET the minimum DID for the ACID
  include 'from_CID_get_min_division.inc.php';

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to

    // Is the product type listed by name
    $sql = "SELECT pt.ID as PTID
            FROM product_type pt
              , division d
            WHERE pt.ACID = ?
            AND pt.name = ?
            -- AND pt.DID = d.ID
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-fnpt</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $ACID, $pref);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $PTID = $row['PTID'];
      // echo '<br>Product reference : '.$proDID;
    }

    // Use this to check against the short code and the above to check against the product description!
    // Is the product type listed by ID
    $sql = "SELECT pt.ID as PTIDs
            FROM product_type pt
              -- , division d
            WHERE pt.ACID = ?
            AND pt.scode = ?
            -- AND pt.DID = d.ID
    ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-fnpt1</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $ACID, $scode);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $PTIDs = $row['0'];
      // echo '<br>Short code : '.$scodEID;
    }

    //Check the data
    if ($PTID <> 0) {
      // echo '<br>The product name is already in use';
      header("Location:../styles.php?S&snp&c");
      exit();
    }elseif ($PTIDs <> 0) {
      // echo '<br>The name is free';
      // echo '<br>BUT the short code is taken!';
      header("Location:../styles.php?S&snp&b");
      exit();
    }elseif(strlen($scode)>= 11){
      // echo '<br>The code is too long';
      header("Location:../styles.php?S&snp&a&p=$pref");
      exit();
    }else{

      //Register the product type
      $sql = "INSERT INTO product_type
                (ACID, UID, name, scode, inputtime)
              VALUES
                (?,?,?,?,?)
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo '<b>FAIL-fnpt2</b>';
      }else{
        // echo '<br>WORKING';
        mysqli_stmt_bind_param($stmt, "sssss", $ACID, $UID, $pref, $scode, $td);
        mysqli_stmt_execute($stmt);
        // echo '<br>Type registered';
      }

      // get the product type ID
      $sql = "SELECT pt.ID as PTID
              FROM product_type pt
              WHERE ACID = ?
              AND UID = ?
              AND scode = ?
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo '<b>FAIL-fnpt3</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "sss", $ACID, $UID, $scode);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        $row = mysqli_fetch_array($result);
        $PTID = $row['PTID'];
      }
      // echo '<br>Product type is $PTID : <b>'.$PTID.'</b>';

      // get the company's section ID
      $sql = "SELECT MIN(s.ID) as SID
              FROM section s
                , division d
                , associate_companies ac
              WHERE ac.ID = ?
              AND d.ACID = ac.ID
              AND s.DID = d.ID
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo '<b>FAIL-fnpt4</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "s", $ACID);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        $row = mysqli_fetch_array($result);
        $SID = $row['SID'];
      }
      // echo '<br>Section is $SID : <b>'.$SID.'</b>';

      // add to the section_product_types table this new section/product type
      $sql = "INSERT INTO section_product_types
                (SID, PTID, UID, inputtime)
              VALUES
                (?,?,?,?)
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo '<b>FAIL-fnpt5</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ssss", $SID, $PTID, $UID, $td);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
      }
    }

      //  has a product reference been created
      // include 'I_PRID_check.inc.php';

  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  // echo ' <br>The product type has been registered';
  header("Location:../styles.php?S&spt");
  // header("Location:../styles.php?S&snp");
  exit();
}
