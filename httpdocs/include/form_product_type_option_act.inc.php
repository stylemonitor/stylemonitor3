<?php
include 'dbconnect.inc.php';
// echo "include/form_item_style_option_act.inc.php";

if (!isset($_POST['ent']) && !isset($_POST['canent'])) {
  // echo "WRONG METHOD";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['canent'])) {
  // echo "CANCEL";
  $PRID = $_POST['fso'];
  header("Location:../styles.php?S&spt");
  exit();
}elseif (isset($_POST['ent'])) {
  // echo "Go to wherever!!!!";
  $PTID = $_POST['PTID'];
  $styopt = $_POST['styopt'];
  if (empty($styopt)) {
    header("Location:../styles.php?S&spt");
    exit();
  }elseif ($styopt == 1) {
    header("Location:../styles.php?S&snn&fns&p=$PTID");
    exit();
  }elseif ($styopt == 2) {
    header("Location:../styles.php?S&spe&p=$PTID");
    exit();
  }elseif ($styopt == 3) {
    header("Location:../styles.php?S&spr&p=$PTID");
    exit();
  }
}
