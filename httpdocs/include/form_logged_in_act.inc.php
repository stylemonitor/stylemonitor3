<?php
include 'dbconnect.inc.php';
echo '<b>include/form_logged_in_act.inc.php</b>';
// echo '<br>FROM:form_new_product_type.inc.php</b>';
if (!isset($_POST['confirm']) && !isset($_POST['decline'])){
  // echo 'Incorrect method used';
  header('Location:../index.php');
  exit();
}elseif (isset($_POST['decline'])){
  // echo 'Cancel Season Adding';
  header('Location:../index.php');
  exit();
}elseif (isset($_POST['confirm'])){

  $UID  = $_POST['UID'];
  $cpwd = $_POST['cpwd'];
  $td = date('U');

  // echo "User ID is $UID";
  // echo "<br>Used entered password is $cpwd";

  include 'from_UID_login_get_user_details.inc.php';
  include 'from_UID_get_company_details.inc.php';
  include 'from_CID_get_company_admin.inc.php';
  // echo "<br>Password is $UIDp";

  $pwdCheck = password_verify($cpwd, $UIDp);
  // check if the password is correct for the user
  if($pwdCheck == false){
    echo "<br>The password used <b>does not match</b> their system held password";
    // echo "<br><b>*** 2 ***</b>";
    // enter the information into the password fail registry!!!S
    include 'pwd_lockout_add.inc.php';
    // header("Location:../login.php?A=2&u=$UID&f=$f");
    // exit();

  }elseif($pwdCheck == true){

    // Start transaction
    mysqli_begin_transaction($mysqli);
    try {
      // to

      // close the 'other' login of the user
      $sql = "UPDATE login_registry
            SET logouttime = ?
            WHERE UID = ?
            AND logouttime = 0;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)){
       echo '<b>FAIL6</b>';
      }else{
      mysqli_stmt_bind_param($stmt, "ss", $td, $UID);
      mysqli_stmt_execute($stmt);
      }

      // log them in on this computer
      // LOG the user in
      $sql = "INSERT INTO login_registry
      (UID, logintime, logouttime)
      VALUES
      (?,?,'0');";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b>FAIL1</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ss", $UID, $td);
        mysqli_stmt_execute($stmt);
      }

      // SET THE SESSION VARIABLES
      $_SESSION['UID']    = $UID;    //users ID
      $_SESSION['UIDf']   = $UIDf;   //users firstname
      $_SESSION['UIDv']   = $UIDv;   //users privileges
      $_SESSION['CID']    = $CID;    //users company ID
      $_SESSION['CIDn']   = $CIDn;   //users company name
      $_SESSION['ACID']   = $ACID;   //associate company ID
      $_SESSION['CDUID']  = $CDUID;  //users company division ID
      $_SESSION['DID']    = $DID;    //division ID

      // echo "<br>Session UID : ".$_SESSION['UID']."";
      // echo "<br>Session UIDf : ".$_SESSION['UIDf']."";
      // echo "<br>Session UIDv : ".$_SESSION['UIDv']."";
      // echo "<br>Session CID : ".$_SESSION['CID']."";
      // echo "<br>Session CIDn : ".$_SESSION['CIDn']."";
      // echo "<br>Session ACID : ".$_SESSION['ACID']."";
      // echo "<br>Session CDUID : ".$_SESSION['CDUID']."";

    } catch (mysqli_sql_exception $exception) {
      mysqli_rollback($mysqli);

      throw $exception;
    }

    header("Location:../home.php?H&tab=0");
    exit();
  }
}
