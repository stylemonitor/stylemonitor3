<?php
include 'dbconnect.inc.php';
// echo "include/form_item_style_option_act.inc.php";

if (!isset($_POST['ent']) && !isset($_POST['canent'])) {
  // echo "WRONG METHOD";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['canent'])) {
  // echo "CANCEL";
  $PRID = $_POST['fso'];
  header("Location:../associates.php?A&acn");
  exit();
}elseif (isset($_POST['ent'])) {
  // echo "Go to wherever!!!!";
  $ACID = $_POST['ACID'];
  $styopt = $_POST['styopt'];
  $type = $_POST['type'];
  $CTID = $_POST['ct'];
  if (empty($styopt)) {
    header("Location:../associates.php?A&acn");
    exit();
  }elseif ($styopt == 1) {
    header("Location:../styles.php?S&orn&sal=1&aid=$ACID");
    exit();
  }elseif ($styopt == 2) {
    header("Location:../styles.php?S&orn&pur=1&aid=$ACID");
    exit();
  }elseif ($styopt == 3) {
    header("Location:../associates.php?A&asf&id=$ACID");
    exit();
  }elseif ($styopt == 4) {
    header("Location:../associates.php?A&ase&a=$ACID");
    exit();
  }
}
