<?php
session_start();
// echo '<b>input_associate_divison_section_product_types.inc.php</b>';

// echo "<br>Associate company ID (ACID) $ACID";
// echo "<br>Division ID (DID) $DID";
// echo "<br>Section ID (SID) $SID";
// echo "<br>Product type ID (PTID) $PTID";

// add to the division_product_type table this new division/product type
$sql = "INSERT INTO company_product_types
          (CID, PTID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fnpt5a</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $CID, $PTID, $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
}

// add to the division_product_type table this new division/product type
$sql = "INSERT INTO associate_company_product_types
          (ACID, PTID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fnpt5a</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $ACID, $PTID, $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
}

// add to the division_product_type table this new division/product type
$sql = "INSERT INTO division_product_types
          (DID, PTID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fnpt5a</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $DID, $PTID, $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
}

// add to the section_product_types table this new section/product type
$sql = "INSERT INTO section_product_types
          (SID, PTID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fnpt5</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $SID, $PTID, $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
}
