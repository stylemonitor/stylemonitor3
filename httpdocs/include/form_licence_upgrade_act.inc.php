<?php
include 'dbconnect.inc.php';
// echo "<b>include/form_new_company_user_act.inc.php</b>";

// $UID = $_SESSION['UID'];

if (!isset($_POST['renew_trial']) && !isset($_POST['get_full'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['renew_trial'])) {
  // echo "<br>Renew Trial licence for 2 weeks";
  $UID = $_POST['UID'];
  $CID = $_POST['CID'];
  $td  = date('U');

  include 'from_UID_get_user_details.inc.php';

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to

    // get TRIAL licence count
    $sql = "SELECT LTID as LTID
              , st_date as LTIDs
              , UID as LTIDu
            FROM company_licence
            WHERE CID = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      // echo '<b>FAIL-flua</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "s", $CID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $LTID = $row['LTID'];
      $LTIDs = $row['LTIDs'];
      $LTIDu = $row['LTIDu'];
    }
    // echo "<br>Previous Licence ID $LTID";
    // echo "<br>Previous UID $LTIDu";
    // Increase the trial licence count by 1


    if ($LTID < 10) {

      $LTIDo = $LTID;
      $LTID = $LTID + 1;
      // echo "<br>Company ID is $CID";
      // echo "<br>User ID is $UID";
      // echo "<br>Todays date is $td";
      // echo "<br>Original Licence ID is $LTIDo";
      // echo "<br>Licence count is less than 11";
      // echo "<br> Revised licence type ID is $LTID";
      // echo "<br>Update the company_licence table";

      // log the original data
      $sql = "INSERT INTO log_company_licence
                (CID, ori_licence, st_date, inputuserID)
              VALUES
                (?,?,?,?)
              ;";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
      // echo '<br>FAIL-flua1';
      }else {
      mysqli_stmt_bind_param($stmt, "ssss", $CID, $LTIDo, $LTIDs, $LTIDu);
      mysqli_stmt_execute($stmt);
      }

      $sql = "UPDATE company_licence
      SET UID = ?
      , LTID = ?
      , st_date = ?
      WHERE CID = ?
      ;";
      $stmt = mysqli_stmt_init($con);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        // echo '<b>FAIL-flua1</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ssss", $UID, $LTID, $td, $CID);
        mysqli_stmt_execute($stmt);
      }

      // echo "<br>Now log them in with their email $UIDe";
      // take them back to the login screen with username filled in

      header("Location:../index.php?l&N&u=$UIDe");
      exit();
    }else {
      // echo "<br>Trial licence count limit exceeded";

    }
  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  header("Location:../index.php?X");
  exit();
  // echo "<br>Did it work";
  // header("Location:../company.php?C&use");
  // exit();
}elseif (isset($_POST['get_full'])) {
  // echo "<br>Take out a FULL 1 year licence";
  // include 'dbconnect.inc.php';
  // include 'include/dbconnect.inc.php';
}
