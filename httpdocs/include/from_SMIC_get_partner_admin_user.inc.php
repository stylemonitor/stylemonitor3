<?php
include 'dbconnect.inc.php';
// echo "<br><b>from_SMIC_get_partner_admin_user.inc.php</b>";
// echo "<br>pSIMC from fnp is $pSMIC";

// Get the Potential Partners ADMIN persons details
  $sql = "SELECT c.ID as ppCID
            , c.name as ppadUCIDn
            , u.id as ppadUID
            , u.firstname as ppadUIDf
            , u.email as ppadUIDe
          FROM company c
            , associate_companies ac
            , division d
            , company_division_user cdu
            , users u
          WHERE c.SMIC = ?
          AND c.ID = ac.CID
          AND ac.ID = d.ACID
          AND d.ID = cdu.DID
          AND u.ID = cdu.UID
          AND cdu.current = 1
          AND u.privileges = 1;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL_fnpa</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $pSMIC);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $ppCID  = $row['ppCID'];
    $ppadUCIDn = $row['ppadUCIDn'];
    $ppadUID = $row['ppadUID'];
    $ppadUIDf  = $row['ppadUIDf'];
    $ppadUIDe = $row['ppadUIDe'];
  }
// echo "<br>ppCID = $ppCID :: ppadUCIDn = $ppadUCIDn :: ppadUID = $ppadUID :: ppadUIDf = $ppadUIDf :: ppadUIDe = $ppadUIDe";
