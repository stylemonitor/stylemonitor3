<?php
include 'dbconnect.inc.php';
// echo "include/from_ACID_select_division_dd.inc.php";
include 'from_tCID_get_min_division_part.inc.php';

$sql = "SELECT COUNT(d.ID) as cDID
        FROM division d
          , associate_companies ac
          , company c
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND ac.ID IN (select min(ac.ID)
                      from company c
                      , associate_companies ac
                      WHERE c.id = ?
                      AND ac.CID = c.id)
        AND d.ACID = ac.ID
        AND d.ID NOT IN (?)
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fpsdd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $tCID, $tCID, $mDID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cDID = $row['cDID'];
}

$sql = "SELECT d.ID as DID
          , d.name as DIDn
          , ac.CID as ACCID
          , ac.ID as ACID
        FROM division d
          , associate_companies ac
          , company c
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND ac.ID IN (select min(ac.ID)
                      from company c
                      , associate_companies ac
                      WHERE c.id = ?
                      AND ac.CID = c.id)
        AND d.ACID = ac.ID
        AND d.ID NOT IN (?)
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fpsdd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $tCID, $tCID, $mDID);
  // mysqli_stmt_bind_param($stmt, "ss", $tCID, $mDID);
  // mysqli_stmt_bind_param($stmt, "ss", $fCID, $mDID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  if ($cDID > 0) {
    ?>
    <option value="">Select Division</option>
    <?php
    while ($row = mysqli_fetch_assoc($result)) {
    ?>
    <option value="<?php echo $row['DID'] ?>"><?php echo $row['DIDn'] ?></option>
    <?php
    }
  }elseif ($cDID == 0) {
    ?>
    <option value="">No Divisions Available to Select</option>
    <?php
  }
}

?>
