<?php
include 'dbconnect.inc.php';
// echo '<b>include/from_pCID_select_TO_product_ref.inc.php</b>';

$CID = $_SESSION['CID'];

$sql = "SELECT pr.ID as PRID
		  		, concat(pr.prod_ref, ' : ', pr.prod_desc ,' - ', pt.name) as fullDesc
		  		, pr.prod_ref as PRIDn
          , pr.prod_desc as PRIDr
          , pt.name as PTIDn
          , pt.id as PTID
        FROM company c
          , associate_companies ac
          , prod_ref pr
          , product_type pt
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND pr.ACID = ac.ID
        AND pr.PTID = pt.ID
        AND pr.prod_ref NOT IN ('TBC')
        ORDER BY 3
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br><b>FAIL-fpstpr</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $tPRID  = $row['PRID'];
    $fullDesc = $row['fullDesc'];
    $tPRIDn = $row['PRIDn'];
    $tPTID  = $row['PTID'];
    $tPTIDn = $row['PTIDn'];

    if ($tPTIDn == 'Select Product Type') {
      $tPTIDn = '';
    }else {
      $tPTIDn = $tPTIDn;
    }

    if (strlen($fullDesc > 25)) {
      $fullDesc = substr($fullDesc,0,25);
      echo '<option value="'.$tPRID.'">'.$fullDesc.' ...</option>';

    }
    // echo '<option value="'.$tPRID.'">'.$fullDesc.'</option>';
  }
}
?>
