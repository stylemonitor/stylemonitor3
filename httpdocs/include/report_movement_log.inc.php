<?php
include 'dbconnect.inc.php';
echo "<br><b>report_movement_log.inc.php</b>";
include 'SM_colours.inc.php';
include 'set_urlPage.inc.php';

if (isset($_GET['o'])) {
  $OID = $_GET['o'];
}

if (isset($_GET['OI'])) {
  $OIID = $_GET['OI'];
}

// Set the rows per page
if (isset($_GET['mp'])) {
  $rpp = 4;
}else {
  $rpp = 15;
}

// Check the URL to see what page we are on
if (isset($_GET['pa'])) {
  $pa = $_GET['pa'];
}else{
  $pa = 0;
}

// Check which page we are on
if ($pa > 1) {
  $start = ($pa * $rpp) - $rpp;
}else {
  $start = 0;
}

// count the number of rows that will be in the table
// REMEBER to count them for the requirement of the table ie, this week, next week and Later
$sql = "SELECT COUNT(oi.ID) as OIID
          , ac.name as ACIDn
          , d.name as DIDn
          , oiq.order_qty oiQTY
          , odd.del_Date as odDIDd
          , o.orDate as OIDs
        FROM orders o
          , order_item oi
          , orders_due_dates odd
          , order_placed op
          , order_placed_move opm
          , order_item_movement_reason oimr
          , associate_companies ac
          , division d
          , order_item_qty oiq
        WHERE op.ID = ?
        AND oi.OID = o.ID
        AND op.OIID = oi.ID
        AND opm.OPID = op.ID
        AND opm.OIMRID = oimr.ID
        AND o.tDID = d.ID
        AND d.ACID = ac.ID
        AND oiq.OIID = oi.ID
        AND odd.OID = o.ID
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rmp</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OPID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $cOIID = $row['OIID'];
  $ACIDn = $row['ACIDn'];
  $DIDn = $row['DIDn'];
  $oiQTY = $row['oiQTY'];
  $odDIDd = $row['odDIDd'];
  $OIDs = $row['OIDs'];

  // turn the order date into an easy to read format
  // $OIDs = strtotime($OIDs);
  $OIDs = date('D-d-M-Y', $OIDs);

  // turn the order delivery date into an easy to read format
  $odDIDd = strtotime($odDIDd);
  $odDIDd = date('D-d-M-Y', $odDIDd);

  // echo '<br><br><br>Count of orders for this company is : '.$cOIID;

  // How many pages will this give us Pages To View $PTV
  // ceil rounds up a number ie 1.1 becomes 2 1.9 is also 2
  $PTV = ($cOIID / $rpp);

  // echo '<br>The number of pages will be : '.$PTV;
  $PTV = ceil($PTV);
  // $PTV = ceil($PTV) - 1;
  // echo '<br>The number of pages will be : '.$PTV;

  // echo '<br>Start record is : '.$start;
}

if ($OTID == 1) {
  $bcCol = $salAssCol;
}elseif ($OTID == 2) {
  $bcCol = $purAssCol;
}
?>

<!-- Get a table for the manufacturing unit details -->
<table class="trs" style="position:absolute; top:44%; left:0%; width:100%;">
    <caption style="font-weight:bold; text-align:left; text-indent:2%; background-color:<?php echo $bcCol ?>;"><a class="hreflink" href="home.php?H&rt3&o=<?php echo $OID ?>&OI=<?php echo $OIID ?>">Movement Log</a></caption>
  <tr>
    <th style="width:18%; text-align:left; text-indent:4%; background-color:#b5fcdb; ">Activity Date</th>
    <!-- <th style="width:1%;"></th> -->
    <th style="width:39.7%; text-align:left; text-indent:3%; background-color:<?php echo $movComCol ?>;">Comment</th>
    <th style="width:4%; background-color:#f1f1f1;"></th>
    <th style="width:4%; background-color:#f1f1f1;"></th>
    <th style="width:4%; background-color:#f1f1f1;"></th>
    <th style="width:4%; background-color:#f1f1f1;"></th>
    <th style="width:4%; background-color:#f1f1f1;"></th>
    <th style="width:4%; background-color:#f1f1f1;"></th>
    <th style="width:4%; background-color:#f1f1f1;"></th>
    <th style="width:4%; background-color:#f1f1f1;"></th>
    <th style="width:10%; background-color:#fffddd;">User</th>
  </tr>
<?php

// Get those records
include 'sql/report_movement_log.sql.php';

$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rmp2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssss", $CID, $CID, $OIID, $start, $rpp);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $opmDATE  = $row['Movement_Date'];
    $comts     = $row['Comments'];
    $Awaiting = $row['Awaiting'];
    $Stage1 = $row['Stage1'];
    $Stage2 = $row['Stage2'];
    $Stage3 = $row['Stage3'];
    $Stage4 = $row['Stage4'];
    $Stage5 = $row['Stage5'];
    $Warehouse = $row['Warehouse'];
    $CLIENT = $row['CLIENT'];
    $SUPPLIER = $row['SUPPLIER'];
    $UIDi     = $row['uInitials'];
    $OIMRID   = $row['OIMR_ID'];

    if ($CLIENT == 0) {
      $sent = $SUPPLIER;
    }else {
      $sent = $CLIENT;
    }
    // $cOIID    = $row['OIID'];
    // $OIDnos   = $row['OIDnos'];
    // $OIIDnos  = $row['OIIDnos'];
    // $opmQTY   = $row['opmQTY'];
    // $oimrREAS = $row['oimrREAS'];

    // echo '<br> Order Item ID is $cOIID : '.$cOIID;
    // echo '<br> User ID is $UID : '.$UID;
    // echo '<br> Order Number is $OIDnos : '.$OIDnos;
    // echo '<br> Order Item Number is $OIIDnos : '.$OIIDnos;
    // echo '<br> Movement Quantity is $opmQTY : '.$opmQTY;
    // echo '<br> Movement is $oimrREAS : '.$oimrREAS;
    // echo '<br> Movement date is $opmDATE : '.$opmDATE;

    // $opmDATE = strtotime($opmDATE);
    // $opmDATE = date('D-d-M-Y (H.i)', $opmDATE);
    // $opmTime = date('H:i', $opmDATE);

    // ADDING colour to the quantity
    if ($Awaiting < 0) { $colA = 'red'; $weiA = 'bold';}else { $colA = 'black'; $weiA = 'bold'; }
    if ($Stage1 < 0) { $col1 = 'red'; $wei1 = 'bold';}else { $col1 = 'green'; $wei1 = 'bold'; }
    if ($Stage2 < 0) { $col2 = 'red'; $wei2 = 'bold';}else { $col2 = 'green'; $wei2 = 'bold'; }
    if ($Stage3 < 0) { $col3 = 'red'; $wei3 = 'bold';}else { $col3 = 'green'; $wei3 = 'bold'; }
    if ($Stage4 < 0) { $col4 = 'red'; $wei4 = 'bold';}else { $col4 = 'green'; $wei4 = 'bold'; }
    if ($Stage5 < 0) { $col5 = 'red'; $wei5 = 'bold';}else { $col5 = 'green'; $wei5 = 'bold'; }
    if ($Warehouse < 0) { $colW = 'red'; $weiW = 'bold';}else { $colW = 'green'; $weiW = 'bold'; }
    if ($sent < 0) { $colW = 'red'; $weiW = 'bold';}else { $colW = 'green'; $weiW = 'bold'; }

    // ADDING colour to the reason
    if (($OIMRID == 3) || ($OIMRID == 4) || ($OIMRID == 5) || ($OIMRID == 6) || ($OIMRID == 7) || ($OIMRID == 8)) {
      $resbc = $opmColfwd;
    }elseif (($OIMRID == 13) || ($OIMRID == 14) || ($OIMRID == 15) || ($OIMRID == 16) || ($OIMRID == 17) || ($OIMRID == 18)) {
      $resbc = $opmColbck;
    }elseif (($OIMRID == 22) || ($OIMRID == 23) || ($OIMRID == 24) || ($OIMRID == 25) || ($OIMRID == 26) || ($OIMRID == 27) || ($OIMRID == 28) || ($OIMRID == 29) || ($OIMRID == 30) || ($OIMRID == 31) || ($OIMRID == 32) || ($OIMRID == 33) || ($OIMRID == 34)) {
      $resbc = $opmColbck;
    }elseif (($OIMRID == 19) || ($OIMRID == 21)) {
      $resbc = $sentCol;
    }elseif (($OIMRID == 39) || ($OIMRID == 43) || ($OIMRID == 45)) {
      $resbc = $opmColreq;
    }elseif (($OIMRID == 40) ||($OIMRID == 10) || ($OIMRID == 46)) {
      $resbc = $opmColrej;
    }elseif (($OIMRID == 41) || ($OIMRID == 44) || ($OIMRID == 47)) {
      $resbc = $opmColapp;
    }else {
      $bc = 'rgba(255, 255, 255, 0)';
    }

    ?>
    <tr style="font-family:monospace;">
      <td style="text-align:left; text-indent:2%; border-right:thin solid grey;"><?php echo "$opmDATE"?> </td>
      <!-- <td style="background-color:<?php echo $resbc ?>"></td> -->
      <?php
      if (strlen($comts) > 45) {
        $comts = substr($comts,0,45);
        ?>
        <td style="text-align:left; text-indent:2%; background-color:<?php echo $resbc ?>; border-right:thin solid grey;"><?php echo "$comts"?> ...</td>
        <?php
      }else {
        ?>
        <td style="text-align:left; text-indent:2%; background-color:<?php echo $resbc ?>; border-right:thin solid grey;"><?php echo "$comts"?></td>
        <?php
      }
      ?>
      <?php
      if ($Awaiting == 0 ) {
        ?>
        <td style="color:<?php echo $colA ?>; font-weight:<?php echo $weiA ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"></td>
        <?php
      }else {
        ?>
        <td style="color:<?php echo $colA ?>; font-weight:<?php echo $weiA ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"><?php echo $Awaiting ?></td>
        <?php
      }
      ?>
      <?php
      if ($Stage1 == 0 ) {
        ?>
        <td style="color:<?php echo $col1 ?>; font-weight:<?php echo $wei1 ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"></td>
        <?php
      }else {
        ?>
        <td style="color:<?php echo $col1 ?>; font-weight:<?php echo $wei1 ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"><?php echo $Stage1 ?></td>
        <?php
      }
      ?>
      <?php
      if ($Stage2 == 0 ) {
        ?>
        <td style="color:<?php echo $col2 ?>; font-weight:<?php echo $wei2 ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"></td>
        <?php
      }else {
        ?>
        <td style="color:<?php echo $col2 ?>; font-weight:<?php echo $wei2 ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"><?php echo $Stage2 ?></td>
        <?php
      }
      ?>
      <?php
      if ($Stage3 == 0 ) {
        ?>
        <td style="color:<?php echo $col3 ?>; font-weight:<?php echo $wei3 ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"></td>
        <?php
      }else {
        ?>
        <td style="color:<?php echo $col3 ?>; font-weight:<?php echo $wei3 ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"><?php echo $Stage3 ?></td>
        <?php
      }
      ?>
      <?php
      if ($Stage4 == 0 ) {
        ?>
        <td style="color:<?php echo $col4 ?>; font-weight:<?php echo $wei4 ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"></td>
        <?php
      }else {
        ?>
        <td style="color:<?php echo $col4 ?>; font-weight:<?php echo $wei4 ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"><?php echo $Stage4 ?></td>
        <?php
      }

      if ($Stage5 == 0 ) {
        ?>
        <td style="color:<?php echo $col5 ?>; font-weight:<?php echo $wei5 ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"></td>
        <?php
      }else {
        ?>
        <td style="color:<?php echo $col5 ?>; font-weight:<?php echo $wei5 ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"><?php echo $Stage5 ?></td>
        <?php
      }

      if ($Warehouse == 0 ) {
        ?>
        <td style="color:<?php echo $colW ?>; font-weight:<?php echo $weiW ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"></td>
        <?php
      }else {
        ?>
        <td style="color:<?php echo $colW ?>; font-weight:<?php echo $weiW ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"><?php echo $Warehouse ?></td>
        <?php
      }

      if ($sent == 0 ) {
        ?>
        <td style="color:<?php echo $colW ?>; font-weight:<?php echo $weiW ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"></td>
        <?php
      }else {
        ?>
        <td style="color:<?php echo $colW ?>; font-weight:<?php echo $weiW ?>; border-right:thin solid grey; text-align:right;  padding-right:0.1%;"><?php echo $sent ?></td>
        <?php
      }
      ?>
      <!-- <td style="  padding-right:0.1%; border-right:thin solid grey;"><?php echo $sent ?></td> -->
      <td style="  padding-right:0.1%;"><?php echo $UIDi ?></td>
    </tr>
    <?php
  }
}
 ?>
</table>

<?php
if (isset($_GET['mp'])) {
}else {
  ?>
  <div style="position:absolute; bottom:0%; right:5%;font-size:200%">
    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      if (isset($_GET['oi'])) {
        ?>
        <a style="color:black; text-decoration:none;" href='<?php echo $urlPage ?>&o=<?php echo $OID ?>&oi=<?php echo $OIID ?>&pa=<?php echo $x ?>'><?php echo $x ?></a>
        <?php
      }elseif (isset($_GET['OI'])) {
        ?>
        <a style="color:black; text-decoration:none;" href='<?php echo $urlPage ?>&o=<?php echo $OID ?>&OI=<?php echo $OIID ?>&pa=<?php echo $x ?>'><?php echo $x ?></a>
        <?php
      }
    } ?>
  </div>
  <?php
}
?>
