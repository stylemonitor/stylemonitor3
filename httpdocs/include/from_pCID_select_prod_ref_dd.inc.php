<?php
include 'dbconnect.inc.php';
// echo '<b>include/from_pCID_select_prod_ref_dd.inc.php</b>';
if (isset($_POST['tPTID'])) {
  $tPTID = $_POST['tPTID'];
}

if (isset($_POST['pPTID'])) {
  $pPTID = $_POST['pPTID'];
}

$CID = $_SESSION['CID'];

// count how many items there are
$sql = "SELECT pr.ID as cPRID
        FROM company c
          , associate_companies ac
          -- , division d
          , prod_ref pr
          , product_type pt
        WHERE c.ID = ?
        AND ac.CID = c.ID
        -- AND d.ACID = ac.ID
        AND pr.ACID = ac.ID
        AND pr.PTID = pt.ID
        AND pt.ID IN (?)
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br><b>FAIL-fpstrd1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $tPTID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $Nrows = $row['cPRID'];
}

// list the items for selection
$sql = "SELECT pr.ID as PRID
          , pr.prod_ref as PRIDn
          , pt.name as PTIDn
          , pt.id as PTID
          , pr.prod_desc PRIDd
        FROM company c
          , associate_companies ac
          -- , division d
          , prod_ref pr
          , product_type pt
        WHERE c.ID = ?
        AND ac.CID = c.ID
        -- AND d.ACID = ac.ID
        AND pr.ACID = ac.ID
        AND pr.PTID = pt.ID
        AND pt.ID IN (?)
        ORDER BY 2
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br><b>FAIL-fpstrd2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $tPTID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $tPRID  = $row['PRID'];
    $tPTID  = $row['PTID'];
    $tPRIDn = $row['PRIDn'];
    $tPRIDd = $row['PRIDd'];
    $tPTIDn = $row['PTIDn'];

    if ($tPTIDn == 'Select Product Type') {
      $tPTIDn = '';
    }else {
      $tPTIDn = $tPTIDn;
    }

    if ($Nrows <> 0) {
      ?>
      <option value="<?php echo $tPRID ?>"><?php echo "$tPRIDn : $tPRIDd" ?></option>
      <?php
    }elseif ($Nrows == 0) {
      ?>
      <option value="">No Styles to Select</option>
      <?php
    }
  }
}
?>
