<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CPID_get_suppliers_mACID.inc.php</b>";

$sql = "SELECT MIN(ac.ID) as mACIDsup
          , ac.name as mACIDsupName
        FROM associate_companies ac
        WHERE ac.CID = (SELECT acc_CID as supCID
				                FROM company_partnerships
				                WHERE ID = ?)
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fcgpc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CPID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $ACID = $row['mACIDsup'];
  $ACIDn = $row['mACIDsupName'];
}
