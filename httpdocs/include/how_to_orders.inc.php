<?php
include 'dbconnect.inc.php';
$sm = '<b>S</b>tyle<b>M</b>onitor';

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'How to ... ORDERS';
$pddhbc = '#e08df4';
include 'page_description_date_header.inc.php';

?>
<div class="" style="position:absolute; top:10%; left:0%; width:100%; background-color:#f4f4f4; text-align:left; text-indent: 1%;">
  <p>The ORDER form is the same for all order types. However, depending upon where you start from, the pre-set information is different.</p>
    <p style="text-indent:2.5%;">If you start from <b>STYLES</b> then the first style is set for you</p>
    <p style="text-indent:2.5%;">If you come from <b>ASSOCIATES</b> then the Client or Supplier is pre-set.</p>
  <br>
  <p>Wherever you start from, you need to enter the following information</p>
  <p><b>Our Reference</b></p>
  <p style="text-indent:2.5%;">This can be anything from a short desrcition of the order to your client/suppliers Order number</p>
  <p><b>Item Description</b></p>
  <p style="text-indent:2.5%;">Again a brief description but this time of the individual item in the order</p>
  <p><b>Quantity</b></p>
  <p style="text-indent:2.5%;">Goes without saying, you need to know how many to make</p>
  <br>
  <p>This next set of information is needed BUT the system will set it if you don't</p>
  <p><b>Order Date</b></p>
  <p style="text-indent:2.5%;">The system will automatically fill in the order date as today UNLESS you put something else in</p>
  <p><b>Item Due Date</b></p>
  <p style="text-indent:2.5%;">The system will automatically fill this in according to your preferences set in <a style="color:blue; font-weight:bold; text-decoration:none;" href="company.php?C&stt"> COMPANY:SETTINGS</a></p>
  <p><b>Item Type</b></p>
  <p style="text-indent:2.5%;">The system will automatically set this to <b>PRODUCTION</b> unless you choose <b>SAMPLE</b></p>
  <br>
  <p>You can add the following information but it's not vital that you do</p>
  <p><b>Client/Supplier Reference</b></p>
  <p style="text-indent:2.5%;">This can be anything from a short desrcition of the order to your client/suppliers Order number</p>
  <br>
  <p>IF you have named your Clients/Suppliers a drop-down menu will appear in the 'TO' section of the order form. This is for you to select the company to whom the order is going. Don't worry if you forget, it will automatically place the order with you but you can change this at a later date if you want to</p>
</div>
