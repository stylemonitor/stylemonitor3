<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_PRID_count_divisions.inc.php</b>";
$sql = "SELECT COUNT(d.ID) as cDID
        FROM prod_ref pr
          , prod_ref_to_div prtd
          , division d
        WHERE pr.ID = ?
        AND prtd.PRID = pr.ID
        AND prtd.DID = d.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fprd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $PRID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $cDID   = $row['cDID'];
}
