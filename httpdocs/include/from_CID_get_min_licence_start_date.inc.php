<?php
include 'dbconnect.inc.php';
// echo "<br><b>inlcude/from_CID_get_min_licence_start_date.inc.php</b>";
$sql = "SELECT MIN(st_date) as mCIDstd
        FROM company_licence
        WHERE CID = ?;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmlsd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $mCIDstd = $row['mCIDstd'];
  // echo "Licence Start date $mCIDstd";
}
