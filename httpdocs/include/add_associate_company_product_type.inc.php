<?php
include 'dbconnect.inc.php';
// echo "add_associate_company_product_type.inc.php";

include 'from_ACID_get_div_and_sect_ID.inc.php';

$UID = $_SESSION['UID'];
?>
<div class="overlay"></div>

<div style="position:absolute; top:30%; height:15%; left:30%; width:40%; font-size:110%; font-weight:bold; border: thin solid grey; border-radius:10px; background-color:pink;">
  Add Product Type to <?php echo $ACIDn ?>
</div>
<div style="position:absolute; top:35.7%; left:31%; width:10%;">
  Product Type
</div>

<form action="include/add_associate_company_product_type_act.inc.php" method="post">
  <input type="hidden" name="UID" value="<?php echo $UID ?>">
  <input type="hidden" name="ACID" value="<?php echo $ACID ?>">
  <input type="hidden" name="DID" value="<?php echo $DID ?>">
  <input type="hidden" name="SID" value="<?php echo $SID ?>">
  <select style="position:absolute; top:35%; height:4%; left:42%; width:27.8%; background-color:<?php echo $ddmCol ?>" name="PTID">
    <option value="">Select Product Type</option>
    <?php
    include 'include/from_CID_select_additional_prod_type.inc.php';
    ?>
  </select>
  <button class="entbtn" type="submit" style="position:absolute; top:40%; left:37%; width:10%;"name="addPT">Add</button>
  <button class="entbtn" type="submit" style="position:absolute; top:40%; left:53%; width:10%;" name="cancelADDpt">Cancel</button>
</form>
