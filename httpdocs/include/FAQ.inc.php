<?php
include 'dbconnect.inc.php';
// echo "<b>include/about.inc.php</b>";

?>
<div class="cpsty" style="top:0%; height:3%; left:0%; width:100%;background-color:#d1d1d1:">
  Some of Your Frequently Asked Questions - Answered
</div>
<?php
if (empty($UID)) {
  ?>
  <div style="position:absolute; top:10%; height:100%; left:5%; width:90%; text-align:left;">
    <br><br>
    <h1>Isn't SM just another PLM</h1>
    <p>No, SM is <b>NOT</b> another <b>P</b>roduct <b>L</b>ifecycle <b>M</b>anagement tool
    <br>Instead it fills a gap by <b>monitoring</b> an order through the sections of a factory, from start to finish, so it is a <b>P</b>roduction <b>L</b>ine <b>M</b>onitor</p>
    <br>
    <h2>Is SM suitable for me as a manufacturer</h2>
    <p><b>YES</b>, SM can be used by any manufacturing company to monitor the progress of any order it has, either sales OR purchases</p>
    <br>
    <h2>Is SM suitable for me as a retailer</h2>
    <p><b>YES</b>, SM can be used by any retailer to monitor the progress of any order it has, either sales OR purchases</p>
    <br>
    <h2>So why should I use SM</h2>
    <p>SM is best used with a PARTNER (another SM member) whereby the retailer can see their orders at a manufacturer who can update them as they progress though their factory
    <br>The retailer can <b>only</b> monitor what is happening with <b>their</b> orders and they cannot alter the production status
    <br><b>No more</b> multiple copies of out of date spreadsheets
    <br><b>No more</b> 'what's happening to my order' phone calls or emails</p>
    <br>
    <h2>We don't have overseas partners</h2>
    <p>Not a issue, SM will look after your orders whether you are a retailer or supplier, regardless of where you, or they, are</p>
    <br>
    <h2>Will anybody be able to see my orders</h2>
    <p><b>NO</b>. They are authorised by your company, OR, by your PARTNERS company, then they will have full visability, if not they will not even know you are on the system</p>
    <br>
    <h2>What do I need to use SM</h2>
    <p>As SM is Cloud based, all you need is a computer with internet access</p>

  </div>
  <img src="include/SM-web02.jpg" style="position:absolute; top:5%; height:90%; left:0%; width:100%; opacity:0.1;">
<?php
}else {
  echo "How it works pages";
}
