<?php
include 'dbconnect.inc.php';
//echo '<b>include/from_CID_select_prod_type.inc.php</b>';
$CID = $_SESSION['CID'];

$sql = "SELECT  d.ID as DID
	        , d.name as DIDn
        FROM division d
        	, associate_companies ac
            , company c
        WHERE c.ID = ?
        AND ac.ID = (SELECT MIN(ac.ID)
             				 FROM associate_companies ac
             				 , company c
		            		 WHERE c.ID = ?
		            	 	 AND ac.CID = c.ID)
				AND d.ACID = ac.ID
        AND ac.CID = c.ID
        ORDER BY d.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcsd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $CID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
	while($row = mysqli_fetch_array($res)){
		$DID = $row['DID'];
		$DIDn = $row['DIDn'];

		echo '<option value="'.$DID.'">'.$DIDn.'</option>';
	}
};
?>
