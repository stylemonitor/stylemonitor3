<?php
include 'dbconnect.inc.php';
// echo "include/from_movement_count_entries.inc.php";

$CID = $_SESSION['CID'];

$sql = "SELECT from_unixtime(opm.inputtime,'%Y-%m-%d') AS ENTRY_DATE
          , count(opm.ID) AS COUNT
-- Movement_Count_report_section_20210223.sql
        FROM order_placed_move opm INNER JOIN order_placed op ON op.ID = opm.OPID
				INNER JOIN order_item_movement_reason oimr ON oimr.ID = opm.OIMRID
        INNER JOIN order_item oi on oi.ID = op.OIID
        INNER JOIN orders o ON o.ID = oi.OID
        INNER JOIN company c ON o.fCID = c.ID
        INNER JOIN company cS ON o.tCID = cS.ID
        INNER JOIN associate_companies ac ON o.fACID = ac.ID
        INNER JOIN associate_companies acS ON o.tACID = acS.ID
        INNER JOIN division d ON o.fDID = d.ID
        INNER JOIN division dS ON o.tDID = dS.ID
        WHERE (o.fCID = ? OR o.tCID = ?)
        AND oimr.ID IN (8,18,19,20,21,22)
        AND from_unixtime(opm.inputtime,'%Y-%m-%d') = ?
-- GROUP BY from_unixtime(opm.inputtime,'%Y-%m-%d')
        ORDER BY ENTRY_DATE DESC
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fmcesw</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $movDate);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $cOPMID = $row['COUNT'];
}
// echo "countWhs = $cOPMID";
