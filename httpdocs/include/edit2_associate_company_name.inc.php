<?php
include 'dbconnect.inc.php';
// echo "<b>include/edit_company_name.inc.php</b>";
$CID   = $_SESSION['CID'];

if (isset($_GET['id'])) {
  include 'include/from_ACID_get_associate_company_details.inc.php';
  $ACID = $_GET['id'];
  $ct = $_GET['ct'];
  $ACIDn = $ACIDn;
  $title = 'Enter the revised name of the associate company';
}

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'Edit Company Name';
$pddhbc = '#fffddd';
include 'page_description_date_header.inc.php';

?>

<div style="position:absolute; top:0%; height:100%; left:0%; width:100%; background-blend-mode:overlay;"></div>

<div style="position:absolute; top:30%; height:40%; left:30%; width:40%; border:2px ridge grey; border-radius:10px; background-color:<?php echo $edtCol ?>; z-index:1;">
  <h1><?php echo $title ?></h1>
  <br>
  <p>Your SMIC will remain the same as before</p>
</div>


<form class="acjSM" style="background-color:#f1f1f1;" action="include/edit2_associate_company_name_act.inc.php" method="POST">
  <!-- <input  type="text" name="CID" value="<?php echo "$CID"; ?>"> -->
  <input  type="hidden" name="CIDn" value="<?php echo "$ACIDn"; ?>">
  <input  type="hidden" name="id" value="<?php echo "$ACID"; ?>">
  <input  type="hidden" name="ct" value="<?php echo "$ct"; ?>">

  <div style="position:absolute; top:28%; left:2%; width:27%; background-color:#f1f1f1; border:none; text-align:right;" ><?php echo "$ACIDn"; ?></div>
  <input autofocus class="edit_user" type="text" style="top:25%;" name="nCIDn" placeholder="<?php echo "$ACIDn"; ?>" value="">

  <button class="entbtn" style="position:absolute; top:22.5%; height:10%; left:80%; width:10%;" type="submit" name="update_associate_name">Update</button>
  <button class="entbtn" style="position:absolute; top:37.5%; height:10%; left:80%; width:10%;" type="submit" name="cancel_associate_name">Cancel</button>
</form>
