<?php
// get the users password reset questions
include 'include/dbconnect.inc.php';
// echo "include/select_user_pwd_reset_question.inc.php";

$sql = "SELECT fpua.ID as qID
          , fpq.question as uQT
          , fpua.ID as AID
          , fpua.qans as uANS
        FROM forgotten_password_questions fpq
          , forgotten_password_user_answers fpua
        WHERE fpua.UID = ?
        AND fpua.FPQID = fpq.ID;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  // echo '<b>FAIL1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $UID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $qID = $row['qID'];
    $uQT = $row['uQT'];
    $AID = $row['AID'];
    $uANS = $row['uANS'];
    echo "<option value=".$qID.">".$uQT."</option>"
  }
}
