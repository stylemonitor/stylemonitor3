<div style="position:absolute; top:2%; height:2.5%; right:5%; width:90%; background-color:#c9d1ce;">
  ORDER NOTES
  <br><br>
  <p>Select either
  <br><b>Sample</b> OR <b>Production</b>
  <br>then</p>
  <br>
  <p>Select from <b><br>Sales <br>Purchases <br>Partner Sales <br>Partner Purchases</p></b>
  <br>
  <p>to see what orders you have for each combination</p>
  <br>
  <p>Moving along the top tabs changes the time frame</p>
</div>
<div style="position:absolute; top:50%; height:2.5%; right:20%; width:60%; background-color:#c9d1ce;">ITEM REVIEW
  <br>click on the  Item number</div>
<div style="position:absolute; top:60%; height:2.5%; right:20%; width:60%; background-color:#c9d1ce;">MOVE WORK
  <br>click on the quantity where the order row meets the section column</div>
