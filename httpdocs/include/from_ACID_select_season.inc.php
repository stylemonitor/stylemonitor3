<?php
include 'dbconnect.inc.php';
$CID = $_SESSION['CID'];
//echo '<b>include/from_ACID_select_season.inc.php</b>';
// $ACID = $_SESSION['ACID'];
$sql = "SELECT sn.ID as SNID
	        , sn.name as SNIDn
        FROM season sn
        	, associate_companies ac
        WHERE ac.ID = (SELECT MIN(ac.ID) as mACID
								        FROM associate_companies ac
								          , company c
								        WHERE c.ID = ?
								        AND ac.CID = c.ID)
        AND sn.ACID = ac.ID
        AND sn.name NOT IN ('Select Season')
        ORDER BY sn.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  // $row = mysqli_fetch_array($result);
	while($row = mysqli_fetch_array($res)){
	  $SNID = $row['SNID'];
	  $SNIDn = $row['SNIDn'];

	  // echo '<option value="'.$PTID.'">'.$PTIDn.' : '.$PTIDsc.'</option>';
	  echo '<option value="'.$SNID.'">'.$SNIDn.'</option>';
	}
};
?>
