<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_count_prod_ref.inc.php</b>";

$sql = "SELECT COUNT(pr.ID) as cPRID
        FROM prod_ref pr
          , company c
          , associate_companies ac
        WHERE pr.PTID = ?
        AND c.ID = ?
        AND pr.ACID = ac.ID
        AND ac.CID = c.ID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fccpt</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $PTID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cPRID = $row['cPRID'];
}
