<?php
// echo "<br><b>include/check_product_description_valid.inc.php</b>";
// echo "new descriptionis $nPTIDn";

if ($PTIDn <> $nPTIDn) {
  // check if the derscription is already in use
  $sql = "SELECT ID as PTID
            , scode as PTIDsc
            , name as PTIDn
          FROM product_type
          WHERE CID = ?
          AND name = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-cpscv</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $CID, $nPTIDn);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_assoc($result);
    $oPTID = $row['PTID'];
    $oPTIDsc = $row['PTIDsc'];
    $oPTIDn = $row['PTIDn'];
  }

  $oPTIDsc = "$oPTIDsc / $oPTIDn";

  if (empty($PTID)) {
    // echo "Short code in use";
    header("Location:../styles.php?S&spe&p=$oPTID&bpt=$oPTIDsc");
    exit();
  }
}
