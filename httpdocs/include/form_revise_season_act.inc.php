<?php
include 'dbconnect.inc.php';
// echo '<b>include/form_revise_collection_act.inc.php</b>';
if (!isset($_POST['revName']) && !isset($_POST['canName'])){
  // echo 'Incorrect method used';
  header('Location:../index.php');
  exit();
}elseif (isset($_POST['canName'])){
  // echo 'Cancel Season Adding';
  header('Location:../styles.php?S&cor');
  exit();
}elseif (isset($_POST['revName'])){
  include 'from_CID_count_collection.inc.php';

  $UID = $_SESSION['UID'];
  $seas = $_POST['seas'];
  $SID = $_POST['SID'];
  $SIDn = $_POST['SIDn'];

  $td = date('U');

  // echo '<br>The collection ID ($CLID) is : <b>'.$CLID.'</b>';
  // echo '<br>The original collection name  ($CLIDn) was : <b>'.$CLIDn.'</b>';
  // echo '<br>The revised collection name  ($coll) is : <b>'.$coll.'</b>';
  // echo '<br>The user ID ($UID) is : <b>'.$UID.'</b>';


  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to

    // register the original name in log_collection
    $sql = "INSERT INTO log_seasons
              (SID, oriName, UID, inputtime)
            VALUES
              (?,?,?,?)
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-frsa</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $SID, $SIDn, $UID, $td);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
    }

    // Update the collection details
    $sql = "UPDATE season
            SET name = ?
            WHERE ID = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-frsa2</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $seas, $SID);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
      $CLID = $row['ID'];
    }

  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  // echo ' <br>The product type has been registered';
  header("Location:../styles.php?S&ser");
  exit();
}
?>
