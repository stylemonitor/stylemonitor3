<?php
// gets a count of the orders in a Section
// vaiarble IN (x,y,x1,y1)

SELECT COUNT(opm.OPID)
FROM order_placed_move opm
, order_item_movement_reason oimr
WHERE oimr.ID IN (4,14)
AND opm.OIMRID = oimr.ID

// gets the actual order ID as above
SELECT o.ID
, o.orNos as OIDno
, oi.ord_item_nos as OIIDno
FROM order_placed_move opm
, order_placed op
, order_item_movement_reason oimr
, order_item oi
, orders o
WHERE oimr.ID IN (4,14)
AND opm.OIMRID = oimr.ID
AND opm.OPID = op.ID
AND op.OIID = oi.ID
AND oi.OID = o.ID

// need to now get the quantity for each order
SELECT SUM(opm.omQty*oimr.numVal)
FROM order_placed_move opm
, order_placed op
, order_item_movement_reason oimr
, order_item oi
, orders o
WHERE oimr.ID IN (8,18)
AND opm.OIMRID = oimr.ID
AND opm.OPID = op.ID
AND op.OIID = oi.ID
AND oi.OID = o.ID

// not sure about
SELECT sum(opm.omQty*oimr.numval)
FROM company c
, associate_companies ac
, division d
, orders o
, order_item oi
, order_item_qty oiq
, order_placed op
, order_placed_move opm
, order_item_movement_reason oimr
WHERE c.ID = 2
AND ac.CID = c.ID
AND d.ACID = ac.ID
AND o.DID = d.ID
AND oi.OID = o.ID
AND op.OIID = oi.ID
AND opm.OPID = op.ID
AND opm.OIMRID = oimr.ID
AND oimr.ID IN (2,3,13,23,19)
