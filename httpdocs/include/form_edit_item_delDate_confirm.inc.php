<?php
include 'dbconnect.inc.php';
// echo "<br><b>form_edit_item_delDate_confirm.inc.php</b>";
$UID = $_SESSION['UID'];
$CID = $_SESSION['CID'];
$td = date('U');

if (!isset($_POST['YesBtn']) && !isset($_POST['NoBtn']) && !isset($_POST['chgBtn'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['NoBtn'])) {
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  header("Location:../home.php?H&rt3&o=$OID");
  exit();
}elseif (isset($_POST['chgBtn'])) {
  echo "CHANGE REQUEST";
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  // header("Location:../home.php?H&rt3&o=$OID&OI=$OIID&eit=$OIID&qty");
  // exit();
}elseif (isset($_POST['YesBtn'])) {
  // echo "<br>CHANGE ITEM ON ORDER";
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  $OIID = $_POST['OIID'];
  $OTID = $_POST['OTID'];
  $OPID = $_POST['OPID'];
  $OIDDIDd = $_POST['OIDDIDd'];
  $delDate = $_POST['Ndate'];
  $canReas = $_POST['canReas'];
  $td = date('U');

  $Nqty = 0;

  $delDate = strtotime($delDate);
  // echo "Original Del date = $OIDDIDd:: Revised Del date=$delDate";

  // echo "<br>OID = $OID :: OIID = $OIID :: OTID = $OTID :: OPID = $OPID :: oiQty =  $oiQty :: delDate =  $delDate :: Nqty = $Nqty :: canReas =  $canReas :: date =  $td";

  include 'from_OIID_get_order_details.inc.php';

  // echo "<br>TWO // OID = $OID :: OIID = $OIID :: OTID = $OTID :: OPID = $OPID :: oiQty =  $oiQty :: delDate =  $delDate :: Nqty = $Nqty :: canReas =  $canReas :: date =  $td";

  $OIDDIDdDay = date('d-M-Y', $OIDDIDd);
  $NdateDay = date('d-M-Y',$delDate);

  if ($delDate < $OIDDIDd) {
    // REDUCTION IN DELIVERY DATE
    $days = ceil(($OIDDIDd - $delDate)/86400)-1;

    $accReas = "Delivery date reduced by $days days";
    $reaBoxLine = "You are requesting delivery date reduction <br><br>of <b>$days days</b>";

  }elseif ($delDate > $OIDDIDd) {
    // EXTENSION OF DELIVERY DATE
    $days = ceil(($delDate - $OIDDIDd)/86400)-1;
    $reaBoxLine = "You are requesting delivery date extension <br><br>of <b>$days days</b>";
  }

  $OIMRID = 40;
  ?>
  <div class="overlay"></div>

  <div style="position:absolute; top:32%; height:30%; left:30%; width:40%; font-size: 120%; background-color: pink; border:1px solid black; border-radius:10px; z-index:3;">
   <?php echo $reaBoxLine ?>
   <br><br>from <?php echo $OIDDIDdDay  ?> to  <?php echo $NdateDay ?>
  </div>

  <div class="" style="position:absolute; top:51%; left:30%; width:40%; font-size: 120%; font-weight: bold; text-align: center; border-top:1px solid black; z-index:3;">
   <?php echo $canReas ?>
  </div>

  <form style="z-index:3;" action="include/form_edit_item_confirm_deldate_act.inc.php" method="post">
   <input type="hidden" name="OID" value="<?php echo $OID ?>">
   <input type="hidden" name="oicCID" value="<?php echo $oicCID ?>">
   <input type="hidden" name="OTID" value="<?php echo $OTID ?>">
   <input type="hidden" name="OIID" value="<?php echo $OIID ?>">
   <input type="hidden" name="OIMRID" value="<?php echo $OIMRID ?>">
   <input type="hidden" name="oiQty" value="0">
   <input type="hidden" name="OPID" value="<?php echo $OPID ?>">
   <input type="hidden" name="canReas" value="<?php echo $canReas ?>">
   <input type="hidden" name="del" value="<?php echo $delDate ?>">
   <input type="hidden" name="Nqty" value="0">
   <input type="hidden" name="eit" value="4">
   <input type="hidden" name="incItem" value="1">

  <button class="entbtn" type="submit" style="position:absolute; top:56%; left:32.5%; width:10%; background-color:<?php echo $fsvCol ?>; z-index:3;" name="YesBtn">Confirm888</button>
   <button class="entbtn" type="submit" style="position:absolute; top:56%; left:45%; width:10%; background-color:<?php echo $edtCol ?>; z-index:3;" name="chgBtn">Change</button>
   <button formnovalidate class="entbtn" type="submit" style="position:absolute; top:56%; left:57.5%; width:10%; background-color:<?php echo $fcnCol ?>; z-index:3;" name="NoBtn">Cancel</button>
  </form>
  <?php
}
