<?php
echo "input_additional_product_type.inc.php";

?>

<div style="position:absolute; top:30%; height:20%; left:15%; width:70%; background-color:#f1f1f1; font-size:150%; border: medium solid grey; border-radius:10px;">
  <h1>Additional Product Type for <?php echo $ACIDn ?></h1>
</div>

<form action="include/input_additional_product_type_act.inc.php" method="POST">

  <!-- <input type="text" name="CID" value="<?php echo "$CID"; ?>"> -->
  <input type="hidden" name="ACID" value="<?php echo "$ACID"; ?>">
  <input type="hidden" name="ct" value="<?php echo "$ct"; ?>">
  <input type="hidden" name="AID" value="<?php echo "$AID"; ?>">
  <input type="hidden" name="UID" value="<?php echo "$UID"; ?>">

  <div style="position:absolute; top:38%; left:15.5%; width:20%; font-weight:bold; text-align:right;">
    Additional Product Type
  </div>
  <select style="position:absolute; top:37%; height:4%; left:40%; width:40.8%; background-color:<?php echo $ddmCol ?>" name="PTID">
    <option value="">Select Additional Product Type</option>
    <?php
    include 'include/from_CID_select_additional_prod_type.inc.php';
    ?>
  </select>

  <button class="entbtn" style="position:absolute; top:45%; left:35%; width:10%;" type="submit" name="addPTID">Add</button>
  <button formnovalidate class="entbtn" style="position:absolute; top:45%; left:55%; width:10%;" type="submit" name="cancel">Cancel</button>
</form>
