<?php
// echo "<br><b>form_edit_item_delDate_partner_act.inc.php</b>";

$canReasN = $canReas;

// check if there is an open change request
include 'action/Count_OIC_from_OIID_delDate.act.php';

// echo "<br>cOICID = $cOICID :: OICID = $OICID";

// echo "<br>order_item_change :: $OICID";
if ($cOICID == 1) {
  // update the OIC file to close the previous OIC entry
  include 'action/update_OIC_table.act.php';

  // add the cancel comment the open delivery date change
  $OIMRID = 41;
  $canReas = "Request cancelled";
  include 'action/insert_into_order_place_move.act.php';
}

// add the new del date change request
$OIMRID = 39;
$canReas = $canReasN;
include 'action/insert_into_order_place_move.act.php';

// get the new OPMID
include 'action/select_OPMID.act.php';

// get the input users CID as reqCID
include 'action/select_CID_for_user.act.php';

$Nqty = 0;
include 'action/insert_into_order_item_change_table.act.php';

// echo "<br>Added to the order placed move table, now needs the other sides approval";
// echo "<br>send out email informing PARTNER of the change of address";

header("Location:../home.php?H&rt3&o=$OID&OI=$OIID");
exit();
