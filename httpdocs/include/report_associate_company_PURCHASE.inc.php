<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "<b>include/report_associate_company.inc.php</b>";
// get the information from the db
$CID = $_SESSION['CID'];
include 'from_CID_count_associates_clients.inc.php';
include 'from_CID_count_associates_supplier.inc.php';
// include 'from_ACID_count_associate_orders_open.inc.php';
// include 'from_ACID_count_associate_orders_closed.inc.php';

if (isset($_GET['acs'])) {
  $pddh   = 'Suppliers - Associates/Partners';
  $pddhbc = $purAssCol;
  $rpp    = 25;
  $tdiv   = 9;
  $ttab   = 12.1;
  $pageTab = '90%';
  $ACIDct  = 5;
  $count = ($cACIDs);
  $hc = $purAssCol;
}elseif (isset($_GET['acn'])) {
  $rpp    = 5;
  $tdiv = 55;
  $ttab = 58;
  $pageTab = '90%';
  $ACIDct  = '5';
  $count = ($cACIDs);
  $hc = $purAssCol;
}

// want to add a count of each companies divisions, orders open and closed
// need Andrew to do the sql!!!

// $r sets the rows per page
// Check which page we are on
// Check the URL to see what page we are on
if (isset($_GET['start'])) {
  $start = $_GET['start'];
}else{
  $start = 0;
}

if ($start > 1) {
  $start = ($start * $rpp) - $rpp;
}else {
  $start = 0;
}
// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($count / $rpp);
// $PTV = ceil($PTV);

// if (isset($_GET['AS'])) {
//   $type = 'AS';
//   $bc = $purAssCol;
// }

// want to add a count of each companies divisions, orders open and closed
// need Andrew to do the sql!!!

if ($count == 0) {?>
  <div class="cpsty" style="position:absolute; top: <?php echo $tdiv ?>%; height:3%; left:0%; width:100%; background-color:<?php echo $hc ?>;"><?php echo "You have NO Associate SUPPLIER Companies (excluding you)" ?></div>
  <?php
}elseif ($count == 1) {
  ?>
  <div class="cpsty" style="position:absolute; top: <?php echo $tdiv ?>%; height:3%; left:0%; width:100%; background-color:<?php echo $hc ?>;"><?php echo "You have $count Associate SUPPLIER Company (excluding you)" ?></div>
  <?php
}else {
  ?>
  <div class="cpsty" style="position:absolute; top: <?php echo $tdiv ?>%; height:3%; left:0%; width:100%; background-color:<?php echo $hc ?>;"><?php echo "You have $count SUPPLIER Companies" ?></div>
  <?php
}
?>
<table class="trs" style="position:absolute; top:<?php echo $ttab ?>%; left:0%; width:100%;">
  <tr>
    <th style="width:8%; text-align:left; text-indent:2%;"></th>
    <th style="width:32%; text-align:left; text-indent:2%;">Associate Company</th>
    <th style="width:30%; text-align:left; text-indent:2%;">Country</th>
    <th style="width:13%; text-align:left; text-indent:2%;">Product Types</th>
    <th style="width:7%;">Orders</th>
    <th style="width:10%;">Added</th>
  </tr>

<?php

include 'sql/order_counts_PURCHASES.sql.php';

$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-racS</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sssss", $CID, $CID, $CID, $start, $rpp);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $ASACID = $row['ASACID'];
    $ACIDn = $row['ASSOC_CO'];
    $CTID = $row['CTID'];
    $CTIDn = $row['CO_TYPE'];
    $CYIDn = $row['CYIDn'];
    $stDate = $row['DATE_JOINED'];
    $Ptype = $row['PTYPE'];
    $cOIDc = $row['PPCHASE_ORDER'];
    $stDate = date('M-Y',$stDate);
    $OTID = $row['OTID'];

    if ($CTID == 5) {
      $OTID = 2;
    }elseif (($CTID == 7) && ($OTID == 4)) {
      $OTID = 4;
    }

    if ($OTID == 0) {
      $coType = "";
      $coTypeCol = $purAssCol;
    }
    if ($OTID == 1) {
      $coType = "Associate";
      $coTypeCol = $salAssCol;
      $coReview = "associates.php?A&acs&pur&fao=$ASACID";
    }elseif ($OTID == 2) {
      $coType = "Associate";
      $coTypeCol = $purAssCol;
      $coReview = "associates.php?A&acs&pur&fao=$ASACID";
    }elseif ($OTID == 3) {
      $coType = "Partner";
      $coTypeCol = $salParCol;
      $coReview = "partners.php?P&pas";
    }elseif ($OTID == 4) {
      $coType = "Partner";
      $coReview = "partners.php?P&pas";
      $coTypeCol = $purParCol;
    }

?>
  <tr>
    <td style="background-color:<?php echo $coTypeCol ?>"><?php echo $coType ?></td>
    <td style="text-align:left; text-indent:2%;"><a style="color:blue; text-decoration:none;"href="<?php echo $coReview ?>"><?php echo $ACIDn ?></a></td>
    <td style="text-align:left; text-indent:2%;"><?php echo $CYIDn ?></td>
    <td style="text-align:left; text-indent:2%;"><?php echo $Ptype ?></td>
    <td><a class="hreflink" href="associates.php?A&asf&id=<?php echo $ASACID ?>"><?php echo $cOIDc ?></a></td>
    <td><?php echo $stDate ?></td>
  </tr>
  <?php
  } ?>
</table>
<div style="position:absolute; top:<?php echo $pageTab ?>; height:5%; left:85%; width:10%;font-size: 200%; font-weight: bold; z-index:1;">
<?php
}
if (isset($_GET['acn'])) {
  if ($count > $rpp) {
    // $count = $count - $rpp;
    echo "<a style='color:blue; text-decoration:none;' href='?A&acs'>MORE</a>";
  }else {
    // don't show anything!;
    // echo "string";
  }
}elseif (isset($_GET['acs'])) {
  for ($x = 1 ; $x <= $PTV ; $x++){
    echo "<a style='color:blue; text-decoration:none;' href='?A&acs&start=$x'>  $x  </a>";
    // echo "<a style='color:black; text-decoration:none;' href='?$page&tab=$sp&od=$x'>  $x  </a>";
  }
}
?>
</div>
