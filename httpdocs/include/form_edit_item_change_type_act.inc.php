<?php
session_start();
include 'dbconnect.inc.php';
$UID = $_SESSION['UID'];
$CID = $_SESSION['CID'];
$td = date('U');
// echo "<br><b>include/form_edit_item_change_type_act.inc.php</b>";

if (!isset($_POST['YesBtn']) && !isset($_POST['NoBtn'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['NoBtn'])) {
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  header("Location:../home.php?H&o=$OID&OI=$OIID&eit=$OIID");
  exit();
}elseif (isset($_POST['YesBtn'])) {
  // echo "Change item type";
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  $samProd = $_POST['samProd'];
  $canReas = $_POST['canReas'];
  $td = date('U');
  // echo "OIID=$OIID :: samProd = $samProd :: ";
  include 'from_OIID_get_order_details.inc.php';
  // echo "OPID=$OPID";
  include 'action/insert_into_log_order_item.act.php';

  if ($samProd == 1) {
    $samProd = 2;
  }elseif ($samProd == 2) {
    $samProd = 1;
  }

  include 'action/update_order_item_samProd.act.php';
  include 'action/insert_into_OPM_samProd.act.php';

  header("Location:../home.php?H&o=$OID&OI=$OIID");
  exit();
}
