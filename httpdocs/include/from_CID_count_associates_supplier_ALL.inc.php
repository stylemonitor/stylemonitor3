<?php
include 'include/dbconnect.inc.php';
// echo "include/from_CID_count_associates_supplier_ALL.inc.php";

$CID = $_SESSION['CID'];

$sql = "SELECT COUNT(ac.ID) AS cACID
        FROM associate_companies ac
        , company c
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND ac.CTID IN (5,6)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgms</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($res);
  $cACIDsALL = $row['cACID'];
  global $cACIDsALL;
}
