<?php
// echo "order_counts.sql.php";

$sql = "SELECT *
-- SQL is order_counts.sql
FROM
(
-- This section returns details of companies with orders
SELECT DISTINCT c.ID AS COMPANY
       , ac.ID AS ASACID
       , ac.name AS ASSOC_CO
       , ct.ID AS CTID
       , ct.name AS CO_TYPE
       , cy.ID AS CYID
       , cy.name AS CYIDn
       , ac.inputtime AS DATE_JOINED
       , IF(o.OTID IS NULL,'N/A',o.OTID) AS OTID
       , IF(ot.name IS NULL,'N/A',ot.name) AS OTYPE
       , IF(COUNT(DISTINCT pt.ID) = 1,pt.name,'Multiple') AS PTYPE
       , IF(CASE WHEN o.OTID IN(2,4) THEN COUNT(DISTINCT o.ID) END IS NULL,0,CASE WHEN o.OTID IN(2,4) THEN COUNT(DISTINCT o.ID) END) AS PCHASE_ORDER
       , IF(CASE WHEN o.OTID IN(1,3) THEN COUNT(DISTINCT o.ID) END IS NULL,0,CASE WHEN o.OTID IN(1,3) THEN COUNT(DISTINCT o.ID) END) AS SALES_ORDER
       , IF(CASE WHEN o.OTID IN(2,4) THEN COUNT(DISTINCT oi.ID) END IS NULL,0,CASE WHEN o.OTID IN(2,4) THEN COUNT(DISTINCT oi.ID) END) AS PCHASE_ITEMS
       , IF(CASE WHEN o.OTID IN(1,3) THEN COUNT(DISTINCT oi.ID) END IS NULL,0,CASE WHEN o.OTID IN(1,3) THEN COUNT(DISTINCT oi.ID) END) AS SALES_ITEMS
FROM company c
     INNER JOIN associate_companies ac ON c.ID = ac.CID
     INNER JOIN country cy ON cy.ID = ac.CYID
     LEFT JOIN company_type ct ON ac.CTID = ct.ID
     LEFT OUTER JOIN orders o ON ac.ID = o.tACID
     LEFT OUTER JOIN order_item oi ON o.ID = oi.OID
     LEFT OUTER JOIN order_type ot ON o.OTID = ot.ID
     LEFT OUTER JOIN product_type pt ON c.ID = pt.CID
WHERE pt.name != 'Select Product Type'
  AND (o.tCID = ? OR o.fCID = ?)
  AND oi.itComp = 0
  AND ct.ID = ?
GROUP BY ac.ID, o.OTID
UNION ALL
-- This next section returns only those companies with no orders
SELECT *
FROM
(
SELECT DISTINCT c.ID AS COMPANY
       , ac.ID AS ASACID
       , ac.name AS ASSOC_CO
       , ct.ID AS CTID
       , ct.name AS CO_TYPE
       , cy.ID AS CYID
       , cy.name AS CYIDn
       , ac.inputtime AS DATE_JOINED
       , IF(o.OTID IS NULL,'N/A',o.OTID) AS OTID
       , IF(ot.name IS NULL,'N/A',ot.name) AS OTYPE
       , IF(COUNT(DISTINCT pt.ID) = 1,pt.name,'Multiple') AS PTYPE
       , IF(CASE WHEN o.OTID IN(2,4) THEN COUNT(DISTINCT o.ID) END IS NULL,0,CASE WHEN o.OTID IN(2,4) THEN COUNT(DISTINCT o.ID) END) AS PCHASE_ORDER
       , IF(CASE WHEN o.OTID IN(1,3) THEN COUNT(DISTINCT o.ID) END IS NULL,0,CASE WHEN o.OTID IN(1,3) THEN COUNT(DISTINCT o.ID) END) AS SALES_ORDER
       , IF(CASE WHEN o.OTID IN(2,4) THEN COUNT(DISTINCT oi.ID) END IS NULL,0,CASE WHEN o.OTID IN(2,4) THEN COUNT(DISTINCT oi.ID) END) AS PCHASE_ITEMS
       , IF(CASE WHEN o.OTID IN(1,3) THEN COUNT(DISTINCT oi.ID) END IS NULL,0,CASE WHEN o.OTID IN(1,3) THEN COUNT(DISTINCT oi.ID) END) AS SALES_ITEMS
FROM company c
     INNER JOIN associate_companies ac ON c.ID = ac.CID
     INNER JOIN country cy ON cy.ID = ac.CYID
     LEFT JOIN company_type ct ON ac.CTID = ct.ID
     LEFT OUTER JOIN orders o ON ac.ID = o.tACID
     LEFT OUTER JOIN order_item oi ON o.ID = oi.OID
     LEFT OUTER JOIN order_type ot ON o.OTID = ot.ID
     LEFT OUTER JOIN product_type pt ON c.ID = pt.CID
WHERE pt.name != 'Select Product Type'
GROUP BY ac.ID, o.OTID
) NO_ORDERS
WHERE NO_ORDERS.OTID = 'N/A'
  AND NO_ORDERS.COMPANY = ?
  AND NO_ORDERS.CTID = ?
) TOTALS
ORDER BY TOTALS.ASSOC_CO
LIMIT ?,?
;";
