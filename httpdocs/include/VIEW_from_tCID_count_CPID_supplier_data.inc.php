<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/VIEW_from_tCID_count_CPID_supplier_data.inc.php";
// COUNT the number of associate companies AND divisions for the member company
$CID = $_SESSION['CID'];

$sql = "SELECT cc.CL_ID as cli_CID
          , cc.CL_ASSOC_CO as ccli_ACID
          , cc.CL_ASSOC_DIV as ccli_DID
          , c.name as CIDn
        FROM client_count cc
       	  , company c
        WHERE cc.CL_ID = ?
        AND c.ID = cc.CL_ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-vftccsd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $tCID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $sup_CID   = $row['cli_CID'];
  $csup_ACID = $row['ccli_ACID'];
  $csup_DID = $row['ccli_DID'];
  $sup_CIDn  = $row['CIDn'];
}
