<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_partners.inc.php";
include 'from_CID_count_company_partners_pending.inc.php'; // $cpCPID
include 'from_CID_count_company_partners_requested.inc.php'; // $crCPID

// $ACID = $_SESSION['ACID'];
$CID = $_SESSION['CID'];
$td = date('U');

// SETS the ORDER unless changed
if(isset($_GET['or'])){ $ord = $_GET['or']; }else{ $ord = 'c.name'; }
// SETS the SORT sequence at ASC unless changed to DESC
if(isset($_GET['so'])){ $sort = $_GET['so']; }else{ $sort = 'ASC'; }

if (isset($_GET['H'])) {$hc = '5199fc'; }
if (isset($_GET['P'])) {$hc = 'e5ff1c'; }


// echo "<br>**** count_orders count : $cPTID ****";
  if ($crCPIDr == 0) {
    ?>
    <!-- <caption class="cpsty" style="background-color:#<?php echo $hc ?>"><?php echo "We no Partnerships requests pending" ?></caption> -->
    <?php
  }else {
    // Check the URL to see what page we are on
    if (isset($_GET['pa'])) { $pa = $_GET['pa']; }else{ $pa = 0; }

    // Check which page we are on
    if ($pa > 1) { $start = ($pa * $rpp) - $rpp; }else { $start = 0; }
    // Set the rows per page
    $rpp = 20;

    // How many pages will this give us Pages To View $PTV
    // ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
    $PTV = ($cpCPID / $rpp);

    // echo '<br>The number of pages will be : '.$PTV;
    $PTV = ceil($PTV);
    // echo '<br>The number of pages will be : '.$PTV;

    // echo '<br>Start record is : '.$start;
  ?>

  <!-- Partners YOU have requested to join with you -->
  <div class="cpsty" style="background-color:#<?php echo $hc ?>"><?php echo "We have requested $crCPIDr Partnerships" ?></div>
    <table class="trs" style="position:relative; top:0%;">

      <tr>
        <th><a class="hreflink" href="?P&pap&or=c.name&so=<?php echo $sort ?>">Company</a></th>
        <!-- <th><a class="hreflink" href="?P&pap&or=c.SMIC&so=<?php echo $sort ?>">SMIC</a></th> -->
        <th><a class="hreflink" href="?P&pap&or=ct.name&so=<?php echo $sort ?>">We are their</a></th>
        <th><a class="hreflink" href="?P&pap&or=cp.inputtime&so=<?php echo $sort ?>">Date Instigated</a></th>
      </tr>
      <?php

  $sql = "SELECT cp.ID as CPID
          	, cp.rel as CPIDrel
            , cp.req_CID as CPIDr
            , c.name as pCIDn
            , c.SMIC as pCIDs
            , ct.name as CTIDn
            , cp.inputtime as CPIDt
          FROM company_partnerships cp
          	, company c
            , company_type ct
          WHERE ((cp.acc_CID = ?) || (cp.req_CID = ?))
          AND cp.req_CID = c.ID
          AND cp.rel = ct.ID
          AND cp.insCID = ?
          AND cp.insCID NOT IN (0)
          AND cp.active = 0
          ORDER BY $ord $sort
          LIMIT $start, $rpp
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-rp1</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $CID);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    // if ($sort==DESC) { $sort = ASC } else { $sort = DESC }
    $sort == DESC ? $sort = ASC : $sort = DESC;
    while($row = mysqli_fetch_array($res)){
      $CPID = $row['CPID'];
      $CPIDrel = $row['CPIDrel'];
      $CPIDr = $row['CPIDr'];
      $CTIDn = $row['CTIDn'];
      $pCIDn = $row['pCIDn'];
      $pCIDs = $row['pCIDs'];
      $CPIDt = $row['CPIDt'];

      // this needs sorting to give the other companies name not OURS!!!!
      if ($CPIDr == $CID) {$pCIDn = $pCIDn;}

      $CPIDt = date('d-M-Y', $CPIDt);

      ?>
      <tr>
        <td style="text-align:left; text-indent:5%;"><?php echo "$pCIDn"; ?></td>
        <!-- <td style="text-align:left; text-indent:5%;"><?php echo "$CPID-$pCIDn"; ?></td> -->
        <!-- <td style="text-align:left; text-indent:5%;"><?php echo "$pCIDs"; ?></td> -->
        <td style="text-align:left; text-indent:5%;"><?php echo "$CTIDn"; ?></td>
        <td style="text-align:left; text-indent:5%;"><?php echo "$CPIDt"; ?></td>
      </tr>
    <?php
    }
  }
  ?>
  </table>

  <?php
  }
  if ($cpCPID == 0) {
    ?>
    <div class="cpsty" style="background-color:#<?php echo $hc ?>"><?php echo "We have no outstanding Partnerships requests" ?></div>
    <?php

  }else{
    // Check the URL to see what page we are on
    if (isset($_GET['pa'])) { $pa = $_GET['pa']; }else{ $pa = 0; }

    // Check which page we are on
    if ($pa > 1) { $start = ($pa * $rpp) - $rpp; }else { $start = 0; }

    // Set the rows per page
    $rpp = 20;

    // How many pages will this give us Pages To View $PTV
    // ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
    $PTV = ($cpCPID / $rpp);

    // echo '<br>The number of pages will be : '.$PTV;
    $PTV = ceil($PTV);
    // echo '<br>The number of pages will be : '.$PTV;

    // echo '<br>Start record is : '.$start;
    ?>

    <!-- Members who have REQUESTED you to join them -->
    <div class="cpsty" style="position:relative; top:40%;
    background-color:<?php echo $hc; ?>"><?php echo "We have been asked to form $cpCPIDp Partnerships" ?></div>
    <table class="trs" style="position:relative; top:40%;">

      <tr>
        <th><a class="hreflink" href="?P&pap&or=c.name&so=<?php echo $sort ?>">Company</a></th>
        <!-- <th><a class="hreflink" href="?P&pap&or=c.SMIC&so=<?php echo $sort ?>">SMIC</a></th> -->
        <th><a class="hreflink" href="?P&pap&or=ct.name&so=<?php echo $sort ?>">We are their</a></th>
        <th><a class="hreflink" href="?P&pap&or=cp.inputtime&so=<?php echo $sort ?>">Date Instigated</a></th>
        <th></th>
        <th></th>
      </tr>
      <?php

    $sql = "SELECT cp.ID as CPID
              , cp.rel as CPIDrel
              , cp.req_CID as CPIDr
              , cp.inputtime as CPIDt
              , c.name as pCIDn
              , c.SMIC as pCIDs
              , ct.name as CTIDn
            FROM company_partnerships cp
              , company c
              , company_type ct
            WHERE cp.acc_CID = c.ID
            AND ((cp.req_CID = ?) || (cp.acc_CID = ?))
            -- WHERE cp.req_CID = c.ID
            AND cp.insCID NOT IN (?)
            -- AND cp.insCID NOT IN (0)
            AND cp.rel = ct.ID
            AND cp.active = 0
            ORDER BY cp.inputtime
            ;";
            // AND pt.name NOT IN ('Any')";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-rp2</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $CID);
      mysqli_stmt_execute($stmt);
      $res = mysqli_stmt_get_result($stmt);
      while($row = mysqli_fetch_array($res)){
        $CPID = $row['CPID'];
        $CPIDrel = $row['CPIDrel'];
        $CPIDr = $row['CPIDr'];
        $pCIDn = $row['pCIDn'];
        $pCIDs = $row['pCIDs'];
        // $CTIDn = $row['CTIDn'];
        $CPIDt = $row['CPIDt'];
        // echo "CID : $CID CPIDr :: $CPIDr CPIDrel ::: $CPIDrel";

        if (($CID ==  $CPIDr) && ($CPIDrel == 4)) { $CTIDn = "CLIENT";}
        elseif (($CID ==  $CPIDr) && ($CPIDrel == 5)) { $CTIDn = "SUPPLIER";}
        elseif (($CID <>  $CPIDr) && ($CPIDrel == 4)) { $CTIDn = "Supplier";}
        elseif (($CID <>  $CPIDr) && ($CPIDrel == 5)) { $CTIDn = "Client";}

        $CPIDt = date('d-M-Y', $CPIDt)
        ?>
        <tr>
          <td style="text-align:left; text-indent:5%;"><?php echo "$pCIDn"; ?></td>
          <!-- <td style="text-align:left; text-indent:5%;"><?php echo "$CPID-$pCIDn"; ?></td> -->
          <!-- <td style="text-align:left; text-indent:5%;"><?php echo "$pCIDs"; ?></td> -->
          <td style="text-align:left; text-indent:5%;"><?php echo "$CTIDn"; ?></td>
          <td style="text-align:left; text-indent:5%;"><?php echo "$CPIDt"; ?></td>
          <td><a class="hreflink" href="partners.php?P&paa&p=<?php echo $CPID ?>&a">Acc</a></td>
          <td><a class="hreflink" href="partners.php?P&paa&p=<?php echo $CPID ?>&r">Rej</a></td>
        </tr>
      <?php
      }
    }
    ?>
    </table>

  <div style="position:absolute; bottom:5%; left:10%;">

    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a href='?H&$sp&pa=$x'>  $x  </a>";
    }
  }
  ?>
</div>
