<?php
session_start();
include 'dbconnect.inc.php';
include 'include/from_CID_count_season.inc.php';

// echo "<b>include/edit_style.inc.php</b>";
// include 'from_CID_count_styles.inc.php';
$CID = $_SESSION['CID'];

if (isset($_GET['se'])) {
  $SNID = $_GET['se'];
}

$sql = "SELECT s.ID as SNID
          , s.name as SNIDn
        FROM season s
        WHERE s.ID = ?
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rse</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $SNID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($res);
  $SNID = $row['SNID'];
  $SNIDn = $row['SNIDn'];
}
?>

<div class="cpsty" style="position:absolute; top:26.6%; width:100%; background-color:yellow">
  Edit Season
</div>

<form class="" style="position:absolute; top:50%; height:8%; left:0%; width:100%; z-index:1;" action="include/edit_season_act.inc.php" method="POST">
  <div style="position:absolute; top:-30%; height:30%; left:0%; width:100%; font-weight: bold; text-align:center;">
    Amend Season Name
  </div>
  <input type="hidden" name="SNID" value="<?php echo $SNID ?>">
  <input type="hidden" name="SNIDn" value="<?php echo $SNIDn ?>">
  <div style="position:absolute; bottom:22%; height:50%; left:0%; width:23%; text-align:right;">
    <?php echo $SNIDn ?>
  </div>
  <input autofocus class="input_data" type="text" style="position:absolute; bottom:22%; height:50%; left:25%; width:30%; text-align:left; text-indent:5%;" placeholder="<?php echo $SNIDn ?>" name="nSNIDn" value="">
  <button class="entbtn "type="submit" style="bottom:35%; height:40%; left:60%; width:10%;" name="revName">Update</button>
  <button class="entbtn "type="submit" style="bottom:35%; height:40%; left:75%; width:10%;" name="canName">Cancel</button>
</form>
<?php
