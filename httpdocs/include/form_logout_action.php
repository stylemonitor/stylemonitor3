<?php
session_start();
include 'dbconnect.inc.php';

$UID = $_SESSION['UID'];
$CID = $_SESSION['CID'];
$CIDn = $_SESSION['CIDn'];

// echo "<br>UID = $UID : CID = $CID :: CIDn = $CIDn";

$td = date("U");

if (!isset($_POST['log-out'])) {
  echo "WRONG METHOD";
}else {

  // echo "<br><b>form_logout_action.php</b>";
  // echo "<br>string $UID";

  // Change the login_registry entry
  // from 0 to 1
  $sql = "UPDATE login_registry
          SET logouttime = unix_timestamp(now())
          WHERE UID = ?
          AND logouttime = 0
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
   echo '<b>FAIL-fla</b>';
  }else{
  mysqli_stmt_bind_param($stmt, "s", $_SESSION['UID']);
  mysqli_stmt_execute($stmt);
  }

  // session_unset();
  $_SESSION = array();
  session_destroy();
  header("Location:../index.php");
  exit();
}
