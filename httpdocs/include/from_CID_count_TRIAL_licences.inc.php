<?php
include 'dbconnect.inc.php';
echo "<br><b>from_CID_count_TRIAL_licences.inc.php</b>";

$sql = "SELECT lcl.cTrial as LCLIDct
          , lcl.cDate as LCLIDcd
        FROM log_company_licence lcl
        WHERE lcl.CID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcctl</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $LCLIDct = $row['LCLIDct'];
  $LCLIDcd = $row['LCLIDcd'];
}

if (empty($LCLIDct)) {
  $LCLIDct = 0;
}else {
  $LCLIDct = $LCLIDct;
}

if (empty($LCLIDcd)) {
  $LCLIDcd = 0;
}else {
  $LCLIDcd = $LCLIDcd;
}

echo "<br>Count = $LCLIDct :: Date = $LCLIDcd";
