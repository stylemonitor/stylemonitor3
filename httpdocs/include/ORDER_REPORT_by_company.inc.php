<?php
session_start();
include 'dbconnect.inc.php';

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'Order Report by Company';
$pddhbc = '#fffddd';
include 'page_description_date_header.inc.php';

?>
<table class="trs" style="position:absolute; top:9%;">
  <tr>
    <td colspan="1" style="background-color:#f1f1f1;"></td>
    <th colspan="4" style="border-radius:10px 10px 0px 0px;">Count of</th>
  </tr>
  <tr>
    <th style="width:52%; text-align:left; text-indent:5%;" style="width:7%;">Associate / Partner</th>
    <th style="width:12%;">Orders</th>
    <th style="width:12%;">Items</th>
    <th style="width:12%;">Styles</th>
    <th style="width:12%;">Quantity</th>
  </tr>

  <?php
  $sql = "SELECT distinct ac.name AS ACIDn
  -- SQL is outstanding_order_count_per_CID.sql
         , ac.ID AS ACID
         , COUNT(DISTINCT o.ID) AS OID_COUNT
         , COUNT(DISTINCT oi.ID) AS ITEM_COUNT
         , COUNT(DISTINCT pr.prod_ref) AS STYLE_COUNT
         , SUM(oiq.order_qty) AS TOT_QTY
  FROM order_item oi
  	 INNER JOIN orders o ON oi.OID = o.ID
  	 INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
  	 INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
  	 INNER JOIN prod_ref pr ON oi.PRID = pr.ID
  	 INNER JOIN company c ON o.fCID = c.ID
     INNER JOIN associate_companies ac ON o.fACID = ac.ID
     INNER JOIN division d ON o.fDID = d.ID
  WHERE (o.fCID = ? OR o.tCID = ?)
    AND oi.itComp = ?
  GROUP BY ac.ID
  ORDER BY ACIDn
  -- LIMIT ?,?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-oQ</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $OC);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      $ACID = $row['ACID'];
      $ACIDn = $row['ACIDn'];
      $cOID = $row['OID_COUNT'];
      $cOIID = $row['ITEM_COUNT'];
      $cPRID = $row['STYLE_COUNT'];
      $cQTY = $row['TOT_QTY'];
      // $ACID = $row['ACID'];

      ?>
      <tr>
        <!-- <td><?php echo $ACID ?></td> -->
        <td style="text-align:left; text-indent:2%;"><?php echo $ACIDn ?></td>
        <td><?php echo $cOID ?></td>
        <td><?php echo $cOIID ?></td>
        <td><?php echo $cPRID ?></td>
        <td><?php echo $cQTY ?></td>
        <!-- <td><?php echo $var ?></td> -->
      </tr>
      <?php
    }
  }
  ?>
</table>
<?php
