<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_count_company_partners_confirmed.inc.php</b>";
$CID = $_SESSION['CID'];
// Check to see how many requests you are waiting a reply from
$sql = "SELECT COUNT(ID) as cCPID
        FROM company_partnerships
        WHERE active = 1
        AND insCID = ?
			  AND ((req_CID = ?) || (acc_CID = ?))
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fcccpc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $ccCPID = $row['cCPID'];
}
