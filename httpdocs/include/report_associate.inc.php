<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/order_report.inc.php";

$CID = $_SESSION['CID'];
$td = date('U');

// Check the URL to see what page we are on
if (isset($_GET['p'])) { $p = $_GET['p']; }else{ $p = 0; }

// Set the rows per page
$r = 20;
// Check which page we are on
if ($p > 1) { $start = ($p * $r) - $r; }else { $start = 0; }

if(isset($_GET['ord'])){ $ord = $_GET['ord']; }else{ $ord = 'oidd.item_del_date'; }
if(isset($_GET['sort'])){ $sort = $_GET['sort']; }else{ $sort = 'ASC'; }

// echo "<br>Company ID : $CID";
// echo "<br>Order status : $OC";
// echo "<br>Today : $td";
// echo "<br>Start day : $sd";
// echo "<br>End day : $ed";
// echo "<br>Count_orders - due date source : $dr";

include 'from_ACID_count_divisions.inc.php';

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cOID / $r);
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;
// echo '<br>Start record is : '.$start;
// echo "<br>**** count_orders count : $cOID ****";

if ($cDID == 0) {
  ?>
  <table class="trs" style="position:relative; top:0%;">
    <caption class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "There are 0 Divisions Listed"?></caption>
  </table>
      <p>NO ORDERS TO REVIEW</p>
  <?php
  header("Location:../home.php?H");
  exit();

}else {
  ?>
  <table class="trs" style="position:relative; top:0%;">
    <caption class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "There are $cDID Divisions" ?></caption>
    <!-- <tr>
      <th colspan="3"></th>
      <th colspan="5">Orders</th>
    </tr> -->
    <tr>
      <th style="width:20%; text-align:left; text-indent:1%;">Division</th>
      <th style="width:20%; text-align:left; text-indent:1%;">Product Types</th>
      <th style="width:20%; text-align:left; text-indent:5%;" style="width:7%;">Styles</th>
      <th style="width:8%; text-align:left; text-indent:5%;">O/due</th>
      <th style="width:8%; text-align:left; text-indent:5%;">T/week</th>
      <th style="width:8%; text-align:left; text-indent:5%;">N/week</th>
      <th style="width:8%; text-align:left; text-indent:5%;">Later</th>
      <th style="width:8%; text-align:left; text-indent:5%;">Finished</th>
    </tr>


<?php

  // $sql = "SELECT o.ID as OID
  //           , o.DID as OIDd
  //           , o.PDID as OIDp
  //           , oi.ID as OIID
  //           , ot.ID as OTID
  //           , o.orNos as oref
  //           , oi.ord_item_nos as oinos
  //           , ac.name as name
  //           , oi.their_item_ref as OIIDtir
  //           , pr.prod_ref as PRIDpr
  //           , oiq.order_qty as OIQIDq
  //           , o.ordate as OIDd
  //           , oidd.item_del_date as OIDDIDd
  //         FROM orders o
  //           , company c
  //           , associate_companies ac
  //           , division d
  //           , order_item oi
 	// 		      , order_item_del_date oidd
  //           , prod_ref pr
  //           , order_item_qty oiq
  //           , order_type ot
  //         WHERE c.ID = ?
  //         AND ac.CID = c.ID
  //         AND d.ACID = ac.ID
  //         AND o.DID = d.ID
  //         AND o.orComp = ?
  //         AND oi.OID = o.ID
  //         AND oidd.OIID = oi.ID
  //         AND oi.PRID = pr.ID
  //         AND oiq.ID = oi.ID
  //         AND o.OTID =  ot.ID
  //         AND $dr BETWEEN $sd AND $ed
  //         ORDER BY $ord $sort
  //         LIMIT $start, $r
  //         ;";
  // $stmt = mysqli_stmt_init($con);
  // if (!mysqli_stmt_prepare($stmt, $sql)) {
  //   echo '<b>FAIL-ra</b>';
  // }else{
  //   mysqli_stmt_bind_param($stmt, "ss", $CID, $OC);
  //   mysqli_stmt_execute($stmt);
  //   $result = mysqli_stmt_get_result($stmt);
  //   $sort == 'DESC' ? $sort == 'ASC' : $sort == 'DESC';
  //   while($row = mysqli_fetch_assoc($result)){
  //     $OTID = $row['OTID'];
  //     $OIID = $row['OIID'];
  //     $OIDn = $row['oref'];
  //     $OIIDn = $row['oinos'];
  //     $ACIDn = $row['name'];
  //     $OIIDtir = $row['OIIDtir'];
  //     $PRIDpr = $row['PRIDpr'];
  //     $OIQIDq = $row['OIQIDq'];
  //     $id = $row['OIDDIDd'];
  //
  //     $idate = date('d-M-Y', $id)
  //
  //
  //     // $od = $row['OIDd'];
  //     // $dd = $row['odel'];
  //

      ?>
      <tr>
        <td style="width:20%; text-align:left; text-indent:2%;">$DIDn</td>
        <td style="width:20%; text-align:left; text-indent:2%;"><a href="styles.php?S&spr"> $cPTID </a></td>
        <!-- <td style="width:20%; text-align:left; text-indent:2%;"><a href="styles.php?S&spr"><?php echo $cPTID ?></a></td> -->
        <td style="width:20%; text-align:left; text-indent:2%;"><a href="styles.php?S&snr"> $cPRID </a></td>
        <!-- <td style="width:20%; text-align:left; text-indent:2%;"><a href="styles.php?S&snr"><?php echo $cPRID ?></a></td> -->
        <td style="width:7%; text-align:right; padding-right:1%;">$cOIDod</td>
        <td style="width:7%; text-align:right; padding-right:1%;">$cOIDtw</td>
        <td style="width:7%; text-align:right; padding-right:1%;">$cOIDnw</td>
        <td style="width:7%; text-align:right; padding-right:1%;">$cOIDlt</td>
        <td style="width:7%; text-align:right; padding-right:1%;">$cOIDcl</td>
      </tr>
      <?php
      }
      ?>
    </table>

    <div style="position:absolute; bottom:5%; left:10%;">

    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a href='?A&$sp&p=$x'>  $x  </a>";
    }

  ?>
</div>
