<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/report_order_despatch_act.inc.php";


include 'from_OIID_get_order_details.inc.php';

if (!isset($_POST['send'])) {
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['send'])) {
  // echo "<br>WHAT THE HELL DO I DO NOW";
  $urlPage = $_POST['urlPage'];
  $OPID = $_POST['OPID'];
  $UID = $_POST['UID'];
  $OIMRID = $_POST['OIMRID'];
  $oQty = $_POST['oQty'];
  $td = $_POST['date'];
  $amt = $_POST['amt'];
  $desNote = $_POST['desNote'];

  // echo "<br>Order place  ID is : $OPID";
  // echo "<br>User ID is : $UID";
  // echo "<br>Order Item Movement ID is : $OIMRID";
  // echo "<br>Movement date is : $date";
  // echo "<br>Amount despatched is : $amt";
  // echo "<br>A note on movement : $desNote";

  if (empty($amt)) {
    // echo "<br>NO AMOUNT has been entered";
  }elseif (!is_numeric($amt)) {
    // echo "The value is NOT a number";
    // header("location: ../oders.php?O&orf&oi=$OIID&v");
    // exit();
  }elseif ($amt > $oQty) {
    // echo "More being sent than ordered!!!!";
    header("Location:../$urlPage&orb&oi=$OPID&A2");
    exit();
  }else {
    // Insert the data into the db
    // echo "Insert into db";
  $sql = "INSERT INTO order_placed_move
            (OPID, OIMRID, UID, omQty, type, inputtime)
          VALUES
            (?,?,?,?,?,?)";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-roda</b>';
  }else{
  mysqli_stmt_bind_param($stmt, "ssssss", $OPID, $OIMRID, $UID, $amt, $desNote, $td);
  mysqli_stmt_execute($stmt);
  }
  }

  // get the original order quantity (ooq)
  // get the total sent quantity (tsq)
  // IF tss == ooq mark as complete

  header("Location:../$urlPage&orb&oi=$OPID");
  exit();
}
