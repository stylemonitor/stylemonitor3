<?php
echo "include/email_suspended_user.inc.php";
// echo '<br><br><b>Prepare and send the email out as before</b>';
// come from register_interest.inc.php
//PREPARES: validation email address
//CREATE: the selector and token for the email
$selector = bin2hex(random_bytes(8));
$token = random_bytes(32);

// becarri.com
$url = "https://becarri.com/include/email/email_demo.inc.php?vkey=".$vkey;

// thesmtest.co.uk
// $url = "https://thesmtest.co.uk/include/email/email_demo.inc.php?vkey=".$vkey;

// stylemonitor.online
// $url = "https://stylemonitor.online/include/email/email_demo.inc.php?vkey=".$vkey;

//close the mysqli - because mmtuts did!
//  mysqli_stmt_close($stmt);
//  mysqli_close($con);
//Send out the email to validate email address
//take out from here to HERE for local checking
$to = $email;
$subject  = "Suspended User Activity";
$message  = "<b>Company : </b>".$CIDn;
$message .= "<br><b>SMIC : </b>".$CIDs;
$message .="<p>We have recorded an attempt by a User who has had their account suspended by you, to login to the system.</p>";
$message .="<p>User name : </p>";
$message .="<p>If they were suspended in error, you can change their status via the 'company name' User page.</p>";
$message .="<p>If they were suspended because they have left the company, please note that they have tried to access your data.</p>";
$message .="<br><br><br>Richard";
// $headers  ="FROM: (SM Sales) sales@stylemonitor.online\r\n";
//For a CC use this as well
//$headers = "CC: jrellieye@gmail.com\r\n";
//For a BCC you need to add this
$headers .="BCC: ellieyesm@gmail.com\r\n";
$headers .="Content-type:text/html\r\n";

mail($to, $subject, $message, $headers);
header ("Location:../interest.php");
// echo '123321';
header ("Location:../interest_ty.php");
exit();
