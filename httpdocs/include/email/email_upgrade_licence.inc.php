<?php
// echo "include/email_upgrade_licence.inc.php";
include 'from_CID_get_SMIC.inc.php';
include 'from_UID_get_user_details.inc.php';

$SMIC = strtoupper($SMIC);
// $pSMIC = strtoupper($pSMIC);
// $ppadUIDf = ucfirst($ppadUIDf);

$email = 'info@stylemonitor.online';

// becarri.com
$url = "https://becarri.com/include/tc_acceptance.inc.php?v=".$tcCode;

// thesmtest.co.uk
// $url = "https://thesmtest.co.uk/include/tc_acceptance.inc.php?v=".$tcCode;

// stylemonitor.online
// $url = "https://stylemonitor.online/include/tc_acceptance.inc.php?v=".$tcCode;

$to = $UIDe;

if ($licType == 12) {
  $emailTitle = "MONTHLY Licence";
  $bdType = "monthly";
  $bdText1 ="This plan gives you the flexibility to pop in and out of StyleMonitor as and when you need to.  We will retain your records for 1 year after your last monthly payment.  In that time you can rejoin with all you rprevious records intact including your Partnerships.  After that we will delete them.";
  $bdText2 ="With your MONTHLY plan, we require payments into our account on the <b>first of every month</b> in advance, missing a month will result in the account being suspended until you settle it.  Missing three payments in a rolling year will result in the account being suspended completely and requiring you to become a yearly member.  Cheaper in the long run for you!";
  $dbCost = "£10 per month";
  $emTitle = "SM-LM";
}elseif ($licType == 1) {
  $emailTitle = "YEARLY Licence";
  $bdType = "yearly";
  $bdText1 ="This plan gives you the permenancy of using StyleMonitor.";
  $bdText2 ="With your YEARLY plan, we require payment into our account on the anniversary of your joining <b>in advance</b>, missing a payment will result in the account being suspended until you settle it.";
  $dbCost = "£100 per year";
  $emTitle = "SM-LY";
}

$subject = "$CIDn - $emailTitle";

$message = "Company : <b>$CIDn</b>
            <br>SMIC : <b>$SMIC</b>";
$message .="
  <br><p>Hi <b>$ppadUIDf,</b> thank you for choosing StyleMonitor to track your orders through production and for choosing our $bdType plan.</p>
  <p>$bdText1</p>
  <p>You'll have the ability to</p>
  <b><pre>
          * track your orders</p>
          * create Associate Companies, both Clients and Suppliers</p>
          * create Partnerships</p>
          * review records by Client/Supplier</p>
          * review records by Style</p>
          * to know what is where in your factory.</p>
  </pre></b>
  <p>$bdText2</p>
  <p>Your plan allows you to have upto 10 users on your account.  They must have unique emails that are not being used in conjunction with another StyleMonitor account.  You can have more users simply by requesting them in batches of ten at a modest cost of $dbCost
  <b><p>ACCEPT TERMS AND CONDITIONS</p></b>
  <p>By clicking on the link below you are agreeing to your Terms and Conditions above</p>";
$message .='<br style="text-decoration:none;">'.$url.'';

  // <p>Whichever plan you have, when paying you need to Reference the payment with your <b>SMIC</b>, this is unique to you and tells us exactly who you are.
  // <p>BANK : HSBC</p>
  // <p>ACCOUNT NAME : Ellieye Ltd</p>
  // <p>ACCOUNT No : 12341234</p>
  // <p>SORT CODE : 00 11 22 </p>
  // <p>IBAN : IBAN00112212341234GBR</p>
  // <br>
$message .="
  <br>Regards<br>
  <br><br><br>$UIDf
  <br>On behalf of
  <br>$CIDn";

// $headers = "FROM: ($emTitle) sales@stylemonitor.online\r\n";
$headers .= "CC: $sender\r\n";
$headers .= "BCC: ellieyesm@gmail.com\r\n";
$headers .="Content-type:text/html\r\n";

mail($to, $subject, $message, $headers);
