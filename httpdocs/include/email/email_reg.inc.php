<?php
// echo "<br><b>include/email_reg.inc.php</b>";
// echo '<br><br><b>Prepare and send the email out as before</b>';
// PREPARES: validation email address
// CREATE: the selector and token for the email
$selector = bin2hex(random_bytes(8));
$token = random_bytes(32);

$ans = $_POST['check_ans'];
$ab1 = $_POST['ab1'];
$ab2 = $_POST['ab2'];

$ab3 = $ab1 + $ab2;

if ($ans <> $ab3) {
  // echo "<br>Wrong answer";
  header ("Location:../index.php?c&2&e=$sender");
  exit();
}else {
  //CREATE: the link to our http_build_url

  // becarri.com
  $url = "https://becarri.com/include/email_demo_pwd.php?v=".$vkey;

  // thesmtest.co.uk
  // $url = "https://thesmtest.co.uk/include/email_demo_pwd.php?v=".$vkey;

  // stylemonitor.online
  // $url = "https://stylemonitor.online/include/email_demo_pwd.php?v=".$vkey;

  // $email = $UIDe;

  //Send out email
  $to = $UIDe;
  // $to = $email;
  $subject  = "Interest in StyleMonitor";
  $message  = "<b>Company : </b>".$CIDn;
  $message .= "<br><b>SMIC : </b>".$SMIC;
  $message .= "<p>Thank you <b>".$UIDf.", </b>for expressing an interest in <b>S</b>tyle<b>M</b>onitor.</p>";
  $message .="<p>The email address used was <b>".$UIDe."</b></p>";
  $message .="<p>Please confirm this by following the link below.<p>";
  $message .='<br>'.$url.'';
  $message .="<p>We will then log you into <b>S</b>tyle<b>M</b>onitor for the first time and send you an email with your password for future use during your <b>FREE 2 week trial period</b>.";
  $message .="<p>The password cannot be changed during your trial but can be done when you join us.</p>";
  $message .="<p>We hope you find the system useful and look forward to your company in the future</p>";
  $message .="<p><b>Please retain the SMIC code above for future reference.</b></p>
  <p>This is unique to your company and is your way back into the system should you later decide you wish to join us after your two week trial has ended and still have any data you have entered during your trial.</p>";
  $message .="<p>Take care until then</p>";
  $message .="<br><br><br>Richard";
  // $headers  ="FROM: (SM-e1) sales@stylemonitor.online\r\n";

  //For a CC use this as well
  // $headers .= "CC: jrellieye@gmail.com\r\n";

  //For a BCC you need to add this
  $headers ="BCC: ellieyesm@gmail.com\r\n";
  $headers .="Content-type:text/html\r\n";

  mail($to, $subject, $message, $headers);
}
