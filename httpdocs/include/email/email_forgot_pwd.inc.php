<?php
include 'dbconnect.inc.php';
// echo "<br><b>email_forgot_pwd.inc.php</b>";/

// get the last digit from the date
// uses it to select which demo password the user get sent
$td = date('U');
$select = substr($td, -1);

if ($select == 0) {
  $select = '10';
}

// Start transaction
// mysqli_begin_transaction($mysqli);
// try {
  // to

  // SET the demo password
  $sql = "SELECT dpwd as dpwd
          FROM demo_pwd
          WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-efp</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $select);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $dpwd = $row['dpwd'];
  }

  // echo "<br>Demo password : $dpwd";

  $pwd =password_hash($dpwd, PASSWORD_DEFAULT);
  //cleaned up the password so no nasties can get through
  $pwd = mysqli_real_escape_string($con, $pwd);

  // // echo '<br>Hashed password is : <b>'.$pwd.'</b>';
  //put the password into the db
  $sql = "UPDATE users
          SET pwd = ?
          WHERE (email = ? OR uid = ?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-efp1</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $pwd, $uid, $uid);
    mysqli_stmt_execute($stmt);
  }

  // echo "<br>Users ID is : $UID";

  // get the company details, who, SMIC, date
  $sql = "SELECT u.firstname as UIDf
            , c.name as CIDn
            , c.SMIC as CIDs
          FROM users u
            , company c
            , company_division_user cdu
            , associate_companies ac
            , division d
          WHERE u.ID = ?
          AND cdu.UID = u.ID
          AND cdu.DID = d.ID
          AND d.ACID = ac.ID
          AND ac.CID = c.ID;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-efp2</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $UIDf = $row['UIDf'];
    $CIDn = $row['CIDn'];
    $CIDs = $row['CIDs'];

  // echo "<br>User firstname : $UIDf";
  // echo "<br>Company name : $CIDn";
  // echo "<br>Company SMIC : $CIDs";
  }

// } catch (mysqli_sql_exception $exception) {
//   mysqli_rollback($mysqli);
//
//   throw $exception;
// }

$email = $UIDe;

// becarri.com
$url = "https://becarri.com/index.php?l&u=".$UIDe;

// thesmtest.co.uk
// $url = "https://thesmtest.co.uk/index.php?l&u=".$UIDe;
//
// stylemonitor.online
// $url = "https://stylemonitor.online/index.php?l&u=".$UIDe;

//Send out email
$to = $email;
$subject = "StyleMonitor Forgotten Password";
$message  = "<b>Company : </b>".$CIDn;
$message .= "<br><b>SMIC : </b>".$CIDs;
$message .= "<br><p>Oops, ".$UIDf.", you've done what we all do at some point or other and forgotten your email.</p>";
$message .="<p>Well here is a nice shinny, new one. It can be changed, if you want, via your User page to something you will REMEMBER.</p>";
$message .="<br>Password : <b>$dpwd</b>";
$message .="<p>You can now Login with it using the email address you provided and contiue the good work for your company.</p>";
$message .="<p>Try not to forget it again, but if you do just follow the same steps and we will send out another new one!!! All at no extra cost!</p>";
$message .="<br><br>Take care,<br>";
$message .="<br><br><br>Richard";
// $headers = "FROM: (SM-e4) sales@stylemonitor.online\r\n";
//For a CC use this as well
//$headers = "CC: jrellieye@gmail.com\r\n";
//For a BCC you need to add this
$headers .= "BCC: ellieyesm@gmail.com\r\n";
// $headers .= "BCC: hlellieye@gmail.com\r\n";
$headers .="Content-type:text/html\r\n";

mail($to, $subject, $message, $headers);

// go to a thank you page
header("Location: ../index.php?l&epw");
exit();
