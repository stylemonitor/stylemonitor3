<?php
include 'dbconnect.inc.php';
// echo "<b>email_new_user_demo_pwd.php</b>";

if (isset($_GET['v'])) {
  $vkey = $_GET['v'];

  // echo "<br>vKey set as $vkey";
  // check if the vKey has been used before
  $sql = "SELECT u.ID as ID
            , u.firstname UIDf
            , u.verified as vu
            , u.email as UIDe
          FROM users u
          WHERE vKey = ?;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br><b>FAIL-edp2</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $vkey);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $UIDf = $row['UIDf']; //verified used
    $vu = $row['vu']; //verified used
    $UID = $row['ID'];
    $UIDe = $row['UIDe'];
  }
  // echo "<br>Verified marker = $vu";
  if ($vu == 1) {
    // echo "<br>The email has been used before";
    header("Location:../index.php?U&f=$UIDf");
    exit();
  }else {
    // echo "<br>Send out a demo password";

    // Start transaction
    mysqli_begin_transaction($mysqli);
    try {
      // to

      // UPDATE verifeid to being used
      $sql = "UPDATE users
              SET verified = 1
              WHERE vKey = ?";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b>FAIL-edp3</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "s", $vkey);
        mysqli_stmt_execute($stmt);
      }

      // get the last digit from the date
      // uses it to select which demo password the user get sent
      $td = date('U');
      $select = substr($td, -1);

      if ($select == 0) {
        $select = '10';
      }

      // SET the demo password
      $sql = "SELECT dpwd as dpwd
              FROM demo_pwd
              WHERE ID = ?;";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b>FAIL</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "s", $select);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        $row = mysqli_fetch_array($result);
        $dpwd = $row['dpwd'];
      }

      // echo "<br>Demo password : $dpwd";

      $pwd = password_hash($dpwd, PASSWORD_DEFAULT);
      //cleaned up the password so no nasties can get through
      $pwd = mysqli_real_escape_string($con, $pwd);

      // // echo '<br>Hashed password is : <b>'.$pwd.'</b>';
      //put the password into the db
      $sql = "UPDATE users
              SET pwd = ?
              , active = 2
              WHERE vKey = ? ;";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        // echo '<b>FAIL-edp3</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ss", $pwd, $vkey);
        mysqli_stmt_execute($stmt);
      }

      // echo "<br>Users ID is : $UID";

      // get the company details, who, SMIC, date
      $sql = "SELECT u.firstname as UIDf
                , c.name as CIDn
                , c.SMIC as CIDs
              FROM users u
                , company c
                , company_division_user cdu
                , associate_companies ac
                , division d
              WHERE u.ID = ?
              AND cdu.UID = u.ID
              AND cdu.DID = d.ID
              AND d.ACID = ac.ID
              AND ac.CID = c.ID;";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        echo '<b>FAIL-edp3</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "s", $UID);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        $row = mysqli_fetch_array($result);
        $UIDf = $row['UIDf'];
        $CIDn = $row['CIDn'];
        $CIDs = $row['CIDs'];

      // echo "<br>User firstname ssss : $UIDf";
      // echo "<br>Company name : $CIDn";
      // echo "<br>Company SMIC : $CIDs";
      }

    } catch (mysqli_sql_exception $exception) {
      mysqli_rollback($mysqli);

      throw $exception;
    }

    $email = $UIDe;

    // becarri.com
    $url = "https://becarri.com/index.php?l&u=".$UIDe;

    // thesmtest.co.uk
    // $url = "https://thesmtest.co.uk/index.php?l&u=".$UIDe;

    // LIVE db
    // $url = "https://stylemonitor.online/index.php?l&u=".$UIDe;



    //Send out email
    $to = $email;
    $subject = "StyleMonitor Demo Password";
    $message  = "<b>Company : </b>".$CIDn;
    $message .= "<br><b>SMIC : </b>".$CIDs;
    $message .= "<p>Hi ".$UIDf.", your company would like you to use <b>S</b>tyle<b>M</b>onitor on their behalf.</p>";
    $message .="<p>Here is your initial password. It cannot be changed whenever you want to a more memorable one for you.</p>";
    $message .="Password : <b>$dpwd</b>";
    $message .="<br><p>You can now Login with it using your works email address.</p>";
    $message .="<p>You can go directly to the login pages by following the link below.<p>";
    $message .="<br><p>You can then copy and paste your password to get you started.</p>";
    $message .='<br><a href="'.$url.'">'.$url.'</a>';
    $message .="<br><p>We look forward to seeing you in the near future</p>";
    $message .="<br><br>Take care until then<br>";
    $message .="<br><br><br>Richard";
    // $headers = "FROM: (SM-e5) sales@stylemonitor.online\r\n";
    //For a CC use this as well
    //$headers = "CC: jrellieye@gmail.com\r\n";
    //For a BCC you need to add this
    $headers .= "BCC: ellieyesm@gmail.com\r\n";
    // $headers .= "BCC: hlellieye@gmail.com\r\n";
    $headers .="Content-type:text/html\r\n";

    mail($to, $subject, $message, $headers);

    // go to a thank you page
    header("Location: ../interest.php?10&f=$UIDf");
    exit();
  }
}
