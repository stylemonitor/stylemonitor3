<?php
// echo "include/email_partner_supplier.inc.php";

$SMIC = strtoupper($SMIC);
$pSMIC = strtoupper($pSMIC);
$ppadUIDf = ucfirst($ppadUIDf);

$to = $ppadUIDe;

$subject = "$CIDn INITIATED SUPPLIER PARTNERSHIP REQUEST";

$message = "From : <b>".$CIDn."</b> (Supplier)
            <br>SMIC : <b>".$SMIC."</b>";
$message .="<br>To : <b>".$ppadUCIDn."</b> (Client)
            <br>SMIC : <b>".$pSMIC."</b>";
$message .="<br>REQUEST CODE : <b>".$acc_SEL."</b>";
$message .="
  <br><p>Hi <b>$ppadUIDf,</b> we would like to share with you information regarding our orders.</p>
  <p>By accepting this request, you will be allowing us to see a summary of <b>ALL</b> our orders placed with you on <b>S</b>tyle<b>M</b>onitor.</p>
  <p>We will be able to see the latest, up to date, production figures of our orders.  We will not have to request them from you.</p>
  <p>As your customer, we will not be able to see
  <br>* the detailed movements of an order
  <br>* dates of movements
  <br>* how many units an order has been placed with
  <br>
  <p>To <b>ACCEPT</b> this request, or REJECT it, you need to sign in to your <b>S</b>tyle<b>M</b>onitor account.  There will be a <b>Partner</b> index tab on your Home page, and a <b>Pending</b> index tab in your Partners page.  Either will allow you to <b>ACCEPT</b> or reject this request.</p>
  <br>
  <br><p>We look forward to working more closely with you in the near future</p>
  <br>Regards<br>
  <br><br><br>$UIDf
  <br>On behalf of
  <br>$CIDn";

$headers = "CC: $sender\r\n";
$headers .= "BCC: ellieyesm@gmail.com\r\n";
$headers .="Content-type:text/html\r\n";

mail($to, $subject, $message, $headers);

// echo "<br>email_us.php file should have sent out the email by now!!!";

header("location:../partners.php?P&pap ");
exit();
