<?php
echo "<br><b>include/email_partner_order_placed_SM_p3.inc.php</b>";
$UID  = $_SESSION['UID'];
$CIDn = $_SESSION['CIDn'];

include 'from_PRID_get_prod_details.inc.php';
include 'from_CPID_get_client_data.inc.php';
include 'from_CPID_get_supplier_data.inc.php';
include 'from_CID_get_client_company_managers.inc.php';
$cUID = $caUIDf;
$sender = $caUIDe;
include 'from_CID_get_supplier_company_managers.inc.php';
$ppadUIDf = $caUIDf;
$ppadUIDe = $caUIDe;

$orDate = date('d-M-Y', $orDate);
$delDate = date('d-M-Y', $delDate);

if ($sp == 1) {
  $sp = "Sample";
}elseif ($sp = 2) {
  $sp = "Production";
}

// echo "<br>Partnership ID(CPID) = $CPID";
// echo "<br>Client Name (cli_ACIDn) = $cli_ACIDn";
// echo "<br>Client Manager email (cUIDe) = $sender";
// echo "<br>Supplier Name (sup_ACIDn) = $ppadUIDf";
// echo "<br>Supplier Manager email (sUIDe) = $ppadUIDe";
//
// echo "<br>ORDER number (cOID) = $cOID";
// echo "<br>Order recorded date (orDate) = $orDate";
// echo "<br>Order DUE Date (delDate) = $delDate";
// echo "<br>CLIENT Product Short Code (PRIDr) = $PRIDr";
// echo "<br>CLIENT Product Reference (PRIDd) = $PRIDd";
// echo "<br>Quantity (orQTY) = $orQTY";
// echo "<br>Initial Order Item Type (sp) = $sp";

// echo "<br>url PAGE (urlPage) = $urlPage";
// echo "<br>Order ID (OID) = $OID";


// $SMIC = strtoupper($SMIC);
// $pSMIC = strtoupper($pSMIC);
// $ppadUIDf = ucfirst($ppadUIDf);

$to = $ppadUIDe;

$subject = "$CIDn PARTNERSHIP ORDER RECORDED";

$message = "From : <b>$CIDn</b> (Supplier)";
$message .= "<br>To : <b>$ppadUCIDn</b> (Client)";
$message .= "
  <br><p>Hi <b>$ppadUIDf,</b> We have recorded the following order on <b>S</b>tyle<b>M</b>onitor.</p>
  <p>Please maintain the movement records so that we can follow its progress through production without the need to contact you.
  <p>Partnership Order Number : $cOID</p>
  <p>Order Dated : $orDate</p>
  <p>Order Due : $delDate</p>
  <p>Our Order Reference : $delDate</p>
  <p>Our Item Reference : $PRIDr / $PRIDd</p>
  <p>Order Quantity : $orQTY</p>
  <p>Sample/Production : $sp</p>

  <br>Regards<br>
  <br><br><br>$UIDf
  <br>On behalf of
  <br>$CIDn";

// $headers = "FROM: (SM-p3) sales@stylemonitor.online\r\n";
$headers .= "CC: $sender\r\n";
$headers .= "BCC: ellieyesm@gmail.com\r\n";
$headers .="Content-type:text/html\r\n";

mail($to, $subject, $message, $headers);

// echo "<br>email_us.php file should have sent out the email by now!!!";

header("location:../$urlPage&orp=$OID");
exit();
