<?php
// echo "<br><b>email/email_partner_client.inc.php</b>";

$SMIC = strtoupper($SMIC);
$pSMIC = strtoupper($pSMIC);
$ppadUIDf = ucfirst($ppadUIDf);

$to = $ppadUIDe;

$subject = "$CIDn INITIATED CLIENT PARTNERSHIP REQUEST";

$message = "From : <b>$CIDn</b> (Client)
            <br>SMIC : <b>$SMIC</b>";
$message .="<br>To : <b>".$ppadUCIDn."</b> (Supplier)
            <br>SMIC : <b>".$pSMIC."</b>";
$message .="<br>REQUEST CODE : <b>".$acc_SEL."</b>";
$message .="
  <br><p>Hi <b>$ppadUIDf,</b> we would like to share with you information regarding your orders.</p>
  <p>By accepting this request, you will be able to see a summary of <b>ALL</b> your orders placed with us on <b>S</b>tyle<b>M</b>onitor.</p>
  <p>You will be able to see the latest, up to date, production figures of your orders.  You will not have to request them from us.</p>
  <br>
  <p>As our customer, you will not be able to see</p>
  <p>* the detailed movements of an order</p>
  <p>* historical information on the dates of movements</p>
  <p>* how many units an order has been placed with</p>
  <br>
  <p>To <b>ACCEPT</b> this request, or REJECT it, you need to sign in to your <b>S</b>tyle<b>M</b>onitor account.</p>
  <p>There you will find a <b>Partners</b> index tab via your Home page, and a <b>Pending</b> index tab via your Partners page.  Either will allow you to <b>ACCEPT</b> or reject this request.</p>
  <p>Once you have accepted us as a Partner within <b>S</b>tyle<b>M</b>onitor, orders can then be added by either of us via the <b>Order</b> index tab via the <b>Partners</b> page within <b>S</b>tyle<b>M</b>onitor.</P>
  <p>The order would then automatically appear in your Partners system.</p>
  <p>We look forward to working more closely with you in the near future</p>
  <br>Regards<br>
  <br><br><br>$UIDf
  <br>On behalf of
  <br>$CIDn";

$headers = "CC: $sender\r\n";
$headers .= "BCC: ellieyesm@gmail.com\r\n";
$headers .="Content-type:text/html\r\n";

mail($to, $subject, $message, $headers);

// echo "<br>email_us.php file should have sent out the email by now!!!";

header("location:../partners.php?P&pap ");
exit();
