/*
-----------------------------------------------------
| THESE ARE THE VALUES THAT NEED TO BE CHANGED:     |
|  NAME                DEFAULT VALUE         LINE   |
|---------------------------------------------------|
| o.tID                  ?                20/34/48  |
-----------------------------------------------------
*/
Select of.ID AS OID
-- SQL is email_list_for_order.sql
       , of.orNos AS orNos
       , uf.firstname AS firstname
       , uf.surname AS surname
       , cduf.position AS position
       , uf.email AS email
FROM orders of
     INNER JOIN division df ON of.fDID = df.ID
     INNER JOIN company_division_user cduf ON df.ID = cduf.DID
     INNER JOIN users uf ON cduf.UID = uf.ID
WHERE of.ID = ?
  AND of.UID = uf.ID
UNION  
Select of.ID AS OID
-- SQL is email_list_for_order.sql
       , of.orNos AS orNos
       , uf.firstname AS firstname
       , uf.surname AS surname
       , cduf.position AS position
       , uf.email AS email
FROM orders of
     INNER JOIN division df ON of.fDID = df.ID
     INNER JOIN company_division_user cduf ON df.ID = cduf.DID
     INNER JOIN users uf ON cduf.UID = uf.ID
WHERE of.ID = ?
  AND uf.privileges !=3
  AND uf.active = 2
UNION ALL
Select ot.ID AS OID
       , ot.orNos AS orNos
       , ut.firstname AS firstname
       , ut.surname AS surname
       , cdut.position AS position
       , ut.email AS email
FROM orders ot
     INNER JOIN division dt ON ot.tDID = dt.ID
     INNER JOIN company_division_user cdut ON dt.ID = cdut.DID
     INNER JOIN users ut ON cdut.UID = ut.ID
WHERE ot.ID = ?
  AND ut.privileges !=3
  AND ut.active = 2;