<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>email_demo_pwd_not_sent.inc.php</b>";

// get the form information
$UID = $_POST['UID'];

include 'from_UID_get_user_details.inc.php';
// Reset password
// echo "<br>User ID UID :: $UID";
// echo "<br>User First name UIDf :: $UIDf";
// echo "<br>User email UIDe :: $UIDe";

// Start transaction
// mysqli_begin_transaction($mysqli);
// try {
  // to

  // get a new password
  // uses it to select which demo password the user get sent
  $td = date('U');
  $select = substr($td, -1);

  if ($select == 0) {
    $select = '10';
  }
  // SET the demo password
  $sql = "SELECT dpwd as dpwd
          FROM demo_pwd
          WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-edpns</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $select);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $dpwd = $row['dpwd'];
  }

  // echo "<br>Demo password : $dpwd";

  $pwd =password_hash($dpwd, PASSWORD_DEFAULT);
  //cleaned up the password so no nasties can get through
  $pwd = mysqli_real_escape_string($con, $pwd);

  // echo '<br>Hashed password is : <b>'.$pwd.'</b>';
  //put the password into the db
  $sql = "UPDATE users
          SET DPID = ?
          , pwd = ?
          , active = 1
          , verified = 1
          WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-edpns2</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $select, $pwd, $UID);
    mysqli_stmt_execute($stmt);
  }

  // echo "<br>Password has been updated";

// } catch (mysqli_sql_exception $exception) {
//   mysqli_rollback($mysqli);
//
//   throw $exception;
// }

$email = $UIDe;

// becarri.com
$url = "https://becarri.com/index.php?l&u=".$UIDe;

// thesmtest.co.uk
// $url = "https://thesmtest.co.uk/index.php?l&u=".$UIDe;

// stylemonitor.online
// $url = "https://stylemonitor.online/index.php?l&u=".$UIDe;



//Send out email
$to = $email;
$subject = "StyleMonitor Demo Password";
$message  = "<b>Company : </b>".$CIDn;
$message .= "<br><b>SMIC : </b>".$CIDs;
$message .= "<p>Thank you ".$UIDf.", for your interest in <b>S</b>tyle<b>M</b>onitor.</p>";
$message .="<br><p>Here is your demonstration password. It cannot be changed during your trial period but should be changed when you have a licenced version.</p>";
$message .="Password : <b>$dpwd</b>";
$message .="<br><p>You can now Login with it using the email address you provided and have a look around to see if it is suitable for your company.</p>";
$message .="<p>You can go directly to the login pages by following the link below.<p>";
$message .="<br><p>You can then copy and paste your password to get you started.</p>";
$message .='<br><a href="'.$url.'">'.$url.'</a>';
$message .="<br><p>We look forward to seeing you in the near future</p>";
$message .="<br><br>Take care until then<br>";
$message .="<br><br><br>Richard";
// $headers = "FROM: (SM-e2) sales@stylemonitor.online\r\n";
//For a CC use this as well
//$headers = "CC: jrellieye@gmail.com\r\n";
//For a BCC you need to add this
$headers .= "BCC: ellieyesm@gmail.com\r\n";
// $headers .= "BCC: hlellieye@gmail.com\r\n";
$headers .="Content-type:text/html\r\n";

mail($to, $subject, $message, $headers);

// go to a thank you page
header("Location: ../interest.php?10&u=$UID");
exit();
