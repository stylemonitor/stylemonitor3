<?php
include 'dbconnect.inc.php';
// echo  "<br><b>check_company_use.inc.php</b>";
// echo  "<br>ACID : $ACID";

$sql = "SELECT COUNT(ID) as cCID
        FROM company
        WHERE name LIKE ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-ccu";
}else {
  mysqli_stmt_bind_param($stmt,"s", $CIDn);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cCID  = $row['cCID'];
}

// IF it is a new company
if ($cCID <> 0) {
    // echo "<br>The company has been used before. ERROR message company already registered";
    include 'check_company_country_use.inc.php';
}else {
  // echo "<br>it is the registration of a NEW user to an existing company";
  include 'form_reg_act_db.inc.php';
}
