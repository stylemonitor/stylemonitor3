<?php
include 'dbconnect.inc.php';
//echo '<b>include/from_CID_get_order_item_nos.inc.php</b>';
if(isset($_GET['so'])){
  $OTID = 1;
}elseif (isset($_GET['po'])) {
  $OTID = 2;
}

$CID = $_SESSION['CID'];
//Get the last order number for the associate company
$sql = "SELECT COUNT(oi.ID) as cOIID
        FROM company c
          , associate_companies ac
          , division d
          , orders o
          , order_item oi
          , order_type ot
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND o.DID = d.ID
        AND oi.OID = o.ID
        AND oi.OTID = ot.ID
        AND ot.ID = ?
        AND o.orNos = (SELECT max(o.orNos)+1 as MorNos
                         FROM company c
                          , associate_companies ac
                          , division d
                       	  , orders o
                          , order_type ot
                         WHERE c.ID = ?
                         AND ac.CID = c.ID
                         AND d.ACID = ac.ID
                         AND o.DID = d.ID
                         AND ot.ID = ?)
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fcgoin</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $CID, $OTID, $OTID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOIID  = $row['cOIID']+1;
}
