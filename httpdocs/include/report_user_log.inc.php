<?php
session_start();
include 'dbconnect.inc.php';

include 'set_urlPage.inc.php';

// echo "<br>include/report_movement_log.inc.php";

  // Set the rows per page
  $rpp = 20;

  // Check the URL to see what page we are on
  if (isset($_GET['pa'])) {
    $pa = $_GET['pa'];
  }else{
    $pa = 0;
  }

  // Check which page we are on
  if ($pa > 1) {
    $start = ($pa * $rpp) - $rpp;
  }else {
    $start = 0;
  }

  // count the number of rows that will be in the table
  // REMEBER to count them for teh requirement of the table ie, this week, next week and Later
  $sql = "SELECT COUNT(ID) as UID
          FROM login_registry
          WHERE UID = 1
          ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-rul</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $UID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
  }
    $cUID = $row['ID'];

    // turn the order date into an easy to read format
    // $OIDs = strtotime($OIDs);
    $OIDs = date('D-d-M-Y', $OIDs);

    // turn the order delivery date into an easy to read format
    $odDIDd = strtotime($odDIDd);
    $odDIDd = date('D-d-M-Y', $odDIDd);

    // echo '<br><br><br>Count of orders for this company is : '.$cOIID;

    // How many pages will this give us Pages To View $PTV
    // ceil rounds up a number ie 1.1 becomes 2 1.9 is also 2
    $PTV = ($cUID / $rpp);

    // echo '<br>The number of pages will be : '.$PTV;
    $PTV = ceil($PTV);
    // echo '<br>The number of pages will be : '.$PTV;

    // echo '<br>Start record is : '.$start;
  }
  ?>

  <!-- Get a table for the manufacturing unit details -->

  <table class="trs" style="position:absolute; top:46%; left:0%; width:100%;">
    <!-- <caption class="cpsty">Movement Record</caption> -->
    <tr>
      <th style="width:20%; background-color:#b5fcdb; border-right:thin solid grey;">Movement Date</th>
      <th style="width:10%; background-color:#b5fcdb; padding-right:1%; border-right:thin solid grey;">User</th>
      <th style="width:12%; background-color:#f1f1f1; padding-right:1%; border-right:thin solid grey;"></th>
      <th style="width:12%; background-color:#f1f1f1; padding-right:1%; border-right:thin solid grey;"></th>
      <th style="width:12%; background-color:#f1f1f1; padding-right:1%; border-right:thin solid grey;"></th>
      <th style="width:12%; background-color:#f1f1f1; padding-right:1%; border-right:thin solid grey;"></th>
      <th style="width:12%; background-color:#f1f1f1; padding-right:1%; border-right:thin solid grey;"></th>
      <th style="width:12%; background-color:#f1f1f1; padding-right:1%;"></th>
    </tr>

  <?php

  // Get those records
  $sql = "SELECT oi.ID as OIID
            , u.userInitial as UIDi
            , o.orNos as OIDnos
            , oi.ord_item_nos as OIIDnos
            , opm.omQty as opmQTY
            , oimr.ID as oimrREAS
            , opm.inputtime as opmDATE
          FROM orders o
            , order_item oi
            , order_placed op
            , order_placed_move opm
            , order_item_movement_reason oimr
            , users u
          WHERE op.ID = ?
          AND oi.OID = o.ID
          AND op.OIID = oi.ID
          AND opm.OPID = op.ID
          AND opm.OIMRID = oimr.ID
          AND oimr.ID NOT IN (1,2,11)
          AND opm.UID = u.ID
          -- AND o.DID = d.ID
          -- AND d.ACID = ac.ID
          ORDER BY opm.inputtime
          LIMIT $start, $rpp;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    // echo '<b>FAIL</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $OPID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_array($result)){
      $cOIID = $row['OIID'];
      $UIDi = $row['UIDi'];
      $OIDnos = $row['OIDnos'];
      $OIIDnos = $row['OIIDnos'];
      $opmQTY = $row['opmQTY'];
      $oimrREAS = $row['oimrREAS'];
      $opmDATE = $row['opmDATE'];

      // echo '<br> Order Item ID is $cOIID : '.$cOIID;
      // echo '<br> User ID is $UID : '.$UID;
      // echo '<br> Order Number is $OIDnos : '.$OIDnos;
      // echo '<br> Order Item Number is $OIIDnos : '.$OIIDnos;
      // echo '<br> Movement Quantity is $opmQTY : '.$opmQTY;
      // echo '<br> Movement is $oimrREAS : '.$oimrREAS;
      // echo '<br> Movement date is $opmDATE : '.$opmDATE;

      // $opmDATE = strtotime($opmDATE);
      $opmDATE = date('D-d-M-Y / H.i', $opmDATE);
      $opmTime = date('H:i', $opmDATE);


      ?>
      <tr>
        <td style=" border-right:thin solid grey;"><?php echo "$opmDATE"?> </td>
        <td style=" padding-right:1%; border-right:thin solid grey;"><?php echo $UIDi ?></td>

        <td style="text-align:right; padding-right:1%; border-right:thin solid grey;"><?php
          if ($oimrREAS == 3) {
            echo $opmQTY;
          }elseif ($oimrREAS == 13){
            echo '-'.$opmQTY;
          }elseif ($oimrREAS == 23 || $oimrREAS == 29) {
            echo '('.$opmQTY.')';
          } ?>
        </td>
        <td style="text-align:right; padding-right:1%; border-right:thin solid grey;"><?php
          if ($oimrREAS == 4) {
            echo $opmQTY;
          }elseif ($oimrREAS == 14){
            echo '-'.$opmQTY;
          }elseif ($oimrREAS == 24 || $oimrREAS == 30) {
            echo '('.$opmQTY.')';
          } ?>
        </td>
        <td style="text-align:right; padding-right:1%; border-right:thin solid grey;"><?php
          if ($oimrREAS == 5) {
            echo $opmQTY;
          }elseif ($oimrREAS == 15){
            echo '-'.$opmQTY;
          }elseif ($oimrREAS == 15 || $oimrREAS == 31) {
            echo '('.$opmQTY.')';
          } ?>
        </td>
        <td style="text-align:right; padding-right:1%; border-right:thin solid grey;"><?php
          if ($oimrREAS == 6) {
            echo $opmQTY;
          }elseif ($oimrREAS == 16){
            echo '-'.$opmQTY;
          }elseif ($oimrREAS == 26 || $oimrREAS == 32) {
            echo '('.$opmQTY.')';
          } ?>
        </td>
        <td style="text-align:right; padding-right:1%; border-right:thin solid grey;"><?php
          if ($oimrREAS == 7) {
            echo $opmQTY;
          }elseif ($oimrREAS == 17){
            echo '-'.$opmQTY;
          }elseif ($oimrREAS == 27 || $oimrREAS == 33) {
            echo '('.$opmQTY.')';
          } ?>
        </td>
        <td style="text-align:right; padding-right:1%;"><?php
          if ($oimrREAS == 8) {
            echo $opmQTY;
          }elseif ($oimrREAS == 18){
            echo '-'.$opmQTY;
          }elseif ($oimrREAS == 28 || $oimrREAS == 34) {
            echo '('.$opmQTY.')';
          }
       ?>
        </td>
      </tr>

      <?php
    }
  }
   ?>
  </table>

  <div style="position:absolute; bottom:10%; left:20%;">
    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a href=''.$urlPage.'&mlg&oi=$OIID&pa=$x'>  $x  </a>";
    } ?>
  </div>
