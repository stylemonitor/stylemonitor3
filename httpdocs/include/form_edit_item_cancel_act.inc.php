<?php
session_start();
include 'dbconnect.inc.php';
$UID = $_SESSION['UID'];
$CID = $_SESSION['CID'];
$td = date('U');
echo "<br><b>include/form_edit_item_cancel_act.inc.php</b>";

if (!isset($_POST['YesBtn']) && !isset($_POST['NoBtn'])) {
  // echo '<br>Wrong access method';
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['NoBtn'])) {
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  header("Location:../home.php?H&rt3&o=$OID&OI=$OIID");
  exit();
}elseif (isset($_POST['YesBtn'])) {
  // echo "CANCEL ITEM FROM ORDER";
  $OID = $_POST['OID'];
  $OIID = $_POST['OIID'];
  $OPID = $_POST['OPID'];
  $OTID = $_POST['OTID'];
  $canReas = $_POST['canReas'];
  $OIMRID = 45;
  $qty = 0;
  $td = date('U');

  echo "<br>OID=$OID :: OIID=$OIID :: OPID=$OPID :: OTID=$OTID";

  // check if there is already a cancellation in progress
  include 'action/Count_OIC_CANCEL_from_OIID.act.php';

  if ($cOICID <> 0) {
    // echo "<br>a request is already in place";
    header("Location:../home.php?H&o=$OID&OI=$OIID");
    exit();
  }else {
    include 'action/insert_into_OPM.act.php';
    include 'action/select_OPMID.act.php';
    include 'action/insert_into_order_item_change_table.act.php';

    if (($OTID == 3) || ($OTID == 4)) {
      // echo "<br>THIS IS A PARTNERSHIP ORDER AND NEEDS PARTNER APPROVAL";

      // how to REJECT the request!!!

      // include 'action/update_order_item_table.act.php';
      include 'action/select_OICID_from_OPMID_CANCEL.act.php';
      include 'action/update_order_item_change_table.act.php';
      // include 'action/insert_into_OPM.act.php';
      // cancel the requset!!!


      header("Location:../home.php?H&o=$OID&eit=$OIID");
      exit();

    }elseif (($OTID == 1) || ($OTID == 2)) {
      $status = 1;
      $OIMRID = 46;
      // remove item from order
      include 'action/update_order_item_table.act.php';
      include 'action/select_OICID_from_OPMID_CANCEL.act.php';
      include 'action/update_order_item_change_table.act.php';
      include 'action/insert_into_OPM.act.php';

      header("Location:../home.php?H&o=$OID&eit=$OIID");
      exit();
    }
  }
}
