<?php
include 'dbconnect.inc.php';
// echo "<b>include/upgrade_licence.inc.php</b>";
$UID = $_SESSION['UID'];
$CID = $_SESSION['CID'];

include 'from_UID_get_user_details.inc.php';
include 'from_CID_get_SMIC.inc.php';
// echo "CIDn = $CIDn: SMIC = $SMIC";

$sm = '<b>S</b>tyle<b>M</b>onitor';

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'JOINING StyleMonitor';
$pddhbc = '#fffddd';
include 'page_description_date_header.inc.php';

$tcCode = password_hash($SMIC, PASSWORD_DEFAULT);

?>

<div style="position:absolute; top:9%; height:90%; left:0%; width:100%; background-blend-mode: overlay;"></div>

<div style="position:absolute; top:15%; height:81%; left:10%; width:80%; background-color:<?php echo $addRecCol1 ?>; border: 2px solid grey; border-radius:8px;">
  <br>
  <!-- <p style="font-size:400%;">Thank you for viewing and using</p> -->
  <p style="font-size:250%;">Thank you for choosing</p>
  <p style="font-size:600%; color:#840c0c;"><?php echo $sm ?></p>
  <br>
  <p style="font-size:150%;">to track your orders through production</p>
  <!-- <p>and now for looking at upgrading your licence.</p> -->
  <br>
  <p style="font-size:150%;">We offer 2 plans</p>
  <form class="" action="include/upgrade_licence_act.inc.php" method="post">
    <input type="hidden" name="tcCode" value="<?php echo $tcCode ?>">
    <input type="hidden" name="CID" value="<?php echo $CID ?>">
    <input type="hidden" name="CIDn" value="<?php echo $CIDn ?>">
    <input type="hidden" name="SMIC" value="<?php echo $SMIC ?>">
    <input type="hidden" name="UID" value="<?php echo $UID ?>">
    <div style="position:absolute; top:50%; height:5%; left:10%; width:35%; font-size:175%; border:thin solid black; border-radius:10px 10px 0px 0px; background-color:#f8d185;">
      <p>MONTHLY</p>
    </div>
    <button type="submit" name="month" style="position:absolute; top:55%; height:15%; left:10%; width:35.2%; border:thin solid black; border-radius:0px 0px 0px 0px;">
      <p style="font-size:150%;">£50 per month*</p>
      <br>
      <p style="font-size:150%;">Due every 15th monthly</p>
    </button>
    <div style="position:absolute; top:70%; height:5%; left:10%; width:35%; font-size:175%; border:thin solid black; border-radius:0px 0px 10px 10px; background-color:#f8d185;">
      <p>No Long Term Contract</p>
    </div>

    <div style="position:absolute; top:50%; height:5%; left:55%; width:35%; font-size:175%; border:thin solid black; border-radius:10px 10px 0px 0px; background-color:#c05cff;">
      <p>YEARLY</p>
    </div>
    <button type="submit" name="year" style="position:absolute; top:55%; height:15%; left:55%; width:35.2%; border:thin solid black; border-radius:0px 0px 0px 0px;">
      <p style="font-size:150%;">£500 per year*</p>
      <br>
      <p style="font-size:150%;">Due every anniversary yearly</p>
    </button>
    <div style="position:absolute; top:70%; height:5%; left:55%; width:35%; font-size:175%; border:thin solid black; border-radius:0px 0px 10px 10px; background-color:#c05cff;">
      <p>Cheaper</p>
    </div>
  </form>
  <div style="position:absolute; bottom:1%; left:0%; width:100%;">
    <p>Both have the same method of payment.</p>
    <p>An email will be sent to you giving details about the plan you have selected</p>
    <p>* Both prices quoted are exclusive of UK VAT taxes</p>
  </div>
  <!-- <p>Bank : HSBC</p>
  <p>IBAN : IBAN00000012341234GBR</p>
  <p>Sort Code : 00 00 00</p>
  <p>Account Name : Ellieye Ltd</p>
  <p>Account Number : 12341234</p> -->

  <!-- <div class="pp" style="width:100%;">
  <form  action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
  <input type="hidden" name="cmd" value="_s-xclick">
  <input type="hidden" name="hosted_button_id" value="7VWSSVHV8KP6C">
  <input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online!">
  <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>
</div> -->
</div>
