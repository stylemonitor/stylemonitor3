<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_get_divisions.inc.php</b>";

$sql = "SELECT cp.reqCID as reqCID
        FROM company_partnerships cp
          , company c
        WHERE cp.ID = ?
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  // echo '<b>FAIL-fcgd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CPID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $reqCID = $row['DID'];
}
