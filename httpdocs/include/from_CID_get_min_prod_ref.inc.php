<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_get_min_prod_ref.inc.php</b>";

// from the associate companies ID
// get the minimum section ID
// when the company has NO NAMED sections
$sql = "SELECT MIN(pr.ID) as mPRID
        FROM prod_ref pr
          , company c
          , associate_companies ac
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND pr.ACID = ac.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmpr</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $mPRID = $row['mPRID'];
}
