<?php
// echo "<b>include/associates.inc.php</b>";
include 'dbconnect.inc.php';

// echo "Order items in the warehouse to appear here";

if (isset($_GET['tab'])) {
  if ($tab == 0) {
    // echo "PART orders in warehouse";
    include 'include/order_status_summary_warehouse.inc.php';
    ?>
    <!-- <div style="position:absolute; top:10%; left:10%; width:80%;">
      Goods Warehouse SUMMARY
      <br>To show the total goods in the warehouse
    </div> -->
    <?php
  }elseif ($tab == 1) {
    ?>
    <div style="position:absolute; top:10%; left:10%; width:80%;">
      Goods Warehouse OUTWARD
      <br>To show the SALES orders in the warehouse
      <br>tab = OTID = 1 Associate
      <br>tab = OTID = 3 Partner
      <br>Associate name
      <br>Short code of item
      <br>Quanitity in warehouse
      <br>Quanitity on order
      <br>Quanitity sent
    </div>
    <?php
  }elseif ($tab == 2) {
    ?>
    <div style="position:absolute; top:10%; left:10%; width:80%;">
      Goods Warehouse INWARD
      <br>To show the PURCHASE orders in the warehouse
      <br>tab = OTID = 2 Associate
      <br>Associate name
      <br>Short code of item
      <br>Quanitity in warehouse
      <br>Quanitity on order
      <br>Quanitity sent
    </div>
    <?php
  }elseif ($tab == 3) {
    ?>
    <div style="position:absolute; top:10%; left:10%; width:80%;">
      Goods Warehouse PARTNER PURCHASES
      <br>To show the PARTNER PURCHASES orders in THEIR warehouse
      <br>tab = OTID = 4 Partner
      <br>Associate name
      <br>Short code of item
      <br>Quanitity in warehouse
      <br>Quanitity on order
      <br>Quanitity sent
    </div>
    <?php
  }elseif ($tab == 4) {
    ?>
    <div style="position:absolute; top:10%; left:10%; width:80%;">
      PARTNER SALES TRANSIT
      <br>To show the PARTNER sales orders that have been sent but not yet received
      <br>tab = OTID = 3 Partner
      <br>Associate name
      <br>Short code of item
      <br>Quanitity in warehouse
      <br>Quanitity on order
      <br>Quanitity sent
    </div>
    <?php
  }elseif ($tab == 5) {
    ?>
    <div style="position:absolute; top:10%; left:10%; width:80%;">
      PARTNER PURCHASES TRANSIT
      <br>To show the PARTNER PURCHASES that have been sent but not yet received
      <br>tab = OTID = 4 Partner
      <br>Associate name
      <br>Short code of item
      <br>Quanitity in warehouse
      <br>Quanitity on order
      <br>Quanitity sent
    </div>
    <?php
  }
  // echo "whs is set";
  if (isset($_GET['oi'])) {
    // echo "<br>oi is set";
    include 'include/form_item_movement_update.inc.php';
  }
}
