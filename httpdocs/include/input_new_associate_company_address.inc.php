<?php
echo "<br><b>input_new_associate_company_address.inc.php</b>";
echo "<br>Associate company ID is : $NEACID";
echo "<br>The users country ID is : $CYID";
echo "<br>Timezone ID is : $TID";
echo "<br>The site name is : $addn";
echo "<br>The users address 1 is : $add1";
echo "<br>The users address 2 is : $add2";
echo "<br>The users city is : $city";
echo "<br>The users county is : $county";
echo "<br>The users postcode is : $pcode";
echo "<br>User input ID : $UID";
echo "<br>Time input is : $td";

// ADD THE ASSOCIATE COMPANY ADDRESS TO THE DB
$sql = "INSERT INTO addresses
          (CYID, TID, UID, addn, add1, add2, city, pcode, county, tel, inputtime)
        VALUES
          (?,?,?,?,?,?,?,?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo "<br><b>FAIL-inaca</b>";
}else{
  mysqli_stmt_bind_param($stmt, "sssssssssss", $CYID, $TID, $UID, $addn, $add1, $add2, $city, $pcode, $county, $tel, $td);
  mysqli_stmt_execute($stmt);
}

// get the new address ID
$sql = "SELECT ID as AID
        FROM addresses
        WHERE CYID = ?
        AND UID = ?
        AND inputtime = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-frad7</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $CYID, $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $AID = $row['AID'];
}
echo "<br>Address ID is $AID";

// Join the address to the associate company
$sql = "INSERT INTO company_associates_address
          (ACID, AID, UID, inputtime)
        VALUES
          (?,?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo "<br><b>FAIL-inaca</b>";
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $NEACID, $AID, $UID, $td);
  mysqli_stmt_execute($stmt);
}

$sql = "SELECT ID as CAAID
        FROM company_associates_address
        WHERE ACID = ?
        AND AID = ?
        AND UID = ?
        AND inputtime = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-frad7</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $NEACID, $AID, $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $CAAID = $row['CAAID'];
}
echo "<br> Associate company address ID is $CAAID";
