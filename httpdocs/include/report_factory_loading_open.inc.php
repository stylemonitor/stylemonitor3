<?php
session_start();
include 'dbconnect.inc.php';
echo "include/report_factory_loading_open.inc.php";

$CID = $_SESSION['CID'];
$td = date('U');

// include 'from_CID_count_order_overdue.inc.php';
// include 'from_CID_count_order_thisweek.inc.php';
// include 'from_CID_count_order_nextweek.inc.php';
// include 'from_CID_count_order_later.inc.php';
// include 'from_CID_count_order_new.inc.php'; // $cOIDnew
// include 'from_CID_count_orders_in_work.inc.php'; // $cOIDiw
// include 'from_UID_get_last_logout.inc.php'; // $LRIDout

if (isset($_GET['sum'])) {
  $sp = 'aoo';
  $OC = 0;
  $sd = $td - 31536000;
  $ed = $td + 31536000;
  $dr = 'oidd.item_del_date';
  $cp = "open orders";
  $hc = 'black';
  $hbc = '#b2b988';
}

// echo "<br>Company ID : $CID";
// echo "<br>Order status : $OC";
// echo "<br>Today : $td";
// echo "<br>Start day : $sd";
// echo "<br>End day : $ed";
// echo "<br>Count_orders - due date source : $dr";

$sql = "SELECT COUNT(o.ID) as OID
        FROM orders o
          , company c
          , associate_companies ac
          , division d
          , order_item oi
          , order_item_del_date oidd
        WHERE o.CPID IN(SELECT cp.ID
            						FROM company_partnerships cp
            						WHERE cp.req_CID = ? OR cp.acc_CID = ?)
        AND o.orComp = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND o.tDID = d.ID
        AND oi.OID = o.ID
        AND oidd.OIID = oi.ID
        AND $dr BETWEEN $sd AND $ed
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-orlo1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $CID, $CID, $OC);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOID = $row['OID'];
}

if ($cOID < 1) {
  // echo "<br>NO orders placed";
  ?>
  <div class="trs" style="position:absolute; top:0.25%;">
    <div class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "There are 0 Orders"?></div>
  </div>
      <p>NO ORDERS TO REVIEW</p>
  <?php
  header("Location:../home.php?H");
  exit();

}else {
  ?>
  <!-- <div class="cpsty" style="position:absolute; top:0.25%; width:100%; color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><a style="text-decoration:none;" href="loading.php?L&A&aoo"><?php echo "Orders with items still to start" ?></a></div> -->
  <table class="trs" style="position:absolute; top:3.5%;">
  <tr>
    <th style="width:2%;">O/T</th>
    <th style="width:6%;">O/r</th>
    <th style="width:1%;"></th>
    <th style="width:25%; text-align:left; text-indent:5%;" style="width:7%;">Associate / Partner</th>
    <th style="width:35%; text-align:left; text-indent:5%;">OUR Order Ref</th>
    <th style="width:9%;">Section Qty</th>
    <th style="width:8%; padding-right:1%;">Qty</th>
    <th style="width:13%;">Due date</th>
    <th style="width:1%;"></th>
  </tr>

  <?php
  // echo "Add the right bottom half table here";
  include 'loading_QUERY.inc.php';
  ?>
  <div style="position:absolute; top:18%; right:5%; font-size:200%;">

    <?php
    if (isset($_GET['H'])) {
      $page = 'H';
    }elseif (isset($_GET['L'])) {
      $page = 'L';
    }
      for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a style='text-decoration:none;' href='?$page&$sp&od=$x'>  $x  </a>";
    }
    ?>
  </div>
  <?php
} ?>
