<?php
include 'dbconnect.inc.php';
// echo '<br><b>include/from_CID_select_product_ref.inc.php</b>';

$sql = "SELECT pr.ID as PID
          , pr.prod_ref as PRIDn
          , pt.name as PTIDn
        FROM company c
          , associate_companies ac
          , prod_ref pr
          , product_type pt
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND pr.ACID = ac.ID
        AND pr.PTID = pt.ID
        AND pt.name NOT IN ('Select Product Type')
        ORDER BY 2
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br><b>FAIL-fcspr</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $PRID  = $row['PID'];
    $PRIDn = $row['PRIDn'];
    $PTIDn = $row['PTIDn'];

    // echo '<option value="'.$PRID.'">"'.$PRIDn : $PTIDn.'"</option>';


    ?>
    <option value="<?php echo $PRID ?>"><?php echo "$PRID/$PRIDn : $PTIDn" ?></option>
    <?php
  }
}
?>
