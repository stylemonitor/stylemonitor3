<?php

$COLOURS	#
Overdue             = #d8081e
This week	        = #f7a0a0
Next week	        = #ecf5a9
Later	            = #92ef9d
New	                = #a5b5c1
W/house	            = #baac7b
Completed	        = #eb839f
Despatched	        =

Product	            = #bacba6
Help	            = #d4c50d

New	                = #a8c1a5
Review	            = #f1f1f1
Edit	            = #f1f1f1

Sales               = #f7cc6c
Purchases           = #bbbbff
Partner Sales       = #d68e79
Partner Purchases   = #c9d1ce

$Home       = #fa3d3d
$Styles     = #ffb974
$Orders     = #effd7f
$Associates = #5dffa2
$Partners   = #5199fc
$THEM       = #525fff
$How        = #e08df4
$Contact    = #676767

BUTTONS
New           = #80b988
Review        = #b2b988
Edit          = #dd5858
Cancel        = #b58989

Summary       = #a7e0f5
Section 1     = #63a363
Section 2     = #63a3a3
Section 2     = #6377a3
Section 2     = #8f63a3
Section 2     = #a3637c
