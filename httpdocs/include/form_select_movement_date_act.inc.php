<?php
include 'dbconnect.inc.php';
// echo "include/form_select_movement_date_act.inc.php";

if (!isset($_POST['selDate']) && !isset($_POST['tdBtn']) && !isset($_POST['pdBtn']) && !isset($_POST['ndBtn'])) {
  // echo "HOME PAGE RE-DIRECT";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['selDate'])){
  // echo "Get the date and show the information";
  $tab =$_POST['tab'];
  $OTID = $_POST['OTID'];
  $sp = $_POST['sp'];
  $movDate = $_POST['movDate'];
  if (empty($movDate)) {
    $td = date('U');
    // echo "todays date $td";
    $movDate = date('Y-m-d', $td);
    // echo "Revised date $td";
  }
  // echo "<br>Selected date is $movDate";
  header("Location:../loading.php?M&tab=$tab&ot=$OTID&sp=$sp&rep=$movDate");
  exit();
}elseif (isset($_POST['tdBtn'])) {
  $tab =$_POST['tab'];
  $OTID = $_POST['OTID'];
  $sp = $_POST['sp'];
  $td = date('U');
  $td = $td - 86400;
  $movDate = date('Y-m-d',$td);
  // include 'report_movement_date_awt.inc.php';
  header("Location:../loading.php?M&tab=$tab&ot=$OTID&sp=$sp&rep=$movDate");
  exit();
}elseif (isset($_POST['pdBtn'])) {
  $tab =$_POST['tab'];
  $OTID = $_POST['OTID'];
  $sp = $_POST['sp'];
  $rep = $_POST['rep'];
  $rep = strtotime($rep,0);
  $movDate = $rep - 86400;
  $movDate = date('Y-m-d',$movDate);
  header("Location:../loading.php?M&tab=$tab&ot=$OTID&sp=$sp&rep=$movDate");
  exit();
}elseif (isset($_POST['ndBtn'])) {
  $tab =$_POST['tab'];
  $OTID = $_POST['OTID'];
  $sp = $_POST['sp'];
  $rep = $_POST['rep'];
  $rep = strtotime($rep,0);
  $movDate = $rep + 86400;
  $movDate = date('Y-m-d',$movDate);
  header("Location:../loading.php?M&tab=$tab&ot=$OTID&sp=$sp&rep=$movDate");
  exit();
}
