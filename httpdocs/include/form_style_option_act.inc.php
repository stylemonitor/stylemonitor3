<?php
include 'dbconnect.inc.php';
// echo "include/form_item_style_option_act.inc.php";

if (!isset($_POST['ent']) && !isset($_POST['canent'])) {
  // echo "WRONG METHOD";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['canent'])) {
  // echo "CANCEL";
  $rtnPage = $_POST['rtnPage'];
  header("Location:../styles.php?S&$rtnPage");
  exit();
}elseif (isset($_POST['ent'])) {
  // echo "Go to wherever!!!!";
  $PRID = $_POST['PRID'];
  $styopt = $_POST['styopt'];
  if (empty($styopt)) {
    header("Location:../styles.php?S&snn");
    exit();
  }elseif ($styopt == 1) {
    header("Location:../styles.php?S&orn&sal=1&prd=$PRID");
    exit();
  }elseif ($styopt == 2) {
    header("Location:../styles.php?S&orn&pur=1&prd=$PRID");
    exit();
  }elseif ($styopt == 3) {
    header("Location:../styles.php?S&sis&s=$PRID");
    exit();
  }elseif ($styopt == 4) {
    header("Location:../styles.php?S&sne&s=$PRID");
    exit();
  }elseif ($styopt == 5) {
    header("Location:../styles.php?S&test&s=$PRID");
    exit();
  }
}
