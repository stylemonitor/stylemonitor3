<?php
include 'dbconnect.inc.php';
include 'from_CID_count_collection.inc.php';
// echo "<b>include/form_select_movement_date.inc.php</b>";

$td = date('U');
$td = date('Y-m-d',$td);

if (isset($_GET['ot'])) {
  $OTID = $_GET['ot'];
}
if (isset($_GET['sp'])) {
  $sp = $_GET['sp'];
}

if (isset($_GET['rep'])) {
  $rep = $_GET['rep'];
  $rep = strtotime($rep, 0);
  $rep = date('d M Y', $rep);
}

if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  if ($tab == 9) {
    $secName = 'ALL Sections';
  }elseif ($tab == 0) {
    $secName = 'Awaiting';
  }elseif ($tab == 1) {
    $secName = '1';
  }elseif ($tab == 2) {
    $secName = '2';
  }elseif ($tab == 3) {
    $secName = '3';
  }elseif ($tab == 4) {
    $secName = '4';
  }elseif ($tab == 5) {
    $secName = '5';
  }
}

?>
<div style="position:absolute; top:90%; height:7%; left:0%; width:100%; background-color:#83ce9d; z-index:1;"></div>
<div style="position:absolute; top:90%; height:5.1%; left:0%; width:15%; padding-top: 1%; font-size: 150%; font-weight:bold; text-align: left; text-indent: 2%; background-color:#36c969; border-radius: 0px 10px 10px 0px; z-index:1;">
  Select Date
</div>
<div style="position:absolute; top:90%; height:5.1%; left:33%; width:15%; padding-top: 1%; font-size: 150%; font-weight:bold; text-align: left; text-indent: 2%; z-index:1;">
  <?php echo $rep ?>
</div>
<?php
if (isset($_GET['rep'])) {
  $repDate = $_GET['rep'];
  if ($td == $repDate) {
    ?>
    <form class="" action="include/form_select_movement_date_act.inc.php" method="POST">
      <input type="hidden" name="tab" value="<?php echo $tab ?>">
      <input type="hidden" name="OTID" value="<?php echo $OTID ?>">
      <input type="hidden" name="sp" value="<?php echo $sp ?>">
      <button class="entbtn" type="submit" style="position:absolute; top:91.6%; height:3.8%; left:20%; width:10%; text-align:center; font-weight:bold; z-index:1;" name="tdBtn">Yesterday</button>
      <input class ="sel_dropdown" style="position:absolute; top:91%; height:3.8%; left:58.5%; width:20%; text-align:center; font-weight:bold; z-index:1;" type="date" name="movDate" placeholder="Order Date" value="date()" >
      <button class="entbtn" type="submit" style="position:absolute; top:91.6%; height:3.8%; left:85%; width:10%; text-align:center; font-weight:bold; z-index:1;" name="selDate">Enter</button>
    </form>
    <?php
  }else {
    ?>
    <form class="" action="include/form_select_movement_date_act.inc.php" method="POST">
      <input type="hidden" name="tab" value="<?php echo $tab ?>">
      <input type="hidden" name="OTID" value="<?php echo $OTID ?>">
      <input type="hidden" name="sp" value="<?php echo $sp ?>">
      <input type="hidden" name="rep" value="<?php echo $rep ?>">
      <button class="entbtn" type="submit" style="position:absolute; top:91.6%; height:3.8%; left:20%; width:10%; text-align:center; font-weight:bold; z-index:1;" name="pdBtn">Previous Day</button>
      <button class="entbtn" type="submit" style="position:absolute; top:91.6%; height:3.8%; left:47.5%; width:10%; text-align:center; font-weight:bold; z-index:1;" name="ndBtn">Next Day</button>
      <input class ="sel_dropdown" style="position:absolute; top:91%; height:3.8%; left:58.5%; width:20%; text-align:center; font-weight:bold; z-index:1;" type="date" name="movDate" placeholder="Order Date" value="date()" >
      <button class="entbtn" type="submit" style="position:absolute; top:91.6%; height:3.8%; left:85%; width:10%; text-align:center; font-weight:bold; z-index:1;" name="selDate">Enter</button>
    </form>
    <?php
  }
}
?>
