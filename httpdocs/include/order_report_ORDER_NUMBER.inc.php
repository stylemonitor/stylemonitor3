<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/order_report_ORDER_NUMBER.inc.php";

$CID = $_SESSION['CID'];
$td = date('U');

if (isset($_GET['o'])) {
  $OID = $_GET['o'];
}

include 'from_OID_get_order_details.inc.php';
include 'from_OID_count_order_item_open.inc.php';
include 'set_urlPage.inc.php';
if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  $urlPage = "$urlPage&tab=$tab";
}

?>
<!-- <div style="position: absolute; top:4.5%; height:7%; left:3%; width:40%; font-size: 300%; font-weight: bold; text-align:left;"><?php echo $oref ?>
</div> -->

<?php
// Check the URL to see what page we are on
if (isset($_GET['pg'])) { $pg = $_GET['pg']; }else{ $pg = 0; }

// Set the rows per page
$r = 15;
// Check which page we are on
if ($pg > 1) { $start = ($pg * $r) - $r; }else { $start = 0; }

// sets the order type
// all open orders overdue
// if ($cOIDod == 0) {
// }
if (isset($_GET['tab'])) {
$sp = $_GET['tab'];
}

$iniSelect = 'TOTALS.OID';
// $sp = $tab ;
$divpos = 0;
// $tabpos = 60;
$pagepos = 92;
$OC = 0;
$cp = "orders due out after 2 weeks";
$hc = 'black';
$hbc = '#92ef9d';
$r = 20;

$sdate = date('d-M-Y', $sd);

if(isset($_GET['ord'])){ $ord = $_GET['ord']; }else{ $ord = 'oidd.item_del_date'; }
if(isset($_GET['sort'])){ $sort = $_GET['sort']; }else{ $sort = 'ASC'; }

// echo "<br>Company ID : $CID";
// echo "<br>Order status : $OC";
// echo "<br>Today : $td";
// echo "<br>Start day : $sd";
// echo "<br>End day : $ed";
// echo "<br>Count_orders - due date source : $dr";

include 'order_page_count.inc.php';

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
// NEEDS TO COUNT OIID NOT OID n
$PTV = ($cOIID / $r);
$PTV = ceil($PTV);
// Check which page we are on
if ($pg > 1) { $start = ($pg * $r) - $r;
}else { $start = 0;
}

// echo '<br>The number of pages will be : '.$PTV;
// echo '<br>Start record is : '.$start;
// echo "<br>**** count order items : $cOIID ****";

if ($cOID < 1) {
  // echo "<br>NO orders placed";
  ?>
  <div class="trs" style="position:absolute; top:<?php echo $divpos ?>%; left:0%; width:100%;">
    <div class="cpsty" style="color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "There are 0 Orders"?></div>
  </div>
      <!-- <p>NO ORDERS TO REVIEW count = <?php echo $cOID ?></p> -->
  <?php
}else {
    $bc = '#ffffd1';
  ?>
  <!-- <div class="cpsty" style="position:absolute; top:<?php echo $divpos ?>%; left:0%; width:100%; background-color:<?php echo $hbc ?>; padding-top:0.2%;"><a style="color:blue; text-decoration:none;" href="home.php?H&aot">Items Review</a></div> -->
  <table class="trs" style="position:absolute; top:35%;width:100%;">
    <caption style="text-align:left; text-indent:2%;">ITEMS ON ORDER</caption>
    <tr>
      <th colspan="5" style="background-color:#f1f1f1;"></th>
      <th colspan="6" style="background-color:<?php echo $bc ?>; border-radius: 10px 10px 0px 0px;">In-line Monitoring Points</th>
    </tr>
    <tr>
      <!-- <th style="width:1%;"></th> -->
      <th style="width:3%;">Item</th>
      <th style="width:16%; text-align:left; text-indent:5%;">Division</th>
      <th style="width:27%; text-align:left; text-indent:5%;">Item Reference</th>
      <th style="width:8%;">Short Code</th>
      <th style="width:4%; text-align: center;">Qty</th>
      <th style="width:4%; text-align: center; background-color:<?php echo $bc ?>;"><a style="color:blue; font-weight:bold; text-decoration:none;" href="loading.php?L&tab=1">Awt</a></th>
      <th style="width:4%; text-align: center; background-color:<?php echo $bc ?>;"><a style="color:blue; font-weight:bold; text-decoration:none;" href="loading.php?L&tab=2">MP1</a></th>
      <th style="width:4%; text-align: center; background-color:<?php echo $bc ?>;"><a style="color:blue; font-weight:bold; text-decoration:none;" href="loading.php?L&tab=3">MP2</a></th>
      <th style="width:4%; text-align: center; background-color:<?php echo $bc ?>;"><a style="color:blue; font-weight:bold; text-decoration:none;" href="loading.php?L&tab=4">MP3</a></th>
      <th style="width:4%; text-align: center; background-color:<?php echo $bc ?>;"><a style="color:blue; font-weight:bold; text-decoration:none;" href="loading.php?L&tab=5">MP4</a></th>
      <th style="width:4%; text-align: center; background-color:<?php echo $bc ?>;"><a style="color:blue; font-weight:bold; text-decoration:none;" href="loading.php?L&tab=6">MP5</a></th>
      <th style="width:4%; text-align: center;background-color:#76ffb5;"><a style="color:blue; font-weight:bold; text-decoration:none;" href="warehouse.php?W&tab=0">Whs</a></th>
      <th style="width:4%; text-align: center;background-color:#aff3ff;">Sent</th>
      <th style="width:10%; text-align:right; padding-right:2%;">Due date</th>
    </tr>
  <?php
  include 'order_QUERY_ORDER_NUMBER.inc.php';
  ?>
  <div style="position:absolute; top:<?php echo $pagepos ?>%; right:5%; font-size:200%;">

    <?php
    for ($x = 1 ; $x <= $PTV ; $x++){
    if ($x == 1) {
      // echo "PTV0 = $PTV";
    }else {
      // echo "PTV1 = $PTV";
      echo "<a style='color:blue; text-decoration:none;' href='$urlPage&$sp&o=$OID&pg=$x'>  $x  </a>";
    }
  }
  ?>
  </div>
  <?php
} ?>

<form  action="index.html" method="post">
  <button class="entbtn" type="submit" style="position:absolute; top:33%; left:80%; width:10%;" name="button">ADD ITEM</button>
</form>
