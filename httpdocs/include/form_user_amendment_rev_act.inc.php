<?php
include 'dbconnect.inc.php';
// echo "include/form_unlock_user.inc.php";

if (!isset($_POST['re_act']) && !isset($_POST['cancel'])) {
  // echo "WRONG METHOD";
  header("Location:../index.php");
  exit();
}elseif (isset($_POST['cancel'])) {
  // echo "CANCEL";
  header("Location:../company.php?C&usa");
  exit();
}elseif ((isset($_POST['re_act']))) {
  $sUID = $_POST['sUID'];
  $urlPage = $_POST['url'];
  // echo "Reactivate the user";
  // CHECK that the user is an admin user
  $sql = "UPDATE users
          SET active = 2
          WHERE ID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-fuu</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $sUID);
    mysqli_stmt_execute($stmt);
  }
  header("Location:../$urlPage&usa");
  exit();
}
