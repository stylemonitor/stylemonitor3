<?php
session_start();
include 'dbconnect.inc.php';
// echo  "<br><b>check_company_country_use.inc.php</b>";
// echo  "<br>ACID : $ACID";

$sql = "SELECT COUNT(ID) as cCYID
        FROM company
        WHERE name LIKE ?
        AND CYID = ?
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt,$sql)) {
  echo "FAIL-cccu";
}else {
  mysqli_stmt_bind_param($stmt,"ss", $CIDn, $CYID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cCYID  = $row['cCYID'];
}

if ($cCYID <> 0) {
  // echo "<br>$CIDn is already registered in $CYID";
  header("Location:../index.php?i&SM&CUS&fn=$UIDf&s=$UIDs&co=$CIDn&p=$UIDn&e=$UIDe&cy=$CYID");
  exit();
}else {
  // echo "<br>It is a new company/country combination";
  // // echo "<br>Registration email has been sent";
  include 'form_reg_act_db.inc.php';
}
