<?php
include 'dbconnect.inc.php';

$TODO = 0;
$DONE = 1;
// echo "include/SM_work_to_do_report.inc.php";

// Check the URL to see what page we are on
if (isset($_GET['p'])) { $p = $_GET['p']; }else{ $p = 0; }

// Set the rows per page
$r = 20;
// Check which page we are on
if ($p > 1) { $start = ($p * $r) - $r; }else { $start = 0; }

// sets the order type
// all open orders overdue
// if ($cOIDod == 0) {
// }

// this needs setting depending upon the use of the page count
// default is $cOID = 10
$cOID = 10;

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cOID / $r);
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;
// echo '<br>Start record is : '.$start;
// echo "<br>**** count_orders count : $cOID ****";

?>
<table class="trs" style="position:absolute; top:41%; left:0%; width:100%;">
  <caption>WORK TO BE DONE</caption>
  <tr>
    <th style="width:10%; text-align:left; text-indent:3%;">Date</th>
    <th style="width:50%; text-align:left; text-indent:3%;">Issue</th>
    <th style="width:30%; text-align:left; text-indent:3%;">File</th>
    <th style="width:5%;">Ini's</th>
    <th style="width:5%;">Done</th>
  </tr>
<?php
$sql = "SELECT sm.ID
          , sm.date_rec
          , sm.problem
          , sm.file
          , u.userInitial
        FROM SM_problems sm
          , users u
        WHERE sm.date_fixed = ?
        AND sm.found_by = u.ID
        ORDER BY sm.date_rec
        LIMIT 5
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-aSMwtdr</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $TODO);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $ID = $row['ID'];
    $date_rec = $row['date_rec'];
    $problem = $row['problem'];
    $file = $row['file'];
    $found_by = $row['userInitial'];

    $date_rec = date('d-M-Y', $date_rec)

?>
  <tr>
    <td style="width:10%; text-align:left; text-indent:2%;"><?php echo "$date_rec"; ?></td>
    <td style="width:50%; text-align:left; text-indent:2%;"><?php echo "$problem"; ?></td>
    <td style="width:30%; text-align:left; text-indent:2%;"><?php echo "$file"; ?></td>
    <td style="width:5%; text-align:left; text-indent:2%;"><?php echo "$found_by"; ?></td>
    <th style="width:5%; text-align:center; text-indent:2%; background-color:yellow;"><a href="?B&prb=<?php echo $ID ?>">View</a></th>
  </tr>
  <?php
  }
}?>
</table>

<table class="trs" style="position:absolute; top:61%;">
  <caption>WORK DONE</caption>
  <tr>
    <th style="width:10%; text-align:left; text-indent:3%;">Date</th>
    <th style="width:50%; text-align:left; text-indent:3%;">Issue</th>
    <th style="width:30%; text-align:left; text-indent:3%;">File</th>
    <th style="width:5%;">Ini's</th>
    <th style="width:5%;">Done</th>
  </tr>
<?php

$sql = "SELECT sm.id as ID
          , sm.date_rec as date_rec
          , sm.problem as problem
          , sm.file as file
          , u.userInitial as userInitial
        FROM SM_problems sm
          , users u
        WHERE sm.date_fixed NOT IN (0)
        AND sm.fixed_by = u.ID
        AND sm.php_done = 0
        ORDER BY sm.date_rec DESC
        LIMIT 5
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-aSMwtdr2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $DONE);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $ID = $row['ID'];
    $date_rec = $row['date_rec'];
    $problem = $row['problem'];
    $file = $row['file'];
    $found_by = $row['userInitial'];

    $date_rec = date('d-M-Y', $date_rec)

?>
  <tr>
    <td style="width:10%; text-align:left; text-indent:2%;"><?php echo "$date_rec"; ?></td>
    <td style="width:50%; text-align:left; text-indent:2%;"><?php echo "$problem"; ?></td>
    <td style="width:30%; text-align:left; text-indent:2%;"><?php echo "$file"; ?></td>
    <td style="width:5%; text-align:left; text-indent:2%;"><?php echo "$found_by"; ?></td>
    <th style="width:5%; text-align:center;; text-indent:2%; background-color:yellow;"><a href="becarri.php?B&prb=<?php echo $ID ?>">View</a></th>
  </tr>

  <?php
  }
}?>
</table>
