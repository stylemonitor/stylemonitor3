<?php
session_start();
include 'dbconnect.inc.php';
// echo '<b>include/form_revise_collection_act.inc.php</b>';
if (!isset($_POST['revName']) && !isset($_POST['canName'])){
  // echo 'Incorrect method used';
  header('Location:../index.php');
  exit();
}elseif (isset($_POST['canName'])){
  // echo 'Cancel Season Adding';
  header('Location:../styles.php?S&cor');
  exit();
}elseif (isset($_POST['revName'])){
  include 'from_CID_count_collection.inc.php';

  $UID = $_SESSION['UID'];
  $SNID = $_POST['SNID'];
  $SNIDn = $_POST['SNIDn'];
  $nSNIDn = $_POST['nSNIDn'];

  // echo "<br>Original Season name : $SNIDn";
  // echo "<br>Revised Season name : $nSNIDn";
  // echo "<br>Season ID : $SNID";

  if (empty($nSNIDn)) {$nSNIDn = $SNIDn;
  }else {
    $nSNIDn = $nSNIDn;
  }

  $td = date('U');

  // Start transaction
  mysqli_begin_transaction($mysqli);
  try {
    // to

    // register the original name in log_collection
    $sql = "INSERT INTO log_seasons
              (SID, oriName, UID, inputtime)
            VALUES
              (?,?,?,?)
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-frsa</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ssss", $SNID, $SNIDn, $UID, $td);
      mysqli_stmt_execute($stmt);
      $result = mysqli_stmt_get_result($stmt);
      $row = mysqli_fetch_array($result);
    }

    $sql = "UPDATE season
            SET name = ?
            WHERE ID = ?
            ;";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      echo '<b>FAIL-frsa</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "ss", $nSNIDn, $SNID);
      mysqli_stmt_execute($stmt);
    }

  } catch (mysqli_sql_exception $exception) {
    mysqli_rollback($mysqli);

    throw $exception;
  }

  // echo ' <br>The product type has been registered';
  header("Location:../styles.php?S&ser");
  exit();
}
?>
