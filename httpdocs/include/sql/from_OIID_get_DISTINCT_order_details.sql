SELECT oi.samProd as OIIDs
        -- from_OIID_get_DISTINCT_order_details.sql
          , oi.ord_item_nos as OIIDnos
          , oi.their_item_ref as OIIDoir
          , oi.pItemRef as OIIDtir
          , oiq.order_qty as OIQIDq
          , oidd.item_del_date as OIDDIDd
          , o.orNos as OIDnos
          , o.OTID as OTID
        FROM order_item oi
          , order_item_qty oiq
          , order_item_del_date oidd
          , orders o
        WHERE oi.ID = ?
        AND oi.OID = o.ID
        AND oiq.OIID = oi.ID
        AND oidd.OIID = oi.ID
