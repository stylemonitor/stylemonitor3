<?php
// echo "report_style.sql.php";

$sql = "SELECT distinct(pr.ID) AS PRID
-- report_style.sql
          , pr.prod_ref AS PRIDr
          , pr.prod_desc AS PRIDd
          , pr.PTID as PTID
          , pr.inputtime as input
          , IF(pt.name='Select Product Type','TBC',pt.name) AS PTIDr
          , DIVISIONS.cDIV AS DIVISIONS
          , IF(SEASON.cSN IS NULL,'NO SEASON',SEASON.cSN) AS SEASONS
          , IF(COLLECTION.cCN IS NULL,'NO COLLECTION',COLLECTION.cCN) AS COLLECTIONS
          , pr.etm AS PRIDe
          , IF(O_ORDR.OUTST IS NULL,0,O_ORDR.OUTST) AS OUTSORD
          , IF(TOTAL_ORD.TOT_ORDER IS NULL,0,TOTAL_ORD.TOT_ORDER) AS TOTORD
        FROM prod_ref pr
        INNER JOIN product_type pt ON pr.PTID = pt.ID
        LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                      , count(DISTINCT pr2d.DID) AS cDIV
                    FROM prod_ref pr
                    INNER JOIN prod_ref_to_div pr2d ON pr.ID = pr2d.PRID
                    INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                    GROUP BY pr.ID -- , pr2d.DID
                    ) DIVISIONS ON pr.ID = DIVISIONS.PRID
        LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                      , count(DISTINCT pr2sn.SNID) AS cSN
                    FROM prod_ref pr
                    INNER JOIN prod_ref_to_season pr2sn ON pr.ID = pr2sn.PRID
                    INNER JOIN season sn ON pr2sn.SNID = sn.ID
                    INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                    GROUP BY pr.ID -- , pr2sn.SNID
                  ) SEASON ON pr.ID = SEASON.PRID
        LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                      , count(DISTINCT pr2c.PRID) As cCN
                    FROM prod_ref pr
                    INNER JOIN product_type pt ON pr.PTID = pt.ID
                    INNER JOIN prod_ref_to_collection pr2c ON pr.ID = pr2c.PRID
                    INNER JOIN collection c ON pr2c.CLID = c.ID
                    INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                    GROUP BY pr.ID -- , pr2c.PRID
                    ) COLLECTION ON pr.ID = COLLECTION.PRID
        LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                          , count(DISTINCT o.ID) AS OUTST
                        FROM prod_ref pr
                        INNER JOIN product_type pt ON pr.PTID = pt.ID
                        INNER JOIN order_item oi ON pr.ID = oi.PRID
                        INNER JOIN orders o ON oi.OID = o.ID
                        INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                        AND o.orComp = 0
                        GROUP BY pr.ID
                        ) O_ORDR ON pr.ID = O_ORDR.PRID
        LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                          , count(DISTINCT o.ID) AS TOT_ORDER
                        FROM prod_ref pr
                        INNER JOIN product_type pt ON pr.PTID = pt.ID
                        INNER JOIN order_item oi ON pr.ID = oi.PRID
                        INNER JOIN orders o ON oi.OID = o.ID
                        INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                        GROUP BY pr.ID
                        ) TOTAL_ORD ON pr.ID = TOTAL_ORD.PRID
        LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                          , count(DISTINCT o.ID) AS COMPL
                        FROM prod_ref pr
                        INNER JOIN product_type pt ON pr.PTID = pt.ID
                        INNER JOIN order_item oi ON pr.ID = oi.PRID
                        INNER JOIN orders o ON oi.OID = o.ID
                        INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                        AND o.orComp = 1
                        GROUP BY pr.ID
                        ) COM_ORDR ON pr.ID = O_ORDR.PRID
        WHERE pr.prod_ref != 'TBC'
          AND pr.ACID = ?
        ORDER BY pr.prod_ref
        LIMIT ?,?
;";
