<?php
// echo "outstanding_change_request_count.inc.php";

$sql = "SELECT COUNT(DISTINCT oic.ID) as cOICID
        -- SQL is outstanding_shange_requests_count.sql
        FROM order_item_change oic
             INNER JOIN order_placed_move opm ON oic.OPMID = opm.ID
             INNER JOIN order_placed op ON opm.OPID = op.ID
             INNER JOIN order_item oi ON op.OIID = oi.ID
             INNER JOIN orders o ON oi.OID = o.ID
             INNER JOIN order_item_qty oiq ON oiq.OIID = oi.ID
             INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
             INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
             INNER JOIN associate_companies ac ON o.tACID = ac.ID
             INNER JOIN company c ON ac.CID = c.ID
             INNER JOIN associate_companies ac1 ON o.fACID = ac1.ID
             INNER JOIN company c1 ON ac1.CID = c1.ID
        WHERE (o.fCID = ? OR o.tCID = ?)
          AND oic.status = 0;
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-ocrc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOICID = $row['cOICID'];
}
