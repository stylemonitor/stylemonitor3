SELECT *
FROM
(
SELECT DISTINCT
-- ORDER_ITEM_STATUS_MASTER.sql
-- created by JRL from a DAL sql
-- ordered by section ie Awaiting; MP1; MP2 etc
-- copied to ORDER_ITEM_STATUS_summary for the HOME.php?H page
-- removed all BUT Awaiting
     o.fCID AS cliCID
     , c.name AS cliCIDn
     , o.fACID AS cli_ACID
     , ac.name AS cli_ACIDn
     , o.fDID AS cli_DID
     , d.name AS cli_DIDn
     , o.tCID AS supCID
     , cS.name AS supCIDn
     , o.tACID AS sup_ACID
     , acS.name AS sup_ACIDn
     , o.tDID AS sup_DID
     , d.name AS sup_DIDn
     , Mvemnt.ORDER_ID AS OID
     , Mvemnt.O_ITEM_ID AS OIID
     , LPAD(o.orNos,6,0) AS ordNos
     , LPAD(Mvemnt.ord_item_nos,3,0) AS ordItemNos
     , CASE WHEN o.tCID = o.fCID
            THEN o.OTID
            ELSE CASE WHEN o.tCID = ? THEN 3
                      ELSE 4
                 END
        END AS OTID
     , Mvemnt.samProd AS samProd
     , o.our_order_ref AS ourRef
     , oi.their_item_ref AS theirRef
     , oidd.item_del_date AS item_due_date
     , date_format(from_unixtime(oidd.item_del_date),'%d %b %Y') AS item_del_date
     , yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) AS DEL_WEEK
     , CASE WHEN yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) = yearweek(curdate(),7) THEN IF(from_unixtime(oidd.item_del_date,'%Y-%m-%d') < curdate(),'1','2')
            WHEN yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) = yearweek(curdate(),7)+1 THEN '3'
            WHEN yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) > yearweek(curdate(),7)+1 THEN '4'
            ELSE '1'
       END AS STATUS
     , IF(oi.itComp = 0,'Open','Closed') AS Item_Status
     , o.CPID AS CPID
     , pr.ID AS PRID
     , pr.prod_ref AS PRIDs
     , Mvemnt.ordQty AS OIQIDq
     , Mvemnt.ordQty + SUM(Mvemnt.ORDER_DECR) AS NEW_OIQIDq
-- CALCULATE INDIVIDUAL GARMENT MOVEMENT VALUES FOR EACH SECTION
     , SUM(Mvemnt.ORDER_PLACEMENT + Mvemnt.REJ_BY_STG1 + Mvemnt.ORDER_PLACE_INCR + Mvemnt.COR_OUT_STG1 - Mvemnt.ORDER_DECR - Mvemnt.INTO_STG1 - Mvemnt.ORDER_PLACE_DECR - Mvemnt.COR_IN_STG1) AS Awaiting
     , Mvemnt.revUID AS revUID
     , Mvemnt.last_revised_time AS last_revised_time
FROM (
    SELECT DISTINCT oimr.ID AS OIMR_ID
         , o.ID AS ORDER_ID
         , oi.ID AS O_ITEM_ID
         , oi.PRID AS PRID
         , oi.ord_item_nos AS ord_item_nos
         , oi.samProd AS samProd
         , oiq.order_qty AS ordQty
         , last_update.revUID
         , last_update.last_revised_time
    -- oimr1 not required at present - oiq.order_qty is used instead
    -- OIMRID is no longer needed as you are NOT allowed to increase an order item - you must ADD another item to the order
    -- , IF((CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END)) AS ORDER_INCR

    -- Awaiting
      , IF((CASE WHEN oimr.ID = 2 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 2 THEN SUM(opm.omQty) END)) AS ORDER_PLACEMENT
      , IF((CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END)) AS ORDER_DECR
      -- ONLY USED when an item is split between two or more manufacturing units
        , IF((CASE WHEN oimr.ID = 11 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 11 THEN SUM(opm.omQty) END)) AS ORDER_PLACE_INCR
        , IF((CASE WHEN oimr.ID = 12 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 12 THEN SUM(opm.omQty) END)) AS ORDER_PLACE_DECR

    -- oimr39 to oimr40 not required at present
    FROM order_placed_move opm
         INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
         INNER JOIN order_placed op ON opm.OPID = op.ID
         INNER JOIN order_item oi ON op.OIID = oi.ID
         INNER JOIN orders o ON oi.OID = o.ID
         INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
       -- 'Last user update' sub-select
       INNER JOIN (SELECT opm_order.OIID
                          , opm_order.OPID
                          , opm_order.OPMID_MAX
                          , opm_order.revUID
                          , date_format(from_unixtime(opm.inputtime),'%d %b %Y') AS last_revised_time
                    FROM
                         (
                          SELECT oi.ID AS OIID
                                 , opm.OPID
                                 , MAX(opm.ID) AS OPMID_MAX
                                 , opm.UID AS revUID
                          FROM order_placed_move opm
                               INNER JOIN order_placed op ON op.ID = opm.OPID
                               INNER JOIN order_item oi ON oi.ID = op.OIID
                               INNER JOIN orders o ON oi.OID = o.ID
                          GROUP BY oi.ID
                         ) opm_order INNER JOIN order_placed_move opm ON opm.ID = opm_order.OPMID_MAX
                  ) last_update ON oi.ID = last_update.OIID
       -- End of 'Last user update' sub-select
   WHERE oi.samProd = ?
    GROUP BY oi.ID, oimr.ID
   ) Mvemnt INNER JOIN order_item oi ON Mvemnt.O_ITEM_ID = oi.ID
            INNER JOIN orders o ON Mvemnt.ORDER_ID = o.ID
            INNER JOIN order_item_del_date oidd ON Mvemnt.O_ITEM_ID = oidd.OIID
            INNER JOIN prod_ref pr ON Mvemnt.PRID = pr.ID
            INNER JOIN company c ON o.fCID = c.ID
            INNER JOIN company cS ON o.tCID = cS.ID
            INNER JOIN associate_companies ac ON o.fACID = ac.ID
            INNER JOIN associate_companies acS ON o.tACID = acS.ID
            INNER JOIN division d ON o.fDID = d.ID
            INNER JOIN division dS ON o.tDID = dS.ID
WHERE (o.fCID = ? OR o.tCID = ?)
AND oi.itComp IN (0)
GROUP BY Mvemnt.O_ITEM_ID
ORDER BY from_unixtime(item_due_date), ordNos, ordItemNos, OIQIDq ASC
) OTID_CHECK
-- OTID_CHECK.STATUS
-- the time frame when the order is due
-- 1=Overdue; 2=This Week; 3=Next Week; 4=After Two weeks
 WHERE OTID_CHECK.STATUS = ?
 -- OTID_CHECK.OTID
 -- this is the order type
 -- 1=Associate Sales; 2=Associate Purchases; 3=Partner Sales; 4=Partner Purchases
  AND OTID_CHECK.OTID IN (?)
ORDER BY OTID_CHECK.item_due_date,OTID_CHECK.ordNos,OTID_CHECK.ordItemNos,OTID_CHECK.OIQIDq ASC
;
