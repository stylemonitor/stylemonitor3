SELECT DISTINCT
-- report_movement_log_CLIENT.sql
     Mvemnt.Date_Time AS Movement_Date
     , Mvemnt.O_ITEM_ID AS OIID
     , Mvemnt.OIMR_ID AS OIMR_ID
     , Mvemnt.comments AS Comments
     , o.fCID
     , o.tCID
 , Mvemnt.uInitials AS uInitials
FROM (
    SELECT DISTINCT oimr.ID AS OIMR_ID
         , o.ID AS ORDER_ID
         , oi.ID AS O_ITEM_ID
         , opm.ID AS OPMID
         , opm.type AS comments
         , u.userInitial AS uInitials
         , from_unixtime(opm.inputtime,'%a %d %b %Y (%H:%i)') AS Date_Time
    FROM order_placed_move opm
         INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
         INNER JOIN order_placed op ON opm.OPID = op.ID
         INNER JOIN order_item oi ON op.OIID = oi.ID
         INNER JOIN orders o ON oi.OID = o.ID
         INNER JOIN users u ON opm.UID = u.ID
    WHERE oimr.ID IN (10,39,40,41,42,43,44,45,46,47)
    GROUP BY opm.ID, oi.ID, oimr.ID
   ) Mvemnt INNER JOIN order_item oi ON Mvemnt.O_ITEM_ID = oi.ID
            INNER JOIN orders o ON Mvemnt.ORDER_ID = o.ID
            INNER JOIN company c ON o.fCID = c.ID
            INNER JOIN company cS ON o.tCID = cS.ID
            INNER JOIN associate_companies ac ON o.fACID = ac.ID
            INNER JOIN associate_companies acS ON o.tACID = acS.ID
            INNER JOIN division d ON o.fDID = d.ID
            INNER JOIN division dS ON o.tDID = dS.ID
WHERE (o.fCID = ? OR o.tCID = ?)
  AND oi.ID = ?
GROUP BY Mvemnt.OPMID
ORDER BY Mvemnt.Date_Time ASC
LIMIT ?,?
;";