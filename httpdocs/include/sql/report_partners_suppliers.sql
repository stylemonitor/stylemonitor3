SELECT cp.inputtime AS REQUESTED
-- SQL is report_partners_suppliers.sql
       , cp.ID AS CPID
       , cp.insCID AS INITIATOR
       , cp.req_CID AS CLIENT_ID
       , rc.name AS CLIENT
       , cp.acc_CID AS SUPPLIER_ID
       , ac.name AS SUPPLIER
       , IF(cp.active=0,'PENDING',IF(cp.active=1,'ACCEPTED','REJECTED')) AS STATUS
       , IF(cp.active=0,'PENDING',IF(cp.active=1,cp.accdate,'REJECTED')) AS ACCEPTED
FROM company_partnerships cp
     INNER JOIN company rc ON cp.req_CID = rc.ID
     INNER JOIN company ac ON cp.acc_CID = ac.ID
WHERE cp.active = ?
  AND (cp.req_CID = ?)
  AND (cp.req_CID != cp.acc_CID)
  -- ORDER BY $ord $sort
  -- LIMIT $start, $rpp
;";
