SELECT COUNT(o.ID) AS cOID
        -- from_ACID_count_partner_orders_open.sql
        FROM orders o
          , associate_companies ac
        WHERE ac.ID = ?
        AND o.tACID = ac.ID
        AND o.orComp = 0;
