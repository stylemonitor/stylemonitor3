SELECT IF(CASE WHEN o.OTID IN(3,4) THEN COUNT(DISTINCT o.ID) END IS NULL,0,CASE WHEN o.OTID IN(3,4) THEN COUNT(DISTINCT o.ID) END) AS cOID
-- SQL is from_ACID_count_partner_orders_open.sql
FROM orders o INNER JOIN associate_companies ac ON o.tACID = ac.ID
WHERE ac.ID = ?
  AND o.orComp = 0;
