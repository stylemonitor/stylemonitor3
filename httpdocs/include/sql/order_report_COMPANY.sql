SELECT DISTINCT
-- order_report_COMPANY.sql
     o.fCID AS cliCID
     , c.name AS cliCIDn
     , o.fACID AS cli_ACID
     , ac.name AS cli_ACIDn
     , o.fDID AS cli_DID
     , d.name AS cli_DIDn
     , o.tCID AS supCID
     , cS.name AS supCIDn
     , o.tACID AS sup_ACID
     , acS.name AS sup_ACIDn
     , o.tDID AS sup_DID
     , d.name AS sup_DIDn
     , Mvemnt.ORDER_ID AS OID
     , Mvemnt.O_ITEM_ID AS OIID
     , LPAD(o.orNos,6,0) AS ordNos
     , LPAD(Mvemnt.ord_item_nos,3,0) AS ordItemNos
     , o.OTID AS OTID
     , Mvemnt.samProd AS samProd
     , o.our_order_ref AS ourRef
     , odd.del_Date AS ODDIDdd
     , oidd.item_del_date AS item_due_date
     , o.CPID AS CPID
     , pr.ID AS PRID
     , pr.prod_ref AS PRIDs
     , Mvemnt.ordQty AS OIQIDq
-- CALCULATE INDIVIDUAL GARMENT MOVEMENT VALUES FOR EACH SECTION
     , SUM(Mvemnt.ORDER_PLACEMENT + Mvemnt.REJ_BY_STG1 + Mvemnt.ORDER_PLACE_INCR + Mvemnt.COR_OUT_STG1 - Mvemnt.INTO_STG1 - Mvemnt.ORDER_PLACE_DECR - Mvemnt.COR_IN_STG1) AS Awaiting
     , SUM(Mvemnt.INTO_STG1 + Mvemnt.REJ_BY_STG2 + Mvemnt.COR_IN_STG1 + Mvemnt.COR_OUT_STG2 - Mvemnt.INTO_STG2 - Mvemnt.REJ_BY_STG1 - Mvemnt.COR_OUT_STG1 - Mvemnt.COR_IN_STG2) AS Stage1
     , SUM(Mvemnt.INTO_STG2 + Mvemnt.REJ_BY_STG3 + Mvemnt.COR_IN_STG2 + Mvemnt.COR_OUT_STG3 - Mvemnt.INTO_STG3 - Mvemnt.REJ_BY_STG2 - Mvemnt.COR_OUT_STG2 -Mvemnt.COR_IN_STG3) AS Stage2
     , SUM(Mvemnt.INTO_STG3 + Mvemnt.REJ_BY_STG4 + Mvemnt.COR_IN_STG3 + Mvemnt.COR_OUT_STG4 - Mvemnt.INTO_STG4 - Mvemnt.REJ_BY_STG3  - Mvemnt.COR_OUT_STG3 - Mvemnt.COR_IN_STG4) AS Stage3
     , SUM(Mvemnt.INTO_STG4 + Mvemnt.REJ_BY_STG5 + Mvemnt.COR_IN_STG4 + Mvemnt.COR_OUT_STG5 - Mvemnt.INTO_STG5 - Mvemnt.REJ_BY_STG4  - Mvemnt.COR_OUT_STG4 - Mvemnt.COR_IN_STG5) AS Stage4
     , SUM(Mvemnt.INTO_STG5 + Mvemnt.REJ_BY_WHOUSE + Mvemnt.COR_IN_STG5 + Mvemnt.COR_OUT_WHOUSE - Mvemnt.INTO_WHOUSE - Mvemnt.REJ_BY_STG5  - Mvemnt.COR_OUT_STG5 - Mvemnt.COR_IN_WHOUSE) AS Stage5
     , SUM(Mvemnt.INTO_WHOUSE + Mvemnt.FROM_CLIENT + Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.COR_IN_WHOUSE + Mvemnt.CORR_OUT_CLIENT + Mvemnt.CORR_OUT_SUPPLIER
           - Mvemnt.REJ_BY_WHOUSE - Mvemnt.TO_CLIENT - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.COR_OUT_WHOUSE - Mvemnt.CORR_IN_CLIENT - Mvemnt.CORR_IN_SUPPLIER) AS Warehouse
     , SUM(Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT) AS CLIENT
     , SUM(Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER) AS SUPPLIER
     , Mvemnt.UID AS UID
     , Mvemnt.inputtime AS inputtime
FROM (
    SELECT DISTINCT oimr.ID AS OIMR_ID
         , o.ID AS ORDER_ID
         , oi.ID AS O_ITEM_ID
         , oi.PRID AS PRID
         , oi.ord_item_nos AS ord_item_nos
         , oi.samProd AS samProd
         , oiq.order_qty AS ordQty
         , last_update.UID
         , last_update.inputtime
    -- oimr1 not required at present - oiq.order_qty is used instead
         , IF((CASE WHEN oimr.ID = 2 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 2 THEN SUM(opm.omQty) END)) AS  ORDER_PLACEMENT
         , IF((CASE WHEN oimr.ID = 3 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 3 THEN SUM(opm.omQty) END)) AS INTO_STG1
         , IF((CASE WHEN oimr.ID = 4 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 4 THEN SUM(opm.omQty) END)) AS INTO_STG2
         , IF((CASE WHEN oimr.ID = 5 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 5 THEN SUM(opm.omQty) END)) AS INTO_STG3
         , IF((CASE WHEN oimr.ID = 6 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 6 THEN SUM(opm.omQty) END)) AS INTO_STG4
         , IF((CASE WHEN oimr.ID = 7 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 7 THEN SUM(opm.omQty) END)) AS INTO_STG5
         , IF((CASE WHEN oimr.ID = 8 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 8 THEN SUM(opm.omQty) END)) AS INTO_WHOUSE
    -- oimr9 to oimr10 not required at present
         , IF((CASE WHEN oimr.ID = 11 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 11 THEN SUM(opm.omQty) END)) AS ORDER_PLACE_INCR
         , IF((CASE WHEN oimr.ID = 12 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 12 THEN SUM(opm.omQty) END)) AS ORDER_PLACE_DECR
         , IF((CASE WHEN oimr.ID = 13 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 13 THEN SUM(opm.omQty) END)) AS REJ_BY_STG1
         , IF((CASE WHEN oimr.ID = 14 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 14 THEN SUM(opm.omQty) END)) AS REJ_BY_STG2
         , IF((CASE WHEN oimr.ID = 15 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 15 THEN SUM(opm.omQty) END)) AS REJ_BY_STG3
         , IF((CASE WHEN oimr.ID = 16 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 16 THEN SUM(opm.omQty) END)) AS REJ_BY_STG4
         , IF((CASE WHEN oimr.ID = 17 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 17 THEN SUM(opm.omQty) END)) AS REJ_BY_STG5
         , IF((CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END)) AS REJ_BY_WHOUSE
         , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
         , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
         , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
         , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
         , IF((CASE WHEN oimr.ID = 23 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 23 THEN SUM(opm.omQty) END)) AS COR_IN_STG1
         , IF((CASE WHEN oimr.ID = 24 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 24 THEN SUM(opm.omQty) END)) AS COR_IN_STG2
         , IF((CASE WHEN oimr.ID = 25 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 25 THEN SUM(opm.omQty) END)) AS COR_IN_STG3
         , IF((CASE WHEN oimr.ID = 26 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 26 THEN SUM(opm.omQty) END)) AS COR_IN_STG4
         , IF((CASE WHEN oimr.ID = 27 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 27 THEN SUM(opm.omQty) END)) AS COR_IN_STG5
         , IF((CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END)) AS COR_IN_WHOUSE
         , IF((CASE WHEN oimr.ID = 29 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 29 THEN SUM(opm.omQty) END)) AS COR_OUT_STG1
         , IF((CASE WHEN oimr.ID = 30 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 30 THEN SUM(opm.omQty) END)) AS COR_OUT_STG2
         , IF((CASE WHEN oimr.ID = 31 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 31 THEN SUM(opm.omQty) END)) AS COR_OUT_STG3
         , IF((CASE WHEN oimr.ID = 32 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 32 THEN SUM(opm.omQty) END)) AS COR_OUT_STG4
         , IF((CASE WHEN oimr.ID = 33 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 33 THEN SUM(opm.omQty) END)) AS COR_OUT_STG5
         , IF((CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END)) AS COR_OUT_WHOUSE
         , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
         , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
         , IF((CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END)) AS CORR_OUT_SUPPLIER
         , IF((CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END)) AS CORR_IN_SUPPLIER
    -- oimr39 to oimr40 not required at present
    FROM order_placed_move opm
       INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
       INNER JOIN order_placed op ON opm.OPID = op.ID
       INNER JOIN order_item oi ON op.OIID = oi.ID
       INNER JOIN orders o ON oi.OID = o.ID
       INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
       -- 'Last user update' sub-select
       INNER JOIN (SELECT opm_order.OPID AS OPID
                       , opm_order.UID AS UID
                       , opm_order.inputtime AS inputtime
                   FROM (SELECT opm.OPID
                         , opm.UID
                         , opm.inputtime
                       FROM order_placed_move opm
                         INNER JOIN order_placed op ON op.ID = opm.OPID
                         INNER JOIN order_item oi ON oi.ID = op.OIID
                         INNER JOIN orders o ON oi.OID = o.ID
                       ORDER BY opm.OPID, opm.ID DESC
                       ) opm_order
                   GROUP BY opm_order.OPID
                  ) last_update ON op.ID = last_update.OPID
       -- End of 'Last user update' sub-select
    WHERE oi.itComp = ?
    GROUP BY oi.ID, oimr.ID
   ) Mvemnt -- INNER JOIN order_item oi ON Mvemnt.O_ITEM_ID = oi.ID
        INNER JOIN orders o ON Mvemnt.ORDER_ID = o.ID
        INNER JOIN order_item_del_date oidd ON Mvemnt.O_ITEM_ID = oidd.OIID
        INNER JOIN prod_ref pr ON Mvemnt.PRID = pr.ID
        INNER JOIN orders_due_dates odd ON odd.OID = o.ID
        INNER JOIN company c ON o.fCID = c.ID
        INNER JOIN company cS ON o.tCID = cS.ID
        INNER JOIN associate_companies ac ON o.fACID = ac.ID
        INNER JOIN associate_companies acS ON o.tACID = acS.ID
        INNER JOIN division d ON o.fDID = d.ID
        INNER JOIN division dS ON o.tDID = dS.ID
   WHERE (o.fCID = ? OR o.tCID = ?)
     -- AND pr.ID = ?
     AND (o.fACID = ? OR o.tACID = ?)
--     AND oidd.item_del_date BETWEEN ? AND ?
GROUP BY Mvemnt.O_ITEM_ID
ORDER BY from_unixtime(item_due_date), ordNos, ordItemNos, OIQIDq ASC
LIMIT ?,?
;";
