<?php
$sql = "SELECT  Mvemnt.OICID AS OICID
-- report_movement_log_CLIENT_per_OICID.sql
        , Mvemnt.fCID AS fCID
        , Mvemnt.Req_Co AS Req_Co
        , Mvemnt.tCID AS tCID
        , Mvemnt.Resp_Co AS Resp_Co
        , Mvemnt.OIMR_ID AS OIMR_ID
        , Mvemnt.OID AS OID
        , Mvemnt.OIID AS OIID
        , Mvemnt.OPMID AS OPMID
        , Mvemnt.ordQty - SUM(Mvemnt.ORDER_DECR) AS oqtOIQID
        , Mvemnt.rqtOICIC AS rqtOICIC
        , Mvemnt.odaOIDDID AS odaOIDDID
        , Mvemnt.rdaOICIC AS rdaOICIC
        , Mvemnt.Date_REQUESTED AS Date_REQUESTED
        , Mvemnt.Requestor_ID AS Requestor_ID
        , Mvemnt.Requestor AS Requestor
        , Mvemnt.comments AS comments
        , Mvemnt.OPMIDr AS OPMIDr
        , IF(Mvemnt.Date_RESPONDED IS NULL,'',Mvemnt.Date_RESPONDED) AS Date_RESPONDED
        , IF(Mvemnt.Responder_ID IS NULL,'',Mvemnt.Responder_ID) AS Responder_ID
        , IF(Mvemnt.Responder IS NULL,'',Mvemnt.Responder) AS Responder
        , IF(Mvemnt.response IS NULL,'',Mvemnt.response) AS response
        , Mvemnt.status
FROM (
SELECT DISTINCT oic.ID AS OICID
       , o.fCID as fCID
       , c.name AS Req_Co
       , o.tCID as tCID
       , c2.name AS Resp_Co
       , oimr.ID AS OIMR_ID
       , o.ID AS OID
       , oi.ID AS OIID
       , opm.ID AS OPMID
       , opm.type AS comments
       , oic.Nqty AS rqtOICIC
       , oidd.item_del_date AS odaOIDDID
       , oic.Ndate AS rdaOICIC
       , oic.response AS OPMIDr
       , opm2.type AS response
       , u.uid AS Requestor_ID
       , concat(u.firstname,' ',u.surname) AS Requestor
       , u2.uid AS Responder_ID
       , concat(u2.firstname,' ',u2.surname) AS Responder
       , oiq.order_qty AS ordQty
       , from_unixtime(opm.inputtime,'%a %d %b %Y') AS Date_REQUESTED
       , from_unixtime(opm2.inputtime,'%a %d %b %Y') AS Date_RESPONDED
       , IF((CASE WHEN oimr2.ID = 10 THEN SUM(opm2.omQty) END) IS NULL,0,(CASE WHEN oimr2.ID = 10 THEN SUM(opm2.omQty) END)) AS ORDER_DECR
       , oic.status
FROM order_item_change oic
    LEFT OUTER JOIN order_item_change oic2 ON oic.ID = oic2.ID
    INNER JOIN order_placed_move opm ON opm.ID = oic.OPMID
    LEFT OUTER JOIN order_placed_move opm2 ON opm2.ID = oic2.response
    INNER JOIN order_placed op ON opm.OPID = op.ID
    LEFT OUTER JOIN order_placed op2 ON opm2.OPID = op2.ID
    INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
                                              AND oimr.ID != 2
    LEFT OUTER JOIN order_item_movement_reason oimr2 ON opm2.OIMRID = oimr2.ID
                                              AND oimr2.ID = 10
    INNER JOIN order_item oi ON op.OIID = oi.ID
    INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
    INNER JOIN orders o ON oi.OID = o.ID
    INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
    INNER JOIN users u ON opm.UID = u.ID
    LEFT OUTER JOIN users u2 ON opm2.UID = u2.ID
    INNER JOIN company c ON o.fCID = c.ID
    INNER JOIN company c2 ON o.tCID = c2.ID
WHERE oic.ID = ?
GROUP BY opm.ID, oimr2.ID
     ) Mvemnt
";
