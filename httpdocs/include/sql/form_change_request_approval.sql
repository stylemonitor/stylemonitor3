SELECT oic.ID as OICID
          -- form_change_request_approval.sql
          , opm.type as OPMIDt
          , opm.inputtime as OPMIDtime
          , oimr.reason as OIMRIDr
          , oidd.item_del_date as OIDDIDd
          , oic.Ndate as Ndate
          , oiq.order_qty as OIQIDq
          , oic.Nqty as Nqty
          , oi.ord_item_nos as OIIDnos
        FROM order_item_change oic
          , order_placed_move opm
          , order_placed op
          , orders o
          , order_item oi
          , order_item_qty oiq
          , order_item_movement_reason oimr
          , order_item_del_date oidd
        WHERE o.ID = ?
        AND oic.OPMID = opm.ID
        AND opm.OPID = op.ID
        AND op.OIID = oi.ID
        AND oi.OID = o.ID
        AND oic.status = 0
        AND opm.OIMRID = oimr.ID
        AND oidd.OIID = oi.ID
        AND oiq.OIID = oi.ID;
