SELECT COUNT(oic.ID) as cOICID
        -- report_order_header.sql
        FROM order_item_change oic
          , order_placed_move opm
          , order_placed op
          , orders o
          , order_item oi
          , order_item_movement_reason oimr
        WHERE o.ID = ?
        AND oic.OPMID = opm.ID
        AND opm.OPID = op.ID
        AND op.OIID = oi.ID
        AND oi.OID = o.ID
        AND oic.status = 0
        AND opm.OIMRID = oimr.ID;
