<?php
echo "<br><b>pageCount/pc_report_partners.pc.php</b>";

$sql = "SELECT COUNT(cp.inputtime) AS pageCount
-- SQL is Pending_Partners_list.sql
       , cp.ID AS CPID
       , cp.insCID AS INITIATOR
       , cp.req_CID AS CLIENT_ID
       , rc.name AS CLIENT
       , cp.acc_CID AS SUPPLIER_ID
       , ac.name AS SUPPLIER
       , IF(cp.active=0,'PENDING',IF(cp.active=1,'ACCEPTED','REJECTED')) AS STATUS
       , IF(cp.active=0,'PENDING',IF(cp.active=1,cp.accdate,'REJECTED')) AS ACCEPTED
FROM company_partnerships cp
     INNER JOIN company rc ON cp.req_CID = rc.ID
     INNER JOIN company ac ON cp.acc_CID = ac.ID
WHERE cp.active = ?
  AND (cp.req_CID = ? OR cp.acc_CID = ?)
        -- ORDER BY $ord $sort
        -- LIMIT $start, $rpp
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rp1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $pstat, $CID, $CID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($res);
  $pCount = $row['pageCount'];
}
