<?php

$sql = "SELECT cp.inputtime AS REQUESTED
-- SQL is report_partners_suppliers.sql
       , cp.ID AS CPID
       , cp.insCID AS INITIATOR
       , cp.req_CID AS CLIENT_ID
       , MIN(rac.ID) AS A_CLI_ID
       , rac.name AS A_CLIn
       , rc.name AS CLIENT
       , cp.acc_CID AS SUPPLIER_ID
       , ac.name AS SUPPLIER
       , MIN(aac.ID) AS A_SUP_ID
       , aac.name AS A_SUPn
       , IF(cp.active=0,'PENDING',IF(cp.active=1,'ACCEPTED','REJECTED')) AS STATUS
       , IF(cp.active=0,'PENDING',IF(cp.active=1,cp.accdate,'REJECTED')) AS ACCEPTED
FROM company_partnerships cp
     INNER JOIN company rc ON cp.req_CID = rc.ID
     INNER JOIN associate_companies rac ON rc.ID = rac.CID
     INNER JOIN company ac ON cp.acc_CID = ac.ID
     INNER JOIN associate_companies aac ON ac.ID = aac.CID
WHERE cp.active = ?
  AND (cp.req_CID = ?)
  AND (cp.req_CID != cp.acc_CID)
GROUP BY CPID
  -- ORDER BY $ord $sort
  -- LIMIT $start, $rpp
;";
