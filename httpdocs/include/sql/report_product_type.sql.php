<?php
// echo "sql/report_product_type.sql.php";

$sql = "SELECT distinct c.ID AS company
-- SQL is report_product_type.sql
          , pt.ID AS PTID
          , pt.inputtime as input
          , pt.scode AS Short_code
          , pt.name AS Product_Type_Ref
          , count(distinct ac.ID) AS cptACID
          , count(distinct oi.ID) AS cptOIID
          , count(distinct pr.prod_ref) AS cptSID
FROM product_type pt
     LEFT OUTER JOIN prod_ref pr ON pt.ID = pr.PTID
     LEFT OUTER JOIN order_item oi ON pr.ID = oi.PRID
     LEFT OUTER JOIN orders o ON oi.OID = o.ID
     LEFT OUTER JOIN company c ON pt.CID = c.ID
     LEFT OUTER JOIN associate_companies ac ON ac.CID = c.ID
     LEFT OUTER JOIN division d ON d.ACID = ac.ID
WHERE c.ID = ?
  AND pt.name NOT IN ('Select Product Type')
GROUP BY pt.ID
ORDER BY pt.scode
LIMIT ?,?
;"; ?>
