<?php

$sql = "SELECT ORDER_DETAILS.OIIDs AS OIIDs
-- from_OIID_get_DISTINCT_order_details.sql
      , ORDER_DETAILS.OIIDnos AS OIIDnos
      , ORDER_DETAILS.OIIDoir AS OIIDoir
      , ORDER_DETAILS.OIIDtir AS OIIDtir
      , ORDER_DETAILS.OIQIDq + SUM(ORDER_DETAILS.ORDER_INCR - ORDER_DETAILS.ORDER_DECR) AS OIQIDq
      , ORDER_DETAILS.OIDDIDd as OIDDIDd
      , ORDER_DETAILS.OIDnos AS OIDnos
      , CASE WHEN ORDER_DETAILS.tCID = ORDER_DETAILS.fCID
             THEN ORDER_DETAILS.OTID
             ELSE CASE WHEN ORDER_DETAILS.tCID = ? THEN 3
                       ELSE 4
                 END
        END AS OTID
      , ORDER_DETAILS.PRID AS PRID
      , ORDER_DETAILS.pPRID AS pPRID
      , ORDER_DETAILS.pStyleRef AS pStyleRef
      , ORDER_DETAILS.pProdType AS pProdType
FROM (SELECT oi.samProd AS OIIDs
-- from_OIID_get_DISTINCT_order_details.sql
             , oi.ord_item_nos AS OIIDnos
             , oi.their_item_ref AS OIIDoir
             , oi.pItemRef AS OIIDtir
             , oiq.order_qty AS OIQIDq
             , oidd.item_del_date as OIDDIDd
             , o.orNos AS OIDnos
             , o.OTID AS OTID
             , oi.PRID AS PRID
             , oi.pPRID AS pPRID
             , oi.pProdType AS pProdType
             , oi.pStyleRef AS pStyleRef
             , o.tCID AS tCID
             , o.fCID AS fCID
             , IF((CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END)) AS ORDER_INCR
             , IF((CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END)) AS ORDER_DECR
      FROM order_item oi
           INNER JOIN order_item_qty oiq  ON oiq.OIID = oi.ID
           INNER JOIN order_item_del_date oidd ON oidd.OIID = oi.ID
           INNER JOIN orders o ON oi.OID = o.ID
           INNER JOIN order_placed op ON oi.ID = op.OIID
           INNER JOIN order_placed_move opm ON op.ID = opm.OPID
           INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
           INNER JOIN company c ON o.fCID = c.ID
           INNER JOIN company cS ON o.tCID = cS.ID
           INNER JOIN associate_companies ac ON o.fACID = ac.ID
           INNER JOIN associate_companies acS ON o.tACID = acS.ID
           INNER JOIN division d ON o.fDID = d.ID
           INNER JOIN division dS ON o.tDID = dS.ID
      WHERE oi.ID = ?
        AND (o.fCID = ? OR o.tCID = ?)
      GROUP BY opm.ID, oi.ID, oimr.ID
     ) ORDER_DETAILS;
";
