<?php
$sql = "SELECT CASE WHEN ORDER_STATUS.DEL_WEEK = yearweek(curdate(),7)   THEN IF(ORDER_STATUS.DEL_DATE < curdate(),'1','2')
-- SQL is order_status_summary.sql revised 20-10-21
        WHEN ORDER_STATUS.DEL_WEEK = yearweek(curdate(),7)+1 THEN '3'
        WHEN ORDER_STATUS.DEL_WEEK > yearweek(curdate(),7)+1 THEN '4'
        ELSE '1'
   END AS STATUS
   , ORDER_STATUS.OTID as OTID
   , ORDER_STATUS.SAM_PROD AS SAM_PROD
   , COUNT(DISTINCT ORDER_STATUS.OIID) AS COUNT_OIID
   , COUNT(DISTINCT ORDER_STATUS.OID) AS COUNT_OID
   , ORDER_STATUS.ORDER_QTY AS ORIG_ORDER_QTY
   , ORDER_STATUS.NEW_ORDER_QTY AS ORDER_QTY
   , CASE WHEN ORDER_STATUS.OTID IN (1,3) THEN (ORDER_STATUS.NEW_ORDER_QTY - SUM(ORDER_STATUS.CLIENT))
          WHEN ORDER_STATUS.OTID IN (2,4) THEN (ORDER_STATUS.NEW_ORDER_QTY - SUM(ORDER_STATUS.SUPPLIER))
     END AS QTY
FROM
(
SELECT DISTINCT
Mvemnt.OID AS OID
, Mvemnt.O_ITEM_ID AS OIID
, Mvemnt.OIMR_ID
, yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) AS DEL_WEEK
, from_unixtime(oidd.item_del_date,'%Y-%m-%d') AS DEL_DATE
, oiq.order_qty AS ORDER_QTY
, Mvemnt.ORDER_DECR
, oiq.order_qty - Mvemnt.ORDER_DECR AS NEW_ORDER_QTY
, oi.samProd AS SAM_PROD
, CASE WHEN o.tCID = o.fCID
      THEN o.OTID
      ELSE CASE WHEN o.tCID = ? THEN 3
           ELSE 4
           END
 END AS OTID
, Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT AS CLIENT
, Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER AS SUPPLIER
FROM (
SELECT DISTINCT
     oimr.ID AS OIMR_ID
     , oimr.reason AS Reason
     , oi.ID AS O_ITEM_ID
     , opm.ID AS OPMID
     , o.ID AS OID
     -- , IF((CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END)) AS ORDER_INCR
     , IF((CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END)) AS ORDER_DECR
     , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
     , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
     , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
     , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
     , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
     , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
     , IF((CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END)) AS CORR_OUT_SUPPLIER
     , IF((CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END)) AS CORR_IN_SUPPLIER
FROM order_placed_move opm
   INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
   INNER JOIN order_placed op ON opm.OPID = op.ID
   INNER JOIN order_item oi ON op.OIID = oi.ID
   INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
   INNER JOIN orders o ON oi.OID = o.ID
-- WHERE oimr.ID IN (10,19,20,21,22,35,36,37,38)
-- GROUP BY oimr.ID, opm.ID, oi.ID
GROUP BY oimr.ID, oi.ID
) Mvemnt INNER JOIN order_item oi ON Mvemnt.O_ITEM_ID = oi.ID
      INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
      INNER JOIN orders o ON oi.OID = o.ID
      INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
      INNER JOIN company c ON o.fCID = c.ID
      INNER JOIN company cS ON o.tCID = cS.ID
      INNER JOIN associate_companies ac ON o.fACID = ac.ID
      INNER JOIN associate_companies acS ON cS.ID = acS.CID
      INNER JOIN division d ON o.fDID = d.ID
      INNER JOIN division dS ON acS.ID = dS.ACID
WHERE (o.fCID = ? OR o.tCID = ?)
-- AND oi.itComp != 9  -- Only include open order items
AND oi.itComp = 0  -- Only include open order items
GROUP BY OIMR_ID
ORDER BY OIID ASC
) ORDER_STATUS
GROUP BY STATUS, SAM_PROD, OTID;
";
