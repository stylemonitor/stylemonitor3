<?php
$sql = "SELECT  Mvemnt.OICID AS OICID
-- report_movement_log_CLIENT.sql
        , Mvemnt.fCID AS fCID
        , Mvemnt.tCID AS tCID
        , Mvemnt.OIMR_ID AS OIMRID
        , Mvemnt.OID AS OID
        , Mvemnt.OIID AS OIID
        , Mvemnt.OPMID AS OPMID
        , Mvemnt.ordQty + SUM(Mvemnt.ORDER_DECR) AS oqtOIQID
        , Mvemnt.rqtOICIC AS rqtOICIC
        , Mvemnt.odaOIDDID AS odaOIDDID
        , Mvemnt.rdaOICIC AS rdaOICIC
        , Mvemnt.Date_REQUESTED AS Date_REQUESTED
        , Mvemnt.Requestor AS Requestor
        , Mvemnt.comments AS comments
        , Mvemnt.OPMIDr AS OPMIDr
        , IF(Mvemnt.Date_RESPONDED IS NULL,'',Mvemnt.Date_RESPONDED) AS Date_RESPONDED
        , IF(Mvemnt.Responder IS NULL,'',Mvemnt.Responder) AS Responder
        , IF(Mvemnt.response IS NULL,'',Mvemnt.response) AS response
        , Mvemnt.status
FROM (
      SELECT DISTINCT oic.ID AS OICID
             , oimr.ID AS OIMR_ID
             , o.ID AS OID
             , oi.ID AS OIID
             , opm.ID AS OPMID
             , opm.type AS comments
             , oic.Nqty AS rqtOICIC
             , oidd.item_del_date AS odaOIDDID
             , oic.Ndate AS rdaOICIC
             , oic.response AS OPMIDr
             , opm2.type AS response
             , u.uid AS Requestor
             , u2.uid AS Responder
             , oiq.order_qty AS ordQty
             , opm.inputtime AS Date_REQUESTED
             , opm2.inputtime AS Date_RESPONDED
             , IF((CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END)) AS ORDER_DECR
             , oic.status
             , o.fCID as fCID
             , o.tCID as tCID
      FROM order_item_change oic
          LEFT OUTER JOIN order_item_change oic2 ON oic.ID = oic2.ID
          INNER JOIN order_placed_move opm ON opm.ID = oic.OPMID
          LEFT OUTER JOIN order_placed_move opm2 ON opm2.ID = oic2.response
          INNER JOIN order_placed op ON opm.OPID = op.ID
          LEFT OUTER JOIN order_placed op2 ON opm2.OPID = op2.ID
          INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
          INNER JOIN order_item oi ON op.OIID = oi.ID
          INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
          INNER JOIN orders o ON oi.OID = o.ID
          INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
          INNER JOIN users u ON opm.UID = u.ID
          LEFT OUTER JOIN users u2 ON opm2.UID = u2.ID
      WHERE oimr.ID IN (10,39,40,41,42,43,44,45,46,47)
       AND (o.fCID = ? OR o.tCID = ?)
       AND oi.ID = ?
      GROUP BY opm.ID, oimr.ID
     ) Mvemnt
GROUP BY Mvemnt.OPMID
ORDER BY Mvemnt.OICID DESC
LIMIT ?,?;
";
