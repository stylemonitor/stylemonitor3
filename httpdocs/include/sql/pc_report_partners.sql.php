<?php
// echo "<br><b>sql/pc_report_partners.sql.php</b>";

$sql = "SELECT COUNT(cp.inputtime) AS pageCount
-- SQL is pc_report_partners.sql
        FROM company_partnerships cp
        INNER JOIN company rc ON cp.req_CID = rc.ID
        INNER JOIN company ac ON cp.acc_CID = ac.ID
        WHERE cp.active = ?
        AND (cp.req_CID = ? OR cp.acc_CID = ?)
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rp1</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $pstat, $CID, $CID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($res);
  $pCount = $row['pageCount'];
}
// echo "<br>Page Count = $pCount";
