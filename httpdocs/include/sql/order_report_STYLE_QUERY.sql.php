<?php

$sql = "SELECT FIRST_CALC.*
       , FIRST_CALC.OIQIDq + FIRST_CALC.ORD_CHANGE AS NEW_OIQIDq
FROM
(
SELECT Mvemnt.ORDER_ID as OID
-- SQL is order_report_style_query.sql
       , o.fCID AS cliCID
       , c.name AS cliCIDn
       , o.fACID AS cli_ACID
       , ac.name AS cli_ACIDn
       , o.tCID AS supCID
       , cS.name AS supCIDn
       , o.tACID AS sup_ACID
       , acS.name AS sup_ACIDn
       , LPAD(o.orNos,6,'0') AS ordNos
       , o.our_order_ref AS ourRef
       , o.their_order_ref AS theirRef
       , pr.ID AS PRID
       , pr.prod_ref AS PRIDs
       , pt.ID AS PTID
       , MIN(oidd.item_del_date) AS earliest_date
       , CASE WHEN o.tCID = o.fCID
              THEN o.OTID
              ELSE CASE WHEN o.tCID = ? THEN 3
              ELSE 4
              END
         END AS OTID
       , ord_qty.COUNT_OIID AS OIID_COUNT
       , ord_qty.qty AS OIQIDq
       , sum(Mvemnt.ORDER_INCR - Mvemnt.ORDER_DECR) AS ORD_CHANGE
FROM (
       select o.ID AS OID
              , count(oi.ID) AS COUNT_OIID
              , sum(oiq.order_qty) AS qty
       from orders o inner join order_item oi ON o.id = oi.oid
                     inner join order_item_qty oiq ON oi.ID = oiq.OIID
       group by o.ID
     ) ord_qty INNER JOIN (SELECT o.ID AS ORDER_ID
                                  , IF((CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID =  9 THEN SUM(opm.omQty) END)) AS ORDER_INCR
                                  , IF((CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END)) AS ORDER_DECR
                            FROM order_placed_move opm
                                 INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
                                 INNER JOIN order_placed op ON opm.OPID = op.ID
                                 INNER JOIN order_item oi ON op.OIID = oi.ID
                                 INNER JOIN orders o ON oi.OID = o.ID
                                 INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
                            WHERE oi.itComp = 0
                            GROUP BY o.ID -- , opm.OIMRID
                          ) Mvemnt ON Mvemnt.ORDER_ID = ord_qty.OID
               INNER JOIN orders o ON Mvemnt.ORDER_ID = o.ID
               INNER JOIN order_item oi ON o.ID = oi.OID
               INNER JOIN prod_ref pr ON oi.PRID = pr.ID
               INNER JOIN product_type pt ON pr.PTID = pt.ID
               INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
               INNER JOIN orders_due_dates odd ON odd.OID = o.ID
               INNER JOIN order_type ot ON o.OTID = ot.ID
               INNER JOIN company c ON o.fCID = c.ID
               INNER JOIN company cS ON o.tCID = cS.ID
               INNER JOIN associate_companies ac ON o.fACID = ac.ID
               INNER JOIN associate_companies acS ON o.fACID = acS.ID
               INNER JOIN division d ON o.fDID = d.ID
               INNER JOIN division dS ON o.fDID = dS.ID
WHERE (o.fCID = ? OR o.tCID = ?)
 AND pr.ID = ?
GROUP BY o.ID
) FIRST_CALC
ORDER BY earliest_date,ordNos, NEW_OIQIDq ASC
LIMIT ?,?
;";
