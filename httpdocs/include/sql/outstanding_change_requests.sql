SELECT c.ID AS tCID
-- SQL is outstanding_shange_requests.sql
       , c.name AS tCIDn
       , c1.ID AS fCID
       , c1.name AS fCIDn
       , oic.ID AS OICID
       , opm.type as OPMIDt
       , opm.inputtime as OPMIDtime
       , oimr.reason as OIMRIDr
       , oidd.item_del_date as OIDDIDd
       , oic.Ndate as Ndate
       , oiq.order_qty as OIQIDq
       , oic.Nqty as Nqty
       , oi.ord_item_nos as OIIDnos
FROM order_item_change oic
     INNER JOIN order_placed_move opm ON oic.OPMID = opm.ID
     INNER JOIN order_placed op ON opm.OPID = op.ID
     INNER JOIN order_item oi ON op.OIID = oi.ID
     INNER JOIN orders o ON oi.OID = o.ID
     INNER JOIN order_item_qty oiq ON oiq.OIID = oi.ID
     INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
     INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
     INNER JOIN associate_companies ac ON o.tACID = ac.ID
     INNER JOIN company c ON ac.CID = c.ID
     INNER JOIN associate_companies ac1 ON o.fACID = ac1.ID
     INNER JOIN company c1 ON ac1.CID = c1.ID
WHERE (o.fCID = ? OR o.tCID = ?)
  AND oic.status = 0
ORDER BY opm.inputtime ASC;

