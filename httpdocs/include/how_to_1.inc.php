<?php
include 'dbconnect.inc.php';
include 'from_UID_get_user_details.inc.php';
$sm = '<b>S</b>tyle<b>M</b>onitor';

?>
<div class="cpsty" style="top:0%; height:3%; left:0%; width:100%; background-color:#e08df4;">
  <p>How to ... 1</p>
</div>
<div class="" style="top:3%; height:97%; left:0%; width:100%; background-color:#f4f4f4;">
  <br>
  <p>If this is your first time on <?php echo $sm ?> we know it can be a little daunting.</p>
  <br>
  <p>Where do I go ... What should I do ... What if I break it.</p>
  <br>
  <p>Well don't worry, we've tried, and failed (and some times succeded), to do the latter.</p>
  <br>
  <p>For the other two, help is always here in the <b>HOW TO ... </b> tab on the left.</p>
  <br>
  <p>If it's not, let use know and we'll do something about it straight away.</p>
  <br>
  <p>For each tab on the left, there will be an index tab at the top within the <b>HOW TO ... </b> section.</p>
  <br>
  <p>So just click on the left hand tab and then select the area you want to know more about at the top.</p>
  <br>
  <p>There should then be a page, or two, telling you about that section and how it is supposed to work.</p>
  <br>
  <p>Now, lets get you started in <?php echo $sm ?> by going to the <a href="how_to.php?W&hts">Styles</a> page.</p>
  <br>
  <p>If you don't want to see this screen every time you open <?php echo $sm ?> click the button below.</p>
  <br>
  <p>It will still be available via the <b>How to ...</b> tab on the left.</p>
  <br>
  <br>
  <p>I almost forgot, if it's there, you can do something.  If it's not... well you know that too.</p>
</div>

<?php
  if ($UIDis == 0) {
    // echo "<br><br>$UIDis";
    ?>
    <form class="" style="position:absolute; top:85%; height:10%; left:0%; width:100%;"action="include/how_to_1_act.inc.php" method="POST">
      <button class="entbtn" style="position:absolute; top:0%; height:80%; left:35%; width:30%;"type="submit" name="stop_ht1">Don't show again</button>
    </form>
    <?php
  }else {
    echo "show no button";
  }
?>
