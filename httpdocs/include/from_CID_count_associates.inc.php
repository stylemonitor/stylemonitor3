<?php
include 'include/dbconnect.inc.php';
// echo "include/from_CID_count_associates.inc.php";
$CID = $_SESSION['CID'];

$sql = "SELECT COUNT(ac.ID) as cACID
        FROM associate_companies ac
          , company c
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND ac.ID NOT IN (SELECT MIN(ac.ID) as mACID
                          FROM associate_companies ac
                            , company c
                          WHERE c.ID = ?
                          AND ac.CID = c.ID)
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcca</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cACID = $row['cACID'];
}
