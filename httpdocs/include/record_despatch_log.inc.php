<?php
session_start();
include 'dbconnect.inc.php';

if (isset($_GET['oi'])) {
  $OPID = $_GET['oi'];
  $OIMRID = 19;
}
?>

<div class="cpsty" style="position:absolute; top:57.5%; left:0%; width:21%; border-top-right-radius: 10px; color:<?php echo $hc ?>; background-color:<?php echo $hbc; ?>"><?php echo "Despatch Record"?></div>

<table class="trs" style="position:absolute; top:60.5%; left:0%; width:100%;">
  <tr>
    <!-- <th style="width:10%;">Available</th> -->
    <th style="width:10%;">Despatched</th>
    <th style="width:40%;text-align:left; text-indent:4%;">Note</th>
    <th style="width:25%;">By</th>
    <th style="width:15%;">Date</th>
    <th style="width:2%;"></th>
  </tr>
  <!-- need to add a query to say how many have been despatched and when etc -->
  <?php
  $sql = "SELECT opm.ID as OPMID
            , opm.omQty as opmQTY
            , opm.type as OPMnote
            , u.firstname as UIDf
            , u.surname as UIDs
            , opm.inputtime as desDate
          FROM order_placed_move opm
            , users u
          WHERE opm.OIMRID = ?
          AND opm.OPID = ?
          AND opm.UID = u.ID
          ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-rdl</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "ss", $OIMRID, $OPID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_assoc($result)){
      $OPMID  = $row['OPMID'];
      $opmQTY  = $row['opmQTY'];
      $OPMnote = $row['OPMnote'];
      $UIDf    = $row['UIDf'];
      $UIDs    = $row['UIDs'];
      $desDate = $row['desDate'];

      $desDate = date('d-M-Y', $desDate);
      ?>
  <tr>
    <!-- <td style="width:10%;"></td> -->
    <td style="width:10%;"><?php echo $opmQTY ?></td>
    <td style="width:40%;text-align:left; text-indent:2%;"><?php echo $OPMnote ?></td>
    <td style="width:25%;"><?php echo "$UIDf $UIDs" ?></td>
    <td style="width:15%;"><?php echo $desDate ?></td>
    <td style="width:2%; background-color:yellow;"><a style="text-decoration:none;" href="?O&orb&oi=<?php echo $OPID ?>&des=<?php echo $OPMID ?>">e</a> </td>
  </tr>
  <?php
    }
  } ?>
</table>
