<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_get_divisions.inc.php</b>";

$sql = "SELECT d.ID as DID
          , d.name as DIDn
        FROM division d
        WHERE d.ID = ?
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  // echo '<b>FAIL-fcgd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $DID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $DID = $row['DID'];
  $DIDn = $row['DIDn'];
}
