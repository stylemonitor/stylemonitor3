<?php
include 'dbconnect.inc.php';
// echo "<br><b>form_new_orders_combined_plus.inc.php</b>";
$CID = $_SESSION['CID'];
if (isset($_GET['orp'])) {
  $OID = $_GET['orp'];
}

include 'basic_order_details.inc.php';
include 'from_OID_count_order_item_open.inc.php';
include 'report_order_item.inc.php';
include 'from_fDID_get_min_section.inc.php';
include 'SM_colours.inc.php';
include 'set_urlPage.inc.php';

if ($OTID == 1) {
  $pddh = 'Sales Order';
  $pddhbc = '#f7cc6c';
}elseif ($OTID == 2) {
  $pddh = 'Purchase Order';
  $pddhbc = '#bbbbff';
}elseif ($OTID == 4) {
  $pddh = 'Partner Order';
  $pddhbc = '#c9d1ce';
}
// echo "Order Number : $OID";
// $cOIID = $cOIID + 1;
// echo "NEXT Order Item Number : $cOIID";
?>

<!-- the 'header' -->
<?php
$td = date('d M Y', $td);
// include 'page_description_date_header.inc.php';
?>

<!-- <div class="" style="position:absolute; top:0%; height:3%; left:0%; width:79%; background-color:#<?php echo $hc ?>;"><b><?php  echo "$ht"?></b></div> -->
<!-- _______________________________________________________________________________________________  -->
<div style="position:absolute; top:10%; height:3%; left:79%; width:12%; background-color:#f6ffb9; text-align:center; border-radius:5px 5px 0px 0px;">SM Record Info</div>
<!-- <div style="position:absolute; top:10.1%; height:23.9%; left:82%; width:17%; z-index:1;">Order Number</div> -->
<div class ="RNbtn" style="position:absolute; top:13.1%; height:15.9%; left:79%; width:20%; padding-right: 0.5%; font-size:250%; text-align: right; border:thin solid black;"><?php echo $orNos ?></div>
<!--  ______________________________________________________________________________________________ -->

<!-- The date the order was placed -->
<div style="position:absolute; top:25%; left:80%; width:15%; text-align:left;">Ordered</div>
<div style="position:absolute; top:24.5%; right:1%; width:18%; text-align:right; font-size: 130%; font-weight:bold; z-index:1;"><?php echo "$orderDate" ?></div>

<!-- The date the order is due -->
<!-- <div style="position:absolute; top:25%; left:80%; width:15%; text-align:left;">Due Date</div> -->
<!-- <div style="position:absolute; top:24.5%; right:1%; width:18%; text-align:right; font-size: 130%; font-weight:bold;"><?php echo "$delDate" ?></div> -->

<!-- _______________________________________________________________________________________________  -->
<!-- * * * * * LEFT / FROM Side * * * * * -->
<div style="position:absolute; top:10%; height:3%; left:0.5%; width:10%; background-color: #7ffad9; text-align: left; text-indent: 5%; border-radius:5px 5px 0px 0px;">From
</div>
<div style="position:absolute; top:13.1%; height:15.9%; left:0.5%; width:38.5%; border:thin solid grey; z-index:1;">
</div>
<div style="position:absolute; top:13.5%; left:1%; width:38.5%; font-size:200%; font-weight:bold; text-align: left;"><?php echo $fACIDn ?></div>
<!-- ADD when Divisons is fully working -->
<!-- <div style="position:absolute; top:21%; left:0.5%; width:38.5%; text-align: left; text-indent: 2%;">Division : <?php echo "$fDIDn" ?></div> -->

<?php
if (strlen($ourRef) > 35) {
  $ourRef = substr($ourRef,0,35);
  ?>
  <div style="position:absolute; top:25%; left:0.5%; width:38.5%; text-align: left; text-indent: 2%;">Ref : <?php echo "$ourRef" ?> ...</div>
  <?php
}else {
  ?>
  <div style="position:absolute; top:25%; left:0.5%; width:38.5%; text-align: left; text-indent: 2%;">Ref : <?php echo "$ourRef" ?></div>
  <?php
}
?>

<!-- _______________________________________________________________________________________________  -->

<!-- * * * * * RIGHT / TO Side * * * * * -->
<div style="position:absolute; top:10%; height:3%; left:40%; width:10%; background-color: <?php echo $pddhbc ?>; text-align: left; text-indent: 5%; border-radius:5px 5px 0px 0px;">To</div>
<div style="position:absolute; top:13.1%; height:15.9%; left:40%; width:38.5%; border:thin solid grey; z-index:1;">
</div>
<div style="position:absolute; top:13.5%; left:40.5%; width:38.5%; font-size:200%; font-weight:bold; text-align: left;"><?php echo "$tACIDn" ?></div>
<!-- ADD when Divisons is fully working -->
<!-- <div style="position:absolute; top:21%; left:40%; width:38.5%; text-align: left; text-indent: 2%; ">Division : <?php echo "$tDIDn"; ?></div> -->

<?php
if (strlen($theirRef) > 35) {
  $theirRef = substr($theirRef, 0, 35);
  ?>
  <div style="position:absolute; top:25%; left:40%; width:38.5%; text-align: left; text-indent: 2%; ">Ref : <?php echo "$theirRef" ?> ...</div>
  <?php
}else {
  ?>
  <div style="position:absolute; top:25%; left:40%; width:38.5%; text-align: left; text-indent: 2%; ">Ref : <?php echo "$theirRef" ?></div>
  <?php
}
?>


<!-- _______________________________________________________________________________________________  -->

<form class="" action="include/form_new_order_item_entry_act.inc.php" method="post">
  <input type="hidden" name="ACID" value="<?php echo $ACID ?>">
  <input type="hidden" name="OID" value="<?php echo $OID ?>">
  <input type="hidden" name="url" value="<?php echo $urlPage ?>">
  <input type="hidden" name="mSID" value="<?php echo $fSID ?>">

<?php
include 'form_new_order_item_entry.inc.php';
?>
<button class="entbtn" type="submit" name="addItem" style="position:absolute; bottom:5%; left:72.5%; width:10%; background-color: <?php echo $fsvCol ?>; z-index:1;">Add Item</button>
<button formnovalidate class="entbtn" type="submit" name="canItem" style="position:absolute; bottom:5%; left:85%; width:10%; background-color: <?php echo $fcnCol ?>; z-index:1;">Close</button>

</form>
