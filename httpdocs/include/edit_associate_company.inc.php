<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/edit_associate_company.inc.php</b>";

if (isset($_GET['a'])) {
  $ACID = $_GET['a'];
}

if (isset($_GET['AC'])) {
  $type = 'AC';
}elseif (isset($_GET['AS'])) {
  $type = 'AS';
}

include 'include/from_ACID_get_associate_company.inc.php';
// echo "type = $asCTID";

// $td = date('U');
$td = date('d M Y', $td);
$pddh = 'Edit Associate Company Details';
$pddhbc = '#fffddd';
include 'page_description_date_header.inc.php';
?>

<div class="overlay"></div>

<div style="position:absolute; top:25%; height:20%; left:30%; width:40%; border: 2px ridge grey; border-radius:10px; background-color:<?php echo $edtCol ?>; font-weight:bold;">
  Your SMIC will remain the same as before
</div>

<form action="include/edit_associate_company_act.inc.php" method="POST">
  <!-- The hidden values needed -->
  <input type="hidden" name="CTID" value="<?php echo "$asCTID"; ?>">
  <input type="hidden" name="ASACID" value="<?php echo "$ACID"; ?>">
  <input type="hidden" name="asCYID" value="<?php echo "$asCYID"; ?>">
  <input type="hidden" name="asCTID" value="<?php echo "$asCTID"; ?>">
  <input type="hidden" name="ACIDn" value="<?php echo "$ACIDn"; ?>">

  <!-- Just to 'show' what the names to be changed are -->
  <div style="position:absolute; top:15%; left:32%; width:36%; background-color:#f1f1f1; font-size: 170%; font-weight: bold; text-align:right; z-index:1;"><?php echo "$ACIDn" ?>dddfff</div>
  <div style="position:absolute; top:20%; left: 33%; width:36%; background-color:#f1f1f1; font-size: 170%; font-weight: bold; text-align:right; z-index:1;"><?php echo "$asCYIDn" ?>fffddd</div>

  <!-- The revised data to be input into the system -->
  <input autofocus type="text" style="position:absolute; top:30%; left:32%; width:36%;  z-index:1;" name="ACIDn" placeholder="<?php echo "$ACIDn"; ?>" value="">
  <!-- <select class ="sel_dropdown" style="position:absolute; top:13%; height:7%; left:31%; width:40%;" name="NCTID"> -->
    <?php
    // include 'include/from_db_get_rel_type.inc.php';
    ?>
  <!-- </select> -->
  <select class ="sel_dropdown" style="position:absolute; top:40%; height:4%; left:32%; width:36%;" name="NCYID">
    <option value="">Select a Country</option>
    <?php
    include 'select_company_country_details.php';
    ?>
  </select>

  <button class="entbtn" style="position:absolute; top:45%; left:37.5%; width:10%; background-color:<?php echo $fsvCol ?>;" type="submit" name="update_associate_name">Update</button>

  <!-- ADD a division if required -->
<!--      _____________________________________________________________________      -->
  <!-- <div style="position:absolute; top:36%; height:6%; left:10%; width:20%; font-size:150%; font-weight:bold; text-align:right;">Add Division</div> -->
  <!-- <input type="text" style="position:absolute; top:35%; height:4%; left:31%; width:39.5%;" name="add_div" value=""> -->
  <!-- <button class="entbtn" style="position:absolute; top:36%; height:5%; left:80%; width:10%;" type="submit" name="update_associate_name">Update</button> -->

  <!-- ADD an ORDER TOLERANCE if known -->
<!--      _____________________________________________________________________      -->
  <!-- <div style="position:absolute; top:43%; left:5%; width:90%; border-bottom:thin solid black;"></div> -->
  <!-- <div style="position:absolute; top:46%; height:6%; left:10%; width:20%; font-size:150%; font-weight:bold; text-align:right;">Order Tolerance</div> -->
  <!-- <input type="text" style="position:absolute; top:45%; height:4%; left:31%; width:39.5%;" name="ordtol" value=""> -->
  <!-- <button class="entbtn" style="position:absolute; top:46%; height:5%; left:80%; width:10%;" type="submit" name="update_associate_name">ADD</button> -->

  <!--      _____________________________________________________________________      -->
  <!-- <div style="position:absolute; top:43%; left:5%; width:90%; border-bottom:thin solid black;"></div>
  <input readonly class="edit_user" type="text" style="top:47%; height:6%; left:3%; width:27%; background-color:#f1f1f1; border-style:none; text-align:right;" name="ACIDn" value="<?php echo "$ACIDn"; ?>">
  <select class ="sel_dropdown" style="position:absolute; top:48%; height:7%; left:31%; width:40%;" name="NCTID">
    <?php
    include 'include/from_db_get_rel_type.inc.php';
    ?>
  </select> -->

  <!-- <button class="entbtn" style="position:absolute; top:49%; height:5%; left:80%; width:10%;" type="submit" name="update_associate_name">Update</button> -->
  <button class="entbtn" style="position:absolute; top:45%; left:52.5%; width:10%; background-color:<?php echo $fcnCol ?>;" type="submit" name="cancel_associate_name">Cancel</button>
</form>
