<?php
session_start();
// echo "<b>include/styles.inc.php</b>";
include 'dbconnect.inc.php';
$CID = $_SESSION['CID'];

// echo "<b>start_SM.php</b>";
// echo "<br>No orders have yet been placed on the system";

// Select the tab along the top

// Setthe report date
$td = date('U');
$MD = date('d M Y', $td);
if (isset($_GET['rep'])) {
  $movDate = $_GET['rep'];
}

if (isset($_GET['mp'])) {
  if ($tab == 9) {
    // SUMMARY tab for section
    include 'include/report_ORDER_STATUS_due_date.inc.php';
  }elseif ($tab == 0) {
    // AWAITING
    include 'include/report_factory_loading_awaiting.inc.php';
  }elseif ($tab == 1) {
    // Section MP1
    include 'include/report_factory_loading_1.inc.php';
  }elseif ($tab == 2) {
    // Section MP2
    include 'include/report_factory_loading_2.inc.php';
  }elseif ($tab == 3) {
    // Section MP3
    include 'include/report_factory_loading_3.inc.php';
  }elseif ($tab == 4) {
    // Section MP4
    include 'include/report_factory_loading_4.inc.php';
  }elseif ($tab == 5) {
    // Section MP5
    include 'include/report_factory_loading_5.inc.php';
  }
  include 'form_item_movement_update.inc.php';
}elseif (isset($_GET['oi'])) {
  $OIID = $_GET['oi'];
  include 'include/REPORT_ORDER_header.inc.php';
  include 'include/report_order_movement.inc.php';
}elseif (isset($_GET['o'])) {
  $OID = $_GET['o'];
  include 'REPORT_ORDER_header.inc.php';
  include 'report_order_by_item.inc.php';
}elseif (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
  if ($tab == 9) {
    $secName = 'All';
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date.inc.php';
      include 'form_select_movement_date.inc.php';
    }else {
      // SUMMARY tab for section
      include 'include/report_ORDER_STATUS_due_date.inc.php';
    }
  }elseif ($tab == 0) {
    $secName = 'Awaiting';
    if (isset($movDate)) {
      echo "string";
      $movDate = $_GET['rep'];
      include 'report_movement_date.inc.php';
      include 'form_select_movement_date.inc.php';
    }else {
      echo "BEAST";
      // AWAITING
      // $secName = 'Factory Loading';
      include 'include/report_factory_loading_awaiting.inc.php';
    }
  }elseif ($tab == 1) {
    $secName = '1';
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date_stage1.inc.php';
      include 'form_select_movement_date.inc.php';
    }else {
      // Section #1
      include 'include/report_factory_loading_1.inc.php';
    }
  }elseif ($tab == 2) {
    $secName = '2';
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date_stage2.inc.php';
      include 'form_select_movement_date.inc.php';
    }else {
      // Section #2
      include 'include/report_factory_loading_2.inc.php';
    }
  }elseif ($tab == 3) {
    $secName = '3';
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date_stage3.inc.php';
      include 'form_select_movement_date.inc.php';
    }else {
      // Section #3
      include 'include/report_factory_loading_3.inc.php';
    }
  }elseif ($tab == 4) {
    $secName = '4';
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date_stage4.inc.php';
      include 'form_select_movement_date.inc.php';
    }else {
      // Section #4
      include 'include/report_factory_loading_4.inc.php';
    }
  }elseif ($tab ==5) {
    $secName = '5';
    if (isset($_GET['rep'])) {
      $movDate = $_GET['rep'];
      include 'report_movement_date_stage5.inc.php';
      include 'form_select_movement_date.inc.php';
    }else {
      // Section #5
      include 'include/report_factory_loading_5.inc.php';
    }
  }
}
