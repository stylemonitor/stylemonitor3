<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_get_company_address.inc.php</b>";
$sql = "SELECT MIN(a.ID) as AID
          , a.addn as addn
          , a.add1 as add1
          , a.add2 as add2
          , a.city as city
          , a.pcode as pcode
          , a.county as county
          , a.tel as tel
          -- , cty.name as CTYIDn
        FROM company c
        , associate_companies ac
        , division d
        , company_division_address cda
        , addresses a
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND cda.DID = d.ID
        AND cda.AID = a.ID
        ;";
        $stmt = mysqli_stmt_init($con);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
          echo '<b>FAIL-fagca</b>';
        }else{
          mysqli_stmt_bind_param($stmt, "s", $CID);
          mysqli_stmt_execute($stmt);
          $result = mysqli_stmt_get_result($stmt);
          $row    = mysqli_fetch_assoc($result);
          $AID    = $row['AID'];
          $addn   = $row['addn'];
          $add1   = $row['add1'];
          $add2   = $row['add2'];
          $city   = $row['city'];
          $pcode  = $row['pcode'];
          $county = $row['county'];
          $tel    = $row['tel'];
          // $AIDc   = $row['CTYIDn'];
        }
