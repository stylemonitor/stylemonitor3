<?php
include 'dbconnect.inc.php';
// echo "<br><b>include/order_QUERY_ORDER_NUMBER.inc.php</b>";

include 'set_urlPage.inc.php';
include 'from_UID_get_last_logout.inc.php'; // $LRIDout
include 'ot_sp_selection_movement.inc.php';

$url = $urlPage;

$CID = $_SESSION['CID'];

$OC = 0;

if (isset($_GET['o'])) {
  $OID = $_GET['o'];
}

include 'sql/order_query_order_number.sql.php';

$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-oQON</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssssss", $CID, $CID, $CID, $OID, $start, $r);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $cliCID = $row['cliCID'];
    $cliCIDn = $row['cliCIDn'];
    $cli_ACID = $row['cli_ACID'];
    $cli_ACIDn = $row['cli_ACIDn'];
    $cli_DIDn = $row['cli_DIDn'];
    $supCID = $row['supCID'];
    $supCIDn = $row['supCIDn'];
    $sup_ACID = $row['sup_ACID'];
    $sup_ACIDn = $row['sup_ACIDn'];
    $OID = $row['OID'];
    $OIID = $row['OIID'];
    $OIDnos = $row['ordNos'];
    $OIIDnos = $row['ordItemNos'];
    $OTID = $row['OTID'];
    $samProd = $row['samProd'];
    $ourRef = $row['ourRef'];
    $theirRef = $row['theirRef'];
    $ODDIDdd = $row['ODDIDdd'];
    $OIDDIDd = $row['item_due_date'];
    $CPID = $row['CPID'];
    $PRID = $row['PRID'];
    $PRIDs = $row['PRIDs'];
    $PRIDn = $row['Prod_desc'];
    $pPRID = $row['pPRID'];
    $pPRIDs = $row['pPRIDs'];
    $pPRIDn = $row['pProd_desc'];
    $OIQIDq = $row['OIQIDq'];
    $Awaiting = $row['Awaiting'];
    $Stage1 = $row['Stage1'];
    $Stage2 = $row['Stage2'];
    $Stage3 = $row['Stage3'];
    $Stage4 = $row['Stage4'];
    $Stage5 = $row['Stage5'];
    $Warehouse = $row['Warehouse'];
    $CLIENT = $row['CLIENT'];
    $SUPPLIER = $row['SUPPLIER'];

// opPRID to be added as the old pPRID

// Taken OUT as DAL sorted in his sql
    // if ($cliCID <> $CID) {
    //   $cli_ACIDn = $cli_ACIDn;
    //   $OTID = $OTID - 1;
    // }else {
    //   $cli_ACIDn = $sup_ACIDn;
    //   $OTID = $OTID;
    // }

    $idate = date('d-M-Y', $OIDDIDd);

    if ($samProd == 0) {
      $spc ='#76f8bc';
      $spbc = '#76f8bc';
    }else {
      $spc = '#fa7b7b';
      $spbc = '#fa7b7b';
    }
    // Shows you the orders placed within the last 7 days
    // if($od>($td-604740)){
    if ($OTID == 1) {
      $c = 'black';
      $bc = 'f7cc6c';
    }elseif ($OTID == 2) {
      $c = 'black';
      $bc = 'bbbbff';
    }elseif ($OTID == 3) {
      $c = 'white';
      $bc = 'f243e4';
    }elseif ($OTID == 4) {
      $c = 'white';
      $bc = '577268';
    }
    ?>
    <tr style="font-family: monospace;">
      <td style="background-color:<?php echo $spbc?>; text-align: center; padding-right:0.2%;"><a style="color:blue; text-decoration:none; " href="<?php echo $url ?>&rt3&o=<?php echo $OID ?>&OI=<?php echo $OIID;?>"><?php echo $OIIDnos;?></a></td>
      <?php
      if (($OTID == 1) || ($OTID == 2) || ($OTID == 3)) {
        if (strlen($theirRef)>22) {
          $theirRef = substr($theirRef,0,22);
          ?>
          <td style="text-align:left; text-indent:1%; border-left: thin solid grey; border-right:thin solid grey;"><?php echo $theirRef ?> ...</td>
          <?php
        }else {?>
          <td style="text-align:left; text-indent:1%; border-left: thin solid grey; border-right:thin solid grey; background-color:<?php echo $CCOL ?>"><?php echo $theirRef ?></td>
          <?php
        }
        ?>
        <td style="width:8%; text-align:left; text-indent:1%;"><a class="hreflink" href="styles.php?S&sio&s=<?php echo $PRID ?>"> <?php echo $PRIDs ?></a></td>
        <?php

        if (strlen($PRIDn)>25) {
          $PRIDn = substr($PRIDn,0,25);
          ?>
          <td style="width:22%; text-align:left; text-indent:1%; border-right:thin solid grey;"><a class="hreflink" href="styles.php?S&sio&s=<?php echo $PRID ?>"> <?php echo "$PRIDn";?> ...</a></td>
          <?php
        }else {
          ?>
          <td style="width:22%; text-align:left; text-indent:1%; border-right:thin solid grey;"><a class="hreflink" href="styles.php?S&sio&s=<?php echo $PRID ?>"> <?php echo "$PRIDn";?></a></td>
          <?php
        }
        // <td style="text-align:left; text-indent:3%; border-right:thin solid grey;"><?php echo "$PRIDs-$PRIDn";?></td>
        <?php
      }elseif ($OTID == 4) {
        // check if the PRID is matched to the CID if it is not then show it
        if (strlen($theirRef)>22) {
          $theirRef = substr($theirRef,0,22);
          ?>
          <td style="text-align:left; text-indent:1%; border-left: thin solid grey; border-right:thin solid grey;"><?php echo $theirRef ?> ...</td>
          <?php
        }else {?>
          <!-- <td style="text-align:left; text-indent:1%; border-left: thin solid grey; border-right:thin solid grey;">SUPPLIER</td> -->
          <td style="text-align:left; text-indent:1%; border-left: thin solid grey; border-right:thin solid grey;"><?php echo $theirRef ?></td>
          <?php
        }
        ?>
        <td style="width:8%; text-align:left; text-indent:1%;"><?php echo $PRIDs ?><td>
        <?php

        if (strlen($PRIDn)>25) {
          $PRIDn = substr($PRIDn,0,25);
          ?>
          <td style="width:22%; text-align:left; text-indent:1%; border-right:thin solid grey;"><?php echo "$PRIDn";?> ...</td>
          <?php
        }else {
          ?>
          <td style="width:22%; text-align:left; text-indent:1%; border-right:thin solid grey;"><?php echo "$PRIDn";?></td>
          <?php
        }
      }
      ?>
      <td></td>
      <td style="text-align:right; border-right:thin solid grey; padding-right:0.5%;"><?php echo $OIQIDq;?></td>
      <?php
      include 'movement_table_item_update.inc.php';

      if($OIDDIDd<$td){;?>
        <td style="text-align:right; padding-right:0.1%; background-color:#fc4141;"><?php echo $idate ;?></td>
        <?php
      }elseif($OIDDIDd<($td+604740)){;?>
        <td style="text-align:right; padding-right:0.1%; background-color:#f09595;"><?php echo $idate ;?></td>
        <?php
      }elseif($OIDDIDd>($td+121509600)){;?>
        <td style="text-align:right; padding-right:0.1%; background-color:#e2e8a8;"><?php echo $idate ;?></td>
        <?php
      }else{;?>
        <td style="text-align:right; padding-right:0.1%;"><?php echo $idate ;?></td>
      <?php
    }
    ?>
    </tr>
    <?php
    }
  } ?>
  <tr>
    <td colspan="15" style="border-top:2px solid black;"></td>
  </tr>
  <?php
  // include 'order_QUERY_ORDER_NUMBER_closed.inc.php';
  ?>
  <!-- </table> -->
