<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "<b>include/report_company_general.inc.php</b>";

// $CID = $_SESSION['CID'];
// $ACID = $_SESSION['ACID'];

include 'from_ACID_count_divisions.inc.php';
include 'from_CID_count_users.inc.php';
include 'from_CID_get_company_address.inc.php';
include 'from_CID_count_collection.inc.php';
include 'from_CID_count_company_partners_accepted.inc.php';
include 'from_CID_count_product_type.inc.php';
include 'from_CID_count_season.inc.php';
include 'from_CID_count_sections.inc.php';
include 'from_CID_count_styles.inc.php';
include 'from_CID_get_company_date.inc.php';
include 'from_UID_get_company_details.inc.php';
include 'from_UID_get_user_details.inc.php';

// Count of orders
include 'from_CID_count_associate_orders_open.inc.php';
include 'from_CID_count_associate_CLIENT_orders_open.inc.php';
include 'from_CID_count_associate_SUPPLIER_orders_open.inc.php';
include 'from_CID_count_partner_clients.inc.php';
include 'from_CID_count_Partner_Sales_open.inc.php';
include 'from_CID_count_partner_suppliers.inc.php';
include 'from_CID_count_Partner_Purchase_open.inc.php';
$TIDutc = $TIDoff;

$pddh = 'Company Information';
$pddhbc = $cnyCol;
// include 'page_description_date_header.inc.php';

$licexp = $clstd + $lttl;
$licexp = date('d-M-Y', $licexp);
// echo "<br><br><br><br>CID = $CID";
?>

<table class="trs" style="position:absolute; top:10%; left:2%; width:66%;">
  <tr>
    <th style="width:28%; font-size:150%; text-align:left; text-indent:5%;">Name</th>
    <td style="font-size:150%; text-align:left; text-indent:5%;"><?php echo "$CIDn"; ?></td>
  </tr>
  <tr >
    <th style="font-size:150%; text-align:left; text-indent:5%;">SMIC</th>
    <td style="font-size:150%; text-align:left; text-indent:5%; font-weight:bold;"><?php echo "$SMIC"; ?></td>
  </tr>
  <tr>
    <th style="font-size:150%; text-align:left; text-indent:5%;">Country</th>
    <td style="font-size:150%; text-align:left; text-indent:5%;"><?php echo "$CYIDn" ?></td>
  </tr>
  <tr>
    <th style="font-size:150%; text-align:left; text-indent:5%;">Timezone</th>
    <td style="font-size:120%; text-align:left; text-indent:5%;"><?php echo "$TIDn ($TIDutc)" ?></td>
  </tr>
</table>

<!-- ____________________________________________________________________________________________ -->

<table class="trs" style="position:absolute; top:36.5%; left:2%; width:46%;">
  <caption style="font-weight:bold; text-align:left; text-indent:2%;">Main Company Address</caption>
  <tr>
    <th style="width:40%; text-align:right;padding-right:2.5%;">Name</th>
    <td style="text-align:left; text-indent:2%; font-weight:bold;"><?php echo "$addn"; ?></td>
  </tr>
  <tr>
    <th style="text-align:right;padding-right:2.5%;">Address</th>
    <td style="text-align:left; text-indent:2%;"><?php echo "$add1"; ?></td>
  </tr>
  <tr>
    <th style="text-align:right;padding-right:2.5%;"></th>
    <td style="text-align:left; text-indent:2%;"><?php echo "$add2"; ?></td>
  </tr><tr>
    <th style="text-align:right;padding-right:2.5%;">City/Town</th>
    <td style="text-align:left; text-indent:2%;"><?php echo "$city"; ?></td>
  </tr><tr>
    <th style="text-align:right;padding-right:2.5%;">Post/Zip Code</th>
    <td style="text-align:left; text-indent:2%;"><?php echo "$pcode"; ?></td>
  </tr><tr>
    <th style="text-align:right;padding-right:2.5%;">County/State</th>
    <td style="text-align:left; text-indent:2%;"><?php echo "$county"; ?></td>
  </tr><tr>
    <th style="text-align:right;padding-right:2.5%;">Telephone</th>
    <td style="text-align:left; text-indent:2%;"><?php echo "$tel"; ?></td>
  </table>

<!-- ____________________________________________________________________________________________ -->
<?php
if ($UIDv <> 1) {
}else {
  ?>
  <table class="trs" style="position:absolute; top:36.5%; left:52%; width:46%">
    <caption style="font-weight:bold; text-align:left; text-indent:2%;">Licence Summary</caption>
    <tr>
      <th style="width:40%; text-align:right; padding-right:2.5%;">Licence Type</th>
      <td style="font-weight:bold;"><?php echo "$LTIDt"; ?></td>
    </tr>
    <tr>
      <th style="text-align:right; padding-right:2.5%;" >Expiry Date</th>
      <td style="font-weight:bold;"><?php echo "$licexp"; ?></td>
    </tr>
    <tr>
      <th style="text-align:right; padding-right:2.5%;"><a style="color:blue; text-decoration:none;" href="company.php?C&usa">Active Login</a></th>
      <td ><a style="color:blue; text-decoration:none;" href="company.php?C&usa"><?php echo "$cUSERSa"; ?></a></td>
    </tr>
    <tr>
      <th style="text-align:right; padding-right:2.5%;">Available Users</th>
      <?php
      $cUSERSl = ($cCIDu - $cUSERSa - $cUSERSp)
      ?>
      <td ><?php echo "$cUSERSl" ?></td>
    </tr>
    <tr>
      <?php
      if ($cUSERSp == 0) {
        ?>
        <th style="text-align:right; padding-right:2.5%;">Pending Users</th>
        <td ><?php echo "$cUSERSp"; ?></td>
        <?php
      }else {
        ?>
        <th style="text-align:right; padding-right:2.5%;"><a style="color:blue; text-decoration:none;" href="company.php?C&usp">Pending Users</a></th>
        <td ><a style="color:blue; text-decoration:none;" href="company.php?C&usp"><?php echo "$cUSERSp"; ?></a></td>
        <?php
      }
      ?>
    </tr>
    <tr>
      <th style="text-align:right; padding-right:2.5%;">Licenced Users</th>
      <td ><?php echo "$cCIDu"; ?></td>
    </tr>
    <tr>
      <?php
      // if no suspended users then NO href!
      if ($cUSERSs == 0) {
        ?>
        <th style="text-align:right; padding-right:2.5%;">Suspended Users</th>
        <td style="background-color:pink;"><?php echo "$cUSERSs"; ?></td>
        <?php
      }else {
        ?>
        <th style="text-align:right; padding-right:2.5%;"><a style="color:blue; text-decoration:none;" href="company.php?C&usu">Suspended Users</a></th>
        <td style="background-color:pink;"><a style="color:blue; text-decoration:none;" href="company.php?C&usu"><?php echo "$cUSERSs"; ?></a></td>
        <?php
      }
      ?>
    </tr>
  </table>

  <form action="include/report_company_general_act.inc.php" method="POST">
    <button class="entbtn" style="position:absolute; top:27.5%; left:15.5%; width:15%; font-weight: bold; padding-bottom:0.3%; background-color:<?php echo $edtCol ?>;" type="submit" name="edit_company_name">Edit Company Name</button>

    <button class="entbtn" style="position:absolute; top:59.5%; left:80%; width:15%;font-weight: bold; color:white; background-color:purple; padding-bottom:0.3%;" type="submit" name="update_licence">Upgrade Licence</button>
    <?php
    // Check if available users is less than or equal to licence
    include 'include/from_CID_count_users.inc.php';
    if (($cCIDu >= $cUSERSa) && ($cUSERSp <= ($cCIDu - $cUSERSa))) {
      ?>
      <button class="entbtn" style="position:absolute; top:59.5%; left:60%; width:10%;font-weight: bold; background-color:#76f481; padding-bottom:0.3%;" type="submit" name="add_user">Add User</button>
      <?php
    }
    ?>
    <button class="entbtn" style="position:absolute; top:59.5%; left:15.5%; width:15%; font-weight: bold; padding-bottom:0.3%; background-color:<?php echo $edtCol ?>;" type="submit" name="edit_address">Edit Address</button>
  </form>
  <?php
}

// if ($UIDv <> 1) {
//   // if ($caUIDp == 1) {
//   // echo "<br><br>CANNOT AMEND THINGS HERE";
// }else {
// }
?>

<!-- _________________________________________________________________________________________________________ -->
<table class="trs" style="position:absolute; top:69.5%; left:2%; width:46%;">
  <caption style="font-weight:bold; text-align:left; text-indent:2%;">Summary <?php echo $CIDn ?> Company details</caption>
  <tr>
    <th style="width:40%; text-align:right; padding-right:2.5%;">Open Orders</th>
    <td><a style="color:blue; text-decoration:none;" href="home.php?H&home"><?php echo "$cOIDopen";?></a></td>
    <!-- <td><a style="color: blue; text-decoration:none;" href="#"> <?php echo "$cOIDopen";?></a></td> -->
  </tr>
  <!-- <tr>
    <th>Divisions</th>
    <?php
    if ($cDID == 0) {
      ?>
      <td style=""><?php echo $cDID ?></th>
      <?php
    }else {
      ?>
      <td style=""><a style="text-decoration:none;" href="company.php?C&sdr"><?php echo $cDID ?></a></th>
      <?php
    }
    ?>
  </tr> -->
  <tr>
    <th style="width:40%; text-align:right; padding-right:2.5%;">Styles</th>
    <?php
    if ($cPRID == 0) {
      ?>
      <td style=""><?php echo $cPRID ?></td>
      <?php
    }else {
      ?>
      <td style=""><a style="color: blue; text-decoration:none;" href="styles.php?S&snn"><?php echo $cPRID ?></a></td>
      <?php
    }
    ?>
  </tr>
  <tr>
    <th style="width:40%; text-align:right; padding-right:2.5%;">Product Types</th>
    <?php
    if ($cPTID == 0) {
      ?>
      <td style=""><?php echo $cPTID ?></td>
      <?php
    }else {
      ?>
      <td style=""><a style="color: blue; text-decoration:none;" href="styles.php?S&spt"><?php echo $cPTID ?></a></td>
      <?php
    }
    ?>
  </tr>
  <!-- <tr>
    <th>Seasons</th>
    <?php
    if ($cSNID == 0) {
      ?>
      <td style=""><?php echo $cSNID ?></td>
      <?php
    }else {
      $cSNID = $cSNID-1;
      ?>
      <td style=""><a style="text-decoration:none;" href="styles.php?S&ser"><?php echo $cSNID ?></a></td>
      <?php
    }
     ?>
  </tr> -->
  <!-- <tr>
    <th>Collections</th>
    <?php
    if ($cCLID == 0) {
      ?>
      <td style=""><?php echo $cCLID ?></td>
      <?php
    }else {
      $cCLID = $cCLID-1;
      ?>
      <td style=""><a style="text-decoration:none;" href="styles.php?S&cor"><?php echo $cCLID ?></a></td>
      <?php
    }
    ?>
  </tr> -->
</table>

<!-- _________________________________________________________________________________________________________ -->

<table class="trs" style="position:absolute; top:69.5%; left:52%; width:46%;">
  <caption style="font-weight:bold; text-align:left; text-indent:2%;">Breakdown of Clients / Suppliers and their Orders</caption>

  <tr>
    <th style="width:50%; text-align:right; background-color: <?php echo $salAssCol ?>; padding-right:2.5%;">Associate Clients</th>
    <!-- <td>A count will appear here</td> -->
    <?php
    // echo "$cACID";
    if ($cACIDc == 0) { ?>
      <td style="width:50%;">-</td>
    <?php }else { ?>
      <td><a style="color:blue; width:50%; color:blue; text-decoration:none; " href="associates.php?A&acc"><?php echo "$cACIDc"; ?></a></td>
    <?php
    }
    ?>
  </tr>
  <tr>
    <th style="font-weight:normal; text-align:right; background-color: <?php echo $salAssCol ?>; padding-right:2.5%;">Associate Sales Orders</th>
    <td><a style="color:blue; text-decoration:none;" href="home.php?H&home"><?php echo $CcOIDo ?></a></td>
  </tr>
  <tr>
    <th style="text-align:right; background-color: <?php echo $purAssCol ?>; padding-right:2.5%;">Associate Suppliers</th>
    <!-- <td>A count will appear here</td> -->
    <?php
    if ($cACIDs == 0) { ?>
      <td style="">-</td>
    <?php }else { ?>
      <td><a style="color:blue; text-decoration:none; " href="associates.php?A&acs"><?php echo "$cACIDs"; ?></a></td>
    <?php }
    ?>
  </tr>
  <tr>
    <th style="font-weight:normal; text-align:right; background-color: <?php echo $purAssCol ?>; padding-right:2.5%;">Associate Purchase Orders</th>
    <td><a style="color:blue; text-decoration:none;" href="home.php?H&home"><?php echo $ScOIDo ?></a></td>
  </tr>
  <tr>
    <th style="text-align:right; background-color: <?php echo $salPrtCol ?>; padding-right:2.5%;">Partner Clients</th>
    <!-- <td>A count will appear here</td> -->
    <?php
    // echo "$cACID";
    if ($cCPIDcli == 0) { ?>
      <td style="">-</td>
    <?php }else { ?>
      <td><a style="color:blue; text-decoration:none;" href="partners.php?P&pac"><?php echo "$cACIDc"; ?></a></td>
    <?php
    }
    ?>
  </tr>
  <tr>
    <th style="font-weight:normal; text-align:right; background-color: <?php echo $salPrtCol ?>; padding-right:2.5%;">Partner Sales Orders</th>
    <?php
    if ($CpsOIDo == 0) {
      ?>
      <td>-</td>
      <?php
    }else {
      ?>
      <td><?php echo $CpsOIDo ?></td>
      <?php
    }
    ?>
  </tr>
  <tr>
    <th style="text-align:right; background-color: <?php echo $purPrtCol ?>; padding-right:2.5%;">Partner Suppliers</th>
    <!-- <td>A count will appear here</td> -->
    <?php
    if ($cCPIDsup == 0) { ?>
      <td style="">-</a></td>
    <?php }else { ?>
      <td><a style="color:blue; text-decoration:none;" href="partner.php?P&pas"><?php echo "$cCPIDsup"; ?></a></td>
    <?php }
    ?>
  </tr>
  <tr>
    <th style="font-weight:normal; background-color: <?php echo $purPrtCol ?>; text-align:right; padding-right:2.5%;">Partner Purchase Orders</th>
    <?php
    if ($CppOIDo == 0) {
      ?>
      <td>-</td>
      <?php
    }else {
      ?>
      <td><?php echo $CppOIDo ?></td>
      <?php
    }
    ?>
  </tr>
</table>
