<?php
session_start();
include 'dbconnect.inc.php';

if(isset($_GET['oi'])){
  $OIID = $_GET['oi'];

  // GETS THE TOTALS FOR THE SUMMARY INFORMATION AND DOES THE CALCULATIONS ON THEM
  include 'from_OIID_get_make_unit.inc.php';
  include 'from_OPID_get_qty.inc.php';
  include 'R_on_place_summd.inc.php';
  include 'R_on_sum_MP1d.inc.php';
  include 'R_on_sum_MP2d.inc.php';
  include 'R_on_sum_MP3d.inc.php';
  include 'R_on_sum_MP4d.inc.php';
  include 'R_on_sum_MP5d.inc.php';
  include 'R_on_sum_MP6d.inc.php';

  $B1 = ($upQTY - $sMP1d);
  $B2 = ($sMP1d - $sMP2d);
  $B3 = ($sMP2d - $sMP3d);
  $B4 = ($sMP3d - $sMP4d);
  $B5 = ($sMP4d - $sMP5d);
  $B6 = ($sMP5d - $sMP6d);

  // if(isset($_GET['up'])){
    // include 'include/R_on_place_summ.inc.php';
    ?>
    <!-- <table class="trs" > -->
    <table class="trs" style="position:absolute; top:37%;">
      <caption class="cpsty">Item production progress</caption>

      <tr style="background-color:#f1f1f1;">
        <td style="width:30%; background-color:#f1f1f1;"></td>
        <th style="width:12%; background-color:#f1f1f1;">Order Balance</th>
        <th style="width:12%; background-color:#f1f1f1;">1</th>
        <th style="width:12%; background-color:#f1f1f1;">2</th>
        <th style="width:12%; background-color:#f1f1f1;">3</th>
        <th style="width:12%; background-color:#f1f1f1;">4</th>
        <th style="width:12%; background-color:#f1f1f1;">5</th>
        <!-- <th style="width:12%; background-color:#f1f1f1;">6</th> -->
      </tr>

      <tr>
        <th style="background-color:#f1f1f1; font-size:100%;">Work available/in section</th>
        <!-- BUFFER 1 -->
        <?php
        if ($B1 < 0){
          echo '<td style="background-color:#f4632e;">'.($B1*(-1)).'</td>';
        }elseif ($sMP1d < $upQTY) {
          echo '<td style="background-color:#ffffba;">'.$B1.'</td>';
        }elseif ($sMP1d == $upQTY) {
          echo '<td style="color:#a6ffa6; background-color:#a6ffa6;">'.$B1.'</td>';
        }elseif ($sMP1d > $upQTY) {
          echo '<td style="background-color:#ffa2a2;">'.$B1.'</td>';
        }elseif ($B1 > 0) {
          echo '<td style="background-color:#ffa2a2;">'.$B1.'</td>';
        } ?>

        <!-- BUFFER 2 -->
        <?php
        if ($B2 < 0){
          echo '<td style="background-color:#f4632e;">'.($B2*-1).'</td>';
        }elseif ($sMP2d < $upQTY){
          echo '<td style="background-color:#ffffba;">'.$B2.'</td>';
        }elseif ($sMP2d == $upQTY){
          echo '<td style="color:#a6ffa6; background-color:#a6ffa6;">'.$B2.'</td>';
        }elseif ($sMP2d > $upQTY){
          echo '<td style="background-color:#ffa2a2;">'.$B2.'</td>';
        }elseif ($B2 > 0) {
          echo '<td style="background-color:#ffa2a2;">'.$B2.'</td>';
        } ?>

        <!-- BUFFER 3 -->
        <?php
        if ($B3 < 0){
          echo '<td style="background-color:#f4632e;">'.($B3*-1).'</td>';
        }elseif ($sMP3d < $upQTY){
          echo '<td style="background-color:#ffffba;">'.$B3.'</td>';
        }elseif ($sMP3d == $upQTY){
          echo '<td style="color:#a6ffa6; background-color:#a6ffa6;">'.$B3.'</td>';
        }elseif ($sMP3d > $upQTY){
          echo '<td style="background-color:#ffa2a2;">'.$B3.'</td>';
        }elseif ($B3 > 0) {
          echo '<td style="background-color:#ffa2a2;">'.$B3.'</td>';
        } ?>

        <!-- BUFFER 4 -->
        <?php
        if ($B4 < 0){
          echo '<td style="background-color:#f4632e;">'.($B4*-1).'</td>';
        }elseif ($sMP4d < $upQTY){
          echo '<td style="background-color:#ffffba;">'.$B4.'</td>';
        }elseif ($sMP4d == $upQTY){
          echo '<td style="color:#a6ffa6; background-color:#a6ffa6;">'.$B4.'</td>';
        }elseif ($sMP4d > $upQTY){
          echo '<td style="background-color:#ffa2a2;">'.$B4.'</td>';
        }elseif ($B4 > 0) {
          echo '<td style="background-color:#ffa2a2;">'.$B4.'</td>';
          // code...
        } ?>

        <!-- BUFFER 5 -->
        <?php
        if ($B5 < 0){
          echo '<td style="background-color:#f4632e;">'.($B5*-1).'</td>';
        }elseif ($sMP5d < $upQTY){
          echo '<td style="background-color:#ffffba;">'.$B5.'</td>';
        }elseif ($sMP5d == $upQTY){
          echo '<td style="color:#a6ffa6; background-color:#a6ffa6;">'.$B5.'</td>';
        }elseif ($sMP5d > $upQTY){
          echo '<td style="background-color:#ffa2a2;">'.$B5.'</td>';
        }elseif ($B5 > 0) {
          echo '<td style="background-color:#ffa2a2;">'.$B5.'</td>';
        } ?>

        <!-- BUFFER 6 -->
        <!-- calculates the number of garments between MP5 and MP6       -->
        <?php
        if ($B6 < 0){
          echo '<td style="background-color:#f4632e;">'.($B6*-1).'</td>';
        }elseif ($sMP6d < $upQTY){
          echo '<td style="background-color:#ffffba;">'.$B6.'</td>';
        }elseif ($sMP6d == $upQTY){
          echo '<td style="color:#a6ffa6; background-color:#a6ffa6;">'.$B6.'</td>';
        }elseif ($sMP6d > $upQTY){
          echo '<td style="background-color:#ffa2a2;">'.$B6.'</td>';
        }elseif ($sMP6d > 0) {
          echo '<td style="background-color:#ffa2a2;">'.$B5.'</td>';
        } ?>

        </tr>

    </table>
  <?php }
 ?>
