<?php
include 'dbconnect.inc.php';

$UID = $_SESSION['UID'];
$UIDc = $_SESSION['UID'];

// include 'include/from_UIDc_get_company_admin.inc.php';
include 'include/from_UID_get_user_details.inc.php';
// echo "<b>include/company.inc.php</b>";

if (isset($_GET['coi'])) {
  include 'include/report_company_general.inc.php';
  if (isset($_GET['ecn'])) {
    include 'include/edit_company_name.inc.php';
  }elseif (isset($_GET['eca'])) {
    include 'edit_company_address.inc.php';
  }elseif (isset($_GET['usc'])) {
    // echo "ADD a Users for the company";
    include 'include/form_new_company_user.inc.php';
  }elseif (isset($_GET['udl'])) {
    include 'upgrade_licence.inc.php';
  }elseif (isset($_GET['udt'])) {
    include 'upgrade_licence_thank_you.inc.php';
  }
}

if ($UIDv <> 1) {
  // for users NOT ABLE to change information
  // if ($caUIDp <> 1) {
  if (isset($_GET['uss'])) {
    include 'include/form_users_select_type.inc.php';
  }elseif (isset($_GET['usp']) || isset($_GET['usa']) || isset($_GET['usu'])) {
    // echo "Go to company users page";
    include 'include/form_users.inc.php';
    if (isset($_GET['usc'])) {
      // echo "ADD a Users for the company";
      include 'include/form_new_company_user.inc.php';
    }
  }elseif (isset($_GET['ume'])) {
    echo "Go to Users own page";
  }elseif (isset($_GET['usr'])) {
    // echo "ADD a Users for the company";
    include 'include/form_user_details.inc.php';
    if (isset($_GET['usb'])) {
      // echo "EDIT a Users for the company";
      include 'include/edit_user_details.inc.php';

    }
  }elseif (isset($_GET['cou'])) {
    // echo "EDIT a Users for the company";
    include 'include/form_email_us.inc.php';
    // include 'include/form_new_company_user.inc.php';
  }elseif (isset($_GET['cot'])) {
    // echo "EDIT a Users for the company";
    include 'include/form_email_us_thanks.inc.php';
    // include 'include/form_new_company_user.inc.php';
  }elseif (isset($_GET['cos'])) {
    include 'include/report_company_summary.inc.php';
  }elseif (isset($_GET['scr'])) {
    include 'report_division.inc.php';
  }elseif (isset($_GET['sde'])) {
    include 'report_division_select.inc.php';
    include 'report_section.inc.php';
    include 'form_new_section.inc.php';
  }elseif (isset($_GET['sdn'])) {
    include 'include/report_division.inc.php';
  }elseif (isset($_GET['sds'])) {
    include 'include/report_division.inc.php';
    include 'include/form_new_division.inc.php';
    include 'include/form_select_division_action.inc.php';
  }elseif (isset($_GET['sdr'])) {
  }elseif (isset($_GET['sor'])) {
    echo "Section Orders";
    // include 'report_division.inc.php';
  }elseif (isset($_GET['dir'])) {
    include 'company_report.inc.php';
  }elseif (isset($_GET['spq'])) {
    echo "Above the line";
    include 'form_set_pwd_questions.inc.php';
    if (isset($_GET['dif']) || isset($_GET['ans'])) {
      include 'form_set_pwd_dif_questions.inc.php';
    }
  }
}else {
  // for users able to change information
  if (isset($_GET['cos'])) {
    include 'include/report_company_summary.inc.php';
  }elseif (isset($_GET['cot'])) {
    // echo "EDIT a Users for the company";
    include 'include/form_email_us_thanks.inc.php';
    // include 'include/form_new_company_user.inc.php';
  }elseif (isset($_GET['cou'])) {
    // echo "EDIT a Users for the company";
    include 'include/form_email_us.inc.php';
    // include 'include/form_new_company_user.inc.php';
  }elseif (isset($_GET['scr'])) {
    include 'report_division.inc.php';
  }elseif (isset($_GET['sde'])) {
    include 'report_division_select.inc.php';
    include 'report_section.inc.php';
    include 'form_new_section.inc.php';
  }elseif (isset($_GET['sdn'])) {
    include 'include/report_division.inc.php';
  }elseif (isset($_GET['sds'])) {
    // include 'include/form_select_division_action.inc.php';
    include 'include/report_division.inc.php';
    include 'include/form_new_division.inc.php';
  }elseif (isset($_GET['sdr'])) {
  }elseif (isset($_GET['ulu'])) {
    // echo "EDIT a Users for the company";
    include 'include/unlock_user.inc.php';
  }elseif (isset($_GET['usp']) || isset($_GET['usa']) || isset($_GET['usu'])) {
    // echo "Go to company users page";
    include 'include/form_users.inc.php';
    if (isset($_GET['usc'])) {
      // echo "ADD a Users for the company";
      include 'include/form_new_company_user.inc.php';
    }
  }elseif (isset($_GET['ulk']) || isset($_GET['sus']) || isset($_GET['rev'])) {
    if (isset($_GET['sus'])) {
      include 'include/form_user_amendment.inc.php';
    }elseif (isset($_GET['rev'])) {
      // echo "Re-activate the users account";
      include 'include/form_user_amendment.inc.php';
      // include 'include/form_users.inc.php';
      // code...
    }
    include 'include/form_users.inc.php';
  }elseif (isset($_GET['usr'])) {
    // echo "ADD a Users for the company145";
    include 'include/form_user_details.inc.php';
    if (isset($_GET['usb'])) {
      // echo "EDIT a Users for the company150";
      include 'include/edit_user_details.inc.php';
    }elseif (isset($_GET['rpw'])) {
      // echo "Re-set Password";
      include 'form_pwd_reset.inc.php';
      if (isset($_GET['fp'])) {
        // echo "Change the question and answer";
        include 'form_reset_forgotten_pwd.inc.php';
      }
    }elseif (isset($_GET['log'])) {
      include 'include/from_UID_get_user_login_records.inc.php';
    }
  }elseif (isset($_GET['uss'])) {
    include 'include/form_users_select_type.inc.php';
  }elseif (isset($_GET['stt'])) {
    // echo "Go to company users page";
    include 'include/company_SETTINGS.inc.php';
  }elseif (isset($_GET['spq'])) {
    echo "Below the line";
    include 'form_set_pwd_questions.inc.php';
    if (isset($_GET['dif']) || isset($_GET['ans'])) {
      include 'form_set_pwd_dif_questions.inc.php';
    }
  }
  // echo "<br><br>USER NOT ADMIN USER";
  // include 'form_user_details.inc.php';
}
