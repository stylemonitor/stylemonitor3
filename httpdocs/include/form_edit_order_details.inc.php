<?php
include 'dbconnect.inc.php';
echo "form_edit_order_details.inc.php";

$OID = $_POST['OID'];
$urlPage = $_POST['url'];

if (isset($_GET['tab'])) {
  $tab = $_GET['tab'];
}else {
  $tab = 9;
}

include 'from_OID_get_order_details.inc.php';

$cliDet = 'from_CID_select_associate_company_sales.inc.php';
$supDet = 'from_CID_select_associate_company_purchases.inc.php';

if (isset($_GET['o'])) {
  $OID = $_GET['o'];
}

if ($OTID == 1) {
  $hc = "f7cc6c";
  $ordType = 'Client';
  $ordTypeEdit = 'Update Client Name';
  $cliDet = $cliDet;
}elseif ($OTID == 2) {
  $hc = "bbbbff";
  $ordType = 'Supplier';
  $ordTypeEdit = 'Update Supplier Name';
  $cliDet = $supDet;
}elseif ($OTID == 3) {
  $hc = "c9d1ce";
  $ordType = 'Client';
  $ordTypeEdit = 'Update Client Name';
  $cliDet = $cliDet;
}elseif ($OTID == 4) {
  $hc = "c9d1ce";
  $ordType = 'Supplier';
  $ordTypeEdit = 'Update Supplier Name';
  $cliDet = $supDet;
}

// echo "GENERAL ORDER INFORMATION";
// echo "<br>Order ID : $OID :: Order number : $oref";
// echo "<br>Order due date : $dueDate";
// echo "<br>Order type ID = $OTID";
//
// echo "<br>CLIENT INFORMATION";
// echo "<br>Associate company ID : $ACIDC :: Associate company name : $ACIDnC";
//
// echo "<br>SUPPLIER INFORMATION";
// echo "<br>Associate company ID : $ACIDS :: Associate company name : $ACIDnS";
//
// echo "<br>Our order Reference : $OIDcor";
// echo "<br>Their order Reference : $OIDtor";

if ($OTID == 4) {
  ?>
  <div style="position: absolute; top:0%; height:100%; left:0%; width:100%; background-blend-mode:overlay; z-index:1;"></div>
  <div  style="position: absolute; top:25%; height:50%; left:20%; width:60%; background-color: <?php echo $edtCol ?>; border:medium ridge grey; border-radius: 30px; z-index:1;"></div>
  <!-- <div style="position:absolute; top:30%; height:40%; left:20%; width:60%; background-color:pink;"></div> -->
  <div style="position:absolute; top:26%; height:4.5%; left:20%; width:60%; background-color:<?php echo $hc ?>; font-size:150%; text-align:center; z-index:1;">Edit Our Item Reference Information</div>

  <div style="position:absolute; top:55%; height:3%; left:21%; width:28.5%; font-weight: bold; font-size:100%; text-align:left; z-index:1;">Our Item Reference</div>
  <div style="position:absolute; top:58.5%; height:3%; left:21.5%; width:27.5%; font-size:100%; text-align:left; z-index:1;"><?php echo $DIDnS ?></div>
  <div style="position:absolute; top:62%; height:3%; left:21%; width:28.5%; font-weight: bold; font-size:100%; text-align:left; z-index:1;">Our Reference</div>
  <div style="position:absolute; top:65.5%; height:3%; left:21.5%; width:27.5%; font-size:100%; text-align:left; z-index:1;"><?php echo $OIDcor ?></div>


  <form class="" action="include/form_edit_order_details_act.inc.php" method="post">
    <input type="text" name="OID" value="<?php echo $OID ?>">
    <input type="text" name="urlPage" value="<?php echo $urlPage ?>">
    <input type="text" name="tab" value="<?php echo $tab ?>">
    <input type="text" name="fACID" value="<?php echo $fACID ?>">
    <!-- <select style="position:absolute; top:35%; left:50.5%; width:28.2%; background-color:<?php echo $ddmCol ?>; font-size:100%; text-align:left; z-index:1;" name="fACID">
      <option value="">
        <?php echo $ordTypeEdit ?>
      </option>
      <?php include $cliDet; ?>
    </select> -->
    <!-- OUR Division -->
    <select style="position:absolute; top:58%; left:50.5%; width:27.5%; background-color:<?php echo $ddmCol ?>; font-size:100%; text-align:left; z-index:1;" name="fDID">
    <option value="">
    Select Our Item Reference
    </option>
    <?php include $cliDet; ?>
    </select>

    <input type="text" style="position:absolute; top:48%; height:3%; left:50.5%; width:27.5%; background-color:<?php echo $optCol ?>; font-size:100%; text-align:left; z-index:1;" name="tnewRef" value="" placeholder="Revised THEIR Reference">
    <input type="text" style="position:absolute; top:64%; height:3%; left:50.5%; width:27.5%; background-color:<?php echo $optCol ?>; font-size:100%; text-align:left; z-index:1;" name="fnewRef" value="" placeholder="Revised OUR Reference">

    <button class="entbtn" type="submit" style="position:absolute; top:70%; left:37.5%; width:10%; background-color:<?php echo $fsvCol ?>; z-index:1;" name="save">SAVE</button>
    <button class="entbtn" type="submit" style="position:absolute; top:70%; left:52.5%; width:10%; background-color:<?php echo $fcnCol ?>; z-index:1;" name="cancel">CANCEL</button>

  </form>
  <?php
}else {
  ?>
  <div style="position: absolute; top:0%; height:100%; left:0%; width:100%; background-blend-mode:overlay; z-index:1;"></div>

  <div  style="position: absolute; top:25%; height:50%; left:20%; width:60%;background-color: <?php echo $edtCol ?>; border:medium ridge grey; border-radius: 30px; z-index:1;"></div>
  <!-- <div style="position:absolute; top:30%; height:40%; left:20%; width:60%; background-color:pink;"></div> -->
  <div style="position:absolute; top:26%; height:4.5%; left:20%; width:60%; background-color:<?php echo $hc ?>; font-size:150%; text-align:center; z-index:1;">Edit / Update Basic Order Information</div>

  <!-- Left side CLIENT -->
  <div style="position:absolute; top:32%; height:3%; left:21%; width:28.5%; font-weight: bold; font-size:100%; text-align:left; z-index:1;"><?php echo $ordType ?></div>
  <div style="position:absolute; top:35.5%; height:3%; left:21.5%; width:27.5%; font-size:100%; text-align:left; z-index:1;"><?php echo "$ACIDnC ($ACIDC)" ?></div>
  <!-- <div style="position:absolute; top:39%; height:3%; left:21%; width:28.5%; font-weight:bold; font-size:100%; text-align:left; z-index:1;">Division (if known)</div>
  <div style="position:absolute; top:42.5%; height:3%; left:21.5%; width:27.5%; font-size:100%; text-align:left; z-index:1;"><?php echo "$DIDnC" ?></div> -->
  <div style="position:absolute; top:46%; height:3%; left:21%; width:28.5%; font-weight: bold; font-size:100%; text-align:left; z-index:1;">Their Reference</div>
  <div style="position:absolute; top:49.5%; height:3%; left:21.5%; width:27.5%; font-size:100%; text-align:left; z-index:1;"><?php echo $OIDtor ?></div>
  <div style="position:absolute; top:54%; left:20.5%; width:60%; border-top:thin solid grey; z-index:1;"></div>
  <!-- <div style="position:absolute; top:55%; height:3%; left:21%; width:28.5%; font-weight: bold; font-size:100%; text-align:left; z-index:1;">Our Division</div>
  <div style="position:absolute; top:58.5%; height:3%; left:21.5%; width:27.5%; font-size:100%; text-align:left; z-index:1;"><?php echo $DIDnS ?></div> -->
  <div style="position:absolute; top:62%; height:3%; left:21%; width:28.5%; font-weight: bold; font-size:100%; text-align:left; z-index:1;">Our Reference</div>
  <div style="position:absolute; top:65.5%; height:3%; left:21.5%; width:27.5%; font-size:100%; text-align:left; z-index:1;"><?php echo $OIDcor ?></div>


  <form class="" action="include/form_edit_order_details_act.inc.php" method="post">
    <input type="hidden" name="OID" value="<?php echo $OID ?>">
    <input type="hidden" name="urlPage" value="<?php echo $urlPage ?>">
    <input type="hidden" name="tab" value="<?php echo $tab ?>">
    <select style="position:absolute; top:35%; left:50.5%; width:28.2%; background-color:<?php echo $ddmCol ?>; font-size:100%; text-align:left; z-index:1;" name="fACID">
      <option value="">
        <?php echo $ordTypeEdit ?>
      </option>
      <?php include $cliDet; ?>
    </select>
    <!-- CLIENT / SUPPLIER division -->
    <!-- <select style="position:absolute; top:42%; left:50.5%; width:27.5%; background-color:<?php echo $ddmCol ?>; font-size:100%; text-align:left; z-index:1;" name="fDID">
      <option value="">
        New CLIENT Division Name
      </option>
      <?php include $cliDet; ?>
    </select> -->

    <!-- OUR Division -->
    <!-- <select style="position:absolute; top:58%; left:50.5%; width:27.5%; background-color:<?php echo $ddmCol ?>; font-size:100%; text-align:left; z-index:1;" name="fDID">
    <option value="">
    New CLIENT Division Name
    </option>
    <?php include $cliDet; ?>
    </select> -->

    <input type="text" style="position:absolute; top:48%; height:3%; left:50.5%; width:27.5%; background-color:<?php echo $optCol ?>; font-size:100%; text-align:left; z-index:1;" name="tnewRef" value="" placeholder="Revised THEIR Reference">
    <input type="text" style="position:absolute; top:64%; height:3%; left:50.5%; width:27.5%; background-color:<?php echo $optCol ?>; font-size:100%; text-align:left; z-index:1;" name="fnewRef" value="" placeholder="Revised OUR Reference">

    <button class="entbtn" type="submit" style="position:absolute; top:70%; left:37.5%; width:10%; background-color:<?php echo $upRecCol ?>; z-index:1;" name="save">Update</button>
    <button class="entbtn" type="submit" style="position:absolute; top:70%; left:52.5%; width:10%; background-color:<?php echo $fcnCol ?>; z-index:1;" name="cancel">CANCEL</button>

  </form>
  <?php
}
