<?php
session_start();
include 'include/dbconnect.inc.php';
// echo "<br><b>include/input_additional_associate_address.inc.php</b>";

$UID  = $_SESSION['UID'];
$DID  = $_SESSION['DID'];

if (isset($_GET['id'])) {
  $ACID = $_GET['id'];
  $ct = $_GET['ct'];
}

include 'from_ACID_get_associate_company_details.inc.php';

$td = date('U');
$td = date('d M Y',$td);

?>

<br><br><br><br>
<br>
<div style="position:absolute; top:16%; height:70%; left:15%; width:70%; background-color:#f1f1f1; font-size:150%; border: medium solid grey; border-radius:10px;">
  <h1>Additional <?php echo $ACIDn ?> address</h1>
</div>
<form action="include/input_additional_associate_address_act.inc.php" method="POST">

  <!-- <input type="text" name="CID" value="<?php echo "$CID"; ?>"> -->
  <input type="hidden" name="ACID" value="<?php echo "$ACID"; ?>">
  <input type="hidden" name="ct" value="<?php echo "$ct"; ?>">
  <input type="hidden" name="AID" value="<?php echo "$AID"; ?>">

  <div style="position:absolute; top:24%; left:20.5%; width:10%; border:none; background-color: #f1f1f1; text-align:right">Name</div>
  <div style="position:absolute; top:30%; left:20.5%; width:10%; border:none; background-color: #f1f1f1; text-align:right">Address 1</div>
  <div style="position:absolute; top:36%; left:20.5%; width:10%; border:none; background-color: #f1f1f1; text-align:right"></div>
  <div style="position:absolute; top:42%; left:20.5%; width:10%; border:none; background-color: #f1f1f1; text-align:right">Town/City
  </div>
  <div style="position:absolute; top:48%; left:20.5%; width:10%; border:none; background-color: #f1f1f1; text-align:right">Post/Zip code</div>
  <div style="position:absolute; top:54%; left:20.5%; width:10%; border:none; background-color: #f1f1f1; text-align:right">County/State</div>
  <div style="position:absolute; top:60%; left:20.5%; width:10%; border:none; background-color: #f1f1f1; text-align:right">Country</div>
  <div style="position:absolute; top:66%; left:20.5%; width:10%; border:none; background-color: #f1f1f1; text-align:right">TimeZone</div>
  <div style="position:absolute; top:72%; left:20.5%; width:10%; border:none; background-color: #f1f1f1; text-align:right">Tel</div>
  <!-- <divstyle="position:absolute; top:82%; left:20.5%; width:10%; border:none; background-color: #f1f1f1; text-align:right" name="AIDc" value="<?php echo "$AIDc"; ?></div> -->

  <input autofocus required class="edit_user" type="text" style="position:absolute; top:23%; height:4%; left:32%;" name="addn" placeholder="<?php echo "$addn"; ?>" value="">
  <input class="edit_user" type="text" style="position:absolute; top:29%; height:4%; left:32%;" name="add1" placeholder="<?php echo "$add1"; ?>" value="">
  <input class="edit_user" type="text" style="position:absolute; top:35%; height:4%; left:32%;" name="add2" placeholder="<?php echo "$add2"; ?>" value="">
  <input class="edit_user" type="text" style="position:absolute; top:41%; height:4%; left:32%;" name="city" placeholder="<?php echo "$city"; ?>" value="">
  <input class="edit_user" type="text" style="position:absolute; top:47%; height:4%; left:32%;" name="pcode" placeholder="<?php echo "$pcode"; ?>" value="">
  <input class="edit_user" type="text" style="position:absolute; top:53%; height:4%; left:32%;" name="county" placeholder="<?php echo "$county"; ?>" value="">

  <!-- Dropdown dependent menu START -->
    <select id="CYID" name="CYID" onchange="FetchTimeZone(this.value)" style="position:absolute; top:59%; height:4%; left:32%; width:40.8%; background-color:<?php echo $ddmCol ?>;">
      <option value="">Select Country</option>
      <?php
      include 'include/select_company_country_details.inc.php';
      ?>
    </select>
    <script type="text/javascript">
    function FetchTimeZone(id){
      $('#TZID').html('');
      // alert(id);
      // return false;
      $.ajax({
        type : 'POST',
        url : 'include/select_country_timezone.inc.php',
        data : { CYID:id},
        success : function(data){
          $('#TZID').html(data);
        }
      })
    }
    </script>
    <select required id="TZID" name="TZID" style="position:absolute; top:65%; height:4%; left:32%; width:40.8%; background-color:<?php echo $ddmCol ?>">
      <option value="">Select TimeZone</option>
      <?php
      include 'include/select_country_timezone.inc.php';
      ?>
    </select>
  <!-- Dropdown dependent menu END -->

  <input class="edit_user" type="text" style="position:absolute; top:71%; height:4%; left:32%;" name="tel" placeholder="<?php echo "$tel"; ?>" value="">

  <!-- <select required class ="edit_user" style="position:absolute; top:84%; height: 10%; font-size: 120%; text-align: center;" name="country">
    <?php
    include 'select_company_country_details.php';
    ?>
  </select> -->

  <button class="entbtn" style="position:absolute; top:79%; left:35%; width:10%;" type="submit" name="addADDRESS">Add</button>
  <button formnovalidate class="entbtn" style="position:absolute; top:79%; left:55%; width:10%;" type="submit" name="cancel">Cancel</button>
</form>
