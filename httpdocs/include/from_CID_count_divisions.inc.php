<?php
include 'include/dbconnect.inc.php';
// echo "include/from_CID_count_divisions.inc.php";
// include 'from_CID_get_min_division.inc.php';
$CID = $_SESSION['CID'];

$sql = "SELECT COUNT(d.ID) AS cDID
        FROM associate_companies ac
          , company c
          , division d
        WHERE c.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND d.name NOT IN ('Select division')
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fcgocc</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cDID = $row['cDID'];
  global $cDID;
}
