<?php
// echo "form_edit_item_cancel.inc.php";
?>
<div class="overlay" style="z-index:2;"></div>

<div style="position:absolute; top:35%; height:18%; left:30%; width:40%; background-color: pink; border: 2px solid grey; border-radius: 5px; z-index:3;">
  <b style="font-size:120%;">Do you really want to cancel this item?</b>
</div>

<form action="include/form_edit_item_cancel_act.inc.php" method="post">
  <input type="hidden" name="OID" value="<?php echo $OID ?>">
  <input type="text" name="OIID" value="<?php echo $OIID ?>">
  <input type="text" name="OPID" value="<?php echo $OPID ?>">
  <input type="hidden" name="OTID" value="<?php echo $OTID ?>">
  <textarea autofocus required style="position:absolute; top:40%; left: 31%; width:38%; z-index:3;" name="canReas" value="" placeholder="Please say why the item is being cancelled"></textarea>

  <button class="entbtn" type="submit" style="position:absolute; top:48%; left:37.5%; width:10%; background-color:<?php echo $fsvCol ?>; z-index:3;" name="YesBtn">YES</button>
  <button formnovalidate class="entbtn" type="submit" style="position:absolute; top:48%; left:52.5%; width:10%; background-color:<?php echo $fsvCol ?>; z-index:3;" name="NoBtn">No</button>
</form>
