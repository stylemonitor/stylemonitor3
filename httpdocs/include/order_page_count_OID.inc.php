<?php
include 'dbconnect.inc.php';

// echo "order_page_count_OID.inc.php";

$sql = "SELECT COUNT(o.ID) as OID
        FROM orders o
          , company c
          , associate_companies ac
          , division d
          , order_item oi
          , order_item_del_date oidd
        WHERE o.CPID IN(SELECT cp.ID
            						FROM company_partnerships cp
            						WHERE cp.req_CID = ? OR cp.acc_CID = ?)
        AND o.orComp = ?
        AND o.ID = ?
        AND ac.CID = c.ID
        AND d.ACID = ac.ID
        AND o.tDID = d.ID
        AND oi.OID = o.ID
        AND oidd.OIID = oi.ID
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-opcO</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss", $CID, $CID, $OC, $OID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $cOID = $row['OID'];
}

// NOT REQUIRED AS DATES NOT APPLICABLE TO PAGE COUNT ON ORDER ID
// AND oidd.item_del_date BETWEEN ? AND ?
// mysqli_stmt_bind_param($stmt, "sssss", $CID, $CID, $OC, $sd, $ed);
