<?php
include 'dbconnect.inc.php';
// echo "<br><b>form_edit_order_item_details.inc.php</b>";
include 'SM_colours.inc.php';

$CID = $_SESSION['CID'];

if (isset($_GET['OI'])) { $OIID = $_GET['OI'];}

if (isset($_GET['o'])) { $OID = $_GET['o'];}
// echo "OTID1::$OTID";

include 'from_OIID_get_DISTINCT_order_details.inc.php';
include 'action/Count_OIC_from_OIID_delDate.act.php';
include 'action/Count_OIC_from_OIID_qty.act.php';

// include 'action/select_cliCID_from_OIID.act.php';
// include 'whats_being_made_per_order_item.sql.php';

// if ($cliCID = $CID) {
//   $OTID = $OTID;
// }elseif ($cliCID <> $CID) {
//   $OTID = 3;
// }

// echo "OTID2::$OTID";


// $oOIIDd = $OIIDd;
// $OIIDd = date('d-M-Y', $OIIDd);

if ($samProd == 1) {
  $spType = "Sample";
}elseif ($samProd == 2) {
  $spType = "Production";
}

?>
<div class="overlay"></div>
<!-- lines across the page -->
<div style="position:absolute; top:31.5%; height:5%; left:5.25%; width:90.1%; border-top:1px solid grey; background-color: pink; z-index:2;"></div>

  <div style="position:absolute; top:32.5%; left:10%; width:14%; padding-left:0.5%; font-size:120%; font-weight:bold; text-align:left; border-radius: 2px; z-index:2;"><a class="hreflink" href="home.php?H&rt3&o=<?php echo $OID ?>&OI=<?php echo $OIID ?>&eit=1">Cancel Item</a></div>

  <div style="position:absolute; top:32.5%; left:26%; width:23%; padding-left:0.5%; font-weight: bold; font-size:120%; text-align:left; border-radius: 2px;z-index:2;"><a class="hreflink" href="home.php?H&rt3&o=<?php echo $OID ?>&OI=<?php echo $OIID ?>&eit=2"> Change <?php echo $spType ?> type</a></div>
  <?php
  if ($cOICIDq == 0) {
    ?>
    <div style="position:absolute; top:32.5%; left:50%; width:25%; font-size:120%; font-weight: bold; text-align:left; z-index:2;"><a class="hreflink" href="home.php?H&rt3&o=<?php echo $OID ?>&OI=<?php echo $OIID ?>&eit=3">Change Item Quantity</a></div>
    <?php
  }else {
    ?>
    <div style="position:absolute; top:32.5%; left:50%; width:25%; font-size:120%; font-weight: bold; text-align:left; z-index:2;">Change Requested</div>
    <?php
  }

  if ($cOICIDd == 0) {
    ?>
    <div style="position:absolute; top:32.5%; left:75%; width:19%; font-size:120%; font-weight: bold; text-align:left; z-index:2;"><a class="hreflink" href="home.php?H&rt3&o=<?php echo $OID ?>&OI=<?php echo $OIID ?>&eit=4">Revised Due Date</a></div>
    <?php
  }else {
    ?>
    <div style="position:absolute; top:32.5%; left:75%; width:19%; font-size:120%; font-weight: bold; text-align:left; z-index:2;">Change Requested</div>
    <?php
  }
  ?>

<div style="position:absolute; top:36.5%; left:5.2%; width:90.3%; border-top:1px solid grey; z-index:2;"></div>
<?php

if ($OIIDComp == 2) {
  // echo "You cannot edit a cancelled order";
  // include 'include/form_edit_order_item_details_can.inc.php';


  $sql = "SELECT opm.type as OPMIDnote
          FROM order_placed_move opm
          WHERE opm.OPID = ?
          AND opm.OIMRID = 41 ;
  ";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-frca</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $OPID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($result);
    $note = $row['OPMIDnote'];
  }

  $sql = "UPDATE order_item
          SET itComp = 2
          WHERE id = ?;
  ";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo '<b>FAIL-frca</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $OIID);
    mysqli_stmt_execute($stmt);
  }

  ?>
  <div class="overlay" style="position:absolute; top:45%; height:15%; left:20%; width:60%; border: 2px ridge black; border-radius:10px; background-color:white; padding-top:1%; font-size:150%; font-style:bold; z-index:1;">
      This item has been CANCELLED from the order
      <br><?php echo $note ?>
  </div>
  <form action="include/form_edit_order_item_details_cancelled_act.inc.php" method="post">
    <input type="hidden" name="OID" value="<?php echo $OID ?>">
    <button class="entbtn" type="submit" style="position:absolute; top:57%; height:4%; left:45%; width:10%; z-index:1;"name="close">Close</button>
  </form>
  <?php
  // header("Location:../styles.php?S&cor");
  // exit();
}else {

  if ($OIIDComp == 0) {
    $bc = $edtCol;
  }elseif ($OIIDComp == 1) {
    $bc = "white";
  }

  if ($OTID == 1) {
    $Tref = "Clients";
    $Tref2 = "SALES";
  }elseif ($OTID == 2) {
    $Tref = "Suppliers";
    $Tref2 = "PURCHASE";
  }elseif (($OTID == 3) || $OTID == 4) {
    $Tref = "PARTNERS";
    $Tref2 = "PARTNER";
  }

  // if a PARTNERSHIP
  // Select if you are the Supplier
  // form_edit_order_item_details.inc.php needs to also include
  // BOTH tCID and FCID values so that the SUPPLIER detail can be isolated
  ?>
  <div style="position:absolute; top:0%; height:100%; left:0%; width:100%; background-blend-mode:overlay;"></div>

  <div  style="position: absolute; top:25%; height:40%; left:5%; width:90%; background-color: <?php echo $bc ?>; border:3px ridge grey; border-radius: 20px; z-index:1;"></div>
  <div style="position: absolute; top:26%; left:5%; width:90%; font-size: 200%; font-weight: bold; text-align:center; z-index:1;">
    Revised <?php echo $Tref2 ?> Order <?php echo $oref ?> Item <?php echo $OIIDnos ?>
  </div>

  <!-- <div style="position:absolute; top:42.5%; left:5.2%; width:90.3%; border-top:1px solid grey; z-index:1;"></div> -->
  <div style="position:absolute; top:42%; left:5.2%; width:90.1%; border-top:2px solid black; z-index:1;"></div>

  <div style="position:absolute; top:37.5%; left:55%; width:5%; font-weight: bold; font-size:120%; text-align:right; z-index:1;"><?php echo $OIQIDq ?></div>

  <div style="position:absolute; top:37.5%; left:77%; width:12%; font-size:120%; font-weight: bold; text-align:center; z-index:1;"><?php echo $OIDDIDd ?></div>

  <div style="position:absolute; top:43%; left:6%; width:30%; font-size:120%; text-align:left; z-index:1;">Our Item Description</div>
  <div style="position:absolute; top:43%; left: 36.5%; width:30%; font-size:120%; text-align:left; z-index:1;">Our Product Type</div>
  <div style="position:absolute; top:43%; left:67%; width:20%; font-size:120%; text-align:left; z-index:1;">Our Style Name</div>

  <!-- Partners details associate OR PARTNER -->
  <div style="position:absolute; top:51.5%; left:6%; width:30%; font-size:120%; text-align:left; z-index:1;"><?php echo $Tref ?> Item Description</div>
  <div style="position:absolute; top:51.5%; left: 36.5%; width:30%; font-size:120%; text-align:left; z-index:1;"><?php echo $Tref ?> Product Type</div>
  <div style="position:absolute; top:51.5%; left:67%; width:20%; font-size:120%; text-align:left; z-index:1;"><?php echo $Tref ?> Style Name</div>



  <form action="include/form_edit_order_item_details_act.inc.php" method="POST">
    <input type="hidden" name="OTID" value="<?php echo $OTID ?>">
    <input type="hidden" name="OID" value="<?php echo $OID ?>">
    <input type="hidden" name="OIID" value="<?php echo $OIID ?>">
    <input type="hidden" name="urlPage" value="<?php echo $urlPage ?>">
    <input type="hidden" name="OPID" value="<?php echo $OPID ?>">

    <?php
    if (($OIIDComp == 1) && $OIIDComp == 2) {
      ?>
      <input type="hidden" name="canOrdItem" value="0">
      <?php
    }elseif ($OIIDComp == 0) {
    }
    ?>

    <!-- THEIR references -->
    <?php
    if (($OTID == 1) || ($OTID == 2) || ($OTID == 4)) {
      ?>
      <input type="text" style="position:absolute; top:47%; left:6%; width:29%; background-color:<?php echo $optCol ?>; font-size:100%; text-align:left; z-index:1;" name="NitemRef" value="" placeholder="<?php echo $OIIDoir ?>">
      <div style="position:absolute; top:47%; left: 36.5%; width:30%; font-size:120%; font-weight: bold; text-align:left; z-index:1;"><?php echo $PTIDn ?></div>

      <!-- OUR references -->
      <?php
      // count how many PRID's they have if 1 cannot change
      // if > 1 dropdown menu
      include 'from_CID_count_styles.inc.php';
      if ($cPRID == 1) {
        // show that one style
        ?>
        <input type="hidden" name="NStyleRef" value="<?php echo "NstyleRef" ?>">
        <div style="position:absolute; top:47%; left:67%; width:29%; font-size:100%; font-weight: bold; text-align:left; z-index:1;"><?php echo $PRIDn ?></div>
        <?php
      }else {
        ?>
        <input type="hidden" name="NStyleRef" value="<?php echo "NstyleRef" ?>">
        <select style="position:absolute; top:47%; height:4%; left:67%; width:27.5%; background-color:<?php echo $ddmCol ?>;  font-size:100%; text-align:left; z-index:1;" name="nPRID" >
          <option value=""><?php echo "$PRIDn (change)" ?></option>
          <!-- <option value="">Revised Style (if applicable)</option> -->
          <?php
          include 'from_PTID_select_product_ref.inc.php';
          ?>
        </select>
        <?php
      }
      // count how many PTID's they have
      // if > 1 dropdown menu
      if (($OTID == 1) || ($OTID == 2)) {
        // echo "OTID = 1 or 2";

        // if the CLIENT/SUPPLIER has given us their ITEM DESCRIPTION then use that
        if (!empty($pIRef)) {
          $pItemRef = $pIRef;
        }else {
          $pItemRef = $pItemRef;
        }

        // if the CLIENT/SUPPLIER has given us their PRODUCT TYPE then use that
        if (!empty($pPType)) {
          $pPTIDn = $pPType;
        }else {
          $pPTIDn = $pPTIDn;
        }

        // if the CLIENT/SUPPLIER has given us their STYLE NAME then use that
        $pStyle = "$PRIDr : $PRIDn";
        if (!empty($pSRef)) {
          $pStyle = $pSRef;
        }else {
          $pStyle = $pStyle;
        }

        ?>

        <input type="hidden" name="OIIDComp" value="<?php echo $OIIDComp ?>">
        <input type="hidden" name="oNpPRID" value="<?php echo $pItemRef ?>">
        <input type="hidden" name="oNpPTID" value="<?php echo $pPTIDn ?>">
        <input type="hidden" name="oNpPRIDs" value="<?php echo $pStyle ?>">

        <?php
        ?>
        <input style="position:absolute; top:55%; left:6%; width:29%; font-weight: bold; text-align:left; z-index:1;" name="pItemRef" placeholder="<?php echo $pItemRef ?>">
        <input style="position:absolute; top:55%; left: 36.5%; width:29%; font-weight: bold; text-align:left; z-index:1;" name="pPTIDn" placeholder="<?php echo $pPTIDn ?>">
        <input style="position:absolute; top:55%; left:67%; width:26.5%; font-weight: bold; text-align:left; z-index:1;" name="pStyle" placeholder="<?php echo $pStyle ?>">

        <?php
      }else {
        ?>
        <input type="hidden" name="NStyleRef" value="">
        <div style="position:absolute; top:55%; left:6%; width:29%; font-size:100%; font-weight: bold; text-align:left; z-index:1;"><?php echo $pItemRef ?></div>
        <!-- <div style="position:absolute; top:65%; left: 36.5%; width:29%; font-size:100%; font-weight: bold; text-align:left; z-index:1;">partners product type</div> -->
        <div style="position:absolute; top:55%; left:67%; width:29%; font-size:100%; text-align:left; z-index:1;"><?php echo $pSRef ?></div>
        <?php
      }
    }elseif(($OTID == 3)) {
      // echo "OTID = 3";
      $PRID = $pPRID;
      ?>
      <input type="text" style="position:absolute; top:47%; left:6%; width:29%; background-color:<?php echo $optCol ?>; font-size:100%; text-align:left; z-index:1;" name="NpitemRef" value="" placeholder="NpitemRef">

      <?php
      // if product type is set then it cannot be changed
      if ($PTID <> $pPTID) {
        ?>
        <!-- <div style="position:absolute; top:49%; left:1%; width:15%; text-align:left;">Product Type</div> -->
        <select id="tPTID" name="pPTID" onchange="FetchToStyle(this.value)" style="position:absolute; top:47%; height:3.8%; left: 36.5%; width:28%; background-color:<?php echo $optCol ?>; z-index:1;">
          <option>pPTID</option>
          <?php
          include 'include/from_CID_select_prod_type_dd.inc.php';
          ?>
        </select>
        <script type="text/javascript">
          function FetchToStyle(id){
            $('#fPRID').html;
            // alert(id);
            // return false;
            $.ajax({
              type : 'POST',
              url : 'include/from_pCID_select_TO_product_ref_dd.inc.php',
              data : {tPTID:id},
              success : function(data){
                $('#fPRID').html(data);
              }
            })
          }
        </script>
        <?php
      }else {
        ?>
        <div style="position:absolute; top:47%; height:3.8%; left: 36.5%; width:28%;font-weight: bold; text-align: left; z-index:1;">
          <?php echo $pPTIDn ?>
        </div>
        <?php
      }

      ?>

      <select required id="fPRID" name="pPRID" style="position:absolute; top:47%; height:3.8%; left:67%; width:28%; background- color:<?php echo $rqdCol ?>; z-index:1;">
        <option value="">pPRID</option>
        <?php
        include 'from_pCID_select_TO_product_ref.inc.php';
        ?>
      </select>
      <div style="position:absolute; top:55%; height:4%; left:6%; width:30%; font-size:100%; font-weight: bold; text-align:left; z-index:1;" name="NpPRID"><?php echo $itemRef ?></div>
      <div style="position:absolute; top:55%; left: 36.5%; width:30%; font-size:120%; font-weight: bold; text-align:left; z-index:1;"><?php echo $pPTIDn ?></div>
      <div style="position:absolute; top:55%; height:4%; left:67%; width:28%; font-size:100%; font-weight: bold; text-align:left; z-index:1;" name="NpPRID"><?php echo "$PRIDr : $PRIDd" ?></div>
      <?php
    }
    ?>

    <button class="entbtn" type="submit" style="position:absolute; top:60%; left:37.5%; width:10%; background-color:<?php echo $fsvCol ?>; z-index:1;" name="save">SAVE</button>
    <button formnovalidate class="entbtn" type="submit" style="position:absolute; top:60%; left:52.5%; width:10%; background-color:<?php echo $fcnCol ?>; z-index:1;" name="cancel">CANCEL</button>

  </form>
  <?php
}
?>
<!--  -->
