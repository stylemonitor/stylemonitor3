<?php
include 'dbconnect.inc.php';
// echo "<b>include/form_new_partner.inc.php</b>";

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'Partnership Creation';
$pddhbc = '#5199fc';
include 'page_description_date_header.inc.php';

if (!isset($_GET['se'])) { ?>

  <div class="">
    <div class="" style="position:absolute; top:16%; left:2%; width:96%; font-size:150%;">
      You need to know their SMI code
    </div>

    <form class="" action="include/form_new_partner_act.inc.php" method="post">
    <?php
    if (isset($_GET['s'])) {
      $SMIC = $_GET['s']; ?>
      <input autofocus required class="input_data" style="position:absolute; top:26%; left:30%; height:6%; width:40%; font-size: 150%; border-radius:10px; text-align:center;" type="text" name="pSMIC" placeholder="_ _ _ _ _ - _ _ _ _ _ _ - _ _" value="<?php echo $SMIC ?>">
    <?php }else { ?>
      <input autofocus required class="input_data" style="position:absolute; top:26%; left:30%; height:6%; width:40%; font-size: 150%;  border-radius:10px; text-align:center;" type="text" name="pSMIC" placeholder="_ _ _ _ _ - _ _ _ _ _ _ - _ _" value="">
    <?php } ?>
    <div style="position:absolute; top:45%; left:2%; width:96%; font-size:150%;">
      What is your relationship to them
    </div>
    <?php
    if (isset($_GET['c'])) {
        ?>
        <div style="position:absolute; top:55%; height:5%; left:20%; width:60%; font-size: 175%; font-weight: bold; text-align:center;" name="rel" value="3">
          They are our CLIENT we supply them
        </div>
        <?php
    }elseif (isset($_GET['s'])) {
      ?>
      <div style="position:absolute; top:55%; height:5%; left:20%; width:60%; font-size: 175%; font-weight: bold; text-align:center;" name="rel" value="4">
        They are our SUPPLIER we buy from them
      </div>
      <?php
    }else {
      ?>
      <select class="sel_dropdown" style="position:absolute; top:55%; height:5%; left:30%; width:40%; font-size: 150%; text-align:center;" name="rel">
        <option value="">Select Relationship</option>
        <?php
        include 'include/get_rel_type.inc.php';
        ?>
      </select>
      <?php
    }
    ?>

    <button class="entbtn" style="position:absolute; bottom:23%; height:6%; left:20%; width:28%;" type="submit" name="req_part">Request Partnership</button>
    <button formnovalidate class="entbtn" style="position:absolute; bottom:23%; height:6%; left:50%; width:28%;" type="submit" name="can_req_part">Cancel Partnership</button>

    <div class="" style="position:absolute; bottom:15%; left:2%; width:96%; color:RED; text-align:center; font-weight:bold; ">

      <?php
      if (isset($_GET['nt'])) {
        $nt = $_GET['nt'];

        // the printed response is
        if ($nt == 1) {
          $SMIC = $_GET['s'];
          echo'<br><br>We have no record of a company
              <br><br>with an SMIC of <b>'.$SMIC."</b>";
        }elseif ($nt == 2) {
          echo'<br>What is their relationship to you?';
        }elseif ($nt == 3 ) {
          echo'<br>The SMIC used is your own!!!';
        }elseif ($nt == 4) {
          echo'<br>They have been invited to be a partner but have yet to accept';
        }elseif ($nt == 5) {
          echo'<br>They have already accepted being a Partner';
        }elseif ($nt == 6) {
          echo'<br>You cannot make this request, contact your company StyleMonitor Administrator';
        }elseif ($nt == 7) {
          echo'<br>They have previously REJECTED a Partnership agreement';?>
          <button class="entbtn" style="position:absolute; bottom:20%; height:20%; left:20%; width:61%;" type="submit" name="req_part">Submit Partnership Request</button>
          <button formnovalidate class="entbtn" style="position:absolute; bottom:20%;; height:80%; left:40% width:61%;" type="submit" name="can_req_part">Cancel</button>
          <?php
        }
      }
      ?>
    </div>
  </form>
  </div>

<?php
}else{
  $se = $_GET['se'];
  echo '<br> An invitational email has been sent to '.$se;
  echo '<br><br><a href="mm_P.php?P&re">Invite another company<?a>';
}
?>
