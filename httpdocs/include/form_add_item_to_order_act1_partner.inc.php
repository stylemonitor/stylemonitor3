<?php
include 'dbconnect.inc.php';
echo "<br><b>form_add_item_to_order_act1_partner.inc.php</b>";

echo "<br>incItem = $incItem";

if (empty($tPRID)) {
  $tPRID = $fPRID;
}

// sets the lead time for an order
if (empty($sp)) {
  $sp = 2;
  $Ltime = $acLtime;
}else {
  $sp = 1;
  $Ltime = $acsLtime;
}

// echo "Item Delivery date = $delDate";
if (empty($IDD)) {
  $IDD = $td + ($Ltime * 86400);
}else {
  $IDD = $IDD;
}

// echo "delDate : $delDate";

if (empty($delDate)) {
  if (empty($IDD)) {
    $delDate = $td + ($Ltime * 86400);
  }else {
    $delDate = $IDD;
  }
}else {
  $delDate = $delDate;
}
// echo "Item Delivery date = $delDate";
// echo "Revised delivery date is :: $IDD";

// $IDD = strtotime($IDD,0);

if (empty($pPRID)) {
  $pPRID = $fPRID;
}else {
  $pPRID = $pPRID;
}

// what we have
// echo "<br>ACID = $ACID";
// echo "<br>Prod Lead time = $acLtime";
// echo "<br>Sample Lead time = $acsLtime";
// echo "<br>Order ID OID : $OID";
// echo "<br>Style ref PRID : $fPRID";
// echo "<br>Partners Style ref pPRID : $pPRID";
// echo "<br>Production (0) or Sample (1) : $sp";
// echo "<br>User ID UID : $UID";
// echo "<br>Order Item ID OIID : $OIID";
// echo "<br>Our reference oItemDes : $oItemDes";
// echo "<br>Their reference tItemDes : $tItemDes";
// echo "<br>Todays date td : $td";
// echo "<br>Section mSID : $mSID";
// echo "<br>QTY orQTY : $orQTY";
// echo "<br>Item del Date : $IDD";
//

// echo " $OID, $fPRID, $tPRID, $sp, $UID, $cOIID, $their_item_ref, $td";
// OID, PRID, pPRID, samProd, UID, ord_item_nos, their_item_ref, pItemRef, inputtime
// Add the data to the order_item table

// GET the order_item ID
// get the OPID
  $sql = "SELECT COUNT(ID) as OIID
          FROM order_item
          WHERE OID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-foia31';
  }else {
    mysqli_stmt_bind_param($stmt, "s", $OID);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($res);
    $cOIID = $row['OIID'];
  }

  $itemCount = $cOIID + 1;

// echo " $OID, $fPRID, $tPRID, $sp, $UID, $itemCount, $their_item_ref, $their_item_ref, $td";

  $sql = "INSERT INTO order_item
            (OID, PRID, pPRID, samProd, UID, ord_item_nos, their_item_ref, pItemRef, inputtime)
          VALUES
            (?,?,?,?,?,?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-foia1';
  }else {
    mysqli_stmt_bind_param($stmt, "sssssssss", $OID, $fPRID, $tPRID, $sp, $UID, $itemCount, $oItemDes, $tItemDes, $td);
        mysqli_stmt_execute($stmt);
  }

  // get the new OIID
  $sql = "SELECT ID as OIID
          FROM order_item
          WHERE OID = ?
          AND their_item_ref = ?
          AND inputtime = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-foia31';
  }else {
    mysqli_stmt_bind_param($stmt, "sss", $OID, $oItemDes, $td);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($res);
    $OIID = $row['OIID'];
  }

  // input the qty into the order_item_qty table
  $sql = "INSERT INTO order_item_qty
            (OIID, UID, order_qty, inputtime)
          VALUES
            (?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-foia5';
  }else {
    mysqli_stmt_bind_param($stmt, "ssss", $OIID, $UID, $orQTY, $td);
    mysqli_stmt_execute($stmt);
  }

  echo "<br> $OIID, $mSID, $UID, $td";

  // ADD the order item
  $sql = "INSERT INTO order_placed
            (OIID, SID, UID, inputtime)
          VALUES
            (?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo "<br>FAIL - foia";
  }else {
    mysqli_stmt_bind_param($stmt,"ssss", $OIID, $mSID, $UID, $td);
    mysqli_stmt_execute($stmt);
  }

  // get the OPID
  $sql = "SELECT MAX(op.ID) as xOPID
          FROM order_placed op
          WHERE UID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-foia3';
  }else {
    mysqli_stmt_bind_param($stmt, "s", $UID);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($res);
    $OPID = $row['xOPID'];
  }

// echo "Order Placed ID is :: $OPID";
// echo "<br>Check if it has worked";

  // add into the order_placed_move table
  $sql = "INSERT INTO order_placed_move
            (OPID, OIMRID, UID, omQty, inputtime)
          VALUES
            (?,?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-foia4';
  }else {
    mysqli_stmt_bind_param($stmt, "sssss", $OPID, $OIMRID, $UID, $orQTY, $td);
    mysqli_stmt_execute($stmt);
  }



  // input the del date into the order_item_del_date table
  $sql = "INSERT INTO order_item_del_date
            (OIID, UID, item_del_date, inputtime)
          VALUES
            (?,?,?,?)
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-foia6';
  }else {
    mysqli_stmt_bind_param($stmt, "ssss", $OIID, $UID, $delDate, $td);
    mysqli_stmt_execute($stmt);
  }

  // GET the order date
  // if the order date is LESS than the current item due date
  // change the order date to the latest item due date
  if ($IDD > $delDate) {
    // echo "Item date is greater than original delivery date";
    $sql = "UPDATE orders_due_dates
            SET del_Date = ?
            WHERE ID = ?
    ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<br>FAIL-fnoiea7';
    }else {
      mysqli_stmt_bind_param($stmt, "ss", $IDD, $OID);
      mysqli_stmt_execute($stmt);
    }
  }

if ($incItem == 1) {
  // get the OPID
  $sql = "SELECT MAX(ID) as OIID
          FROM order_item
          WHERE OID = ?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<br>FAIL-foia3';
  }else {
    mysqli_stmt_bind_param($stmt, "s", $OID);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_array($res);
    $OIID = $row['OIID'];
  }
  header("Location:../home.php?H&o=$OID&OI=$OIID");
  exit();
}elseif ($incItem != 1) {
  header("Location:../styles.php?S&orp=$OID");
  exit();
}
