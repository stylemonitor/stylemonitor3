<?php
// echo "<br><b>input_associate_company_division.inc.php</b>";

if (!empty($NEACID)) {
  $ACID = $NEACID;
}elseif (!empty($newACID)) {
  $ACID = $newACID;
}else {
  $ACID = $ACID;
}

$sql = "INSERT INTO division
          (ACID, UID, inputtime)
        VALUES
          (?,?,?)
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-ccs4</b>';
}else{
  mysqli_stmt_bind_param($stmt, "sss", $ACID, $UID, $td);
  mysqli_stmt_execute($stmt);
}

// Add a section
$sql = "SELECT ID as DID
        FROM division
        WHERE UID = ?
        AND inputtime = ?
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-frad7</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $UID, $td);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $DID = $row['DID'];
}
// echo "<br>DID :: $DID";
