<?php
include 'dbconnect.inc.php';
// echo "<br><b>form_edit_item_confirm.inc.php</b>";

if (isset($_GET['cty'])) {
  $Nqty = $_GET['cty'];
  $OIID = $_GET['OI'];
  $canReas = $_GET['r'];
  $eit = 3;
}
$td = date('U');

// echo "<br>Confirm the changes::cty=$Nqty::OIID=$OIID";

include 'from_OIID_get_order_details.inc.php';
// echo "<br>DelDate=$OIIDd";
$delDate = $OIIDd;


if ($Nqty < $oiQty) {
   // QUANTITY REDUCTION
   $OIMRID = 43;
   // $OIMRID = 10;
   // the qty is the amount that has to be reduced from the order
   // echo "oiQty=$oiQty::Nqty=$Nqty";
  $qty = $oiQty - $Nqty;
  $accReas = "Item quantity reduction of $qty ACCEPTED";
  $reaBoxLine = "You are requesting a reduction in quantity of <br><br><b>$qty from $oiQty down to $Nqty</b>";
  // echo "<br>Order Item Qty DECREASE - now confirm the change is what you want";

  // insert into order_placed_move table
  // include 'action/insert_into_order_place_move.act.php';

  // insert into order_item_change table
  // include 'action/insert_into_order_item_change_table.act.php';



  // go to confirmation of request
  // if the order is an ASSOCIATE ONE do the following
  if (($OTID == 1) OR ($OTID == 2)) {
    // echo "<br>ASSOCIATE CHANGE - pre-AGREED";
    // can automatically approve the reduction without the associate needing to take any action
    include 'action/insert_into_OPM.act.php';
    include 'action/select_OPMID.act.php';
    // echo "OPMID=$OPMID";
    $OPMID1 = $OPMID;
    include 'action/insert_into_order_item_change_table.act.php';
    include 'action/insert_into_OPM_assoc.act.php';
    include 'action/select_OPMID_assoc.act.php';
    // echo "::OPMID2=$OPMID";
    $status = 2;
    include 'action/select_OICID_from_OPMID_CANCEL.act.php';
    include 'action/update_order_item_change_table.act.php';

    header("Location:../home.php?h&rt2&o=$OID&OI=$OIID");
    exit();
  }else {
    echo "<br>PARTNER CHANGE";
    // need to send out confirmation request to the partner

    ?>
    <div class="overlay"></div>

    <div style="position:absolute; top:32%; height:30%; left:29%; width:42%; font-size: 120%; background-color: pink; border:1px solid black; border-radius:10px; z-index:3;">
       <?php echo $reaBoxLine ?>
       <!-- <br>from <?php echo $OIIDd  ?> to  <?php echo $Ndate ?> -->
       <!-- <br><br><?php echo $DateChange ?> -->
     </div>

     <div class="" style="position:absolute; top:46%; left:30%; width:40%; font-size: 120%; font-weight: bold; text-align: center; border-top:1px solid black; z-index:3;">
       <?php echo $canReas ?>
     </div>

    <form style="z-index:3;" action="include/form_edit_item_confirm_act.inc.php" method="post">
       <input type="hidden" name="eit" value="3">
       <input type="hidden" name="OID" value="<?php echo $OID ?>">
       <input type="hidden" name="OIID" value="<?php echo $OIID ?>">
       <input type="hidden" name="OTID" value="<?php echo $OTID ?>">
       <input type="hidden" name="OPID" value="<?php echo $OPID ?>">
       <input type="hidden" name="OIMRID" value="<?php echo $OIMRID ?>">
       <input type="hidden" name="qty" value="<?php echo $qty ?>">
       <input type="hidden" name="canReas" value="<?php echo $canReas ?>">
       <input type="hidden" name="oiQty" value="<?php echo $oiQty ?>">
       <input type="hidden" name="Nqty" value="<?php echo $Nqty ?>">
       <input type="hidden" name="eit" value="<?php echo $eit ?>">

       <button class="entbtn" type="submit" style="position:absolute; top:56%; left:32.5%; width:10%; background-color:<?php echo $fsvCol ?>; z-index:3;" name="YesBtn">Confirm777</button>
       <button class="entbtn" type="submit" style="position:absolute; top:56%; left:45%; width:10%; background-color:<?php echo $edtCol ?>; z-index:3;" name="chgBtn">Change</button>
       <button formnovalidate class="entbtn" type="submit" style="position:absolute; top:56%; left:57.5%; width:10%; background-color:<?php echo $fcnCol ?>; z-index:3;" name="NoBtn">Cancel</button>
     </form>
    <?php
  }
 // the item qty change must be added to the OID table etc for PARTNERSHIP
}elseif ($Nqty > $oiQty) {
   // QUANTITY INCREASE == NEW ITEM ON ORDER
  $qtyIncrease = $Nqty - $oiQty;
  $OIMRID = 48;
  $reaBoxLine = "You are requesting to add <br><br><b>$qtyIncrease</b> to the item";

  // $DateChange ="With a delivery date of <b>$Ndate</b><br>We will add an ITEM to the order";

   // a new item must be added to the order to add the extra quantity
  echo "Another Item needs to be added to the order";

   // add as an extra item on the order
}
