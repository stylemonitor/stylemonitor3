<?php
// echo "<b>incluDe/from_tCID_get_min_division.inc.php</b>";
// changed 15-9-20 to just ACID and not CID

$CID = $tCID;

$sql = "SELECT MIN(d.ID) as DID
        FROM division d
          , associate_companies ac
          , company c
          WHERE ac.ID = ?
        -- WHERE c.ID = ?
        -- AND ac.CID = c.ID
        AND d.ACID = ac.ID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcgmd</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $tACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $mtDID = $row['DID'];
}
