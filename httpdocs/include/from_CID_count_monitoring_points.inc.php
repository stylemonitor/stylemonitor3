<?php
include 'dbconnect.inc.php';
// echo "<b>include/from_CID_count_monitoring_points.inc.php</b>";
$CID = $_SESSION['CID'];

$sql ="SELECT mp.ID as cMPID
       FROM company c
        , associate_companies ac
        , division d
        , section s
        , monitoring_point mp
       WHERE c.ID = ?
       AND ac.CID = c.ID
       AND d.ACID = ac.ID
       AND s.DID = d.ID
       AND s.ID = mp.SID
       ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
 echo '<b>FAIL-fcgocc2</b>';
}else{
 mysqli_stmt_bind_param($stmt, "s", $CID);
 mysqli_stmt_execute($stmt);
 $result = mysqli_stmt_get_result($stmt);
 $row = mysqli_fetch_assoc($result);
 $cMPID = $row['cMPID'];
 global $cMPID;
}
