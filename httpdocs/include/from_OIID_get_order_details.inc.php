<?php
include 'dbconnect.inc.php';
// echo "<br><b>from_OIID_get_order_details.inc.php</b>";

// GET CLIENT ORDER DETAILS
$sql = "SELECT Mvemnt.*
       , Mvemnt.oiQtyBASE - SUM(Mvemnt.ORDER_DECR) AS oiQty
FROM
(SELECT DISTINCT o.ID AS OID
-- SQL is 'from_OIID_get_order_details.sql'
      , o.orNos AS oref
      , o.orDate AS orDate
      , odd.del_Date AS dueDate
      , oidd.item_del_date AS OIIDd
      , oiq.order_qty AS oiQtyBASE
      , o.fCID AS fCID
      , cac.ID AS ACIDC
      , cac.name AS ACIDnC
      , sac.ID AS ACIDS
      , sac.name AS ACIDnS
      , IF(cd.name = 'Select Division','Unknown',cd.name) AS DIDnC
      , IF(sd.name = 'Select Division','Unknown',sd.name) AS DIDnS
      , o.CPID AS CPID
      , oi.samProd AS samProd
      , CASE WHEN o.tCID = o.fCID
             THEN o.OTID
             ELSE CASE WHEN o.tCID = ? THEN 3
             ELSE 4
             END
        END AS OTID
      , o.our_order_ref OIDcor
      , IF(o.their_order_ref = '','Unknown',o.their_order_ref) OIDtor
      , oi.ID AS OIID
      , oi.ord_item_nos AS OIIDnos
      , oi.their_item_ref AS OIIDcr
      , oi.pItemRef AS OIIDsr
      , oi.itComp AS OIIDComp
      , oi.note AS NOTE
      , oiq.ID AS OIQID
      , oidd.item_del_date AS OIDD
      , opm.ID AS OPMID
      , op.ID AS OPID
      , pr.ID as PRID
      , pr.prod_ref AS prRef
      , pt.ID AS PTID
      , pt.name AS PTIDn
      , pr.prod_desc AS PRIDn
      , prp.ID AS pPRID
      , prp.prod_ref AS PprRef
      , ptp.ID AS pPTID
      , ptp.name AS pPTIDn
      , prp.prod_desc AS pPRIDn
      , IF((CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 10 THEN SUM(opm.omQty) END)) AS ORDER_DECR
FROM order_item oi
     INNER JOIN order_item oip ON oi.ID = oip.ID
     INNER JOIN orders o ON oi.OID = o.ID
     INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
     INNER JOIN orders_due_dates odd ON o.ID = odd.OID
     INNER JOIN order_item_del_date oidd ON oi.OID = oidd.OIID
     INNER JOIN order_placed op ON oi.ID = op.OIID
     INNER JOIN order_placed_move opm ON op.ID = opm.OPID
     INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
     INNER JOIN prod_ref pr ON pr.ID = oi.PRID
     INNER JOIN prod_ref prp ON prp.ID = oip.pPRID
     INNER JOIN product_type pt ON pt.ID = pr.PTID
     INNER JOIN product_type ptp ON ptp.ID = prp.PTID
     INNER JOIN division cd ON o.tDID = cd.ID
     INNER JOIN division sd ON o.fDID = sd.ID
     INNER JOIN associate_companies cac ON o.tACID = cac.ID
     INNER JOIN associate_companies sac ON o.fACID = sac.ID
WHERE oi.ID = ?
GROUP BY oimr.ID) Mvemnt
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fOIIgod</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $OIID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $OID     = $row['OID'];
  $OIID    = $row['OIID'];
  $OIIDd   = $row['OIIDd'];
  $OIIDnos = $row['OIIDnos'];
  $OPMID   = $row['OPMID'];
  $OTID    = $row['OTID'];
  $OPID    = $row['OPID'];
  $OIQID   = $row['OIQID'];
  $CPID    = $row['CPID'];
  $oref    = $row['oref'];
  $odel    = $row['orDate'];
  $OIIDComp = $row['OIIDComp'];
  $samProd = $row['samProd'];
  $oiQty   = $row['oiQty'];
  $NOTE    = $row['NOTE'];
  $dueDate = $row['dueDate'];
  $ACIDC   = $row['ACIDC'];
  $ACIDnC  = $row['ACIDnC'];
  $DIDnC   = $row['DIDnC'];
  $ACIDnS  = $row['ACIDnS'];
  $ACIDS   = $row['ACIDS'];
  $DIDnS   = $row['DIDnS'];
  $OIDcor  = $row['OIDcor'];
  $OIDtor  = $row['OIDtor'];
  $itemRef = $row['OIIDcr'];
  $pPRID   = $row['pPRID'];
  $pItemRef  = $row['PprRef'];
  $PTID   = $row['PTID'];
  $PTIDn   = $row['PTIDn'];
  $PRIDr   = $row['prRef'];
  $PRID   = $row['PRID'];
  $PRIDn   = $row['PRIDn'];
  $pPTID   = $row['pPTID'];
  $pPTIDn   = $row['pPTIDn'];
  $OIIDd = $row['OIIDd'];
  $odel = date('d-M-Y', $odel);
}
// echo "OPID::$OPID // ";
