<?php
session_start();

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'Order Status';
$pddhbc = '#fffddd';
include 'page_description_date_header.inc.php';
include 'set_urlPage.inc.php';

  $sql = "SELECT ORDER_STATUS.DEL_WEEK AS DEL_WEEK
  -- SQL is order_status_summary.sql
         , count(distinct ORDER_STATUS.OIID) AS COUNT_OIID
         , CASE WHEN ORDER_STATUS.OTID IN (1,3) THEN(SUM(ORDER_STATUS.ORDER_QTY)) - ORDER_SENT.CLIENT
                WHEN ORDER_STATUS.OTID IN (2,4) THEN(SUM(ORDER_STATUS.ORDER_QTY)) - ORDER_SENT.SUPPLIER
           END AS QTY
         , ORDER_STATUS.PTNR_ASSOC AS PTNR_ASSOC
         , ORDER_STATUS.SAM_PRODN AS SAM_PRODN
         , ORDER_STATUS.SAM_PROD AS SAM_PROD
         , ORDER_STATUS.OTIDN as OTIDn
         , ORDER_STATUS.OTID as OTID
         , CASE WHEN ORDER_STATUS.DEL_WEEK = yearweek(curdate(),7) THEN 'Due This Week'
  			  WHEN ORDER_STATUS.DEL_WEEK = yearweek(curdate(),7)+1 THEN 'Due Next Week'
  			  WHEN ORDER_STATUS.DEL_WEEK > yearweek(curdate(),7)+1 THEN 'DUE AFTER NEXT WEEK'
  			  ELSE 'Overdue'
  		 END AS STATUS
  FROM
  (
  SELECT oi.ID AS OIID
         , yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) AS DEL_WEEK
         , oiq.order_qty AS ORDER_QTY
         , IF(oi.samProd = 1,'SAMPLE','PRODUCTION') AS SAM_PRODN
         , oi.samProd AS SAM_PROD
         , CASE WHEN o.OTID = 1 THEN 'Sales'
                WHEN o.OTID = 2 THEN 'Purchases'
                WHEN o.OTID = 3 THEN 'Partner Sales'
                WHEN o.OTID = 4 THEN 'Partner Purchases'
           END AS OTIDN
         , o.OTID AS OTID
         , IF(o.fCID = o.tCID,'ASSOCIATE','PARTNER') AS PTNR_ASSOC
  FROM order_item oi
       INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
       INNER JOIN orders o ON oi.OID = o.ID
       INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
       INNER JOIN company c ON o.fCID = c.ID
       INNER JOIN company cS ON o.tCID = cS.ID
       INNER JOIN associate_companies ac ON o.fACID = ac.ID
       INNER JOIN associate_companies acS ON o.tACID = acS.ID
       INNER JOIN division d ON o.fDID = d.ID
       INNER JOIN division dS ON o.tDID = dS.ID
  WHERE (o.fCID = 1 OR o.tCID = 1)
    AND oi.itComp = 0  -- Only include open order items
  GROUP BY DEL_WEEK, OTID, PTNR_ASSOC, SAM_PROD
  ) ORDER_STATUS INNER JOIN (
  							SELECT DISTINCT
  							     Mvemnt.O_ITEM_ID AS OIID
  							     , SUM(Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT) AS CLIENT
  							     , SUM(Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER) AS SUPPLIER
  							FROM (
  							    SELECT DISTINCT oimr.ID AS OIMR_ID
  							         , oimr.reason AS Reason
  							         , oi.ID AS O_ITEM_ID
  							         , opm.ID AS OPMID
  							         , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
  							         , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
  							         , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
                                       , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
  							         , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
  							         , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
  							         , IF((CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END)) AS CORR_OUT_SUPPLIER
                                       , IF((CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END)) AS CORR_IN_SUPPLIER
  							    FROM order_placed_move opm
  							         INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
  							         INNER JOIN order_placed op ON opm.OPID = op.ID
  							         INNER JOIN order_item oi ON op.OIID = oi.ID
  							         INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
  							    GROUP BY opm.ID, oi.ID, oimr.ID
  							   ) Mvemnt INNER JOIN order_item oi ON Mvemnt.O_ITEM_ID = oi.ID
  							GROUP BY Mvemnt.OPMID
  							ORDER BY Mvemnt.OPMID DESC
  						  ) ORDER_SENT ON ORDER_STATUS.OIID = ORDER_SENT.OIID
  GROUP BY DEL_WEEK, SAM_PROD, PTNR_ASSOC
  ;";
  $stmt = mysqli_stmt_init($con);
  if (!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b><br>FAIL-fdcs</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "s", $fCID);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_array($result)){
      $DEL_WEEK = $row['DEL_WEEK'];
      $COUNT_OIID = $row['COUNT_OIID'];
      $PTNR_ASSOC = $row['PTNR_ASSOC'];
      $SAM_PRODN = $row['SAM_PRODN'];
      $sp = $row['SAM_PROD'];
      $QTY = $row['QTY'];
      $STATUS = $row['STATUS'];
      $OTID = $row['OTID'];
      $OTIDn = $row['OTIDn'];

      if ($STATUS == "Overdue") {
        $bc = "fc4141";
      }elseif ($STATUS == "Due This Week") {
        $bc = "f7a0a0";
      }elseif ($STATUS == "Due Next Week") {
        $bc = "ecf5a9";
      }

      ?>
      <!-- <tr>
        <td style="background-color: #<?php echo $bc ?>; text-align:left; text-indent:2%;"><?php echo $STATUS ?></td>
        <td style="text-align:left; text-indent:2%;"><a style="color:blue; text-decoration:none;" href="<?php echo "$urlPage&tab=1&ot=$OTID&sp=$sp" ?>"><?php echo $SAM_PRODN ?></a></td>
        <td style="text-align:left; text-indent:2%;"><a style="color:blue; text-decoration:none;" href="<?php echo "$urlPage&tab=1&ot=$OTID&sp=$sp" ?>"><?php echo $OTIDn ?></a></td>
        <td style="text-align:left; text-indent:2%;"><a style="color:blue; text-decoration:none;" href="<?php echo "$urlPage&tab=1&ot=$OTID&sp=$sp" ?>"><?php echo $COUNT_OIID ?></a></td>
        <td style="text-align:left; text-indent:2%;"><a style="color:blue; text-decoration:none;" href="<?php echo "$urlPage&tab=1&ot=$OTID&sp=$sp" ?>"><?php echo $QTY ?></a></td>
      </tr> -->
    <?php
    }
  }
  ?>
</table>

  <table class="trs" style="position:absolute; top:10%; left:30%; width:40%;">
    <caption>Summary (count) of Associate Sales Orders by Due by Date</caption>
    <tr>
      <th></th>
      <th>Over due</th>
      <th>This Week</th>
      <th>Next Week</th>
      <th>Later</th>
    </tr>
    <tr>
      <th>Samples</th>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=1&ot=1&sp=1">O/d</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=2&ot=1&sp=1">T/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=3&ot=1&sp=1">N/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=4&ot=1&sp=1">Later</a></td>
    </tr>
    <tr>
      <th>Production</th>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=1&ot=1&sp=2">O/d</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=2&ot=1&sp=2">T/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=3&ot=1&sp=2">N/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=4&ot=1&sp=2">Later</a></td>
    </tr>
  </table>

  <table class="trs" style="position:absolute; top:30%; left:30%; width:40%;">
    <caption>Summary (count) of Associate Sales Orders by Due by Date</caption>
    <tr>
      <th></th>
      <th>Over due</th>
      <th>This Week</th>
      <th>Next Week</th>
      <th>Later</th>
    </tr>
    <tr>
      <th>Samples</th>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=1&ot=2&sp=1">O/d</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=2&ot=2&sp=1">T/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=3&ot=2&sp=1">N/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=4&ot=2&sp=1">Later</a></td>
    </tr>
    <tr>
      <th>Production</th>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=1&ot=2&sp=2">O/d</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=2&ot=2&sp=2">T/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=3&ot=2&sp=2">N/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=4&ot=2&sp=2">Later</a></td>
    </tr>
  </table>

  <table class="trs" style="position:absolute; top:50%; left:30%; width:40%;">
    <caption>Summary (count) of Partner Sales Orders by Due by Date</caption>
    <tr>
      <th></th>
      <th>Over due</th>
      <th>This Week</th>
      <th>Next Week</th>
      <th>Later</th>
    </tr>
    <tr>
      <th>Samples</th>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=1&ot=3&sp=1">O/d</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=2&ot=3&sp=1">T/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=3&ot=3&sp=1">N/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=4&ot=3&sp=1">Later</a></td>
    </tr>
    <tr>
      <th>Production</th>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=1&ot=3&sp=2">O/d</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=2&ot=3&sp=2">T/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=3&ot=3&sp=2">N/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=4&ot=3&sp=2">Later</a></td>
    </tr>
  </table>

  <table class="trs" style="position:absolute; top:70%; left:30%; width:40%;">
    <caption>Summary (count) of Associate Purchase Orders by Due by Date</caption>
    <tr>
      <th></th>
      <th>Over due</th>
      <th>This Week</th>
      <th>Next Week</th>
      <th>Later</th>
    </tr>
    <tr>
      <th>Samples</th>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=1&ot=4&sp=1">O/d</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=2&ot=4&sp=1">T/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=3&ot=4&sp=1">N/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=4&ot=4&sp=1">Later</a></td>
    </tr>
    <tr>
      <th>Production</th>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=1&ot=4&sp=2">O/d</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=2&ot=4&sp=2">T/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=3&ot=4&sp=2">N/w</a></td>
      <td><a style="color:blue; text-decoration:none;" href="<?php echo $urlPage ?>&tab=4&ot=4&sp=2">Later</a></td>
    </tr>
  </table>
