<?php
session_start();
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';

$td = date('U');
$td = date('d M Y',$td);
$pddh = 'Warehoused Goods Summary';
$pddhbc = $whsCol;

include 'from_CID_count_partner_clients.inc.php';
include 'from_CID_count_partner_suppliers.inc.php';
include 'page_description_date_header.inc.php';
include 'set_urlPage.inc.php';

$CID = $_SESSION['CID'];

$sql = "SELECT O_STATUS.STATUS AS STATUS
-- SQL is order_status_summary_20210409.sql
       , O_STATUS.OTID AS OTID
       , O_STATUS.SAM_PROD AS SAM_PROD
       , SUM(O_STATUS.COUNT_OIID) AS COUNT_OIID
       , SUM(O_STATUS.COUNT_OID) AS COUNT_OID
       , SUM(QTY) AS QTY
FROM
(
SELECT CASE WHEN ORDER_STATUS.DEL_WEEK = yearweek(curdate(),7)   THEN IF(ORDER_STATUS.DEL_DATE < curdate(),'1','2')
            WHEN ORDER_STATUS.DEL_WEEK = yearweek(curdate(),7)+1 THEN '3'
            WHEN ORDER_STATUS.DEL_WEEK > yearweek(curdate(),7)+1 THEN '4'
            ELSE '1'
       END AS STATUS
       , ORDER_STATUS.OTID as OTID
       , ORDER_STATUS.SAM_PROD AS SAM_PROD
       , count(distinct ORDER_STATUS.OIID) AS COUNT_OIID
       , count(distinct ORDER_STATUS.OID) AS COUNT_OID
       , CASE WHEN ORDER_STATUS.OTID IN (1,3) THEN(SUM(ORDER_STATUS.ORDER_QTY)) - ORDER_SENT.CLIENT
              WHEN ORDER_STATUS.OTID IN (2,4) THEN(SUM(ORDER_STATUS.ORDER_QTY)) - ORDER_SENT.SUPPLIER
         END AS QTY
FROM
(
SELECT oi.ID AS OIID
       , o.ID AS OID
       , yearweek(from_unixtime(oidd.item_del_date,'%Y-%m-%d'),7) AS DEL_WEEK
       , from_unixtime(oidd.item_del_date,'%Y-%m-%d') AS DEL_DATE
       , oiq.order_qty AS ORDER_QTY
       , oi.samProd AS SAM_PROD
       , o.OTID AS OTID
FROM order_item oi
     INNER JOIN order_item_del_date oidd ON oi.ID = oidd.OIID
     INNER JOIN orders o ON oi.OID = o.ID
     INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
     INNER JOIN company c ON o.fCID = c.ID
     INNER JOIN company cS ON o.tCID = cS.ID
     INNER JOIN associate_companies ac ON o.fACID = ac.ID
     INNER JOIN associate_companies acS ON o.tACID = acS.ID
     INNER JOIN division d ON o.fDID = d.ID
     INNER JOIN division dS ON o.tDID = dS.ID
WHERE (o.fCID = ? OR o.tCID = ?)
  AND oi.itComp = 0  -- Only include open order items
-- GROUP BY DEL_WEEK, OTID, SAM_PROD
ORDER BY DEL_WEEK
) ORDER_STATUS INNER JOIN (
              SELECT DISTINCT
                   Mvemnt.O_ITEM_ID AS OIID
                   , SUM(Mvemnt.TO_CLIENT + Mvemnt.CORR_IN_CLIENT - Mvemnt.FROM_CLIENT - Mvemnt.CORR_OUT_CLIENT) AS CLIENT
                   , SUM(Mvemnt.RECEIVED_FROM_SUPPLIER + Mvemnt.CORR_IN_SUPPLIER - Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.CORR_OUT_SUPPLIER) AS SUPPLIER
              FROM (
                  SELECT DISTINCT oimr.ID AS OIMR_ID
                         , oimr.reason AS Reason
                         , oi.ID AS O_ITEM_ID
                         , opm.ID AS OPMID
                         , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
                         , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
                         , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
                         , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
                         , IF((CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 35 THEN SUM(opm.omQty) END)) AS CORR_OUT_CLIENT
                         , IF((CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 36 THEN SUM(opm.omQty) END)) AS CORR_IN_CLIENT
                         , IF((CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 37 THEN SUM(opm.omQty) END)) AS CORR_OUT_SUPPLIER
                         , IF((CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 38 THEN SUM(opm.omQty) END)) AS CORR_IN_SUPPLIER
                  FROM order_placed_move opm
                       INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
                       INNER JOIN order_placed op ON opm.OPID = op.ID
                       INNER JOIN order_item oi ON op.OIID = oi.ID
                       INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
                  GROUP BY opm.ID, oi.ID, oimr.ID
                 ) Mvemnt INNER JOIN order_item oi ON Mvemnt.O_ITEM_ID = oi.ID
              GROUP BY OIID
              ORDER BY OIID ASC
              ) ORDER_SENT ON ORDER_STATUS.OIID = ORDER_SENT.OIID
GROUP BY STATUS, SAM_PROD, OTID, ORDER_STATUS.OID
ORDER BY STATUS, OTID, SAM_PROD ASC
) O_STATUS
GROUP BY O_STATUS.STATUS, O_STATUS.SAM_PROD, O_STATUS.OTID
;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b><br><br><br><br><br><br>FAIL-fdcs</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $OTID = $row['OTID'];
    $sp = $row['SAM_PROD'];
    $tab = $row['STATUS'];
    $QTY = $row['QTY'];
    $cOIID = $row['COUNT_OIID'];

    ?>
    <!-- SALES ORDERS -->
    <div style="position:absolute; top:16%; left:25%; width:50%; font-weight: bold; text-align:center; background-color:<?php echo $whsCol ?>;">SALES Orders</div>
    <div style="position:absolute; top:19%; left:35%; width:10%; font-weight: bold; text-align:center;">Over Due</div>
    <div style="position:absolute; top:19%; left:45%; width:10%; font-weight: bold; text-align:center;">This Week</div>
    <div style="position:absolute; top:19%; left:55%; width:10%; font-weight: bold; text-align:center;">Next Week</div>
    <div style="position:absolute; top:19%; left:65%; width:10%; font-weight: bold; text-align:center;">Later</div>

    <div style="position:absolute; top:22%; left:25%; width:50%; font-weight: bold; text-align:left; text-indent:2%; background-color:#ffffdd;">Samples</div>
    <div style="position:absolute; top:25%; left:25%; width:10%; font-weight: bold; text-align:center;">Production</div>


    <?php
    // SAMPLES
    if (($tab == 1) && ($OTID == 1) && ($sp == 1)) {
      ?>
      <div style="position:absolute; top:22%;  left:35%; width:10%; text-align:center; z-index:1;">
          <a style="color:blue; text-decoration:none;" href="home.php?H&tab=<?php echo  $tab ?>&sp=<?php echo $sp ?>&ot=<?php echo $OTID ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
    if (($tab == 2) && ($OTID == 1) && ($sp == 1)) {
      ?>
      <div style="position:absolute; top:22%;  left:45%; width:10%; text-align:center; z-index:1;">
         <a style="color:blue; text-decoration:none;" href="home.php?H&tab=<?php echo  $tab ?>&sp=<?php echo $sp ?>&ot=<?php echo $OTID ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
    if (($tab == 3) && ($OTID == 1) && ($sp == 1)) {
      ?>
      <div style="position:absolute; top:22%;  left:55%; width:10%; text-align:center; z-index:1;">
         <a style="color:blue; text-decoration:none;" href="home.php?H&tab=<?php echo  $tab ?>&sp=<?php echo $sp ?>&ot=<?php echo $OTID ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
    if (($tab == 4) && ($OTID == 1) && ($sp == 1)) {
      ?>
      <div style="position:absolute; top:22%;  left:65%; width:10%; text-align:center; z-index:1;">
         <a style="color:blue; text-decoration:none;" href="home.php?H&tab=<?php echo  $tab ?>&sp=<?php echo $sp ?>&ot=<?php echo $OTID ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }

    // PRODUCTION
    if (($tab == 1) && ($OTID == 1) && ($sp == 2)) {
      ?>
      <div style="position:absolute; top:25%;  left:35%; width:10%; text-align:center;">
         <a style="color:blue; text-decoration:none;" href="home.php?H&tab=<?php echo  $tab ?>&sp=<?php echo $sp ?>&ot=<?php echo $OTID ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
    if (($tab == 2) && ($OTID == 1) && ($sp == 2)) {
      ?>
      <div style="position:absolute; top:25%;  left:45%; width:10%; text-align:center;">
         <a style="color:blue; text-decoration:none;" href="home.php?H&tab=<?php echo  $tab ?>&sp=<?php echo $sp ?>&ot=<?php echo $OTID ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
    if (($tab == 3) && ($OTID == 1) && ($sp == 2)) {
      ?>
      <div style="position:absolute; top:25%;  left:55%; width:10%; text-align:center;">
         <a style="color:blue; text-decoration:none;" href="home.php?H&tab=<?php echo  $tab ?>&sp=<?php echo $sp ?>&ot=<?php echo $OTID ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
    if (($tab == 4) && ($OTID == 1) && ($sp == 2)) {
      ?>
      <div style="position:absolute; top:25%;  left:65%; width:10%; text-align:center;">
         <a style="color:blue; text-decoration:none;" href="home.php?H&tab=<?php echo  $tab ?>&sp=<?php echo $sp ?>&ot=<?php echo $OTID ?>"><?php echo "$QTY / $cOIID" ?></a>
      </div>
      <?php
    }
  }
}
