<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "<br><b>form_change_request_approval.inc.php</b>";

if (isset($_GET['o'])) {
  $OID = $_GET['o'];
}

?>
<table class="trs" style="position:absolute; top:82%; left:0%; width:100%; z-index:1;">
  <caption style="background-color:<?php echo $edtCol ?>; font-weight:bold;">Partner requests needing approval</caption>
  <th style="width:10%;">Date</th>
  <th style="width:4%;">Item</th>
  <th style="width:38%; text-align:left; text-indent:1%;">Request</th>
  <th style="width:42%; text-align:left; text-indent:1%;">Reason</th>
  <th colspan="2" style="width:6%;">Action</th>
<?php

$sql = "SELECT oic.ID as OICID
          -- form_change_request_approval.sql
          , oic.reqCID as oicCID
          , opm.type as OPMIDt
          , opm.inputtime as OPMIDtime
          , oimr.id as OIMRID
          , oimr.reason as OIMRIDr
          , oidd.item_del_date as OIDDIDd
          , oic.Ndate as Ndate
          , oiq.order_qty as OIQIDq
          , oic.Nqty as Nqty
          , oi.ord_item_nos as OIIDnos
          , oi.id as OIID
        FROM order_item_change oic
          , order_placed_move opm
          , order_placed op
          , orders o
          , order_item oi
          , order_item_qty oiq
          , order_item_movement_reason oimr
          , order_item_del_date oidd
        WHERE o.ID = ?
        -- AND oi.ID = ?
        AND oic.OPMID = opm.ID
        AND opm.OPID = op.ID
        AND op.OIID = oi.ID
        AND oi.OID = o.ID
        AND oic.status = 0
        AND opm.OIMRID = oimr.ID
        AND oidd.OIID = oi.ID
        AND oiq.OIID = oi.ID;
";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>FAIL-fcra</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_assoc($result)){
    $OICID     = $row['OICID'];
    $oicCID     = $row['oicCID'];
    $OPMIDt    = $row['OPMIDt'];
    $OPMIDtime = $row['OPMIDtime'];
    $OIMRID   = $row['OIMRID'];
    $OIMRIDr   = $row['OIMRIDr'];
    $OIDDIDd     = $row['OIDDIDd'];
    $Ndate     = $row['Ndate'];
    $OIQIDq    = $row['OIQIDq'] ;
    $Nqty    = $row['Nqty'] ;
    $OIIDnos   = $row['OIIDnos'];
    $OIID     = $row['OIID'];

    $OIDDIDda = date('d-M-Y', $OIDDIDd);
    $Ndatea = date('d-M-Y', $Ndate);
    $OPMIDtime = date('d-M-Y', $OPMIDtime);

    if ($OIMRID == 40) {
      // echo "<br>DATE CHANGE REQUEST";
      $type = "Request to change the item delivery date";
    }elseif ($OIMRID == 43) {
      // echo "<br>QUANTITY CHANGE REQUEST";
      $type = "Request to reduce the item quantity";
    }elseif ($OIMRID == 45) {
      // echo "<br>CANCELLATION REQUEST";
      $type = "Request to cancel the item";
    }

    // if ($Ndate == 0) {
    //   if ($OIQIDq < $Nqty) {
    //     $type = "Item quantity reduced to $Nqty";
    //     $bc = "#ff879c";
    //   }
    // }elseif ($Ndate != 0) {
    //   // we are using the date
    //   if ($OIDDIDd > $Ndate) {
    //     $type = "Delivery date reduced to $Ndatea";
    //     $bc = "#ff879c";
    //   }elseif ($OIDDIDd < $Ndate) {
    //     $type = "Delivery date extension to $Ndatea";
    //     $bc = "#86eb86";
    //   }
    // }

    ?>
    <tr style="font-family:monospace;">
      <td><?php echo $OPMIDtime ?></td>
      <td><?php echo $OIIDnos ?></td>
      <td style="background-color: <?php echo $bc ?>; text-align:left; text-indent:1%;"><?php echo $type ?> (<?php echo $OICID ?>)</td>
      <td style=" text-align:left; text-indent:1%;"><?php echo $OPMIDt ?>(<?php echo "$oicCID/$CID" ?>)</td>
      <?php
      if ($oicCID == $CID) {
        ?>
        <td colspan="2">Pending</td>
        <?php
      }else {
        ?>
        <td><a href="home.php?H&o=<?php echo $OID ?>&OI=<?php echo $OIID ?>&oiy=<?php echo $OICID ?>">Yes</a></td>
        <td><a href="home.php?H&o=<?php echo $OID ?>&OI=<?php echo $OIID ?>&oin=<?php echo $OICID ?>">No</a></td>
        <?php
      }
       ?>
    </tr>
    <?php
  }
  ?></table><?php
}
