<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
include 'from_CID_count_orders.inc.php';
include 'from_CID_get_min_prod_type.inc.php';
// echo "report_styles_per_product_type.inc.php";

// if ((isset($_POST['prd']))) {
//   $PRID = $_POST['prd'];
// }
// include 'include/from_CID_count_styles.inc.php';

include 'set_urlPage.inc.php';

// if (isset($_GET['p'])) {
//   $PTID = $_GET['p'];
// }

if (isset($_GET['p'])) {
  $PTID = $_GET['p'];
}

include 'from_PTID_get_product_type_info.inc.php';

// $ACID = $_SESSION['ACID'];
$CID = $_SESSION['CID'];
$ACID = $_SESSION['ACID'];
// echo "ACID = $ACID";

$td = date('U');
$td = date('d M Y',$td);
$pddh = "Style Register : $PTIDn";
$pddhbc = $styCol;
include 'page_description_date_header.inc.php';

$td = date('U');
// $cPRID = $cPRID - 1;

// set the return pages section
if (isset($_GET['snn'])) {$head = 'snn'; $rpp = 25;
}elseif (isset($_GET['snr'])) {$head = 'snr'; $rpp = 30;
}elseif (isset($_GET['sne'])) {$head = 'sne'; $rpp = 10;
}elseif (isset($_GET['sis'])) {$head = 'sis'; $rpp = 10;
}

// Check the URL to see what page we are on
if (isset($_GET['pa'])) {
  $pa = $_GET['pa'];
}else{
  $pa = 0;
}

// Set the rows per page
$rpp = 30;

// Check which page we are on
if ($pa > 1) {
  $start = ($pa * $rpp) - $rpp;
}else {
  $start = 0;
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cPRID / $rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV);

// need to count the number of styles for the product type
$sql = "SELECT Count(pr.ID) as PRID
        FROM prod_ref pr
        WHERE pr.PTID = ?
        AND pr.prod_desc NOT IN('To be Selected')
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rst</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $ACID);
  mysqli_stmt_execute($stmt);
  $res = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($res);
  $cPRID = $row['PRID'];
}

?>

<table class="trs" style="position:relative; top:9%;">
   <tr>
     <th style="background-color:#f1f1f1;" colspan="2"></th>
     <th style="border-radius:5px 5px 0px 0px;" colspan="2">ORDERS</th>
   </tr>
  <tr>
    <th style="width:10%;text-align:left; text-indent:4%;">Short Code</th>
    <th style="width:68%;text-align:left; text-indent:3%;">Description</th>
    <th style="width:5%;border-left:thin solid black;">Open</th>
    <th style="width:5%;border-right:thin solid black;">Total</th>
    <th style="width:12%;">Created</th>
  </tr>
  <?php

  $sql = "SELECT distinct(pr.ID) AS PRID
  -- report_style.sql
            , pr.prod_ref AS PRIDr
            , pr.prod_desc AS PRIDd
            , pr.PTID as PTID
            , pr.inputtime as input
            , IF(pt.name='Select Product Type','TBC',pt.name) AS PTIDr
            , DIVISIONS.cDIV AS DIVISIONS
            , IF(SEASON.cSN IS NULL,'NO SEASON',SEASON.cSN) AS SEASONS
            , IF(COLLECTION.cCN IS NULL,'NO COLLECTION',COLLECTION.cCN) AS COLLECTIONS
            , pr.etm AS PRIDe
            , IF(O_ORDR.OUTST IS NULL,0,O_ORDR.OUTST) AS OUTSORD
            , IF(TOTAL_ORD.TOT_ORDER IS NULL,0,TOTAL_ORD.TOT_ORDER) AS TOTORD
          FROM prod_ref pr
          INNER JOIN product_type pt ON pr.PTID = pt.ID
          LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                        , count(DISTINCT pr2d.DID) AS cDIV
                      FROM prod_ref pr
                      INNER JOIN prod_ref_to_div pr2d ON pr.ID = pr2d.PRID
                      INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                      GROUP BY pr.ID -- , pr2d.DID
                      ) DIVISIONS ON pr.ID = DIVISIONS.PRID
          LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                        , count(DISTINCT pr2sn.SNID) AS cSN
                      FROM prod_ref pr
                      INNER JOIN prod_ref_to_season pr2sn ON pr.ID = pr2sn.PRID
                      INNER JOIN season sn ON pr2sn.SNID = sn.ID
                      INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                      GROUP BY pr.ID -- , pr2sn.SNID
                    ) SEASON ON pr.ID = SEASON.PRID
          LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                        , count(DISTINCT pr2c.PRID) As cCN
                      FROM prod_ref pr
                      INNER JOIN product_type pt ON pr.PTID = pt.ID
                      INNER JOIN prod_ref_to_collection pr2c ON pr.ID = pr2c.PRID
                      INNER JOIN collection c ON pr2c.CLID = c.ID
                      INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                      GROUP BY pr.ID -- , pr2c.PRID
                      ) COLLECTION ON pr.ID = COLLECTION.PRID
          LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                            , count(DISTINCT o.ID) AS OUTST
                          FROM prod_ref pr
                          INNER JOIN product_type pt ON pr.PTID = pt.ID
                          INNER JOIN order_item oi ON pr.ID = oi.PRID
                          INNER JOIN orders o ON oi.OID = o.ID
                          INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                          AND o.orComp = 0
                          GROUP BY pr.ID
                          ) O_ORDR ON pr.ID = O_ORDR.PRID
          LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                            , count(DISTINCT o.ID) AS TOT_ORDER
                          FROM prod_ref pr
                          INNER JOIN product_type pt ON pr.PTID = pt.ID
                          INNER JOIN order_item oi ON pr.ID = oi.PRID
                          INNER JOIN orders o ON oi.OID = o.ID
                          INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                          GROUP BY pr.ID
                          ) TOTAL_ORD ON pr.ID = TOTAL_ORD.PRID
          LEFT OUTER JOIN (SELECT distinct(pr.ID) AS PRID
                            , count(DISTINCT o.ID) AS COMPL
                          FROM prod_ref pr
                          INNER JOIN product_type pt ON pr.PTID = pt.ID
                          INNER JOIN order_item oi ON pr.ID = oi.PRID
                          INNER JOIN orders o ON oi.OID = o.ID
                          INNER JOIN associate_companies ac ON pr.ACID = ac.ID
                          AND o.orComp = 1
                          GROUP BY pr.ID
                          ) COM_ORDR ON pr.ID = O_ORDR.PRID
          WHERE pr.prod_ref != 'TBC'
            AND pr.PTID = ?
          ORDER BY pr.prod_ref
          LIMIT ?,?
  ;";
  $stmt = mysqli_stmt_init($con);
  if(!mysqli_stmt_prepare($stmt, $sql)){
    echo '<b>FAIL-rst</b>';
  }else{
    mysqli_stmt_bind_param($stmt, "sss", $PTID, $start, $rpp);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    while($row = mysqli_fetch_array($res)){
      $PRID  = $row['PRID'];
      $PRIDr = $row['PRIDr'];
      $PRIDd = $row['PRIDd'];
      $PRIDe = $row['PRIDe'];
      $input = $row['input'];
      $PTIDr = $row['PTIDr'];
      $PTID  = $row['PTID'];
      // $DIDn  = $row['DIDn'];
      $cDID  = $row['DIVISIONS'];
      $cSNID = $row['SEASONS'];
      $cCLID = $row['COLLECTIONS'];
      $cOIDo = $row['OUTSORD'] ;
      $cOIDt = $row['TOTORD'] ;

      $cDID = $cDID - 1;
      // $cSNID = $cSNID - 1;
      // $cCLID = $cCLID - 1;

      // echo "<br>PRID :: $PRID";

      // include 'from_CID_count_product_type.inc.php';
      // include 'from_CID_get_min_prod_type.inc.php';

      $input = date('d-M-Y',$input);
      ?>
      <tr style="font-family:monospace;">
        <!-- <td ><?php echo "$PRID"; ?></td> -->
        <td style="width:8%; border-right:thin solid grey;font-size: 100%; text-align:left; text-indent:4%;"><a style="color:blue; text-decoration:none;" href="styles.php?S&spr&fso=<?php echo $PRID ?>&p=<?php echo $PTID ?>"> <?php echo $PRIDr;?></a></td>
        <?php
        if (strlen($PRIDd) < 50) {
          ?>
          <td style="border-right:thin solid black; text-align:left; text-indent:2%;"><a style="color:blue; text-decoration:none;" href="styles.php?S&spr&fso=<?php echo $PRID ?>&p=<?php echo $PTID ?>"> <?php echo $PRIDd;?></a></td>
          <?php
        }else {
          $PRIDd = substr($PRIDd,0,50);
          ?>
          <td style="border-right:thin solid black; text-align:left; text-indent:2%;"><a style="color:blue; text-decoration:none;" href="styles.php?S&spr&fso=<?php echo $PRID ?>&p=<?php echo $PTID ?>"><?php echo $PRIDd;?>...</a></td>
          <?php
        }

        if ($cOIDo == 0) {
          ?>
          <td style="border-right:thin solid grey;"></td>
          <?php
        }else {
          ?>
          <td style="border-right:thin solid grey;"><a style="color:blue; text-decoration:none;" href="styles.php?S&sio&s=<?php echo $PRID ?>"><?php echo $cOIDo ?></a></td>
          <?php
        }
        if ($cOIDt == 0) {
          ?>
          <td style="border-right:thin solid black;"></td>
          <?php
        }else {
          ?>
          <td style="border-right:thin solid black;"><?php echo $cOIDt ?></td>
          <?php
        }
        ?>
        <td><?php echo $input ?></td>
      </tr>
      <?php
      }
      ?>
      <tr>
        <td colspan="6" style="border-top:2px solid black;"></td>
      </tr>
    </table>
<?php
}

?>
<form action="styles.php?S&spr&fns&p=<?php echo $PTID ?>&fnsh" method="post">
  <button class="entbtn" type="submit" style="position:absolute; bottom:5%; left:5%; width:15%; background-color:<?php echo $hlpCol ?>" name="help">HELP</button>
</form>
<form action="styles.php?S&spr&fns&p=<?php echo $PTID ?>" method="post">
  <button class="entbtn" type="submit" style="position:absolute; bottom:5%; left:80%; width:15%;" name="addPtypebutton">Add Style</button>
</form>
