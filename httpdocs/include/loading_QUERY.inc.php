<?php
session_start();
include 'dbconnect.inc.php';
// echo "include/loading_QUERY.inc.php";

include 'set_urlPage.inc.php';

$CID = $_SESSION['CID'];

// SQL is order_report_with section_loading_20201112.sql
$sql = "SELECT TOTALS.cliCID as cliCID
          , cr.Client_Name AS cliCIDn
          , cr.Assoc_Client_Co AS cli_ACID
          , cr.Assoc_Client_Name AS cli_ACIDn
          , TOTALS.supCID
          , cr.Supplier_Co_Name AS supCIDn
          , cr.Assoc_Supplier_Co AS sup_ACID
          , cr.Assoc_Supplier_name AS sup_ACIDn
          , TOTALS.UID as TUID
          , TOTALS.inputtime as Ttd
          , TOTALS.OID
          , TOTALS.OIID
          , TOTALS.ordNos
          , TOTALS.ordItemNos
          , TOTALS.OTID
          , TOTALS.ourRef
          , TOTALS.ODDIDdd
          , TOTALS.item_due_date
          , TOTALS.CPID
          , TOTALS.PRIDs
          , TOTALS.OIQIDq
          , TOTALS.Awaiting
          , TOTALS.Stage1
          , TOTALS.Stage2
          , TOTALS.Stage3
          , TOTALS.Stage4
          , TOTALS.Stage5
          , TOTALS.Warehouse
          , TOTALS.CLIENT AS CLIENT
          , TOTALS.SUPPLIER AS SUPPLIER
        FROM
          (
          SELECT DISTINCT
               o.fCID AS cliCID
               , cp.acc_CID AS supCID
               , Mvemnt.ORDER_ID AS OID
               , oi.ID AS OIID
               , o.orNos AS ordNos
               , oi.ord_item_nos AS ordItemNos
               , ot.ID AS OTID
               , o.our_order_ref AS ourRef
               , odd.del_Date AS ODDIDdd
               , oidd.item_del_date AS item_due_date
               , o.CPID AS CPID
               , pr.prod_ref AS PRIDs
               , Mvemnt.ordQty AS OIQIDq
               , cp.req_CID
               , o.tACID
          -- CALCULATE INDIVIDUAL GARMENT MOVEMENT VALUES FOR EACH SECTION
               , SUM(Mvemnt.ORDER_PLACEMENT - Mvemnt.INTO_STG1 + Mvemnt.ORDER_PLACE_INCR - Mvemnt.ORDER_PLACE_DECR + Mvemnt.REJ_BY_STG1 + Mvemnt.COR_IN_STG1 - Mvemnt.COR_OUT_STG1) AS Awaiting
               , SUM(Mvemnt.INTO_STG1 - Mvemnt.INTO_STG2 - Mvemnt.REJ_BY_STG1 + Mvemnt.REJ_BY_STG2 - Mvemnt.COR_IN_STG1 + Mvemnt.COR_IN_STG2 + Mvemnt.COR_OUT_STG1 - Mvemnt.COR_OUT_STG2) AS Stage1
               , SUM(Mvemnt.INTO_STG2 - Mvemnt.INTO_STG3 - Mvemnt.REJ_BY_STG2 + Mvemnt.REJ_BY_STG3 - Mvemnt.COR_IN_STG2 + Mvemnt.COR_IN_STG3 + Mvemnt.COR_OUT_STG2 - Mvemnt.COR_OUT_STG3) AS Stage2
               , SUM(Mvemnt.INTO_STG3 - Mvemnt.INTO_STG4 - Mvemnt.REJ_BY_STG3 + Mvemnt.REJ_BY_STG4 - Mvemnt.COR_IN_STG3 + Mvemnt.COR_IN_STG4 + Mvemnt.COR_OUT_STG3 - Mvemnt.COR_OUT_STG4) AS Stage3
               , SUM(Mvemnt.INTO_STG4 - Mvemnt.INTO_STG5 - Mvemnt.REJ_BY_STG4 + Mvemnt.REJ_BY_STG5 - Mvemnt.COR_IN_STG4 + Mvemnt.COR_IN_STG5 + Mvemnt.COR_OUT_STG4 - Mvemnt.COR_OUT_STG5) AS Stage4
               , SUM(Mvemnt.INTO_STG5 - Mvemnt.INTO_WHOUSE - Mvemnt.REJ_BY_STG5 + Mvemnt.REJ_BY_WHOUSE - Mvemnt.COR_IN_STG5 + Mvemnt.COR_IN_WHOUSE + Mvemnt.COR_OUT_STG5 - Mvemnt.COR_OUT_WHOUSE) AS Stage5
               , SUM(Mvemnt.INTO_WHOUSE - Mvemnt.REJ_BY_WHOUSE - Mvemnt.TO_CLIENT + Mvemnt.FROM_CLIENT - Mvemnt.RETURNED_TO_SUPPLIER + Mvemnt.RECEIVED_FROM_SUPPLIER - Mvemnt.COR_IN_WHOUSE + Mvemnt.COR_OUT_WHOUSE) AS Warehouse
               , SUM(Mvemnt.TO_CLIENT - Mvemnt.FROM_CLIENT) AS CLIENT
               , SUM(Mvemnt.RETURNED_TO_SUPPLIER - Mvemnt.RECEIVED_FROM_SUPPLIER) AS SUPPLIER
               , Mvemnt.UID AS UID
               , Mvemnt.inputtime AS inputtime
          FROM (
              SELECT oimr.ID AS OIMR_ID
                   , o.ID AS ORDER_ID
                   , o.ID AS Order_Placed_ID
                   , oiq.order_qty AS ordQty
                   , last_update.UID
                   , last_update.inputtime
              -- oimr1 not required at present - oiq.order_qty is used instead
                   , IF((CASE WHEN oimr.ID = 2 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 2 THEN SUM(opm.omQty) END)) AS  ORDER_PLACEMENT
                   , IF((CASE WHEN oimr.ID = 3 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 3 THEN SUM(opm.omQty) END)) AS INTO_STG1
                   , IF((CASE WHEN oimr.ID = 4 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 4 THEN SUM(opm.omQty) END)) AS INTO_STG2
                   , IF((CASE WHEN oimr.ID = 5 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 5 THEN SUM(opm.omQty) END)) AS INTO_STG3
                   , IF((CASE WHEN oimr.ID = 6 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 6 THEN SUM(opm.omQty) END)) AS INTO_STG4
                   , IF((CASE WHEN oimr.ID = 7 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 7 THEN SUM(opm.omQty) END)) AS INTO_STG5
                   , IF((CASE WHEN oimr.ID = 8 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 8 THEN SUM(opm.omQty) END)) AS INTO_WHOUSE
              -- oimr9 to oimr10 not required at present
                   , IF((CASE WHEN oimr.ID = 11 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 11 THEN SUM(opm.omQty) END)) AS ORDER_PLACE_INCR
                   , IF((CASE WHEN oimr.ID = 12 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 12 THEN SUM(opm.omQty) END)) AS ORDER_PLACE_DECR
                   , IF((CASE WHEN oimr.ID = 13 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 13 THEN SUM(opm.omQty) END)) AS REJ_BY_STG1
                   , IF((CASE WHEN oimr.ID = 14 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 14 THEN SUM(opm.omQty) END)) AS REJ_BY_STG2
                   , IF((CASE WHEN oimr.ID = 15 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 15 THEN SUM(opm.omQty) END)) AS REJ_BY_STG3
                   , IF((CASE WHEN oimr.ID = 16 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 16 THEN SUM(opm.omQty) END)) AS REJ_BY_STG4
                   , IF((CASE WHEN oimr.ID = 17 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 17 THEN SUM(opm.omQty) END)) AS REJ_BY_STG5
                   , IF((CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 18 THEN SUM(opm.omQty) END)) AS REJ_BY_WHOUSE
                   , IF((CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 19 THEN SUM(opm.omQty) END)) AS TO_CLIENT
                   , IF((CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 20 THEN SUM(opm.omQty) END)) AS FROM_CLIENT
                   , IF((CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 21 THEN SUM(opm.omQty) END)) AS RETURNED_TO_SUPPLIER
                   , IF((CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 22 THEN SUM(opm.omQty) END)) AS RECEIVED_FROM_SUPPLIER
                   , IF((CASE WHEN oimr.ID = 23 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 23 THEN SUM(opm.omQty) END)) AS COR_IN_STG1
                   , IF((CASE WHEN oimr.ID = 24 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 24 THEN SUM(opm.omQty) END)) AS COR_IN_STG2
                   , IF((CASE WHEN oimr.ID = 25 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 25 THEN SUM(opm.omQty) END)) AS COR_IN_STG3
                   , IF((CASE WHEN oimr.ID = 26 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 26 THEN SUM(opm.omQty) END)) AS COR_IN_STG4
                   , IF((CASE WHEN oimr.ID = 27 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 27 THEN SUM(opm.omQty) END)) AS COR_IN_STG5
                   , IF((CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 28 THEN SUM(opm.omQty) END)) AS COR_IN_WHOUSE
                   , IF((CASE WHEN oimr.ID = 29 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 29 THEN SUM(opm.omQty) END)) AS COR_OUT_STG1
                   , IF((CASE WHEN oimr.ID = 30 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 30 THEN SUM(opm.omQty) END)) AS COR_OUT_STG2
                   , IF((CASE WHEN oimr.ID = 31 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 31 THEN SUM(opm.omQty) END)) AS COR_OUT_STG3
                   , IF((CASE WHEN oimr.ID = 32 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 32 THEN SUM(opm.omQty) END)) AS COR_OUT_STG4
                   , IF((CASE WHEN oimr.ID = 33 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 33 THEN SUM(opm.omQty) END)) AS COR_OUT_STG5
                   , IF((CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END) IS NULL,0,(CASE WHEN oimr.ID = 34 THEN SUM(opm.omQty) END)) AS COR_OUT_WHOUSE
              -- oimr35 to oimr40 not required at present
              FROM order_placed_move opm
                 INNER JOIN order_item_movement_reason oimr ON opm.OIMRID = oimr.ID
                 INNER JOIN order_placed op ON opm.OPID = op.ID
                 INNER JOIN order_item oi ON op.OIID = oi.ID
                 INNER JOIN orders o ON oi.OID = o.ID
                 INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
                 INNER JOIN (SELECT opm_order.OPID AS OPID
                           , opm_order.UID AS UID
                           , opm_order.inputtime AS inputtime
                       FROM (SELECT opm.OPID
                             , opm.UID
                             , opm.inputtime
                           FROM order_placed_move opm
                             INNER JOIN order_placed op ON op.ID = opm.OPID
                             INNER JOIN order_item oi ON oi.ID = op.OIID
                             INNER JOIN orders o ON oi.OID = o.ID
                           ORDER BY opm.OPID, opm.ID DESC
                         ) opm_order
                       GROUP BY opm_order.OPID
                      ) last_update ON op.ID = last_update.OPID
                WHERE o.orComp = ?
              GROUP BY o.ID, oimr.ID
             ) Mvemnt INNER JOIN order_item oi ON Mvemnt.Order_Placed_ID = oi.ID
                  INNER JOIN orders o ON oi.OID = o.ID
                  INNER JOIN order_type ot ON oi.OTID = ot.ID
                  INNER JOIN order_item_qty oiq ON oi.ID = oiq.OIID
                  INNER JOIN order_item_del_date oidd ON oidd.OIID = oi.ID
                  INNER JOIN prod_ref pr ON pr.ID = oi.PRID
                  INNER JOIN company_partnerships cp ON o.CPID = cp.ID
                                  AND cp.req_CID = o.fCID
                  INNER JOIN orders_due_dates odd ON odd.OID = o.ID

          GROUP BY Mvemnt.ORDER_ID
          ) TOTALS
                      INNER JOIN company_relationships cr ON TOTALS.CPID = cr.CPID
                                 AND TOTALS.req_CID = cr.Client_ID
                                 AND TOTALS.tACID = cr.Assoc_Supplier_Co
        WHERE TOTALS.cliCID = ?
        AND cr.CPID IN (SELECT ID
                        FROM company_partnerships
                        WHERE ((req_CID = ?) || (acc_CID = ?))
                       )
        -- AND $dr BETWEEN $sd AND $ed
        -- ORDER BY $ord $sort
        -- LIMIT $start, $r
        GROUP BY cr.Client_ID, OID
        ;";
$stmt = mysqli_stmt_init($con);
if (!mysqli_stmt_prepare($stmt, $sql)) {
  echo '<b>TTFAIL-lQ</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ssss",$OC, $CID, $CID, $CID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $sort == 'DESC' ? $sort == 'ASC' : $sort == 'DESC';
  while($row = mysqli_fetch_assoc($result)){
    $cliCID = $row['cliCID'];
    $cliCIDn = $row['cliCIDn'];
    $cli_ACID = $row['cli_ACID'];
  $cli_ACIDn = $row['cli_ACIDn'];
    $supCID = $row['supCID'];
    $supCIDn = $row['supCIDn'];
    $sup_ACID = $row['sup_ACID'];
  $sup_ACIDn = $row['sup_ACIDn'];
    $TUID = $row['TUID'];
    $Ttd = $row['Ttd'];
    $OID = $row['OID'];
  $OIID = $row['OIID'];
  $OIDnos = $row['ordNos'];
  $OIIDnos = $row['ordItemNos'];
    $OTID = $row['OTID'];
  $ourRef = $row['ourRef'];
    $ODDIDdd = $row['ODDIDdd'];
  $OIDDIDd = $row['item_due_date'];
    $CPID = $row['CPID'];
  $PRIDs = $row['PRIDs'];
  $OIQIDq = $row['OIQIDq'];
  $Awaiting = $row['Awaiting'];
  $Stage1 = $row['Stage1'];
  $Stage2 = $row['Stage2'];
  $Stage3 = $row['Stage3'];
  $Stage4 = $row['Stage4'];
  $Stage5 = $row['Stage5'];
  $Warehouse = $row['Warehouse'];
    $CLIENT = $row['CLIENT'];
    $SUPPLIER = $row['SUPPLIER'];

// Taken OUT as DAL sorted in his sql
// LEFT IN UNTIL DAL UPDATES THE SQL
    if ($cliCID <> $CID) {
      $cli_ACIDn = $cli_ACIDn;
      $OTID = $OTID - 1;
    }else {
      $cli_ACIDn = $sup_ACIDn;
      $OTID = $OTID;
    }

    $idate = date('d-M-Y', $OIDDIDd);
    ?>
    <tr>
      <?php
      // Shows you the orders placed within the last 7 days
      // if($od>($td-604740)){
      if ($OTID == 1) {
        $c = 'black';
        $bc = 'f7cc6c';
        $SamProd = 'P';
        // $coName = $pACIDn;
      }elseif ($OTID == 2) {
        $c = 'black';
        $dferr = '';
        $bc = 'bbbbff';
        $SamProd = 'P';
        // $coName = $pACIDn;
      }elseif ($OTID == 3) {
        $c = 'white';
        $bc = 'f243e4';
        $SamProd = 'P';
        // $coName = $pACIDn;
      }elseif ($OTID == 4) {
        $c = 'white';
        $bc = '577268';
        $SamProd = 'P';
        // $coName = $ACIDn;
      }elseif ($OTID == 5) {
        $c = 'black';
        $bc = 'f7cc6c';
        $SamProd = 'S';
        // $coName = $ACIDn;
      }elseif ($OTID == 6) {
        $c = 'black';
        $bc = 'bbbbff';
        $SamProd = 'S';
        // $coName = $ACIDn;
      }elseif ($OTID == 7) {
        $c = 'white';
        $bc = 'f243e4';
        $SamProd = 'S';
        // $coName = $ACIDn;
      }elseif ($OTID == 8) {
        $c = 'white';
        $bc = '577268';
        $SamProd = 'S';
        // $coName = $pACIDn;
      }
      ?>
      <td style="width:2%; font-weight: bold;"><?php echo $SamProd?></td>
      <td style="width:6%; color:red; text-align: right; padding-right: 1%;"><a style="text-decoration:none; " href="<?php echo $urlPage ?>&orb&oi=<?php echo $OIID;?>"><?php echo $OIDnos.':'.$OIIDnos;?></a></td>
      <td style="width:1%; font-weight: bold; background-color:yellow;"><a style="text-decoration:none; " href="<?php echo $urlPage ?>&oed&oi=<?php echo $OIID;?>">e</a></td>
      <td style="width:24%; text-align:left; text-indent:1%; border-left: thin solid grey; border-right:thin solid grey; color: <?php echo $c ?>;background-color: #<?php echo $bc ?>;"><?php echo $cli_ACIDn ?></td>
      <td style="width:35%; text-align:left; text-indent:1%; border-right:thin solid grey;"><?php echo $ourRef;?></td>
      <?php
      if ($Stage1 == 0) {
        ?>
        <td style="width:8%; text-align:right; padding-right:1%; background-color: pink; border-right:thin solid grey;"><?php echo $Stage1;?></td>
        <?php
      }else {
        ?>
        <td style="width:8%; text-align:right; padding-right:1%; border-right:thin solid grey;"><?php echo $Stage1;?></td>
        <?php
      }
      ?>
      <td style="width:8%; text-align:right; padding-right:1%; border-right:thin solid grey;"><?php echo $OIQIDq;?></td>
      <?php
      if($OIDDIDd<$td){;?>
        <td style="width:14%; text-align:center; border-right:thin solid grey; background-color:#d8081e;"><?php echo $idate ;?></td>
        <?php
      }elseif($OIDDIDd<($td+604740)){;?>
        <td style="width:14%; text-align:center; border-right:thin solid grey; background-color:#f09595;"><?php echo $idate ;?></td>
        <?php
      }elseif($OIDDIDd>($td+121509600)){;?>
        <td style="width:14%; text-align:center; border-right:thin solid grey; background-color:#e2e8a8;"><?php echo $idate ;?></td>
        <?php
      }else{;?>
        <td style="width:14%; text-align:center; border-right:thin solid grey;"><?php echo $$idate ;?>
      </td>
      <?php
    }?>
    </tr>
    <?php
    if ($LRIDlast < $td) {
      ?>
      <td style="width:1%; background-color:RED;"></td>
      <?php
    }else {
      ?>
      <td style="width:1%; background-color:green;"></td>
      <?php
    }
  }
  } ?>
  </table>
<!--
  }
} -->
