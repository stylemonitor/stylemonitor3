<?php
session_start();
include 'dbconnect.inc.php';
// echo "<b>include/for_OR_get_min_fACID.inc.php</b>";

$sql = "SELECT MIN(d.ID) as DID
        FROM associate_companies ac
        , division d
        WHERE ac.ID = ?
        AND d.ACID = ac.ID
        ;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-fOgcfA</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $tACID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  $minfDID = $row['DID'];
}
