<?php
include 'dbconnect.inc.php';
include 'SM_colours.inc.php';
// echo "include/report_product_type.inc.php";
$CID = $_SESSION['CID'];

include 'from_CID_count_product_type.inc.php';
include 'from_CID_count_styles.inc.php';

$td = date('U');
$td = date('d M Y',$td);
// $cPTID = $cPTID - 1;

if (isset($_GET['spt'])) {$head = 'spt'; $rpp = 25;
}elseif (isset($_GET['spr'])) {$head = 'spr'; $rpp = 5;
}elseif (isset($_GET['spe'])) {$head = 'spe'; $rpp = 5;
}

// Check the URL to see what page we are on
if (isset($_GET['pa'])) { $pa = $_GET['pa'];
}else{ $pa = 0;
}
// if (isset($_GET['pa'])) { $pa = $_GET['pa'];
// }else{ $pa = 0;
// }

// Check which page we are on
if ($pa > 1) { $start = ($pa * $rpp) - $rpp;
}else { $start = 0;
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cPTID/$rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV);
// echo '<br>The number of pages will be : '.$PTV;
// echo '<br>Start record is : '.$start;
// echo "<br>**** count_orders count : $cPTID ****";
?>
<?php
if ($cPTID == 0) {?>
<?php
  $pddh = 'Create Product Type';
  $pddhbc = $prtCol;
  include 'page_description_date_header.inc.php';

  if (!isset($_GET['fnpt'])) {
    ?>
    <div style="position:absolute; top:28%; height:25%; left:30%; width:40%;">
      <p style="font-size:150%;">Before you go any further
      <br>we need to know the type of products you make.</p>
      <br>
      <p style="font-size:150%;">These are then used with your</p>
      <br>
      <p style="font-size:150%;"><b>STYLES, CLIENTS and SUPPLIERS</b></p>
      <br>
      <p style="font-size:150%;">to quickly identify what products you have in common with them.</p>
    </div>
    <?php
  }
}else {
  $pddh = 'Product Type Register';
  $pddhbc = $prtCol;
  include 'page_description_date_header.inc.php';

  if ($cPRID == 0) {
    ?>
    <div style="position:absolute; top:40%; left:20%; width:60%; font-size:200%;">
      You now need to enter a <a style="color:blue; text-decoration:none;" href="styles.php?S&snn&fns">Style</a>
    </div>
    <?php
  }else {
  }
?>
<!-- Eventually there should be a SORT on the SHort Code, the Product Type Reference and date created -->

  <table class="trs" style="position:relative; top:9%;">
    <tr>
      <th style="width:10%; text-align:left; text-indent:0.5%;">Short Code</th>
      <th style="width:40%; text-align:left; text-indent:0.5%;">Product Type Reference</th>
      <th style="width:20%; text-align:center;"><a style="color:black; text-decoration:none;">Orders</th>
      <th style="width:20%; text-align:center;"><a style="color:black; text-decoration:none;">Styles</th>
      <th style="width:10%;">Created</th>
    </tr>
    <?php

    include 'sql/report_product_type.sql.php';

    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-rpta</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "sss", $CID, $start, $rpp);
      mysqli_stmt_execute($stmt);
      $res = mysqli_stmt_get_result($stmt);
      // $sort == DESC ? $sort = ASC : $sort = DESC;

      while($row = mysqli_fetch_array($res)){
        $PTID = $row['PTID'];
        $input = $row['input'];
        $PTIDsc = $row['Short_code'];
        $PTIDn = $row['Product_Type_Ref'];
        $cACID = $row['cptACID'];
        $cOIID = $row['cptOIID'];
        $cSID = $row['cptSID'];

        $input = date('d-M-Y',$input);
        ?>
        <tr style="font-family:monospace;">
          <td class="select" style="border-right: thin solid grey; text-align:left; text-indent:3%;"><a class="hreflink" href="styles.php?S&spt&fpto=<?php echo $PTID ?>"><?php echo "$PTIDsc"; ?></a></td>
          <td class="select" style="border-right: thin solid grey; text-align:left; text-indent:2%;"><a class="hreflink" href="styles.php?S&spt&fpto=<?php echo $PTID ?>"><?php echo "$PTIDn"; ?></a></td>
          <td class="select" style="border-right: thin solid grey; text-indent:3%;"><a class="hreflink" href="styles.php?S&spo=<?php echo $PTID ?>"><?php echo $cOIID ?></a></td>
          <td class="select" style="border-right: thin solid grey; text-indent:3%;"><a class="hreflink" href="styles.php?S&spr&p=<?php echo $PTID ?>"><?php echo $cSID ?></a></td>
          <td><?php echo $input ?></td>
        </tr>
        <?php
      }
    }
    ?>
    <tr>
      <td colspan="5" style="border-top:2px solid black;"></td>
    </tr>
  </table>
<?php }
if (isset($_GET['spr'])) {
  ?>
  <div style="position:absolute; bottom:10%; right:5%;">
    <?php for ($pa = 1 ; $pa <= $PTV ; $pa++){
      echo "<a style='font-size:200%; text-decoration:none;' href='?S&$head&pa=$pa'>  $pa  </a>";
    }
    ?>
  </div>
  <?php
}else {
  if ($pa == 0) {
    // code...
  }else {
    ?>
    <div style="position:absolute; top:80%; right:5%; z-index:1;">
      <?php for ($pa = 1 ; $pa <= $PTV ; $pa++){
        echo "<a style='font-size:200%; text-decoration:none;' href='?S&$head&pa=$pa'>  $pa  </a>";
      }
      ?>
    </div>
    <?php
  }
}
if (!isset($_GET['fnpt']) && (!isset($_GET['fpto']))) {
  ?>
  <form action="styles.php?S&spt&spth" method="post">
    <button class="entbtn" type="submit" style="position:absolute; bottom:5%; left:5%; width:15%; background-color:<?php echo $hlpCol ?>;" name="addPtypebutton">HELP</button>
  </form>
  <form action="styles.php?S&spt&fnpt" method="post">
    <button class="entbtn" type="submit" style="position:absolute; bottom:5%; left:80%; width:15%;" name="addPtypebutton">Add Product Type</button>
  </form>
  <?php
}
