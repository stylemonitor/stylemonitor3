<?php
session_start();
// include 'dbconnect.inc.php';
include 'dbconnect.inc.php';
include 'include/from_CID_count_season.inc.php';
// echo "include/report_seasons.inc.php";
$CID = $_SESSION['CID'];
// $cSNID = $cSNID - 1;
$td = date('U');

// set the return pages section
if (isset($_GET['sea'])) {$head = 'sea'; $rpp = 30;
}elseif (isset($_GET['ser'])) {$head = 'ser'; $rpp = 30;
}

// Check the URL to see what page we are on
if (isset($_GET['pa'])) { $pa = $_GET['pa'];
}else{ $pa = 0;
}

// Set the rows per page
// $rpp = 30;

// Check which page we are on
if ($pa > 1) { $start = ($pa * $rpp) - $rpp;
}else { $start = 0;
}

// How many pages will this give us Pages To View $PTV
// ceil rounds up a number ie 1.1 becomes 2 and 1.9 is also 2
$PTV = ($cSNID / $rpp);

// echo '<br>The number of pages will be : '.$PTV;
$PTV = ceil($PTV);

?>
<?php
if ($cSNID == 0) {
  $td = date('d M Y',$td);
  $pddh = 'Create a Season';
  $pddhbc = '#fffddd';
  include 'page_description_date_header.inc.php';
  ?>
  <!-- <div class="cpsty" style="position:relative; top:0%; background-color:#b2b988"><?php echo "You have no product types" ?></div> -->
  <br><br><br>
  <br><br><br>
  <br><br><br>
  <p style="font-size:200%;">You can add a SEASON whenever you want</p>
  <br>
  <p style="font-size:200%;">When you have, you need to then EDIT your Styles accordingly</p>
  <br>
  <p style="font-size:200%;">You do not need to have any Seasons for the system to work</p>
  <?php

}else {
  if ($cSNID == 1) {
    $seatitle = 'season';
  }else {
    $seatitle = 'seasons';
  }
  $td = date('d M Y',$td);
  $pddh = 'Season Register';
  include 'page_description_date_header.inc.php';
  ?>

  <!-- <div class="cpsty" style="background-color:#b2b988"><?php echo "Your company has $cSNID $seatitle" ?></div> -->
  <table class="trs" style="position:relative; top:12%;">
    <tr>
      <th style="width:32%; text-align:left; text-indent:2%;">Season</th>
      <th style="width:16%;">Count Styles</th>
      <th style="width:16%;">Count Clients</th>
      <th style="width:16%;">Count Orders</th>
      <th style="width:6%;">Details</th>
      <th style="width:6%;">Edit</th>
      <th style="width:8%;">Created</th>
    </tr>
    <?php

    $sql = "SELECT s.ID as SID
              , s.name as SIDn
              , s.inputtime as input
            FROM season s
              , associate_companies ac
              , company c
            WHERE s.ACID = ?
            AND s.ACID = ac.ID
            AND ac.CID = c.ID
            AND s.name NOT IN ('Select Season')
            ORDER BY s.name
            LIMIT $start, $rpp
            ;";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
      echo '<b>FAIL-rpt</b>';
    }else{
      mysqli_stmt_bind_param($stmt, "s", $ACID);
      mysqli_stmt_execute($stmt);
      $res = mysqli_stmt_get_result($stmt);
      $sort == DESC ? $sort = ASC : $sort = DESC;

      while($row = mysqli_fetch_array($res)){
        $SID = $row['SID'];
        $SIDn = $row['SIDn'];
        $input = $row['input'];

        $input = date('d-M-Y', $input);
        ?>
        <tr>
          <td style="text-align:left; text-indent:3%;"><?php echo $SIDn ?></td>
          <td style="border-left:thin solid grey;">$cSIDsty</td>
          <td style="border-left:thin solid grey;">$cSIDclis</td>
          <td style="border-left:thin solid grey;">$cSIDord</td>
          <td style="background-color:pink;"><a style="text-decoration:none;" href="styles.php?S&see&se=<?php echo $SID ?>">Open</a></td>
          <td style="background-color:yellow;"><a style="text-decoration:none;" href="styles.php?S&see&se=<?php echo $SID ?>">Edit</a></td>
          <td><?php echo $input ?></td>
        </tr>
        <?php
      }
    }
    ?>
    <tr>
      <td style="border-top: 2px solid black;"></td>
      <td style="border-top: 2px solid black;"></td>
      <td style="border-top: 2px solid black;"></td>
      <td style="border-top: 2px solid black;"></td>
      <td style="border-top: 2px solid black;"></td>
      <td style="border-top: 2px solid black;"></td>
      <td style="border-top: 2px solid black;"></td>
    </tr>
  </table>
<?php }
if (isset($_GET['ser'])) {
  ?>
  <div style="position:absolute; bottom:10%; right:5%;">
    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a style='font-size:200%; text-decoration:none;' href='?S&$head&pa=$x'>  $x  </a>";
    }
    ?>
  </div>
  <?php
}else {
  ?>
  <div style="position:absolute; top:65%; right:5%;">
    <?php for ($x = 1 ; $x <= $PTV ; $x++){
      echo "<a style='font-size:200%; text-decoration:none;' href='?S&$head&pa=$x'>  $x  </a>";
    }
    ?>
  </div>
  <?php
}
?>
