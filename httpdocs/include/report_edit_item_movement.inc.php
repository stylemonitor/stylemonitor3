<?php
// echo "report_edit_item_movement.inc.php";

if (isset($_GET['eir'])) {
  $OICID = $_GET['eir'];
}

// echo "OICID=$OICID";

include 'sql/report_movement_log_CLIENT_OICID.sql.php';
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<b>FAIL-rmp2</b>';
}else{
  mysqli_stmt_bind_param($stmt, "s", $OICID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_array($result);
  $reqCIDn = $row['Req_Co'];
  $reqName = $row['Requestor'];
  $reqComms = $row['comments'];
  $resCIDn = $row['Resp_Co'];
  $resName = $row['Responder'];
  $resComms = $row['response'];
  $status = $row['status'];
  $reqDate = $row['Date_REQUESTED'];
  $resDate = $row['Date_RESPONDED'];
}

if ($status == 0) {
  $status = "PENDING";
  $statCol = "white";
}elseif ($status == 1) {
  $status = "REJECTED";
  $statCol = $rejCol;
}elseif ($status == 2) {
  $status = "APPROVED";
  $statCol = $appCol;
}
?>
<div class="overlay" style="z-index:3;"></div>

<table class="trs" style="position:absolute; top:32.5%; left:5%; width:90%; background-color: white; border: 1px solid black; z-index:4;">
  <caption style="background-color:<?php echo $statCol ?>"><b>Item Change Report - <?php echo $status ?></b></caption>
  <tr>
    <th style="width:18%; text-align:left; text-indent:2%;">Requesting Company</th>
    <td style="width:42%; text-align:left; text-indent:2%;"><?php echo $reqCIDn ?></td>
    <th style="width:15%;">Date of request</th>
    <td style="width:25%;"><?php echo $reqDate ?></td>
  </tr>
  <tr>
    <th style="width:18%; text-align:left; text-indent:2%;">Requesting Person</th>
    <td style="text-align:left; text-indent:2%;"><b><?php echo $reqName ?></b></td>
  </tr>
  <tr>
    <th style="width:18%; text-align:left; text-indent:2%;">Reason</th>
    <td colspan="3" style="text-align:left; text-indent:2%;"><?php echo $reqComms ?></td>
  </tr>
  <tr>
    <th style="width:18%; text-align:left; text-indent:2%;">Responding Company</th>
    <td style="width:42%; text-align:left; text-indent:2%;"><?php echo $resCIDn ?></td>
    <th style="width:15%;">Date of response</th>
    <td style="width:25%;"><?php echo $resDate ?></td>
  </tr>
  <tr>
    <th style="width:18%; text-align:left; text-indent:2%;">Responding Person</th>
    <td colspan="3" style="text-align:left; text-indent:2%;"><b><?php echo $resName ?></b></td>
  </tr>
  <tr>
    <th style="width:18%; text-align:left; text-indent:2%;">Comments</th>
    <td colspan="3" style="text-align:left; text-indent:2%;"><?php echo $resComms ?></td>
  </tr>


</table>
<!--
<div style="position:absolute; top:20%; height:50%; left:20%; width:60%; border:1px ridge black; background-color: white; z-index:4;">
  Item Movement Request Report
  <div style="position:relative; top:5%; height:10%; left:5%; width:60%; text-align:left; z-index:4;">
    <br>Requesting Company :
    <br>Requesting person  :
    <br>Type of Request    :
    <br>Reason             :
  </div>
  <div style="position:relative; top:40%; height:10%; left:0%; width:100%; text-align:left; border-top:1px solid grey; z-index:4;">

  </div>
  <div style="position:relative; top:30%; height:10%; left:5%; width:60%; text-align:left; z-index:4;">
    Responding Company :
    <br>Responding person  :
    <br>Response           :
  </div>
</div> -->

<form  action="home.php?H&o=<?php echo $OID ?>&OI=<?php echo $OIID ?>" method="post">
  <button class="entbtn" type="submit" style="position:absolute; top:65%; height:4%; left:72%; width:6%; border-radius: 10px; border: 1px ridge grey; z-index:4;" name="button">Close</button>
</form>
<?php






?>
