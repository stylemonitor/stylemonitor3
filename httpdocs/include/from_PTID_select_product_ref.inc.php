<?php
include 'dbconnect.inc.php';
// echo '<br><b>include/from_PTID_select_product_ref.inc.php</b>';

$sql = "SELECT pr.ID as PRID
		      , pr.prod_desc as PRIDn
          , pr.prod_ref as PRIDsc
        FROM prod_ref pr
          , product_type pt
        WHERE pt.ID = ?
        AND pr.PTID = pt.ID
        AND pt.name NOT IN ('Select Product Type')
        AND pr.ID NOT IN (?)
        ORDER BY pr.prod_ref
;";
$stmt = mysqli_stmt_init($con);
if(!mysqli_stmt_prepare($stmt, $sql)){
  echo '<br><b>FAIL-fcspr</b>';
}else{
  mysqli_stmt_bind_param($stmt, "ss", $PTID, $PRID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  while($row = mysqli_fetch_array($result)){
    $PRID  = $row['PRID'];
    $PRIDn = $row['PRIDn'];
    $PRIDsc = $row['PRIDsc'];

    // echo '<option value="'.$PRID.'">"'.$PRIDn : $PTIDn.'"</option>';


    ?>
    <option value="<?php echo $PRID ?>"><?php echo "$PRIDsc : $PRIDn" ?></option>
    <?php
  }
}
?>
