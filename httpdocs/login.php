<?php
if(!isset($_SESSION['UID'])){
  include 'header.php';
}
?>

<div class="lhsb">
  <div class="lhsbw">
    <!-- The left side box for additioinal information regarding the current page -->
  </div>
</div>

<div class="ms" style="background-color:rgb(180, 240, 240)">
  <div class="msdc" style="background-color: #f1f1f1; text-align:center;">
    <div class="msssl"><h2>Log-in to StyleMonitor</h2>
    </div>
      <?php
        include "include/login.inc.php";
      ?>
  </div>
</div>

<div class="rhsb">
  <div class="rhsbw">
    <!-- <div style="position:absolute;top:90%; height:20%; left:60%; width:20%; z-index:1;">
      <script src="https://platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
      <script type="IN/FollowCompany" style="z-index:1;" data-id="68219017" data-counter="bottom"></script>
    </div> -->
    <?php
      // include "include/login_rhs.inc.php";
    ?>
  </div>
</div>
<!-- </div> -->

<?php
  include 'footer.php';
?>
