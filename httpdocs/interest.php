<?php
if(!isset($_SESSION['UID'])){
  include ('header.php');
}
?>

<div class="lhsb">
  <div class="lhsbw">
  </div>
</div>

<div class="ms" style="background-color:rgb(180, 240, 240);">
  <div class="msdc" style="text-align:center; background-color: #f1f1f1;">
    <div class="msssl"><h2>Register Interest in SM</h2>
    </div>
    <?php
    include ('include/interest.inc.php');
    ?>

  </div>
</div>

<!-- New rhs info -->
<div class="rhsb">
  <div class="rhsbw">
    <!-- <div style="position:absolute;top:90%; height:10%; left:60%; width:10%; z-index:1;">
      <script src="https://platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
      <script type="IN/FollowCompany" style="z-index:1;" data-id="68219017" data-counter="bottom"></script>
    </div> -->
    <?php
    include ('include/interest.rhsb.php');
    ?>

  </div>
</div>

<?php
include 'footer.php';
?>
