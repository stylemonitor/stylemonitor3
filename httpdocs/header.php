<?php
session_start();
$td = date('U');
$yd = date('d M Y', $td);
// Added to prevent someone accessing a page via the URL input only...IF IT WORKS
// it works BUT how to let them onto the index page in the first place
// but none after that???
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="shortcut icon" href="http://becarri.com/SMfav.ico">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <!-- <META HTTP-EQUIV="refresh" CONTENT="15"> -->

  <!-- <title>smDev</title> -->
  <!-- <title>smTest 2.1</title> -->
  <!-- <title>StyleMonitor 2.0</title> -->
  <title>becarri</title>

  <link href="style.css" rel="stylesheet" type="text/css" />
  <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
  <script src='https://https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js'></script>
</head>

<body>
  <!-- SM logo on the lefthand side in the header -->
  <div style="position:absolute; top:1%; height:5%; left:1.3%; width:3%; z-index:1;">
    <img src="SMfavicon.ico" alt="SM" height="100%" width="100%">
  </div>

  <header class="ns" style="background-color:#a2a899">

    <!-- SELECT AS APPROPRIATE -->
    <!-- title of page on righthand side in the header -->
    <!-- <b>smDev</b> -->
    <!-- <b>S</b>tyle<b>m</b>onitor -->
    <b>becarri</b>
    <!-- <b>smTest</b> -->
    <!-- smLive -->


    <!-- LinkedIn counter to show number of followers on LinkedIn -->
    <!-- <div style="position:absolute;top:90%; height:20%; left:55%; width:20%; z-index:1;">
      <script src="https://platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
      <script type="IN/FollowCompany" style="z-index:1;" data-id="68219017" data-counter="bottom"></script>
    </div> -->

  </header>

  <div class="ls">
    <?php
    // if (isset($_GET['H'])) {
    // include 'include/from_UID_get_user_details.inc.php';
    if (isset($_SESSION['UID'])) {
      // $UID = $_SESSION['UID'];
      // $UIDf = $_SESSION['UIDf'];
      // $CID = $_SESSION['CID'];
      // $CIDn = $_SESSION['CIDn'];
      // $udate = $_SESSION['udate'];

      // include 'include/get_user_login_details.php';

      // table used in the below query no longer exists!
      // include 'include/timezone.inc.php';

      ?>
      <form action="include/form_logout_action.php" method="POST">
        <a style="position:absolute; top:45%; left:3%; color:black; font-weight: bold;  text-align:left;text-decoration:none;">Hi, <?php echo $_SESSION['UIDf']; ?> you are logged in on behalf of <?php echo $_SESSION['CIDn']; ?></a>
        <button class="LObtn" type="submit" name="log-out">Log-Out</button>
      </form>
    <?php
    }else {
      if (isset($_GET['h'])) {
        // echo "<br>UID = $UID : CID = $CID :: CIDn = $CIDn";

        ?>
        <div style="position:absolute; top:45%; left:3%; width:8.5%; color:black; font-weight: bold; text-align:left;">
          <a href="index.php?h" style="text-decoration:none;">Home</a>
        </div>
        <?php
      }else {
        ?>
        <a href="index.php?h" style="position:absolute; top:45%; left:3%; width:8.5%; color:grey; font-weight: bold; text-align:left;  text-decoration:none;">Home</a>
        <?php
      }
      if (isset($_GET['a'])) {
        ?>
        <a href="index.php?a" style="position:absolute; top:45%; left:11.5%; color:black; font-weight: bold; text-align:left; text-decoration:none;">About</a>
        <?php
      }else {
        ?>
        <a href="index.php?a" style="position:absolute; top:45%; left:11.5%; color:grey; font-weight: bold; text-align:left; text-decoration:none;">About</a>
        <?php
      }
      if (isset($_GET['a'])) {
        ?>
        <a href="index.php?a" style="position:absolute; top:45%; left:11.5%; color:black; font-weight: bold; text-align:left; text-decoration:none;">About</a>
        <?php
      }else {
        ?>
        <a href="index.php?a" style="position:absolute; top:45%; left:11.5%; color:grey; font-weight: bold;  text-align:left; text-decoration:none;">About</a>
        <?php
      }
      if (isset($_GET['i'])) {
        ?>
        <a href="index.php?i" style="position:absolute; top:45%; left:20%; color:black; font-weight: bold;  text-align:left; text-decoration:none;">Interested</a>
        <?php
      }else {
        ?>
        <a href="index.php?i" style="position:absolute; top:45%; left:20%; color:grey; font-weight: bold; text-align:left; text-decoration:none;">Interested</a>
        <?php
      }
      if (isset($_GET['q'])) {
        ?>
      <a href="index.php?q" style="position:absolute; top:45%; left:30%; color:black; background-color: black; font-weight: bold;  text-align:left; text-decoration:none; z-index:1;">FAQ</a>
        <?php
      }else {
        ?>
      <a href="index.php?q" style="position:absolute; top:45%; left:30%; color:grey; font-weight: bold;  text-align:left; text-decoration:none;">FAQ</a>
        <?php
      }
      if (isset($_GET['c'])) {
        ?>
      <!-- <a href="index.php?c" style="position:absolute; top:45%; left:37%; color:black; font-weight: bold; text-align:left; text-decoration:none;">Contacts</a> -->
        <?php
      }else {
        ?>
      <!-- <a href="index.php?c" style="position:absolute; top:45%; left:37%; color:grey; font-weight: bold; text-align:left; text-decoration:none;">Contacts</a> -->
        <?php
      }
      if (isset($_GET['l'])) {
        ?>
        <a href="index.php?l" style="position:absolute; top:45%; right:3%; color:black; font-weight: bold; text-align:left; text-decoration:none;">Log-in</a>
        <?php
      }else {
        ?>
      <a href="index.php?l" style="position:absolute; top:45%; right:3%; color:grey; font-weight: bold; text-align:left; text-decoration:none;">Log-in</a>
        <?php
      }
    } ?>
  </div>
