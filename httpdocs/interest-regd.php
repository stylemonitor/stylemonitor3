<?php
include 'header.php'
?>

<div class="lhsb">
  <div class="lhsbw">
    <?php
    include 'include/menu_bar_left_side.inc.php';
    ?>
  </div>
</div>

<div class="ms" style="background-color:rgb(180, 240, 240)";>
  <!-- Taken out the tab button -->
  <!-- <a class ="mswhl" href="index.php">Back</a> -->
  <div class="msdc" style="text-align:center; background-color: #f1f1f1;">
    <div class="msssl"><h2>Your company is already registered in StyleMonitor</h2></div>

    <?php

    if (isset($_GET['c'])) {
      // GET the information from the form
      $c = $_GET['c'];
      $f = $_GET['first'];

      // GET the name of the companies administrator
      $sql = "SELECT ID as UID
              FROM users
              WHERE uid=? or email=?;";
      $stmt = mysqli_stmt_init($con);
      if(!mysqli_stmt_prepare($stmt, $sql)){
        // echo '<b>FAIL</b>';
      }else{
        mysqli_stmt_bind_param($stmt, "ss", $*, $*);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);

        // Then one of the following
        $row = mysqli_fetch_array($result);
      }



      echo "
      <br>Hi ".$f.".
      <br><br><p>Your company, ".$c." is already been registered with us</p>";
      echo "<br>We have sent an email to 'enter a name' your company administrator asking that they register you as a user";
      echo "<p>Please speak to them the next time you see them</p>";
      echo "<p>Regards, Richard</p>";


    }

     ?>

  </div>
</div>

<div class="rhsb">
  <div class="rhsbw">
    <?php
    include 'include/session_dets.inc.php';
    ?>
  </div>
</div>

<?php
include 'footer.php';
?>
