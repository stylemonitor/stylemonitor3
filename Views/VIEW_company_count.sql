CREATE OR REPLACE VIEW company_count
AS
(SELECT cR.name AS CLIENT
       , cR.ID AS CLIENT_ID
       , COUNT(acR.ID) AS CL_ASSOC_CO
       , COUNT(dR.ID) AS CL_ASSOC_DIV
       , cA.name AS SUPPLIER
       , cA.ID AS SUPPLIER_ID
       , COUNT(acA.ID) AS SU_ASSOC_CO
       , COUNT(dA.ID) AS SU_ASSOC_DIV
FROM company_partnerships cp
     INNER JOIN associate_companies acR ON cp.req_CID = acR.CID
     INNER JOIN company cR ON acR.CID = cR.ID
     INNER JOIN associate_companies acA ON cp.acc_CID = acA.CID
     INNER JOIN company cA ON acA.CID = cA.ID
     INNER JOIN division dR ON dR.ACID = acR.ID
     INNER JOIN division dA ON dA.ACID = acA.ID
GROUP BY cR.name, cA.name
);