/*
StyleMonitor views
Four views:
client_count
company_count
company_relationships
supplier_count
*/

-- client_count view
CREATE OR REPLACE VIEW client_count
AS
(
 SELECT ASSOC_CO.COMPANY AS CLIENT
        , DIVISIONS.CO_ID AS CL_ID
        , ASSOC_CO.ASSOC_COUNT AS CL_ASSOC_CO
        , DIVISIONS.DIV_COUNT AS CL_ASSOC_DIV
        , SECTIONS.SECTION_COUNT AS CL_DIV_SECTIONS
FROM (
      SELECT c.name AS COMPANY
             ,COUNT(distinct ac.ID) AS ASSOC_COUNT
      FROM associate_companies ac
           INNER JOIN company c ON c.ID = ac.CID
           INNER JOIN company_partnerships cp ON cp.req_CID = ac.CID
      WHERE ac.CTID in (4,6,7)  
      GROUP BY c.name
     ) ASSOC_CO INNER JOIN (SELECT c.name AS COMPANY
                                   , c.ID AS CO_ID
                                   , COUNT(distinct d.ID) AS DIV_COUNT
                            FROM division d
                                 INNER JOIN associate_companies ac ON ac.ID = d.ACID
                                 INNER JOIN company c ON c.ID = ac.CID
                                 INNER JOIN company_partnerships cp ON cp.req_CID = c.ID
                            WHERE ac.CTID in (4,6,7)
                            GROUP BY c.name
                           ) DIVISIONS ON ASSOC_CO.COMPANY = DIVISIONS.COMPANY
                INNER JOIN (SELECT c.name AS COMPANY
                                   , COUNT(distinct s.ID) AS SECTION_COUNT
                            FROM associate_companies ac
                                 INNER JOIN company c ON c.ID = ac.CID
                                 INNER JOIN company_partnerships cp ON cp.req_CID = ac.CID
                                 INNER JOIN division d ON ac.ID = d.ACID
                                 INNER JOIN section s ON d.ID = s.DID
                            WHERE ac.CTID in (4,6,7)
                            GROUP BY c.name) SECTIONS ON ASSOC_CO.COMPANY = SECTIONS.COMPANY
ORDER BY CLIENT
);

-- company_count view
CREATE OR REPLACE VIEW company_count
AS
(SELECT cR.name AS CLIENT
       , cR.ID AS CLIENT_ID
       , COUNT(acR.ID) AS CL_ASSOC_CO
       , COUNT(dR.ID) AS CL_ASSOC_DIV
       , cA.name AS SUPPLIER
       , cA.ID AS SUPPLIER_ID
       , COUNT(acA.ID) AS SU_ASSOC_CO
       , COUNT(dA.ID) AS SU_ASSOC_DIV
FROM company_partnerships cp
     INNER JOIN associate_companies acR ON cp.req_CID = acR.CID
     INNER JOIN company cR ON acR.CID = cR.ID
     INNER JOIN associate_companies acA ON cp.acc_CID = acA.CID
     INNER JOIN company cA ON acA.CID = cA.ID
     INNER JOIN division dR ON dR.ACID = acR.ID
     INNER JOIN division dA ON dA.ACID = acA.ID
GROUP BY cR.name, cA.name
);

-- company_relationships view
CREATE OR REPLACE VIEW company_relationships
AS
(SELECT CASE WHEN cR.ID = cA.ID THEN 'Internal' ELSE 'External' END AS Relationship
       , cp.ID AS CPID
       , cR.ID AS Client_ID
       , cR.name AS Client_Name
       , acR.ID AS Assoc_Client_Co
       , acR.name AS Assoc_Client_Name
       , dR.ID AS Assoc_Client_Div_ID
       , IF(dR.Name = 'Select Division','Not Known',dR.Name) AS Assoc_Client_Div_Name
       , cA.ID AS Supplier_Co_ID
       , cA.name AS Supplier_Co_Name
       , acA.ID AS Assoc_Supplier_Co
       , acA.name AS Assoc_Supplier_name
       , dA.ID AS Assoc_Supplier_Div
       , IF(dA.name = 'Select Division','Not Known',dA.Name) AS Assoc_Supplier_Div_Name
FROM company_partnerships cp
     INNER JOIN associate_companies acR ON cp.req_CID = acR.CID
     INNER JOIN company cR ON acR.CID = cR.ID
     INNER JOIN associate_companies acA ON cp.acc_CID = acA.CID
     INNER JOIN company cA ON acA.CID = cA.ID
     INNER JOIN division dR ON dR.ACID = acR.ID
     INNER JOIN division dA ON dA.ACID = acA.ID
ORDER BY cR.ID, Relationship
 );

-- supplier_count view
CREATE OR REPLACE VIEW supplier_count
AS
(SELECT ASSOC_CO.COMPANY AS SUPPLIER
	      , DIVISIONS.CO_ID AS SU_ID
        , ASSOC_CO.ASSOC_COUNT AS SU_ASSOC_CO
        , DIVISIONS.DIV_COUNT AS SU_ASSOC_DIV
        , SECTIONS.SECTION_COUNT AS SU_DIV_SECTIONS
FROM (SELECT c.name AS COMPANY
             ,COUNT(distinct ac.name) AS ASSOC_COUNT
      FROM associate_companies ac
           INNER JOIN company c ON c.ID = ac.CID
           INNER JOIN company_partnerships cp ON cp.acc_CID = ac.CID
      WHERE ac.CTID in (5,6,7)	
      GROUP BY c.name
     ) ASSOC_CO INNER JOIN (SELECT c.name AS COMPANY
                                   , c.ID AS CO_ID
              							       , COUNT(distinct d.ID) AS DIV_COUNT
              							FROM division d
              							     INNER JOIN associate_companies ac ON ac.ID = d.ACID
              							     INNER JOIN company c ON c.ID = ac.CID
              							     INNER JOIN company_partnerships cp ON cp.acc_CID = ac.CID
              							WHERE ac.CTID in (5,6,7)
              							GROUP BY c.name
						               ) DIVISIONS ON ASSOC_CO.COMPANY = DIVISIONS.COMPANY
                INNER JOIN (SELECT c.name AS COMPANY
                                   , COUNT(distinct s.ID) AS SECTION_COUNT
                            FROM associate_companies ac
                                 INNER JOIN company c ON c.ID = ac.CID
                                 INNER JOIN company_partnerships cp ON cp.req_CID = ac.CID
                                 INNER JOIN division d ON ac.ID = d.ACID
                                 INNER JOIN section s ON d.ID = s.DID
                            WHERE ac.CTID in (5,6,7)
                            GROUP BY c.name) SECTIONS ON ASSOC_CO.COMPANY = SECTIONS.COMPANY     
ORDER BY SUPPLIER
);