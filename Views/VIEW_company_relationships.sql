CREATE OR REPLACE VIEW company_relationships
AS
(SELECT CASE WHEN cR.ID = cA.ID THEN 'Internal' ELSE 'External' END AS Relationship
       , cp.ID AS CPID
       , cR.ID AS Client_ID
       , cR.name AS Client_Name
       , acR.ID AS Assoc_Client_Co
       , acR.name AS Assoc_Client_Name
       , dR.ID AS Assoc_Client_Div_ID
       , IF(dR.Name = 'Select Division','Not Known',dR.Name) AS Assoc_Client_Div_Name
       , cA.ID AS Supplier_Co_ID
       , cA.name AS Supplier_Co_Name
       , acA.ID AS Assoc_Supplier_Co
       , acA.name AS Assoc_Supplier_name
       , dA.ID AS Assoc_Supplier_Div
       , IF(dA.name = 'Select Division','Not Known',dA.Name) AS Assoc_Supplier_Div_Name
FROM company_partnerships cp
     INNER JOIN associate_companies acR ON cp.req_CID = acR.CID
     INNER JOIN company cR ON acR.CID = cR.ID
     INNER JOIN associate_companies acA ON cp.acc_CID = acA.CID
     INNER JOIN company cA ON acA.CID = cA.ID
     INNER JOIN division dR ON dR.ACID = acR.ID
     INNER JOIN division dA ON dA.ACID = acA.ID
ORDER BY cR.ID, Relationship
 );