CREATE OR REPLACE VIEW client_count
AS
(
 SELECT ASSOC_CO.COMPANY AS CLIENT
        , DIVISIONS.CO_ID AS CL_ID
        , ASSOC_CO.ASSOC_COUNT AS CL_ASSOC_CO
        , DIVISIONS.DIV_COUNT AS CL_ASSOC_DIV
        , SECTIONS.SECTION_COUNT AS CL_DIV_SECTIONS
FROM (
      SELECT c.name AS COMPANY
             ,COUNT(distinct ac.ID) AS ASSOC_COUNT
      FROM associate_companies ac
           INNER JOIN company c ON c.ID = ac.CID
           INNER JOIN company_partnerships cp ON cp.req_CID = ac.CID
      WHERE ac.CTID in (4,6,7)  
      GROUP BY c.name
     ) ASSOC_CO INNER JOIN (SELECT c.name AS COMPANY
                                   , c.ID AS CO_ID
                                   , COUNT(distinct d.ID) AS DIV_COUNT
                            FROM division d
                                 INNER JOIN associate_companies ac ON ac.ID = d.ACID
                                 INNER JOIN company c ON c.ID = ac.CID
                                 INNER JOIN company_partnerships cp ON cp.req_CID = c.ID
                            WHERE ac.CTID in (4,6,7)
                            GROUP BY c.name
                           ) DIVISIONS ON ASSOC_CO.COMPANY = DIVISIONS.COMPANY
                INNER JOIN (SELECT c.name AS COMPANY
                                   , COUNT(distinct s.ID) AS SECTION_COUNT
                            FROM associate_companies ac
                                 INNER JOIN company c ON c.ID = ac.CID
                                 INNER JOIN company_partnerships cp ON cp.req_CID = ac.CID
                                 INNER JOIN division d ON ac.ID = d.ACID
                                 INNER JOIN section s ON d.ID = s.DID
                            WHERE ac.CTID in (4,6,7)
                            GROUP BY c.name) SECTIONS ON ASSOC_CO.COMPANY = SECTIONS.COMPANY
ORDER BY CLIENT
);